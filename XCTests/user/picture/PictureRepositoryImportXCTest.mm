/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "PictureRepositoryImportTest.h"
#import "ChangeLogRepository.h"

@interface PictureRepositoryImportXCTest : AbstractXCTests
@end

@implementation PictureRepositoryImportXCTest
{
  PictureRepositoryImportTest* test;
}
- (void)setUp {
  [super setUp];
  test = new PictureRepositoryImportTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testImportAddPicture {
  test->testImportAddPicture();
}
- (void)testImportUpdatePicture {
  test->testImportUpdatePicture();
}
- (void)testImportUpdatePicture_Identical {
  test->testImportUpdatePicture_Identical();
}
- (void)testImportUpdatePicture_Conflict {
  test->testImportUpdatePicture_Conflict();
}
- (void)testImportDeletePicture {
  test->testImportDeletePicture();
}
- (void)testImportDeletePicture_Conflict {
  test->testImportDeletePicture_Conflict();
}
@end
