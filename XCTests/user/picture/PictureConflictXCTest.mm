/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "PictureConflictTest.h"
#include "ChangeLogRepository.h"

@interface PictureConflictXCTest : AbstractXCTests
@end

@implementation PictureConflictXCTest
{
  PictureConflictTest* test;
}
- (void)setUp {
  [super setUp];
  test = new PictureConflictTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddPicture {
  test->testAddPicture();
}
- (void)testAddPicture_Empty {
  test->testAddPicture_Empty();
}
- (void)testUpdatePicture_LocalUnchanged {
  test->testUpdatePicture_LocalUnchanged();
}
- (void)testUpdatePicture_LocalUpdate_EqualContent {
  test->testUpdatePicture_LocalUpdate_EqualContent();
}
- (void)testUpdatePicture_LocalUpdate {
  test->testUpdatePicture_LocalUpdate();
}
- (void)testDeletePicture_LocalUnchanged {
  test->testDeletePicture_LocalUnchanged();
}
- (void)testDeletePicture_LocalUpdate {
  test->testDeletePicture_LocalUpdate();
}
- (void)testForceClearConflict {
  test->testForceClearConflict();
}
@end
