/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "MemberRepositoryImportTest.h"
#import "ChangeLogRepository.h"

@interface MemberRepositoryImportXCTest : AbstractXCTests
@end

@implementation MemberRepositoryImportXCTest
{
  MemberRepositoryImportTest* test;
}
- (void)setUp {
  [super setUp];
  test = new MemberRepositoryImportTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testImportAddMember {
  test->testImportAddMember();
}
- (void)testImportAddMemberWithApprovals {
  test->testImportAddMemberWithApprovals();
}
- (void)testImportUpdateMember {
  test->testImportUpdateMember();
}
- (void)testImportUpdateMember_AddApproval {
  test->testImportUpdateMember_AddApproval();
}
- (void)testImportUpdateMember_Identical {
  test->testImportUpdateMember_Identical();
}
- (void)testImportUpdateMember_Conflict {
  test->testImportUpdateMember_Conflict();
}
- (void)testImportUpdateMember_RemoveApproval {
  test->testImportUpdateMember_RemoveApproval();
}
- (void)testImportDeleteMember {
  test->testImportDeleteMember();
}
- (void)testImportDeleteMember_Conflict {
  test->testImportDeleteMember_Conflict();
}
@end
