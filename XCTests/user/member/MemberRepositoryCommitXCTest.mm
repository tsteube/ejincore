/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "MemberRepositoryCommitTest.h"
#import "ChangeLogRepository.h"

@interface MemberRepositoryCommitXCTest : AbstractXCTests
@end

@implementation MemberRepositoryCommitXCTest
{
  MemberRepositoryCommitTest* test;
}
- (void)setUp {
  [super setUp];
  test = new MemberRepositoryCommitTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testCommitUpdatedMember {
  test->testCommitUpdatedMember();
}
- (void)testCommitAddedApproval {
  test->testCommitAddedApproval();
}
- (void)testCommitDeletedApproval {
  test->testCommitDeletedApproval();
}
@end
