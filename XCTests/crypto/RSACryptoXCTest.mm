/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "RSACryptoTest.h"

@interface RSACryptoXCTest : AbstractXCTests
@end

@implementation RSACryptoXCTest
{
  RSACryptoTest* test;
}
- (void)setUp {
  [super setUp];
  test = new RSACryptoTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testBase64 {
  test->testBase64();
}
- (void)testRSAGenerationToFolder {
  test->testRSAGenerationToFolder();
}
- (void)testRSAGenerationToProtectedFolder {
  test->testRSAGenerationToProtectedFolder();
}
- (void)testLoadingKeysPEM {
  test->testLoadingKeysPEM();
}
- (void)testLoadingKeysPEMWithPassword {
  test->testLoadingKeysPEMWithPassword();
}
- (void)testLoadingKeysASN1 {
  test->testLoadingKeysASN1();
}
- (void)testTransferSessionKey {
  test->testTransferSessionKey();
}
- (void)testTransferMessage {
  test->testTransferMessage();
}
- (void)testTransferOldMessage {
  test->testTransferOldMessage();
}
- (void)testTransferFile {
  test->testTransferFile();
}
- (void)testFileIO {
  test->testFileIO();
}
- (void)testAESInteroperability {
  test->testAESInteroperability();
}
- (void)testDecryptMessage {
  test->testDecryptMessage();
}
- (void)testDecryptOldMessage {
  test->testDecryptOldMessage();
}
- (void)testEncryptSessionKey {
  test->testEncryptSessionKey();
}
- (void)testMessageSignature {
  test->testMessageSignature();
}
- (void)testAssignPrivateKey {
  test->testAssignPrivateKey();
}
- (void)testExportKeys {
  test->testExportKeys();
}
@end
