/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "CppWrapperTest.h"

@interface CppWrapperXCTest : AbstractXCTests
@end

@implementation CppWrapperXCTest
{
  CppWrapperTest* test;
}
- (void)setUp {
  [super setUp];
  test = new CppWrapperTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAttachment {
  test->testAttachment();
}
- (void)testAttachmentArchive {
  test->testAttachmentArchive();
}
- (void)testCRUD {
  test->testCRUD();
}
- (void)testCRUDArchive {
  test->testCRUDArchive();
}
- (void)testSqlValue {
  test->testSqlValue();
}
- (void)testDBUnit {
  test->testDBUnit();
}
- (void)testProperties {
  test->testProperties();
}
- (void)testEjinDbRekey {
  test->testEjinDbRekey();
}
- (void)testEjinDbConfig {
  test->testEjinDbConfig();
}
@end
