/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "MediaRepositoryImportTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"

@interface MediaRepositoryImportXCTest : AbstractXCTests
@end

@implementation MediaRepositoryImportXCTest
{
  MediaRepositoryImportTest* test;
}
- (void)setUp {
  [super setUp];
  test = new MediaRepositoryImportTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testImportAddMedia {
  test->testImportAddMedia();
}
- (void)testImportUpdateMedia {
  test->testImportUpdateMedia();
}
- (void)testImportUpdateMedia_Identical {
  test->testImportUpdateMedia_Identical();
}
- (void)testImportUpdateMedia_Conflict {
  test->testImportUpdateMedia_Conflict();
}
- (void)testImportDeleteMedia {
  test->testImportDeleteMedia();
}
- (void)testImportDeleteMedia_Conflict {
  test->testImportDeleteMedia_Conflict();
}
- (void)testMediaMetadata {
  test->testMediaMetadata();
}
- (void)testAttachment {
  test->testAttachment();
}
@end
