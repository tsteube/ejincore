/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "MediaConflictTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"

@interface MediaConflictXCTest : AbstractXCTests
@end

@implementation MediaConflictXCTest
{
  MediaConflictTest* test;
}
- (void)setUp {
  [super setUp];
  test = new MediaConflictTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddMedia {
  test->testAddMedia();
}
- (void)testAddMedia_Empty {
  test->testAddMedia_Empty();
}
- (void)testUpdateMedia_LocalUnchanged {
  test->testUpdateMedia_LocalUnchanged();
}
- (void)testUpdateMedia_LocalAdd {
  test->testUpdateMedia_LocalAdd();
}
- (void)testUpdateMedia_LocalUpdate_EqualContent {
  test->testUpdateMedia_LocalUpdate_EqualContent();
}
- (void)testUpdateMedia_LocalUpdate {
  test->testUpdateMedia_LocalUpdate();
}
- (void)testUpdateMedia_LocalDelete_EqualContent {
  test->testUpdateMedia_LocalDelete_EqualContent();
}
- (void)testUpdateMedia_LocalDelete {
  test->testUpdateMedia_LocalDelete();
}
- (void)testDeleteMedia_LocalUnchanged {
  test->testDeleteMedia_LocalUnchanged();
}
- (void)testDeleteMedia_LocalAdd {
  test->testDeleteMedia_LocalAdd();
}
- (void)testDeleteMedia_LocalUpdate {
  test->testDeleteMedia_LocalUpdate();
}
- (void)testDeleteMedia_LocalDelete {
  test->testDeleteMedia_LocalDelete();
}
- (void)testDeleteMedia_LocalDelete_Conflict {
  test->testDeleteMedia_LocalDelete_Conflict();
}
- (void)testForceClearConflict {
  test->testForceClearConflict();
}
@end
