/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelMediaConflictTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelMediaConflictXCTest : AbstractXCTests
@end

@implementation ChannelMediaConflictXCTest
{
  ChannelMediaConflictTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelMediaConflictTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddChannelMedia {
  test->testAddChannelMedia();
}
- (void)testUpdateChannelMedia_LocalUnchanged {
  test->testUpdateChannelMedia_LocalUnchanged();
}
- (void)testUpdateChannelMedia_LocalAdd {
  test->testUpdateChannelMedia_LocalAdd();
}
- (void)testUpdateChannelMedia_LocalDelete {
  test->testUpdateChannelMedia_LocalDelete();
}
- (void)testUpdateChannelMedia_LocalUpdate {
  test->testUpdateChannelMedia_LocalUpdate();
}
- (void)testUpdateChannelMedia_LocalUpdate_noChange {
  test->testUpdateChannelMedia_LocalUpdate_noChange();
}
- (void)testDeleteChannelMedia_LocalUnchanged {
  test->testDeleteChannelMedia_LocalUnchanged();
}
- (void)testDeleteChannelMedia_LocalAdd {
  test->testDeleteChannelMedia_LocalAdd();
}
- (void)testDeleteChannelMedia_LocalUpdate {
  test->testDeleteChannelMedia_LocalUpdate();
}
- (void)testDeleteChannelMedia_LocalDelete {
  test->testDeleteChannelMedia_LocalDelete();
}
@end
