/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelMediaRepositoryTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"

@interface ChannelMediaRepositoryXCTest : AbstractXCTests
@end

@implementation ChannelMediaRepositoryXCTest
{
  ChannelMediaRepositoryTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelMediaRepositoryTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddAndCommitChannelMedia {
  test->testAddAndCommitChannelMedia();
}
- (void)testUpdateAndCommitChannelMedia {
  test->testUpdateAndCommitChannelMedia();
}
- (void)testUpdateChannelMedia_Conflict {
  test->testUpdateChannelMedia_Conflict();
}
- (void)testRemoveChannelMedia {
  test->testRemoveChannelMedia();
}
- (void)testRemoveChannelMedia_Conflict {
  test->testRemoveChannelMedia_Conflict();
}
@end
