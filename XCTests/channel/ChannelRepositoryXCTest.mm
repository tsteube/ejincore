/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelRepositoryTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelRepositoryXCTest : AbstractXCTests
@end

@implementation ChannelRepositoryXCTest
{
  ChannelRepositoryTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelRepositoryTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testDetectConflict {
  test->testDetectConflict();
}
- (void)testLoadModifications_Unchanged {
  test->testLoadModifications_Unchanged();
}
- (void)testLoadModifications_Add {
  test->testLoadModifications_Add();
}
- (void)testLoadModifications_Remove {
  test->testLoadModifications_Remove();
}
- (void)testLoadModifications_Update {
  test->testLoadModifications_Update();
}
- (void)testLoadModifications_MediaChanges {
  test->testLoadModifications_MediaChanges();
}
- (void)testLoadModifications_MemberChanges {
  test->testLoadModifications_MemberChanges();
}
- (void)testAddChannel {
  test->testAddChannel();
}
- (void)testUpdateChannel {
  test->testUpdateChannel();
}
- (void)testDeleteChannel {
  test->testDeleteChannel();
}
@end
