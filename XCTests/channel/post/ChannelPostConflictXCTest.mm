/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelPostConflictTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelPostConflictXCTest : AbstractXCTests
@end

@implementation ChannelPostConflictXCTest
{
  ChannelPostConflictTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelPostConflictTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddChannelPost {
  test->testAddChannelPost();
}
- (void)testAddChannelPost_Empty {
  test->testAddChannelPost_Empty();
}
- (void)testUpdateChannelPost_LocalUnchanged {
  test->testUpdateChannelPost_LocalUnchanged();
}
- (void)testUpdateChannelPost_LocalAdd {
  test->testUpdateChannelPost_LocalAdd();
}
- (void)testUpdateChannelPost_LocalUpdate_EqualContent {
  test->testUpdateChannelPost_LocalUpdate_EqualContent();
}
- (void)testUpdateChannelPost_LocalUpdate {
  test->testUpdateChannelPost_LocalUpdate();
}
- (void)testUpdateChannelPost_LocalDelete_EqualContent {
  test->testUpdateChannelPost_LocalDelete_EqualContent();
}
- (void)testUpdateChannelPost_LocalDelete {
  test->testUpdateChannelPost_LocalDelete();
}
- (void)testDeleteChannelPost_LocalUnchanged {
  test->testDeleteChannelPost_LocalUnchanged();
}
- (void)testDeleteChannelPost_LocalAdd {
  test->testDeleteChannelPost_LocalAdd();
}
- (void)testDeleteChannelPost_LocalUpdate {
  test->testDeleteChannelPost_LocalUpdate();
}
- (void)testDeleteChannelPost_LocalDelete {
  test->testDeleteChannelPost_LocalDelete();
}
- (void)testDeleteChannelPost_LocalDelete_Conflict {
  test->testDeleteChannelPost_LocalDelete_Conflict();
}
- (void)testForceClearConflict {
  test->testForceClearConflict();
}
@end

