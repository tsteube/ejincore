/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelPostRepositoryImportTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelPostRepositoryImportXCTest : AbstractXCTests
@end

@implementation ChannelPostRepositoryImportXCTest
{
  ChannelPostRepositoryImportTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelPostRepositoryImportTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testImportAddPost {
  test->testImportAddPost();
}
- (void)testImportAddPostWithResource {
  test->testImportAddPostWithResource();
}
- (void)testImportUpdatePost {
  test->testImportUpdatePost();
}
- (void)testImportUpdatePost_Identical {
  test->testImportUpdatePost_Identical();
}
- (void)testImportUpdatePost_Conflict {
  test->testImportUpdatePost_Conflict();
}
- (void)testImportUpdatePostAddResource {
  test->testImportUpdatePostAddResource();
}
- (void)testImportUpdatePostDeleteResource {
  test->testImportUpdatePostDeleteResource();
}
- (void)testImportDeletePost {
  test->testImportDeletePost();
}
- (void)testImportDeletePostWithResource {
  test->testImportDeletePostWithResource();
}
- (void)testImportDeletedPostWithComment {
  test->testImportDeletedPostWithComment();
}
- (void)testImportDeletePost_Conflict {
  test->testImportDeletePost_Conflict();
}
- (void)testChannelPostMetadata {
  test->testChannelPostMetadata();
}
@end
