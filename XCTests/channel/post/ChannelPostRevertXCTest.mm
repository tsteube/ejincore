/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelPostRevertTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelPostRevertXCTest : AbstractXCTests
@end

@implementation ChannelPostRevertXCTest
{
  ChannelPostRevertTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelPostRevertTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testChannelPostRevert_Empty {
  test->testChannelPostRevert_Empty();
}
- (void)testChannelPostRevert_AddMedia {
  test->testChannelPostRevert_AddMedia();
}
- (void)testChannelPostRevert_AddComment {
  test->testChannelPostRevert_AddComment();
}
- (void)testChannelPostRevert_Update {
  test->testChannelPostRevert_Update();
}
- (void)testChannelPostRevert_Delete {
  test->testChannelPostRevert_Delete();
}
- (void)testChannelPostRevert_Restore {
  test->testChannelPostRevert_Restore();
}
@end
