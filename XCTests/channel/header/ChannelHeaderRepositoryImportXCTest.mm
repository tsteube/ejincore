/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelHeaderRepositoryImportTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelHeaderRepositoryImportXCTest : AbstractXCTests
@end

@implementation ChannelHeaderRepositoryImportXCTest
{
  ChannelHeaderRepositoryImportTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelHeaderRepositoryImportTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testImportAddHeader {
  test->testImportAddHeader();
}
- (void)testImportAddHeaderWithResource {
  test->testImportAddHeaderWithResource();
}
- (void)testImportAddHeaderWithMember {
  test->testImportAddHeaderWithMember();
}
- (void)testImportUpdateHeader {
  test->testImportUpdateHeader();
}
- (void)testImportUpdateHeader_Identical {
  test->testImportUpdateHeader_Identical();
}
- (void)tetestImportUpdateHeader_Conflictst {
  test->testImportUpdateHeader_Conflict();
}
- (void)testImportAddResource {
  test->testImportAddResource();
}
- (void)testImportDeleteResource {
  test->testImportDeleteResource();
}
- (void)testImportAddMember {
  test->testImportAddMember();
}
- (void)testImportUpdateMember {
  test->testImportUpdateMember();
}
- (void)testImportDeleteMember {
  test->testImportDeleteMember();
}
- (void)testImportUpdateMember_Conflict {
  test->testImportUpdateMember_Conflict();
}
- (void)testImportUpdateMembershipOnly {
  test->testImportUpdateMembershipOnly();
}
- (void)testImportDeleteHeader {
  test->testImportDeleteHeader();
}
- (void)testImportDeleteHeaderWithResource {
  test->testImportDeleteHeaderWithResource();
}
- (void)testImportDeleteHeaderWithMember {
  test->testImportDeleteHeaderWithMember();
}
- (void)testestImportDeleteHeaderWithPostt {
  test->testImportDeleteHeaderWithPost();
}
- (void)testImportDeleteHeaderWithComment {
  test->testImportDeleteHeaderWithComment();
}
- (void)testImportDeleteHeader_Conflict {
  test->testImportDeleteHeader_Conflict();
}
- (void)testChannelHeaderMetadata {
  test->testChannelHeaderMetadata();
}
@end
