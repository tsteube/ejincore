/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelHeaderConflictTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelHeaderConflictXCTest : AbstractXCTests
@end

@implementation ChannelHeaderConflictXCTest
{
  ChannelHeaderConflictTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelHeaderConflictTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddChannelContent {
  test->testAddChannelContent();
}
- (void)testAddChannelContent_Empty {
  test->testAddChannelContent_Empty();
}
- (void)testUpdateChannelContent_LocalUnchanged {
  test->testUpdateChannelContent_LocalUnchanged();
}
- (void)testUpdateChannelContent_LocalAdd {
  test->testUpdateChannelContent_LocalAdd();
}
- (void)testUpdateChannelContent_LocalUpdate_EqualContent {
  test->testUpdateChannelContent_LocalUpdate_EqualContent();
}
- (void)testUpdateChannelContent_LocalUpdate {
  test->testUpdateChannelContent_LocalUpdate();
}
- (void)testUpdateChannelContent_LocalDelete_EqualContent {
  test->testUpdateChannelContent_LocalDelete_EqualContent();
}
- (void) testUpdateChannelContent_LocalDelete{
  test->testUpdateChannelContent_LocalDelete();
}
- (void)testDeleteChannelContent_LocalUnchanged {
  test->testDeleteChannelContent_LocalUnchanged();
}
- (void)testDeleteChannelContent_LocalAdd {
  test->testDeleteChannelContent_LocalAdd();
}
- (void)testDeleteChannelContent_LocalUpdate {
  test->testDeleteChannelContent_LocalUpdate();
}
- (void)testDeleteChannelContent_LocalDelete {
  test->testDeleteChannelContent_LocalDelete();
}
 - (void)testDeleteChannelContent_LocalDelete_Conflict {
   test->testDeleteChannelContent_LocalDelete_Conflict();
 }
 - (void)testForceClearConflict {
   test->testForceClearConflict();
 }
@end

