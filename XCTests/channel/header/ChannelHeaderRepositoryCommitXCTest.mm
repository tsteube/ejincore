/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelHeaderRepositoryCommitTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelHeaderRepositoryCommitXCTest : AbstractXCTests
@end

@implementation ChannelHeaderRepositoryCommitXCTest
{
  ChannelHeaderRepositoryCommitTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelHeaderRepositoryCommitTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testCommitAddedHeader {
  test->testCommitAddedHeader();
}
- (void)testCommitAddedHeaderWithResource {
  test->testCommitAddedHeaderWithResource();
}
- (void)testCommitAddedHeaderWithMember {
  test->testCommitAddedHeaderWithMember();
}
- (void)testCommitUpdatedHeader {
  test->testCommitUpdatedHeader();
}
- (void)tetestCommitAddedResourcest {
  test->testCommitAddedResource();
}
- (void)tetestCommitDeletedResourcest {
  test->testCommitDeletedResource();
}
- (void)testCommitAddedMember {
  test->testCommitAddedMember();
}
- (void)testCommitUpdatedMember {
  test->testCommitUpdatedMember();
}
/*
- (void)testCommitDeletedMember {
  test->testCommitDeletedMember();
}
*/
- (void)testCommitDeletedHeader {
  test->testCommitDeletedHeader();
}
- (void)testCommitDeletedHeaderWithResource {
  test->testCommitDeletedHeaderWithResource();
}
/*
- (void)testCommitDeletedHeaderWithMember {
  test->testCommitDeletedHeaderWithMember();
}
*/
- (void)testCommitDeletedHeaderWithPost {
  test->testCommitDeletedHeaderWithPost();
}
- (void)testCommitDeletedHeaderWithComment {
  test->testCommitDeletedHeaderWithComment();
}
- (void)testModificationMask {
  test->testModificationMask();
}
@end
