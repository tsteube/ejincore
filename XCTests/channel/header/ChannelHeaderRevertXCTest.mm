/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelHeaderRevertTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelHeaderRevertXCTest : AbstractXCTests
@end

@implementation ChannelHeaderRevertXCTest
{
  ChannelHeaderRevertTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelHeaderRevertTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testChannelHeaderRevert_Empty {
  test->testChannelHeaderRevert_Empty();
}
- (void)testChannelHeaderRevert_AddMember {
  test->testChannelHeaderRevert_AddMember();
}
- (void)testChannelHeaderRevert_AddMedia {
  test->testChannelHeaderRevert_AddMedia();
}
- (void)testChannelHeaderRevert_AddPost {
  test->testChannelHeaderRevert_AddPost();
}
- (void)testChannelHeaderRevert_AddComment {
  test->testChannelHeaderRevert_AddComment();
}
- (void)testChannelHeaderRevert_Update {
  test->testChannelHeaderRevert_Update();
}
- (void)testChannelHeaderRevert_Delete {
  test->testChannelHeaderRevert_Delete();
}
- (void)testChannelHeaderRevert_Restore {
  test->testChannelHeaderRevert_Restore();
}
@end
