/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelCommentConflictTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelCommentConflictXCTest : AbstractXCTests
@end

@implementation ChannelCommentConflictXCTest
{
  ChannelCommentConflictTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelCommentConflictTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddChannelComment {
  test->testAddChannelComment();
}
- (void)testAddChannelComment_Empty {
  test->testAddChannelComment_Empty();
}
- (void)testUpdateChannelComment_LocalUnchanged {
  test->testUpdateChannelComment_LocalUnchanged();
}
- (void)testUpdateChannelComment_LocalAdd {
  test->testUpdateChannelComment_LocalAdd();
}
- (void)testUpdateChannelComment_LocalUpdate_EqualContent {
  test->testUpdateChannelComment_LocalUpdate_EqualContent();
}
- (void)testUpdateChannelComment_LocalUpdate {
  test->testUpdateChannelComment_LocalUpdate();
}
- (void)testUpdateChannelComment_LocalDelete_EqualContent {
  test->testUpdateChannelComment_LocalDelete_EqualContent();
}
- (void)testUpdateChannelComment_LocalDelete {
  test->testUpdateChannelComment_LocalDelete();
}
- (void)testDeleteChannelComment_LocalUnchanged {
  test->testDeleteChannelComment_LocalUnchanged();
}
- (void)testDeleteChannelComment_LocalAdd {
  test->testDeleteChannelComment_LocalAdd();
}
- (void)testDeleteChannelComment_LocalUpdate {
  test->testDeleteChannelComment_LocalUpdate();
}
- (void)testDeleteChannelComment_LocalDelete {
  test->testDeleteChannelComment_LocalDelete();
}
- (void)testDeleteChannelComment_LocalDelete_Conflict {
  test->testDeleteChannelComment_LocalDelete_Conflict();
}
- (void)testForceClearConflict {
  test->testForceClearConflict();
}
@end

