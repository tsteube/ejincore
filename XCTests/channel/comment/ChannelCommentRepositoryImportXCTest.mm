/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelCommentRepositoryImportTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelCommentRepositoryImportXCTest : AbstractXCTests
@end

@implementation ChannelCommentRepositoryImportXCTest
{
  ChannelCommentRepositoryImportTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelCommentRepositoryImportTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testImportAddHeaderComment {
  test->testImportAddHeaderComment();
}
- (void)testImportAddPostComment {
  test->testImportAddPostComment();
}
- (void)testImportAddPostCommentWithResource {
  test->testImportAddPostCommentWithResource();
}
- (void)testImportUpdatePostComment {
  test->testImportUpdatePostComment();
}
- (void)testImportUpdatePostComment_Identical {
  test->testImportUpdatePostComment_Identical();
}
- (void)testImportUpdatePostComment_Conflict {
  test->testImportUpdatePostComment_Conflict();
}
- (void)testImportUpdatePostCommentAddResource {
  test->testImportUpdatePostCommentAddResource();
}
- (void)testImportUpdatePostCommentDeleteResource {
  test->testImportUpdatePostCommentDeleteResource();
}
- (void)testImportDeletePostComment {
  test->testImportDeletePostComment();
}
- (void)testImportDeleteHeaderComment {
  test->testImportDeleteHeaderComment();
}
- (void)testestImportDeletePostCommentWithResourcet {
  test->testImportDeletePostCommentWithResource();
}
- (void)testImportDeletePostComment_Conflict {
  test->testImportDeletePostComment_Conflict();
}
- (void)testChannelCommentMetadata {
  test->testChannelCommentMetadata();
}
@end
