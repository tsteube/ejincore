/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelCommentRepositoryCommitTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelCommentRepositoryCommitXCTest : AbstractXCTests
@end

@implementation ChannelCommentRepositoryCommitXCTest
{
  ChannelCommentRepositoryCommitTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelCommentRepositoryCommitTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testCommitAddedPostComment {
  test->testCommitAddedPostComment();
}
- (void)testCommitAddedHeaderComment {
  test->testCommitAddedHeaderComment();
}
- (void)testCommitAddedPostCommentWithResource {
  test->testCommitAddedPostCommentWithResource();
}
- (void)testCommitUpdatedPostComment {
  test->testCommitUpdatedPostComment();
}
- (void)testCommitUpdatedPostCommentAddedResource {
  test->testCommitUpdatedPostCommentAddedResource();
}
- (void)testCommitUpdatedPostCommentDeletedResource {
  test->testCommitUpdatedPostCommentDeletedResource();
}
- (void)testCommitDeletedPostComment {
  test->testCommitDeletedPostComment();
}
- (void)testCommitDeletedPostCommentWithResource {
  test->testCommitDeletedPostCommentWithResource();
}
- (void)testCommitDeletedChannelComment {
  test->testCommitDeletedChannelComment();
}
- (void)testModificationMask {
  test->testModificationMask();
}
@end

