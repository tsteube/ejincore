/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#import "AbstractXCTest.h"
#import "ChannelHeaderProfileTest.h"
#import "ChangeLogRepository.h"
#import "ChannelMemberRepository.h"
#import "ChannelMediaRepository.h"
#import "ChannelHeaderRepository.h"
#import "ChannelPostRepository.h"
#import "ChannelCommentRepository.h"
#import "ChannelService.h"

@interface ChannelHeaderProfileXCTest : AbstractXCTests
@end

@implementation ChannelHeaderProfileXCTest
{
  ChannelHeaderProfileTest* test;
}
- (void)setUp {
  [super setUp];
  test = new ChannelHeaderProfileTest();
  test->setUp();
}
- (void)tearDown {
  test->tearDown();
  [super tearDown];
}
// ------------- T E S T S -----------
- (void)testAddedChannelProfile {
  test->testAddedChannelProfile();
}
- (void)testModifiedChannelProfile {
  test->testModifiedChannelProfile();
}
- (void)testRemovedChannelProfile {
  test->testRemovedChannelProfile();
}
- (void)testConflictedChannelProfile {
  test->testConflictedChannelProfile();
}
- (void)testOutDatedChannelProfile {
  test->testOutDatedChannelProfile();
}
- (void)testChannelInvitations {
  test->testChannelInvitations();
}
- (void)testChannelToAccept {
  test->testChannelToAccept();
}
- (void)testChannelToReject {
  test->testChannelToReject();
}
- (void)testChannelMembership {
  test->testChannelMembership();
}
@end
