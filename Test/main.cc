/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <iostream>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main( int argc, char* argv[] )
{
  
#ifdef _LIBCPP_VERSION
  std::cout << "Using libc++\n";
#else
  std::cout << "Using libstdc++\n";
#endif
#ifdef __GXX_EXPERIMENTAL_CXX0X__
#if __cplusplus == 1
  std::cout << "Language mode = gnu++11\n";
#else
  std::cout << "Language mode = c++11\n";
#endif
#else
#if __cplusplus == 1
  std::cout << "Language mode = gnu++98\n";
#else
  std::cout << "Language mode = c++98\n";
#endif
#endif

  //example3();
  //exit(0);
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;
  
  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );        
  
  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );      
  
  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );
  
  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
  outputter.write(); 
  
  return result.wasSuccessful() ? 0 : 1;
}
