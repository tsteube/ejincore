/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __DB_UNIT_H__
#define __DB_UNIT_H__

#include "UsingEjinTypes.h"
#include "EjinDatabase.h"

using sqlite::BaseEntity;

#define __EJIN_SCHEMA_VERSION 25
#define __EJIN_TEST_MAIN_DB_NAME "ejin.db"
#define __EJIN_TEST_ATTACHMENT_DB_NAME "attachment.db"

extern std::string TestResourcePath;

class DBUnit {
  friend class SqlUnitTest;
  friend class CppWrapperTest;
  
public:
  DBUnit( ejin::Database& db ): _db(db) {}
  int setupTables( const string& srcDir, bool clean );
  
private:
  int insertRecords( const char dir[], const string& tablename );
  list<const Attribute*> insertEntity( const BaseEntity& prototype, string header );
  void insertEntity( const BaseEntity& prototype, list<const Attribute*> attributes, string line );
  
private:
  ejin::Database& _db;
  
};  


#endif // __DB_UNIT_H__
