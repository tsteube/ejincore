/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "I18NTest.h"

/* -DU_DISABLE_RENAMING=1 */
//#define U_DISABLE_RENAMING 1

#include <iostream>       // std::cout
#include <locale>         // std::locale
#include <stdexcept>      // std::runtime_error
#include <clocale>

#include <icu/unicode/resbund.h>
#include <icu/unicode/msgfmt.h>
#include <icu/unicode/gregocal.h>
#include <icu/unicode/unistr.h>
#include <icu/unicode/ustream.h>
#include <icu/unicode/smpdtfmt.h>
#include <icu/unicode/simpletz.h>
#include <icu/unicode/dtfmtsym.h>
#include <icu/unicode/listformatter.h>

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif

#include <TemplateResources/LocalizationManager.h>


CPPUNIT_TEST_SUITE_REGISTRATION( I18NTest );

void I18NTest::setUp( void )
{
  std::setlocale(LC_ALL, "en_US.UTF-8"); // for C and C++ where synced with stdio
  std::locale::global(std::locale("en_US.UTF-8")); // for C++
  std::cout.imbue(std::locale());
  std::locale loc; // initialized to locale::classic()
  //std::cout << "The selected locale is: " << loc.name() << '\n';

}
void I18NTest::tearDown( void )
{
}

// http://icu-project.org/userguide/ResourceManagement.html
void I18NTest::testResourceBundle( void )
{
  // -----
  // Load resource bundle
  UErrorCode status = U_ZERO_ERROR;
  ResourceBundle myResource( TestResourcePath + "messages" , "es", status);
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  UnicodeString s(myResource.getStringEx( "hello", status));
  CPPUNIT_ASSERT_EQUAL( UnicodeString("Hola"), s );
}

// http://formatjs.io/guides/message-syntax/
void I18NTest::testFormatting( void )
{
  UErrorCode status = U_ZERO_ERROR;
  Formattable args[4];
  UnicodeString message;

  // -----
  // Simple pattern
  args[0] = 2;
  MessageFormat::format("I have {0, number} cats.", args, 1, message, status );
  std::cout << "HHHHH " << status << std::endl;
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  CPPUNIT_ASSERT_EQUAL( UnicodeString("I have 2 cats."), message );
  message.remove();

  args[0] = 0.2;
  MessageFormat::format("Almost {0, number, percent} of them are black.", args, 1, message, status );
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  //std::cout << message << std::endl;
  CPPUNIT_ASSERT_EQUAL( UnicodeString("Almost 20% of them are black."), message );
  message.remove();

  GregorianCalendar cal(TimeZone::createDefault(), status);
  args[0] = Formattable( cal.getTime(status), Formattable::kIsDate);
  MessageFormat::format("Sale begins {0, date, medium} at {0, time, short}", args, 1, message, status );
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  //std::cout << message << std::endl;
  message.remove();
  
  // http://www.icu-project.org/apiref/icu4c/classMessageFormat.html
  unsigned long milliseconds_since_epoch =
  std::chrono::system_clock::now().time_since_epoch() /
  std::chrono::milliseconds(1);
  args[0] = Formattable( milliseconds_since_epoch, Formattable::kIsDate );
  MessageFormat::format("Sale begins {0, date, medium} at {0, time, medium}: {0, time, hh:mm:ss a} ", args, 1, message, status );
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  //std::cout << message << std::endl;
  message.remove();
  
  // Format the current time.
  // http://userguide.icu-project.org/formatparse/datetime
  // http://www.icu-project.org/apiref/icu4c/classSimpleDateFormat.html
  SimpleDateFormat formatter( UnicodeString("yyyy.MM.dd G 'at' hh:mm:ss a ZZZ"), status );
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  UDate currentTime_1 = cal.getTime(status);
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  FieldPosition fp(FieldPosition::DONT_CARE);
  UnicodeString dateString;
  formatter.format( currentTime_1, dateString, fp );
  //std::cout << "result: " << dateString << std::endl;

  // -----
  // Plural pattern
  args[0] = 5;
  args[1] = "in total";
  MessageFormat::format("Found {0,plural, one{# file} =2{2 files} other{# files {1}}}.",
                        args, 2, message, status );
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  CPPUNIT_ASSERT_EQUAL( UnicodeString("Found 5 files in total."), message );
  message.remove();

  // -----
  // Select pattern
  args[0] = "male";
  args[1] = "He";
  MessageFormat::format("{0, select, male {He} female {She} other {They} } will respond shortly.",
                        args , 2, message, status );
  CPPUNIT_ASSERT( U_SUCCESS(status) );
  CPPUNIT_ASSERT_EQUAL( UnicodeString("He will respond shortly."), message );
  message.remove();
  
  // -----
  // List pattern
  
  //http://userguide.icu-project.org/formatparse/messages
  //http://icu-project.org/apiref/icu4c/classicu_1_1ListFormatter.html
}

