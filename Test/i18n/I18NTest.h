/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __I18N_TEST
#define __I18N_TEST

#include "UsingEjinTypes.h"
#include "DBUnit.h"

#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/extensions/HelperMacros.h>

/*
 * SQL unit tests
 */
class I18NTest : public CPPUNIT_NS::TestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( I18NTest );
  CPPUNIT_TEST( testResourceBundle );
  CPPUNIT_TEST( testFormatting );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test setup
  // ==========
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Test methods
  // ==========
protected:
  
  void testFormatting( void );
  void testResourceBundle( void );
  
  // ==========
  // Private Interface
  // ==========
private:
  
};

#endif // __I18N_TEST_H__
