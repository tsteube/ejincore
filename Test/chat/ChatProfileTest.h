/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEMBER_PROFILE_UNIT_TEST_H__
#define __MEMBER_PROFILE_UNIT_TEST_H__

#include "AbstractChatTestFixture.h"

/*
 * Ejin unit tests
 */
class ChatProfileTest : public AbstractChatTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChatProfileTest );  
  CPPUNIT_TEST( testFindLastMessageNoInChat );
  CPPUNIT_TEST( testFindNewMessages );
  CPPUNIT_TEST( testFindAllMessagesInChat );
  CPPUNIT_TEST( testFindMessagesInChatSince );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testFindLastMessageNoInChat( void );
  void testFindNewMessages( void );
  void testFindAllMessagesInChat( void );
  void testFindMessagesInChatSince( void );
  
  // ==========
  // Private Interface
  // ==========
private:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "chat"; }
  
};

#endif // __MEMBER_PROFILE_UNIT_TEST_H__
