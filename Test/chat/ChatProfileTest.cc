/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatProfileTest.h"

#include "Chat.h"
#include "Message.h"
#include "ChangeLogRepository.h"

using ejin::Chat;
using ejin::Message;

CPPUNIT_TEST_SUITE_REGISTRATION( ChatProfileTest );

// ==========
// TEST Methods
// ==========

void ChatProfileTest::testFindLastMessageNoInChat( void )
{
  shared_ptr<Chat> chat( this->chatService_->loadChatByPartner( "member1" ));
  CPPUNIT_ASSERT( chat );
  CPPUNIT_ASSERT( chat->getId() == 1 );
  CPPUNIT_ASSERT( this->chatService_->lastMessageNoIn( *chat ) == 3 );
}
void ChatProfileTest::testFindNewMessages( void )
{
  shared_ptr<Chat> chat( this->chatService_->loadChatByPartner( "member4" ));
  CPPUNIT_ASSERT( chat );
  
  list< shared_ptr<Message> > messages(this->chatService_->findNewMessages( *chat ));
  CPPUNIT_ASSERT( messages.size() == 1 );
  CPPUNIT_ASSERT( (*messages.begin())->getId() == 6 );
}
void ChatProfileTest::testFindAllMessagesInChat( void )
{
  shared_ptr<Chat> chat( this->chatService_->loadChatByPartner( "member1" ));
  CPPUNIT_ASSERT( chat );
  CPPUNIT_ASSERT( chat->getId() == 1 );
  
  list< shared_ptr<Message> > messages(this->chatService_->findAllMessages( *chat ));
  CPPUNIT_ASSERT( messages.size() == 3 );
}
void ChatProfileTest::testFindMessagesInChatSince( void )
{
  shared_ptr<Chat> chat( this->chatService_->loadChatByGid( "1" ));
  CPPUNIT_ASSERT( chat );
  CPPUNIT_ASSERT( chat->getId() == 1 );
  
  list< shared_ptr<Message> > messages(this->chatService_->findMessagesSince( *chat, 2 ));
  CPPUNIT_ASSERT( messages.size() == 2 );
}



