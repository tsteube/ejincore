/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatServiceTest.h"

#include "Chat.h"
#include "Message.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "SyncSource.h"
#include "Modifications.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"

using ejin::Chat;
using ejin::Message;

CPPUNIT_TEST_SUITE_REGISTRATION( ChatServiceTest );

// ==========
// TEST Methods
// ==========

void ChatServiceTest::testExportNewMessage( void )
{
  shared_ptr<Chat> chat( shared_ptr<Chat>(new Chat()) );
  chat->setId( 4 );
  this->db_->refresh( *chat );
  chat->setGid( "4" );
  CPPUNIT_ASSERT( chat );
  
  list< shared_ptr<Message> > container;
  this->chatService_->exportNewMessage( *chat, container );
  
  CPPUNIT_ASSERT( container.size() == 1 );
}
void ChatServiceTest::testImportChats( void )
{
  // preparation
  Modifications<Chat> container;
  container.setSyncAnchor( string( "10" ) );
  
  // add
  shared_ptr<Chat> add(shared_ptr<Chat>(new Chat()));
  add->setMessageCount( 0 );
  add->setAcknowledged( 0 );
  add->setGid( "99" );
  add->setPartner( "member5" );
  add->setPartnerAcknowledged( 0 );
  container.getAdded().push_back( add );
  
  // update
  shared_ptr<Chat> update(shared_ptr<Chat>(new Chat()));
  update->setMessageCount( 5 );
  update->setAcknowledged( 4 );
  update->setGid( "1" );
  update->setPartner( "member1" );
  update->setPartnerAcknowledged( 5 );
  container.getUpdated().push_back( update );
  
  // delete
  shared_ptr<Chat> del(shared_ptr<Chat>(new Chat()));
  del->setGid( "3" );
  del->setPartner( "member3" );
  container.getRemoved().push_back( del );
  
  // TEST
  CPPUNIT_ASSERT( this->chatService_->importUpdatedChat( container, "100", jtime() ) );
  
  // post conditions
  Chat chat( 1 );
  this->db_->refresh( chat );
  CPPUNIT_ASSERT_EQUAL( (integer)4, chat.getAcknowledged() );
  
  try {
    chat.setId( 3 );
    this->db_->refresh( chat );
    CPPUNIT_FAIL( "" );
  } catch (ejin::data_access_error& e) {
  }
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChat, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member1") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChat, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member5") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChatServiceTest::testImportMessages( void )
{
  Chat chat( 1 );
  this->db_->refresh( chat );
  
  // ====
  // TEST add message
  {
    list< shared_ptr<Message> > messages;
    
    shared_ptr<Message> message(shared_ptr<Message>(new Message()));
    message->setAcknowledged( true );
    message->setNo( 4 );
    message->setContent( "message" );
    message->setSender( "owner" );
    message->setEncrypted( false );
    message->setSecure( false );
    message->clearSendTime( );
    messages.push_back( message );
    
    CPPUNIT_ASSERT_EQUAL( (integer)4, this->chatService_->importUpdatedMessage( chat, messages, "100", jtime() ) );
    CPPUNIT_ASSERT_EQUAL( (size_t)4, this->chatService_->findAllMessages( chat ).size() );
    
    // ----
    // TEST CHANGE LOG
    CPPUNIT_ASSERT( this->changeLogRepository_->end() );
    
    list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
    CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
    shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
    CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
    CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChatMessage, (ejin::ChangeLogSource)changeLog->getSource() );
    CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
    CPPUNIT_ASSERT( changeLog->hasResourceId1() );
    CPPUNIT_ASSERT( changeLog->hasResourceId2() );
    CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  }

  // ====
  // TEST update message
  {
    list< shared_ptr<Message> > messages;
    
    shared_ptr<Message> message(shared_ptr<Message>(new Message()));
    message->setAcknowledged( true );
    message->setNo( 4 );
    message->setContent( "test" );
    message->setSender( "owner" );
    message->setEncrypted( false );
    message->setSecure( false );
    message->clearSendTime( );
    messages.push_back( message );
    
    CPPUNIT_ASSERT_EQUAL( (integer)4, this->chatService_->importUpdatedMessage( chat, messages, "101", jtime() ) );
    
    Message msg( 101 );
    this->db_->refresh( msg );
    CPPUNIT_ASSERT_EQUAL( string("test"), msg.getContent() );
    
    // ----
    // TEST CHANGE LOG
    CPPUNIT_ASSERT( this->changeLogRepository_->end() );
    
    list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "101" ) );
    CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
    shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
    CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
    CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChatMessage, (ejin::ChangeLogSource)changeLog->getSource() );
    CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
    CPPUNIT_ASSERT( changeLog->hasResourceId1() );
    CPPUNIT_ASSERT( changeLog->hasResourceId2() );
    CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  }
  
  // ====
  // TEST delete chat
  {
    list< shared_ptr<Message> > messages;
    
    shared_ptr<Message> message(shared_ptr<Message>(new Message()));
    message->setNo( 4 );
    message->setEmpty( true );
    messages.push_back( message );
    
    // do not delete first message in database on import
    CPPUNIT_ASSERT_EQUAL( (integer)4, this->chatService_->importUpdatedMessage( chat, messages, "102", jtime() ) );
    CPPUNIT_ASSERT_EQUAL( (size_t)3, this->chatService_->findAllMessages( chat ).size() );

    // ----
    // TEST CHANGE LOG
    CPPUNIT_ASSERT( this->changeLogRepository_->end() );
    
    list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "102" ) );
    CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
    shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
    CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
    CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChatMessage, (ejin::ChangeLogSource)changeLog->getSource() );
    CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
    CPPUNIT_ASSERT( changeLog->hasResourceId1() );
    CPPUNIT_ASSERT( changeLog->hasResourceId2() );
    CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  }

}
void ChatServiceTest::testExportChat( void )
{
  // pre conditions
  shared_ptr<Chat> chat( this->chatService_->exportChat( 1L ) );
  CPPUNIT_ASSERT_EQUAL( (size_t)3, chat->getMessages().size() );
  
  shared_ptr<Message> message( *chat->getMessages().begin() );
  CPPUNIT_ASSERT( message->isSend() );
  CPPUNIT_ASSERT( message->isDelivered() );
  CPPUNIT_ASSERT( message->isMyMessage() );
}

