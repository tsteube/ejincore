/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatRepositoryTest.h"

#include "Chat.h"
#include "Message.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"

using ejin::Chat;
using ejin::Message;

CPPUNIT_TEST_SUITE_REGISTRATION( ChatRepositoryTest );

// ==========
// TEST Methods
// ==========

void ChatRepositoryTest::testLoadChatByUnknownGid( void )
{
  try {
    this->chatService_->loadChatByGid( "99" );
  } catch (ejin::data_retrieval_error e) {
  }
}
void ChatRepositoryTest::testLoadChatByUnknownPartner( void )
{
  try {
    this->chatService_->loadChatByPartner( "99" );
  } catch (ejin::data_retrieval_error e) {
  }
}
void ChatRepositoryTest::testLoadNewChat( void )
{
  shared_ptr<Chat> chat( this->chatService_->loadChatByPartner2( "member4" ));
  CPPUNIT_ASSERT( chat );
  CPPUNIT_ASSERT( chat->hasId() );
  CPPUNIT_ASSERT( chat->getPartner() == "member4" );
  
}

void ChatRepositoryTest::testImportAddChat( void )
{
  ejin::Chat chat;
  chat.setMessageCount( 0 );
  chat.setAcknowledged( 0 );
  chat.setGid( "99" );
  chat.setPartner( "Paul" );
  chat.setPartnerAcknowledged( 0 );

  // ----
  // TEST
  shared_ptr<Chat> chatEntity( this->chatService_->update( chat, "100", jtime() ));
  
  CPPUNIT_ASSERT( chatEntity );
  CPPUNIT_ASSERT( chatEntity->hasId() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChat, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "Paul" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChatRepositoryTest::testImportAddChat_Reuse( void )
{
  ejin::Chat chat;
  chat.setMessageCount( 1 );
  chat.setAcknowledged( 0 );
  chat.setGid( "99" );
  chat.setPartner( "member2" );
  chat.setPartnerAcknowledged( 0 );
  
  // ----
  // TEST: shift message into the past
  shared_ptr<Chat> chatEntity( this->chatService_->update( chat, "100", jtime() ));

  CPPUNIT_ASSERT( chatEntity );
  CPPUNIT_ASSERT_EQUAL( (integer)2, chatEntity->getId() );
  CPPUNIT_ASSERT_EQUAL( string("99"), chatEntity->getGid() );
  CPPUNIT_ASSERT_EQUAL( -1, this->chatService_->lastMessageNoIn( *chatEntity ) );
  Value chatId( chatEntity->getId() );
  unique_ptr<Value> result( db_->valueOf( Message::TABLE_NAME, "messageCount", &chatId, NULL ) );
  CPPUNIT_ASSERT_EQUAL( (integer)2, result->num() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChat, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member2" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChatRepositoryTest::testImportUpdateChat( void )
{
  ejin::Chat chat;
  chat.setMessageCount( 5 );
  chat.setAcknowledged( 4 );
  chat.setGid( "1" );
  chat.setPartnerAcknowledged( 5 );
  
  // ----
  // TEST
  shared_ptr<Chat> chatEntity( this->chatService_->update( chat, "100", jtime() ));
  
  CPPUNIT_ASSERT( chatEntity );
  CPPUNIT_ASSERT( chatEntity->getId() == 1 );
  CPPUNIT_ASSERT( chatEntity->getMessageCount() == 5 );
  CPPUNIT_ASSERT( chatEntity->getAcknowledged() == 4 );
  CPPUNIT_ASSERT( chatEntity->getPartnerAcknowledged() == 5 );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChat, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member1" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChatRepositoryTest::testImportDeleteChat_KeepMessages( void )
{
  ejin::Chat chat;
  chat.setGid( "2" );
  chat.setEmpty( true );
  
  // ----
  // TEST
  shared_ptr<Chat> chatEntity( this->chatService_->update( chat, "100", jtime() ));
  
  // post conditions
  CPPUNIT_ASSERT( chatEntity );
  CPPUNIT_ASSERT( !chatEntity->hasGid() );
  CPPUNIT_ASSERT_EQUAL( -1, this->chatService_->lastMessageNoIn( *chatEntity ));
  Value chatId( chatEntity->getId() );
  unique_ptr<Value> result( db_->valueOf( ejin::Message::TABLE_NAME, "messageCount", &chatId, NULL ) );
  CPPUNIT_ASSERT_EQUAL( (integer)2, result->num() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}
void ChatRepositoryTest::testImportDeleteChat( void )
{
  ejin::Chat chat;
  chat.setGid( "3" );
  chat.setEmpty( true );
  
  // ----
  // TEST
  shared_ptr<Chat> chatEntity( this->chatService_->update( chat, "100", jtime() ));
  
  try {
    this->chatService_->loadChatByGid( "3" );
    CPPUNIT_FAIL( "" );
  } catch (ejin::data_retrieval_error e) {
  }
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}
void ChatRepositoryTest::testCommitChat( void )
{
  shared_ptr<Chat> chat( shared_ptr<Chat>(new Chat()) );
  chat->setId( 4 );
  this->db_->refresh( *chat );
  CPPUNIT_ASSERT( chat );
  chat->setGid( "4" );

  this->chatService_->commitChat( *chat, "100", jtime() );

  this->db_->refresh( *chat );
  chat->setGid( "4" );
  CPPUNIT_ASSERT( chat->getGid() == "4" );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChat, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member4" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
