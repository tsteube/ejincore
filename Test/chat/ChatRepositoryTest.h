/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHAT_REPOSITORY_UNIT_TEST_H__
#define __CHAT_REPOSITORY_UNIT_TEST_H__

#include "AbstractChatTestFixture.h"

/*
 * Ejin unit tests
 */
class ChatRepositoryTest : public AbstractChatTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChatRepositoryTest );
  CPPUNIT_TEST( testLoadChatByUnknownGid );
  CPPUNIT_TEST( testLoadChatByUnknownPartner );
  CPPUNIT_TEST( testLoadNewChat );
  CPPUNIT_TEST( testImportAddChat );
  CPPUNIT_TEST( testImportAddChat_Reuse );
  CPPUNIT_TEST( testImportUpdateChat );
  CPPUNIT_TEST( testImportDeleteChat_KeepMessages );
  CPPUNIT_TEST( testImportDeleteChat );
  CPPUNIT_TEST( testCommitChat );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testLoadChatByUnknownGid( void );
  void testLoadChatByUnknownPartner( void );
  void testLoadNewChat( void );
  void testImportAddChat( void );
  void testImportAddChat_Reuse( void );
  void testImportUpdateChat( void );
  void testImportDeleteChat_KeepMessages( void );
  void testImportDeleteChat( void );
  void testCommitChat( void );

protected:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "chat"; }

};

#endif // __CHAT_REPOSITORY_UNIT_TEST_H__
