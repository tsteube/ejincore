/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __ABSTRACT_CHAT_TEST_FIXTURE_H__
#define __ABSTRACT_CHAT_TEST_FIXTURE_H__

#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/extensions/HelperMacros.h>

#include "UsingEjinTypes.h"
#include "EjinDatabase.h"
#include "DBUnit.h"

#include "MemberProfile.h"
#include "ApprovedMemberRepository.h"
#include "ChatService.h"

// forward definition
class DBUnit;

/**
 * Abstract test setup for member unit tests
 */
class AbstractChatTestFixture : public CPPUNIT_NS::TestFixture
{
  
  // ==========
  // Test Setup / Teardown
  // ==========
public:
  
  virtual void setUp( void );
  virtual void tearDown( void );
  
  // ==========
  // Protected Interface
  // ==========
protected:
  
  virtual const string getDbConfigDirectory() = 0;
  
  unique_ptr<ejin::Database>           db_;
  unique_ptr<DBUnit>                   dbunit_;
  unique_ptr<ChangeLogRepository>      changeLogRepository_;
  unique_ptr<ejin::ChatService>        chatService_;
  unique_ptr<ejin::MemberProfile>      memberProfile_;
  unique_ptr<ApprovedMemberRepository> approvedMemberRepository_;

  struct timeval                     tval_start_;
  struct timeval                     tval_before_;
};

#endif // __ABSTRACT_CHAT_TEST_FIXTURE_H__
