/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __UNIT_CIPHER_H__
#define __UNIT_CIPHER_H__

#include "UsingEjinTypes.h"

#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/extensions/HelperMacros.h>
#include "EjinProperties.h"
#include "DBUnit.h"

// forward declaration
namespace ejin {
  class RSACrypto;
}

using ejin::RSACrypto;
//using std::string;
using ejin::EjinProperties;

/*
 * Cipher unit tests
 */
class RSACryptoTest : public CPPUNIT_NS::TestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( RSACryptoTest );
  CPPUNIT_TEST( testBase64 );
  CPPUNIT_TEST( testRSAGenerationToFolder );
  CPPUNIT_TEST( testRSAGenerationToProtectedFolder );
  CPPUNIT_TEST( testLoadingKeysPEM );
  CPPUNIT_TEST( testLoadingKeysPEMWithPassword );
  CPPUNIT_TEST( testLoadingKeysASN1 );
  CPPUNIT_TEST( testTransferSessionKey );
  CPPUNIT_TEST( testTransferMessage );
  CPPUNIT_TEST( testTransferOldMessage );
  CPPUNIT_TEST( testTransferFile );
  CPPUNIT_TEST( testFileIO );
  CPPUNIT_TEST( testAESInteroperability );
  CPPUNIT_TEST( testDecryptMessage );
  CPPUNIT_TEST( testDecryptOldMessage );
  CPPUNIT_TEST( testEncryptSessionKey );
  CPPUNIT_TEST( testMessageSignature );
  CPPUNIT_TEST( testAssignPrivateKey );
  CPPUNIT_TEST( testExportKeys );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test setup
  // ==========
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Test methods
  // ==========
  
  void testBase64( void );
  void testRSAGenerationToFolder( void );
  void testRSAGenerationToProtectedFolder( void );
  void testLoadingKeysPEM( void );
  void testLoadingKeysPEMWithPassword( void );
  void testLoadingKeysASN1( void );
  void testTransferSessionKey( void );
  void testTransferMessage( void );
  void testTransferOldMessage( void );
  void testTransferFile( void );
  void testFileIO( void );
  void testAESInteroperability( void );
  void testDecryptMessage( void );
  void testDecryptOldMessage( void );
  void testEncryptSessionKey( void );
  void testMessageSignature( void );
  void testAssignPrivateKey( void );
  void testExportKeys( void );

  // ==========
  // Private Interface
  // ==========
private:
  
  //ejin::RSACrypto* crypto_;
  clock_t        begin_time_;
  EjinProperties properties_;
  EjinProperties oldProperties_;
  
};

#endif // __UNIT_CIPHER_H__
