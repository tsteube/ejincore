/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "RSACryptoTest.h"

#include "RSACrypto.h"

#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <openssl/err.h>
#include <sys/stat.h>

// ================================================================
// Formated dump of a general type.
// ================================================================
template< typename T> void vdump(const std::string& fn,
                                 uint ln,
                                 const std::string& prefix,
                                 const T& d)
{
  std::cout << fn << ":" << ln << ": " << prefix << "\t" << d << std::endl;
}
// ================================================================
// Explicit template instantiation of the above for string
// types so that I can report the length.
// ================================================================
template<> void vdump<string>(const string& fn,
                              uint ln,
                              const string& prefix,
                              const string& d)
{
  std::cout << fn << ":" << ln << ": "
  << prefix << "\t"
  << std::left << std::setw(64) << d
  << " (" << d.size() << ")"
  << std::endl;
}
// ================================================================
// Dump for fixed sized types like salt and key.
// ================================================================
template<typename T> void tdump(const string& fn,
                                uint ln,
                                const string& prefix,
                                const T& d)
{
  std::cout << fn << ":" << ln << ": " << prefix << "\t";
  for(uint i=0;i<sizeof(T);++i) {
    // Prettified output.
    // I turned it off so that the format would match openssl.
    if ((i%16)==0) {
      if (i) {
        std::cout << std::endl;
        std::cout << "\t\t\t";
      }
      else {
        if (prefix.size()<4) {
          std::cout << "\t";
        }
        std::cout << "\t";
      }
    }
    else if (i) {
      std::cout << ", ";
    }
    std::cout << std::setw(2) << std::setfill('0') << std::hex << std::right << uint(d[i]) << std::dec << std::setfill(' ');
  }
  std::cout << " (" << sizeof(T) << ")" << std::endl;
}
// ================================================================
// Binary data dump.
// ================================================================
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
static void bdump(const string& fn,
                  uint ln,
                  const string& prefix,
                  const unsigned char* a,
                  size_t len)
{
  std::cout << fn << ":" << ln << ": " << prefix;
  for(uint i=0;i<len;++i) {
    if ((i%16)==0) {
      if (i) {
        std::cout << std::endl;
        std::cout << "\t\t\t";
      }
      else {
        std::cout << "\t\t";
      }
    }
    else if (i) {
      std::cout << ", ";
    }
    std::cout << std::setw(2) << std::hex << std::right << uint(a[i]) << std::dec;
  }
  std::cout << " (" << len << ")" << std::endl;
}
#pragma clang diagnostic pop

#define DBG_TDUMP(v)   tdump(__FILE__,__LINE__,#v,v)
#define DBG_PKV(v)     vdump(__FILE__,__LINE__,#v,v)
#define DBG_PKVR(k,v)  vdump(__FILE__,__LINE__,k,v)
#define DBG_BDUMP(a,x) bdump(__FILE__,__LINE__,#a,a,x)
#define DBG_MDUMP(a)   bdump(__FILE__,__LINE__,#a,(unsigned char*)a.c_str(),a.size())
#define PKV(v)         vdump(__FILE__,__LINE__,#v,v)
#define DBG_CDUMP(c)   bdump(__FILE__,__LINE__,#c,c.data(),c.size());
/*
static string loadFile( const char* filename )
{
  std::ifstream t(filename);
  std::string str;
  
  t.seekg(0, std::ios::end);
  str.reserve((size_t)t.tellg());
  t.seekg(0, std::ios::beg);
  
  str.assign((std::istreambuf_iterator<char>(t)),
             std::istreambuf_iterator<char>());
  return str;
}
static bool isFilesEqual(const std::string& lFilePath, const std::string& rFilePath)
{
  std::ifstream lFile(lFilePath.c_str(), std::ios::in | std::ios::binary);
  std::ifstream rFile(rFilePath.c_str(), std::ios::in | std::ios::binary);
  
  if(!lFile.good() || !rFile.good())
  {
    return false;
  }
  
  std::streamsize lReadBytesCount = 0;
  std::streamsize rReadBytesCount = 0;
  
  char *p_lBuffer = new char[BUFSIZ]();
  char *p_rBuffer = new char[BUFSIZ]();
  do {
    lFile.read(p_lBuffer, BUFSIZ);
    rFile.read(p_rBuffer, BUFSIZ);
    lReadBytesCount = lFile.gcount();
    rReadBytesCount = rFile.gcount();
    
    if (lReadBytesCount != rReadBytesCount || std::memcmp(p_lBuffer, p_rBuffer, lReadBytesCount) != 0)
    {
      return false;
    }
  } while (lFile.good() || rFile.good());
  
  delete[] p_lBuffer;
  delete[] p_rBuffer;
  return true;
}
*/
#define REPOSITORY_DIRECTORY (TestResourcePath + "attachment/")
#define GCM_TEST_ENCRYPTION_PROPERTIES (TestResourcePath + "crypto/aes-gcm-test-encryption.properties")
#define CBC_TEST_ENCRYPTION_PROPERTIES (TestResourcePath + "crypto/aes-cbc-test-encryption.properties")

CPPUNIT_TEST_SUITE_REGISTRATION( RSACryptoTest );

void RSACryptoTest::setUp( void )
{
  ejin::setupCryptoLib( );
  
  properties_.load( GCM_TEST_ENCRYPTION_PROPERTIES.c_str() );
  oldProperties_.load( CBC_TEST_ENCRYPTION_PROPERTIES.c_str() );
  begin_time_ = clock();
}
void RSACryptoTest::tearDown( void )
{
  rmdir( (TestResourcePath + "crypto_test").c_str() );
  std::cout << " " << float( clock () - begin_time_ ) /  CLOCKS_PER_SEC;
  ejin::teardownCryptoLib( );
}

void RSACryptoTest::testBase64( void )
{
  shared_ptr<RSACrypto> crypto(shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( NULL )));
  RSACrypto::Cipher random(crypto->randomBits( 80, "abc" ));
  CPPUNIT_ASSERT( random.size() == 10 );
  //DBG_CDUMP( random );
  RSACrypto::Cipher encoded;
  RSACrypto::Cipher decoded;
  CPPUNIT_ASSERT(crypto->encodeBase64( random, encoded ));
  CPPUNIT_ASSERT( crypto->decodeBase64( encoded.str(), decoded ));
  CPPUNIT_ASSERT( random == decoded );
}

void RSACryptoTest::testRSAGenerationToFolder( void )
{
  shared_ptr<RSACrypto> crypto(shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( (TestResourcePath + "crypto_test").c_str() )));
  CPPUNIT_ASSERT( crypto.get() ) ;
  CPPUNIT_ASSERT( ! crypto->getPublicKeyASN1().empty() );
  CPPUNIT_ASSERT( ! crypto->getPrivateKeyASN1().empty() );

  // reload
  shared_ptr<RSACrypto> crypto2( shared_ptr<RSACrypto>(RSACrypto::loadRSAKeyPair( (TestResourcePath + "crypto_test").c_str() )) );
  CPPUNIT_ASSERT( crypto2.get() ) ;
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyASN1(), crypto2->getPublicKeyASN1() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPrivateKeyASN1(), crypto2->getPrivateKeyASN1() );
  
  RSACrypto::clearRSAKeyPair( (TestResourcePath + "crypto_test").c_str() );
}
void RSACryptoTest::testRSAGenerationToProtectedFolder( void )
{
  shared_ptr<RSACrypto> crypto(shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( (TestResourcePath + "crypto_test").c_str(), "password" )));
  CPPUNIT_ASSERT( crypto.get() ) ;
  CPPUNIT_ASSERT( ! crypto->getPublicKeyASN1().empty() );
  CPPUNIT_ASSERT( ! crypto->getPrivateKeyASN1().empty() );
  
  // reload
  shared_ptr<RSACrypto> crypto2( shared_ptr<RSACrypto>(RSACrypto::loadRSAKeyPair( (TestResourcePath + "crypto_test").c_str(), "password" )) );
  CPPUNIT_ASSERT( crypto2.get() ) ;
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyASN1(), crypto2->getPublicKeyASN1() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPrivateKeyASN1(), crypto2->getPrivateKeyASN1() );
  
  RSACrypto::clearRSAKeyPair( (TestResourcePath + "crypto_test").c_str() );
}
void RSACryptoTest::testLoadingKeysPEM( void )
{
  shared_ptr<RSACrypto> crypto((shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  CPPUNIT_ASSERT( ! crypto->getPublicKeyPEM().empty() );
  CPPUNIT_ASSERT( ! crypto->getPrivateKeyPEM().empty() );

  // load public key only
  shared_ptr<RSACrypto> crypto2( shared_ptr<RSACrypto>(RSACrypto::fromPublicKeyPEM( crypto->getPublicKeyPEM() )) );
  CPPUNIT_ASSERT( crypto2.get() ) ;
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyPEM(), crypto2->getPublicKeyPEM() );
  CPPUNIT_ASSERT( ! crypto2->hasPrivateKeyPEM() );
  
  // load private key
  crypto2 = shared_ptr<RSACrypto>(RSACrypto::fromPrivateKeyPEM( crypto->getPrivateKeyPEM() ));
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyPEM(), crypto2->getPublicKeyPEM() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPrivateKeyPEM(), crypto2->getPrivateKeyPEM() );
}
void RSACryptoTest::testLoadingKeysPEMWithPassword( void )
{
  const char* password = "password";
  shared_ptr<RSACrypto> crypto((shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  CPPUNIT_ASSERT( ! crypto->getPublicKeyPEM().empty() );
  CPPUNIT_ASSERT( ! crypto->getPrivateKeyPEM().empty() );
  
  // load private key
  string encryptedKey( crypto->getPrivateKeyPEM( password ) );
  shared_ptr<RSACrypto> crypto2 = shared_ptr<RSACrypto>(RSACrypto::fromPrivateKeyPEM( encryptedKey, password ));
  CPPUNIT_ASSERT( crypto2.get() ) ;
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyPEM(), crypto2->getPublicKeyPEM() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPrivateKeyPEM(), crypto2->getPrivateKeyPEM() );
  CPPUNIT_ASSERT( crypto->getPrivateKeyPEM( password ) != crypto2->getPrivateKeyPEM( password ) );
}
void RSACryptoTest::testLoadingKeysASN1( void )
{
  shared_ptr<RSACrypto> crypto((shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  CPPUNIT_ASSERT( ! crypto->getPublicKeyASN1().empty() );
  CPPUNIT_ASSERT( ! crypto->getPrivateKeyASN1().empty() );

  // load public key only
  shared_ptr<RSACrypto> crypto2( shared_ptr<RSACrypto>(RSACrypto::fromPublicKeyASN1( crypto->getPublicKeyASN1() )) );
  CPPUNIT_ASSERT( crypto2.get() ) ;
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyASN1(), crypto2->getPublicKeyASN1() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyFingerPrint(), crypto2->getPublicKeyFingerPrint() );
  CPPUNIT_ASSERT( ! crypto2->hasPrivateKeyASN1() );
  
  // load private key
  crypto2 = shared_ptr<RSACrypto>(RSACrypto::fromPrivateKeyASN1( crypto->getPrivateKeyASN1() ));
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyASN1(), crypto2->getPublicKeyASN1() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPublicKeyFingerPrint(), crypto2->getPublicKeyFingerPrint() );
  CPPUNIT_ASSERT_EQUAL( crypto->getPrivateKeyASN1(), crypto2->getPrivateKeyASN1() );
}
void RSACryptoTest::testTransferSessionKey( void )
{
  shared_ptr<RSACrypto> crypto((shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  crypto->setRandomSessionKey( );
  crypto->setRandomSessionKey( );
  string encryptedKey( crypto->getEncryptedSessionKey( ) );
  
  shared_ptr<RSACrypto> crypto2( shared_ptr<RSACrypto>(RSACrypto::fromPrivateKeyASN1( crypto->getPrivateKeyASN1() )) );
  CPPUNIT_ASSERT( crypto2.get() ) ;
  crypto2->setEncryptedSessionKey( encryptedKey );
  
  CPPUNIT_ASSERT_EQUAL( crypto->getSessionKey(), crypto2->getSessionKey() );
}
void RSACryptoTest::testTransferMessage( void )
{
  shared_ptr<RSACrypto> crypto(( shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  shared_ptr<RSACrypto> crypto2(( shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( )) ));
  CPPUNIT_ASSERT( crypto2.get() ) ;
  
  crypto->setRandomSessionKey( );
  crypto2->setSessionKey( crypto->getSessionKey() );

  string input( "hello" );  
  string cipher( crypto->encrypt( input ) );
  string output( crypto2->decrypt( cipher ) );
  
  CPPUNIT_ASSERT_EQUAL( input, output );
}
void RSACryptoTest::testTransferOldMessage( void )
{
  shared_ptr<RSACrypto> crypto(( shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  shared_ptr<RSACrypto> crypto2(( shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( )) ));
  CPPUNIT_ASSERT( crypto2.get() ) ;
  
  string input1( "hello1" );
  string input2( "hello2" );
  crypto->setRandomSessionKey( );
  string cipher1( crypto->encrypt( input1 ) );
  crypto->setRandomSessionKey( );
  string cipher2( crypto->encrypt( input2 ) );

  crypto2->setSessionKey( crypto->getSessionKey() );
  string output1( crypto2->decrypt( cipher1 ) );
  string output2( crypto2->decrypt( cipher2 ) );
  
  CPPUNIT_ASSERT_EQUAL( input1, output1 );
  CPPUNIT_ASSERT_EQUAL( input2, output2 );
}
void RSACryptoTest::testAESInteroperability( void )
{
  ejin::AESCrypto crypto;
  
  crypto.setHmacSessionKeyFrom( "password" );
  string key1( crypto.getSessionKey() );
  crypto.clearSessionKey();
  crypto.setHmacSessionKeyFrom( "password" );
  string key2( crypto.getSessionKey() );
  CPPUNIT_ASSERT_EQUAL( key1, key2 );
  
  crypto.clearSessionKey();
  crypto.setHmacSessionKeyFrom( "test" );
  CPPUNIT_ASSERT_EQUAL( crypto.getSessionKey(), string("gYcaWbv0ix+jgDfG7mC1bhppMWRLZ+fBMO7kiHokDNI=") );
  
  string source( "hello" );
  string cipher( crypto.encrypt( source ) );
  string target( crypto.decrypt( cipher ) );
  CPPUNIT_ASSERT_EQUAL( source, target );

  target = crypto.decrypt( "AQfw3jkPAgZFui1NrW9KrbsEw35/tHUdhDW2wjemNZUKAQ==" );
  CPPUNIT_ASSERT_EQUAL( source, target );
}
void RSACryptoTest::testTransferFile( void )
{
  unique_ptr<AESCrypto> crypto((unique_ptr<AESCrypto>(new ejin::AESCrypto())));
  unique_ptr<AESCrypto> crypto2((unique_ptr<AESCrypto>(new ejin::AESCrypto())));
  
  crypto->setRandomSessionKey( );
  crypto2->setSessionKey( crypto->getSessionKey() );

  ::mkdir( (TestResourcePath + "crypto_test").c_str(), S_IRWXU );
  std::ofstream outputFile( TestResourcePath + "crypto_test/test.txt" );
  outputFile << "hello";
  outputFile.close();

  CPPUNIT_ASSERT( crypto->encryptFile(  (TestResourcePath + "crypto_test/test.txt").c_str(),
                                      (TestResourcePath + "crypto_test/test.enc").c_str() ) );
  CPPUNIT_ASSERT( crypto2->decryptFile(  (TestResourcePath + "crypto_test/test.enc").c_str(),
                                       (TestResourcePath + "crypto_test/test.dec").c_str() ) );

  std::ifstream file( TestResourcePath + "crypto_test/test.dec" );
  std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
  
  CPPUNIT_ASSERT_EQUAL( string("hello"), content );
}
void RSACryptoTest::testFileIO( void )
{
  unique_ptr<AESCrypto> crypto(( unique_ptr<AESCrypto>(new AESCrypto( ))));
  unique_ptr<AESCrypto> crypto2(( unique_ptr<AESCrypto>(new AESCrypto( )) ));
  
  crypto->setRandomSessionKey( );
  crypto2->setSessionKey( crypto->getSessionKey() );
  
  ::mkdir( (TestResourcePath + "crypto_test").c_str(), S_IRWXU );
  string file = TestResourcePath + "crypto_test/test.sec";
  
  string message( "hello" );
  CPPUNIT_ASSERT( crypto->encryptToFile( message.c_str(), 5, file.c_str(), true) );
  size_t length;
  char* data = NULL;
  CPPUNIT_ASSERT( crypto2->decryptFromFile( &data, &length, file.c_str(), true) );
  unique_ptr<char> p = unique_ptr<char>( data ); // ensure to delete buffer
  string message2(data, length);
  CPPUNIT_ASSERT_EQUAL( message, message2 );
}
void RSACryptoTest::testDecryptMessage( void )
{
  shared_ptr<RSACrypto> crypto( shared_ptr<RSACrypto>(RSACrypto::fromPrivateKeyASN1( this->properties_.get( "PrivateKey" ) )) );
  crypto->setEncryptedSessionKey( this->properties_.get( "EncryptedSessionKey" ) );
  CPPUNIT_ASSERT_EQUAL( string( "hello" ), crypto->decrypt( this->properties_.get( "MessageCipher" ) ) );
}
void RSACryptoTest::testDecryptOldMessage( void )
{
  shared_ptr<RSACrypto> crypto( shared_ptr<RSACrypto>(RSACrypto::fromPrivateKeyASN1( this->oldProperties_.get( "PrivateKey" ) )) );
  crypto->setEncryptedSessionKey( this->oldProperties_.get( "EncryptedSessionKey" ) );
  CPPUNIT_ASSERT_EQUAL( string( "hello" ), crypto->decrypt( this->oldProperties_.get( "MessageCipher" ) ) );
}
void RSACryptoTest::testEncryptSessionKey( void )
{
  shared_ptr<RSACrypto> crypto( shared_ptr<RSACrypto>(RSACrypto::fromPublicKeyASN1( this->properties_.get( "PublicKey" ) )) );
  CPPUNIT_ASSERT_EQUAL( this->properties_.get( "FingerPrint" ), crypto->getPublicKeyFingerPrint( ) );
  crypto->setSessionKey( this->properties_.get( "SessionKey" ) );
  string sessionKeyCipher = crypto->getEncryptedSessionKey();
  CPPUNIT_ASSERT( ! sessionKeyCipher.empty() );
}
void RSACryptoTest::testMessageSignature( void )
{
  shared_ptr<RSACrypto> crypto((shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  CPPUNIT_ASSERT( ! crypto->getPublicKeyASN1().empty() );
  CPPUNIT_ASSERT( ! crypto->getPrivateKeyASN1().empty() );

  string signature( crypto->signMessage( "hello" ) );

  CPPUNIT_ASSERT( ! crypto->verifyMessage( "hallo", signature ) );
  CPPUNIT_ASSERT(   crypto->verifyMessage( "hello", signature ) );
}
void RSACryptoTest::testAssignPrivateKey( void )
{
  shared_ptr<RSACrypto> fullCrypto((shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( fullCrypto.get() ) ;
  CPPUNIT_ASSERT( fullCrypto->hasPublicKeyASN1() );
  CPPUNIT_ASSERT( fullCrypto->hasPrivateKeyASN1() );

  RSACrypto crypto(* RSACrypto::fromPublicKeyASN1(fullCrypto->getPublicKeyASN1()) ); // copy
  CPPUNIT_ASSERT( crypto.hasPublicKeyASN1() );
  CPPUNIT_ASSERT( ! crypto.hasPrivateKeyASN1() );
  CPPUNIT_ASSERT( *fullCrypto == crypto ); // compare on public key

  crypto = *fullCrypto; // assign private key
  CPPUNIT_ASSERT( crypto.hasPublicKeyASN1() );
  CPPUNIT_ASSERT( crypto.hasPrivateKeyASN1() );  
  CPPUNIT_ASSERT_EQUAL( fullCrypto->getPublicKeyASN1(), crypto.getPublicKeyASN1() );
  CPPUNIT_ASSERT_EQUAL( fullCrypto->getPrivateKeyASN1(), crypto.getPrivateKeyASN1() );
}
void RSACryptoTest::testExportKeys( void )
{
  shared_ptr<RSACrypto> crypto(( shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ))));
  CPPUNIT_ASSERT( crypto.get() ) ;
  crypto->setRandomSessionKey( );
  
  string input( "hello" );
  string cipher( crypto->encrypt( input ) );
  string signature( crypto->signMessage( input ) );
  
  // get time now
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];
  
  time (&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer,80,"%d-%m-%Y %I:%M:%S",timeinfo);
  std::string str(buffer);

  std::fstream fs;
  fs.open ("unittest/test-encryption.properties", std::fstream::out );
  fs << "#" << str << std::endl;
  fs << "PublicKey=" << crypto->getPublicKeyASN1() << std::endl;
  fs << "PrivateKey=" << crypto->getPrivateKeyASN1() << std::endl;
  fs << "SessionKey=" << crypto->getSessionKey() << std::endl;
  fs << "EncryptedSessionKey=" << crypto->getEncryptedSessionKey() << std::endl;
  fs << "FingerPrint=" << crypto->getPublicKeyFingerPrint() << std::endl;
  fs << "Message=" << input << std::endl;
  fs << "MessageCipher=" << cipher << std::endl;
  fs << "MessageSignature=" << signature << std::endl;
  fs.close();
}

