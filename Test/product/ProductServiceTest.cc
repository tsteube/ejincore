/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductServiceTest.h"

#include "Product.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "SqliteValue.h"

using ejin::Product;

CPPUNIT_TEST_SUITE_REGISTRATION( ProductServiceTest );

// ==========
// TEST Methods
// ==========

void ProductServiceTest::testUpdateProducts( void )
{
  list< shared_ptr<Product> > products(this->productService_->findAllProducts( ));
  CPPUNIT_ASSERT( products.size() == 1 );
  CPPUNIT_ASSERT_EQUAL( string("subscription1"), (*products.begin())->getIdentifier() );

  shared_ptr<Product> product = shared_ptr<Product>(new Product());
  product->setIdentifier( "subscription2" );
  product->setName( "subscription2" );
  product->setPrice( 10.0 );
  product->setMaxAttachmentCount( 10 );

  list< shared_ptr<Product> > newProducts;
  newProducts.push_back(product);
  
  CPPUNIT_ASSERT( this->productService_->updateAllProducts( newProducts ) );
  
  products = this->productService_->findAllProducts( );
  CPPUNIT_ASSERT( products.size() == 1 );
  CPPUNIT_ASSERT_EQUAL( string("subscription2"), (*products.begin())->getIdentifier() );
}
