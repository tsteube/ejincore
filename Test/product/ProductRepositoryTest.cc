/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductRepositoryTest.h"

#include "Product.h"
#include "Utilities.h"
#include "SqliteValue.h"

using ejin::Product;

CPPUNIT_TEST_SUITE_REGISTRATION( ProductRepositoryTest );

// ==========
// TEST Methods
// ==========

void ProductRepositoryTest::testAddProduct( void )
{
  ejin::Product product;
  product.setIdentifier( "test" );
  product.setNo( 0 );
  product.setName( "test" );
  product.setPrice( 10.0 );
  product.setPriceLocale( "de" );
  product.setMaxAttachmentCount( 10 );
  product.setMaxChannelCount( 10 );
  product.setMaxTotalAttachmentSize( 1000 );
  product.setSubscriptionPeriod( 12 );
  product.setSubscriptionPeriodUnit( ejin::year );
  product.setInformation( "description" );

  // ----
  // TEST
  shared_ptr<Product> productEntity( this->productService_->updateProduct( product ));
  
  CPPUNIT_ASSERT( productEntity );
  CPPUNIT_ASSERT( productEntity->hasId() );
}
void ProductRepositoryTest::testUpdateProduct( void )
{
  ejin::Product product;
  product.setNo( 0 );
  product.setIdentifier( "subscription1" );
  product.setName( "test" );
  product.setPrice( 10.0 );
  product.setMaxAttachmentCount( 10 );
  
  // ----
  // TEST
  shared_ptr<Product> productEntity( this->productService_->updateProduct( product ));
  
  CPPUNIT_ASSERT( productEntity );
  CPPUNIT_ASSERT_EQUAL( (integer)1, productEntity->getId() );
}
