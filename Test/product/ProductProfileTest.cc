/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductProfileTest.h"

#include "Product.h"

using ejin::Product;

CPPUNIT_TEST_SUITE_REGISTRATION( ProductProfileTest );

// ==========
// TEST Methods
// ==========

void ProductProfileTest::testFindAllProducts( void )
{
  list< shared_ptr<Product> > products(this->productService_->findAllProducts( ));
  CPPUNIT_ASSERT( products.size() == 1 );
}
void ProductProfileTest::testLoadProduct( void )
{
  shared_ptr<Product> product( this->productService_->loadProductByIdentifier( "subscription1" ));
  CPPUNIT_ASSERT( product );
  CPPUNIT_ASSERT( product->getId() == 1 );
}
