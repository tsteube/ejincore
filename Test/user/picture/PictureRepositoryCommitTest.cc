/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "PictureRepositoryCommitTest.h"

#include "Picture.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( PictureRepositoryCommitTest );

// ==========
// TEST Methods
// ==========

void PictureRepositoryCommitTest::testCommitAddedPicture( void )
{
  // preparation
  Picture picture;
  picture.setId( 13 );
  picture.setMemberId( 13 );
  picture.setModifiedBy( "member" );

  jtime now;
  // ----
  // TEST
  this->pictureRepository_->commit( picture, string( "100" ), now );
  
  // post conditions
  this->db_->refresh( picture );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, picture.getSyncInd() );
  CPPUNIT_ASSERT( picture.hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, picture.hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void PictureRepositoryCommitTest::testCommitUpdatedPicture( void )
{
  // preparation
  Picture picture;
  picture.setId( 14 );
  picture.setMemberId( 14 );
  picture.setModifiedBy( "member" );

  jtime now;
  // ----
  // TEST
  this->pictureRepository_->commit( picture, string( "100" ), now );
  
  // post conditions
  this->db_->refresh( picture );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, picture.getSyncInd() );
  CPPUNIT_ASSERT( picture.hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, picture.hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void PictureRepositoryCommitTest::testCommitDeletedPicture( void )
{
  // preparation
  Picture picture;
  picture.setId( 15 );
  picture.setMemberId( 15 );
  picture.setModifiedBy( "member" );

  jtime now;
  // ----
  // TEST
  this->pictureRepository_->commit( picture, string( "100" ), now );
  
  // post conditions
  try {
    this->db_->refresh( picture );
    CPPUNIT_ASSERT( false );
  } catch (ejin::data_access_error e) {
  }
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}
