/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __PICTURE_REVERT_UNIT_TEST_H__
#define __PICTURE_REVERT_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class PictureRevertTest : public AbstractUserTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( PictureRevertTest );
  CPPUNIT_TEST( testPictureRevert_Empty );
  CPPUNIT_TEST( testPictureRevert_Update );
  CPPUNIT_TEST( testPictureRevert_Restore );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  void setUp( void );
  
  // ==========
  // Test methods
  // ==========
  void testPictureRevert_Empty( void );
  void testPictureRevert_Update( void );
  void testPictureRevert_Restore( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
  
};

#endif // __PICTURE_REVERT_UNIT_TEST_H__
