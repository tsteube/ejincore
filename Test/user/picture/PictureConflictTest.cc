/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "PictureConflictTest.h"

#include "PictureRepository.h"
#include "Picture.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( PictureConflictTest );

#define INITAL_NUM_OF_PICTURES 4

shared_ptr<Picture> PictureConflictTest::loadByMemberId( const integer memberId, SyncInd state )
{
  shared_ptr<Picture> picture = this->pictureRepository_->loadPicture( memberId, false );
  if ( state >= 0 ) {
    CPPUNIT_ASSERT( picture != NULL );
    CPPUNIT_ASSERT_EQUAL( (integer) state, picture->getSyncInd() );
  } else {
    CPPUNIT_ASSERT( picture == NULL );
  }
  return picture;
}

shared_ptr<Picture> PictureConflictTest::validate(const integer memberId,
                                              SyncInd state,
                                              long expectedTotalNumOfPictures )
{
  shared_ptr<Picture> picture = loadByMemberId( memberId, state );
  CPPUNIT_ASSERT( picture != NULL );
  bool expectConflict = ( state == kSyncIndConflict );
  CPPUNIT_ASSERT( expectConflict == this->pictureRepository_->detectConflict( memberId ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfPictures, this->db_->count( "PICTURE_TBL" ) );
  return picture;
}

// ==========
// TEST Methods
// ==========

void PictureConflictTest::testAddPicture( void )
{
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 1 );
  picture->setData( "image" );
  picture->setMimeType( "image/jpeg" );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  validate( 1, kSyncIndSynchronous, INITAL_NUM_OF_PICTURES + 1);
}

void PictureConflictTest::testAddPicture_Empty( void )
{
  shared_ptr<Picture> picture = shared_ptr<Picture>( new Picture( ) );
  picture->setMemberId( 1 );
  picture->setData( "image" );
  picture->setMimeType( "image/jpeg" );
  picture->setEmpty(true);
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) == NULL );
}

void PictureConflictTest::testUpdatePicture_LocalUnchanged( void )
{
  shared_ptr<Picture> picture = loadByMemberId( 11, kSyncIndSynchronous );
  picture->setData( "image2" );
  picture->setMimeType( "image/jpeg" );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  validate( 11, kSyncIndSynchronous, INITAL_NUM_OF_PICTURES );
}

void PictureConflictTest::testUpdatePicture_LocalUpdate_EqualContent( void )
{
  shared_ptr<Picture> picture = loadByMemberId( 12, kSyncIndUpdate );
  picture->setData( "image" );
  picture->setMimeType( "image/jpeg" );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  validate( 12, kSyncIndSynchronous, INITAL_NUM_OF_PICTURES );
}

void PictureConflictTest::testUpdatePicture_LocalUpdate( void )
{
  // 1. update data
  shared_ptr<Picture> picture = loadByMemberId( 12, kSyncIndUpdate );
  string data = picture->getData( );
  string mimeType = picture->getMimeType( );
  picture->setData( "image2" );
  picture->setMimeType( "image/jpeg" );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  picture = validate( 12, kSyncIndConflict, INITAL_NUM_OF_PICTURES + 1 );
  CPPUNIT_ASSERT_EQUAL( picture->getData(), string("image2") );
  
  // 2. update data
  picture->setData( "image3" );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  picture = validate( 12, kSyncIndConflict, INITAL_NUM_OF_PICTURES + 1 );
  CPPUNIT_ASSERT_EQUAL( picture->getData(), string("image3") );

  // 3. update/remove
  picture->clearData( );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  picture = validate( 12, kSyncIndConflict, INITAL_NUM_OF_PICTURES + 1 );
  CPPUNIT_ASSERT( ! picture->hasData() );
  
  // resolve conflict data with original data content
  picture->setData( data );
  picture->setMimeType( mimeType );;
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  validate( 12, kSyncIndSynchronous, INITAL_NUM_OF_PICTURES );
}

void PictureConflictTest::testDeletePicture_LocalUnchanged( void )
{
  shared_ptr<Picture> picture = loadByMemberId( 11, kSyncIndSynchronous );
  picture->setEmpty( true );
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) == NULL );
}

void PictureConflictTest::testDeletePicture_LocalUpdate( void )
{
  // remove picture
  shared_ptr<Picture> picture = loadByMemberId( 12, kSyncIndUpdate );
  picture->clearMimeType();
  picture->clearData();
  CPPUNIT_ASSERT( this->pictureRepository_->update( *picture, "100", jtime() ) != NULL );
  validate( 12, kSyncIndConflict, INITAL_NUM_OF_PICTURES + 1 );
  CPPUNIT_ASSERT( this->pictureRepository_->detectConflict( 12 ) );

  // force resolve conflict
  CPPUNIT_ASSERT( this->pictureRepository_->clearConflict( "member12" ) );
}

void PictureConflictTest::testForceClearConflict( void )
{
  shared_ptr<Picture> picture = loadByMemberId( 13, kSyncIndConflict );
  CPPUNIT_ASSERT( this->pictureRepository_->clearConflict( "member13" ) );
  validate( 13, kSyncIndSynchronous, INITAL_NUM_OF_PICTURES - 1 );
}
