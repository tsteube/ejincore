/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "PictureRevertTest.h"

#include "Picture.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( PictureRevertTest );

void PictureRevertTest::setUp( void )
{
  AbstractUserTestFixture::setUp();
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
}

// ==========
// TEST Methods
// ==========

void PictureRevertTest::testPictureRevert_Empty( void )
{
  Picture picture;
  picture.setId( 1L );
  CPPUNIT_ASSERT( this->db_->contains(picture) );
  this->db_->refresh(picture);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)picture.getSyncInd(), ejin::kSyncIndSynchronous );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( ! this->pictureRepository_->revert( "member", false ) );
  
  this->db_->refresh(picture);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)picture.getSyncInd(), ejin::kSyncIndSynchronous );
}

void PictureRevertTest::testPictureRevert_Update( void )
{
  Picture picture;
  picture.setId( 1L );
  this->db_->refresh(picture);
  picture.setData("tested");
  picture.setSyncInd(ejin::kSyncIndUpdate);
  CPPUNIT_ASSERT( this->db_->update(picture) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->pictureRepository_->revert( "member", false ) );
  
  this->db_->refresh(picture);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)picture.getSyncInd(), ejin::kSyncIndConflict );
  CPPUNIT_ASSERT_EQUAL( picture.getData(), string( "image" ) );
}

void PictureRevertTest::testPictureRevert_Restore( void )
{
  Picture picture;
  picture.setId( 1L );
  this->db_->refresh(picture);
  CPPUNIT_ASSERT( this->db_->remove(picture) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->pictureRepository_->revert( "member", false ) );
  
  this->db_->refresh(picture);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)picture.getSyncInd(), ejin::kSyncIndSynchronous );
}
