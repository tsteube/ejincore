/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __PICTURE_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __PICTURE_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class PictureConflictTest : public AbstractUserTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( PictureConflictTest );
  CPPUNIT_TEST( testAddPicture );
  CPPUNIT_TEST( testAddPicture_Empty );
  CPPUNIT_TEST( testUpdatePicture_LocalUnchanged );
  CPPUNIT_TEST( testUpdatePicture_LocalUpdate_EqualContent );
  CPPUNIT_TEST( testUpdatePicture_LocalUpdate );
  CPPUNIT_TEST( testDeletePicture_LocalUnchanged );
  CPPUNIT_TEST( testDeletePicture_LocalUpdate );
  CPPUNIT_TEST( testForceClearConflict );
  CPPUNIT_TEST_SUITE_END();

public:

  // ==========
  // Test methods
  // ==========
public:
  void testAddPicture( void );
  void testAddPicture_Empty( void );
  void testUpdatePicture_LocalUnchanged( void );
  void testUpdatePicture_LocalUpdate_EqualContent( void );
  void testUpdatePicture_LocalUpdate( void );
  void testDeletePicture_LocalUnchanged( void );
  void testDeletePicture_LocalUpdate( void );
  void testForceClearConflict( void );
  
protected:

  const string getDbConfigDirectory( void ) { return TestResourcePath + "user/conflict"; }
  shared_ptr<ejin::Picture> loadByMemberId( const integer memberId, SyncInd state );
  shared_ptr<ejin::Picture> validate( const integer memberId, SyncInd state, long expectedTotalNumOfPictures );

};

#endif // __PICTURE_RESOLVE_CONFLICT_UNIT_TEST_H__
