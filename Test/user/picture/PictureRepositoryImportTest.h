/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __PICTURE_REPOSITORY_IMPORT_UNIT_TEST_H__
#define __PICTURE_REPOSITORY_IMPORT_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class PictureRepositoryImportTest : public AbstractUserTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( PictureRepositoryImportTest );  
  CPPUNIT_TEST( testImportAddPicture );
  CPPUNIT_TEST( testImportUpdatePicture );
  CPPUNIT_TEST( testImportUpdatePicture_Identical );
  CPPUNIT_TEST( testImportUpdatePicture_Conflict );
  CPPUNIT_TEST( testImportDeletePicture );
  CPPUNIT_TEST( testImportDeletePicture_Conflict );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  void testImportAddPicture( void );
  void testImportUpdatePicture( void );
  void testImportUpdatePicture_Identical( void );
  void testImportUpdatePicture_Conflict( void );
  void testImportDeletePicture( void );
  void testImportDeletePicture_Conflict( void );
  
protected:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "user"; }
  
};

#endif // __PICTURE_REPOSITORY_IMPORT_UNIT_TEST_H__
