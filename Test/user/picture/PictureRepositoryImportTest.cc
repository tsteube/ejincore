/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "PictureRepositoryImportTest.h"

#include "Picture.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( PictureRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void PictureRepositoryImportTest::testImportAddPicture( void )
{
  // preparation
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 1 );
  picture->setData( "image" );
  picture->setMimeType( "image/jpeg" );
  picture->setModifiedBy( "member" );
  
  // ----
  // TEST
  picture = this->pictureRepository_->update( *picture, "100", jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( picture );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, picture->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "image" ), picture->getData() );
  CPPUNIT_ASSERT_EQUAL( string( "image/jpeg" ), picture->getMimeType() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}
void PictureRepositoryImportTest::testImportUpdatePicture( void )
{
  // preparation
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 2 );
  picture->setData( "image2" );
  picture->setMimeType( "image/jpeg" );
  picture->setModifiedBy( "member" );

  // ----
  // TEST
  picture = this->pictureRepository_->update( *picture, "100", jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( picture );
  CPPUNIT_ASSERT_EQUAL( (integer) 2, picture->getId() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, picture->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "image2" ), picture->getData() );
  CPPUNIT_ASSERT_EQUAL( string( "image/jpeg" ), picture->getMimeType() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void PictureRepositoryImportTest::testImportUpdatePicture_Identical( void )
{
  // preparation
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 3 );
  picture->setData( "image" );
  picture->setMimeType( "image/jpeg" );
  picture->setModifiedBy( "member" );

  // ----
  // TEST
  picture = this->pictureRepository_->update( *picture, "100", jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( picture );
  CPPUNIT_ASSERT_EQUAL( (integer) 3, picture->getId() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, picture->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "image" ), picture->getData() );
  CPPUNIT_ASSERT_EQUAL( string( "image/jpeg" ), picture->getMimeType() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void PictureRepositoryImportTest::testImportUpdatePicture_Conflict( void )
{
  // preparation
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 3 );
  picture->setData( "image2" );
  picture->setMimeType( "image/jpeg" );
  picture->setModifiedBy( "member" );

  // ----
  // TEST
  picture = this->pictureRepository_->update( *picture, "100", jtime() );

  // post conditions
  CPPUNIT_ASSERT( picture );
  CPPUNIT_ASSERT_EQUAL( (integer) 3, picture->getId() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, picture->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "image2" ), picture->getData() );
  CPPUNIT_ASSERT_EQUAL( string( "image/jpeg" ), picture->getMimeType() );
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->pictureRepository_->clearConflict( "member3" ) == true );
  
  // post condition
  CPPUNIT_ASSERT( this->pictureRepository_->detectConflict( 3 ) == false );
  this->db_->refresh(*picture);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, picture->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void PictureRepositoryImportTest::testImportDeletePicture( void )
{
  // preparation
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 2 );
  picture->setModifiedBy( "member" );
  picture->setEmpty( true );
  
  // ----
  // TEST
  picture = this->pictureRepository_->update( *picture, "100", jtime() );

  CPPUNIT_ASSERT( !picture );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void PictureRepositoryImportTest::testImportDeletePicture_Conflict( void )
{
  // preparation
  shared_ptr<Picture> picture = shared_ptr<Picture>(new Picture( ));
  picture->setMemberId( 3 );
  picture->setEmpty( true );
  picture->setModifiedBy( "member" );

  // ----
  // TEST
  picture = this->pictureRepository_->update( *picture, "100", jtime() );

  // post conditions
  CPPUNIT_ASSERT_EQUAL( (integer) 3, picture->getId() );
  CPPUNIT_ASSERT( ! picture->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! picture->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->pictureRepository_->detectConflict( 3 ) == true );
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->pictureRepository_->clearConflict( "member3" ) == true );
  
  // post condition
  CPPUNIT_ASSERT( this->pictureRepository_->detectConflict( 3 ) == false );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
