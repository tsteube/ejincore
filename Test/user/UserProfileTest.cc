/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UserProfileTest.h"

#include "SqliteValue.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( UserProfileTest );

// ==========
// TEST Methods
// ==========

void UserProfileTest::testLoadUser( void )
{
  // ----
  // TEST
  shared_ptr<User> user(userService_->loadByUsername( "member12" ));

  CPPUNIT_ASSERT( user );
  CPPUNIT_ASSERT( user->hasMember() );
  CPPUNIT_ASSERT( ! user->hasPicture() );
}

void UserProfileTest::testModifiedUser( void )
{
  // ----
  // TEST
  list< shared_ptr<User> > users( userService_->findUpdated() );
  
  // post validation
  //list< shared_ptr<User> >::iterator it = members.begin();
  bool result = compareLists(users, "member3", "member12", "member13", "member14", "member15", NULL);
  CPPUNIT_ASSERT( result );
}

void UserProfileTest::testConflictedUser( void )
{
  // ----
  // TEST
  list< string > users(userService_->findConflicts());
  
  // post validation
  bool result = compareLists(users, "member4", NULL);
  CPPUNIT_ASSERT( result );
}

// ==========
// Helper
// ==========

bool UserProfileTest::compareLists( list< shared_ptr<User> >& users, const char* name, ... )
{
  va_list argptr;
  va_start(argptr,name);
  
  // convert variable arguments into a list of User objects
  list< User > values;
  if (name) {
    values.push_back( User( string(name) ) );
    const char* param = va_arg(argptr, const char*);
    while (param && *param) {
      values.push_back( User( string(param) ) );
      param = va_arg(argptr, const char*);
    }
  }
  
  va_end(argptr);
  
  // compare both sets
  bool isEqual(values.size() == users.size());
  if (isEqual) {      
    list< shared_ptr<User> >::iterator it = users.begin();
    for (; it!=users.end() && isEqual; it++) {
      list<User>::iterator findIter = std::find(values.begin(), values.end(), *(*it));      
      isEqual = findIter != values.end();
    }
  }
  return isEqual;
}
bool UserProfileTest::compareLists( list< string >& users, const char* name, ... )
{
  va_list argptr;
  va_start(argptr,name);
  
  // convert variable arguments into a list of User objects
  list< string > values;
  if (name) {
    values.push_back( string(name) );
    const char* param = va_arg(argptr, const char*);
    while (param && *param) {
      values.push_back( string(param) );
      param = va_arg(argptr, const char*);
    }
  }
  
  va_end(argptr);
  
  // compare both sets
  bool isEqual(values.size() == users.size());
  if (isEqual) {
    list< string >::iterator it = users.begin();
    for (; it!=users.end() && isEqual; it++) {
      list<string>::iterator findIter = std::find(values.begin(), values.end(), (*it));
      isEqual = findIter != values.end();
    }
  }
  return isEqual;
}

