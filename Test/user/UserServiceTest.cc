/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UserServiceTest.h"

#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "User.h"
#include "Member.h"
#include "ApprovedMember.h"
#include "SyncSource.h"
#include "Modifications.h"
#include "SqliteValue.h"
#include "RSACrypto.h"
#include "Utilities.h"

CPPUNIT_TEST_SUITE_REGISTRATION( UserServiceTest );

void UserServiceTest::setUp( void )
{
  AbstractUserTestFixture::setUp();
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
}

// ==========
// TEST Methods
// ==========

void UserServiceTest::testSessionKey( void )
{
  
  Member owner(0);
  this->db_->refresh(owner);
  const shared_ptr<RSACrypto> crypto = this->userService_->generateRSAKey( "owner" );
  CPPUNIT_ASSERT( crypto.get() );
  this->db_->refresh(owner);
  CPPUNIT_ASSERT( owner.hasPubKey() );
  
  RSACrypto crypto2(*crypto);
  crypto2.setRandomSessionKey();
  string key = crypto2.getSessionKey();
  string encryptedKey = crypto2.getEncryptedSessionKey( );
  CPPUNIT_ASSERT( ! encryptedKey.empty() );
  
  string aesKey = crypto2.setEncryptedSessionKey( encryptedKey );
  CPPUNIT_ASSERT_EQUAL( key, crypto2.getSessionKey() );
}

void UserServiceTest::testExportUsers( void )
{
  list< shared_ptr<User> > container;
  
  // TEST
  unique_ptr<SyncSource> source(userService_->exportModifiedUser( container ));
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 5, container.size() );
  CPPUNIT_ASSERT_EQUAL( string( "10" ), source->getSyncAnchor() );
}

void UserServiceTest::testImportMember( void )
{
  // preparation
  Modifications<User> container;
  container.setSyncAnchor( string( "10" ) );
  
  // add member
  shared_ptr<Member> add(new Member());
  add->setUsername( "member99" );
  add->setPubKey( "aGFsbG8=" );
  container.getAdded().push_back( shared_ptr<User>( new User("member99", add, NULL) ) );
  
  // update member
  shared_ptr<Member> update(new Member());
  update->setUsername( "member1" );
  update->setPubKey( "aGFsbG8=" );
  container.getUpdated().push_back( shared_ptr<User>( new User("member1", add, NULL) ) );
  
  // delete member
  shared_ptr<User> remove(new User());
  remove->setUsername( "member2" );
  container.getRemoved().push_back( shared_ptr<User>( new User("member2") ) );
  
  // TEST
  CPPUNIT_ASSERT( this->userService_->importUpdatedUser( container, jtime() ).size() == 0 );
  
  // post conditions
  Member entity1(1);
  this->db_->refresh(entity1);
  CPPUNIT_ASSERT( "aGFsbG8=" == entity1.getPubKey() );
  
  Member entity2(2);
  CPPUNIT_ASSERT( (db_->contains(entity2) == false) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 4, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberApproval, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member1") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );

  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member2") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );

  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member99") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void UserServiceTest::testImportMember_Conflict( void )
{
  // preparation
  Modifications<User> container;
  container.setSyncAnchor( string( "10" ) );
  
  // update member
  shared_ptr<Member> update(new Member());
  update->setUsername( "member3" );
  update->setPubKey( "aGFsbG8=" );

  shared_ptr<ApprovedMember> approval(new ApprovedMember());
  approval->setUsername( "owner" );
  update->getApprovedMembers().push_back( approval );
  
  container.getUpdated().push_back( shared_ptr<User>( new User( "member3", update, NULL ) ) );
  

  // TEST
  CPPUNIT_ASSERT( this->userService_->importUpdatedUser( container, jtime() ).size() > 0 );
  Member entity(3);
  this->db_->refresh(entity);
  CPPUNIT_ASSERT( "aGFsbG8=" == entity.getPubKey() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member3"), changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void UserServiceTest::testImportMember_AddPicture( void )
{
  // preparation
  Modifications<User> container;
  container.setSyncAnchor( string( "10" ) );
  
  // update member
  shared_ptr<Picture> update(new Picture());
  update->setMimeType( "image/jpeg" );
  update->setData( "image" );
  container.getUpdated().push_back( shared_ptr<User>( new User("member1", NULL, update) ) );
  
  // TEST
  CPPUNIT_ASSERT( this->userService_->importUpdatedUser( container, jtime() ).size() == 0 );
  
  // post conditions
  Picture entity(101);
  this->db_->refresh(entity);
  CPPUNIT_ASSERT( "image" == entity.getData() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member1"), changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void UserServiceTest::testImportMember_UpdatePicture( void )
{
  // preparation
  Modifications<User> container;
  container.setSyncAnchor( string( "10" ) );
  
  // update member
  shared_ptr<Picture> update(new Picture());
  update->setMimeType( "image/jpeg" );
  update->setData( "image2" );
  container.getUpdated().push_back( shared_ptr<User>( new User("member2", NULL, update) ) );
  
  // TEST
  CPPUNIT_ASSERT( this->userService_->importUpdatedUser( container, jtime() ).size() == 0 );
  
  // post conditions
  Picture entity(2);
  this->db_->refresh(entity);
  CPPUNIT_ASSERT( "image2" == entity.getData() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member2"), changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void UserServiceTest::testImportMember_RemovePicture( void )
{
  // preparation
  Modifications<User> container;
  container.setSyncAnchor( string( "10" ) );
  
  // update member
  shared_ptr<Picture> update(new Picture());
  container.getUpdated().push_back( shared_ptr<User>( new User("member2", NULL, update) ) );
  
  // TEST
  CPPUNIT_ASSERT( this->userService_->importUpdatedUser( container, jtime() ).size() == 0 );
  
  // post conditions
  Picture entity(2);
  CPPUNIT_ASSERT( ! this->db_->contains(entity) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberPicture, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("member2"), changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void UserServiceTest::testPictureRevert_Update( void )
{
  Picture picture;
  picture.setId( 2L );
  this->db_->refresh(picture);
  CPPUNIT_ASSERT( this->db_->remove(picture) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->userService_->revert( "Picture/member2", false ) );
  
  this->db_->refresh(picture);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)picture.getSyncInd(), ejin::kSyncIndSynchronous );
}
void UserServiceTest::testMemberRevert_Update( void )
{
  Member member;
  member.setId( 1L );
  this->db_->refresh(member);
  member.setEmail("tested");
  member.setSyncInd(ejin::kSyncIndUpdate);
  CPPUNIT_ASSERT( this->db_->update(member) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->userService_->revert( "Member/member1", false ) );
  
  this->db_->refresh(member);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)member.getSyncInd(), ejin::kSyncIndConflict );
}

