/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __USER_PROFILE_UNIT_TEST_H__
#define __USER_PROFILE_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"
#include "User.h"

/*
 * Ejin unit tests
 */
class UserProfileTest : public AbstractUserTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( UserProfileTest );
  CPPUNIT_TEST( testLoadUser );
  CPPUNIT_TEST( testModifiedUser );
  CPPUNIT_TEST( testConflictedUser );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testLoadUser( void );
  void testModifiedUser( void );
  void testConflictedUser( void );
  
  // ==========
  // Private Interface
  // ==========
private:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "user"; }

  static bool compareLists( list< shared_ptr<User> >& users, const char* name, ... ) ;
  static bool compareLists( list< string >& values, const char* name, ... );

};

#endif // __USER_PROFILE_UNIT_TEST_H__
