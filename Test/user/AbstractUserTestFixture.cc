/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "AbstractUserTestFixture.h"

#include <iostream>
#include <sys/time.h>
#include <copyfile.h>

#include "DBUnit.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"

#define PASSWORD "test"

// ==========
// Test Setup / Teardown
// ==========

void AbstractUserTestFixture::setUp( void )
{
  gettimeofday(&tval_start_, NULL);
  
  this->db_.reset();
  this->dbunit_.reset();
  this->userService_.reset();
  this->changeLogRepository_.reset();
  this->approvedMemberRepository_.reset();
  this->memberRepository_.reset();
  this->pictureRepository_.reset();
  this->userService_.reset();
  
  unique_ptr<ejin::Database> db( unique_ptr<ejin::Database>(new ejin::Database("test")) );
  const string mainDb = TestResourcePath + __EJIN_TEST_MAIN_DB_NAME;
  const string attachmentDb = TestResourcePath + __EJIN_TEST_ATTACHMENT_DB_NAME;
  const string testMainDb = TestResourcePath + "ejin(test).db";
  const string testAttachmentDb = TestResourcePath + "attachment(test).db";
  if ( db->open(__EJIN_SCHEMA_VERSION,
                mainDb.c_str(), testMainDb.c_str(),
                attachmentDb.c_str(), testAttachmentDb.c_str(),
                "test.key", "private.key",
                PASSWORD, true ) == ejin::Database::Status::DatabaseOpen ) {
    db->attachBackupDb( PASSWORD );
    db->attachArchiveDb( PASSWORD, true );
    this->db_ = std::move(db);
    
    // setup database with samples
    this->dbunit_ = unique_ptr<DBUnit>(new DBUnit(*db_.get()));
    
    // create object to test
    this->changeLogRepository_ = unique_ptr<ChangeLogRepository>(new ChangeLogRepository(*db_));
    this->approvedMemberRepository_ = unique_ptr<ApprovedMemberRepository>(new ApprovedMemberRepository(*db_, *this->changeLogRepository_));
    this->memberRepository_ = unique_ptr<MemberRepository>(new MemberRepository(*db_, *this->changeLogRepository_ , *this->approvedMemberRepository_));
    this->channelMemberRepository_ = unique_ptr<ChannelMemberRepository>(new ChannelMemberRepository(*db_, *this->changeLogRepository_ , *this->approvedMemberRepository_));
    this->pictureRepository_ = unique_ptr<PictureRepository>(new PictureRepository(*db_, *this->changeLogRepository_, *this->memberRepository_));
    this->userService_ = unique_ptr<UserService>(new UserService(*db_, *this->memberRepository_, *this->pictureRepository_));

    // start transaction
    this->db_->transactionBegin();
    this->dbunit_->setupTables(this->getDbConfigDirectory(), true);

  }
  gettimeofday(&tval_before_, NULL);

  this->changeLogRepository_->begin();
}

void AbstractUserTestFixture::tearDown( void )
{
  struct timeval tval_after, ttest_result;
  gettimeofday(&tval_after, NULL);
  timersub(&tval_after, &tval_before_, &ttest_result);

  if (db_.get() != NULL) {
    if (db_->isClean()) {
      // commit transaction
      this->db_->transactionCommit();
    } else {
      // rollback transaction
      this->db_->transactionRollback();
    }
  }

  this->db_->detachArchiveDb( );
  this->db_->detachBackupDb( );
  this->db_.reset();
  this->dbunit_.reset();
  this->userService_.reset();
  this->pictureRepository_.reset();
  this->memberRepository_.reset();
  this->approvedMemberRepository_.reset();
  this->channelMemberRepository_.reset();
  this->changeLogRepository_.reset();

  struct timeval tval_end, ttotal_result;
  gettimeofday(&tval_end, NULL);
  timersub(&tval_end, &tval_start_, &ttotal_result);

  printf(" %ld.%06ld (%ld.%06ld in total)",
         (long int)ttest_result.tv_sec, (long int)ttest_result.tv_usec,
         (long int)ttotal_result.tv_sec, (long int)ttotal_result.tv_usec);
}
