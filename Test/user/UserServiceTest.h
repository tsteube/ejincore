/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __USER_SERVICE_UNIT_TEST_H__
#define __USER_SERVICE_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class UserServiceTest : public AbstractUserTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( UserServiceTest );
  CPPUNIT_TEST( testSessionKey );
  CPPUNIT_TEST( testImportMember );
  CPPUNIT_TEST( testExportUsers );
  CPPUNIT_TEST( testImportMember_AddPicture );
  CPPUNIT_TEST( testImportMember_UpdatePicture );
  CPPUNIT_TEST( testImportMember_RemovePicture );
  CPPUNIT_TEST( testImportMember_Conflict );
  CPPUNIT_TEST( testMemberRevert_Update );
  CPPUNIT_TEST( testPictureRevert_Update );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  void setUp( void );
  
  // ==========
  // Test methods
  // ==========
  void testSessionKey( void );
  void testExportUsers( void );
  void testImportMember( void );
  void testImportMember_Conflict( void );
  void testImportMember_AddPicture( void );
  void testImportMember_UpdatePicture( void );
  void testImportMember_RemovePicture( void );
  void testPictureRevert_Update( void );
  void testMemberRevert_Update( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "user"; }
  
};

#endif // __USER_SERVICE_UNIT_TEST_H__
