/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEMBER_REPOSITORY_IMPORT_UNIT_TEST_H__
#define __MEMBER_REPOSITORY_IMPORT_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class MemberRepositoryImportTest : public AbstractUserTestFixture
{
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( MemberRepositoryImportTest );  
  CPPUNIT_TEST( testImportAddMember );
  CPPUNIT_TEST( testImportAddMemberWithApprovals );
  CPPUNIT_TEST( testImportUpdateMember );
  CPPUNIT_TEST( testImportUpdateMember_AddApproval );
  CPPUNIT_TEST( testImportUpdateMember_Identical );
  CPPUNIT_TEST( testImportUpdateMember_Conflict );
  CPPUNIT_TEST( testImportUpdateMember_RemoveApproval );
  CPPUNIT_TEST( testImportDeleteMember );
  CPPUNIT_TEST( testImportDeleteMember_Conflict );
  CPPUNIT_TEST_SUITE_END();
  
public:
  // ==========
  // Test methods
  // ==========
  void testImportAddMember( void );
  void testImportAddMemberWithApprovals( void );
  void testImportUpdateMember( void );
  void testImportUpdateMember_AddApproval( void );
  void testImportUpdateMember_Identical( void );
  void testImportUpdateMember_Conflict( void );
  void testImportUpdateMember_RemoveApproval( void );
  void testImportDeleteMember( void );
  void testImportDeleteMember_Conflict( void );
  
protected:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "user"; }
  
};

#endif // __MEMBER_REPOSITORY_IMPORT_UNIT_TEST_H__
