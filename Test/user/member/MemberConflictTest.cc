/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberConflictTest.h"

#include "MemberRepository.h"
#include "Member.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MemberConflictTest );

#define INITAL_NUM_OF_MEMBERS 7

shared_ptr<Member> MemberConflictTest::loadByUsername( const string& name, const string& syncAnchor, SyncInd state )
{
  shared_ptr<Member> member = this->memberRepository_->loadByUsername( name );
  if ( syncAnchor.empty() ) {
    CPPUNIT_ASSERT( member == NULL );
  } else {
    CPPUNIT_ASSERT( member != NULL );
    CPPUNIT_ASSERT_EQUAL( syncAnchor, member->getSyncAnchor() );
    CPPUNIT_ASSERT_EQUAL( (integer) state, member->getSyncInd() );
  }
  return member;
}

shared_ptr<Member> MemberConflictTest::validate(const string& name, 
                                              const string& syncAnchor,
                                              SyncInd state,
                                              long expectedTotalNumOfMembers )
{
  shared_ptr<Member> member = loadByUsername( name, syncAnchor, state );
  CPPUNIT_ASSERT( member != NULL );
  bool expectConflict = ( state == kSyncIndConflict );
  CPPUNIT_ASSERT( expectConflict == this->memberRepository_->detectConflict( name ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfMembers, this->db_->count( "MEMBER_TBL" ) );
  return member;
}

// ==========
// TEST Methods
// ==========

void MemberConflictTest::testAddMember( void )
{
  shared_ptr<Member> member = shared_ptr<Member>(new Member( "99" ));
  member->setUsername( "member99" );
  member->setPubKey( "aGFsbG8=" );
  member->setSyncAnchor( "1" );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  validate( "member99", "1", kSyncIndSynchronous, INITAL_NUM_OF_MEMBERS + 1);
}

void MemberConflictTest::testAddMember_Empty( void )
{
  shared_ptr<Member> member = shared_ptr<Member>(new Member( "99" ));
  member->setUsername( "member99" );
  member->setPubKey( "aGFsbG8=" );
  member->setSyncAnchor( "1" );
  member->setEmpty( true );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) == NULL );
}

void MemberConflictTest::testUpdateMember_LocalUnchanged( void )
{ 
  shared_ptr<Member> member = loadByUsername( "member11", "0", kSyncIndSynchronous );
  member->setSyncAnchor( "1" );
  member->setPubKey( "aGFsbG8=" );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  validate( "member11", "1", kSyncIndSynchronous, INITAL_NUM_OF_MEMBERS );
}

void MemberConflictTest::testUpdateMember_LocalUpdate_EqualContent( void )
{  
  shared_ptr<Member> member = loadByUsername( "member12", "0", kSyncIndUpdate );
  member->setSyncAnchor( "1" );
  member->setPubKey( "aGFsbG8=" );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  validate( "member12", "1", kSyncIndSynchronous, INITAL_NUM_OF_MEMBERS );
}

void MemberConflictTest::testUpdateMember_LocalUpdate( void )
{  
  // 1. update data
  shared_ptr<Member> member = loadByUsername( "member12", "0", kSyncIndUpdate );
  string key = member->getPubKey( );
  member->setSyncAnchor( "1" );
  member->setPubKey( "aGFsbG8y" );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  member = validate( "member12", "1", kSyncIndConflict, INITAL_NUM_OF_MEMBERS + 1 );
  CPPUNIT_ASSERT_EQUAL( member->getPubKey(), string("aGFsbG8y") );
  
  // 2. update data
  member->setSyncAnchor( "2" );
  member->setPubKey( "aGFsbG8z" );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  member = validate( "member12", "2", kSyncIndConflict, INITAL_NUM_OF_MEMBERS + 1 );
  CPPUNIT_ASSERT_EQUAL( member->getPubKey(), string("aGFsbG8z") );

  // 3. update data
  member->setSyncAnchor( "3" );
  member->setEmpty( true );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  member = validate( "member12", "3", kSyncIndConflict, INITAL_NUM_OF_MEMBERS + 1 );
  CPPUNIT_ASSERT( ! member->hasPubKey() );
  
  // resolve conflict data with original data content
  member->setSyncAnchor( "4" );
  member->setPubKey( key );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  validate( "member12", "4", kSyncIndSynchronous, INITAL_NUM_OF_MEMBERS );  
}

void MemberConflictTest::testDeleteMember_LocalUnchanged( void )
{
  shared_ptr<Member> member = shared_ptr<Member>(new Member( "member11" ));
  member->setSyncAnchor( "1" );
  member->setEmpty( true );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  loadByUsername( "member11", "", (SyncInd) -1 );
}

void MemberConflictTest::testDeleteMember_LocalUpdate( void )
{
  // remove member
  shared_ptr<Member> member = shared_ptr<Member>(new Member( "member12" ));
  member->setSyncAnchor( "1" );
  member->setEmpty( true );
  CPPUNIT_ASSERT( this->memberRepository_->update( *member, false, jtime() ) != NULL );
  member = loadByUsername( "member12", "1", kSyncIndConflict );
  CPPUNIT_ASSERT( ! member->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! member->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->memberRepository_->detectConflict( "member12" ) );

  // force resolve conflict
  CPPUNIT_ASSERT( this->memberRepository_->clearConflict( "member12" ) );
}

void MemberConflictTest::testForceClearConflict( void )
{
  shared_ptr<Member> member = loadByUsername( "member13", "0", kSyncIndConflict );
  CPPUNIT_ASSERT( this->memberRepository_->clearConflict( "member13" ) );
  validate( "member13", "0", kSyncIndSynchronous, INITAL_NUM_OF_MEMBERS - 1 );
}
