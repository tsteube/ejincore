/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMemberRepositoryImportTest.h"

#include "ChannelMember.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelMemberRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void ChannelMemberRepositoryImportTest::testImportAddChannelMember( void )
{
  // preparation
  shared_ptr<ChannelMember> channelMember = shared_ptr<ChannelMember>(new ChannelMember( 1, "owner" ));
  channelMember->setTags( "tag" );
  channelMember->setSessionKey( "key" );
  channelMember->setModifiedBy( "owner" );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelMemberRepository_->searchFullText("tag").size());

  // ----
  // TEST
  ChannelMember& channelMember2( this->channelMemberRepository_->add( *channelMember, "100", jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT_EQUAL( string( "tag" ), channelMember2.getTags() );
  CPPUNIT_ASSERT_EQUAL( string( "key" ), channelMember2.getSessionKey() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );

  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelMemberRepository_->searchFullText("tag").size());
}

void ChannelMemberRepositoryImportTest::testImportUpdateChannelMember_noop( void )
{
  // preparation
  shared_ptr<ChannelMember> entityRef( channelMemberRepository_->loadChannelMemberInternal( Value((integer)1), "member2", false ) );
  shared_ptr<ChannelMember> channelMember = shared_ptr<ChannelMember>(new ChannelMember( 1, "member3" ));
  channelMember->setTags( "tag" );
  channelMember->setSessionKey( "key" );
  channelMember->setModifiedBy( "owner" );
  jtime modTime( entityRef->getModifyTime() );
  modTime.addMinutes( -1 );
  channelMember->setModifyTime( modTime );
  
  // ----
  // TEST
  channelMemberRepository_->update( *entityRef, *channelMember, "100", jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( entityRef->hasTags() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}

void ChannelMemberRepositoryImportTest::testImportUpdateChannelMember( void )
{
  // preparation
  shared_ptr<ChannelMember> entityRef( channelMemberRepository_->loadChannelMemberInternal( Value((integer)1), "member1", false ) );
  shared_ptr<ChannelMember> channelMember = shared_ptr<ChannelMember>(new ChannelMember( 1, "member3" ));
  channelMember->setTags( "tag" );
  channelMember->setSessionKey( "key" );
  channelMember->setModifiedBy( "owner" );
  channelMember->setModifyTime( jtime() );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelMemberRepository_->searchFullText("tag").size());

  // ----
  // TEST
  channelMemberRepository_->update( *entityRef, *channelMember, "100", jtime() );
  
  // post conditions
  CPPUNIT_ASSERT_EQUAL( string( "tag" ), entityRef->getTags() );
  CPPUNIT_ASSERT_EQUAL( string( "key" ), entityRef->getSessionKey() );

  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );

  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelMemberRepository_->searchFullText("tag").size());
}
void ChannelMemberRepositoryImportTest::testImportRemoveChannelMember( void )
{
  // preparation
  shared_ptr<ChannelMember> entityRef( channelMemberRepository_->loadChannelMemberInternal( Value((integer)1), "member2", false ) );
  entityRef->setModifiedBy( "owner" );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 7, this->channelMemberRepository_->searchFullText("hello").size());

  // ----
  // TEST
  CPPUNIT_ASSERT( channelMemberRepository_->remove( *entityRef, "100", jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! channelMemberRepository_->loadChannelMemberInternal( Value((integer)1), "member2", false ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 6, this->channelMemberRepository_->searchFullText("hello").size());
}
