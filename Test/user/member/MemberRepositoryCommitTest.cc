/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberRepositoryCommitTest.h"

#include "Member.h"
#include "ApprovedMember.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MemberRepositoryCommitTest );

// ==========
// TEST Methods
// ==========

void MemberRepositoryCommitTest::testCommitUpdatedMember( void )
{
  // preparation
  Member member;
  member.setId(12);
  member.setUsername( string( "member12" ) );
  
  jtime now;
  // ----
  // TEST
  this->memberRepository_->commit( member, string( "100" ), now );
  
  // post conditions
  this->db_->refresh(member);
  CPPUNIT_ASSERT_EQUAL( string( "member12" ), member.getUsername() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member.getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member.getSyncAnchor() );
  CPPUNIT_ASSERT( member.hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, member.hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member12" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryCommitTest::testCommitAddedApproval( void )
{
  // preparation
  shared_ptr<Member> member( shared_ptr<Member>(new Member()) );
  member->setId(2);
  member->setUsername( string( "member2" ) );
  
  jtime now;
  // ----
  // TEST
  this->memberRepository_->commit( *member, string( "100" ), now );
  
  // post conditions
  member = memberRepository_->loadByUsername( "member2" );
  CPPUNIT_ASSERT_EQUAL( string( "member2" ), member->getUsername() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  CPPUNIT_ASSERT( member->hasModifyTime() );
  CPPUNIT_ASSERT_EQUAL( (size_t)1, member->getApprovedMembers().size() );
  shared_ptr<ApprovedMember> approvedMember( *member->getApprovedMembers().begin() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, approvedMember->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberApproval, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryCommitTest::testCommitDeletedApproval( void )
{
  // preparation
  shared_ptr<Member> member( shared_ptr<Member>(new Member()) );
  member->setId(3);
  member->setUsername( string( "member3" ) );
  
  jtime now;
  // ----
  // TEST
  this->memberRepository_->commit( *member, string( "100" ), now );
  
  // post conditions
  member = memberRepository_->loadByUsername( "member3" );
  CPPUNIT_ASSERT_EQUAL( string( "member3" ), member->getUsername() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  CPPUNIT_ASSERT( member->hasModifyTime() );
  CPPUNIT_ASSERT_EQUAL( (size_t)0, member->getApprovedMembers().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberApproval, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );

  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member3" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}
