/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberRepositoryImportTest.h"

#include "Member.h"
#include "ApprovedMember.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MemberRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void MemberRepositoryImportTest::testImportAddMember( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setUsername( "member99" );
  member->setPubKey( "aGFsbG8=" );
  member->setEmail( "member99@foo.de" );
  member->setSyncAnchor( string( "100" ) );
  
  // ----
  // TEST
  member = this->memberRepository_->update( *member, false, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( member );
  CPPUNIT_ASSERT( member->hasId() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member99" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportAddMemberWithApprovals( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setUsername( "member99" );
  member->setPubKey( "aGFsbG8=" );
  member->setEmail( "member99@foo.de" );
  member->setSyncAnchor( string( "100" ) );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "member" )) );

  // ----
  // TEST
  this->memberRepository_->update( *member, false, jtime() );
  member = memberRepository_->loadByUsername( "member99" );

  // post conditions
  CPPUNIT_ASSERT( member );
  CPPUNIT_ASSERT( member->hasId() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (size_t)1, member->getApprovedMembers().size() );
  shared_ptr<ApprovedMember> approvedMember( * member->getApprovedMembers().begin() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, approvedMember->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (integer) 101L, approvedMember->getMemberId() );
  CPPUNIT_ASSERT( "member" == approvedMember->getUsername() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberApproval, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member99" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportUpdateMember( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setUsername( "member2" );
  member->setEmail( "member2@foo.de" );
  member->setPubKey( "aGFsbG7=" );
  member->setSyncAnchor( string( "100" ) );
  
  // ----
  // TEST
  member = memberRepository_->update( *member, false, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( member->hasId() );
  CPPUNIT_ASSERT_EQUAL( (integer) 2, member->getId() );
  CPPUNIT_ASSERT( "aGFsbG7=" == member->getPubKey() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member2" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportUpdateMember_AddApproval( void )
{
  // preparation
  shared_ptr<Member> member( memberRepository_->loadByUsername( "member1" ) );
  member->setSyncAnchor( string( "100" ) );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "member12" )) );
  // ----
  // TEST
  member = memberRepository_->update( *member, false, jtime() );
  CPPUNIT_ASSERT( member->hasId() );
  
  // post conditions
  member = memberRepository_->loadByUsername( "member1" );
  CPPUNIT_ASSERT_EQUAL( (size_t)2, member->getApprovedMembers().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberApproval, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member12" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportUpdateMember_Identical( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setUsername( "member3" );
  member->setEmail( "member3@foo.de" );
  member->setPubKey( "aGFsbG8=" );
  member->setSyncAnchor( string( "100" ) );
  member->setSyncInd( ejin::kSyncIndUpdate );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "owner" )) );
  
  // ----
  // TEST
  member = memberRepository_->update( *member, false, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT_EQUAL( (integer) 3, member->getId() );
  CPPUNIT_ASSERT( "aGFsbG8=" == member->getPubKey() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  CPPUNIT_ASSERT( this->memberRepository_->detectConflict( "member3" ) == false );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member3" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportUpdateMember_Conflict( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setUsername( "member3" );
  member->setEmail( "member3@foo.net" );
  member->setPubKey( "aGFsbG8=" );
  member->setSyncAnchor( string( "100" ) );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "owner" )) );

  // ----
  // TEST
  member = memberRepository_->update( *member, false, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT_EQUAL( (integer) 3, member->getId() );
  CPPUNIT_ASSERT( "aGFsbG8=" == member->getPubKey() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), member->getSyncAnchor() );
  CPPUNIT_ASSERT( this->memberRepository_->detectConflict( "member3" ) == true );
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->memberRepository_->clearConflict( "member3" ) == true );
  
  // post condition
  CPPUNIT_ASSERT( this->memberRepository_->detectConflict( "member3" ) == false );
  member->setId(3);
  this->db_->refresh(*member);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member3" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportUpdateMember_RemoveApproval( void )
{
  // preparation
  shared_ptr<Member> member( memberRepository_->loadByUsername( "member1" ) );
  member->setSyncAnchor( string( "100" ) );
  member->getApprovedMembers().clear();

  // ----
  // TEST
  member = memberRepository_->update( *member, false, jtime() );
  CPPUNIT_ASSERT( member->hasId() );
  
  // post conditions
  member = memberRepository_->loadByUsername( "member1" );
  CPPUNIT_ASSERT_EQUAL( (size_t)0, member->getApprovedMembers().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMemberApproval, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportDeleteMember( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setSyncAnchor( "100" );
  member->setUsername( "member2" );
  member->setEmpty( true );
  
  // ----
  // TEST
  this->memberRepository_->update( *member, false, jtime() );
  
  CPPUNIT_ASSERT( this->db_->contains( *member) == false );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member2" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}

void MemberRepositoryImportTest::testImportDeleteMember_Conflict( void )
{
  // preparation
  shared_ptr<Member> member = shared_ptr<Member>(new Member( ));
  member->setSyncAnchor( "100" );
  member->setUsername( "member3" );
  member->setEmpty( true );
  
  // ----
  // TEST
  member = memberRepository_->update( *member, false, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT_EQUAL( (integer) 3, member->getId() );
  CPPUNIT_ASSERT( ! member->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! member->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->memberRepository_->detectConflict( "member3" ) == true );
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->memberRepository_->clearConflict( "member3" ) == true );
  
  // post condition
  CPPUNIT_ASSERT( this->memberRepository_->detectConflict( "member3" ) == false );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "member3" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( !changeLog->hasResourceId3() );
}
