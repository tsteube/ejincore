/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEMBER_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __MEMBER_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class MemberConflictTest : public AbstractUserTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( MemberConflictTest );
  CPPUNIT_TEST( testAddMember );
  CPPUNIT_TEST( testAddMember_Empty );
  CPPUNIT_TEST( testUpdateMember_LocalUnchanged );
  CPPUNIT_TEST( testUpdateMember_LocalUpdate_EqualContent );
  CPPUNIT_TEST( testUpdateMember_LocalUpdate );
  CPPUNIT_TEST( testDeleteMember_LocalUnchanged );
  CPPUNIT_TEST( testDeleteMember_LocalUpdate );
  CPPUNIT_TEST( testForceClearConflict );
  CPPUNIT_TEST_SUITE_END();

public:

  // ==========
  // Test methods
  // ==========
  void testAddMember( void );
  void testAddMember_Empty( void );
  void testUpdateMember_LocalUnchanged( void );
  void testUpdateMember_LocalUpdate_EqualContent( void );
  void testUpdateMember_LocalUpdate( void );
  void testDeleteMember_LocalUnchanged( void );
  void testDeleteMember_LocalUpdate( void );
  void testForceClearConflict( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "user/conflict"; }
  shared_ptr<Member> loadByUsername( const string& name, const string& syncAnchor, SyncInd state );
  shared_ptr<Member> validate( const string& name, const string& syncAnchor, SyncInd state, long expectedTotalNumOfMembers );
  

};

#endif // __MEMBER_RESOLVE_CONFLICT_UNIT_TEST_H__
