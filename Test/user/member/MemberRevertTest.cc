/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberRevertTest.h"

#include "ChannelMemberRepository.h"
#include "Member.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MemberRevertTest );

void MemberRevertTest::setUp( void )
{
  AbstractUserTestFixture::setUp();
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
}

// ==========
// TEST Methods
// ==========

void MemberRevertTest::testMemberRevert_Empty( void )
{
  Member member;
  member.setId( 1L );
  CPPUNIT_ASSERT( this->db_->contains(member) );
  this->db_->refresh(member);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)member.getSyncInd(), ejin::kSyncIndSynchronous );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( ! this->memberRepository_->revert( "member", false ) );
  
  this->db_->refresh(member);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)member.getSyncInd(), ejin::kSyncIndSynchronous );
}

void MemberRevertTest::testMemberRevert_Update( void )
{
  Member member;
  member.setId( 1L );
  this->db_->refresh(member);
  member.setEmail("tested");
  member.setSyncInd(ejin::kSyncIndUpdate);
  CPPUNIT_ASSERT( this->db_->update(member) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->memberRepository_->revert( "member", false ) );
  
  this->db_->refresh(member);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)member.getSyncInd(), ejin::kSyncIndConflict );
}

void MemberRevertTest::testMemberRevert_Restore( void )
{
  Member member;
  member.setId( 1L );
  this->db_->refresh(member);
  CPPUNIT_ASSERT( this->db_->remove(member) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->memberRepository_->revert( "member", false ) );
  
  this->db_->refresh(member);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)member.getSyncInd(), ejin::kSyncIndSynchronous );
}
