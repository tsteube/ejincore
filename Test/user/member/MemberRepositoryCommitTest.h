/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEMBER_REPOSITORY_COMMIT_UNIT_TEST_H__
#define __MEMBER_REPOSITORY_COMMIT_UNIT_TEST_H__

#include "AbstractUserTestFixture.h"

/*
 * Ejin unit tests
 */
class MemberRepositoryCommitTest : public AbstractUserTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( MemberRepositoryCommitTest );  
  CPPUNIT_TEST( testCommitUpdatedMember );
  CPPUNIT_TEST( testCommitAddedApproval );
  CPPUNIT_TEST( testCommitDeletedApproval );
  CPPUNIT_TEST_SUITE_END();
  
public:
  // ==========
  // Test methods
  // ==========
  void testCommitUpdatedMember( void );
  void testCommitAddedApproval( void );
  void testCommitDeletedApproval( void );
  
protected:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "user"; }

};

#endif // __MEMBER_REPOSITORY_COMMIT_UNIT_TEST_H__
