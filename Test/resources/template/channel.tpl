<!DOCTYPE html>
<html>

<head>
    <title>{{CHANNEL_NAME}}</title>
    <meta name="copyright" content="&#169; 2020 Thorsten Steube" />
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <!--meta name="viewport" content="initial-scale=0.8, minimum-scale=0.5 maximum-scale=2.0 user-scalable=0" /-->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">{% include "js/channel.js" %}</script>
    <style type="text/css">{% include "css/channel.css" %}</style>
    <link rel="stylesheet" href="/css/device.css" />
</head>

<body>
  {% if exists("HAS_UNSECURE") %}
  <!--img style="float:left;padding-left:5px;" src="/img/unlocked.png" /-->
  {% endif %}
  <article>
      <time pubdate datetime="{{HEADER_CREATE_ISO_TIME}}"></time>
      <header>
          {% if exists("HAS_INVALID_SESSION_KEY") %}
            <h3 style="color:var(--conflict-color); text-align:center">{{INVALID_SESSION_KEY}}</h3>
            <small style="color:var(--conflict-color)">
              <p>{{INVALID_SESSION_KEY_TEXT1}}</p>
              <p>{{INVALID_SESSION_KEY_TEXT2}}</p>
            </small>
          {% endif %}
      </header>
      {% if exists("HAS_MEMBER") %}
        <aside id="aside">
            <div class="action enabled">
            <a href="../action/editChannelMemberList:{{HEADER_ID}}/">
                <div style="text-align:center;"
                 {% if exists("MEMBERLIST_HAS_CONFLICT") %}class="conflict"{% endif %}
                 {% if exists("MEMBERLIST_WAS_UPDATED") %}class="updated"{% endif %}
                 >
                    {{MEMBER_TITLE}}
                </div>
            </a>
            </div>
            <hr/>
            <dl>
## for m in MEMBER
                <dt class="action enabled
                  {% if existsIn(m, "MEMBER_HAS_CONFLICT") %}class="conflict"{% endif %}
                  {% if existsIn(m, "MEMBER_WAS_UPDATED") %}class="updated"{% endif %}
                  >
                      {{m.MEMBERSHIP}} <a href="../action/editChannelMember:{{m.USERNAME}}/">{{m.FULLNAME}}</a></dt>
                <dd>
                    {% if existsIn(m, "HAS_TAGS") %}{{m.MEMBER_TAGS}}<br/>{% endif %}
                    {{m.ROLE_LABEL}}
                </dd>
## endfor
## for s in SUMMARY
                <dt>{{s.TITLE}}</dt>
                <dd>{{s.DESCRIPTION}}</dd>
## endfor
            </dl>
        </aside>
      {% endif %}
      <a name="EJChannelHeader_{{HEADER_ID}}">
          <h3 style="white-space:normal;text-align:center">{{CHANNEL_NAME}}</h3>
      </a>
      <div class="metadata">
          {{CHANNEL_ROLE}}
      </div>
      <div>
          <div class="content
              {% if exists("HEADER_HAS_CONFLICT") %}class="conflict"{% endif %}
              {% if exists("HEADER_WAS_UPDATED") %}class="updated"{% endif %}
              >
            {% if exists("HEADER_SELECTED_RESOURCE") %}
              <a href="../action/channelAttachmentList:{{HEADER_ID}}/">
                  <img src="../resource/{{HEADER_SELECTED_RESOURCE.ID}}?type={{HEADER_SELECTED_RESOURCE.MIMETYPE}}&thumb=1"
                       alt="{{HEADER_SELECTED_RESOURCE.CAPTION}}"
                       {% if exists("HEADER_SELECTED_RESOURCE.MEDIA_HAS_CONFLICT") %}class="conflict"{% endif %}
                       {% if exists("HEADER_SELECTED_RESOURCE.MEDIA_WAS_UPDATED") %}class="updated"{% endif %}
                       />
              </a>
            {% endif %}
            <div class="metadata">
                <ul>
                {% if exists("UNSECURE") %}<li style="color:var(--conflict-color)">{{UNSECURE}}</li>{% endif %}
                {% if exists("CHANNEL_STATUS_LINE0") %}<li>{{CHANNEL_STATUS_LINE0}}</li>{% endif %}
                {% if exists("CHANNEL_STATUS_LINE1") %}<li>{{CHANNEL_STATUS_LINE1}}</li>{% endif %}
                {% if exists("CHANNEL_STATUS_LINE2") %}<li>{{CHANNEL_STATUS_LINE2}}</li>{% endif %}
                {% if exists("HEADER_STATUS_LINE3") %}<li>{{HEADER_STATUS_LINE3}}</li>{% endif %}
                {% if exists("HEADER_STATUS_LINE4") %}<li>{{HEADER_STATUS_LINE4}}</li>{% endif %}
                {% if exists("HEADER_STATUS_LINE5") %}<li>{{HEADER_STATUS_LINE5}}</li>{% endif %}
                </ul>
            </div>
            <p>{{HEADER_CONTENT}}&nbsp;</p>
          </div>
          <nav>
              <table>
                  <tr>
                      <td align="center" class="action {% if exists("HEADER_HAS_ATTACHMENTS") %}enabled{% endif %} {% if exists("CHANNEL_MEDIA_HAS_CONFLICT") %}conflict{% endif %}">
                          {% if exists("HEADER_HAS_ATTACHMENTS") %}
                            <a href="../action/channelAttachmentList:{{HEADER_ID}}/">{{ATTACHMENT_LABEL}}</a>
                          {% endif %}
                      </td>
                      <td align="center" class="action {% if exists("HEADER_IS_ADMIN") %}enabled{% endif %} {% if exists("HEADER_HAS_CONFLICT") %}conflict{% endif %}">
                          {% if exists("HEADER_IS_ADMIN") %}
                            <a href="../action/editHeader:{{HEADER_ID}}/">{{EDIT_LABEL}}</a>
                          {% endif %}
                      </td>
                      <td align="center" class="action {% if exists("HEADER_IS_WRITER") %}enabled{% endif %}">
                          {% if exists("HEADER_IS_WRITER") %}
                            <a href="../action/addPost:{{HEADER_ID}}/">{{ADD_POST_LABEL}}</a>
                          {% endif %}
                      </td>
                  </tr>
              </table>
          </nav>
## for p in POST
          <article>
              <div class="content {% if existsIn(p, "POST_HAS_CONFLICT") %}conflict{% endif %} {% if existsIn(p, "POST_WAS_UPDATED") %}updated{% endif %}">
                {% if existsIn(p, "POST_SELECTED_RESOURCE") %}
                  <a href="../action/postAttachmentList:{{p.POST_ID}}/">
                      <img src="../resource/{{p.POST_SELECTED_RESOURCE.ID}}?type={{p.POST_SELECTED_RESOURCE.MIMETYPE}}&thumb=1"
                           alt="{{p.POST_SELECTED_RESOURCE.CAPTION}}"
                           {% if existsIn(p, "POST_SELECTED_RESOURCE.MEDIA_HAS_CONFLICT") %}class="conflict"{% endif %}
                           {% if existsIn(p, "POST_SELECTED_RESOURCE.MEDIA_WAS_UPDATED") %}class="updated"{% endif %}
                           />
                  </a>
                {% endif %}
                  <a name="EJChannelPost_{{p.POST_ID}}"/></a>
                  <p>{{p.POST_CONTENT}}&nbsp;</p>
                  <div class="metadata">
                    <ul>
                      {% if existsIn(p, "POST_STATUS_LINE0") %}<li>{{p.POST_STATUS_LINE0}}</li>{% endif %}
                      {% if existsIn(p, "POST_STATUS_LINE1") %}<li>{{p.POST_STATUS_LINE1}}</li>{% endif %}
                      {% if existsIn(p, "POST_STATUS_LINE2") %}<li>{{p.POST_STATUS_LINE2}}</li>{% endif %}
                      {% if existsIn(p, "POST_STATUS_LINE3") %}<li>{{p.POST_STATUS_LINE3}}</li>{% endif %}
                      {% if existsIn(p, "POST_STATUS_LINE4") %}<li>{{p.POST_STATUS_LINE4}}</li>{% endif %}
                      {% if existsIn(p, "POST_STATUS_LINE5") %}<li>{{p.POST_STATUS_LINE5}}</li>{% endif %}
                      {% if existsIn(p, "POST_STATUS_LINE6") %}<li>{{p.POST_STATUS_LINE6}}</li>{% endif %}
                    </ul>
                  </div>
              </div>
              <nav>
                  <table>
                      <tr>
                          <td align="center" class="action {% if existsIn(p, "POST_HAS_ATTACHMENTS") %}enabled{% endif %} {% if existsIn(p, "POST_MEDIA_HAS_CONFLICT") %}conflict{% endif %}">
                              {% if existsIn(p, "POST_HAS_ATTACHMENTS") %}
                                <a href="../action/postAttachmentList:{{p.POST_ID}}/">{{p.ATTACHMENT_LABEL}}</a>
                              {% endif %}
                          </td>
                          <td align="center" class="action {% if existsIn(p, "POST_IS_ADMIN") %}enabled{% endif %} {% if existsIn(p, "POST_HAS_CONFLICT") %}conflict{% endif %}">
                              {% if existsIn(p, "POST_IS_ADMIN") %}
                                <a href="../action/editPost:{{p.POST_ID}}/">{{p.EDIT_LABEL}}</a>
                              {% endif %}
                          </td>
                          <td align="center" class="action {% if existsIn(p, "POST_IS_WRITER") %}enabled{% endif %}">
                              {% if existsIn(p, "POST_IS_WRITER") %}
                                <a href="../action/addComment:{{p.POST_ID}}/">{{p.ADD_COMMENT_LABEL}}</a>
                              {% endif %}
                          </td>
                      </tr>
                  </table>
              </nav>
## for c in p.POST_COMMENT
              <article>
                  <div class="content {% if existsIn(c, "COMMENT_HAS_CONFLICT") %}conflict{% endif %} {% if existsIn(c, "COMMENT_WAS_UPDATED") %}updated{% endif %}">
                    {% if existsIn(c, "COMMENT_SELECTED_RESOURCE") %}
                      <a href="../action/commentAttachmentList:{{c.COMMENT_ID}}/">
                          <img src="../resource/{{c.COMMENT_SELECTED_RESOURCE.ID}}?type={{c.COMMENT_SELECTED_RESOURCE.MIMETYPE}}&thumb=1"
                               alt="{{c.COMMENT_SELECTED_RESOURCE.CAPTION}}"
                               {% if existsIn(c, "COMMENT_SELECTED_RESOURCE.MEDIA_HAS_CONFLICT") %}class="conflict"{% endif %}
                               {% if existsIn(c, "COMMENT_SELECTED_RESOURCE.MEDIA_WAS_UPDATED") %}class="updated"{% endif %}
                               />
                      </a>
                    {% endif %}
                      <a name="EJChannelComment_{{c.COMMENT_ID}}"/></a>
                      <p>{{c.COMMENT_CONTENT}}&nbsp;</p>
                      <div class="metadata">
                        <ul>
                          {% if existsIn(c, "COMMENT_STATUS_LINE0") %}<li>{{c.COMMENT_STATUS_LINE0}}</li>{% endif %}
                          {% if existsIn(c, "COMMENT_STATUS_LINE1") %}<li>{{c.COMMENT_STATUS_LINE1}}</li>{% endif %}
                          {% if existsIn(c, "COMMENT_STATUS_LINE2") %}<li>{{c.COMMENT_STATUS_LINE2}}</li>{% endif %}
                          {% if existsIn(c, "COMMENT_STATUS_LINE3") %}<li>{{c.COMMENT_STATUS_LINE3}}</li>{% endif %}
                          {% if existsIn(c, "COMMENT_STATUS_LINE4") %}<li>{{c.COMMENT_STATUS_LINE4}}</li>{% endif %}
                          {% if existsIn(c, "COMMENT_STATUS_LINE5") %}<li>{{c.COMMENT_STATUS_LINE5}}</li>{% endif %}
                          {% if existsIn(c, "COMMENT_STATUS_LINE6") %}<li>{{c.COMMENT_STATUS_LINE6}}</li>{% endif %}
                        </ul>
                      </div>
                  </div>
                  <nav>
                      <table>
                          <tr>
                              <td align="center" class="action {% if existsIn(c, "COMMENT_HAS_ATTACHMENTS") %}enabled{% endif %} {% if existsIn(c, "COMMENT_MEDIA_HAS_CONFLICT") %}conflict{% endif %}">
                                  {% if existsIn(c, "COMMENT_HAS_ATTACHMENTS") %}
                                    <a href="../action/commentAttachmentList:{{c.COMMENT_ID}}/">{{c.ATTACHMENT_LABEL}}</a>
                                  {% endif %}
                              </td>
                              <td align="center" class="action {% if existsIn(c, "COMMENT_IS_ADMIN") %}enabled{% endif %} {% if existsIn(c, "COMMENT_HAS_CONFLICT") %}conflict{% endif %}">
                                  {% if existsIn(c, "COMMENT_IS_ADMIN") %}
                                    <a href="../action/editComment:{{c.COMMENT_ID}}/">{{c.EDIT_LABEL}}</a>
                                  {% endif %}
                              </td>
                              <td>&nbsp;</td>
                          </tr>
                      </table>
                  </nav>
            </article>
## endfor
          </article>
## endfor
      </div>
  </article>
  <div id="debug"></div>
</body>

</html>
