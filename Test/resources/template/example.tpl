{{>STYLES}}

Hello {{OWNER}},
   You have just won ${{VALUE}}!
   {{#IN_CA}}Well, ${{TAXED_VALUE}}, after taxes.{{/IN_CA}}

Last five searches:<ol>
{{#MEMBER}}
<li>{{USERNAME}}</li>
{{/MEMBER}}
</ol>
