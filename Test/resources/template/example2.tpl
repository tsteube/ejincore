{% include "include2.tpl" %}

Hello {{ OWNER }},
   You have just won ${{ VALUE }}!
   {% if exists("IN_CA") %}Well, ${{ TAXED_VALUE }}, after taxes.{% endif %}

Last five searches:<ol>
## for m in MEMBERS
  <li>{{ m.NAME }}</li>
## endfor
</ol>
