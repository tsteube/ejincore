<!DOCTYPE html>
<html>
  <head>
    <title>{{CHAT_NAME}}</title>
    <meta name="copyright" content="&#169; 2020 Thorsten Steube" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=0.8, minimum-scale=0.5 maximum-scale=2.0 user-scalable=1" />
    <!--meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" /-->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">{% include "js/chat.js" %}</script>
    <style type="text/css">{% include "css/chat.css" %}</style>
    <link rel="stylesheet" href="/css/device.css" />
  </head>
  <body>
   <section>
## for m in MESSAGE
      {% if existsIn(m, "DELIMITER") %}
        <div class="hline"><span>{{m.DELIMITER}}</span></div>
      {% endif %}
      <div
        {% if existsIn(m, "HAS_MESSAGE_FROM_ME") %}class="from-me"{% endif %}
        {% if existsIn(m, "HAS_MESSAGE_FROM_THEM") %}class="from-them"{% endif %}
        >
        {{m.CONTENT}}
      </div>
      <div class="clear"/>
        {% if existsIn(m, "CAPTION") %}
        <div
          {% if existsIn(m, "HAS_MESSAGE_FROM_ME") %}class="from-me-caption"{% endif %}
          {% if existsIn(m, "HAS_MESSAGE_FROM_THEM") %}class="from-them-caption"{% endif %}
          >
        {{m.CAPTION}}
        </div>
        <div class="clear"/>
      {% endif %}
      {% if existsIn(m, "ERROR_CAPTION") %}
        <div style="color:var(--conflict-color)"
          {% if existsIn(m, "HAS_MESSAGE_FROM_ME") %}class="from-me-caption"{% endif %}
          {% if existsIn(m, "HAS_MESSAGE_FROM_THEM") %}class="from-them-caption"{% endif %}
        >
          {{m.ERROR_CAPTION}}
        </div>
        <div class="clear"/>
      {% endif %}
      <a name="no_{{m.NO}}"/>
## endfor
   </section>
   <a name="no_0"/><!-- new message -->
  </body>
</html>
