/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelServiceTest.h"

#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelService.h"
#include "Modifications.h"
#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelServiceTest );

void ChannelServiceTest::testExportChannel_Unchanged( void )
{  
  Channel channel( 100, string( "100" ), false );
  
  // TEST
  string anchor( this->channelService_->exportModifiedChannel( 100, channel ) );
  
  // post conditions
  CPPUNIT_ASSERT( anchor.empty() );
}
void ChannelServiceTest::testExportChannel_Updated( void )
{
  Channel channel( 41, string( "41" ), false );
  
  // TEST
  string anchor( this->channelService_->exportModifiedChannel( 41, channel ) );
  
  // post conditions
  CPPUNIT_ASSERT_EQUAL( string( "10" ), anchor );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel.getOperation() );
  CPPUNIT_ASSERT( channel.getHeader() == NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel.getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel.getComments().size() );
}
void ChannelServiceTest::testExportChannel_Conflict( void )
{
  Channel channel( 11, string( "11" ), false );
  
  // TEST
  string anchor( this->channelService_->exportModifiedChannel( 11, channel ) );
  
  CPPUNIT_ASSERT_EQUAL( string( "" ), anchor );
}
void ChannelServiceTest::testImportChannel( void )
{
  // pre conditions
  shared_ptr<Channel> container( this->channelService_->exportChannel( string( "100" ) ) );
  container->setSyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT( container->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, container->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, container->getComments().size() );
  
  // preparation
  
  // update header
  container->getHeader()->setModifiedBy( "owner" );
  container->getHeader()->setContent( "hello world" );
  // no post update
  container->getPosts().clear();
  // no comment updates
  container->getComments().clear();
  
  // TEST
  container->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelService_->importChannelUpdates( *container, "owner", jtime() ) );
  
  // pre conditions
  container = this->channelService_->exportChannel( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), container->getHeader()->getContent() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChannelServiceTest::testImportChannel_Conflict( void )
{
  // pre conditions
  shared_ptr<Channel> container( this->channelService_->exportChannel( string( "41" ) ) );
  container->setSyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT( container->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, container->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, container->getComments().size() );
  
  // preparation
  
  // no header update
  container->clearHeader( );
  // no post update
  container->getPosts().clear();
  
  // remove comment
  shared_ptr<ChannelComment> comment = *container->getComments().begin();
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  comment->setModifiedBy( "owner" );
  
  // TEST
  container->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( ! this->channelService_->importChannelUpdates( *container, "owner", jtime() ) );
  
  // pre conditions
  container = this->channelService_->exportChannel( string( "41" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, container->getComments().size() );
  CPPUNIT_ASSERT( ejin::kSyncIndConflict == (*container->getComments().begin())->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
void ChannelServiceTest::testExportAllModifications( void )
{
  // preparation
  Modifications<ChannelHeader> container;
  
  CPPUNIT_ASSERT_EQUAL( string( "10" ), this->channelService_->exportModifiedChannelHeader( container ) );
  
  CPPUNIT_ASSERT_EQUAL( string( "10" ), container.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string("owner"), container.getUsername() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, container.getAdded().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 8, container.getUpdated().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, container.getRemoved().size() );
}
void ChannelServiceTest::testImportAllModifications( void )
{
  // preparation
  Modifications<ChannelHeader> container;
  container.setSyncAnchor( string( "10" ) );
  
  // add channel
  shared_ptr<ChannelHeader> header( new ChannelHeader() );
  header->setGid( string( "99" ) );
  header->setOwner( "owner" );
  header->setName( "new channel" );
  header->setContent( "hello world" );
  header->setModifiedBy( "owner" );
  header->setSyncAnchor( "10" );
  header->setState( 0 );
  container.getAdded().push_back( header );
  
  // update channel
  header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setGid( string( "100" ) );
  header->setOwner( "owner" );
  header->setName( "updated channel" );
  header->setContent( "hello world" );
  header->setModifiedBy( "owner" );
  header->setSyncAnchor( "10" );
  header->setState( 0 );
  container.getUpdated().push_back( header );
  
  // remove channel
  header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setGid( string( "41" ) );
  header->setModifiedBy( "owner" );
  header->setSyncAnchor( "10" );
  container.getRemoved().push_back( header );
  
  CPPUNIT_ASSERT( this->channelService_->importUpdatedChannelHeader( container, jtime() ).empty() );
  
  // pre conditions
  shared_ptr<Channel> channel( this->channelService_->exportChannel( string( "99" ) ) );
  CPPUNIT_ASSERT( channel != NULL );
  
  channel = this->channelService_->exportChannel( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), channel->getHeader()->getContent() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 3, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationOutdated, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );

  try {
    channel = this->channelService_->exportChannel( string( "41" ) );
  } catch (ejin::data_retrieval_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}
