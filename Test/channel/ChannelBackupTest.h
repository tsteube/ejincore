/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_BACKUP_UNIT_TEST_H__
#define __CHANNEL_BACKUP_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelBackupTest : public AbstractChannelTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelBackupTest );
  CPPUNIT_TEST( testRestoreConflict );
  CPPUNIT_TEST( testRestoreConflict_Attachment );
  CPPUNIT_TEST( testRestoreUpdateMetadataOnly );
  CPPUNIT_TEST( testRestoreInsert );
  CPPUNIT_TEST( testRestoreUpdate );
  CPPUNIT_TEST( testRestoreRemove );
  CPPUNIT_TEST_SUITE_END();

public:
  void setUp( void );

  // ==========
  // Test methods
  // ==========
public:
  
  void testRestoreInsert( void );
  void testRestoreUpdateMetadataOnly( void );
  void testRestoreUpdate( void );
  void testRestoreRemove( void );
  void testRestoreConflict( void );
  void testRestoreConflict_Attachment( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }

private:
  
  shared_ptr<ChannelHeader> loadHeaderById( integer id );
  shared_ptr<ChannelPost> loadPostById( integer id );
  shared_ptr<ChannelComment> loadCommentById( integer id );
  shared_ptr<ChannelMember> loadChannelMemberById( integer id );
  shared_ptr<Media> loadMediaById( integer id );
  shared_ptr<Attachment> loadAttachment( const Value& mediaId, bool hidden ) const;

};

#endif // __CHANNEL_BACKUP_UNIT_TEST_H__
