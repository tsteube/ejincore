/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "AbstractChannelTestFixture.h"

#include <iostream>
#include <copyfile.h>
#include <sys/time.h>

#include "DBUnit.h"
#include "EjinDatabase.h"
#include "Attachment.h"
#include "ChannelService.h"
#include "ChangeLogRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelCommentRepository.h"
#include "SqliteValue.h"

#define PASSWORD "test"

// ==========
// Test Setup/Teardown
// ==========

void AbstractChannelTestFixture::setUp( void )
{
  gettimeofday(&tval_start_, NULL);
  
  this->db_.reset();
  this->dbunit_.reset();
  this->changeLogRepository_.reset();
  this->channelMediaRepository_.reset();
  this->channelPostRepository_.reset();
  this->channelHeaderRepository_.reset();
  this->channelCommentRepository_.reset();
  this->channelMemberRepository_.reset();
  this->channelService_.reset();
  
  unique_ptr<ejin::Database> db( unique_ptr<ejin::Database>(new ejin::Database("test")) );
  const string mainDb = TestResourcePath + __EJIN_TEST_MAIN_DB_NAME;
  const string attachmentDb = TestResourcePath + __EJIN_TEST_ATTACHMENT_DB_NAME;
  const string testMainDb = TestResourcePath + "ejin(test).db";
  const string testAttachmentDb = TestResourcePath + "attachment(test).db";
  if ( db->open(__EJIN_SCHEMA_VERSION,
                mainDb.c_str(), testMainDb.c_str(),
                attachmentDb.c_str(), testAttachmentDb.c_str(),
                "test.key", "private.key",
                PASSWORD, true ) == ejin::Database::Status::DatabaseOpen ) {
    db->attachBackupDb( PASSWORD );
    db->attachArchiveDb( PASSWORD, true );
    this->db_ = std::move(db);
    
    // setup database with samples
    this->dbunit_ = unique_ptr<DBUnit>(new DBUnit(*db_.get()));
    
    // create object to test
    this->changeLogRepository_ = unique_ptr<ChangeLogRepository>(new ChangeLogRepository(*db_));
    this->approvedMemberRepository_ = unique_ptr<ApprovedMemberRepository>(new ApprovedMemberRepository(*db_, *this->changeLogRepository_));
    this->channelMemberRepository_ = unique_ptr<ChannelMemberRepository>(new ChannelMemberRepository(*db_, *this->changeLogRepository_, *this->approvedMemberRepository_));
    this->channelMediaRepository_ = unique_ptr<ChannelMediaRepository>(new ChannelMediaRepository(*db_, *this->changeLogRepository_));
    this->channelCommentRepository_ = unique_ptr<ChannelCommentRepository>(new ChannelCommentRepository(*db_, *this->changeLogRepository_, *channelMediaRepository_));
    this->channelPostRepository_ = unique_ptr<ChannelPostRepository>(new ChannelPostRepository(*db_, *this->changeLogRepository_, *channelCommentRepository_, *channelMediaRepository_));
    this->channelHeaderRepository_ = unique_ptr<ChannelHeaderRepository>(new ChannelHeaderRepository(*db_, *changeLogRepository_ , *channelMemberRepository_, *channelPostRepository_, *channelCommentRepository_, *channelMediaRepository_));
    this->channelService_ = unique_ptr<ChannelService>(new ChannelService(*db_, *channelHeaderRepository_, *channelPostRepository_, *channelCommentRepository_, *channelMediaRepository_, *channelMemberRepository_));
    // start transaction
    this->db_->transactionBegin();
    this->dbunit_->setupTables(this->getDbConfigDirectory(), true);
  }
  
  this->_TEST_CHANNEL.setId( 1L );
  this->_TEST_CHANNEL.setGid( string( "1" ) );
  this->_TEST_CHANNEL.setOwner( "owner" );
  this->_TEST_CHANNEL.setModifiedBy( "owner" );
  this->_TEST_CHANNEL.setSyncAnchor( string( "100" ) );
  this->_TEST_CHANNEL.setLastSyncTime( jtime() );
  this->_TEST_CHANNEL.setSyncTime( jtime() );
  gettimeofday(&tval_before_, NULL);
  
  this->changeLogRepository_->begin();
}

void AbstractChannelTestFixture::tearDown( void )
{
  struct timeval tval_after, ttest_result;
  gettimeofday(&tval_after, NULL);
  timersub(&tval_after, &tval_before_, &ttest_result);

  if (db_.get() != NULL) {
    if (db_->isClean()) {
      // commit transaction
      this->db_->transactionCommit();
    } else {
      // rollback transaction
      this->db_->transactionRollback();
    }
  }
  
  this->db_->detachArchiveDb( );
  this->db_->detachBackupDb( );
  this->db_.reset();
  this->dbunit_.reset();
  this->channelMediaRepository_.reset();
  this->channelPostRepository_.reset();
  this->channelHeaderRepository_.reset();
  this->channelCommentRepository_.reset();
  this->channelMemberRepository_.reset();
  this->channelService_.reset();

  struct timeval tval_end, ttotal_result;
  gettimeofday(&tval_end, NULL);
  timersub(&tval_end, &tval_start_, &ttotal_result);
  
  printf(" %ld.%06ld (%ld.%06ld in total)",
         (long int)ttest_result.tv_sec, (long int)ttest_result.tv_usec,
         (long int)ttotal_result.tv_sec, (long int)ttotal_result.tv_usec);
}
