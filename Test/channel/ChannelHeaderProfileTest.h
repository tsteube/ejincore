/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_HEADER_PROFILE_UNIT_TEST_H__
#define __CHANNEL_HEADER_PROFILE_UNIT_TEST_H__

#include "Declarations.h"
#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelHeaderProfileTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelHeaderProfileTest );  
  CPPUNIT_TEST( testAddedChannelProfile );  
  CPPUNIT_TEST( testModifiedChannelProfile );
  CPPUNIT_TEST( testRemovedChannelProfile );
  CPPUNIT_TEST( testConflictedChannelProfile );
  CPPUNIT_TEST( testOutDatedChannelProfile );
  CPPUNIT_TEST( testChannelInvitations );  
  CPPUNIT_TEST( testChannelToAccept );  
  CPPUNIT_TEST( testChannelToReject );  
  CPPUNIT_TEST( testChannelMembership );  
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testAddedChannelProfile( void );
  void testModifiedChannelProfile( void );
  void testRemovedChannelProfile( void );
  void testConflictedChannelProfile( void );
  void testOutDatedChannelProfile( void );
  void testChannelInvitations( void );
  void testChannelToAccept( void );
  void testChannelToReject( void );
  void testChannelMembership( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
  
  // ==========
  // Private Interface
  // ==========
private:
  
  bool compareLists(list< shared_ptr<ChannelHeader> >& headers, const char* gid, ...) ;
  
};

#endif // __CHANNEL_HEADER_PROFILE_UNIT_TEST_H__
