/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelRepositoryTest.h"

#include "Channel.h"
#include "ChannelService.h"
#include "ChannelHeader.h"
#include "ChangeLog.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelRepositoryTest );

// ==========
// TEST Methods
// ==========

void ChannelRepositoryTest::testDetectConflict( void )
{
  // 1. TEST
  CPPUNIT_ASSERT( this->channelService_->detectChannelConflict( string( "11" ) ) );
  CPPUNIT_ASSERT( this->channelService_->detectChannelConflict( string( "12" ) ) );
  CPPUNIT_ASSERT( this->channelService_->detectChannelConflict( string( "13" ) ) );
  
  // 2. TEST: clear conflicts
  CPPUNIT_ASSERT( this->channelService_->clearConflict( string( "11" ) ) );
  CPPUNIT_ASSERT( this->channelService_->clearConflict( string( "12" ) ) );
  CPPUNIT_ASSERT( this->channelService_->clearConflict( string( "13" ) ) );
  CPPUNIT_ASSERT( this->channelService_->clearConflict( string( "15" ) ) );
  CPPUNIT_ASSERT( this->channelService_->clearConflict( string( "16" ) ) );
  
  // 1. TEST again
  CPPUNIT_ASSERT( ! this->channelService_->detectChannelConflict( string( "11" ) ) );
  CPPUNIT_ASSERT( ! this->channelService_->detectChannelConflict( string( "12" ) ) );
  CPPUNIT_ASSERT( ! this->channelService_->detectChannelConflict( string( "13" ) ) );
}

void ChannelRepositoryTest::testLoadModifications_Unchanged( void )
{
  // TEST 1
  shared_ptr<Channel> channel( this->channelService_->loadModificiations( 100 ));
  
  // post conditions
  CPPUNIT_ASSERT( channel == NULL );
}

void ChannelRepositoryTest::testLoadModifications_Add( void )
{
  // TEST 1
  shared_ptr<Channel> channel(this->channelService_->loadModificiations( 21 ));
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "21" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncInsert, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getComments().size() );
  
  // TEST 2
  channel = this->channelService_->loadModificiations( 22 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "22" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncInsert, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
}

void ChannelRepositoryTest::testLoadModifications_Remove( void )
{
  // TEST 1
  shared_ptr<Channel> channel(this->channelService_->loadModificiations( 31 ));
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "31" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncRemove, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 31, channel->getHeader()->getId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
  
  // TEST 2
  channel = this->channelService_->loadModificiations( 32 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "32" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncRemove, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 32, channel->getHeader()->getId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
}

void ChannelRepositoryTest::testLoadModifications_Update( void )
{
  // TEST 1
  shared_ptr<Channel> channel(this->channelService_->loadModificiations( 41 ));
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "41" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() == NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getComments().size() );
  
  // TEST 2
  channel = this->channelService_->loadModificiations( 42 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "42" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() == NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
  
  // TEST 3
  channel = this->channelService_->loadModificiations( 43 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "43" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
}

void ChannelRepositoryTest::testLoadModifications_MediaChanges( void )
{
  // TEST 1
  shared_ptr<Channel> channel(this->channelService_->loadModificiations( 51 ));
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "51" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() == NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getComments().size() );
  
  // TEST 2
  channel = this->channelService_->loadModificiations( 52 );
  
  // post conditions
  CPPUNIT_ASSERT( channel == NULL );
  
  // TEST 3
  channel = this->channelService_->loadModificiations( 53 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "53" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
}

void ChannelRepositoryTest::testLoadModifications_MemberChanges( void )
{
  // TEST 1
  shared_ptr<Channel> channel(this->channelService_->loadModificiations( 61 ));
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "61" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
  
  // TEST 2
  channel = this->channelService_->loadModificiations( 62 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "62" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
  
  // TEST 3
  channel = this->channelService_->loadModificiations( 63 );
  
  // post conditions
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "63" ), channel->getGid() );
  CPPUNIT_ASSERT_EQUAL( ejin::kSyncUpdate, channel->getOperation() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
}

void ChannelRepositoryTest::testAddChannel( void )
{
  // pre conditions
  shared_ptr<Channel> channel(this->channelService_->exportChannel( string( "1" ) ));
  
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "1" ), channel->getGid() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getComments().size() );
  
  // preparation
  channel->setSyncAnchor( string( "100" ) );
  channel->setSyncTime( jtime() );
  
  // update channel gid
  channel->setHeader( shared_ptr<ChannelHeader>(new ChannelHeader( *channel->getHeader() ) ) );
  channel->getHeader()->clearId( );
  channel->getHeader()->setGid( string( "99" ) );
  channel->getHeader()->setName( string( "new channel" ) );
  channel->getHeader()->setCreateTime( jtime() );
  
  // update post gid
  shared_ptr<ChannelPost> post(* channel->getPosts().begin());
  post->clearId( );
  post->setGid( string( "99" ) );
  post->setCreateTime( jtime() );
  
  // update comment gid
  shared_ptr<ChannelComment> comment(* channel->getComments().begin());
  comment->clearId( );
  comment->setGid( string( "99" ) );
  comment->setPostGid( string( "99" ) );
  comment->setCreateTime( jtime() );
  
  // TEST
  channel->setOperation( ejin::kSyncInsert );
  this->channelService_->update( *channel, "owner", jtime() );
  
  // pre conditions
  channel = this->channelService_->exportChannel( string( "99" ) );
  
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getComments().size() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), channel->getHeader()->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), (*channel->getPosts().begin())->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), (*channel->getComments().begin())->getGid() );
  
  jtime now;
  // TEST
  channel->setOperation( ejin::kSyncInsert );
  this->channelService_->commit( *channel, string( "100" ), string( "owner" ), now );
  
  // pre conditions
  channel = this->channelService_->exportChannel( string( "99" ) );
  
  CPPUNIT_ASSERT_EQUAL(string( "100" ), channel->getHeader()->getSyncAnchor() );
  CPPUNIT_ASSERT( channel->getHeader()->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, channel->getHeader()->getModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChannelRepositoryTest::testUpdateChannel( void )
{
  // pre conditions
  shared_ptr<Channel> channel(this->channelService_->exportChannel( string( "1" ) ));
  
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT_EQUAL(string( "1" ), channel->getGid() );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, channel->getComments().size() );
  
  // preparation
  channel->setSyncAnchor( string( "100" ) );
  channel->setSyncTime( jtime() );
  
  // update channel gid
  channel->getHeader()->setContent( "hello world" );
  channel->getHeader()->setModifyTime( jtime() );
  channel->getHeader()->setModifiedBy( "owner" );
  
  // add post
  shared_ptr<ChannelPost> post(shared_ptr<ChannelPost>(new ChannelPost()));
  post->setContent( "test" );
  post->setOwner( "owner" );
  post->setGid( "99" );
  post->setModifiedBy( "owner" );
  post->setModifyTime( jtime() );
  post->setState( 0 );
  channel->getPosts().push_back( post );
  
  // remove comment
  shared_ptr<ChannelComment> comment(* channel->getComments().begin());
  comment->clearContent( );
  comment->setModifyTime( jtime() );
  comment->setModifiedBy( "owner" );
  comment->setOperation( ejin::kSyncRemove );
  
  // TEST
  channel->setOperation( ejin::kSyncUpdate );
  this->channelService_->update( *channel, "owner", jtime() );
  
  // pre conditions
  channel = this->channelService_->exportChannel( string( "1" ) );
  
  CPPUNIT_ASSERT( channel != NULL );
  CPPUNIT_ASSERT( channel->getHeader() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), channel->getHeader()->getContent() );
  CPPUNIT_ASSERT( channel->getHeader()->hasSyncTime() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, channel->getPosts().size() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, channel->getComments().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 3, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChannelRepositoryTest::testDeleteChannel( void )
{
  // pre conditions
  shared_ptr<Channel> channel(this->channelService_->exportChannel( string( "1" ) ));
  
  // preparation
  channel->setSyncAnchor( string( "100" ) );
  
  // TEST
  channel->setOperation( ejin::kSyncRemove );
  this->channelService_->update( *channel, "owner", jtime() );
  
  // pre conditions
  try {
    channel = this->channelService_->exportChannel( string( "1" ) );
  } catch (ejin::data_retrieval_error e) {
  }    
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string("owner") , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

