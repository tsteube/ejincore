/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_REPOSITORY_UNIT_TEST_H__
#define __CHANNEL_REPOSITORY_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelRepositoryTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelRepositoryTest );  
  CPPUNIT_TEST( testDetectConflict );  
  CPPUNIT_TEST( testLoadModifications_Unchanged );  
  CPPUNIT_TEST( testLoadModifications_Add );  
  CPPUNIT_TEST( testLoadModifications_Remove );  
  CPPUNIT_TEST( testLoadModifications_Update );
  CPPUNIT_TEST( testLoadModifications_MediaChanges );
  CPPUNIT_TEST( testLoadModifications_MemberChanges );
  CPPUNIT_TEST( testAddChannel );
  CPPUNIT_TEST( testUpdateChannel );  
  CPPUNIT_TEST( testDeleteChannel );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testDetectConflict( void );
  void testLoadModifications_Unchanged( void );
  void testLoadModifications_Add( void );
  void testLoadModifications_Remove( void );
  void testLoadModifications_Update( void );
  void testAddChannel( void );
  void testUpdateChannel( void );
  void testLoadModifications_MediaChanges( void );
  void testLoadModifications_MemberChanges( void );
  void testDeleteChannel( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
};

#endif // __CHANNEL_REPOSITORY_UNIT_TEST_H__
