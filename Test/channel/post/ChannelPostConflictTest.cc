/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPostConflictTest.h"

#include <sstream>

#include "ChannelService.h"
#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelPost.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelPostConflictTest );

#define INITAL_NUM_OF_POSTS 6

shared_ptr<ChannelPost> ChannelPostConflictTest::loadByGid( integer gid, SyncInd syncInd )
{
  std::stringstream out;
  out << gid;
  
  // initialize channel header
  this->channelHeader_ = shared_ptr<ChannelHeader>( new ChannelHeader( ) );
  this->channelHeader_->setId( gid );
  this->channelHeader_->setGid( out.str() );
  this->channelHeader_->setOwner( "owner" );

  this->channelPosts_.clear();
  list< shared_ptr<ChannelPost> > allPosts = this->channelPostRepository_->loadAll( *this->channelHeader_ );
  if ( syncInd >= 0 ) {
    // remapping
    for (list< shared_ptr<ChannelPost> >::iterator it = allPosts.begin(); it != allPosts.end(); it++) {
      this->channelPosts_.push_back(*it);
    }
    
    CPPUNIT_ASSERT( this->channelPosts_.size() > 0 );
    for (list< shared_ptr<ChannelPost> >::iterator it = this->channelPosts_.begin(); it != this->channelPosts_.end(); it++) {
      if ( (*it)->getGid() == out.str() ) {
        CPPUNIT_ASSERT( syncInd == (*it)->getSyncInd() );
        return (*it);
      }
    }
    CPPUNIT_ASSERT( false );
  } else {
    CPPUNIT_ASSERT( allPosts.size() == 0 );
  }
  return shared_ptr<ChannelPost>();
}

shared_ptr<ChannelPost> ChannelPostConflictTest::validate(integer gid,
                                                                       SyncInd state,
                                                                       long expectedTotalNumOfPosts )
{
  return validate( gid, gid, state, expectedTotalNumOfPosts);
}

shared_ptr<ChannelPost> ChannelPostConflictTest::validate( integer channelGid, 
                                                                        integer gid,
                                                                        SyncInd state,
                                                                        long expectedTotalNumOfPosts )
{
  shared_ptr<ChannelPost> comment( loadByGid( channelGid, state ) );
  bool expectConflict = ( state == kSyncIndConflict );
  std::stringstream out;
  out << gid;
  CPPUNIT_ASSERT( expectConflict == this->channelPostRepository_->detectConflict( out.str() ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfPosts, this->db_->count( "POST_TBL" ) );
  return comment;
}

// ==========
// TEST Methods
// ==========

void ChannelPostConflictTest::testAddChannelPost( void )
{
  loadByGid( 1, kSyncIndSynchronous );

  shared_ptr<ChannelPost> comment = shared_ptr<ChannelPost>(new ChannelPost( "99" ));
  comment->setContent( "test" );
  comment->setOwner( "owner" );
  comment->setOperation( ejin::kSyncInsert );
  this->channelPosts_.push_back( comment );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  validate( 1, 99, kSyncIndSynchronous, INITAL_NUM_OF_POSTS + 1);
}

void ChannelPostConflictTest::testAddChannelPost_Empty( void )
{
  loadByGid( 1, kSyncIndSynchronous );
  
  shared_ptr<ChannelPost> comment = shared_ptr<ChannelPost>(new ChannelPost( "99" ));
  comment->clearContent( );
  comment->setOwner( "owner" );
  comment->setOperation( ejin::kSyncInsert );
  this->channelPosts_.push_back( comment );
  this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() );
  validate( 1, 1, kSyncIndSynchronous, INITAL_NUM_OF_POSTS );
}

void ChannelPostConflictTest::testUpdateChannelPost_LocalUnchanged( void )
{  
  shared_ptr<ChannelPost> comment = loadByGid( 1, kSyncIndSynchronous );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  validate( 1, kSyncIndSynchronous, INITAL_NUM_OF_POSTS );
}

void ChannelPostConflictTest::testUpdateChannelPost_LocalAdd( void )
{  
  loadByGid( 2, kSyncIndInsert );
  try {
    this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() );
  } catch (ejin::data_integrity_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}

void ChannelPostConflictTest::testUpdateChannelPost_LocalUpdate_EqualContent( void )
{  
  shared_ptr<ChannelPost> header = loadByGid( 3, kSyncIndUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  validate( 3, kSyncIndSynchronous, INITAL_NUM_OF_POSTS );
}

void ChannelPostConflictTest::testUpdateChannelPost_LocalUpdate( void )
{  
  // 1. update data
  shared_ptr<ChannelPost> comment = loadByGid( 3, kSyncIndUpdate );
  string content = comment->getContent( );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  comment = validate( 3, kSyncIndConflict, INITAL_NUM_OF_POSTS + 1 );
  CPPUNIT_ASSERT_EQUAL( comment->getContent(), string("update") );

  // 2. update data
  comment->setContent( "update2" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  comment = validate( 3, kSyncIndConflict, INITAL_NUM_OF_POSTS + 1 );
  CPPUNIT_ASSERT_EQUAL( comment->getContent(), string("update2") );

  // 2. remove data
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  comment = validate( 3, kSyncIndConflict, INITAL_NUM_OF_POSTS + 1 );
  CPPUNIT_ASSERT( ! comment->hasContent() );

  // resolve conflict data with original data content
  comment->setContent( content );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  validate( 3, kSyncIndSynchronous, INITAL_NUM_OF_POSTS );
}

void ChannelPostConflictTest::testUpdateChannelPost_LocalDelete_EqualContent( void )
{  
  shared_ptr<ChannelPost> comment = loadByGid( 4, kSyncIndDelete );
  CPPUNIT_ASSERT( ! this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  validate( 4, kSyncIndDelete, INITAL_NUM_OF_POSTS );
}

void ChannelPostConflictTest::testUpdateChannelPost_LocalDelete( void )
{  
  // update data
  shared_ptr<ChannelPost> comment = loadByGid( 4, kSyncIndDelete );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  comment = validate( 4, kSyncIndConflict, INITAL_NUM_OF_POSTS + 1 );
  
  // force resolve conflict
  CPPUNIT_ASSERT( this->channelPostRepository_->clearConflict( "4" ) );
  validate( 4, kSyncIndSynchronous, INITAL_NUM_OF_POSTS );
}

void ChannelPostConflictTest::testDeleteChannelPost_LocalUnchanged( void )
{
  shared_ptr<ChannelPost> comment = loadByGid( 1, kSyncIndSynchronous );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  loadByGid( 1, (SyncInd) -1 );
}

void ChannelPostConflictTest::testDeleteChannelPost_LocalAdd( void )
{
  shared_ptr<ChannelPost> comment = loadByGid( 2, kSyncIndInsert );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  loadByGid( 2, (SyncInd) -1 );
}

void ChannelPostConflictTest::testDeleteChannelPost_LocalUpdate( void )
{
  shared_ptr<ChannelPost> post = loadByGid( 3, kSyncIndUpdate );
  post->clearContent( );
  post->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  post = loadByGid( 3, kSyncIndConflict );
  CPPUNIT_ASSERT( ! post->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! post->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->channelPostRepository_->detectConflict( "3" ) );
  CPPUNIT_ASSERT( this->channelPostRepository_->clearConflict( "3" ) );
}

void ChannelPostConflictTest::testDeleteChannelPost_LocalDelete( void )
{
  shared_ptr<ChannelPost> comment = loadByGid( 4, kSyncIndDelete );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  loadByGid( 4, (SyncInd) -1 );
  CPPUNIT_ASSERT( ! this->channelPostRepository_->detectConflict( "4" ) );
}

void ChannelPostConflictTest::testDeleteChannelPost_LocalDelete_Conflict( void )
{  
  // update data
  shared_ptr<ChannelPost> comment = loadByGid( 4, kSyncIndDelete );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  comment = validate( 4, kSyncIndConflict, INITAL_NUM_OF_POSTS + 1 );
  
  // resolve conflict data with original data content to delete the channel
  comment = loadByGid( 4, kSyncIndConflict );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelPostRepository_->update( *this->channelHeader_, this->channelPosts_, jtime() ) );
  loadByGid( 4, (SyncInd) -1 );
  CPPUNIT_ASSERT( ! this->channelPostRepository_->detectConflict( "4" ) );
}

void ChannelPostConflictTest::testForceClearConflict( void )
{
  shared_ptr<ChannelPost> header = loadByGid( 5, kSyncIndConflict );
  CPPUNIT_ASSERT( this->channelPostRepository_->clearConflict( "5" ) );
  validate( 5, kSyncIndSynchronous, INITAL_NUM_OF_POSTS - 1 );
}
