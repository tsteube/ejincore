/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPostRepositoryCommitTest.h"

#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelHeader.h"
#include "ChannelComment.h"
#include "ChannelPost.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelService.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelPostRepositoryCommitTest );

// ==========
// TEST Methods
// ==========

void ChannelPostRepositoryCommitTest::testCommitAddedPost( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 11 );
  post->setGid( string( "11" ) );
  post->setIV( string( "iv" ) );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 11, post->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "11" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), post->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitAddedPostWithResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 12 );
  post->setGid( string( "12" ) );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 12 );
  media->setGid( string( "12" ) );
  post->getResources().push_back( media );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 12, post->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, post->getResources().size() );
  media = *post->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 12, media->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ), media->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndInsert, media->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitUpdatedPost( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 13 );
  post->setGid( string( "13" ) );
  post->setIV( string( "iv" ) );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 13, post->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "13" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), post->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitAddedResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 14 );
  post->setGid( string( "14" ) );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 14 );
  media->setGid( string( "14" ) );
  post->getResources().push_back( media );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 14, post->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "14" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, post->getResources().size() );
  media = *post->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 14, media->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "14" ), media->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndInsert, media->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitDeletedResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 15 );
  post->setGid( string( "15" ) );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 15, post->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "15" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 15 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitDeletedPost( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 16 );
  post->setGid( string( "16" ) );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  

  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitDeletedPostWithResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 17 );
  post->setGid( string( "17" ) );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 17 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testCommitDeletedPostWithComment( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 18 );
  post->setGid( string( "18" ) );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  this->channelPostRepository_->commit( this->_TEST_CHANNEL, channelPosts, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 18 );
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryCommitTest::testModificationMask( void )
{
  ejin::EntityModification metadata( this->channelPostRepository_->getContainerModification( 1 ) );
  CPPUNIT_ASSERT_EQUAL( jtime( 0L ), metadata.time );
  CPPUNIT_ASSERT_EQUAL( (integer)kNoneModification, metadata.mask );
  CPPUNIT_ASSERT_EQUAL( string(""), metadata.by );
}
