/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __POST_REVERT_UNIT_TEST_H__
#define __POST_REVERT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelPostRevertTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelPostRevertTest );
  CPPUNIT_TEST( testChannelPostRevert_Empty );
  CPPUNIT_TEST( testChannelPostRevert_AddMedia );
  CPPUNIT_TEST( testChannelPostRevert_AddComment );
  CPPUNIT_TEST( testChannelPostRevert_Update );
  CPPUNIT_TEST( testChannelPostRevert_Delete );
  CPPUNIT_TEST( testChannelPostRevert_Restore );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  // private methods
  void setUp( void );
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testChannelPostRevert_Empty( void );
  void testChannelPostRevert_AddMedia( void );
  void testChannelPostRevert_AddComment( void );
  void testChannelPostRevert_Update( void );
  void testChannelPostRevert_Delete( void );
  void testChannelPostRevert_Restore( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
};

#endif // __POST_REVERT_UNIT_TEST_H__
