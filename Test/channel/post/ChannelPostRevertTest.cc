/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPostRevertTest.h"

#include "ChannelService.h"
#include "Media.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "Utilities.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelPostRevertTest );

void ChannelPostRevertTest::setUp( void )
{
  AbstractChannelTestFixture::setUp();
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
}

// ==========
// TEST Methods
// ==========

void ChannelPostRevertTest::testChannelPostRevert_Empty( void )
{
  ChannelPost post;
  post.setId( 100L );
  CPPUNIT_ASSERT( this->db_->contains(post) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( ! this->channelService_->revert( "Channel/100/Post/100", false ) );
  
  this->db_->refresh(post);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndSynchronous );
}
void ChannelPostRevertTest::testChannelPostRevert_AddMedia( void )
{
  Media media;
  media.setChannelId( 100L );
  media.setPostId( 100L );
  media.setName( "new media" );
  media.setOwner( "owner" );
  media.setNo( 1 );
  CPPUNIT_ASSERT( this->db_->insert(media) );
  CPPUNIT_ASSERT_EQUAL( media.getId(), (integer)101 );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100/Media", false ) );
  
  ChannelPost post;
  post.setId(100L);
  this->db_->refresh(post);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndSynchronous );
  
  this->db_->refresh(media);
  CPPUNIT_ASSERT( ! media.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelPostRevertTest::testChannelPostRevert_AddComment( void )
{
  ChannelComment comment;
  comment.setChannelId( 100L );
  comment.setPostId( 100L );
  comment.setContent( "new post" );
  comment.setOwner( "owner" );
  CPPUNIT_ASSERT( this->db_->insert(comment) );
  CPPUNIT_ASSERT_EQUAL( comment.getId(), (integer)101 );

  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100/Comment", false ) );
  
  ChannelPost post;
  post.setId(100L);
  this->db_->refresh(post);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndSynchronous );

  this->db_->refresh( comment );
  CPPUNIT_ASSERT( ! comment.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelPostRevertTest::testChannelPostRevert_Update( void )
{
  ChannelPost post;
  post.setId( 100L );
  this->db_->refresh(post);
  post.setContent("tested");
  post.setSyncInd(ejin::kSyncIndUpdate);
  CPPUNIT_ASSERT( this->db_->update(post) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100", false ) );
  
  this->db_->refresh(post);
  CPPUNIT_ASSERT_EQUAL( post.getContent(), string("Test") );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelPostRevertTest::testChannelPostRevert_Delete( void )
{
  ChannelPost post;
  post.setId( 100L );
  this->db_->refresh(post);
  post.setSyncInd(ejin::kSyncIndDelete);
  CPPUNIT_ASSERT( this->db_->update(post) );

  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100", false ) );
  
  this->db_->refresh(post);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelPostRevertTest::testChannelPostRevert_Restore( void )
{
  ChannelPost post;
  post.setId( 100L );
  this->db_->refresh(post);
  CPPUNIT_ASSERT( this->db_->remove(post) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100", false ) );
  
  this->db_->refresh(post);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndSynchronous );
}
