/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_POST_REPOSITORY_IMPORT_UNIT_TEST_H__
#define __CHANNEL_POST_REPOSITORY_IMPORT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelPostRepositoryImportTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelPostRepositoryImportTest );  
  CPPUNIT_TEST( testImportAddPost );
  CPPUNIT_TEST( testImportAddPostWithResource );
  CPPUNIT_TEST( testImportUpdatePost );
  CPPUNIT_TEST( testImportUpdatePost_Identical );
  CPPUNIT_TEST( testImportUpdatePost_Conflict );
  CPPUNIT_TEST( testImportUpdatePostAddResource );
  CPPUNIT_TEST( testImportUpdatePostDeleteResource );
  CPPUNIT_TEST( testImportDeletePost );
  CPPUNIT_TEST( testImportDeletePostWithResource );
  CPPUNIT_TEST( testImportDeletedPostWithComment );  
  CPPUNIT_TEST( testImportDeletePost_Conflict );
  CPPUNIT_TEST( testChannelPostMetadata );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testImportAddPost( void );
  void testImportAddPostWithResource( void );
  void testImportUpdatePost( void );
  void testImportUpdatePost_Conflict( void );
  void testImportUpdatePost_Identical( void );
  void testImportUpdatePostAddResource( void );
  void testImportUpdatePostDeleteResource( void ); 
  void testImportDeletePost( void );
  void testImportDeletePostWithResource( void );
  void testImportDeletedPostWithComment( void );
  void testImportDeletePost_Conflict( void );
  void testChannelPostMetadata( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/post"; }
  
};

#endif // __MEMBER_POST_IMPORT_UNIT_TEST_H__
