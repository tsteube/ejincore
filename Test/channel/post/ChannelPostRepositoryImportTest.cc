/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPostRepositoryImportTest.h"

#include "ChannelPostRepository.h"
#include "ChangeLogRepository.h"
#include "ChangeLog.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelPost.h"
#include "ChannelMedia.h"
#include "ChannelComment.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelService.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelPostRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void ChannelPostRepositoryImportTest::testImportAddPost( void )
{  
  // pre conditions
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setChannelId( this->_TEST_CHANNEL.getId() );
  post->setContent( "test" );
  post->setOwner( this->_TEST_CHANNEL.getOwner() );
  post->setGid( string( "99" ) );
  post->setIV( "iv" );
  post->setState( 0 );
  post->setOperation( ejin::kSyncInsert );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelPostRepository_->searchFullText("test").size());
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post->hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), post->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );

  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelPostRepository_->searchFullText("test").size());
}

void ChannelPostRepositoryImportTest::testImportAddPostWithResource( void )
{
  // pre conditions
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setChannelId( this->_TEST_CHANNEL.getId() );
  post->setContent( "test" );
  post->setOwner( this->_TEST_CHANNEL.getOwner() );
  post->setGid( string( "99" ) );
  post->setState( 0 );
  post->setOperation( ejin::kSyncInsert );

  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setGid( string( "99" ) );
  media->setOwner( this->_TEST_CHANNEL.getOwner() );
  post->getResources().push_back( media );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post->hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), post->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, post->getResources().size() );
  media = *post->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( string( "99" ), media->getGid() );
  CPPUNIT_ASSERT( media->isAttachmentOutdated() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryImportTest::testImportUpdatePost( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setGid( string( "2" ) );  
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  
  post->setContent( "hello world" );
  post->setOperation( ejin::kSyncUpdate );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  post->setIV( "iv" );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 11, this->channelPostRepository_->searchFullText("hello").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelPostRepository_->searchFullText("world").size());

  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post!=NULL && post->hasId() );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), post->getContent() );
  CPPUNIT_ASSERT_EQUAL( string("iv"), post->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );

  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 11, this->channelPostRepository_->searchFullText("hello").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelPostRepository_->searchFullText("world").size());
}

void ChannelPostRepositoryImportTest::testImportUpdatePost_Identical( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setGid( string( "3" ) );  
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  
  post->setContent( "hello" );
  post->setOperation( ejin::kSyncUpdate );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post->hasId() );
  CPPUNIT_ASSERT_EQUAL( string("hello"), post->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  CPPUNIT_ASSERT( ! this->channelPostRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryImportTest::testImportUpdatePost_Conflict( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setGid( string( "3" ) );  
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  
  post->setContent( "hello world" );
  post->setOperation( ejin::kSyncUpdate );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post->hasId() );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), post->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, post->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, post->getResources().size() );
  CPPUNIT_ASSERT( this->channelPostRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelPostRepository_->clearConflict( string( "3" ) ) );
  
  // post condition
  CPPUNIT_ASSERT( ! this->channelPostRepository_->detectConflict( string( "3" ) ) );
  this->db_->refresh(*post);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, post->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryImportTest::testImportUpdatePostAddResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setGid( string( "2" ) );  
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  post->setOperation( ejin::kSyncUpdate );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setGid( string( "99" ) );
  media->setOwner( this->_TEST_CHANNEL.getOwner() );
  media->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  post->getResources().push_back( media );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( ! this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post->hasId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, post->getResources().size() );
  media = *post->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( string( "99" ), media->getGid() );
  CPPUNIT_ASSERT( media->isAttachmentOutdated() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}

void ChannelPostRepositoryImportTest::testImportUpdatePostDeleteResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setGid( string( "1" ) );  
  list< shared_ptr<ChannelPost> > posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );

  post->getResources( ).clear();
  post->setOperation( ejin::kSyncUpdate );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( ! this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  posts = this->channelPostRepository_->loadAll( this->_TEST_CHANNEL );
  post = Utilities::findElementIn (posts, *post);
  CPPUNIT_ASSERT( post != NULL );
  CPPUNIT_ASSERT( post->hasId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, post->getResources().size() );
  
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 1L );
  CPPUNIT_ASSERT( this->db_->contains(*media) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}

void ChannelPostRepositoryImportTest::testImportDeletePost( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 2 );  
  post->setGid( string( "2" ) );  
  post->setOperation( ejin::kSyncRemove );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 11, this->channelPostRepository_->searchFullText("hello").size());

  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 10, this->channelPostRepository_->searchFullText("hello").size());
}

void ChannelPostRepositoryImportTest::testImportDeletePostWithResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 1 );  
  post->setGid( string( "1" ) );  
  post->setOperation( ejin::kSyncRemove );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryImportTest::testImportDeletedPostWithComment( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 1 );  
  post->setGid( string( "1" ) );  
  post->setOperation( ejin::kSyncRemove );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );
  shared_ptr<ChannelComment> comment(shared_ptr<ChannelComment>(new ChannelComment()));
  comment->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelPostRepositoryImportTest::testImportDeletePost_Conflict( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 3 );  
  post->setGid( string( "3" ) );  
  post->setOperation( ejin::kSyncRemove );
  post->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelPostRepository_->update( this->_TEST_CHANNEL, channelPosts, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( this->db_->contains(*post) );
  CPPUNIT_ASSERT( this->channelPostRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelPostRepository_->clearConflict( string( "3" ) ) );
  
  // post condition
  CPPUNIT_ASSERT( ! this->channelPostRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelPost, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChannelPostRepositoryImportTest::testChannelPostMetadata( void )
{
  // ----
  // TEST
  ejin::EntityModification containerMetadata( this->channelPostRepository_->getContainerModification( 1 ) );
  CPPUNIT_ASSERT_EQUAL( jtime( 0L ), containerMetadata.time );
  CPPUNIT_ASSERT_EQUAL( (integer)kNoneModification, containerMetadata.mask );
  CPPUNIT_ASSERT_EQUAL( string(""), containerMetadata.by );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleRead, this->channelPostRepository_->getAccessRole( 3, "test" ) );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleOwner, this->channelPostRepository_->getAccessRole( 3, "owner" ) );
}
