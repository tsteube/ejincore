/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __POST_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __POST_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelPostConflictTest : public AbstractChannelTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelPostConflictTest );
  CPPUNIT_TEST( testAddChannelPost );
  CPPUNIT_TEST( testAddChannelPost_Empty );
  CPPUNIT_TEST( testUpdateChannelPost_LocalUnchanged );
  CPPUNIT_TEST( testUpdateChannelPost_LocalAdd );
  CPPUNIT_TEST( testUpdateChannelPost_LocalUpdate_EqualContent );
  CPPUNIT_TEST( testUpdateChannelPost_LocalUpdate );
  CPPUNIT_TEST( testUpdateChannelPost_LocalDelete_EqualContent );
  CPPUNIT_TEST( testUpdateChannelPost_LocalDelete );
  CPPUNIT_TEST( testDeleteChannelPost_LocalUnchanged );
  CPPUNIT_TEST( testDeleteChannelPost_LocalAdd );
  CPPUNIT_TEST( testDeleteChannelPost_LocalUpdate );
  CPPUNIT_TEST( testDeleteChannelPost_LocalDelete );
  CPPUNIT_TEST( testDeleteChannelPost_LocalDelete_Conflict );
  CPPUNIT_TEST( testForceClearConflict );
  CPPUNIT_TEST_SUITE_END();

  // ==========
  // Test methods
  // ==========
public:

  void testAddChannelPost( void );
  void testAddChannelPost_Empty( void );
  void testUpdateChannelPost_LocalUnchanged( void );
  void testUpdateChannelPost_LocalAdd( void );
  void testUpdateChannelPost_LocalUpdate_EqualContent( void );
  void testUpdateChannelPost_LocalUpdate( void );
  void testUpdateChannelPost_LocalDelete_EqualContent( void );
  void testUpdateChannelPost_LocalDelete( void );
  void testDeleteChannelPost_LocalUnchanged( void );
  void testDeleteChannelPost_LocalAdd( void );
  void testDeleteChannelPost_LocalUpdate( void );
  void testDeleteChannelPost_LocalDelete( void );
  void testDeleteChannelPost_LocalDelete_Conflict( void );
  void testForceClearConflict( void );

protected:
  
  shared_ptr<ChannelHeader>          channelHeader_;
  list< shared_ptr<ChannelPost> >    channelPosts_;
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/post/conflict"; }
  shared_ptr<ChannelPost> loadByGid( integer gid, SyncInd state );
  shared_ptr<ChannelPost> validate( integer gid, SyncInd state, long expectedTotalNumOfPosts );
  shared_ptr<ChannelPost> validate( integer channelGid, integer gid, SyncInd state, long expectedTotalNumOfPosts );

};

#endif // __POST_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__
