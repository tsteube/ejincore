/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_POST_REPOSITORY_COMMIT_UNIT_TEST_H__
#define __CHANNEL_POST_REPOSITORY_COMMIT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelPostRepositoryCommitTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelPostRepositoryCommitTest );  
  CPPUNIT_TEST( testCommitAddedPost );
  CPPUNIT_TEST( testCommitAddedPostWithResource );
  CPPUNIT_TEST( testCommitUpdatedPost );
  CPPUNIT_TEST( testCommitAddedResource );
  CPPUNIT_TEST( testCommitDeletedResource );
  CPPUNIT_TEST( testCommitDeletedPost );
  CPPUNIT_TEST( testCommitDeletedPostWithResource );
  CPPUNIT_TEST( testCommitDeletedPostWithComment );
  CPPUNIT_TEST( testModificationMask );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testCommitAddedPost( void );
  void testCommitAddedPostWithResource( void );
  void testCommitUpdatedPost( void );
  void testCommitAddedResource( void );
  void testCommitDeletedResource( void );
  void testCommitDeletedPost( void );
  void testCommitDeletedPostWithResource( void );
  void testCommitDeletedPostWithComment( void );
  void testModificationMask( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/post"; }
};

#endif // __CHANNEL_POST_REPOSITORY_COMMIT_UNIT_TEST_H__
