/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderProfileTest.h"

#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelService.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelHeaderProfileTest );

// ==========
// TEST Methods
// ==========

void ChannelHeaderProfileTest::testAddedChannelProfile( void )
{
  // test
  list< shared_ptr<ChannelHeader> > headers(this->channelHeaderRepository_->findAdded());
  
  // validation
  bool result = compareLists(headers, "21", "22", NULL);
  CPPUNIT_ASSERT( result );
}

void ChannelHeaderProfileTest::testModifiedChannelProfile( void )
{
  // test
  list< shared_ptr<ChannelHeader> > headers(this->channelHeaderRepository_->findUpdated());
  
  // validation
  bool result = compareLists(headers, "41", "42", "43", "51", "53", "61", "62", "63", NULL);
  CPPUNIT_ASSERT( result );
}

void ChannelHeaderProfileTest::testRemovedChannelProfile( void )
{
  // test
  list< shared_ptr<ChannelHeader> > headers(this->channelHeaderRepository_->findRemoved());
  
  // validation
  bool result = compareLists(headers, "31", "32", NULL);
  CPPUNIT_ASSERT( result );
}

void ChannelHeaderProfileTest::testConflictedChannelProfile( void )
{
  // test
  list< shared_ptr<ChannelHeader> > headers(this->channelHeaderRepository_->findConflicted());
  
  // validation
  bool result = compareLists(headers, "11", "12", "13", "15", "16", NULL);
  CPPUNIT_ASSERT( result );
}

void ChannelHeaderProfileTest::testOutDatedChannelProfile( void )
{
  // test
  list< shared_ptr<ChannelHeader> > headers(this->channelHeaderRepository_->findOutdated());
  
  // validation
  bool result = compareLists(headers, "1", "2", "3", NULL);
  CPPUNIT_ASSERT( result );
}

void ChannelHeaderProfileTest::testChannelInvitations( void )
{
  // test
  list< string > headers(this->channelHeaderRepository_->findInvitations( ));
  
  // validation
  CPPUNIT_ASSERT( headers.size() == 1 );
}

void ChannelHeaderProfileTest::testChannelToAccept( void )
{
  // test
  list< string > headers(this->channelHeaderRepository_->findChannelToAccept( ));
  
  // validation
  CPPUNIT_ASSERT( headers.size() == 1 );
}

void ChannelHeaderProfileTest::testChannelToReject( void )
{
  // test
  list< string > headers(this->channelHeaderRepository_->findChannelToReject( ));
  
  // validation
  CPPUNIT_ASSERT( headers.size() == 1 );
}

void ChannelHeaderProfileTest::testChannelMembership( void )
{
  CPPUNIT_ASSERT( this->channelHeaderRepository_->isMemberOf( "owner", "1" ) == true);
  CPPUNIT_ASSERT( this->channelHeaderRepository_->isMemberOf( "owner", "100" ) == true);
  CPPUNIT_ASSERT( this->channelHeaderRepository_->isMemberOf( "owner", "999999" ) == false);
}

// ==========
// Helper
// ==========

bool ChannelHeaderProfileTest::compareLists( list< shared_ptr<ChannelHeader> >& headers, const char* gid, ... )
{
  va_list argptr;
  va_start(argptr,gid);
  
  // convert variable arguments into a list of Media objects
  set< ChannelHeader > values;
  if (gid) {
    values.insert( ChannelHeader( string(gid) ) );   
    const char* param = va_arg(argptr, const char*);
    while (param) {
      values.insert( ChannelHeader( string(param) ) );
      param = va_arg(argptr,const char*);
    }
  }
  
  va_end(argptr);
  
  // compare both sets
  bool isEqual(values.size() == headers.size());
  if (isEqual) {      
    list< shared_ptr<ChannelHeader> >::iterator it = headers.begin();
    for (; it!=headers.end() && isEqual; it++) {
      isEqual = values.find(*(*it)) != values.end();
    }
  }
  
  return isEqual;
}
