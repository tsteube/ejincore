/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelCommentConflictTest.h"

#include <sstream>

#include "ChannelService.h"
#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "ChannelComment.h"
#include "ChannelPost.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelCommentConflictTest );

#define INITAL_NUM_OF_COMMENTS 6

shared_ptr<ChannelComment> ChannelCommentConflictTest::loadByGid( integer gid, SyncInd syncInd )
{
  std::stringstream out;
  out << gid;
  
  // initialize channel header
  this->channelHeader_ = shared_ptr<ChannelHeader>( new ChannelHeader( ) );
  this->channelHeader_->setId( gid );
  this->channelHeader_->setGid( out.str() );
  this->channelHeader_->setOwner( "owner" );

  this->channelComments_.clear();
  list< shared_ptr<ChannelComment> > allComments = this->channelCommentRepository_->loadAll( *this->channelHeader_ );
  if ( syncInd >= 0 ) {
    // remapping
    for (list< shared_ptr<ChannelComment> >::iterator it = allComments.begin(); it != allComments.end(); it++) {      
      this->channelComments_.push_back(*it);
    }
    
    CPPUNIT_ASSERT( this->channelComments_.size() > 0 );
    for (list< shared_ptr<ChannelComment> >::iterator it = this->channelComments_.begin(); it != this->channelComments_.end(); it++) {
      if ( (*it)->getGid() == out.str() ) {
        CPPUNIT_ASSERT( syncInd == (*it)->getSyncInd() );
        return (*it);
      }
    }
    CPPUNIT_ASSERT( false );
  } else {
    CPPUNIT_ASSERT( allComments.size() == 0 );
  }
  return shared_ptr<ChannelComment>();
}

shared_ptr<ChannelComment> ChannelCommentConflictTest::validate(integer gid,
                                                                       SyncInd state,
                                                                       long expectedTotalNumOfComments )
{
  return validate( gid, gid, state, expectedTotalNumOfComments);
}

shared_ptr<ChannelComment> ChannelCommentConflictTest::validate( integer channelGid, 
                                                                        integer gid,
                                                                        SyncInd state,
                                                                        long expectedTotalNumOfComments )
{
  shared_ptr<ChannelComment> comment( loadByGid( channelGid, state ) );
  bool expectConflict = ( state == kSyncIndConflict );
  std::stringstream out;
  out << gid;
  CPPUNIT_ASSERT( expectConflict == this->channelCommentRepository_->detectConflict( out.str() ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfComments, this->db_->count( "COMMENT_TBL" ) );
  return comment;
}

// ==========
// TEST Methods
// ==========

void ChannelCommentConflictTest::testAddChannelComment( void )
{
  loadByGid( 1, kSyncIndSynchronous );

  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment( "99" ));
  comment->setContent( "test" );
  comment->setOwner( "owner" );
  comment->setOperation( ejin::kSyncInsert );
  this->channelComments_.push_back( comment );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  validate( 1, 99, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS + 1);
}

void ChannelCommentConflictTest::testAddChannelComment_Empty( void )
{
  loadByGid( 1, kSyncIndSynchronous );
  
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment( "99" ));
  comment->clearContent( );
  comment->setOwner( "owner" );
  comment->setOperation( ejin::kSyncInsert );
  this->channelComments_.push_back( comment );
  this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() );
  validate( 1, 1, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS );
}

void ChannelCommentConflictTest::testUpdateChannelComment_LocalUnchanged( void )
{  
  shared_ptr<ChannelComment> comment = loadByGid( 1, kSyncIndSynchronous );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  validate( 1, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS );
}

void ChannelCommentConflictTest::testUpdateChannelComment_LocalAdd( void )
{  
  loadByGid( 2, kSyncIndInsert );
  try {
    this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() );
  } catch (ejin::data_integrity_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}

void ChannelCommentConflictTest::testUpdateChannelComment_LocalUpdate_EqualContent( void )
{  
  shared_ptr<ChannelComment> header = loadByGid( 3, kSyncIndUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  validate( 3, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS );
}

void ChannelCommentConflictTest::testUpdateChannelComment_LocalUpdate( void )
{  
  // 1. update data
  shared_ptr<ChannelComment> comment = loadByGid( 3, kSyncIndUpdate );
  string content = comment->getContent( );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  comment = validate( 3, kSyncIndConflict, INITAL_NUM_OF_COMMENTS + 1 );
  CPPUNIT_ASSERT_EQUAL( comment->getContent(), string("update") );

  // 2. update data
  comment->setContent( "update2" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  comment = validate( 3, kSyncIndConflict, INITAL_NUM_OF_COMMENTS + 1 );
  CPPUNIT_ASSERT_EQUAL( comment->getContent(), string("update2") );

  // 2. remove data
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  comment = validate( 3, kSyncIndConflict, INITAL_NUM_OF_COMMENTS + 1 );
  CPPUNIT_ASSERT( ! comment->hasContent() );

  // resolve conflict data with original data content
  comment->setContent( content );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  validate( 3, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS );
}

void ChannelCommentConflictTest::testUpdateChannelComment_LocalDelete_EqualContent( void )
{  
  shared_ptr<ChannelComment> comment = loadByGid( 4, kSyncIndDelete );
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  validate( 4, kSyncIndDelete, INITAL_NUM_OF_COMMENTS );
}

void ChannelCommentConflictTest::testUpdateChannelComment_LocalDelete( void )
{  
  // update data
  shared_ptr<ChannelComment> comment = loadByGid( 4, kSyncIndDelete );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  comment = validate( 4, kSyncIndConflict, INITAL_NUM_OF_COMMENTS + 1 );
  
  // force resolve conflict
  CPPUNIT_ASSERT( this->channelCommentRepository_->clearConflict( "4" ) );
  validate( 4, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS );
}

void ChannelCommentConflictTest::testDeleteChannelComment_LocalUnchanged( void )
{
  shared_ptr<ChannelComment> comment = loadByGid( 1, kSyncIndSynchronous );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  loadByGid( 1, (SyncInd) -1 );
}

void ChannelCommentConflictTest::testDeleteChannelComment_LocalAdd( void )
{
  shared_ptr<ChannelComment> comment = loadByGid( 2, kSyncIndInsert );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  loadByGid( 2, (SyncInd) -1 );
}

void ChannelCommentConflictTest::testDeleteChannelComment_LocalUpdate( void )
{
  shared_ptr<ChannelComment> comment = loadByGid( 3, kSyncIndUpdate );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  comment = loadByGid( 3, kSyncIndConflict );
  CPPUNIT_ASSERT( ! comment->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! comment->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->channelCommentRepository_->detectConflict( "3" ) );
  CPPUNIT_ASSERT( this->channelCommentRepository_->clearConflict( "3" ) );
}

void ChannelCommentConflictTest::testDeleteChannelComment_LocalDelete( void )
{
  shared_ptr<ChannelComment> comment = loadByGid( 4, kSyncIndDelete );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  loadByGid( 4, (SyncInd) -1 );
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->detectConflict( "4" ) );
}

void ChannelCommentConflictTest::testDeleteChannelComment_LocalDelete_Conflict( void )
{  
  // update data
  shared_ptr<ChannelComment> comment = loadByGid( 4, kSyncIndDelete );
  comment->setContent( "update" );
  comment->setOperation( ejin::kSyncUpdate );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  comment = validate( 4, kSyncIndConflict, INITAL_NUM_OF_COMMENTS + 1 );
  
  // resolve conflict data with original data content to delete the channel
  comment = loadByGid( 4, kSyncIndConflict );
  comment->clearContent( );
  comment->setOperation( ejin::kSyncRemove );
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( *this->channelHeader_, list< shared_ptr<ChannelPost> >(), this->channelComments_, jtime() ) );
  loadByGid( 4, (SyncInd) -1 );
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->detectConflict( "4" ) );
}

void ChannelCommentConflictTest::testForceClearConflict( void )
{
  shared_ptr<ChannelComment> header = loadByGid( 5, kSyncIndConflict );
  CPPUNIT_ASSERT( this->channelCommentRepository_->clearConflict( "5" ) );
  validate( 5, kSyncIndSynchronous, INITAL_NUM_OF_COMMENTS - 1 );
}
