/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_COMMENT_REPOSITORY_IMPORT_UNIT_TEST_H__
#define __CHANNEL_COMMENT_REPOSITORY_IMPORT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelCommentRepositoryImportTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelCommentRepositoryImportTest );  
  CPPUNIT_TEST( testImportAddHeaderComment );
  CPPUNIT_TEST( testImportAddPostComment );
  CPPUNIT_TEST( testImportAddPostCommentWithResource );
  CPPUNIT_TEST( testImportUpdatePostComment );
  CPPUNIT_TEST( testImportUpdatePostComment_Identical );
  CPPUNIT_TEST( testImportUpdatePostComment_Conflict );
  CPPUNIT_TEST( testImportUpdatePostCommentAddResource );
  CPPUNIT_TEST( testImportUpdatePostCommentDeleteResource );
  CPPUNIT_TEST( testImportDeletePostComment );
  CPPUNIT_TEST( testImportDeleteHeaderComment );
  CPPUNIT_TEST( testImportDeletePostCommentWithResource );
  CPPUNIT_TEST( testImportDeletePostComment_Conflict );
  CPPUNIT_TEST( testChannelCommentMetadata );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testImportAddHeaderComment( void );
  void testImportAddPostComment( void );
  void testImportAddPostCommentWithResource( void );
  void testImportUpdatePostComment( void );
  void testImportUpdatePostComment_Identical( void );
  void testImportUpdatePostComment_Conflict( void );
  void testImportUpdatePostCommentAddResource( void );
  void testImportUpdatePostCommentDeleteResource( void ); 
  void testImportDeletePostComment( void );
  void testImportDeleteHeaderComment( void );
  void testImportDeletePostCommentWithResource( void );
  void testImportDeletePostComment_Conflict( void );
  void testChannelCommentMetadata( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/comment"; }
};

#endif // __MEMBER_REPOSITORY_IMPORT_UNIT_TEST_H__
