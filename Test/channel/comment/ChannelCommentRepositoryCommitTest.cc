/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelCommentRepositoryCommitTest.h"

#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelService.h"


CPPUNIT_TEST_SUITE_REGISTRATION( ChannelCommentRepositoryCommitTest );

// ==========
// TEST Methods
// ==========

void ChannelCommentRepositoryCommitTest::testCommitAddedHeaderComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 21 );
  comment->setGid( string( "21" ) );
  comment->setIV( "iv" );
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 21, comment->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "21" ) , comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ) , comment->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );

  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitAddedPostComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 11 );
  comment->setGid( string( "11" ) );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 11, comment->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "11" ) , comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitAddedPostCommentWithResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 12 );
  comment->setGid( string( "12" )  );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 12 );
  media->setGid( string( "12" )  );
  comment->getResources().push_back( media );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 12, comment->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ) , comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, comment->getResources().size() );
  media = *comment->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 12, media->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ) , media->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndInsert, media->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitUpdatedPostComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 13 );
  comment->setGid( string( "13" )  );
  comment->setIV( string( "iv" )  );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 13, comment->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "13" ) , comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ) , comment->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitUpdatedPostCommentAddedResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 14 );
  comment->setGid( string( "14" )  );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 14 );
  media->setGid( string( "14" )  );
  comment->getResources().push_back( media );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 14, comment->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "14" ) , comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, comment->getResources().size() );
  media = *comment->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 14, media->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "14" ) , media->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndInsert, media->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitUpdatedPostCommentDeletedResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 15 );
  comment->setGid( string( "15" )  );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) 15, comment->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "15" ) , comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 15 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitDeletedPostComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 16 );
  comment->setGid( string( "16" )  );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitDeletedPostCommentWithResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 17 );
  comment->setGid( string( "17" )  );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );  
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 17 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testCommitDeletedChannelComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 26 );
  comment->setGid( string( "26" )  );
  
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  this->channelCommentRepository_->commit( this->_TEST_CHANNEL, channelComments, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryCommitTest::testModificationMask( void )
{
  ejin::EntityModification metadata( this->channelCommentRepository_->getEntityModification( 1 ) );
  CPPUNIT_ASSERT_EQUAL( jtime( 0L ), metadata.time );
  CPPUNIT_ASSERT_EQUAL( (integer)kNoneModification, metadata.mask );
  CPPUNIT_ASSERT_EQUAL( string(""), metadata.by );
}

