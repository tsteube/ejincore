/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelCommentRevertTest.h"

#include "ChannelService.h"
#include "ChannelHeader.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "Media.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelMemberRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelCommentRevertTest );

void ChannelCommentRevertTest::setUp( void )
{
  AbstractChannelTestFixture::setUp();
  this->_TEST_CHANNEL.setId( 100L );
  this->_TEST_CHANNEL.setGid( string( "100" ) );
  this->_TEST_CHANNEL.setOwner( "owner" );
  
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
}

// ==========
// TEST Methods
// ==========

void ChannelCommentRevertTest::testChannelCommentRevert_Empty( void )
{
  ChannelComment comment;
  comment.setId( 100L );
  CPPUNIT_ASSERT( this->db_->contains(comment) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( ! this->channelService_->revert( "Channel/100/Post/100/Comment/100", false ) );

  this->db_->refresh(comment);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndSynchronous );
}
void ChannelCommentRevertTest::testChannelCommentRevert_AddMedia( void )
{
  Media media;
  media.setChannelId( 100L );
  media.setPostId( 100L );
  media.setCommentId( 100L );
  media.setName( "new media" );
  media.setOwner( "owner" );
  media.setNo( 1 );
  CPPUNIT_ASSERT( this->db_->insert(media) );
  CPPUNIT_ASSERT_EQUAL( media.getId(), (integer)101 );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100/Comment/100/Media", false ) );

  ChannelComment comment;
  comment.setId(100L);
  this->db_->refresh(comment);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndSynchronous );

  this->db_->refresh(media);
  CPPUNIT_ASSERT( ! media.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelCommentRevertTest::testChannelCommentRevert_Update( void )
{
  ChannelComment comment;
  comment.setId( 100L );
  this->db_->refresh(comment);
  comment.setContent("test");
  comment.setSyncInd(ejin::kSyncIndUpdate);
  CPPUNIT_ASSERT( this->db_->update(comment) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100/Comment/100", false ) );
  
  this->db_->refresh(comment);
  CPPUNIT_ASSERT_EQUAL( comment.getContent(), string("hello") );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelCommentRevertTest::testChannelCommentRevert_Delete( void )
{
  ChannelComment comment;
  comment.setId( 100L );
  this->db_->refresh(comment);
  comment.setSyncInd(ejin::kSyncIndDelete);
  CPPUNIT_ASSERT( this->db_->update(comment) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100/Comment/100", false ) );
  
  this->db_->refresh(comment);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelCommentRevertTest::testChannelCommentRevert_Restore( void )
{
  ChannelComment comment;
  comment.setId( 100L );
  this->db_->refresh(comment);
  CPPUNIT_ASSERT( this->db_->remove(comment) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post/100/Comment/100", false ) );
  
  this->db_->refresh(comment);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndSynchronous );
}
