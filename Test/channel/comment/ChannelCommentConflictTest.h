/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __COMMENT_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __COMMENT_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelCommentConflictTest : public AbstractChannelTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelCommentConflictTest );
  CPPUNIT_TEST( testAddChannelComment );
  CPPUNIT_TEST( testAddChannelComment_Empty );
  CPPUNIT_TEST( testUpdateChannelComment_LocalUnchanged );
  CPPUNIT_TEST( testUpdateChannelComment_LocalAdd );
  CPPUNIT_TEST( testUpdateChannelComment_LocalUpdate_EqualContent );
  CPPUNIT_TEST( testUpdateChannelComment_LocalUpdate );
  CPPUNIT_TEST( testUpdateChannelComment_LocalDelete_EqualContent );
  CPPUNIT_TEST( testUpdateChannelComment_LocalDelete );
  CPPUNIT_TEST( testDeleteChannelComment_LocalUnchanged );
  CPPUNIT_TEST( testDeleteChannelComment_LocalAdd );
  CPPUNIT_TEST( testDeleteChannelComment_LocalUpdate );
  CPPUNIT_TEST( testDeleteChannelComment_LocalDelete );
  CPPUNIT_TEST( testDeleteChannelComment_LocalDelete_Conflict );
  CPPUNIT_TEST( testForceClearConflict );
  CPPUNIT_TEST_SUITE_END();

  // ==========
  // Test methods
  // ==========
public:
  
  void testAddChannelComment( void );
  void testAddChannelComment_Empty( void );
  void testUpdateChannelComment_LocalUnchanged( void );
  void testUpdateChannelComment_LocalAdd( void );
  void testUpdateChannelComment_LocalUpdate_EqualContent( void );
  void testUpdateChannelComment_LocalUpdate( void );
  void testUpdateChannelComment_LocalDelete_EqualContent( void );
  void testUpdateChannelComment_LocalDelete( void );
  void testDeleteChannelComment_LocalUnchanged( void );
  void testDeleteChannelComment_LocalAdd( void );
  void testDeleteChannelComment_LocalUpdate( void );
  void testDeleteChannelComment_LocalDelete( void );
  void testDeleteChannelComment_LocalDelete_Conflict( void );
  void testForceClearConflict( void );

protected:

  shared_ptr<ChannelHeader>            channelHeader_;
  list< shared_ptr<ChannelComment> >    channelComments_;
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/comment/conflict"; }
  shared_ptr<ChannelComment> loadByGid( integer gid, SyncInd state );
  shared_ptr<ChannelComment> validate( integer gid, SyncInd state, long expectedTotalNumOfComments );
  shared_ptr<ChannelComment> validate( integer channelGid, integer gid, SyncInd state, long expectedTotalNumOfComments );

};

#endif // __COMMENT_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__
