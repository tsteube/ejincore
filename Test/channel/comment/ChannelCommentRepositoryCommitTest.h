/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_COMMENT_REPOSITORY_COMMIT_UNIT_TEST_H__
#define __CHANNEL_COMMENT_REPOSITORY_COMMIT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelCommentRepositoryCommitTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelCommentRepositoryCommitTest );  
  CPPUNIT_TEST( testCommitAddedPostComment );
  CPPUNIT_TEST( testCommitAddedHeaderComment );
  CPPUNIT_TEST( testCommitAddedPostCommentWithResource );
  CPPUNIT_TEST( testCommitUpdatedPostComment );
  CPPUNIT_TEST( testCommitUpdatedPostCommentAddedResource );
  CPPUNIT_TEST( testCommitUpdatedPostCommentDeletedResource );
  CPPUNIT_TEST( testCommitDeletedPostComment );
  CPPUNIT_TEST( testCommitDeletedPostCommentWithResource );
  CPPUNIT_TEST( testCommitDeletedChannelComment );
  CPPUNIT_TEST( testModificationMask );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testCommitAddedHeaderComment( void );
  void testCommitAddedPostComment( void );
  void testCommitAddedPostCommentWithResource( void );
  void testCommitUpdatedPostComment( void );
  void testCommitUpdatedPostCommentAddedResource( void );
  void testCommitUpdatedPostCommentDeletedResource( void );
  void testCommitDeletedPostComment( void );
  void testCommitDeletedPostCommentWithResource( void );
  void testCommitDeletedChannelComment( void );
  void testModificationMask( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/comment"; }
};

#endif // __CHANNEL_COMMENT_REPOSITORY_COMMIT_UNIT_TEST_H__
