/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelCommentRepositoryImportTest.h"

#include "ChannelCommentRepository.h"
#include "ChangeLogRepository.h"
#include "ChangeLog.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelService.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelCommentRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void ChannelCommentRepositoryImportTest::testImportAddHeaderComment( void )
{  
  // pre conditions
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setChannelId( this->_TEST_CHANNEL.getId() );
  comment->setContent( "test" );
  comment->setOwner(_TEST_CHANNEL.getOwner() );
  comment->setGid( string( "99" ) );
  comment->setIV( string("iv") );
  comment->setState( 0 );
  comment->setOperation( ejin::kSyncInsert );

  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelCommentRepository_->searchFullText("test").size());

  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, list< shared_ptr<ChannelPost> >(), channelComments, jtime() ) );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT( ! comment->hasPostId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), comment->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );

  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelCommentRepository_->searchFullText("test").size());
}

void ChannelCommentRepositoryImportTest::testImportAddPostComment( void )
{  
  // pre conditions
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 2L );
  post->setGid( string( "2" ) );
  post->setState( 0 );
  
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setChannelId( this->_TEST_CHANNEL.getId() );
  comment->setPostGid( string( "2" ) );
  comment->setContent( "test" );
  comment->setOwner(_TEST_CHANNEL.getOwner() );
  comment->setGid( string( "99" ) );
  comment->setState( 0 );
  comment->setOperation( ejin::kSyncInsert );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryImportTest::testImportAddPostCommentWithResource( void )
{
  // preparation
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 2L );
  post->setGid( string( "2" ) );
  post->setState( 0 );

  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setChannelId( this->_TEST_CHANNEL.getId() );
  comment->setPostGid( string( "2" ) );
  comment->setContent( "test" );
  comment->setOwner( this->_TEST_CHANNEL.getOwner() );
  comment->setGid( string( "99" ) );
  comment->setState( 0 );
  comment->setOperation( ejin::kSyncInsert );

  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setGid( string( "99" ) );
  media->setOwner( this->_TEST_CHANNEL.getOwner() );
  comment->getResources().push_back( media );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  channelPosts.push_back(post);
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), comment->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, comment->getResources().size() );
  media = *comment->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( string( "99" ), media->getGid() );
  CPPUNIT_ASSERT( media->isAttachmentOutdated() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void ChannelCommentRepositoryImportTest::testImportUpdatePostComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setGid( string( "2" ) );  
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  
  comment->setContent( "hello world" );
  comment->setIV( "iv" );
  comment->setOperation( ejin::kSyncUpdate );
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 20, this->channelCommentRepository_->searchFullText("hello").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelCommentRepository_->searchFullText("world").size());

  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), comment->getContent() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), comment->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 20, this->channelCommentRepository_->searchFullText("hello").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelCommentRepository_->searchFullText("world").size());
}
void ChannelCommentRepositoryImportTest::testImportUpdatePostComment_Identical( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setGid( string( "3" ) );  
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  
  comment->setContent( "hello" );
  comment->setOperation( ejin::kSyncUpdate );
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( string("hello"), comment->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->detectConflict( string( "3" ) ) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
void ChannelCommentRepositoryImportTest::testImportUpdatePostComment_Conflict( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setGid( string( "3" ) );  
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  
  comment->setContent( "hello world" );
  comment->setOperation( ejin::kSyncUpdate );
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), comment->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, comment->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, comment->getResources().size() );
  CPPUNIT_ASSERT( this->channelCommentRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelCommentRepository_->clearConflict( string( "3" ) ) );
  
  // post condition
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->detectConflict( string( "3" ) ) );
  this->db_->refresh(*comment);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, comment->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
void ChannelCommentRepositoryImportTest::testImportUpdatePostCommentAddResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setGid( string( "2" ) );  
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  comment->setOperation( ejin::kSyncUpdate );

  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setGid( string( "99" ) );
  media->setOwner( this->_TEST_CHANNEL.getOwner() );
  media->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  comment->getResources().push_back( media );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, comment->getResources().size() );
  media = *comment->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( string( "99" ), media->getGid() );
  CPPUNIT_ASSERT( media->isAttachmentOutdated() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}
void ChannelCommentRepositoryImportTest::testImportUpdatePostCommentDeleteResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setGid( string( "1" ) );  
  list< shared_ptr<ChannelComment> > comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  
  comment->getResources( ).clear();
  
  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  comments = this->channelCommentRepository_->loadAll( this->_TEST_CHANNEL );
  comment = Utilities::findElementIn (comments, *comment);
  CPPUNIT_ASSERT( comment != NULL );
  CPPUNIT_ASSERT( comment->hasId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, comment->getResources().size() );
  
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 1L );
  CPPUNIT_ASSERT( this->db_->contains(*media) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}
void ChannelCommentRepositoryImportTest::testImportDeletePostComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 2 );  
  comment->setGid( string( "2" ) );
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  comment->setOperation( ejin::kSyncRemove );

  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
void ChannelCommentRepositoryImportTest::testImportDeletePostCommentWithResource( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 1 );  
  comment->setGid( string( "1" ) );  
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  comment->setOperation( ejin::kSyncRemove );
  
  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
void ChannelCommentRepositoryImportTest::testImportDeletePostComment_Conflict( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 3 );  
  comment->setGid( string( "3" ) );  
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  comment->setOperation( ejin::kSyncRemove );

  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( this->db_->contains(*comment) );
  CPPUNIT_ASSERT( this->channelCommentRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelCommentRepository_->clearConflict( string( "3" ) ) );
  
  // post condition
  CPPUNIT_ASSERT( ! this->channelCommentRepository_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
void ChannelCommentRepositoryImportTest::testImportDeleteHeaderComment( void )
{
  // preparation
  shared_ptr<ChannelComment> comment = shared_ptr<ChannelComment>(new ChannelComment());
  comment->setId( 5 );
  comment->setGid( string( "5" ) );
  comment->setOperation( ejin::kSyncRemove );
  comment->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  list< shared_ptr<ChannelPost> > channelPosts;
  list< shared_ptr<ChannelComment> > channelComments;
  channelComments.push_back(comment);
  
  list<integer>  l = this->channelCommentRepository_->searchFullText("hello");
  CPPUNIT_ASSERT_EQUAL( (size_t) 20, this->channelCommentRepository_->searchFullText("hello").size());
  
  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelCommentRepository_->update( this->_TEST_CHANNEL, channelPosts, channelComments, jtime() ) );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*comment) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelComment, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );  
  
  // ----
  // TEST FULL TEXT SEARCH
  list<integer>  l2 = this->channelCommentRepository_->searchFullText("hello");

  CPPUNIT_ASSERT_EQUAL( (size_t) 19, this->channelCommentRepository_->searchFullText("hello").size());
}
void ChannelCommentRepositoryImportTest::testChannelCommentMetadata( void )
{
  // ----
  // TEST
  ejin::EntityModification containerMetadata( this->channelCommentRepository_->getEntityModification( 1 ) );
  CPPUNIT_ASSERT_EQUAL( jtime( 0L ), containerMetadata.time );
  CPPUNIT_ASSERT_EQUAL( (integer)kNoneModification, containerMetadata.mask );
  CPPUNIT_ASSERT_EQUAL( string(""), containerMetadata.by );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleRead, this->channelCommentRepository_->getAccessRole( 3, "test" ) );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleOwner, this->channelCommentRepository_->getAccessRole( 3, "owner" ) );
}
