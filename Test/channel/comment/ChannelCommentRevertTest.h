/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __COMMENT_REVERT_UNIT_TEST_H__
#define __COMMENT_REVERT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelCommentRevertTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelCommentRevertTest );
  CPPUNIT_TEST( testChannelCommentRevert_Empty );
  CPPUNIT_TEST( testChannelCommentRevert_AddMedia );
  CPPUNIT_TEST( testChannelCommentRevert_Update );
  CPPUNIT_TEST( testChannelCommentRevert_Delete );
  CPPUNIT_TEST( testChannelCommentRevert_Restore );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  // private methods
  void setUp( void );
  
  // ==========
  // Test methods
  // ==========
  
  void testChannelCommentRevert_Empty( void );
  void testChannelCommentRevert_AddMedia( void );
  void testChannelCommentRevert_Update( void );
  void testChannelCommentRevert_Delete( void );
  void testChannelCommentRevert_Restore( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
};

#endif // __COMMENT_REVERT_UNIT_TEST_H__
