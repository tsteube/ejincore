/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __ABSTRACT_CHANNEL_TEST_FIXTURE_H__
#define __ABSTRACT_CHANNEL_TEST_FIXTURE_H__

#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/extensions/HelperMacros.h>

#include "UsingEjinTypes.h"
#include "ChannelHeader.h"
#include "MediaRepository.h"
#include "ApprovedMemberRepository.h"
#include "DBUnit.h"

/*
 * abstract test setup for member unit tests
 */
class AbstractChannelTestFixture : public CPPUNIT_NS::TestFixture
{
  
  // ==========
  // Test setup
  // ==========
public:
  
  virtual void setUp( void );
  virtual void tearDown( void );
  
  // ==========
  // Private Interface
  // ==========
protected:
  
  virtual const string getDbConfigDirectory() = 0;

  unique_ptr<ejin::Database>            db_;
  unique_ptr<DBUnit>                    dbunit_;
  unique_ptr<ChangeLogRepository>       changeLogRepository_;
  unique_ptr<ApprovedMemberRepository>  approvedMemberRepository_;
  unique_ptr<ChannelMemberRepository>   channelMemberRepository_;
  unique_ptr<ChannelMediaRepository>    channelMediaRepository_;
  unique_ptr<ChannelCommentRepository>  channelCommentRepository_;
  unique_ptr<ChannelPostRepository>     channelPostRepository_;
  unique_ptr<ChannelHeaderRepository>   channelHeaderRepository_;
  unique_ptr<ChannelService>            channelService_;
  
  ChannelHeader _TEST_CHANNEL;
  
  struct timeval                     tval_start_;
  struct timeval                     tval_before_;
};

#endif // __ABSTRACT_CHANNEL_TEST_FIXTURE_H__
