/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ChannelBackupTest.h"

#include "Declarations.h"
#include "EjinDatabase.h"
#include "Attachment.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMember.h"
#include "SqliteBaseEntity.h"
#include "SqliteResultSet.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelService.h"

#include <fstream>

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelBackupTest );

void ChannelBackupTest::setUp( void )
{
  AbstractChannelTestFixture::setUp();
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
}

void ChannelBackupTest::testRestoreInsert( void )
{
  // insert post
  shared_ptr<ChannelPost> post( shared_ptr<ChannelPost>(new ejin::ChannelPost()) );
  post->setId( 101L );
  post->setChannelId( 100L );
  post->setContent("content");
  post->setOwner("owner");
  CPPUNIT_ASSERT( this->db_->insert(*post) );
  
  // insert comment
  shared_ptr<ChannelComment> comment( shared_ptr<ChannelComment>(new ejin::ChannelComment()) );
  comment->setId( 101L );
  comment->setChannelId( 100L );
  comment->setContent("content");
  comment->setOwner("owner");
  CPPUNIT_ASSERT( this->db_->insert(*comment) );
  
  // insert media
  shared_ptr<Media> media( shared_ptr<Media>(new ejin::Media()) );
  media->setId( 101L );
  media->setNo( 1 );
  media->setChannelId( 100L );
  media->setName("content");
  media->setOwner("owner");
  CPPUNIT_ASSERT( this->db_->insert(*media) );
  
  // --------
  // T E S T
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
  CPPUNIT_ASSERT( this->db_->restoreChannel( "100" ) );
  
  // revert marker
  post = loadPostById( 101L );
  CPPUNIT_ASSERT( post.get() == NULL );
  
  comment = loadCommentById( 101L );
  CPPUNIT_ASSERT( comment.get() == NULL );
  
  media = loadMediaById( 101L );
  CPPUNIT_ASSERT( media.get() == NULL );
}

void ChannelBackupTest::testRestoreUpdateMetadataOnly( void )
{
  // mark for update
  shared_ptr<ChannelHeader> header( loadHeaderById( 100 ) );
  header->setSyncInd( ejin::kSyncIndUpdate  );
  header->setContent("update");
  header->setSyncAnchor("111");
  this->db_->update(*header);
  
  // --------
  // T E S T
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
  CPPUNIT_ASSERT( this->db_->restoreChannel( "100" ) );
  //
  // --------
  
  // revert marker
  header = loadHeaderById( 100 );
  CPPUNIT_ASSERT( header.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello"), header->getContent() );
  CPPUNIT_ASSERT_EQUAL( string("111"), header->getSyncAnchor() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == header->getSyncInd() );
}

void ChannelBackupTest::testRestoreUpdate( void )
{
  // mark for update
  shared_ptr<ChannelHeader> header( loadHeaderById( 100 ) );
  header->setModifyTime( jtime() );
  header->setSyncInd( ejin::kSyncIndUpdate  );
  header->setContent("update");
  this->db_->update(*header);

  shared_ptr<ChannelPost> post( loadPostById( 100 ) );
  post->setSyncInd( ejin::kSyncIndUpdate  );
  post->setContent("update");
  this->db_->update(*post);

  shared_ptr<ChannelComment> comment( loadCommentById( 100 ) );
  comment->setSyncInd( ejin::kSyncIndUpdate  );
  comment->setContent("update");
  this->db_->update(*comment);
  
  shared_ptr<ChannelMember> member( loadChannelMemberById( 100 ) );
  member->setSyncInd( ejin::kSyncIndUpdate  );
  member->setTags("update");
  this->db_->update(*member);
  
  shared_ptr<Media> media( loadMediaById( 100 ) );
  media->setSyncInd( ejin::kSyncIndUpdate  );
  media->setName("update");
  this->db_->update(*media);
  
  // --------
  // T E S T
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
  CPPUNIT_ASSERT( this->db_->restoreChannel( "100" ) );
  //
  // --------
  
  // revert marker
  header = loadHeaderById( 100 );
  CPPUNIT_ASSERT( header.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello"), header->getContent() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == header->getSyncInd() );

  post = loadPostById( 100 );
  CPPUNIT_ASSERT( post.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("Test"), post->getContent() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == post->getSyncInd() );

  comment = loadCommentById( 100 );
  CPPUNIT_ASSERT( comment.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello"), comment->getContent() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == comment->getSyncInd() );

  member = loadChannelMemberById( 100 );
  CPPUNIT_ASSERT( member.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string(""), member->getTags() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == member->getSyncInd() );
  
  media = loadMediaById( 100 );
  CPPUNIT_ASSERT( media.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string(""), media->getName() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == media->getSyncInd() );
}

void ChannelBackupTest::testRestoreRemove( void )
{
  // mark for delete
  shared_ptr<ChannelHeader> header( loadHeaderById( 100 ) );
  header->setModifyTime( jtime() );
  header->setSyncInd( ejin::kSyncIndDelete  );
  this->db_->update(*header);
  
  shared_ptr<ChannelPost> post( loadPostById( 100 ) );
  post->setSyncInd( ejin::kSyncIndDelete  );
  this->db_->update(*post);
  
  shared_ptr<ChannelComment> comment( loadCommentById( 100 ) );
  post->setSyncInd( ejin::kSyncIndDelete  );
  this->db_->update(*comment);

  shared_ptr<ChannelMember> member( loadChannelMemberById( 100 ) );
  member->setSyncInd( ejin::kSyncIndDelete  );
  this->db_->update(*member);

  shared_ptr<Media> media( loadMediaById( 100 ) );
  media->setSyncInd( ejin::kSyncIndDelete  );
  this->db_->update(*media);
  
  // --------
  // T E S T
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
  CPPUNIT_ASSERT( this->db_->restoreChannel( "100" ) );
  //
  // --------

  // revert marker
  header = loadHeaderById( 100 );
  CPPUNIT_ASSERT( header.get() != NULL );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == header->getSyncInd() );
  
  post = loadPostById( 100 );
  CPPUNIT_ASSERT( post.get() != NULL );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == post->getSyncInd() );
  
  comment = loadCommentById( 100 );
  CPPUNIT_ASSERT( comment.get() != NULL );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == comment->getSyncInd() );

  member = loadChannelMemberById( 100 );
  CPPUNIT_ASSERT( member.get() != NULL );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == member->getSyncInd() );
  
  media = loadMediaById( 100 );
  CPPUNIT_ASSERT( media.get() != NULL );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == media->getSyncInd() );
}

void ChannelBackupTest::testRestoreConflict( void )
{
  // mark for update
  shared_ptr<ChannelHeader> header( loadHeaderById( 100 ) );
  header->setModifyTime( jtime() );
  header->setSyncInd( ejin::kSyncIndConflict  );
  header->setContent("update");
  this->db_->update(*header);
  header = shared_ptr<ChannelHeader>(new ejin::ChannelHeader());
  header->setId( 101L );
  header->setMasterId( 100L );
  header->setName( "old" );
  header->setContent( "old" );
  header->setOwner( "owner" );
  CPPUNIT_ASSERT( this->db_->insert(*header) );

  shared_ptr<ChannelPost> post( loadPostById( 100 ) );
  post->setSyncInd( ejin::kSyncIndConflict  );
  post->setContent("update");
  this->db_->update(*post);
  post = shared_ptr<ChannelPost>(new ejin::ChannelPost());
  post->setId( 101L );
  post->setMasterId( 100L );
  post->setChannelId( 100L );
  post->setContent( "old" );
  post->setOwner( "owner" );
  CPPUNIT_ASSERT( this->db_->insert(*post) );

  shared_ptr<ChannelComment> comment( loadCommentById( 100 ) );
  comment->setSyncInd( ejin::kSyncIndConflict  );
  comment->setContent("update");
  this->db_->update(*comment);
  comment = shared_ptr<ChannelComment>(new ejin::ChannelComment());
  comment->setId( 101L );
  comment->setMasterId( 100L );
  comment->setChannelId( 100L );
  comment->setContent( "old" );
  comment->setOwner( "owner" );
  CPPUNIT_ASSERT( this->db_->insert(*comment) );
  
  shared_ptr<ChannelMember> member( loadChannelMemberById( 100 ) );
  member->setSyncInd( ejin::kSyncIndConflict  );
  member->setTags("update");
  this->db_->update(*member);
  member = shared_ptr<ChannelMember>(new ejin::ChannelMember());
  member->setId( 101L );
  member->setUsername( "owner" );
  member->setMasterId( 100L );
  member->setChannelId( 100L );
  member->setTags( "old" );
  CPPUNIT_ASSERT( this->db_->insert(*member) );

  shared_ptr<Media> media( loadMediaById( 100 ) );
  media->setSyncInd( ejin::kSyncIndConflict  );
  media->setName("update");
  this->db_->update(*media);
  media = shared_ptr<Media>(new ejin::Media());
  media->setId( 101 );
  media->setMasterId( 100 );
  media->setChannelId( 100 );
  media->setNo( 1 );
  media->setName("old");
  media->setOwner("owner");
  CPPUNIT_ASSERT( this->db_->insert(*media) );

  // --------
  // T E S T
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
  CPPUNIT_ASSERT( this->db_->restoreChannel( "100" ) );
  //
  // --------
  
  // revert marker
  header = loadHeaderById( 100 );
  CPPUNIT_ASSERT( header.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("update"), header->getContent() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == header->getSyncInd() );
  header = loadHeaderById( 101 );
  CPPUNIT_ASSERT( header.get() == NULL );

  post = loadPostById( 100 );
  CPPUNIT_ASSERT( post.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("update"), post->getContent() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == post->getSyncInd() );
  post = loadPostById( 101 );
  CPPUNIT_ASSERT( post.get() == NULL );

  comment = loadCommentById( 100 );
  CPPUNIT_ASSERT( comment.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("update"), comment->getContent() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == comment->getSyncInd() );
  comment = loadCommentById( 101 );
  CPPUNIT_ASSERT( comment.get() == NULL );

  member = loadChannelMemberById( 100 );
  CPPUNIT_ASSERT( member.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("update"), member->getTags() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == member->getSyncInd() );
  member = loadChannelMemberById( 101 );
  CPPUNIT_ASSERT( member.get() == NULL );

  media = loadMediaById( 100 );
  CPPUNIT_ASSERT( media.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string("update"), media->getName() );
  CPPUNIT_ASSERT( ejin::kSyncIndSynchronous == media->getSyncInd() );
  media = loadMediaById( 101 );
  CPPUNIT_ASSERT( media.get() == NULL );
}

void ChannelBackupTest::testRestoreConflict_Attachment( void )
{
  // preconditions
  Value mediaId( (integer)100 );
  shared_ptr<Attachment> attachment( this->loadAttachment( mediaId, false ) );
  shared_ptr<Attachment> attachment2( shared_ptr<Attachment>(new ejin::Attachment()) );
  attachment2->setMasterId( attachment->getId() );
  attachment2->setMediaId( 100L );
  attachment2->setData("hello world", 11);
  CPPUNIT_ASSERT( this->db_->insert(*attachment2) );
  
  // ------
  // T E S T
  //
  CPPUNIT_ASSERT( this->db_->backupMember( ) );
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
  CPPUNIT_ASSERT( this->db_->restoreChannel( "100" ) );
  // ------

  // post conditions
  attachment = this->loadAttachment( mediaId, false );
  CPPUNIT_ASSERT( attachment );
  CPPUNIT_ASSERT( strncmp("hello world", attachment->getData(), 11) == 0 );
  CPPUNIT_ASSERT( ! this->loadAttachment( mediaId, true ) );
}

// -- helper

shared_ptr<ChannelHeader> ChannelBackupTest::loadHeaderById( integer id )
{
  Value channelId( id );
  unique_ptr<ResultSet> values( this->db_->find("CHANNEL_TBL", "findChannelById", &channelId, NULL) );
  
  shared_ptr<ChannelHeader> header;
  vector< sqlite::BaseEntity* >::iterator it = values->begin();
  if (it != values->end()) {
    // initialize found member
    header = shared_ptr<ChannelHeader>(static_cast< ChannelHeader* >(*it));
    it = values->take(it);
    return header;
  }
  return shared_ptr<ChannelHeader>();
}
shared_ptr<ChannelPost> ChannelBackupTest::loadPostById( integer id )
{
  Value channelId( id );
  unique_ptr<ResultSet> values( this->db_->find("POST_TBL", "findPostById", &channelId, NULL) );
  
  shared_ptr<ChannelPost> header;
  vector< sqlite::BaseEntity* >::iterator it = values->begin();
  if (it != values->end()) {
    // initialize found member
    header = shared_ptr<ChannelPost>(static_cast< ChannelPost* >(*it));
    it = values->take(it);
    return header;
  }
  return shared_ptr<ChannelPost>();
}
shared_ptr<ChannelComment> ChannelBackupTest::loadCommentById( integer id )
{
  Value channelId( id );
  unique_ptr<ResultSet> values( this->db_->find("COMMENT_TBL", "findCommentById", &channelId, NULL) );
  
  shared_ptr<ChannelComment> header;
  vector< sqlite::BaseEntity* >::iterator it = values->begin();
  if (it != values->end()) {
    // initialize found member
    header = shared_ptr<ChannelComment>(static_cast< ChannelComment* >(*it));
    it = values->take(it);
    return header;
  }
  return shared_ptr<ChannelComment>();
}
shared_ptr<ChannelMember> ChannelBackupTest::loadChannelMemberById( integer id )
{
  Value mediaId( id );
  unique_ptr<ResultSet> values( this->db_->find("CHANNEL_MEMBER_TBL", "findChannelMemberById", &mediaId, NULL) );
  
  shared_ptr<ChannelMember> member;
  vector< sqlite::BaseEntity* >::iterator it = values->begin();
  if (it != values->end()) {
    // initialize found member
    member = shared_ptr<ChannelMember>(static_cast< ChannelMember* >(*it));
    it = values->take(it);
    return member;
  }
  return shared_ptr<ChannelMember>();
}
shared_ptr<Media> ChannelBackupTest::loadMediaById( integer id )
{
  Value mediaId( id );
  unique_ptr<ResultSet> values( this->db_->find("MEDIA_TBL", "findMediaById", &mediaId, NULL) );
  
  shared_ptr<Media> header;
  vector< sqlite::BaseEntity* >::iterator it = values->begin();
  if (it != values->end()) {
    // initialize found member
    header = shared_ptr<Media>(static_cast< Media* >(*it));
    it = values->take(it);
    return header;
  }
  return shared_ptr<Media>();
}
// helper
shared_ptr<Attachment> ChannelBackupTest::loadAttachment( const Value& mediaId, bool hidden ) const
{
  // call entity finder
  unique_ptr<ResultSet> values;
  if ( hidden )
    values = db_->find(Attachment::TABLE_NAME, "findSlave", &mediaId, NULL);
  else
    values = db_->find(Attachment::TABLE_NAME, "findMaster", &mediaId, NULL);
  return shared_ptr<Attachment>(static_cast< Attachment* >( values->takeFirst() ) );
}

