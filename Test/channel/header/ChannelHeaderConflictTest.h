/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __CHANNEL_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelHeaderConflictTest : public AbstractChannelTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelHeaderConflictTest );
  CPPUNIT_TEST( testAddChannelContent );
  CPPUNIT_TEST( testAddChannelContent_Empty );
  CPPUNIT_TEST( testUpdateChannelContent_LocalUnchanged );
  CPPUNIT_TEST( testUpdateChannelContent_LocalAdd );
  CPPUNIT_TEST( testUpdateChannelContent_LocalUpdate_EqualContent );
  CPPUNIT_TEST( testUpdateChannelContent_LocalUpdate );
  CPPUNIT_TEST( testUpdateChannelContent_LocalDelete_EqualContent );
  CPPUNIT_TEST( testUpdateChannelContent_LocalDelete );
  CPPUNIT_TEST( testDeleteChannelContent_LocalUnchanged );
  CPPUNIT_TEST( testDeleteChannelContent_LocalAdd );
  CPPUNIT_TEST( testDeleteChannelContent_LocalUpdate );
  CPPUNIT_TEST( testDeleteChannelContent_LocalDelete );
  CPPUNIT_TEST( testDeleteChannelContent_LocalDelete_Conflict );
  CPPUNIT_TEST( testForceClearConflict );
  CPPUNIT_TEST_SUITE_END();

public:

  // ==========
  // Test methods
  // ==========
public:
  
  void testAddChannelContent( void );
  void testAddChannelContent_Empty( void );
  void testUpdateChannelContent_LocalUnchanged( void );
  void testUpdateChannelContent_LocalAdd( void );
  void testUpdateChannelContent_LocalUpdate_EqualContent( void );
  void testUpdateChannelContent_LocalUpdate( void );
  void testUpdateChannelContent_LocalDelete_EqualContent( void );
  void testUpdateChannelContent_LocalDelete( void );
  void testDeleteChannelContent_LocalUnchanged( void );
  void testDeleteChannelContent_LocalAdd( void );
  void testDeleteChannelContent_LocalUpdate( void );
  void testDeleteChannelContent_LocalDelete( void );
  void testDeleteChannelContent_LocalDelete_Conflict( void );
  void testForceClearConflict( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/header/content_conflict"; }
  shared_ptr<ChannelHeader> loadByGid( const string& gid, const string& syncAnchor, SyncInd state );
  shared_ptr<ChannelHeader> validate( const string& gid, const string& syncAnchor, SyncInd state, long expectedTotalNumOfChannels );

};

#endif // __CHANNEL_CONTENT_RESOLVE_CONFLICT_UNIT_TEST_H__
