/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderRepositoryCommitTest.h"

#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelHeader.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelService.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelHeaderRepositoryCommitTest );

// ==========
// TEST Methods
// ==========

void ChannelHeaderRepositoryCommitTest::testCommitAddedHeader( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 11 );
  header->setGid( string( "11" ) );
  header->setRole( kSyncRoleWrite );
  header->setMembership( kMembershipAccepted );
  header->setSessionKey( "key" );
  header->setIV( "iv" );
  header->setSecure( false );

  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "11" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 11, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "11" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), header->getIV() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitAddedHeaderWithResource( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 12 );
  header->setGid( string( "12" ) );

  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 12 );
  media->setGid( string( "12" ) );
  header->getResources().push_back( media );

  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "12" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 12, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getResources().size() );
  media = *header->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 12, media->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ), media->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndInsert, media->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitAddedHeaderWithMember( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 11 );
  header->setGid( string( "11" ) );
  header->setRole( kSyncRoleWrite );
  header->setMembership( kMembershipAccepted );
  header->setSessionKey( "key" );
  
  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "11" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 11, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "11" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer)kSyncRoleWrite, header->getRole() );
  CPPUNIT_ASSERT_EQUAL( (integer)kMembershipAccepted, header->getMembership() );
  CPPUNIT_ASSERT_EQUAL( string( "key" ), header->getSessionKey() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ), header->getModifiedBy() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  CPPUNIT_ASSERT( header->hasSessionKey() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getMembers().size() );
  shared_ptr<ChannelMember> member = *header->getMembers().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 11, member->getChannelId() );
  CPPUNIT_ASSERT_EQUAL( (integer)kSyncRoleWrite, member->getRole() );
  CPPUNIT_ASSERT_EQUAL( (integer)kMembershipAccepted, member->getMembership() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT( member->hasFingerPrint() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitUpdatedHeader( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 14 );
  header->setGid( string( "14" ) );
  header->setIV( string( "iv" ) );
  header->setRole( kSyncRoleWrite );
  header->setMembership( kMembershipAccepted );
  header->setSessionKey( "key" );

  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "14" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 14, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "14" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer)kSyncRoleWrite, header->getRole() );
  CPPUNIT_ASSERT_EQUAL( (integer)kMembershipAccepted, header->getMembership() );
  CPPUNIT_ASSERT_EQUAL( string( "key" ), header->getSessionKey() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), header->getIV() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ), header->getModifiedBy() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitAddedResource( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 15 );
  header->setGid( string( "15" ) );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 15 );
  media->setGid( string( "15" ) );
  header->getResources().push_back( media );
  
  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "15" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 15, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "15" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getResources().size() );
  media = *header->getResources().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 15, media->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "15" ), media->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndInsert, media->getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedResource( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 16 );
  header->setGid( string( "16" ) );
  
  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "16" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 16, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "16" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 16 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );
 
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitAddedMember( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 17 );
  header->setGid( string( "17" ) );
  header->setSessionKey( "key" );

  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember());
  member->setId( 18 );
  member->setUsername( "owner" );
  member->setIV( "iv" );
  header->getMembers().push_back( member );
  
  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "17" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 17, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "17" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ), header->getModifiedBy() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getMembers().size() );
  member = *header->getMembers().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 17, member->getChannelId() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), member->getIV() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );

  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitUpdatedMember( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 14 );
  header->setGid( string( "14" ) );
  header->setRole( kSyncRoleWrite );
  header->setMembership( kMembershipAccepted );
  header->setSessionKey( "key" );
  
  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember());
  member->setId( 15 );
  member->setUsername( "owner" );
  member->setIV( "iv" );
  header->getMembers().push_back( member );
  
  jtime now;
  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), now );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "14" ) );
  CPPUNIT_ASSERT_EQUAL( (integer) 14, header->getId() );
  CPPUNIT_ASSERT_EQUAL( string( "14" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer)kSyncRoleWrite, header->getRole() );
  CPPUNIT_ASSERT_EQUAL( (integer)kMembershipAccepted, header->getMembership() );
  CPPUNIT_ASSERT_EQUAL( string( "key" ), header->getSessionKey() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), header->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ), header->getModifiedBy() );
  CPPUNIT_ASSERT( header->hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, header->hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getMembers().size() );
  member = *header->getMembers().begin();
  CPPUNIT_ASSERT_EQUAL( (integer) 14, member->getChannelId() );
  CPPUNIT_ASSERT_EQUAL( (integer)kSyncRoleWrite, member->getRole() );
  CPPUNIT_ASSERT_EQUAL( (integer)kMembershipAccepted, member->getMembership() );
  CPPUNIT_ASSERT( ! member->hasSessionKey() );
  CPPUNIT_ASSERT( member->hasFingerPrint() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, member->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), member->getIV() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedMember( void )
{
  // No need to commit data to channel member!
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedHeader( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 20 );
  header->setGid( string( "20" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), jtime() );
  
  // header conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedHeaderWithResource( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 21 );
  header->setGid( string( "21" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), jtime() );
  
  // header conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );  
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 21 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  

  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedHeaderWithMember( void )
{
  // No need to commit data to channel member!
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedHeaderWithPost( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 23 );
  header->setGid( string( "23" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), jtime() );
  
  // header conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );  
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 23 );
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  

  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testCommitDeletedHeaderWithComment( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 24 );
  header->setGid( string( "24" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  // ----
  // TEST
  this->channelHeaderRepository_->commit( *header, string( "100" ), string( "owner" ), jtime() );
  
  // header conditions
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );  
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 24 );
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  

  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryCommitTest::testModificationMask( void )
{
  ejin::EntityModification channelMetadata( this->channelHeaderRepository_->getChannelModification( 1 ) );
  CPPUNIT_ASSERT_EQUAL( jtime( 0L ), channelMetadata.time );
  CPPUNIT_ASSERT_EQUAL( (integer)kNoneModification, channelMetadata.mask );
  CPPUNIT_ASSERT_EQUAL( string("owner"), channelMetadata.by );
}
