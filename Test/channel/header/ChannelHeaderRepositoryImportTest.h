/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_HEADER_REPOSITORY_IMPORT_UNIT_TEST_H__
#define __CHANNEL_HEADER_REPOSITORY_IMPORT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelHeaderRepositoryImportTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelHeaderRepositoryImportTest );  
  CPPUNIT_TEST( testImportAddHeader );
  CPPUNIT_TEST( testImportAddHeaderWithResource );
  CPPUNIT_TEST( testImportAddHeaderWithMember );
  CPPUNIT_TEST( testImportUpdateHeader );
  CPPUNIT_TEST( testImportUpdateHeader_Identical );
  CPPUNIT_TEST( testImportUpdateHeader_Conflict );
  CPPUNIT_TEST( testImportAddResource );
  CPPUNIT_TEST( testImportDeleteResource );
  CPPUNIT_TEST( testImportAddMember );
  CPPUNIT_TEST( testImportUpdateMember );
  CPPUNIT_TEST( testImportDeleteMember );
  CPPUNIT_TEST( testImportUpdateMember_Conflict );
  CPPUNIT_TEST( testImportUpdateMembershipOnly );
  CPPUNIT_TEST( testImportDeleteHeader );
  CPPUNIT_TEST( testImportDeleteHeaderWithResource );
  CPPUNIT_TEST( testImportDeleteHeaderWithMember );
  CPPUNIT_TEST( testImportDeleteHeaderWithPost );
  CPPUNIT_TEST( testImportDeleteHeaderWithComment );
  CPPUNIT_TEST( testImportDeleteHeader_Conflict );
  CPPUNIT_TEST( testChannelHeaderMetadata );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testImportAddHeader( void );
  void testImportAddHeaderWithResource( void );
  void testImportAddHeaderWithMember( void );
  void testImportUpdateHeader( void );
  void testImportUpdateHeader_Identical( void );
  void testImportUpdateHeader_Conflict( void );
  void testImportAddResource( void );
  void testImportDeleteResource( void );
  void testImportAddMember( void );
  void testImportUpdateMember( void );
  void testImportUpdateMember_Conflict( void );
  void testImportDeleteMember( void );
  void testImportUpdateMembershipOnly( void );  
  void testImportDeleteHeader( void );
  void testImportDeleteHeaderWithResource( void );
  void testImportDeleteHeaderWithMember( void );
  void testImportDeleteHeaderWithPost( void );
  void testImportDeleteHeaderWithComment( void );
  void testImportDeleteHeader_Conflict( void );
  void testChannelHeaderMetadata( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/header"; }
};

#endif // __MEMBER_HEADER_IMPORT_UNIT_TEST_H__
