/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderRepositoryImportTest.h"

#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelService.h"
#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelService.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelHeaderRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void ChannelHeaderRepositoryImportTest::testImportAddHeader( void )
{  
  // pre conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setName( "name" );  
  header->setContent( "test" );
  header->setOwner( this->_TEST_CHANNEL.getOwner() );
  header->setGid( string( "99" ) );
  header->setState( 0 );
  header->setSyncAnchor( string( "100" ) );
  header->setIV( string( "iv" ) );

  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelHeaderRepository_->searchFullText("test").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelHeaderRepository_->searchFullText("name").size());

  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "99" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT( header->hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), header->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelHeaderRepository_->searchFullText("test").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelHeaderRepository_->searchFullText("name").size());
}

void ChannelHeaderRepositoryImportTest::testImportAddHeaderWithResource( void )
{
  // pre conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setName( "name" );  
  header->setContent( "test" );
  header->setOwner( this->_TEST_CHANNEL.getOwner() );
  header->setGid( string( "99" ) );
  header->setState( 0 );
  header->setSyncAnchor( string( "100" ) );
  
  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setGid( string( "99" ) );
  media->setOwner( this->_TEST_CHANNEL.getOwner() );
  header->getResources().push_back( media );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "99" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT( header->hasId() );
  CPPUNIT_ASSERT_EQUAL(  string( "99" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getResources().size() );
  media = *header->getResources().begin();
  CPPUNIT_ASSERT_EQUAL(  string( "99" ), media->getGid() );
  CPPUNIT_ASSERT( media->isAttachmentOutdated() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportAddHeaderWithMember( void )
{
  // pre conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setName( "name" );  
  header->setContent( "test" );
  header->setOwner( this->_TEST_CHANNEL.getOwner() );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  header->setGid( string( "99" ) );
  header->setState( 0 );
  header->setSyncAnchor( string( "100" ) );
  
  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember());
  member->setUsername( "member" );
  member->setTags( "tags" );
  header->getMembers().push_back( member );        
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "99" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT( header->hasId() );
  CPPUNIT_ASSERT_EQUAL(  string( "99" ), header->getGid() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getMembers().size() );
  member = *header->getMembers().begin();
  CPPUNIT_ASSERT( member != NULL );
  CPPUNIT_ASSERT_EQUAL( string("member"), member->getUsername() );
  CPPUNIT_ASSERT_EQUAL( string("tags"), member->getTags() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportUpdateHeader( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "2" ) ));
  CPPUNIT_ASSERT( header != NULL );
  
  header->setSyncAnchor( string( "100" ) );
  header->setContent( "hello world" );
  header->setIV( string( "iv" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  CPPUNIT_ASSERT_EQUAL( (size_t) 15, this->channelHeaderRepository_->searchFullText("hello").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->channelHeaderRepository_->searchFullText("world").size());
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  header = this->channelHeaderRepository_->loadByGid( string( "2" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), header->getContent() );
  CPPUNIT_ASSERT_EQUAL( string("iv"), header->getIV() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 15, this->channelHeaderRepository_->searchFullText("hello").size());
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->channelHeaderRepository_->searchFullText("world").size());
}

void ChannelHeaderRepositoryImportTest::testImportUpdateHeader_Identical( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "3" ) ));
  CPPUNIT_ASSERT( header != NULL );
  
  header->setSyncAnchor( string( "100" ) );
  header->setName( "channel3" );  
  header->setContent( "hello" );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  long numberOfChannelMembers = this->db_->count( "CHANNEL_MEMBER_TBL" );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  header = this->channelHeaderRepository_->loadByGid( string( "3" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello"), header->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  CPPUNIT_ASSERT_EQUAL( numberOfChannelMembers, this->db_->count( "CHANNEL_MEMBER_TBL" ) );
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->detectChannelConflict( string( "3" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportUpdateHeader_Conflict( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "3" ) ));
  CPPUNIT_ASSERT( header != NULL );
  
  header->setSyncAnchor( string( "100" ) );
  header->setName( "new channel" );  
  header->setContent( "hello world" );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  long numberOfChannelMembers = this->db_->count( "CHANNEL_MEMBER_TBL" );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  header = this->channelHeaderRepository_->loadByGid( string( "3" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello world"), header->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  CPPUNIT_ASSERT_EQUAL( numberOfChannelMembers, this->db_->count( "CHANNEL_MEMBER_TBL" ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->detectChannelConflict( string( "3" ) ) );
  CPPUNIT_ASSERT( ! this->channelService_->detectMemberConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelHeaderRepository_->clearConflict( string( "3" ) ) );
  // Note: the dependent channel member entities gets deleted by foreign keys, ie. on commit
  CPPUNIT_ASSERT_EQUAL( numberOfChannelMembers, this->db_->count( "CHANNEL_MEMBER_TBL" ) );
  
  // header condition
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->detectChannelConflict( string( "3" ) ) );
  this->db_->refresh(*header);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT( ! this->channelService_->detectMemberConflict( string( "3" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportAddResource( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "2" ) ));
  CPPUNIT_ASSERT( header != NULL );
  
  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  shared_ptr<Resource> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setGid( string( "99" ) );
  media->setOwner( this->_TEST_CHANNEL.getOwner() );
  header->getResources().push_back( media );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "2" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getResources().size() );
  media = *header->getResources().begin();
  CPPUNIT_ASSERT_EQUAL(  string( "99" ), media->getGid() );
  CPPUNIT_ASSERT( media->isAttachmentOutdated() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteResource( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "1" ) ));
  CPPUNIT_ASSERT( header != NULL );
  
  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  header->getResources( ).clear();
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "1" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT( header->hasId() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getResources().size() );
  
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 1L );
  CPPUNIT_ASSERT( this->db_->contains(*media) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( ! this->changeLogRepository_->end() );
}

void ChannelHeaderRepositoryImportTest::testImportAddMember( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "2" ) ));
  CPPUNIT_ASSERT( header != NULL );
  
  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  
  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember());
  member->setUsername( "member" );
  member->setTags( "tags" );
  member->setIV( "iv" );
  header->getMembers().push_back( member );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "2" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, header->getMembers().size() );
  member = Utilities::findElementIn (header->getMembers(), *member);
  CPPUNIT_ASSERT_EQUAL( string("member"), member->getUsername() );
  CPPUNIT_ASSERT_EQUAL( string("tags"), member->getTags() );
  CPPUNIT_ASSERT_EQUAL( string("iv"), member->getIV() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportUpdateMember( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "1" ) ));
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, header->getMembers().size() );
  
  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  
  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember(1, "member"));
  member = Utilities::findElementIn (header->getMembers(), *member);
  CPPUNIT_ASSERT( member != NULL );
  
  member->setTags( "hallo" );
  member->setModifyTime( jtime() );
  member->setIV( "iv" );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "1" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, header->getMembers().size() );
  member = Utilities::findElementIn (header->getMembers(), *member);
  CPPUNIT_ASSERT_EQUAL( string("member"), member->getUsername() );
  CPPUNIT_ASSERT_EQUAL( string("hallo"), member->getTags() );
  CPPUNIT_ASSERT_EQUAL( string("iv"), member->getIV() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteMember( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "1" ) ));
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, header->getMembers().size() );
  
  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  
  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember(1, "member"));
  member = Utilities::findElementIn (header->getMembers(), *member);
  CPPUNIT_ASSERT( member != NULL );
  header->getMembers().remove( member );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "1" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, header->getMembers().size() );
  CPPUNIT_ASSERT( ! this->db_->contains(*member) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportUpdateMember_Conflict( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "4" ) ));
  CPPUNIT_ASSERT( header != NULL );

  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  header->getMembers().clear();
  
  long numberOfChannelMembers = this->db_->count( "CHANNEL_MEMBER_TBL" );
  
  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  header = this->channelHeaderRepository_->loadByGid( string( "4" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( string("hello"), header->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, header->getResources().size() );
  CPPUNIT_ASSERT_EQUAL( numberOfChannelMembers + 1, this->db_->count( "CHANNEL_MEMBER_TBL" ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->detectChannelConflict( string( "4" ) ) );
  CPPUNIT_ASSERT( this->channelService_->detectMemberConflict( string( "4" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelHeaderRepository_->clearConflict( string( "4" ) ) );
  // Note: the dependent channel member entities gets deleted by foreign keys, ie. on commit
  CPPUNIT_ASSERT_EQUAL( numberOfChannelMembers - 1, this->db_->count( "CHANNEL_MEMBER_TBL" ) );
  
  // header condition
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->detectChannelConflict( string( "4" ) ) );
  this->db_->refresh(*header);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT( ! this->channelService_->detectMemberConflict( string( "4" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportUpdateMembershipOnly( void )
{
  // preparation
  shared_ptr<ChannelHeader> header(this->channelHeaderRepository_->loadByGid( string( "1" ) ));
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, header->getMembers().size() );
  
  header->clearContent( );
  header->clearName( );
  header->setSyncAnchor( string( "100" ) );
  header->setModifiedBy( this->_TEST_CHANNEL.getOwner() );

  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember(1, "member"));
  member = Utilities::findElementIn (header->getMembers(), *member);
  CPPUNIT_ASSERT( member != NULL );
  
  member->setRole( ejin::kSyncRoleAdmin );
  member->setModifyTime( jtime() );

  // ----
  // TEST
  this->channelHeaderRepository_->update( *header, true, jtime() );
  
  // header conditions
  header = this->channelHeaderRepository_->loadByGid( string( "1" ) );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT( header->hasContent( ) );
  CPPUNIT_ASSERT( header->hasName( ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 2, header->getMembers().size() );
  member = Utilities::findElementIn (header->getMembers(), *member);
  CPPUNIT_ASSERT_EQUAL( string("member"), member->getUsername() );
  CPPUNIT_ASSERT( ejin::kSyncRoleAdmin == member->getRole() );
    
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannelMember, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteHeader( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "2" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "100" );
  channel->setSyncTime( jtime() );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 15, this->channelHeaderRepository_->searchFullText("hello").size());
  
  // ----
  // TEST
  this->channelHeaderRepository_->deleteChannel( *channel, jtime() );
  
  // header conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 2 );
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 14, this->channelHeaderRepository_->searchFullText("hello").size());
}

void ChannelHeaderRepositoryImportTest::testImportDeleteHeaderWithResource( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "1" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "100" );
  channel->setSyncTime( jtime() );

  // ----
  // TEST
  this->channelHeaderRepository_->deleteChannel( *channel, jtime() );
  
  // header conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia());
  media->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*media) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteHeaderWithMember( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "1" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "100" );
  channel->setSyncTime( jtime() );

  // ----
  // TEST
  this->channelHeaderRepository_->deleteChannel( *channel, jtime() );
  
  // header conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );
  shared_ptr<ChannelMember> member = shared_ptr<ChannelMember>(new ChannelMember(1,"member"));
  CPPUNIT_ASSERT( ! this->db_->contains(*member) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteHeaderWithPost( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "1" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "100" );
  channel->setSyncTime( jtime() );

  // ----
  // TEST
  this->channelHeaderRepository_->deleteChannel( *channel, jtime() );
  
  // header conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 1 );
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteHeaderWithComment( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "2" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "100" );
  channel->setSyncTime( jtime() );
  
  // ----
  // TEST
  this->channelHeaderRepository_->deleteChannel( *channel, jtime() );
  
  // header conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 2 );
  CPPUNIT_ASSERT( ! this->db_->contains(*header) );
  shared_ptr<ChannelPost> post = shared_ptr<ChannelPost>(new ChannelPost());
  post->setId( 2 );
  CPPUNIT_ASSERT( ! this->db_->contains(*post) );  
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}

void ChannelHeaderRepositoryImportTest::testImportDeleteHeader_Conflict( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "3" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "100" );
  channel->setSyncTime( jtime() );

  // ----
  // TEST
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->deleteChannel( *channel, jtime() ) );
  
  // header conditions
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setId( 3 );
  CPPUNIT_ASSERT( this->db_->contains(*header) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->detectChannelConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->channelHeaderRepository_->clearConflict( string( "3" ) ) );
  
  // header condition
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->detectChannelConflict( string( "3" ) ) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceChannel, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( this->_TEST_CHANNEL.getOwner() , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId3() );
}
void ChannelHeaderRepositoryImportTest::testChannelHeaderMetadata( void )
{
  // ----
  // TEST
  ejin::EntityModification containerMetadata( this->channelHeaderRepository_->getChannelModification( 1 ) );
  CPPUNIT_ASSERT_EQUAL( jtime( 0L ), containerMetadata.time );
  CPPUNIT_ASSERT_EQUAL( (integer)kNoneModification, containerMetadata.mask );
  CPPUNIT_ASSERT_EQUAL( string("owner"), containerMetadata.by );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleRead, this->channelHeaderRepository_->getAccessRole( 3, "test" ) );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleOwner, this->channelHeaderRepository_->getAccessRole( 3, "owner" ) );
}
