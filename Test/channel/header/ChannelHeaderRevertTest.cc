/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderRevertTest.h"

#include "ChannelService.h"
#include "ChannelMemberRepository.h"
#include "ChannelHeader.h"
#include "ChannelMember.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "MemberProfile.h"
#include "Media.h"
#include "ChangeLogRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelHeaderRevertTest );

void ChannelHeaderRevertTest::setUp( void )
{
  AbstractChannelTestFixture::setUp();
  this->_TEST_CHANNEL.setId( 100L );
  this->_TEST_CHANNEL.setGid( string( "100" ) );
  this->_TEST_CHANNEL.setOwner( "owner" );

  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
}

// ==========
// TEST Methods
// ==========

void ChannelHeaderRevertTest::testChannelHeaderRevert_Empty( void )
{
  ChannelHeader header;
  header.setId( 100L );
  CPPUNIT_ASSERT( this->db_->contains(header) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( ! this->channelService_->revert( "Channel/100", false ) );
  
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndSynchronous );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_AddMember( void )
{
  ChannelMember member;
  member.setChannelId( 100L );
  member.setUsername( "member" );
  CPPUNIT_ASSERT( this->db_->insert(member) );
  CPPUNIT_ASSERT_EQUAL( member.getId(), (integer)101 );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Member", false ) );
  
  ChannelHeader header;
  header.setId(100L);
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndSynchronous );
  
  this->db_->refresh( member );
  CPPUNIT_ASSERT( ! member.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)member.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_AddMedia( void )
{
  Media media;
  media.setChannelId( 100L );
  media.setName( "new media" );
  media.setOwner( "owner" );
  media.setNo( 1 );
  CPPUNIT_ASSERT( this->db_->insert(media) );
  CPPUNIT_ASSERT_EQUAL( media.getId(), (integer)101 );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Media", false ) );
  
  ChannelHeader header;
  header.setId(100L);
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndSynchronous );
  
  this->db_->refresh(media);
  CPPUNIT_ASSERT( ! media.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_AddPost( void )
{
  ChannelPost post;
  post.setChannelId( 100L );
  post.setContent( "new post" );
  post.setOwner( "owner" );
  CPPUNIT_ASSERT( this->db_->insert(post) );
  CPPUNIT_ASSERT_EQUAL( post.getId(), (integer)101 );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Post", false ) );
  
  ChannelHeader header;
  header.setId(100L);
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndSynchronous );
  
  this->db_->refresh( post );
  CPPUNIT_ASSERT( ! post.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)post.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_AddComment( void )
{
  ChannelComment comment;
  comment.setChannelId( 100L );
  comment.setContent( "new comment" );
  comment.setOwner( "owner" );
  CPPUNIT_ASSERT( this->db_->insert(comment) );
  CPPUNIT_ASSERT_EQUAL( comment.getId(), (integer)101 );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100/Comment", false ) );
  
  ChannelHeader header;
  header.setId(100L);
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndSynchronous );
  
  this->db_->refresh( comment );
  CPPUNIT_ASSERT( ! comment.isHidden() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)comment.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_Update( void )
{
  // preparation
  shared_ptr<ChannelHeader> header = this->channelHeaderRepository_->loadByGid( "100" );
  header->setContent( "tested" );
  header->setSyncInd( kSyncIndUpdate );
  CPPUNIT_ASSERT( this->db_->update(*header) );

  Value channelId(header->getId());
  Value username("owner");
  Value hidden(false);
  shared_ptr<ChannelMember> member =
    this->channelMemberRepository_->loadChannelMemberInternal( channelId, username, hidden );
  member->setTags( "test" );
  member->setSyncInd( kSyncIndUpdate );
  CPPUNIT_ASSERT( this->db_->update(*member) );

  // ----
  // TEST
  CPPUNIT_ASSERT( this->channelHeaderRepository_->revert( string( "100" ) ) );

  header = this->channelHeaderRepository_->loadByGid( "100" );
  CPPUNIT_ASSERT( "hello" == header->getContent() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, header->getSyncInd() );

  member = this->channelMemberRepository_->loadChannelMemberInternal( channelId, username, hidden );
  CPPUNIT_ASSERT( ! member->hasTags() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, member->getSyncInd() );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_Delete( void )
{
  ChannelHeader header;
  header.setId( 100L );
  this->db_->refresh(header);
  header.setSyncInd(ejin::kSyncIndDelete);
  CPPUNIT_ASSERT( this->db_->update(header) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelService_->revert( "Channel/100", false ) );
  
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndConflict );
}
void ChannelHeaderRevertTest::testChannelHeaderRevert_Restore( void )
{
  ChannelHeader header;
  header.setId( 100L );
  this->db_->refresh(header);
  CPPUNIT_ASSERT( this->db_->remove(header) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->channelHeaderRepository_->revert( string( "100" ), false ) );
  
  this->db_->refresh(header);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)header.getSyncInd(), ejin::kSyncIndSynchronous );
}

