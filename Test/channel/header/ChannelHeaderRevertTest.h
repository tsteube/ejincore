/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __HEADER_REVERT_UNIT_TEST_H__
#define __HEADER_REVERT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelHeaderRevertTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelHeaderRevertTest );
  CPPUNIT_TEST( testChannelHeaderRevert_Empty );
  CPPUNIT_TEST( testChannelHeaderRevert_AddMember );
  CPPUNIT_TEST( testChannelHeaderRevert_AddMedia );
  CPPUNIT_TEST( testChannelHeaderRevert_AddPost );
  CPPUNIT_TEST( testChannelHeaderRevert_AddComment );
  CPPUNIT_TEST( testChannelHeaderRevert_Update );
  CPPUNIT_TEST( testChannelHeaderRevert_Delete );
  CPPUNIT_TEST( testChannelHeaderRevert_Restore );
  CPPUNIT_TEST_SUITE_END();
  
public:
  
  // private methods
  void setUp( void );
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testChannelHeaderRevert_Empty( void );
  void testChannelHeaderRevert_AddMember( void );
  void testChannelHeaderRevert_AddMedia( void );
  void testChannelHeaderRevert_AddPost( void );
  void testChannelHeaderRevert_AddComment( void );
  void testChannelHeaderRevert_Update( void );
  void testChannelHeaderRevert_Delete( void );
  void testChannelHeaderRevert_Restore( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
  
};

#endif // __HEADER_REVERT_UNIT_TEST_H__
