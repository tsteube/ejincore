/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_HEADER_REPOSITORY_COMMIT_UNIT_TEST_H__
#define __CHANNEL_HEADER_REPOSITORY_COMMIT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelHeaderRepositoryCommitTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelHeaderRepositoryCommitTest );  
  CPPUNIT_TEST( testCommitAddedHeader );
  CPPUNIT_TEST( testCommitAddedHeaderWithResource );
  CPPUNIT_TEST( testCommitAddedHeaderWithMember );
  CPPUNIT_TEST( testCommitUpdatedHeader );
  CPPUNIT_TEST( testCommitAddedResource );
  CPPUNIT_TEST( testCommitDeletedResource );
  CPPUNIT_TEST( testCommitAddedMember );
  CPPUNIT_TEST( testCommitUpdatedMember );
  //empty CPPUNIT_TEST( testCommitDeletedMember );
  CPPUNIT_TEST( testCommitDeletedHeader );
  CPPUNIT_TEST( testCommitDeletedHeaderWithResource );
  //empty CPPUNIT_TEST( testCommitDeletedHeaderWithMember );
  CPPUNIT_TEST( testCommitDeletedHeaderWithPost );
  CPPUNIT_TEST( testCommitDeletedHeaderWithComment );
  CPPUNIT_TEST( testModificationMask );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testCommitAddedHeader( void );
  void testCommitAddedHeaderWithResource( void );
  void testCommitAddedHeaderWithMember( void );
  void testCommitUpdatedHeader( void );
  void testCommitAddedResource( void );
  void testCommitDeletedResource( void );
  void testCommitAddedMember( void );
  void testCommitUpdatedMember( void );
  void testCommitDeletedMember( void );
  void testCommitDeletedHeader( void );
  void testCommitDeletedHeaderWithResource( void );
  void testCommitDeletedHeaderWithMember( void );
  void testCommitDeletedHeaderWithPost( void );
  void testCommitDeletedHeaderWithComment( void );
  void testModificationMask( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel/header"; }
};

#endif // __CHANNEL_HEADER_REPOSITORY_COMMIT_UNIT_TEST_H__
