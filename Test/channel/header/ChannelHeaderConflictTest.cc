/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderConflictTest.h"

#include "ChannelService.h"
#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelHeaderConflictTest );

#define INITAL_NUM_OF_CHANNELS 6

shared_ptr<ChannelHeader> ChannelHeaderConflictTest::loadByGid( const string& gid, const string& syncAnchor, SyncInd state )
{
  shared_ptr<ChannelHeader> header = this->channelHeaderRepository_->loadByGid( gid );
  if ( syncAnchor.empty() ) {
    CPPUNIT_ASSERT( header == NULL );
  } else {
    CPPUNIT_ASSERT( header != NULL );
    CPPUNIT_ASSERT_EQUAL( syncAnchor, header->getSyncAnchor() );
    CPPUNIT_ASSERT_EQUAL( (integer) state, header->getSyncInd() );
  }
  return header;
}

shared_ptr<ChannelHeader> ChannelHeaderConflictTest::validate( const string& gid, 
                                                                       const string& syncAnchor,
                                                                       SyncInd state,
                                                                       long expectedTotalNumOfChannels )
{
  shared_ptr<ChannelHeader> header = loadByGid( gid, syncAnchor, state );
  CPPUNIT_ASSERT( header != NULL );
  bool expectConflict = ( state == kSyncIndConflict );
  CPPUNIT_ASSERT( expectConflict == this->channelHeaderRepository_->detectChannelConflict( gid ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfChannels, this->db_->count( "CHANNEL_TBL" ) );
  return header;
}

// ==========
// TEST Methods
// ==========

void ChannelHeaderConflictTest::testAddChannelContent( void )
{
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader( "99" ));
  header->setName( "channel99" );  
  header->setContent( "test" );
  header->setOwner( "owner" );
  header->setSyncAnchor( string( "1" ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "99", "1", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS + 1);
}

void ChannelHeaderConflictTest::testAddChannelContent_Empty( void )
{
  shared_ptr<ChannelHeader> header = shared_ptr<ChannelHeader>(new ChannelHeader( "99" ));
  header->setName( "channel99" );  
  header->clearContent( );
  header->setOwner( "owner" );
  header->setSyncAnchor( string( "1" ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) == NULL );
}

void ChannelHeaderConflictTest::testUpdateChannelContent_LocalUnchanged( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "1", "0", kSyncIndSynchronous );
  header->setSyncAnchor( "1" );
  header->setContent( "update" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "1", "1", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS );
}

void ChannelHeaderConflictTest::testUpdateChannelContent_LocalAdd( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "2", "0", kSyncIndInsert );
  header->setSyncAnchor( "1" );
  try {
    this->channelHeaderRepository_->update( *header, true, jtime() );
  } catch (ejin::data_integrity_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}

void ChannelHeaderConflictTest::testUpdateChannelContent_LocalUpdate_EqualContent( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "3", "0", kSyncIndUpdate );
  header->setSyncAnchor( "1" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "3", "1", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS );
}

void ChannelHeaderConflictTest::testUpdateChannelContent_LocalUpdate( void )
{  
  // 1. update data
  shared_ptr<ChannelHeader> header = loadByGid( "3", "0", kSyncIndUpdate );
  string content = header->getContent( );
  header->setSyncAnchor( "1" );
  header->setContent( "update" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "3", "1", kSyncIndConflict, INITAL_NUM_OF_CHANNELS + 1 );
  CPPUNIT_ASSERT_EQUAL( header->getContent(), string("update") );

  // 2. update data
  header->setSyncAnchor( "2" );
  header->setContent( "update2" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "3", "2", kSyncIndConflict, INITAL_NUM_OF_CHANNELS + 1 );
  CPPUNIT_ASSERT_EQUAL( header->getContent(), string("update2") );

  // 2. update data
  header->setSyncAnchor( "3" );
  header->clearContent( );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "3", "3", kSyncIndConflict, INITAL_NUM_OF_CHANNELS + 1 );
  CPPUNIT_ASSERT( ! header->hasContent() );

  // resolve conflict data with original data content
  header->setSyncAnchor( "4" );
  header->setContent( content );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "3", "4", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS );
}

void ChannelHeaderConflictTest::testUpdateChannelContent_LocalDelete_EqualContent( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "4", "0", kSyncIndDelete );
  header->setSyncAnchor( "1" );
  header->clearData();
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "4", "1", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS ); // revert delete
}

void ChannelHeaderConflictTest::testUpdateChannelContent_LocalDelete( void )
{  
  // update data
  shared_ptr<ChannelHeader> header = loadByGid( "4", "0", kSyncIndDelete );
  header->setSyncAnchor( "1" );
  header->setContent( "update" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "4", "1", kSyncIndConflict, INITAL_NUM_OF_CHANNELS + 1 );
  
  // force resolve conflict
  CPPUNIT_ASSERT( this->channelHeaderRepository_->clearConflict( "4" ) );
  validate( "4", "1", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS );
}

void ChannelHeaderConflictTest::testDeleteChannelContent_LocalUnchanged( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "1" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "1" );
  channel->setSyncTime( jtime() );
  
  CPPUNIT_ASSERT( this->channelHeaderRepository_->deleteChannel( *channel, jtime() ) );
  loadByGid( "1", "", (SyncInd) 0 );
}

void ChannelHeaderConflictTest::testDeleteChannelContent_LocalAdd( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "2" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "1" );
  channel->setSyncTime( jtime() );
  
  try {
    this->channelHeaderRepository_->deleteChannel( *channel, jtime() );
  } catch (ejin::data_integrity_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}

void ChannelHeaderConflictTest::testDeleteChannelContent_LocalUpdate( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "3" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "1" );
  channel->setSyncTime( jtime() );
  
  // remove member
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->deleteChannel( *channel, jtime() ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->loadByGid( "3" ) != NULL );
  shared_ptr<ChannelHeader> header = loadByGid( "3", "1", kSyncIndConflict );
  CPPUNIT_ASSERT( ! header->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! header->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->detectChannelConflict( "3" ) );

  // force resolve conflict
  CPPUNIT_ASSERT( this->channelHeaderRepository_->clearConflict( "3" ) );
}

void ChannelHeaderConflictTest::testDeleteChannelContent_LocalDelete( void )
{
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "4" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "1" );
  channel->setSyncTime( jtime() );
  
  // remove member
  CPPUNIT_ASSERT( this->channelHeaderRepository_->deleteChannel( *channel, jtime() ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->loadByGid( "4" ) == NULL );
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->detectChannelConflict( "4" ) );
}

void ChannelHeaderConflictTest::testDeleteChannelContent_LocalDelete_Conflict( void )
{  
  shared_ptr<ChannelHeader> channel = shared_ptr<ChannelHeader>(new ChannelHeader());
  channel->setGid( "4" );
  channel->setModifiedBy( this->_TEST_CHANNEL.getOwner() );
  channel->setSyncAnchor( "1" );
  channel->setSyncTime( jtime() );
  
  // update data
  shared_ptr<ChannelHeader> header = loadByGid( "4", "0", kSyncIndDelete );
  header->setSyncAnchor( "1" );
  header->setContent( "update" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "4", "1", kSyncIndConflict, INITAL_NUM_OF_CHANNELS + 1 );
  
  // resolve conflict data with original data content to delete the channel
  CPPUNIT_ASSERT( this->channelHeaderRepository_->deleteChannel( *channel, jtime() ) );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->loadByGid( "4" ) == NULL );
  CPPUNIT_ASSERT( ! this->channelHeaderRepository_->detectChannelConflict( "4" ) );
}

void ChannelHeaderConflictTest::testForceClearConflict( void )
{
  shared_ptr<ChannelHeader> header = loadByGid( "5", "0", kSyncIndConflict );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->clearConflict( "5" ) );
  validate( "5", "0", kSyncIndSynchronous, INITAL_NUM_OF_CHANNELS - 1 );
}
