/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_SERVICE_UNIT_TEST_H__
#define __CHANNEL_SERVICE_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelServiceTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelServiceTest );  
  CPPUNIT_TEST( testExportChannel_Unchanged );  
  CPPUNIT_TEST( testExportChannel_Updated );  
  CPPUNIT_TEST( testExportChannel_Conflict );  
  CPPUNIT_TEST( testImportChannel );
  CPPUNIT_TEST( testImportChannel_Conflict );  
  CPPUNIT_TEST( testExportAllModifications );  
  CPPUNIT_TEST( testImportAllModifications );  
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testExportChannel_Unchanged( void );
  void testExportChannel_Updated( void );
  void testExportChannel_Conflict( void );
  void testImportChannel( void );
  void testImportChannel_Conflict( void );
  void testExportAllModifications( void );
  void testImportAllModifications( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
};

#endif // __CHANNEL_SERVICE_UNIT_TEST_H__
