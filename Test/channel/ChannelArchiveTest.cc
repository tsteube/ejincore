/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ChannelArchiveTest.h"

#include "ChannelService.h"
#include "ChannelMember.h"
#include "Attachment.h"
#include "Thumb.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "SqliteBaseEntity.h"
#include "SqliteResultSet.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelArchiveTest );

void ChannelArchiveTest::testAttachArchive( void )
{
  // stop global test transaction
  this->db_->transactionCommit();
  
  // ------
  // 1. First database attach
  {
    this->db_->attachArchiveDb( "test" );
    CPPUNIT_ASSERT( this->db_->isClean() );
    
    this->db_->transactionBegin();
    
    this->db_->setTargetSchema( ejin::Database::TargetSchema::MainDatabase );
    ejin::ChannelHeader channelHeader;
    channelHeader.setId( 100L );
    CPPUNIT_ASSERT( this->db_->contains( channelHeader ) );
    
    this->db_->setTargetSchema( ejin::Database::TargetSchema::BackupDatabase );
    channelHeader.setId( 100L );
    CPPUNIT_ASSERT( ! this->db_->contains( channelHeader ) );
    
    this->db_->setTargetSchema( ejin::Database::TargetSchema::ArchiveDatabase );
    channelHeader.setId( 100L );
    CPPUNIT_ASSERT( ! this->db_->contains( channelHeader ) );
    
    this->db_->transactionRollback();
    
    this->db_->detachArchiveDb( );
    CPPUNIT_ASSERT( this->db_->isClean() );
  }
  
  // ------
  // 2. First database attach
  {
    this->db_->attachArchiveDb( "test" );
    CPPUNIT_ASSERT( this->db_->isClean() );
    
    this->db_->transactionBegin();
    
    this->db_->setTargetSchema( ejin::Database::TargetSchema::MainDatabase );
    ejin::ChannelHeader channelHeader;
    channelHeader.setId( 100L );
    CPPUNIT_ASSERT( this->db_->contains( channelHeader ) );
    
    this->db_->setTargetSchema( ejin::Database::TargetSchema::BackupDatabase );
    channelHeader.setId( 100L );
    CPPUNIT_ASSERT( ! this->db_->contains( channelHeader ) );
    
    this->db_->setTargetSchema( ejin::Database::TargetSchema::ArchiveDatabase );
    channelHeader.setId( 100L );
    CPPUNIT_ASSERT( ! this->db_->contains( channelHeader ) );
    
    this->db_->transactionRollback();
    
    this->db_->detachArchiveDb( );
    CPPUNIT_ASSERT( this->db_->isClean() );
  }
  
  // global test transaction
  this->db_->transactionBegin();
}
void ChannelArchiveTest::testArchiveChannel( void )
{
  this->db_->archiveChannel( 100, "test" );
  this->db_->unarchiveChannel( 101, "test" );
  
  ejin::ChannelHeader channelHeader;
  channelHeader.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( channelHeader ) );
  this->db_->refresh( channelHeader );
  CPPUNIT_ASSERT( ejin::kSyncIndInsert == channelHeader.getSyncInd() );

  ejin::ChannelPost channelPost;
  channelPost.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( channelPost ) );
  this->db_->refresh( channelPost );
  CPPUNIT_ASSERT( ejin::kSyncIndInsert == channelPost.getSyncInd() );

  ejin::ChannelPost channelComment;
  channelComment.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( channelComment ) );
  this->db_->refresh( channelComment );
  CPPUNIT_ASSERT( ejin::kSyncIndInsert == channelComment.getSyncInd() );

  ejin::ChannelMember channelMember;
  channelMember.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( channelMember ) );
  this->db_->refresh( channelMember );
  CPPUNIT_ASSERT( ejin::kSyncIndInsert == channelMember.getSyncInd() );

  ejin::Media media;
  media.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( media ) );
  this->db_->refresh( media );
  CPPUNIT_ASSERT( ejin::kSyncIndInsert == media.getSyncInd() );

  ejin::Attachment attachment;
  attachment.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( attachment ) );
  this->db_->refresh( attachment );
  CPPUNIT_ASSERT( attachment.getDataSize() > 0 );

  ejin::Thumb thumb;
  thumb.setId( 101L );
  CPPUNIT_ASSERT( this->db_->contains( thumb ) );
  this->db_->refresh( thumb );
  CPPUNIT_ASSERT( thumb.getDataSize() > 0 );
}
void ChannelArchiveTest::testArchiveChannel_Override( void )
{
  integer totalMemberCount = this->db_->count( "MEMBER_TBL" );
  integer totalChannelCount = this->db_->count( "CHANNEL_TBL" );
  integer totalChannelMemberCount = this->db_->count( "CHANNEL_MEMBER_TBL" );
  integer totalPostCount = this->db_->count( "POST_TBL" );
  integer totalCommentCount = this->db_->count( "COMMENT_TBL" );
  integer totalMediaCount = this->db_->count( "MEDIA_TBL" );
  integer totalAttachmentCount = this->db_->count( "ATTACHMENT_TBL" );
  integer totalThumbCount = this->db_->count( "THUMB_TBL" );

  this->db_->archiveChannel( 100, "test" );
  this->db_->archiveChannel( 100, "test" );
  this->db_->unarchiveChannel( 102, "test" );

  this->db_->setTargetSchema( ejin::Database::TargetSchema::MainDatabase );
  CPPUNIT_ASSERT( totalMemberCount == this->db_->count("MEMBER_TBL") );
  CPPUNIT_ASSERT( totalChannelCount + 1 == this->db_->count("CHANNEL_TBL") );
  CPPUNIT_ASSERT( totalChannelMemberCount + 1 == this->db_->count("CHANNEL_MEMBER_TBL") );
  CPPUNIT_ASSERT( totalPostCount + 1 == this->db_->count("POST_TBL") );
  CPPUNIT_ASSERT( totalCommentCount + 1 == this->db_->count("COMMENT_TBL") );
  CPPUNIT_ASSERT( totalMediaCount + 1 == this->db_->count("MEDIA_TBL") );
  CPPUNIT_ASSERT( totalAttachmentCount + 1 == this->db_->count("ATTACHMENT_TBL") );
  CPPUNIT_ASSERT( totalThumbCount + 1 == this->db_->count("THUMB_TBL") );

  this->db_->setTargetSchema( ejin::Database::TargetSchema::ArchiveDatabase );
  CPPUNIT_ASSERT( 1 == this->db_->count("MEMBER_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("CHANNEL_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("CHANNEL_MEMBER_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("POST_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("COMMENT_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("MEDIA_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("ATTACHMENT_TBL") );
  CPPUNIT_ASSERT( 1 == this->db_->count("THUMB_TBL") );
}
