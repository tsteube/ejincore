/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_ARCHIVE_UNIT_TEST_H__
#define __CHANNEL_ARCHIVE_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelArchiveTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelArchiveTest );
  CPPUNIT_TEST( testAttachArchive );
  CPPUNIT_TEST( testArchiveChannel );
  CPPUNIT_TEST( testArchiveChannel_Override );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testAttachArchive( void );
  void testArchiveChannel( void );
  void testArchiveChannel_Override( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "channel"; }
  
};

#endif // __CHANNEL_ARCHIVE_UNIT_TEST_H__
