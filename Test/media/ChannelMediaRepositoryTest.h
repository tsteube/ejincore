/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_MEDIA_REPOSITORY_UNIT_TEST_H__
#define __CHANNEL_MEDIA_REPOSITORY_UNIT_TEST_H__

#include "AbstractMediaTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelMediaRepositoryTest : public AbstractMediaTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelMediaRepositoryTest );  
  CPPUNIT_TEST( testAddAndCommitChannelMedia );
  CPPUNIT_TEST( testUpdateAndCommitChannelMedia );
  CPPUNIT_TEST( testUpdateChannelMedia_Conflict );
  CPPUNIT_TEST( testRemoveChannelMedia );
  CPPUNIT_TEST( testRemoveChannelMedia_Conflict );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testAddAndCommitChannelMedia( void );
  void testUpdateAndCommitChannelMedia( void );
  void testUpdateChannelMedia_Conflict( void );
  void testRemoveChannelMedia( void );
  void testRemoveChannelMedia_Conflict( void );
  
protected:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "media"; }
};

#endif // __CHANNEL_MEDIA_REPOSITORY_UNIT_TEST_H__
