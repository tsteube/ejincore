/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaProfileTest.h"

#include "Media.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaProfileTest );

// ==========
// TEST Methods
// ==========

void MediaProfileTest::testModifiedMediaProfile( void )
{
  // test
  list< shared_ptr<Media> > medias(mediaService_->findUpdated());
  
  // validation
  bool result = compareLists(medias, "3", "11", "12", NULL);
  CPPUNIT_ASSERT( result );
}

void MediaProfileTest::testConflictedMediaProfile( void )
{
  // test
  list< shared_ptr<Media> > medias(mediaService_->findConflicted());
  
  // validation
  bool result = compareLists(medias, "4", NULL);
  CPPUNIT_ASSERT( result );
}

// ==========
// Helper
// ==========

bool MediaProfileTest::compareLists( list< shared_ptr<Media> >& medias, const char* gid, ... )
{
  va_list argptr;
  va_start(argptr,gid);
  
  // convert variable arguments into a list of Media objects
  set< Media > values;
  if (gid) {
    values.insert( Media( string(gid) ) );   
    const char* param = va_arg(argptr, const char*);
    while (param && *param) {
      values.insert( Media( string(param) ) );
      param = va_arg(argptr, const char*);
    }
  }

  va_end(argptr);
  
  // compare both sets
  bool isEqual(values.size() == medias.size());
  if (isEqual) {      
    list< shared_ptr<Media> >::iterator it = medias.begin();
    for (; it!=medias.end() && isEqual; it++) {
      isEqual = values.find(*(*it)) != values.end();
    }
  }
  
  return isEqual;
}

