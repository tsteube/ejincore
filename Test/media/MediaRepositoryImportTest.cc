/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaRepositoryImportTest.h"

#include "Attachment.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaRepositoryImportTest );

// ==========
// TEST Methods
// ==========

void MediaRepositoryImportTest::testImportAddMedia( void )
{
  // preparation
  Media media;
  media.setChannelGid( string( "1" ) );
  media.setGid( string( "99" ) );
  media.setNo( 0 );
  media.setName( "test" );
  media.setOwner( "owner" );
  media.setModifiedBy( "owner" );
  media.setMimeType( "text/html" );
  media.setModifyTime( jtime() );
  media.setSyncAnchor( string( "100" ) );
  media.setIV( "iv" );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->mediaService_->searchFullText("test").size());

  // ----
  // TEST
  media = *this->mediaService_->update( media, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( media.hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "99" ), media.getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), media.getIV() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, media.getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, this->mediaService_->searchFullText("test").size());
}

void MediaRepositoryImportTest::testImportUpdateMedia( void )
{
  // preparation
  Media media;
  media.setGid( string( "2" ) );
  media.setMimeType( "text/plain" );
  media.setModifyTime( jtime() );
  media.setSyncAnchor( string( "100" ) );
  media.setModifiedBy( "owner" );
  media.setIV( "iv" );

  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->mediaService_->searchFullText("test").size());

  // ----
  // TEST
  media = *this->mediaService_->update( media, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( media.hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "2" ), media.getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), media.getIV() );
  CPPUNIT_ASSERT_EQUAL( string("text/plain"), media.getMimeType() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, media.getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 0, this->mediaService_->searchFullText("test").size());
}

void MediaRepositoryImportTest::testImportUpdateMedia_Identical( void )
{
  // preparation
  Media media;
  media.setSyncInd( ejin::kSyncIndSynchronous );
  media.setGid( string( "3" ) );
  media.setChannelId( 1L );
  media.setName( "hello" );
  media.setMimeType( "text/plain" );
  media.setModifiedBy( "owner" );
  media.setModifyTime( jtime() );
  media.setSyncAnchor( string( "100" ) );
  media.setRole( ejin::kSyncRoleWrite );

  // ----
  // TEST
  media = *this->mediaService_->update( media, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( media.hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "3" ), media.getGid() );
  CPPUNIT_ASSERT_EQUAL( string("text/plain"), media.getMimeType() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, media.getSyncInd() );
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "3" ) ) );
}

void MediaRepositoryImportTest::testImportUpdateMedia_Conflict( void )
{
  // preparation
  Media media;
  media.setGid( string( "3" ) );
  media.setChannelId( 1L );
  media.setMimeType( "text/html" );
  media.setModifiedBy( "owner" );
  media.setModifyTime( jtime() );
  media.setSyncAnchor( string( "100" ) );
  
  // ----
  // TEST
  media = *this->mediaService_->update( media, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( media.hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "3" ), media.getGid() );
  CPPUNIT_ASSERT_EQUAL( string("text/html"), media.getMimeType() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndConflict, media.getSyncInd() );
  CPPUNIT_ASSERT( this->mediaService_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->mediaService_->clearConflict( string( "3" ) ) );
  
  // header condition
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "3" ) ) );
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, media.getSyncInd() );
}

void MediaRepositoryImportTest::testImportDeleteMedia( void )
{
  // preparation
  Media media;
  media.setGid( string( "2" ) );
  media.setModifiedBy( "owner" );
  media.setEmpty( true );
  
  CPPUNIT_ASSERT_EQUAL( (size_t) 8, this->mediaService_->searchFullText("hello").size());

  // ----
  // TEST
  media = *this->mediaService_->update( media, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(media) );
  
  // ----
  // TEST FULL TEXT SEARCH
  CPPUNIT_ASSERT_EQUAL( (size_t) 7, this->mediaService_->searchFullText("hello").size());
}

void MediaRepositoryImportTest::testImportDeleteMedia_Conflict(void)
{
  // preparation
  Media media;
  media.setGid( string( "3" ) );
  media.setModifiedBy( "owner" );
  media.setEmpty( true );
  
  // ----
  // TEST
  media = *this->mediaService_->update( media, jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! media.hasCreateTime( ) );
  CPPUNIT_ASSERT( ! media.hasModifyTime( ) );
  CPPUNIT_ASSERT( this->mediaService_->detectConflict( string( "3" ) ) );
  
  // ----
  // TEST force resolve conflict
  CPPUNIT_ASSERT( this->mediaService_->clearConflict( string( "3" ) ) );
  
  // header condition
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "3" ) ) );
  CPPUNIT_ASSERT( this->db_->contains(media) );
}

void MediaRepositoryImportTest::testAttachment(void)
{
  shared_ptr<Media> media(shared_ptr<Media>(new Media()));
  media->setId( 1L );
  media->setGid( string( "1" ) );
  media->setNo( 0 );
  media->setOwner( "owner" );
  media->setModifiedBy( "owner" );
  media->setTransferKey( "test" );
  media->setAttachmentOutdated( true );
  media->setHmac( "316c0a78a6243f74b3610fd8c9640d01adfac62a" );
  
  ejin::Attachment attachment(media);
  attachment.setData( "hallo", 5 );
  
  // -- TEST save ----------
  mediaService_->saveAttachment( attachment );
  CPPUNIT_ASSERT_EQUAL( false, attachment.getMedia()->isAttachmentOutdated() );

  media->clearHmac( );
  attachment = ejin::Attachment(media);
  mediaService_->loadAttachment( attachment );
  CPPUNIT_ASSERT( attachment.getData() );
  CPPUNIT_ASSERT( strncmp("hallo", attachment.getData(), 5) == 0 );
  CPPUNIT_ASSERT_EQUAL( string("316c0a78a6243f74b3610fd8c9640d01adfac62a"), attachment.getHmac() );
  
  // -- TEST move ----------
  integer targetId = 2L;
  CPPUNIT_ASSERT( mediaService_->copyAttachment( attachment.getMedia()->getId(), targetId ) );

  media->setId( targetId );
  media->clearHmac( );
  attachment = ejin::Attachment(media);
  mediaService_->loadAttachment( attachment );
  CPPUNIT_ASSERT( attachment.getData() );
  CPPUNIT_ASSERT( strncmp("hallo", attachment.getData(), 5) == 0 );
  CPPUNIT_ASSERT_EQUAL( string("316c0a78a6243f74b3610fd8c9640d01adfac62a"), attachment.getHmac() );
  
  // -- TEST delete ----------
  CPPUNIT_ASSERT( mediaService_->deleteAttachment( *media ) );
  
}
void MediaRepositoryImportTest::testMediaMetadata( void )
{
  // ----
  // TEST
  CPPUNIT_ASSERT_EQUAL( kSyncRoleWrite, this->mediaService_->getAccessRole( 3, "test" ) );
  CPPUNIT_ASSERT_EQUAL( kSyncRoleOwner, this->mediaService_->getAccessRole( 3, "owner" ) );
}
