/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_MEDIA_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __CHANNEL_MEDIA_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractChannelTestFixture.h"

/*
 * Ejin unit tests
 */
class ChannelMediaConflictTest : public AbstractChannelTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelMediaConflictTest );  
  CPPUNIT_TEST( testAddChannelMedia );
  CPPUNIT_TEST( testUpdateChannelMedia_LocalUnchanged );
  CPPUNIT_TEST( testUpdateChannelMedia_LocalAdd );
  CPPUNIT_TEST( testUpdateChannelMedia_LocalDelete );
  CPPUNIT_TEST( testUpdateChannelMedia_LocalUpdate );
  CPPUNIT_TEST( testUpdateChannelMedia_LocalUpdate_noChange );
  CPPUNIT_TEST( testDeleteChannelMedia_LocalUnchanged );
  CPPUNIT_TEST( testDeleteChannelMedia_LocalAdd );
  CPPUNIT_TEST( testDeleteChannelMedia_LocalUpdate );
  CPPUNIT_TEST( testDeleteChannelMedia_LocalDelete );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testAddChannelMedia( void );
  void testUpdateChannelMedia_LocalUnchanged( void );
  void testUpdateChannelMedia_LocalAdd( void );
  void testUpdateChannelMedia_LocalDelete( void );
  void testUpdateChannelMedia_LocalUpdate( void );
  void testUpdateChannelMedia_LocalUpdate_noChange( void );
  void testDeleteChannelMedia_LocalUnchanged( void );
  void testDeleteChannelMedia_LocalAdd( void );
  void testDeleteChannelMedia_LocalUpdate( void );
  void testDeleteChannelMedia_LocalDelete( void );
  
protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "media/conflict"; }
  shared_ptr<ejin::ChannelHeader> loadByGid( const string& gid, ejin::SyncInd state, long expectedNumOfChannelMedias );
  shared_ptr<ejin::ChannelHeader> validate( const string& gid, ejin::SyncInd state, long expectedNumOfChannelMedias, long expectedTotalNumOfChannelMedias );
};

#endif // __CHANNEL_MEDIA_RESOLVE_CONFLICT_UNIT_TEST_H__
