/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaConflictTest.h"

#include "MediaService.h"
#include "MediaRepository.h"
#include "Media.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaConflictTest );

#define INITAL_NUM_OF_MEDIAS 6

shared_ptr<Media> MediaConflictTest::loadByGid( const string& gid, const string& syncAnchor, SyncInd state, bool attachmentOutdated )
{
  shared_ptr<Media> media = this->mediaRepository_->loadByGid( gid );
  if ( syncAnchor.empty() ) {
    CPPUNIT_ASSERT( media == NULL );
  } else {
    CPPUNIT_ASSERT( media != NULL );
    CPPUNIT_ASSERT_EQUAL( syncAnchor, media->getSyncAnchor() );
    CPPUNIT_ASSERT_EQUAL( (integer) state, media->getSyncInd() );
    CPPUNIT_ASSERT_EQUAL( attachmentOutdated, media->isAttachmentOutdated() );
  }
  return media;
}

shared_ptr<Media> MediaConflictTest::validate(const string& gid, 
                                              const string& syncAnchor,
                                              SyncInd state,
                                              bool attachmentOutdated,
                                              long expectedTotalNumOfMedias )
{
  shared_ptr<Media> media = loadByGid( gid, syncAnchor, state, attachmentOutdated );
  CPPUNIT_ASSERT( media != NULL );
  bool expectConflict = ( state == kSyncIndConflict );
  CPPUNIT_ASSERT( expectConflict == this->mediaRepository_->detectConflict( gid ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfMedias, this->db_->count( "MEDIA_TBL" ) );
  return media;
}

// ==========
// TEST Methods
// ==========

void MediaConflictTest::testAddMedia( void )
{
  shared_ptr<Media> media = shared_ptr<Media>(new Media( "99" ));
  media->setChannelGid( "1" );
  media->setGid( "99" );
  media->setOwner( "owner" );
  media->setMimeType( "text/html" );
  media->setName( "media99" );
  media->setSyncAnchor( "1" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  validate( "99", "1", kSyncIndSynchronous, true, INITAL_NUM_OF_MEDIAS + 1);
}

void MediaConflictTest::testAddMedia_Empty( void )
{
  shared_ptr<Media> media = shared_ptr<Media>(new Media( "99" ));
  media->setChannelGid( "1" );
  media->setGid( "99" );
  media->setOwner( "owner" );
  media->setMimeType( "text/html" );
  media->setName( "media99" );
  media->setSyncAnchor( "1" );
  media->setEmpty( true );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) == NULL );
}

void MediaConflictTest::testUpdateMedia_LocalUnchanged( void )
{ 
  shared_ptr<Media> media = loadByGid( "1", "0", kSyncIndSynchronous, false );
  media->setSyncAnchor( "1" );
  media->setHmac( "key" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  validate( "1", "1", kSyncIndSynchronous, false, INITAL_NUM_OF_MEDIAS );
}

void MediaConflictTest::testUpdateMedia_LocalAdd( void )
{   
  shared_ptr<Media> media = loadByGid( "2", "0", kSyncIndInsert, true );
  media->setSyncAnchor( "1" );
  media->setHmac( "key1" );
  this->mediaRepository_->update( *media, jtime() );
  validate( "2", "0", kSyncIndInsert, true, INITAL_NUM_OF_MEDIAS );
}

void MediaConflictTest::testUpdateMedia_LocalUpdate_EqualContent( void )
{  
  shared_ptr<Media> media = loadByGid( "3", "0", kSyncIndUpdate, true );
  media->setSyncAnchor( "1" );
  media->setHmac( "key" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  validate( "3", "1", kSyncIndSynchronous, false, INITAL_NUM_OF_MEDIAS );
}

void MediaConflictTest::testUpdateMedia_LocalUpdate( void )
{  
  // 1. update data
  shared_ptr<Media> media = loadByGid( "3", "0", kSyncIndUpdate, true );
  string key = media->getHmac( );
  media->setSyncAnchor( "1" );
  media->setHmac( "key1" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  media = validate( "3", "1", kSyncIndConflict, true, INITAL_NUM_OF_MEDIAS + 1 );
  CPPUNIT_ASSERT_EQUAL( media->getHmac(), string("key1") );
  
  // 2. update data
  media->setSyncAnchor( "2" );
  media->setHmac( "key2" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  media = validate( "3", "2", kSyncIndConflict, true, INITAL_NUM_OF_MEDIAS + 1 );
  CPPUNIT_ASSERT_EQUAL( media->getHmac(), string("key2") );

  // 3. resolve conflict data with original data content
  media->setSyncAnchor( "4" );
  media->setHmac( key );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  validate( "3", "4", kSyncIndSynchronous, false, INITAL_NUM_OF_MEDIAS );  
}

void MediaConflictTest::testUpdateMedia_LocalDelete_EqualContent( void )
{  
  shared_ptr<Media> media = loadByGid( "4", "0", kSyncIndDelete, false );
  media->setSyncAnchor( "1" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  validate( "4", "1", kSyncIndDelete, false, INITAL_NUM_OF_MEDIAS );
}

void MediaConflictTest::testUpdateMedia_LocalDelete( void )
{  
  // update data
  shared_ptr<Media> media = loadByGid( "4", "0", kSyncIndDelete, false );
  media->setSyncAnchor( "1" );
  media->setHmac( "key1" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  media = validate( "4", "1", kSyncIndConflict, true, INITAL_NUM_OF_MEDIAS + 1 );
  
  // force resolve conflict
  CPPUNIT_ASSERT( this->mediaRepository_->clearConflict( "4" ) );
  validate( "4", "1", kSyncIndSynchronous, true, INITAL_NUM_OF_MEDIAS );
}

void MediaConflictTest::testDeleteMedia_LocalUnchanged( void )
{
  shared_ptr<Media> media = shared_ptr<Media>(new Media( "1" ));
  media->setSyncAnchor( "1" );
  media->setEmpty( true );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  loadByGid( "1", "", (SyncInd) -1, false);
}

void MediaConflictTest::testDeleteMedia_LocalAdd( void )
{
  shared_ptr<Media> media = shared_ptr<Media>(new Media( "2" ));
  media->setSyncAnchor( "1" );
  media->setEmpty( true );
  try {
    this->mediaRepository_->update( *media, jtime() );
  } catch (ejin::data_integrity_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}

void MediaConflictTest::testDeleteMedia_LocalUpdate( void )
{
  // remove member
  shared_ptr<Media> media = shared_ptr<Media>(new Media( "3" ));
  media->setSyncAnchor( "1" );
  media->setEmpty( true );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  media = loadByGid( "3", "1", kSyncIndConflict, false );
  CPPUNIT_ASSERT( ! media->hasCreateTime( ) );
  CPPUNIT_ASSERT( ! media->hasModifyTime( ) );
  CPPUNIT_ASSERT( this->mediaRepository_->detectConflict( "3" ) );

  // force resolve conflict
  CPPUNIT_ASSERT( this->mediaRepository_->clearConflict( "3" ) );
}

void MediaConflictTest::testDeleteMedia_LocalDelete( void )
{
  // remove member
  shared_ptr<Media> media = shared_ptr<Media>(new Media( "4" ));
  media->setSyncAnchor( "1" );
  media->setEmpty( true );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  CPPUNIT_ASSERT( this->mediaRepository_->loadByGid( "4" ) == NULL );
  CPPUNIT_ASSERT( ! this->mediaRepository_->detectConflict( "4" ) );
}

void MediaConflictTest::testDeleteMedia_LocalDelete_Conflict( void )
{  
  // update data
  shared_ptr<Media> media = loadByGid( "4", "0", kSyncIndDelete, false );
  media->setSyncAnchor( "1" );
  media->setHmac( "key1" );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  media = validate( "4", "1", kSyncIndConflict, true, INITAL_NUM_OF_MEDIAS + 1 );
  
  // resolve conflict data with original data content to delete the channel
  media = shared_ptr<Media>(new Media( "4" ));
  media->setSyncAnchor( "2" );
  media->setEmpty( true );
  CPPUNIT_ASSERT( this->mediaRepository_->update( *media, jtime() ) != NULL );
  CPPUNIT_ASSERT( this->mediaRepository_->loadByGid( "4" ) == NULL );
  CPPUNIT_ASSERT( ! this->mediaRepository_->detectConflict( "4" ) );
}

void MediaConflictTest::testForceClearConflict( void )
{
  shared_ptr<Media> media = loadByGid( "5", "0", kSyncIndConflict, false );
  CPPUNIT_ASSERT( this->mediaRepository_->clearConflict( "5" ) );
  validate( "5", "0", kSyncIndSynchronous, false, INITAL_NUM_OF_MEDIAS - 1 );
}

