/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaRevertTest.h"

#include "Media.h"
#include "Attachment.h"
#include "SqliteResultSet.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaRevertTest );

void MediaRevertTest::setUp( void )
{
  AbstractMediaTestFixture::setUp();
  CPPUNIT_ASSERT( this->db_->backupChannel( "100" ) );
}

// ==========
// TEST Methods
// ==========

void MediaRevertTest::testMediaRevert_Empty( void )
{
  Media media;
  media.setId( 100L );
  CPPUNIT_ASSERT( this->db_->contains(media) );
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndSynchronous );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( ! this->mediaService_->revert( "Media/100", false ) );
  
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndSynchronous );
}
void MediaRevertTest::testMediaRevert_Update( void )
{
  Media media;
  media.setId( 100L );
  this->db_->refresh(media);
  media.setName("test");
  media.setSyncInd(ejin::kSyncIndUpdate);
  CPPUNIT_ASSERT( this->db_->update(media) );
  
  Value mediaId( media.getId() );
  shared_ptr<Attachment> attachment( this->loadAttachment( mediaId, false ) );
  shared_ptr<Attachment> attachment2( shared_ptr<Attachment>(new ejin::Attachment()) );
  attachment2->setMasterId( attachment->getId() );
  attachment2->setMediaId( 100L );
  attachment2->setData("hello world", 11);
  CPPUNIT_ASSERT( this->db_->insert(*attachment2) );

  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->mediaService_->revert( "Media/100", false ) );
  
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( string(""), media.getName() );
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndConflict );
  
  mediaId = Value( (integer)101L );
  attachment = this->loadAttachment( mediaId, false );
  CPPUNIT_ASSERT( attachment );
  CPPUNIT_ASSERT( strncmp("data", attachment->getData(), 11) == 0 );
}
void MediaRevertTest::testMediaRevert_Restore( void )
{
  Media media;
  media.setId( 100L );
  this->db_->refresh(media);
  CPPUNIT_ASSERT( this->db_->remove(media) );
  
  // ------------
  // T E S T
  //
  CPPUNIT_ASSERT( this->mediaService_->revert( "Media/100", false ) );
  
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (SyncInd)media.getSyncInd(), ejin::kSyncIndSynchronous );
  
  Value mediaId( media.getId() );
  shared_ptr<Attachment> attachment( this->loadAttachment( mediaId, false ) );
  CPPUNIT_ASSERT( attachment );
  CPPUNIT_ASSERT( strncmp("data", attachment->getData(), 5) == 0 );
}
// helper

shared_ptr<Attachment> MediaRevertTest::loadAttachment( const Value& mediaId, bool hidden ) const
{
  // call entity finder
  unique_ptr<ResultSet> values;
  if ( hidden )
    values = db_->find(Attachment::TABLE_NAME, "findSlave", &mediaId, NULL);
  else
    values = db_->find(Attachment::TABLE_NAME, "findMaster", &mediaId, NULL);
  return shared_ptr<Attachment>(static_cast< Attachment* >( values->takeFirst() ) );
}

