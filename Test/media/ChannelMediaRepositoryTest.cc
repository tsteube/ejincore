/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMediaRepositoryTest.h"

#include "ChannelMedia.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelMediaRepositoryTest );

// ==========
// TEST Methods
// ==========

void ChannelMediaRepositoryTest::testAddAndCommitChannelMedia( void )
{
  // preparation
  ChannelMedia resource;
  resource.setChannelId( 1L );
  resource.setGid( string( "10" ) );
  resource.setOwner( "owner" );
  resource.setNo( 0 );
  
  // ----
  // TEST update
  ChannelMedia& resourceEntity(this->channelMediaRepository_->add( resource ));
  
  // post conditions
  CPPUNIT_ASSERT( resourceEntity.hasId() );
  CPPUNIT_ASSERT( ! resourceEntity.hasSyncAnchor() );
  CPPUNIT_ASSERT( resourceEntity.isAttachmentOutdated() );

  // ----
  // TEST commit
  this->channelMediaRepository_->commit( resourceEntity, &resource, string( "100" ), string( "owner" ), jtime() );
  CPPUNIT_ASSERT_EQUAL( string( "10" ), resourceEntity.getGid() );
  CPPUNIT_ASSERT( ! resourceEntity.hasSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string("owner"), resourceEntity.getOwner() );
}
void ChannelMediaRepositoryTest::testUpdateAndCommitChannelMedia( void )
{
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "1" ) ) );
  
  // preparation
  ChannelMedia resourceEntity;
  resourceEntity.setId( 1 );
  this->db_->refresh(resourceEntity);
  
  ChannelMedia resource;
  resource.setChannelId( 1L );
  resource.setGid( string( "1" ) );
  resource.setOwner( string( "owner" ) );
  resource.setNo( 1 );
  
  // ----
  // TEST update
  this->channelMediaRepository_->update( resourceEntity, resource );
  
  // post conditions
  CPPUNIT_ASSERT( resource.hasId() );
  CPPUNIT_ASSERT( resourceEntity.hasId() );
  CPPUNIT_ASSERT_EQUAL( string( "1" ), resourceEntity.getGid() );
  CPPUNIT_ASSERT( ! resourceEntity.hasSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, resourceEntity.getSyncInd() );
  
  // ----
  // TEST commit
  this->channelMediaRepository_->commit( resourceEntity, &resource, string( "100" ), string( "owner" ), jtime() );
  CPPUNIT_ASSERT( ! resourceEntity.hasSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string("owner"), resourceEntity.getOwner() );
}

void ChannelMediaRepositoryTest::testUpdateChannelMedia_Conflict( void )
{
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "3" ) ) );
  
  // preparation
  ChannelMedia resourceEntity;
  resourceEntity.setId( 3 );
  this->db_->refresh(resourceEntity);
  
  ChannelMedia resource;
  resource.setChannelId( 1L );
  resource.setGid( string( "3" ) );
  resource.setOwner( "member" );
  resource.setNo( 3 );
  
  // ----
  // TEST update + commit
  this->channelMediaRepository_->update( resourceEntity, resource );
  this->channelMediaRepository_->commit( resourceEntity, &resource, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! resourceEntity.hasSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndUpdate, resourceEntity.getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string("member"), resourceEntity.getOwner() );
  
  // ----------------- update again
  resource.setOwner( string( "owner" ) );
  
  // ----
  // TEST update + commit
  this->channelMediaRepository_->update( resourceEntity, resource );
  this->channelMediaRepository_->commit( resourceEntity, &resource, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! resourceEntity.hasSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndUpdate, resourceEntity.getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string("owner"), resourceEntity.getOwner() );  
}

void ChannelMediaRepositoryTest::testRemoveChannelMedia( void )
{
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "1" ) ) );
  
  // preparation
  ChannelMedia resourceEntity;
  resourceEntity.setId( 1 );
  this->db_->refresh(resourceEntity);
  
  // TEST
  CPPUNIT_ASSERT( this->channelMediaRepository_->remove( resourceEntity ) );  
  
  // post condition
  CPPUNIT_ASSERT( ! this->db_->contains(resourceEntity) );
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "1" ) ) );
}

void ChannelMediaRepositoryTest::testRemoveChannelMedia_Conflict( void )
{
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "3" ) ) );
  
  // preparation
  ChannelMedia resourceEntity;
  resourceEntity.setId( 3 );
  this->db_->refresh(resourceEntity);
  
  // TEST
  CPPUNIT_ASSERT( this->channelMediaRepository_->remove( resourceEntity ) );  
  
  // post condition
  CPPUNIT_ASSERT( this->db_->contains(resourceEntity) );
  this->db_->refresh(resourceEntity);
  CPPUNIT_ASSERT( ! resourceEntity.hasCreateTime( ) );
  CPPUNIT_ASSERT( ! resourceEntity.hasModifyTime( ) );
  CPPUNIT_ASSERT( this->mediaService_->detectConflict( string( "3" ) ) );
  
  CPPUNIT_ASSERT( this->mediaService_->clearConflict( string( "3" ) ) );
  
  CPPUNIT_ASSERT( ! this->mediaService_->detectConflict( string( "3" ) ) );
}
