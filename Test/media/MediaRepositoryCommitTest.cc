/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaRepositoryCommitTest.h"

#include "Media.h"
#include "SqliteValue.h"
#include "ChangeLog.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaRepositoryCommitTest );

// ==========
// TEST Methods
// ==========

void MediaRepositoryCommitTest::testCommitAddedMedia( void )
{
  // preparation
  Media media;
  media.setId( 11 );
  media.setGid( string( "11" ) );
  media.setIV( string( "iv" ) );
  
  jtime now;
  // ----
  // TEST
  this->mediaService_->commit( media, string( "100" ), string( "owner" ), now );
  
  // post conditions
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (integer) 11, media.getId() );
  CPPUNIT_ASSERT_EQUAL( string( "11" ), media.getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), media.getIV() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT( media.hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, media.hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.
  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, media.getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void MediaRepositoryCommitTest::testCommitUpdatedMedia( void )
{
  // preparation
  Media media;
  media.setId( 12 );
  media.setGid( string( "12" ) );
  media.setIV( string( "iv" ) );

  jtime now;
  // ----
  // TEST
  this->mediaService_->commit( media, string( "100" ), string( "owner" ), now );
  
  // post conditions
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (integer) 12, media.getId() );
  CPPUNIT_ASSERT_EQUAL( string( "12" ), media.getGid() );
  CPPUNIT_ASSERT_EQUAL( string( "100" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string( "iv" ), media.getIV() );
  CPPUNIT_ASSERT( media.hasModifyTime() );
  //CPPUNIT_ASSERT_EQUAL( now, media.hasModifyTime() );  // Note: there is a conversion loss on saving the sync date timestamp.

  CPPUNIT_ASSERT_EQUAL( (integer) ejin::kSyncIndSynchronous, media.getSyncInd() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void MediaRepositoryCommitTest::testCommitDeletedMedia( void )
{
  // preparation
  Media media;
  media.setId( 13 );
  media.setGid( string( "13" ) );
  media.setEmpty( true );
  
  // ----
  // TEST
  this->mediaService_->commit( media, string( "100" ), string( "owner" ), jtime() );
  
  // post conditions
  CPPUNIT_ASSERT( ! this->db_->contains(media) );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "100" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

