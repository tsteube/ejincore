/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaServiceTest.h"

#include "ChangeLogRepository.h"
#include "ChangeLog.h"
#include "Modifications.h"
#include "Media.h"
#include "SyncSource.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaServiceTest );

// ==========
// TEST Methods
// ==========

void MediaServiceTest::testExportResources( void )
{
  list< shared_ptr<Media> > container;
  
  // TEST
  unique_ptr<SyncSource> source(this->mediaService_->exportModifiedMedia( container ));  
  
  CPPUNIT_ASSERT( source.get() != NULL );
  CPPUNIT_ASSERT_EQUAL( string( "10" ), source->getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( (size_t) 3, container.size() );  
}

void MediaServiceTest::testImportResources( void )
{
  // preparation
  Modifications<Media> container;
  container.setSyncAnchor( string( "10" ) );
  
  // add
  shared_ptr<Media> add(shared_ptr<Media>(new Media()));
  add->setChannelId( 1L );
  add->setOwner( "owner" );
  add->setGid( string( "99" ) );
  add->setChannelGid( string( "1" ) );
  add->setMimeType( "text/html" );
  add->setModifiedBy( "owner" );
  container.getAdded().push_back( add );
  
  // update
  shared_ptr<Media> update(shared_ptr<Media>(new Media()));
  update->setGid( string( "1" ) );
  update->setMimeType( "text/html" );
  update->setModifiedBy( "owner" );
  container.getUpdated().push_back( update );
  
  // delete
  shared_ptr<Media> del(shared_ptr<Media>(new Media()));
  del->setGid( string( "2" ) );
  del->setModifiedBy( string( "owner" ) );
  container.getRemoved().push_back( del );
  
  // TEST
  CPPUNIT_ASSERT( this->mediaService_->importUpdatedMedia( container, jtime() ).empty() );
  
  // post conditions
  Media media;
  media.setId( 1 );
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (integer) kSyncIndSynchronous, media.getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "10" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string("text/html"), media.getMimeType() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  shared_ptr<ChangeLog> changeLog;
  CPPUNIT_ASSERT_EQUAL( (size_t) 3, changeLogs.size() );
  auto it = changeLogs.begin();
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
  
  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationRemove, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );

  changeLog = *it++;
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationAdd, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}

void MediaServiceTest::testImportResources_Conflict( void )
{
  // preparation
  Modifications<Media> container;
  container.setSyncAnchor( string( "10" ) );
  
  // add
  shared_ptr<Media> add(shared_ptr<Media>(new Media()));
  add->setChannelId( 3L );
  add->setOwner( "owner" );
  add->setModifiedBy( "owner" );
  add->setGid( string( "3" ) );
  add->setChannelGid( string( "1" ) );
  add->setMimeType( "text/html" );
  container.getAdded().push_back( add );
  
  // TEST
  CPPUNIT_ASSERT( ! this->mediaService_->importUpdatedMedia( container, jtime() ).empty() );  
  
  // post conditions
  Media media;
  media.setId( 3 );
  this->db_->refresh(media);
  CPPUNIT_ASSERT_EQUAL( (integer) kSyncIndConflict, media.getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( string( "10" ), media.getSyncAnchor() );
  CPPUNIT_ASSERT_EQUAL( string("text/html"), media.getMimeType() );
  
  // ----
  // TEST CHANGE LOG
  CPPUNIT_ASSERT( this->changeLogRepository_->end() );
  
  list< shared_ptr<ChangeLog> > changeLogs = this->changeLogRepository_->findBySyncAnchor( string( "10" ) );
  CPPUNIT_ASSERT_EQUAL( (size_t) 1, changeLogs.size() );
  shared_ptr<ChangeLog> changeLog( *changeLogs.begin() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogOperationUpdate, (ejin::ChangeLogOperation)changeLog->getOperation() );
  CPPUNIT_ASSERT_EQUAL( kChangeLogSourceMedia, (ejin::ChangeLogSource)changeLog->getSource() );
  CPPUNIT_ASSERT_EQUAL( string( "owner" ) , changeLog->getModifiedBy() );
  CPPUNIT_ASSERT( changeLog->hasResourceId1() );
  CPPUNIT_ASSERT( ! changeLog->hasResourceId2() );
  CPPUNIT_ASSERT( changeLog->hasResourceId3() );
}
