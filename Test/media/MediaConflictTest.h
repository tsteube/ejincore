/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEDIA_RESOLVE_CONFLICT_UNIT_TEST_H__
#define __MEDIA_RESOLVE_CONFLICT_UNIT_TEST_H__

#include "AbstractMediaTestFixture.h"

/*
 * Ejin unit tests
 */
class MediaConflictTest : public AbstractMediaTestFixture
{

  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( MediaConflictTest );
  CPPUNIT_TEST( testAddMedia );
  CPPUNIT_TEST( testAddMedia_Empty );
  CPPUNIT_TEST( testUpdateMedia_LocalUnchanged );
  CPPUNIT_TEST( testUpdateMedia_LocalAdd );
  CPPUNIT_TEST( testUpdateMedia_LocalUpdate_EqualContent );
  CPPUNIT_TEST( testUpdateMedia_LocalUpdate );
  CPPUNIT_TEST( testUpdateMedia_LocalDelete_EqualContent );
  CPPUNIT_TEST( testUpdateMedia_LocalDelete );
  CPPUNIT_TEST( testDeleteMedia_LocalUnchanged );
  CPPUNIT_TEST( testDeleteMedia_LocalAdd );
  CPPUNIT_TEST( testDeleteMedia_LocalUpdate );
  CPPUNIT_TEST( testDeleteMedia_LocalDelete );
  CPPUNIT_TEST( testDeleteMedia_LocalDelete_Conflict );
  CPPUNIT_TEST( testForceClearConflict );
  CPPUNIT_TEST_SUITE_END();

  // ==========
  // Test methods
  // ==========
public:
  
  void testAddMedia( void );
  void testAddMedia_Empty( void );
  void testUpdateMedia_LocalUnchanged( void );
  void testUpdateMedia_LocalAdd( void );
  void testUpdateMedia_LocalUpdate_EqualContent( void );
  void testUpdateMedia_LocalUpdate( void );
  void testUpdateMedia_LocalDelete_EqualContent( void );
  void testUpdateMedia_LocalDelete( void );
  void testDeleteMedia_LocalUnchanged( void );
  void testDeleteMedia_LocalAdd( void );
  void testDeleteMedia_LocalUpdate( void );
  void testDeleteMedia_LocalDelete( void );
  void testDeleteMedia_LocalDelete_Conflict( void );
  void testForceClearConflict( void );

protected:
  
  const string getDbConfigDirectory( void ) { return TestResourcePath + "media/conflict"; }
  shared_ptr<Media> loadByGid( const string& gid, const string& syncAnchor, SyncInd state, bool attachmentOutdated );
  shared_ptr<Media> validate( const string& gid, const string& syncAnchor, SyncInd state, bool attachmentOutdated, long expectedTotalNumOfMedias );
};

#endif // __MEDIA_RESOLVE_CONFLICT_UNIT_TEST_H__
