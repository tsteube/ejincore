/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEDIA_SERVICE_UNIT_TEST_H__
#define __MEDIA_SERVICE_UNIT_TEST_H__

#include "AbstractMediaTestFixture.h"

/*
 * Ejin unit tests
 */
class MediaServiceTest : public AbstractMediaTestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( MediaServiceTest );  
  CPPUNIT_TEST( testExportResources );
  CPPUNIT_TEST( testImportResources );
  CPPUNIT_TEST( testImportResources_Conflict );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testExportResources( void );
  void testImportResources( void );
  void testImportResources_Conflict( void );
  
protected:
  const string getDbConfigDirectory( void ) { return TestResourcePath + "media"; }
};

#endif // __MEDIA_SERVICE_UNIT_TEST_H__
