/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMediaConflictTest.h"

#include "ChannelService.h"
#include "ChannelHeaderRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelMedia.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChangeLogRepository.h"
#include "ChannelMemberRepository.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelMediaConflictTest );

#define INITAL_NUM_OF_CHANNEL_MEDIAS 6L

shared_ptr<ChannelHeader> ChannelMediaConflictTest::loadByGid( const string& gid, SyncInd state, long expectedNumOfChannelMedias )
{
  shared_ptr<ChannelHeader> header = this->channelHeaderRepository_->loadByGid( gid );
  CPPUNIT_ASSERT( header != NULL );
  CPPUNIT_ASSERT_EQUAL( (integer) kSyncIndSynchronous, header->getSyncInd() );
  CPPUNIT_ASSERT_EQUAL( (size_t) expectedNumOfChannelMedias, header->getResources().size() );
  if ( header->getResources().size() == 1 ) {
    shared_ptr<Resource> media = *(header->getResources().begin());
    CPPUNIT_ASSERT_EQUAL( (integer) state, media->getSyncInd() );
  }
  return header;
}

shared_ptr<ChannelHeader> ChannelMediaConflictTest::validate( const string& gid, SyncInd state,
                                                                      long expectedNumOfChannelMedias,
                                                                      long expectedTotalNumOfChannelMedias )
{
  shared_ptr<ChannelHeader> header = loadByGid( gid, state, expectedNumOfChannelMedias );
  bool expectConflict = ( state == kSyncIndConflict );
  CPPUNIT_ASSERT( expectConflict == this->channelMediaRepository_->detectConflict( gid ) );
  CPPUNIT_ASSERT_EQUAL( expectedTotalNumOfChannelMedias, this->db_->count( "MEDIA_TBL" ) );
  return header;
}

// ==========
// TEST Methods
// ==========

void ChannelMediaConflictTest::testAddChannelMedia( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "1", kSyncIndSynchronous, 1 );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia( ));
  media->setGid( "99" );
  media->setOwner( "member" );
  header->getResources().push_back( media );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "1", kSyncIndSynchronous, 2, INITAL_NUM_OF_CHANNEL_MEDIAS + 1 );
}

void ChannelMediaConflictTest::testUpdateChannelMedia_LocalUnchanged( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "1", kSyncIndSynchronous, 1 );
  shared_ptr<Resource> media = *(header->getResources().begin());
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "1", kSyncIndSynchronous, 1, INITAL_NUM_OF_CHANNEL_MEDIAS );
}

void ChannelMediaConflictTest::testUpdateChannelMedia_LocalAdd( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "2", kSyncIndInsert, 1 );
  shared_ptr<Resource> media = *(header->getResources().begin());
  try {
    this->channelHeaderRepository_->update( *header, true, jtime() );
  } catch (ejin::data_integrity_error& e) { 
    return;
  }
  CPPUNIT_FAIL("exception expected");
}

void ChannelMediaConflictTest::testUpdateChannelMedia_LocalUpdate( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "3", kSyncIndUpdate, 1 );
  shared_ptr<Resource> media = *(header->getResources().begin());
  media->setOwner( "member" );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "3", kSyncIndUpdate, 1, INITAL_NUM_OF_CHANNEL_MEDIAS );
}

void ChannelMediaConflictTest::testUpdateChannelMedia_LocalUpdate_noChange( void )
{
  shared_ptr<ChannelHeader> header = loadByGid( "3", kSyncIndUpdate, 1 );
  shared_ptr<Resource> media = *(header->getResources().begin());
  media->setNo( 12 );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  header = validate( "3", kSyncIndUpdate, 1, INITAL_NUM_OF_CHANNEL_MEDIAS );
}

void ChannelMediaConflictTest::testUpdateChannelMedia_LocalDelete( void )
{  
  // update data
  shared_ptr<ChannelHeader> header = loadByGid( "4", kSyncIndDelete, 0 );
  shared_ptr<ChannelMedia> media = shared_ptr<ChannelMedia>(new ChannelMedia( ));
  media->setGid( "4" );
  media->setOwner( "member" );
  header->getResources().push_back( media );
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "4", kSyncIndConflict, 1, INITAL_NUM_OF_CHANNEL_MEDIAS + 1 );
}

void ChannelMediaConflictTest::testDeleteChannelMedia_LocalUnchanged( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "1", kSyncIndSynchronous, 1 );
  header->getResources().clear();
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  validate( "1", kSyncIndSynchronous, 1, INITAL_NUM_OF_CHANNEL_MEDIAS  );
}

void ChannelMediaConflictTest::testDeleteChannelMedia_LocalAdd( void )
{
  shared_ptr<ChannelHeader> header = loadByGid( "2", kSyncIndInsert, 1 );
  header->getResources().clear();
  this->channelHeaderRepository_->update( *header, true, jtime() );
  validate( "1", kSyncIndSynchronous, 1, INITAL_NUM_OF_CHANNEL_MEDIAS );
}

void ChannelMediaConflictTest::testDeleteChannelMedia_LocalUpdate( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "3", kSyncIndUpdate, 1 );
  header->getResources().clear();
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  CPPUNIT_ASSERT_EQUAL( INITAL_NUM_OF_CHANNEL_MEDIAS, this->db_->count( "MEDIA_TBL" ) );
}

void ChannelMediaConflictTest::testDeleteChannelMedia_LocalDelete( void )
{  
  shared_ptr<ChannelHeader> header = loadByGid( "4", kSyncIndConflict, 0 );
  header->getResources().clear();
  CPPUNIT_ASSERT( this->channelHeaderRepository_->update( *header, true, jtime() ) != NULL );
  CPPUNIT_ASSERT( (INITAL_NUM_OF_CHANNEL_MEDIAS) == this->db_->count( "MEDIA_TBL" ) );
}

