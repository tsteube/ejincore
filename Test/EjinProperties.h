/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PROPERTIES_H__
#define __EJIN_PROPERTIES_H__

#include <string>
#include <map>

namespace ejin
{
  using std::string;
  using std::map;
  using std::ostream;
  
  /**
   * Read properties from file
   */
  class EjinProperties {
    friend ostream& operator<<( ostream& ostr, const EjinProperties& rhs );
    
    // ==========
    // Public Property Keys
    // ==========
  public:
    
    static const string PROPERTY_REST_URL_BASE;    
    static const string PROPERTY_EJIN_VERSION;
    static const string PROPERTY_EJIN_SCHEMA_VERSION;
    static const string PROPERTY_LOGIN_URL;
    static const string PROPERTY_LOGOUT_URL;
    static const string PROPERTY_ALASHAN_REALM;
    static const string PROPERTY_REST_URL_PATH_SEGMENT_MEDIA;
    static const string PROPERTY_REST_URL_PATH_SEGMENT_CHANNEL;
    static const string PROPERTY_REST_URL_PATH_SEGMENT_MEMBER;
    static const string PROPERTY_REST_URL_PATH_SEGMENT_CHANNEL_LIST;
    static const string PROPERTY_REST_URL_PATH_SEGMENT_SYNC;
    static const string PROPERTY_REST_URL_PATH_SEGMENT_COMMIT;
    static const string PROPERTY_DOWNLOAD_URL_PATH;
    static const string PROPERTY_UPLOAD_URL_PATH;
    static const string PROPERTY_EJIN_DATABASE_FOLDER;
    static const string PROPERTY_ENCRYPTED_CHAT_SESSION_LENGTH;

    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctors
    EjinProperties( void );
    EjinProperties( const char file[] );
    EjinProperties( const EjinProperties& record );
    // dtor
    ~EjinProperties( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    EjinProperties& operator=( const EjinProperties& rhs );
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    bool load( const char file[] );
    
    bool contains( const string& key ) const
    {
      map<string,string>::const_iterator it = this->_props.find(key);
      return (it != this->_props.end());
    }
    string get( const string& key ) const
    {
      map<string,string>::const_iterator it = this->_props.find(key);
      return (it != this->_props.end()) ? it->second : "";
    }
    void clear( void )
    {
      this->_props.clear();
    }
    size_t size( void )
    {
      return this->_props.size();
    }
    
    // ==========
    // Protected Interface
    // ==========
  private:
    
    map<string,string> _props;
    
    string& eval( string& str ) const;
    string& escape( string& str ) const;

  };
  
  // debugging
  ostream& operator<<( ostream& ostr, const EjinProperties& rhs );  
  
};

#endif // __EJIN_PROPERTIES_H__
