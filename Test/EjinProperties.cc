/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "EjinProperties.h"

#include <fstream>
#include <iostream>
#include <sstream>

using std::string;

inline string& trim( string& str ) {
  string whitespaces (" \t\f\v\n\r");
  size_t found;
  
  found=str.find_first_not_of(whitespaces);
  if (found!=string::npos) {
    str.erase(0, found);
  } else {
    str.clear();            // str is all whitespace
    return str;
  }
  
  found=str.find_last_not_of(whitespaces);
  if (found!=string::npos) {
    str.erase(found+1);
  }
  
  return str;
}

namespace ejin
{
  using std::ifstream;
  using std::getline;
  using std::ostringstream;
  
  string const EjinProperties::PROPERTY_REST_URL_BASE                       = "restBaseURL";
  string const EjinProperties::PROPERTY_EJIN_VERSION                        = "ejinVersion";
  string const EjinProperties::PROPERTY_EJIN_SCHEMA_VERSION                 = "ejinSchemaVersion";
  string const EjinProperties::PROPERTY_LOGIN_URL                           = "loginURL";
  string const EjinProperties::PROPERTY_LOGOUT_URL                          = "logoutURL";
  string const EjinProperties::PROPERTY_ALASHAN_REALM                       = "realm";
  string const EjinProperties::PROPERTY_REST_URL_PATH_SEGMENT_MEDIA         = "restPathSegment_media";
  string const EjinProperties::PROPERTY_REST_URL_PATH_SEGMENT_CHANNEL       = "restPathSegment_channel";
  string const EjinProperties::PROPERTY_REST_URL_PATH_SEGMENT_MEMBER        = "restPathSegment_member";
  string const EjinProperties::PROPERTY_REST_URL_PATH_SEGMENT_CHANNEL_LIST  = "restPathSegment_list";
  string const EjinProperties::PROPERTY_REST_URL_PATH_SEGMENT_SYNC          = "restPathSegment_sync";
  string const EjinProperties::PROPERTY_REST_URL_PATH_SEGMENT_COMMIT        = "restPathSegment_commit";
  string const EjinProperties::PROPERTY_DOWNLOAD_URL_PATH                   = "downloadURL";
  string const EjinProperties::PROPERTY_UPLOAD_URL_PATH                     = "uploadURL";
  string const EjinProperties::PROPERTY_EJIN_DATABASE_FOLDER                = "databaseFolder";
  string const EjinProperties::PROPERTY_ENCRYPTED_CHAT_SESSION_LENGTH       = "encryptedChatSessionLength";  

  // ==========
  // Ctor / Dtor
  // ==========
  
  EjinProperties::EjinProperties( void ): _props() 
  { 
  }
  EjinProperties::EjinProperties( const char file[] ): _props() 
  { 
    load(file);
  }
  EjinProperties::EjinProperties( const EjinProperties& prop ): _props()  
  { 
    this->operator=(prop); 
  }    
  EjinProperties::~EjinProperties( void ) { }  
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  EjinProperties& EjinProperties::operator=( const EjinProperties& rhs ) {    
    if (this != &rhs) {
      this->_props = rhs._props;
    }
    return *this;
  }
  
  // ==========
  // Public Interface
  // ==========
  
  bool EjinProperties::load( const char file[] ) {
    ifstream is(file);    
    if (!is.is_open()) return false;    
    
    while (!is.eof()) {      
      string strLine;      
      getline(is,strLine);      
      
      size_t nPos = strLine.find('=');      
      if (string::npos == nPos) continue; // no '=', invalid line;
      
      string strKey = strLine.substr(0,nPos);      
      string strVal = strLine.substr(nPos + 1, strLine.length() - nPos + 1);
      trim(strKey);
      trim(strVal);      
      escape(eval(strVal));
      
      this->_props.insert(map<string,string>::value_type(strKey,strVal));
    }
    
    return true;
  }
  
  string& EjinProperties::eval( string& str ) const {    
    string var;
    size_t pos = 0;
    size_t foundStart;
    size_t foundEnd;
    
    while (pos != string::npos) {
      foundStart=str.find('{', pos);
      if (foundStart!=string::npos) {
        foundEnd=str.find('}', foundStart+1);
        if (foundEnd!=string::npos) {
          var = str.substr(foundStart+1, foundEnd-foundStart-1);
          if (this->contains(var)) {
            // replace key by value
            str.replace(foundStart, foundEnd-foundStart+1, this->get(var));
            pos = foundStart;
          } else {
            pos = foundEnd + 1;
          }
          continue;
        }
      }
      break;
    }    
    
    return str;
  }
  
  string& EjinProperties::escape( string& str ) const {
    string var;
    size_t pos = 0;
    size_t found;
    
    while (pos != string::npos) {
      found=str.find('\\', pos);
      if (found!=string::npos) {
        switch (str[found+1]) {
          case '=':
          case '#':
          case '!':
          case ':':
            str.erase(found, 1);
            break;
        }
        pos = found;
        continue;
      }
      break;
    }
    
    return str;
  }
  
  // ==========
  // Debugging
  // ==========
  
  ostream& operator<<( ostream& ostr, const EjinProperties& rhs ) {
    ostringstream buf;
    buf.flags(ostr.flags());
    buf.fill(ostr.fill());
    buf << "Properties[" << "size=" << rhs._props.size() << ",values={";
    // use const_iterator to walk through elements of pairs
    for ( std::map< string, string >::const_iterator iter = rhs._props.begin();
         iter != rhs._props.end(); ++iter ) {
      buf << iter->first << '=' << iter->second << ',';
    }
    buf <<  "}]";    
    ostr << buf.str();
    return ostr;
  }  
  
}
