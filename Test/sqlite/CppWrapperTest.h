/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __UNIT_SQL_H__
#define __UNIT_SQL_H__

#include "UsingEjinTypes.h"

#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/extensions/HelperMacros.h>

// forward declaration
namespace ejin {
  class Database;
}

/*
 * SQL unit tests
 */
class CppWrapperTest : public CPPUNIT_NS::TestFixture
{
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( CppWrapperTest );  
  CPPUNIT_TEST( testEjinDbConfig );
  CPPUNIT_TEST( testEjinDbRekey );
  CPPUNIT_TEST( testSqlValue );
  CPPUNIT_TEST( testAttachment );
  CPPUNIT_TEST( testAttachmentArchive );
  CPPUNIT_TEST( testCRUD );
  CPPUNIT_TEST( testCRUDArchive );
  CPPUNIT_TEST( testDBUnit );
  CPPUNIT_TEST( testProperties );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test setup
  // ==========
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Test methods
  // ==========
  void testAttachment( void );
  void testAttachmentArchive( void );
  void testCRUD( void );
  void testCRUDArchive( void );
  void testSqlValue( void );
  void testDBUnit( void );
  void testProperties( void );
  void testEjinDbRekey( void );
  void testEjinDbConfig( void );

  // ==========
  // Private Interface
  // ==========
private:
  
  ejin::Database* db_;
  
};

#endif // __UNIT_SQL_H__
