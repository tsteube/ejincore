/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "CppWrapperTest.h"

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <xlocale.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <list>
#include <copyfile.h>

#include "EjinProperties.h"
#include "SqliteValue.h"
#include "SqliteResultSet.h"
#include "DBUnit.h"
#include "Member.h"
#include "Attachment.h"

#include "Chat.h"
#include "Message.h"

CPPUNIT_TEST_SUITE_REGISTRATION( CppWrapperTest );

void CppWrapperTest::setUp( void )
{
  // open test db
  const string mainDb = TestResourcePath + __EJIN_TEST_MAIN_DB_NAME;
  const string attachmentDb = TestResourcePath + __EJIN_TEST_ATTACHMENT_DB_NAME;
  const string testMainDb = TestResourcePath + "ejin(test).db";
  const string testAttachmentDb = TestResourcePath + "attachment(test).db";
  this->db_ = new ejin::Database("test");
  if ( this->db_->open(__EJIN_SCHEMA_VERSION,
                       mainDb.c_str(), testMainDb.c_str(),
                       attachmentDb.c_str(), testAttachmentDb.c_str(),
                       "test.key", "private.key",
                       "tes't", true ) == ejin::Database::Status::DatabaseOpen ) {
    this->db_->attachBackupDb( "tes't" );
    this->db_->attachArchiveDb( "tes't", true );
    // start transaction
    this->db_->transactionBegin();
  }
}

void CppWrapperTest::tearDown( void )
{
  if (db_) {
    if (db_->isClean()) {
      // commit transaction
      this->db_->transactionCommit();
    } else {
      // rollback transaction
      this->db_->transactionRollback();
    }
    this->db_->detachBackupDb( );
    this->db_->detachArchiveDb( );
    delete this->db_;
  }
}

void CppWrapperTest::testAttachment( void )
{
  // removeAll old one first
  this->db_->removeAll("ATTACHMENT_TBL");
  
  ejin::Attachment attachment;
  attachment.setMediaId( 1L );
  attachment.setData("hallo", 5);
  this->db_->insert(attachment);
  CPPUNIT_ASSERT_EQUAL( 1, (int) attachment.getId() );

  attachment.clearData();
  this->db_->refresh(attachment);
  CPPUNIT_ASSERT( strncmp("hallo", attachment.getData(), 5) == 0 );

  // update
  attachment.setData("a\0\0b",4);
  this->db_->update(attachment);
  
  attachment.clearData();
  this->db_->refresh(attachment);
  CPPUNIT_ASSERT( strncmp("a\0\0b", attachment.getData(), 4) == 0 );

  // remove
  this->db_->remove(attachment);
  
  // removeAll
  CPPUNIT_ASSERT_EQUAL( 0L, this->db_->removeAll("ATTACHMENT_TBL") );
}
void CppWrapperTest::testAttachmentArchive( void )
{
  // switch database
  this->db_->setTargetSchema( ejin::Database::TargetSchema::ArchiveDatabase );
  testAttachment();
}
void CppWrapperTest::testCRUD( void )
{
  //bornander::debug::list_segment x = all;
  
  // removeAll old one first
  this->db_->removeAll("MEMBER_TBL"); 
  
  ejin::Member test;
  
  // create with id
  test.setId((integer) 1);
  test.setUsername("hallo1");
  test.setSyncInd( ejin::kSyncIndSynchronous );
  this->db_->insert(test);
  CPPUNIT_ASSERT_EQUAL( 1, (int) test.getId() );
  
  // create without id
  ejin::Member test2;
  test2.setUsername("hallo2");
  test2.setSyncInd( ejin::kSyncIndSynchronous );
  this->db_->insert(test2);
  CPPUNIT_ASSERT( test2.getId() > 0 );
  
  /*
   Zeichen	Unicode	Unicode binär	UTF-8 binär	UTF-8 hexadezimal
   Ä	U+00C4	00000000 11000100	11000011 10000100	 %C3%84
   Ö	U+00D6	00000000 11010110	11000011 10010110	 %C3%96
   Ü	U+00DC	00000000 11011100	11000011 10011100	 %C3%9C
   ä	U+00E4	00000000 11100100	11000011 10100100	 %C3%A4
   ö	U+00F6	00000000 11110110	11000011 10110110	 %C3%B6
   ü	U+00FC	00000000 11111100	11000011 10111100	 %C3%BC   
   */
  
  /*
   std::cout << "narrow: " << std::string("\uFF0E").length() <<
   " utf8: " << std::string("\xEF\xBC\x8E").length() <<
   " wide: " << std::wstring(L"\uFF0E").length() << std::endl;
   
   std::cout << "narrow: " << std::string("\U00010102").length() <<
   " utf8: " << std::string("\xF0\x90\x84\x82").length() <<
   " wide: " << std::wstring(L"\U00010102").length() << std::endl;
   */
  
  char umlaut[] = {static_cast<char>(0xC3),static_cast<char>(0x84),0x0};
  
  // update
  test.setUsername(umlaut);
  this->db_->update(test);

  // handle binary data
  test.setEmail("email");
  this->db_->update(test);
  this->db_->refresh(test);
  CPPUNIT_ASSERT_EQUAL( string("email"), test.getEmail() );

  // search
  Value v = umlaut;
  unique_ptr<ResultSet> values = this->db_->find("MEMBER_TBL", "findMemberByUsername", &v, new Value(false), NULL);
  CPPUNIT_ASSERT_EQUAL( 1, (int) values->size() );
  
  // count
  CPPUNIT_ASSERT_EQUAL( 2L, this->db_->count("MEMBER_TBL") );
  
  // refresh
  test.setUsername("");
  this->db_->refresh(test);
  
  // remove
  this->db_->remove(test);
  this->db_->remove(test2);
  
  // removeAll
  CPPUNIT_ASSERT_EQUAL( 0L, this->db_->removeAll("MEMBER_TBL") ); 
}
void CppWrapperTest::testCRUDArchive( void )
{
  // switch database
  this->db_->setTargetSchema( ejin::Database::TargetSchema::ArchiveDatabase );
  testCRUD();
}
void CppWrapperTest::testDBUnit( void )
{
  DBUnit test(*this->db_);
  
  // remove
  this->db_->removeAll("MEMBER_TBL");
  
  const BaseEntity& prototype = this->db_->prototype("MEMBER_TBL");
  
  list<const Attribute*> header = test.insertEntity(prototype, "ID,USERNAME,PUB_KEY,MASTER_ID,SYNC_IND,CREATE_TIME");
  test.insertEntity(prototype, header, "1000,user1,key,null,2,2006-03-29T16:49:20+09:00");
  test.insertEntity(prototype, header, "1001,user2,key,null,2,2006-03-29T16:49:20Z");
  
  // remove
  //CPPUNIT_ASSERT_EQUAL( 2, this->db_->removeAll("MEMBER_TBL") );
}
void CppWrapperTest::testSqlValue( void )
{
  
  locale_t locale = newlocale(LC_ALL_MASK, "en_GB.UTF-8", NULL);
  
  // NULL value
  sqlite::Value v; 
  CPPUNIT_ASSERT( v == sqlite::Value() );
  CPPUNIT_ASSERT( type_text == v.type() );
  CPPUNIT_ASSERT_EQUAL( true, v.isNull() );
  CPPUNIT_ASSERT_EQUAL( (integer) 0, v.num() );
  CPPUNIT_ASSERT_EQUAL( false, v.bol() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.0, v.dbl(), 0.0 );
  CPPUNIT_ASSERT( jtime((time_t)0) == v.date() );
  
  // Double value
  v = 9.9;
  CPPUNIT_ASSERT( v == sqlite::Value(9.9) );
  CPPUNIT_ASSERT( type_float == v.type() );
  CPPUNIT_ASSERT_EQUAL( false, v.isNull() );
  CPPUNIT_ASSERT( strncmp("9.900", v.str(), v.size()) == 0 );
  CPPUNIT_ASSERT_EQUAL( (integer) 9, v.num() );
  CPPUNIT_ASSERT_EQUAL( true, v.bol() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 9.9, v.dbl(), 0.0 );
  CPPUNIT_ASSERT( jtime((double)9.9) == v.date() );
  
  // Long value
  v = (integer) 7000;
  CPPUNIT_ASSERT( v == sqlite::Value((integer)7000) );
  CPPUNIT_ASSERT( type_int == v.type() );
  CPPUNIT_ASSERT_EQUAL( false, v.isNull() );
  CPPUNIT_ASSERT( strncmp("7000", v.str(), v.size()) == 0 );
  CPPUNIT_ASSERT_EQUAL( (integer) 7000, v.num() );
  CPPUNIT_ASSERT_EQUAL( true, v.bol() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 7000., v.dbl(), 0.0 );
  CPPUNIT_ASSERT( jtime((integer)7000) == v.date() );
  CPPUNIT_ASSERT( jtime((time_t)7) == v.date() );
  
  // char* value
  v = "5.5";
  CPPUNIT_ASSERT( v == sqlite::Value("5.5") );
  CPPUNIT_ASSERT( type_text == v.type() );
  CPPUNIT_ASSERT_EQUAL( false, v.isNull() );
  CPPUNIT_ASSERT( strncmp("5.5", v.str(), v.size()) == 0 );
  CPPUNIT_ASSERT_EQUAL( (integer) 5, v.num() );
  CPPUNIT_ASSERT_EQUAL( true, v.bol() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 5.5, v.dbl(), 0.0 );
  CPPUNIT_ASSERT( jtime((time_t)5) == v.date() );
  
  // wchar_t* value
  v = "3.5";
  CPPUNIT_ASSERT( v == sqlite::Value("3.5") );
  CPPUNIT_ASSERT( type_text == v.type() );
  CPPUNIT_ASSERT_EQUAL( false, v.isNull() );
  CPPUNIT_ASSERT( strncmp("3.5", v.str(), v.size()) == 0 );
  CPPUNIT_ASSERT_EQUAL( (integer) 3, v.num() );
  CPPUNIT_ASSERT_EQUAL( true, v.bol() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 3.5, v.dbl(), 0.0 );
  CPPUNIT_ASSERT( jtime((time_t)3) == v.date() );
  
  // bytea value
  v = Value( "a\0\0b", 4 );
  CPPUNIT_ASSERT( v == sqlite::Value("a\0\0b", 4) );
  CPPUNIT_ASSERT( type_bytea == v.type() );
  CPPUNIT_ASSERT_EQUAL( false, v.isNull() );

  // time value
  jtime now = jtime::now();
  v = now; // now
  CPPUNIT_ASSERT( v == sqlite::Value(now) );
  CPPUNIT_ASSERT( type_time == v.type() );
  CPPUNIT_ASSERT_EQUAL( false, v.isNull() );
  CPPUNIT_ASSERT_EQUAL( now.integer(), v.num() );
  CPPUNIT_ASSERT_EQUAL( true, v.bol() );
  
  // ISO8601 parsing
  jtime tm(jtime("2006-03-29T16:49:20000+09:00"));
  CPPUNIT_ASSERT_EQUAL( string("2006-03-29T09:49:20+02:00"), tm.asISO8601() );

  // free locale reference
  freelocale(locale);
}
void CppWrapperTest::testEjinDbRekey( void )
{
  // end open test transaction
  this->db_->transactionRollback();
  
  CPPUNIT_ASSERT( db_->reopen("tes't") == ejin::Database::Status::DatabaseOpen );
  CPPUNIT_ASSERT( db_->resetPassword("tes't", "test'2") );
  CPPUNIT_ASSERT( db_->reopen("test'2") == ejin::Database::Status::DatabaseOpen);
  CPPUNIT_ASSERT( db_->checkConstency( ) );
  
  // clear db to avoid commit
  delete this->db_;
  this->db_ = NULL;
}
void CppWrapperTest::testEjinDbConfig( void )
{
  // end open test transaction
  this->db_->transactionRollback();
  
  CPPUNIT_ASSERT( db_->reopen("tes't") == ejin::Database::Status::DatabaseOpen );
  CPPUNIT_ASSERT( db_->attachBackupDb("tes't") == ejin::Database::Status::DatabaseOpen );
  CPPUNIT_ASSERT( db_->clearAll() );
  CPPUNIT_ASSERT( db_->clearAllBak() );
  CPPUNIT_ASSERT( db_->checkConstency( ) );

  // clear db to avoid commit
  delete this->db_;
  this->db_ = NULL;
}
void CppWrapperTest::testProperties( void )
{
  ejin::EjinProperties props;
  props.load( (TestResourcePath + "ejin.properties").c_str() );
  
  CPPUNIT_ASSERT_EQUAL( (size_t)17, props.size() );
  CPPUNIT_ASSERT( props.get(ejin::EjinProperties::PROPERTY_LOGIN_URL).find( "http:" ) != string::npos );
}


