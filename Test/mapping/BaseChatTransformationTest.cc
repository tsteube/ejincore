/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "BaseChatTransformationTest.h"

#include "Chat.h"
#include "Message.h"

using ejin::Chat;
using ejin::Message;

shared_ptr<Chat> BaseChatTransformationTest::createChat( void )
{
  shared_ptr<Chat> chat(shared_ptr<Chat>(new Chat()));
  chat->setPartner( "member" );
  chat->setPartnerKey( "__________________________key2" );
  chat->setKey( "__________________________key1" );
  return chat;
}

shared_ptr<Message> BaseChatTransformationTest::sendMessage( void )
{
  shared_ptr<Message> message(shared_ptr<Message>(new Message()));
  message->setEncrypted( true );
  message->setContent( "message" );
  return message;
}

shared_ptr<Message> BaseChatTransformationTest::receiveMessage( void )
{
  shared_ptr<Message> message(shared_ptr<Message>(new Message()));
  message->setNo( 1 );
  message->setSender( "owner" );
  message->setSendTime( jtime("2010-01-01T00:00:00.000+01:00") );
  message->setEncrypted( true );
  message->setSecure( true );
  message->setContent( "message" );
  return message;
}

// list of local modified media instances
list< shared_ptr<Message> > BaseChatTransformationTest::remoteUpdatedMessages( void )
{
  list< shared_ptr<Message> > messages;

  shared_ptr<Message> message(shared_ptr<Message>(new Message()));
  message->setNo( 1 );
  message->setSender( "owner" );
  message->setSendTime( jtime("2010-01-01T00:00:00.000+01:00") );
  message->setEncrypted( true );
  message->setSecure( true );
  message->setContent( "message1" );
  messages.push_back( message );
  
  message = shared_ptr<Message>(new Message());
  message->setNo( 2 );
  message->setSendTime( jtime("2010-01-01T00:00:00.000+01:00") );
  message->setEncrypted( true );
  message->setSecure( true );
  message->setContent( "message2" );
  messages.push_back( message );
  
  return messages;
}

// sample container of remote modifications.
Modifications<Chat> BaseChatTransformationTest::remoteModifiedChats( void )
{
  Modifications<Chat> container;
  container.setSyncAnchor( string( "10" ) );
  container.setSyncTime( jtime("2010-01-01T00:00:00.000+01:00") );
  
  // --
  // member added
  shared_ptr<Chat> chat(shared_ptr<Chat>(new Chat()));
  chat->setGid( "1" );
  chat->setMessageCount( 0 );
  chat->setPartner( "member1" );
  chat->setAcknowledged( 0 );
  chat->setPartnerAcknowledged( 0 );
  chat->setHref( "message/1/0" );
  chat->setKey( "__________________________key1" );
  chat->setPartnerKey( "__________________________key2" );
  container.getAdded().push_back( chat );

  chat = shared_ptr<Chat>(new Chat());
  chat->setGid( "2" );
  chat->setMessageCount( 0 );
  chat->setPartner( "member2" );
  chat->setAcknowledged( 0 );
  chat->setPartnerAcknowledged( 0 );
  chat->setHref( "message/2/0" );
  container.getAdded().push_back( chat );
  
  // --
  // member updated
  chat = shared_ptr<Chat>(new Chat());
  chat->setGid( "3" );
  chat->setMessageCount( 10 );
  chat->setAcknowledged( 5 );
  chat->setPartnerAcknowledged( 5 );
  chat->setHref( "message/3/5" );
  container.getUpdated().push_back( chat );
  
  // --
  // member removed
  chat = shared_ptr<Chat>(new Chat());
  chat->setGid( "4" );
  container.getRemoved().push_back( chat );
  
  return container;
}
Chat BaseChatTransformationTest::sampleChat( void )
{
  Chat chat( "member1" );
  
  shared_ptr<Message> message(shared_ptr<Message>( new Message() ));
  chat.getMessages(); //.push_back2( message );
  
  return chat;
}
