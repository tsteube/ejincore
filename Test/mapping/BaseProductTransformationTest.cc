/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "BaseProductTransformationTest.h"

#include "User.h"
#include "ApprovedMember.h"
#include "Product.h"

using ejin::Product;
using ejin::User;

shared_ptr<User> BaseProductTransformationTest::receiveUpdatedUser( void )
{
  shared_ptr<Member> member( shared_ptr<Member>(new Member()) );
  member->setPubKey( "aGFsbG8y" );
  member->setEmail( "member2@foo.de" );
  member->setMaxChannelCount( 10 );
  member->setMaxAttachmentCount( 30 );
  member->setMaxTotalAttachmentSize( 311009280 );
  member->setMaxAttachmentSize( 103669760 );
  member->setMaxTextContentLength( 4000 );
  member->setUsername( "member2" );
  member->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  member->setPubKeyCreateTime( jtime("2010-01-01T00:00:00.000+01:00") );

  shared_ptr<ApprovedMember> approved( shared_ptr<ApprovedMember>(new ApprovedMember()) );
  approved->setUsername( "member1" );
  approved->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );

  member->getApprovedMembers().push_back( approved );
  
  return shared_ptr<User>(new User( "owner", member, NULL ));
}

list< shared_ptr<Product> > BaseProductTransformationTest::allProducts( void )
{
  list< shared_ptr<Product> > products;

  shared_ptr<Product> product(shared_ptr<Product>(new Product()));
  product->setIdentifier( "product1" );
  product->setName( "product1" );
  product->setMaxChannelCount( 10 );
  product->setMaxAttachmentCount( 30 );
  product->setMaxTotalAttachmentSize( 311009280 );
  product->setMaxAttachmentSize( 103669760 );
  product->setMaxTextContentLength( 4000 );
  product->setSubscriptionPeriod( 12 );
  product->setSubscriptionPeriodUnit( ejin::year );
  products.push_back( product );

  product = shared_ptr<Product>(new Product());
  product->setIdentifier( "product2" );
  product->setName( "product2" );
  product->setMaxChannelCount( 20 );
  product->setMaxAttachmentCount( 60 );
  product->setMaxTotalAttachmentSize( 622018560 );
  product->setMaxAttachmentSize( 207339520 );
  product->setMaxTextContentLength( 4000 );
  product->setSubscriptionPeriod( 12 );
  product->setSubscriptionPeriodUnit( ejin::year );
  products.push_back( product );
  
  return products;
}
