/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __BASE_CHANNEL_TRANSFORMATION_UNIT_TEST_H__
#define __BASE_CHANNEL_TRANSFORMATION_UNIT_TEST_H__

#include "AbstractMappingTestFixture.h"

/*
 * Ejin unit tests
 */
class BaseChannelTransformationTest : public AbstractMappingTestFixture
{
  
protected:
  
  Channel submitLocalChannel( void );
  Channel updatedLocalChannel( void );
  Channel updatedRemoteChannel( void );
  Channel sampleChannel( bool full );
  Modifications<ChannelHeader> remoteModifiedChannelHeaders( void );
  
};

#endif // __BASE_CHANNEL_TRANSFORMATION_UNIT_TEST_H__
