/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __ABSTRACT_MAPPING_TEST_FIXTURE_H__
#define __ABSTRACT_MAPPING_TEST_FIXTURE_H__

#pragma GCC diagnostic ignored "-Weffc++"
#include <cppunit/extensions/HelperMacros.h>

#include "UsingEjinTypes.h"
#include "Modifications.h"
#include "XmlMarshaller.h"
#include "DBUnit.h"

char* loadFile( const string& filename, char* dataPtr, size_t* size );

/*
 * abstract test setup for media unit tests
 */
class AbstractMappingTestFixture : public CPPUNIT_NS::TestFixture
{
  
  // ==========
  // Protected Interface
  // ==========
protected:
  
  template<typename T>
  static void assertEquals( const Modifications<T>& ref, const Modifications<T>& dat );
  static void assertEquals( const ChannelHeader& ref, const ChannelHeader& dat );
  static void assertEquals( const ChannelPost& ref, const ChannelPost& dat );
  static void assertEquals( const ChannelComment& ref, const ChannelComment& dat );
  static void assertEquals( const Channel& ref, const Channel& dat );
  static void assertEquals( const User& ref, const User& dat );
  static void assertEquals( const BaseEntity& ref, const BaseEntity& dat );
  
  std::string loadIntoStringBuffer( const string& filename );
  
};

#endif // __ABSTRACT_MEDIA_TEST_FIXTURE_H__
