/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "AbstractMappingTestFixture.h"

#include <iostream>
#include <sstream>

#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "User.h"
#include "Member.h"
#include "Picture.h"
#include "Media.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "Modifications.h"
#include "SerializableEntity.h"
#include "SqliteValue.h"

char* loadFile( const string& filename, char* dataPtr, size_t* size )
{
  // read data from file into the buffer
  std::ifstream is;
  is.open (filename, std::ios::in | std::ios::binary );
  if ( (is.rdstate() & std::ifstream::failbit ) != 0 ) {
    ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "Error opening file %s", filename.c_str());
  }
  
  // get length of file:
  is.seekg (0, std::ios::end);
  size_t length = (long) is.tellg();
  is.seekg (0, std::ios::beg);
  
  char* ptr = (char*)realloc (dataPtr, length * sizeof(char));
  if (ptr) {
    // read data as a block:
    is.read (ptr, length);
    is.close();
    *size = length;
    return ptr;
  } else {
    perror("no more memory");
    return NULL;
  }
}

// ==========
// Validate Domain Objects
// ==========

template<typename T>
void AbstractMappingTestFixture::assertEquals( const Modifications<T>& ref, const Modifications<T>& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
  
  // added
  size_t size = ref.getAdded().size();
  CPPUNIT_ASSERT_EQUAL( ref.getAdded().size(), dat.getAdded().size() );
  typename list< shared_ptr< T > >::const_iterator refIt = ref.getAdded().begin();
  typename list< shared_ptr< T > >::const_iterator datIt = dat.getAdded().begin();
  for (; size > 0; size--) {
    assertEquals(**refIt, **datIt);
    refIt++;
    datIt++;
  }    
  
  // updated
  size = ref.getUpdated().size();
  CPPUNIT_ASSERT_EQUAL( ref.getUpdated().size(), dat.getUpdated().size() );
  refIt = ref.getUpdated().begin();
  datIt = dat.getUpdated().begin();
  for (; size > 0; size--) {
    assertEquals(**refIt, **datIt);
    refIt++;
    datIt++;
  }    
  
  // removed
  size = ref.getRemoved().size();
  CPPUNIT_ASSERT_EQUAL( ref.getRemoved().size(), dat.getRemoved().size() );
  refIt = ref.getRemoved().begin();
  datIt = dat.getRemoved().begin();
  for (; size > 0; size--) {
    assertEquals(**refIt, **datIt);
    refIt++;
    datIt++;
  }    
  
}

void AbstractMappingTestFixture::assertEquals( const Channel& ref, const Channel& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
  
  Channel& ref_(const_cast<Channel&>(ref));
  Channel& dat_(const_cast<Channel&>(dat));  
  
  // header
  shared_ptr<ChannelHeader> refHeader(ref.getHeader());
  shared_ptr<ChannelHeader> datHeader(dat.getHeader());
  if ( refHeader.get() != NULL && datHeader.get() != NULL )
    assertEquals(*refHeader, *datHeader);
  
  // channel posts
  CPPUNIT_ASSERT_EQUAL( ref_.getPosts().size(), dat_.getPosts().size() );
  {
    size_t size = ref_.getPosts().size();
    list< shared_ptr<ChannelPost> >::const_iterator refIt = ref_.getPosts().begin();
    list< shared_ptr<ChannelPost> >::const_iterator datIt = dat_.getPosts().begin();  
    for (; size > 0; size--) {
      assertEquals(**refIt, **datIt);
      refIt++;
      datIt++;
    }    
  }
  
  // channel comments
  CPPUNIT_ASSERT_EQUAL( ref_.getComments().size(), dat_.getComments().size() );
  {
    size_t size = ref_.getPosts().size();
    list< shared_ptr<ChannelComment> >::const_iterator refIt = ref_.getComments().begin();
    list< shared_ptr<ChannelComment> >::const_iterator datIt = dat_.getComments().begin();  
    for (; size > 0; size--) {
      assertEquals(**refIt, **datIt);
      refIt++;
      datIt++;
    }    
  }
  
}

void AbstractMappingTestFixture::assertEquals( const User& ref, const User& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
  
  // member
  shared_ptr<Member> refMember(ref.getMember());
  shared_ptr<Member> datMember(dat.getMember());
  if ( refMember.get() != NULL && datMember.get() != NULL )
    assertEquals(*refMember, *datMember);

  // picture
  shared_ptr<Picture> refPicture(ref.getPicture());
  shared_ptr<Picture> datPicture(dat.getPicture());
  if ( refPicture.get() != NULL && datPicture.get() != NULL )
    assertEquals(*refPicture, *datPicture);
}

void AbstractMappingTestFixture::assertEquals( const ChannelHeader& ref, const ChannelHeader& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
  
  ChannelHeader& ref_(const_cast<ChannelHeader&>(ref));
  ChannelHeader& dat_(const_cast<ChannelHeader&>(dat));
  
  // members
  CPPUNIT_ASSERT_EQUAL( ref_.getMembers().size(), dat_.getMembers().size() );
  {
    size_t size = ref_.getMembers().size();
    list< shared_ptr<ChannelMember> >::const_iterator refIt = ref_.getMembers().begin();
    list< shared_ptr<ChannelMember> >::const_iterator datIt = dat_.getMembers().begin();  
    for (; size > 0; size--) {
      assertEquals(**refIt, **datIt);
      refIt++;
      datIt++;
    }    
  }
  
  // resources
  CPPUNIT_ASSERT_EQUAL( ref_.getResources().size(), dat_.getResources().size() );
  {
    size_t size = ref_.getResources().size();
    list< shared_ptr<Resource> >::const_iterator refIt = ref_.getResources().begin();
    list< shared_ptr<Resource> >::const_iterator datIt = dat_.getResources().begin();
    for (; size > 0; size--) {
      assertEquals(**refIt, **datIt);
      refIt++;
      datIt++;
    }
  }
}
void AbstractMappingTestFixture::assertEquals( const ChannelPost& ref, const ChannelPost& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
  
  ChannelPost& ref_(const_cast<ChannelPost&>(ref));
  ChannelPost& dat_(const_cast<ChannelPost&>(dat));
  
  // resources
  CPPUNIT_ASSERT_EQUAL( ref_.getResources().size(), dat_.getResources().size() );
  {
    size_t size = ref_.getResources().size();
    list< shared_ptr<Resource> >::const_iterator refIt = ref_.getResources().begin();
    list< shared_ptr<Resource> >::const_iterator datIt = dat_.getResources().begin();
    for (; size > 0; size--) {
      assertEquals(**refIt, **datIt);
      refIt++;
      datIt++;
    }
  }
}
void AbstractMappingTestFixture::assertEquals( const ChannelComment& ref, const ChannelComment& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
  
  ChannelComment& ref_(const_cast<ChannelComment&>(ref));
  ChannelComment& dat_(const_cast<ChannelComment&>(dat));
  
  // resources
  CPPUNIT_ASSERT_EQUAL( ref_.getResources().size(), dat_.getResources().size() );
  {
    size_t size = ref_.getResources().size();
    list< shared_ptr<Resource> >::const_iterator refIt = ref_.getResources().begin();
    list< shared_ptr<Resource> >::const_iterator datIt = dat_.getResources().begin();  
    for (; size > 0; size--) {
      assertEquals(**refIt, **datIt);
      refIt++;
      datIt++;
    }
  }
}
void AbstractMappingTestFixture::assertEquals( const BaseEntity& ref, const BaseEntity& dat )
{
  CPPUNIT_ASSERT_EQUAL( ref.toString(), dat.toString() );
}

// template instantiation
template void AbstractMappingTestFixture::assertEquals( const Modifications<ChannelHeader>&, const Modifications<ChannelHeader>&  );
template void AbstractMappingTestFixture::assertEquals( const Modifications<Media>&, const Modifications<Media>&  );
template void AbstractMappingTestFixture::assertEquals( const Modifications<Member>&, const Modifications<Member>&  );

// ==========
// Helper Methods
// ==========

string AbstractMappingTestFixture::loadIntoStringBuffer( const string& filename ) 
{
  // read data from file into the buffer
  std::ifstream is;
  is.open (filename, std::ios::in | std::ios::binary );
  if ( (is.rdstate() & std::ifstream::failbit ) != 0 ) {
    ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Error opening file %s", filename.c_str());
  }
  
  // get length of file:
  is.seekg (0, std::ios::end);
  long length = (long) is.tellg();
  is.seekg (0, std::ios::beg);
  
  // allocate memory:
  char* data = new char [length];
  
  // read data as a block:
  is.read (data,length);
  is.close();
  if ( (is.rdstate() & std::ifstream::badbit ) != 0 ) {
    delete[] data;
    ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Error reading file %s", filename.c_str());
  }
  
  string result(data, length);
  delete[] data;
  return result;
}
