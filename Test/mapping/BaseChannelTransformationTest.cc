/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "BaseChannelTransformationTest.h"

#include <sstream>

#include "XmlMarshaller.h"
#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "Media.h"
#include "Chat.h"
#include "Message.h"
#include "Modifications.h"
#include "SqliteValue.h"

Channel BaseChannelTransformationTest::submitLocalChannel( void )
{
  Channel channel = updatedLocalChannel();
  channel.setGid( string( "1" ) );
  
  shared_ptr<ChannelHeader> header(channel.getHeader());
  header->setGid( string( "1" ) );  
  header->setSessionKey( "key" );
  
  // update post gid
  int i = 1;
  for (list< shared_ptr<ChannelPost> >::const_iterator it=channel.getPosts().begin(); it != channel.getPosts().end(); it++) {      
    stringstream ss;
    ss << i++;
    (*it)->setGid( ss.str() );
  }
  
  // update comment gid
  i = 1;
  list< shared_ptr<ChannelComment> > allComments(channel.getAllComments());
  for (list< shared_ptr<ChannelComment> >::const_iterator it=allComments.begin(); it != allComments.end(); it++) {      
    stringstream ss;
    ss << i++;
    (*it)->setGid( ss.str() );
  }
  
  // update resource gid
  i = 1;
  list< shared_ptr<Resource> > allResource(channel.getAllResources());
  for (list< shared_ptr<Resource> >::const_iterator it=allResource.begin(); it != allResource.end(); it++) {
    stringstream ss;
    ss << i++;
    (*it)->setGid( ss.str() );
  }
  
  return channel;
}

Channel BaseChannelTransformationTest::updatedLocalChannel( void )
{
  Channel channel = sampleChannel( false );
  channel.getHeader()->setEncrypted( true );
  channel.getHeader()->setSecure( true );
  
  shared_ptr<ChannelPost> post(*channel.getPosts().begin());
  // --
  // internal post comment
  {
    shared_ptr<ChannelComment> comment(shared_ptr<ChannelComment>(new ChannelComment()));
    comment->setGid( string( "1" ) );
    comment->setContent( "comment" );
    comment->setOwner( "owner" );
    comment->setModifyTime( jtime("2010-03-01T00:00:00.000+01:00") );
    comment->setCreateTime( jtime("2010-03-01T00:00:00.000+01:00") );
    
    shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
    media->setGid( string( "3" ) );
    media->setOwner( "owner" );
    comment->getResources().push_back( media );
    
    post->getComments().push_back(comment);
  }

  // --
  // internal channel comment
  {
    shared_ptr<ChannelComment> comment(shared_ptr<ChannelComment>(new ChannelComment()));
    comment->setGid( string( "3" ) );
    comment->setContent( "comment" );
    comment->setOwner( "owner" );
    comment->setModifyTime( jtime("2010-03-01T00:00:00.000+01:00") );
    comment->setCreateTime( jtime("2010-03-01T00:00:00.000+01:00") );
    
    shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
    media->setGid( string( "5" ) );
    media->setOwner( "owner" );
    comment->getResources().push_back( media );
    
    channel.getComments().push_back(comment);
  }
  
  return channel;
}

Channel BaseChannelTransformationTest::updatedRemoteChannel( void )
{
  Channel channel = sampleChannel( false );
  channel.setSyncAnchor( "1" );
  channel.getHeader()->clearSyncAnchor();
  channel.getHeader()->setSessionKey( "key" );
  for (list< shared_ptr<ChannelMember> >::const_iterator it=channel.getHeader()->getMembers().begin();
       it != channel.getHeader()->getMembers().end();
       it++) {
    (*it)->clearSessionKey();
  }

  // channel comment 3
  {
    shared_ptr<ChannelComment> comment(shared_ptr<ChannelComment>(new ChannelComment()));
    comment->setGid( string( "3" ) );
    comment->setContent( "comment" );
    comment->setOwner( "owner" );
    comment->setOwnerFullname( "owner" );
    comment->setModifiedBy( "member1" );
    comment->setModifiedByFullname( "member 1" );
    comment->setModifyTime( jtime("2010-03-01T00:00:00.000+01:00") );
    comment->setCreateTime( jtime("2010-03-01T00:00:00.000+01:00") );
    comment->setOperation( ejin::kSyncUpdate );

    shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
    media->setGid( string( "5" ) );
    media->setOwner( "owner" );
    comment->getResources().push_back( media );
    
    channel.getComments().push_back(comment);
  }
  // channel comment 1
  {
    shared_ptr<ChannelComment> comment(shared_ptr<ChannelComment>(new ChannelComment()));
    comment->setGid( string( "1" ) );
    comment->setPostGid( string( "1" ) );
    comment->setContent( "comment" );
    comment->setOwner( "owner" );
    comment->setOwnerFullname( "owner" );
    comment->setModifiedBy( "member1" );
    comment->setModifiedByFullname( "member 1" );
    comment->setModifyTime( jtime("2010-03-01T00:00:00.000+01:00") );
    comment->setCreateTime( jtime("2010-03-01T00:00:00.000+01:00") );
    comment->setOperation( ejin::kSyncUpdate );

    shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
    media->setGid( string( "3" ) );
    media->setOwner( "owner" );
    comment->getResources().push_back( media );
    
    channel.getComments().push_back(comment);
  }
  return channel;
}
Channel BaseChannelTransformationTest::sampleChannel( bool full )
{
  Channel channel( 1, string( "1" ), false );
  
  // --
  // channel header
  {
    shared_ptr<ChannelHeader> header(shared_ptr<ChannelHeader>(new ChannelHeader()));
    header->setGid( string( "1" ) );
    header->setState( 1 );
    header->setName( "channel1" );
    header->setOwner( "owner" );
    header->setOwnerFullname( "owner" );
    header->setModifiedBy( "member1" );
    header->setModifiedByFullname( "member 1" );
    header->setRole( ejin::kSyncRoleAdmin );
    header->setMembership( ejin::kMembershipAccepted );
    header->setContent( "line1<br>\neuro (\u20AC)<br>\nline2" );
    header->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
    header->setCreateTime( jtime("2010-01-01T00:00:00.000+01:00") );        
    
    shared_ptr<ChannelMember> member(shared_ptr<ChannelMember>(new ChannelMember()));
    member->setUsername( "owner" );
    member->setFullname( "owner" );
    member->setModifiedBy( "member1" );
    member->setModifiedByFullname( "member 1" );
    member->setSessionKey( "key" );
    member->setTags( "tag" );
    member->setRole( ejin::kSyncRoleAdmin );
    member->setMembership( ejin::kMembershipAccepted );
    member->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
    member->setCreateTime( jtime("2010-01-01T00:00:00.000+01:00") );    
    header->getMembers().push_back( member );
    
    if ( full ) {
      shared_ptr<Media> media(shared_ptr<Media>(new Media()));
      media->setGid( string( "1" ) );
      media->setOwner( "owner" );
      header->getResources().push_back( media );
    } else {
      shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
      media->setGid( string( "1" ) );
      media->setOwner( "owner" );
      header->getResources().push_back( media );
    }
    
    channel.setHeader( header );
  }
  // --
  // channel post
  {
    shared_ptr<ChannelPost> post(shared_ptr<ChannelPost>(new ChannelPost()));
    post->setGid( string( "1" ) );
    post->setOwner( "owner" );
    post->setOwnerFullname( "owner" );
    post->setModifiedBy( "member1" );
    post->setModifiedByFullname( "member 1" );
    post->setContent( "post" );
    post->setModifyTime( jtime("2010-02-01T00:00:00.000+01:00") );
    post->setCreateTime( jtime("2010-02-01T00:00:00.000+01:00") );
    post->setOperation( ejin::kSyncUpdate );

    if ( full ) {
      shared_ptr<Media> media(shared_ptr<Media>(new Media()));
      media->setGid( string( "2" ) );
      media->setOwner( "owner" );
      post->getResources().push_back( media );
    } else {
      shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
      media->setGid( string( "2" ) );
      media->setOwner( "owner" );
      post->getResources().push_back( media );
    }
    
    channel.getPosts().push_back( post );
  }
  // --
  // channel comment
  {
    shared_ptr<ChannelComment> comment(shared_ptr<ChannelComment>(new ChannelComment()));
    comment->setGid( string( "2" ) );
    comment->setPostGid( string( "1" ) );
    comment->setOwner( "owner" );
    comment->setOwnerFullname( "owner" );
    comment->setModifiedBy( "member1" );
    comment->setModifiedByFullname( "member 1" );
    comment->setContent( "comment" );
    comment->setModifyTime( jtime("2010-01-31T01:00:00+01:00") );
    comment->setCreateTime( jtime("2010-01-31T01:00:00+01:00") );
    comment->setOperation( ejin::kSyncUpdate );

    if ( full ) {
      shared_ptr<Media> media(shared_ptr<Media>(new Media()));
      media->setGid( string( "4" ) );
      media->setOwner( "owner" );
      comment->getResources().push_back( media );
    } else {
      shared_ptr<ChannelMedia> media(shared_ptr<ChannelMedia>(new ChannelMedia()));
      media->setGid( string( "4" ) );
      media->setOwner( "owner" );
      comment->getResources().push_back( media );
    }
    channel.getComments().push_back( comment );
  }
  
  return channel;
}

// sample container of remote modifications.
Modifications<ChannelHeader> BaseChannelTransformationTest::remoteModifiedChannelHeaders( void )
{
  Modifications<ChannelHeader> container;
  container.setSyncAnchor( string( "10" ) );
  
  // --
  // header added
  shared_ptr<ChannelHeader> header(shared_ptr<ChannelHeader>(new ChannelHeader()));
  header->setGid( string( "1" ) );
  header->setState( 1 );
  header->setSyncAnchor( "1" );
  header->setName( "channel1" );
  header->setSessionKey( "key1" );
  header->setRole( ejin::kSyncRoleAdmin );
  header->setMembership( ejin::kMembershipAccepted );
  header->setContent( "hello world 1" );
  header->setOwner( "member1" );
  header->setOwnerFullname( "member 1" );
  header->setModifiedBy( "member2" );
  header->setModifiedByFullname( "member 2" );
  header->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  header->setCreateTime( jtime("2010-01-01T00:00:00.000+01:00") );
  header->setEncrypted( true );
  header->setSecure( true );
  
  container.getAdded().push_back( header );
  
  // --
  // header updated
  header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setGid( string( "2" ) );
  header->setState( 1 );
  header->setSyncAnchor( "2" );
  header->setName( "channel2" );
  header->setSessionKey( "key2" );
  header->setRole( ejin::kSyncRoleAdmin );
  header->setMembership( ejin::kMembershipAccepted );
  header->setContent( "hello world 2" );
  header->setOwner( "member2" );
  header->setOwnerFullname( "member 2" );
  header->setModifiedBy( "member1" );
  header->setModifiedByFullname( "member 1" );
  header->setModifyTime( jtime("2010-02-01T00:00:00.000+01:00") );
  header->setCreateTime( jtime("2010-02-01T00:00:00.000+01:00") );
  
  container.getUpdated().push_back( header );
  
  // --
  // media removed
  header = shared_ptr<ChannelHeader>(new ChannelHeader());
  header->setGid( string( "3" ) );
  header->setSyncAnchor( "3" );
  header->setModifyTime( jtime("2010-03-01T00:00:00.000+01:00") );
  header->setCreateTime( jtime("2010-03-01T00:00:00.000+01:00") );
  header->setOwner( "member2" );
  header->setModifiedBy( "member1" );
  container.getRemoved().push_back( header );
  
  return container;
}
