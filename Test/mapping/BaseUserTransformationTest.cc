/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "BaseUserTransformationTest.h"

#include "User.h"
#include "Picture.h"
#include "Member.h"
#include "ApprovedMember.h"

// list of local modified media instances

shared_ptr<User> BaseUserTransformationTest::updateSubscription( void )
{
  shared_ptr<Member> member(shared_ptr<Member>(new Member()));
  member->setId( 1L );
  member->setUsername( "owner" );
  member->setEmail( "member1@foo.de" );
  member->setFirstName( "firstName" );
  member->setLastName( "lastName" );
  member->setProductIdentifier( "product" );
  member->setProductName( "product" );
  member->setPubKey( "aGFsbG8=" );
  member->setMaxChannelCount( 10 );
  member->setMaxAttachmentCount( 30 );
  member->setMaxTotalAttachmentSize( 311009280 );
  member->setMaxAttachmentSize( 103669760 );
  member->setMaxTextContentLength( 4000 );
  member->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  member->setSubscriptionExpireTime( jtime("2010-01-01T00:00:00.000+02:00") );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "member1" )) );

  return shared_ptr<User>(new User( "owner", member, NULL ));
}
list< shared_ptr<User> > BaseUserTransformationTest::localUpdates( void )
{
  list< shared_ptr<User> > users = this->userUpdates( );
  for ( auto it=users.begin() ; it != users.end(); it++ ) {
    (*it)->getMember()->setPhone( "12345" );
    (*it)->getMember()->setDialInCode( 49 );
    (*it)->getMember()->setTwoFactorAuthentication( true );
  }
  return users;
}
list< shared_ptr<User> > BaseUserTransformationTest::userUpdates( void )
{
  list< shared_ptr<User> > users;
  
  shared_ptr<Member> member(shared_ptr<Member>(new Member()));
  member->setId( 1L );
  member->setPubKey( "aGFsbG8=" );
  member->setEmail( "member1@foo.de" );
  member->setEmailHash( "e568682e5a52dc2791bdd5a04f6cfe65" );
  member->setMaxChannelCount( 10 );
  member->setMaxAttachmentCount( 30 );
  member->setMaxTotalAttachmentSize( 311009280 );
  member->setMaxAttachmentSize( 103669760 );
  member->setMaxTextContentLength( 4000 );
  member->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  member->setUsername( "member1" );
  member->setFirstName( "firstName" );
  member->setLastName( "lastName" );
  member->setProductIdentifier( "product1" );
  member->setProductName( "product1" );
  member->setSubscriptionExpireTime( jtime("2010-07-01T00:00:00.000+02:00") );

  shared_ptr<Picture> picture(shared_ptr<Picture>(new Picture()));
  picture->setMimeType( "image/jpg" );
  picture->setData( string("1\0", 2) );
  picture->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );

  users.push_back( shared_ptr<User>(new User( "member1", member, picture )) );

  member = shared_ptr<Member>(new Member());
  member->setId( 2L );
  member->setPubKey( "aGFsbG8y" );
  member->setEmail( "member2@foo.de" );
  member->setEmailHash( "3cd74a87cae1cf8783d8a9eb1740a200" );
  member->setMaxChannelCount( 10 );
  member->setMaxAttachmentCount( 30 );
  member->setMaxTotalAttachmentSize( 311009280 );
  member->setMaxAttachmentSize( 103669760 );
  member->setMaxTextContentLength( 4000 );
  member->setModifyTime( jtime("2010-02-01T00:00:00.000+01:00") );
  member->setUsername( "member2" );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "member1" )) );
  member->setSubscriptionExpireTime( jtime("2010-08-01T00:00:00.000+02:00") );

  users.push_back( shared_ptr<User>(new User( "member2", member, NULL )) );
  
  return users;
}

// sample container of remote modifications.
Modifications<User> BaseUserTransformationTest::remoteModifiedUsers( void )
{
  Modifications<User> container;
  container.setSyncAnchor( string( "10" ) );
  container.setSyncTime( jtime("2010-01-01T00:00:00.000+01:00") );
  
  // --
  // member added
  shared_ptr<Member> member(shared_ptr<Member>(new Member()));
  member->setPubKey( "aGFsbG8=" );
  member->setEmail( "member1@foo.de" );
  member->setEmailHash( "e568682e5a52dc2791bdd5a04f6cfe65" );
  member->setMaxChannelCount( 10 );
  member->setMaxAttachmentCount( 30 );
  member->setMaxTotalAttachmentSize( 311009280 );
  member->setMaxAttachmentSize( 103669760 );
  member->setMaxTextContentLength( 4000 );
  member->setAppleLogin( true );
  member->setTwoFactorAuthentication( true );
  member->setUsername( "member1" );
  member->setFirstName( "firstName" );
  member->setLastName( "lastName" );
  member->setProductIdentifier( "product" );
  member->setProductName( "product" );

  shared_ptr<Picture> picture(shared_ptr<Picture>(new Picture()));
  picture->setMimeType( "image/jpg" );
  picture->setData( string("1\0", 2) );
  picture->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  
  member->setSubscriptionExpireTime( jtime("2010-07-01T00:00:00.000+02:00") );

  container.getAdded().push_back( shared_ptr<User>(new User( "member1", member, picture )) );

  // --
  // member updated
  member = shared_ptr<Member>(new Member());
  member->setPubKey( "aGFsbG8y" );
  member->setEmail( "member2@foo.de" );
  member->setEmailHash( "3cd74a87cae1cf8783d8a9eb1740a200" );
  member->setProductIdentifier( "product1" );
  member->setProductName( "product1" );
  member->setMaxChannelCount( 10 );
  member->setMaxAttachmentCount( 30 );
  member->setMaxTotalAttachmentSize( 311009280 );
  member->setMaxAttachmentSize( 103669760 );
  member->setMaxTextContentLength( 4000 );
  member->setAppleLogin( true );
  member->setTwoFactorAuthentication( true );
  member->setUsername( "member2" );
  member->getApprovedMembers().push_back( shared_ptr<ApprovedMember>(new ApprovedMember( "member1" )) );
  member->setSubscriptionExpireTime( jtime("2010-08-01T00:00:00.000+02:00") );

  container.getUpdated().push_back( shared_ptr<User>(new User( "member2", member, NULL )) );
  
  // --
  // member removed
  container.getRemoved().push_back( shared_ptr<User>(new User( "member3" )) );
  
  return container;
}
