/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelXmlTransformationTest.h"

#include <sstream>

#include "XmlMarshaller.h"
#include "StringTemplate.h"
#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "Modifications.h"
#include "SqliteValue.h"

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelXmlTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ChannelXmlTransformationTest::setUp( void )
{
  marshaller_.reset();
  marshaller_ = unique_ptr< XmlMarshaller >(new XmlMarshaller());
  
  template_.reset();
  template_ = unique_ptr< StringTemplate >(new StringTemplate());
}

void ChannelXmlTransformationTest::tearDown( void )
{
  marshaller_.reset();
  template_.reset();
}

// ==========
// TEST Methods
// ==========

void ChannelXmlTransformationTest::testExpandChannelTemplate( void )
{
  Channel channel = sampleChannel( true );
  std::string output;
  template_->expand(TestResourcePath + "template/example.tpl",
                    TestResourcePath + "template/include.tpl",
                    "",
                    TestResourcePath + "messages",
                    &channel,
                    output,
                    "de",
                    "test",
                    false);
  //std::cout << output;
}

void ChannelXmlTransformationTest::testMarshalChannel( void )
{
  Channel channel = updatedLocalChannel();
  
  string xml(marshaller_->marshal( channel, true ));
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/xml/channel-request.xml" ));
  
  //cout << ref << endl;
  //std::cout << xml << std::endl;
}

void ChannelXmlTransformationTest::testUnmarshalChannel( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/channel-response.xml", NULL, &length ));

  unique_ptr< Channel > channel( unique_ptr< Channel >( new Channel() ));
  marshaller_->unmarshal(*channel, data.get(), length);

  channel->setId( (integer)1 );
  assertEquals( updatedRemoteChannel(), *channel );  
}

void ChannelXmlTransformationTest::testUnmarshalChannelProfile( void )
{  
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/channel-profile-response.xml", NULL, &length ));

  Channel channel = updatedLocalChannel();
  marshaller_->unmarshal(channel, data.get(), length);
  
  assertEquals( submitLocalChannel(), channel );  
}

void ChannelXmlTransformationTest::testUnmarshalRemoteModifications( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/channel-header-response.xml", NULL, &length ));
  
  unique_ptr< Modifications<ChannelHeader> > headers( unique_ptr< Modifications<ChannelHeader> >( new Modifications<ChannelHeader>() ));
  marshaller_->unmarshal(*headers, data.get(), length);
  
  assertEquals( remoteModifiedChannelHeaders(), *headers );
}
