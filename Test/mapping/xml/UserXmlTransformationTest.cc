/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UserXmlTransformationTest.h"

#include "Member.h"
#include "User.h"
#include "ResultList.h"
#include "Utilities.h"

#include <sstream>

CPPUNIT_TEST_SUITE_REGISTRATION( UserXmlTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void UserXmlTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< XmlMarshaller >(new XmlMarshaller());
}

void UserXmlTransformationTest::tearDown( void )
{
  marshaller_.reset();
}

// ==========
// TEST Methods
// ==========

void UserXmlTransformationTest::testUnmarshalRemoteModifications( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/account-response.xml", NULL, &length ));
  
  unique_ptr< Modifications<User> > users(unique_ptr< Modifications<User> >(new Modifications<User>()));
  marshaller_->unmarshal( *users, data.get(), length );
  
  Modifications<User> reference( remoteModifiedUsers() );
  
  CPPUNIT_ASSERT( *users == reference );
  CPPUNIT_ASSERT( **users->getAdded().begin() == **reference.getAdded().begin() );
  CPPUNIT_ASSERT( **users->getUpdated().begin() == **reference.getUpdated().begin() );
  CPPUNIT_ASSERT( **users->getRemoved().begin() == **reference.getRemoved().begin() );
}
void UserXmlTransformationTest::testUnmarshalSearchResult( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/account-search-response.xml", NULL, &length ));

  unique_ptr< ResultList<User> > result(unique_ptr< ResultList<User> >(new ResultList<User>()));
  marshaller_->unmarshal(*result, data.get(), length);
  
  User ref( "member1" );
  shared_ptr<User> user1 = Utilities::findElementIn( userUpdates(), ref );
  
  CPPUNIT_ASSERT( 2 == result->size() );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), *user1 ) );
}
void UserXmlTransformationTest::testUnmarshalUser( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/select-product-response.xml", NULL, &length ));
  
  unique_ptr< User > result(unique_ptr< User >(new User()));
  marshaller_->unmarshal(*result, data.get(), length);
  CPPUNIT_ASSERT( *updateSubscription() == *result );
}
void UserXmlTransformationTest::testMarshalLocalUpdates( void )
{  
  Modifications<User> users(list< shared_ptr<User> >(), localUpdates(), list< shared_ptr<User> >())  ;
  users.setSyncAnchor( string( "1" ) );
  
  string xml(marshaller_->marshal( users, true ));
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/xml/account-request.xml" ));
  
  //  cout << "XML Reference:" << endl << ref << endl;
  //  cout << "XML Output:" << endl << xml << endl;
  //  CPPUNIT_ASSERT_EQUAL( ref, xml );
}

