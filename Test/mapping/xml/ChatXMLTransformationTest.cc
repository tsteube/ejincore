/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatXMLTransformationTest.h"

#include "Chat.h"
#include "Message.h"
#include "MessageList.h"
#include "ResultList.h"
#include "Utilities.h"
#include "StringTemplate.h"

#include <sstream>

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif

using ejin::Message;
using ejin::MessageList;
using ejin::Chat;

CPPUNIT_TEST_SUITE_REGISTRATION( ChatXmlTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ChatXmlTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< XmlMarshaller >(new XmlMarshaller());

  template_.reset();
  template_ = unique_ptr< StringTemplate >(new StringTemplate());
}

void ChatXmlTransformationTest::tearDown( void )
{
  marshaller_.reset();
  template_.reset();
}

// ==========
// TEST Methods
// ==========

void ChatXmlTransformationTest::testExpandChatTemplate( void )
{
  Chat chat = sampleChat();
  std::string output;
  template_->expand(TestResourcePath + "template/example.tpl",
                    TestResourcePath + "template/include.tpl",
                    "",
                    TestResourcePath + "messages",
                    chat,
                    output,
                    "en",
                    "test");
  std::cout << output;
}

void ChatXmlTransformationTest::testUnmarshalChats( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/chat-list-response.xml", NULL, &length ));
  
  unique_ptr< Modifications<Chat> > chats(unique_ptr< Modifications<Chat> >(new Modifications<Chat>()));
  marshaller_->unmarshal( *chats, data.get(), length );
  
  Modifications<Chat> reference( remoteModifiedChats() );

  CPPUNIT_ASSERT( *chats == reference );
  CPPUNIT_ASSERT( **chats->getAdded().begin() == **reference.getAdded().begin() );
  CPPUNIT_ASSERT( **chats->getUpdated().begin() == **reference.getUpdated().begin() );
  CPPUNIT_ASSERT( **chats->getRemoved().begin() == **reference.getRemoved().begin() );
}
void ChatXmlTransformationTest::testUnmarshalMessageResult( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/message-list-response.xml", NULL, &length ));
  
  unique_ptr< ResultList<Message> > result(unique_ptr< ResultList<Message> >(new ResultList<Message>()));
  marshaller_->unmarshal(*result, data.get(), length);

  list< shared_ptr<Message> > messages = remoteUpdatedMessages();
  
  CPPUNIT_ASSERT( 2 == result->size() );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), **messages.begin() ) );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), **(messages.begin()++) ) );
}
void ChatXmlTransformationTest::testUnmarshalMessage( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/xml/message-response.xml", NULL, &length ));
  
  unique_ptr<Message> message(unique_ptr<Message>(new Message()));
  marshaller_->unmarshal(*message, data.get(), length);
  
  CPPUNIT_ASSERT( *message == *receiveMessage() );
}
void ChatXmlTransformationTest::testMarshalNewMessages( void )
{
  list< shared_ptr<Message> > messages;
  messages.push_back( sendMessage() );

  unique_ptr< MessageList > result(unique_ptr< MessageList >(new MessageList( messages )));

  string xml( marshaller_->marshal( *result, true ) );
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/xml/message-list-request.xml" ));
  
  //std::cout << "XML Reference:" << std::endl << ref << std::endl;
  //std::cout << "XML Output:" << std::endl << xml << std::endl;
  //CPPUNIT_ASSERT_EQUAL( ref, xml );
}
void ChatXmlTransformationTest::testMarshalNewChat( void )
{
  shared_ptr<Chat> chat( createChat( ) );

  string xml( marshaller_->marshal( *chat, true ) );
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/xml/chat-request.xml" ));
  
  //std::cout << "XML Reference:" << std::endl << ref << std::endl;
  //std::cout << "XML Output:" << std::endl << xml << std::endl;
  //CPPUNIT_ASSERT_EQUAL( ref, xml );
}

