/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEDIA_XML_TRANSFORMATION_UNIT_TEST_H__
#define __MEDIA_XML_TRANSFORMATION_UNIT_TEST_H__

#include "BaseMediaTransformationTest.h"
#include "Declarations.h"

/*
 * Ejin unit tests
 */
class MediaXmlTransformationTest : public BaseMediaTransformationTest
{
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( MediaXmlTransformationTest );  
  CPPUNIT_TEST( testUnmarshalRemoteModifications );
  CPPUNIT_TEST( testMarshalLocalUpdates );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testUnmarshalRemoteModifications( void );
  void testMarshalLocalUpdates( void );

private:

  unique_ptr<XmlMarshaller>  marshaller_;
  
};

#endif // __MEDIA_XML_TRANSFORMATION_UNIT_TEST_H__
