/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHTMLTransformationTest.h"

#include <sstream>

#include "StringTemplate.h"
#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "Modifications.h"
#include "SqliteValue.h"

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelHtmlTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ChannelHtmlTransformationTest::setUp( void )
{
  template_.reset();
  template_ = unique_ptr< StringTemplate >(new StringTemplate());
}

void ChannelHtmlTransformationTest::tearDown( void )
{
  template_.reset();
}

// ==========
// TEST Methods
// ==========

void ChannelHtmlTransformationTest::testExpandChannelTemplate( void )
{
  Channel channel = sampleChannel( true );
  std::string output;
  template_->expand(TestResourcePath + "template/example.tpl",
                    TestResourcePath + "template/include.tpl",
                    "",
                    TestResourcePath + "messages",
                    &channel,
                    output,
                    "de",
                    "test",
                    false);
  //std::cout << output;
}
