/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatHTMLTransformationTest.h"

#include "Chat.h"
#include "Message.h"
#include "MessageList.h"
#include "ResultList.h"
#include "Utilities.h"
#include "StringTemplate.h"

#include <sstream>

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif

using ejin::Message;
using ejin::MessageList;
using ejin::Chat;

CPPUNIT_TEST_SUITE_REGISTRATION( ChatHtmlTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ChatHtmlTransformationTest::setUp( void )
{
  template_.reset();
  template_ = unique_ptr< StringTemplate >(new StringTemplate());
}

void ChatHtmlTransformationTest::tearDown( void )
{
  template_.reset();
}

// ==========
// TEST Methods
// ==========

void ChatHtmlTransformationTest::testExpandChatTemplate( void )
{
  Chat chat = sampleChat();
  std::string output;
  template_->expand(TestResourcePath + "template/example.tpl",
                    TestResourcePath + "template/include.tpl",
                    "",
                    TestResourcePath + "messages",
                    chat,
                    output,
                    "en",
                    "test");
  std::cout << output;
}
