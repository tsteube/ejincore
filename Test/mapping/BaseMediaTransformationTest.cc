/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "BaseMediaTransformationTest.h"

#include <sstream>

#include "Media.h"
#include "Modifications.h"
#include "SqliteValue.h"

// list of local modified media instances
list< shared_ptr<Media> > BaseMediaTransformationTest::mediaUpdates( void )
{
  list< shared_ptr<Media> > medias;
  
  shared_ptr<Media> media(shared_ptr<Media>(new Media()));
  media->setId( 1L );
  media->setGid( string( "1" ) );
  media->setSize( 10 );
  media->setTransferKey( "key1" );
  media->setMimeType( "application/xml" );
  media->setOwner( "member1" );
  media->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  media->setCreateTime( jtime("2010-01-01T00:00:00.000+01:00") );
  medias.push_back( media );
  
  media = shared_ptr<Media>(new Media());
  media->setGid( string( "2" ) );
  media->setId( 2L );
  media->setSize( 10 );
  media->setTransferKey( "key2" );
  media->setMimeType( "application/xml" );
  media->setOwner( "member1" );
  media->setModifyTime( jtime("2010-02-01T00:00:00.000+01:00") );
  media->setCreateTime( jtime("2010-02-01T00:00:00.000+01:00") );
  medias.push_back( media );
  
  return medias;
}

// sample container of remote modifications.
Modifications<Media> BaseMediaTransformationTest::remoteModifiedMedias( void )
{
  Modifications<Media> container;
  container.setSyncAnchor( string( "10" ) );
  container.setSyncTime( jtime("2010-01-01T00:00:00.000+01:00") );
  
  // --
  // media added
  shared_ptr<Media> media(shared_ptr<Media>(new Media()));
  media->setGid( string( "1" ) );
  media->setRole( ejin::kSyncRoleAdmin );
  media->setTransferKey( "key1" );
  media->setMimeType( "application/xml" );
  media->setOwner( "member1" );
  media->setModifiedBy( "member2" );
  media->setSize( 10 );
  media->setEncrypted( true );
  media->setSecure( true );
  media->setModifyTime( jtime("2010-01-01T00:00:00.000+01:00") );
  media->setCreateTime( jtime("2010-01-01T00:00:00.000+01:00") );
  container.getAdded().push_back( media );
  
  // --
  // media updated
  media = shared_ptr<Media>(new Media());
  media->setGid( string( "2" ) );
  media->setRole( ejin::kSyncRoleAdmin );
  media->setSize( 10 );
  media->setTransferKey( "key2" );
  media->setMimeType( "application/xml" );
  media->setOwner( "member2" );
  media->setModifiedBy( "member1" );
  media->setModifyTime( jtime("2010-02-01T00:00:00.000+01:00") );
  media->setCreateTime( jtime("2010-02-01T00:00:00.000+01:00") );
  container.getUpdated().push_back( media );
  
  // --
  // media removed
  media = shared_ptr<Media>(new Media());
  media->setGid( string( "3" ) );
  container.getRemoved().push_back( media );
  
  return container;
}
