/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UserJsonTransformationTest.h"

#include "User.h"
#include "Member.h"
#include "JsonMarshaller.h"
#include "ResultList.h"
#include "Utilities.h"

#include <sstream>

CPPUNIT_TEST_SUITE_REGISTRATION( UserJsonTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void UserJsonTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< JsonMarshaller >(new JsonMarshaller());
}
void UserJsonTransformationTest::tearDown( void )
{
  marshaller_.reset();
}

// ==========
// TEST Methods
// ==========

void UserJsonTransformationTest::testUnmarshalRemoteModifications( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/account-response.json", NULL, &length ));
  
  unique_ptr< Modifications<User> > users(unique_ptr< Modifications<User> >(new Modifications<User>()));
  marshaller_->unmarshal(*users, data.get(), length);
  
  Modifications<User> reference( remoteModifiedUsers() );

  CPPUNIT_ASSERT( *users == reference );
  CPPUNIT_ASSERT( **users->getAdded().begin() == **reference.getAdded().begin() );
  CPPUNIT_ASSERT( **users->getUpdated().begin() == **reference.getUpdated().begin() );
  CPPUNIT_ASSERT( **users->getRemoved().begin() == **reference.getRemoved().begin() );
}
void UserJsonTransformationTest::testUnmarshalSearchResult( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/account-search-response.json", NULL, &length ));

  unique_ptr< ResultList<User> > result(unique_ptr< ResultList<User> >(new ResultList<User>()));
  marshaller_->unmarshal(*result, data.get(), length);
  
  User ref( "member1" );
  shared_ptr<User> member1 = Utilities::findElementIn( userUpdates(), ref );
  CPPUNIT_ASSERT( member1 );
  
  CPPUNIT_ASSERT( 2 == result->size() );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), *member1 ) );
}
void UserJsonTransformationTest::testUnmarshalUser( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/select-product-response.json", NULL, &length ));

  unique_ptr< User > result(unique_ptr< User >(new User()));
  marshaller_->unmarshal(*result, data.get(), length);
  CPPUNIT_ASSERT( *updateSubscription() == *result );
}
void UserJsonTransformationTest::testMarshalLocalUpdates( void )
{
  Modifications<User> users(list< shared_ptr<User> >(), localUpdates(), list< shared_ptr<User> >())  ;
  users.setSyncAnchor( string( "1" ) );
  
  string xml(marshaller_->marshal( users, true ));
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/json/account-request.json" ));
  
  //  std::cout << "JSON Reference:" << std::endl << ref << std::endl;
  //  CPPUNIT_ASSERT_EQUAL( ref, xml );
}
