/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductJSONTransformationTest.h"

#include "Product.h"
#include "User.h"
#include "JsonMarshaller.h"
#include "ResultList.h"
#include "Utilities.h"

#include <sstream>

using ejin::Product;

CPPUNIT_TEST_SUITE_REGISTRATION( ProductJsonTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ProductJsonTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< JsonMarshaller >(new JsonMarshaller());
}
void ProductJsonTransformationTest::tearDown( void )
{
  marshaller_.reset();
}

// ==========
// TEST Methods
// ==========

void ProductJsonTransformationTest::testUnmarshalProductResult( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/product-search-response.json", NULL, &length ));
  
  unique_ptr< ResultList<Product> > result(unique_ptr< ResultList<Product> >(new ResultList<Product>()));
  marshaller_->unmarshal(*result, data.get(), length);
  
  list< shared_ptr<Product> > products = allProducts();
  
  CPPUNIT_ASSERT( 2 == result->size() );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), **products.begin() ) );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), **(products.begin()++) ) );
}
void ProductJsonTransformationTest::testUnmarshalUser( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/select-product-response.json", NULL, &length ));
  
  unique_ptr<User> user(unique_ptr<User>(new User()));
  marshaller_->unmarshal(*user, data.get(), length);
  
  CPPUNIT_ASSERT( *user == *receiveUpdatedUser() );
}

