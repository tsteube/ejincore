/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatJSONTransformationTest.h"

#include "Chat.h"
#include "Message.h"
#include "MessageList.h"
#include "JsonMarshaller.h"
#include "ResultList.h"
#include "Utilities.h"

#include <sstream>

using ejin::Message;
using ejin::MessageList;
using ejin::Chat;

CPPUNIT_TEST_SUITE_REGISTRATION( ChatJsonTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ChatJsonTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< JsonMarshaller >(new JsonMarshaller());
}
void ChatJsonTransformationTest::tearDown( void )
{
  marshaller_.reset();
}

// ==========
// TEST Methods
// ==========

void ChatJsonTransformationTest::testUnmarshalChats( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/chat-list-response.json", NULL, &length ));
  
  unique_ptr< Modifications<Chat> > chats(unique_ptr< Modifications<Chat> >(new Modifications<Chat>()));
  marshaller_->unmarshal( *chats, data.get(), length );
  
  Modifications<Chat> reference( remoteModifiedChats() );
  
  CPPUNIT_ASSERT( *chats == reference );
  CPPUNIT_ASSERT( **chats->getAdded().begin() == **reference.getAdded().begin() );
  CPPUNIT_ASSERT( **chats->getUpdated().begin() == **reference.getUpdated().begin() );
  CPPUNIT_ASSERT( **chats->getRemoved().begin() == **reference.getRemoved().begin() );
}
void ChatJsonTransformationTest::testUnmarshalMessageResult( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/message-list-response.json", NULL, &length ));
  
  unique_ptr< ResultList<Message> > result(unique_ptr< ResultList<Message> >(new ResultList<Message>()));
  marshaller_->unmarshal(*result, data.get(), length);
  
  list< shared_ptr<Message> > messages = remoteUpdatedMessages();
  
  CPPUNIT_ASSERT( 2 == result->size() );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), **messages.begin() ) );
  CPPUNIT_ASSERT( Utilities::findElementIn( result->getSet(), **(messages.begin()++) ) );
}
void ChatJsonTransformationTest::testUnmarshalMessage( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/message-response.json", NULL, &length ));
  
  unique_ptr<Message> message(unique_ptr<Message>(new Message()));
  marshaller_->unmarshal(*message, data.get(), length);
  
  CPPUNIT_ASSERT( *message == *receiveMessage() );
}
void ChatJsonTransformationTest::testMarshalNewMessages( void )
{
  list< shared_ptr<Message> > messages;
  messages.push_back( sendMessage() );
  
  unique_ptr< MessageList > result(unique_ptr< MessageList >(new MessageList( messages )));
  
  string xml( marshaller_->marshal( *result, true ) );
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/json/message-list-request.json" ));
  
  //std::cout << "XML Reference:" << std::endl << ref << std::endl;
  //std::cout << "XML Output:" << std::endl << xml << std::endl;
  //CPPUNIT_ASSERT_EQUAL( ref, xml );
}
void ChatJsonTransformationTest::testMarshalNewChat( void )
{
  shared_ptr<Chat> chat( createChat( ) );
  
  string xml( marshaller_->marshal( *chat, true ) );
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/json/chat-request.json" ));
  
  //std::cout << "XML Reference:" << std::endl << ref << std::endl;
  //std::cout << "XML Output:" << std::endl << xml << std::endl;
  //CPPUNIT_ASSERT_EQUAL( ref, xml );
}

