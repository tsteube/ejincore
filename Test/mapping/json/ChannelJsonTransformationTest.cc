/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelJsonTransformationTest.h"

#include <sstream>

#include "JsonMarshaller.h"
#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "Modifications.h"
#include "SqliteValue.h"

CPPUNIT_TEST_SUITE_REGISTRATION( ChannelJsonTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void ChannelJsonTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< JsonMarshaller >(new JsonMarshaller());
}
void ChannelJsonTransformationTest::tearDown( void )
{
  marshaller_.reset();
}

// ==========
// TEST Methods
// ==========

void ChannelJsonTransformationTest::testMarshalChannel( void )
{
  Channel channel = updatedLocalChannel();
  
  string xml(marshaller_->marshal( channel, true ));
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/json/channel-request.json" ));
  
  //std::cout << ref << std::endl;
  //std::cout << xml << std::endl;
}

void ChannelJsonTransformationTest::testUnmarshalChannel( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/channel-response.json", NULL, &length ));

  unique_ptr< Channel > channel( unique_ptr< Channel >( new Channel() ));
  marshaller_->unmarshal(*channel, data.get(), length);

  channel->setId( (integer)1 );
  assertEquals( updatedRemoteChannel(), *channel );  
}

void ChannelJsonTransformationTest::testUnmarshalChannelProfile( void )
{  
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/channel-profile-response.json", NULL, &length ));

  Channel channel = updatedLocalChannel();
  marshaller_->unmarshal(channel, data.get(), length);
  
  assertEquals( submitLocalChannel(), channel );  
}

void ChannelJsonTransformationTest::testUnmarshalRemoteModifications( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/channel-header-response.json", NULL, &length ));
  
  unique_ptr< Modifications<ChannelHeader> > headers( unique_ptr< Modifications<ChannelHeader> >( new Modifications<ChannelHeader>() ));
  marshaller_->unmarshal(*headers, data.get(), length);
  
  assertEquals( remoteModifiedChannelHeaders(), *headers );
}
