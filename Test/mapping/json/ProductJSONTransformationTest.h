/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __PRODUCT_JSON_TRANSFORMATION_UNIT_TEST_H__
#define __PRODUCT_JSON_TRANSFORMATION_UNIT_TEST_H__

#include "Declarations.h"
#include "BaseProductTransformationTest.h"

/*
 * Ejin unit tests
 */
class ProductJsonTransformationTest : public BaseProductTransformationTest
{
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ProductJsonTransformationTest );
  CPPUNIT_TEST( testUnmarshalUser );
  CPPUNIT_TEST( testUnmarshalProductResult );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testUnmarshalUser( void );
  void testUnmarshalProductResult( void );
  
private:
  
  unique_ptr<JsonMarshaller>  marshaller_;
  
};

#endif // __PRODUCT_JSON_TRANSFORMATION_UNIT_TEST_H__
