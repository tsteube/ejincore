/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaJsonTransformationTest.h"

#include <sstream>

#include "JsonMarshaller.h"
#include "Media.h"
#include "Modifications.h"
#include "SqliteValue.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MediaJsonTransformationTest );

// ==========
// Test Setup/Teardown
// ==========

void MediaJsonTransformationTest::setUp( void )
{
  //  _properties.reset();
  marshaller_.reset();
  marshaller_ = unique_ptr< JsonMarshaller >(new JsonMarshaller());
}
void MediaJsonTransformationTest::tearDown( void )
{
  marshaller_.reset();
}

// ==========
// TEST Methods
// ==========

void MediaJsonTransformationTest::testUnmarshalRemoteModifications( void )
{
  size_t length = 0;
  unique_ptr<const char> data = unique_ptr<const char>(loadFile( TestResourcePath + "mapping/json/media-response.json", NULL, &length ));

  unique_ptr< Modifications<Media> > medias(unique_ptr< Modifications<Media> >(new Modifications<Media>()));
  marshaller_->unmarshal(*medias, data.get(), length);
  
  Media m(**medias->getAdded().begin());
  Media m2(**medias->getUpdated().begin());
  Modifications<Media> reference( remoteModifiedMedias() );
  
  CPPUNIT_ASSERT( *medias == reference );
  CPPUNIT_ASSERT( **medias->getAdded().begin() == **reference.getAdded().begin() );
  CPPUNIT_ASSERT( **medias->getUpdated().begin() == **reference.getUpdated().begin() );
  CPPUNIT_ASSERT( **medias->getRemoved().begin() == **reference.getRemoved().begin() );
}
void MediaJsonTransformationTest::testMarshalLocalUpdates( void )
{  
  Modifications<Media> medias(list< shared_ptr<Media> >(), mediaUpdates(), list< shared_ptr<Media> >())  ;
  medias.setSyncAnchor( string( "1" ) );
  
  string xml(marshaller_->marshal( medias, true ));
  string ref(loadIntoStringBuffer( TestResourcePath + "mapping/json/media-request.json" ));
  
  //std::cout << "JSON Reference:" << std::endl << ref << std::endl;
  //std::cout << "JSON Output:" << std::endl << xml << std::endl;
  //CPPUNIT_ASSERT_EQUAL( ref, xml );
}
