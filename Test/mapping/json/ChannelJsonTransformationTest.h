/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_JSON_TRANSFORMATION_UNIT_TEST_H__
#define __CHANNEL_JSON_TRANSFORMATION_UNIT_TEST_H__

#include "BaseChannelTransformationTest.h"

/*
 * Ejin unit tests
 */
class ChannelJsonTransformationTest : public BaseChannelTransformationTest
{
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChannelJsonTransformationTest );
  CPPUNIT_TEST( testMarshalChannel );
  CPPUNIT_TEST( testUnmarshalChannel );
  CPPUNIT_TEST( testUnmarshalChannelProfile );
  CPPUNIT_TEST( testUnmarshalRemoteModifications );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testMarshalChannel( void );
  void testUnmarshalChannel( void );
  void testUnmarshalChannelProfile( void );
  void testUnmarshalRemoteModifications( void );
  
private:

  unique_ptr<JsonMarshaller>  marshaller_;

};

#endif // __CHANNEL_JSON_TRANSFORMATION_UNIT_TEST_H__
