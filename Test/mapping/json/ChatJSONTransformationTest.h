/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __MEMBER_JSON_TRANSFORMATION_UNIT_TEST_H__
#define __MEMBER_JSON_TRANSFORMATION_UNIT_TEST_H__

#include "Declarations.h"
#include "BaseChatTransformationTest.h"

/*
 * Ejin unit tests
 */
class ChatJsonTransformationTest : public BaseChatTransformationTest
{
public:
  
  void setUp( void );
  void tearDown( void );
  
  // ==========
  // Register Tests
  // ==========
  CPPUNIT_TEST_SUITE( ChatJsonTransformationTest );
  CPPUNIT_TEST( testUnmarshalChats );
  CPPUNIT_TEST( testUnmarshalMessageResult );
  CPPUNIT_TEST( testUnmarshalMessage );
  CPPUNIT_TEST( testMarshalNewMessages );
  CPPUNIT_TEST( testMarshalNewChat );
  CPPUNIT_TEST_SUITE_END();
  
  // ==========
  // Test methods
  // ==========
public:
  
  void testUnmarshalChats( void );
  void testUnmarshalMessageResult( void );
  void testUnmarshalMessage( void );
  void testMarshalNewMessages( void );
  void testMarshalNewChat( void );
  
private:
  
  unique_ptr<JsonMarshaller>  marshaller_;
  
};

#endif // __MEMBER_JSON_TRANSFORMATION_UNIT_TEST_H__
