/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "DBUnit.h"

#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>
#include <cctype>
#include <cwctype>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/param.h>
#include <unistd.h>
#include <dirent.h>

#include "SqliteValue.h"
#include "SqliteBaseEntity.h"
#include "SqliteResultSet.h"

std::string TestResourcePath = "./Test/resources/";

#if defined(WIN32) || defined(_WIN32)
#define PATH_SEPARATOR "\\"
#elseif defined(__APPLE__)
#define PATH_SEPARATOR "/"
#else
#define PATH_SEPARATOR "/"
#endif

#define TABLE_ORDERING_FILE "table-ordering.txt"

inline wchar_t char_to_wide( char w )
{
  return wchar_t(w);
}
inline char wide_to_narrow( wchar_t w )
{
  // simple typecast
  // works because UNICODE incorporates ASCII into itself
  return char(w);
}
inline char* trim( char *s ) {
  char *ptr;
  if (!s)
    return NULL;   // handle NULL string
  if (!*s)
    return s;      // handle empty string
  // remove ending spaces
  for (ptr = s + strlen(s) - 1; (ptr >= s) && isspace(*ptr); --ptr);
  ptr[1] = '\0';
  // skip beginning spaces
  for (ptr = s; isspace(*ptr); ++ptr);
  return ptr;
}
inline bool caseInsCharCompareN( char a, char b ) {
  return(toupper(a) == toupper(b));
}
inline bool caseInsCompare( const string& s1, const string& s2 ) {
  return((s1.size( ) == s2.size( )) &&
         equal(s1.begin( ), s1.end( ), s2.begin( ), caseInsCharCompareN));
}
const string NULL_VALUE = "NULL";

int DBUnit::setupTables( const string& srcDir, bool clean ) 
{
  DIR *dp;
  struct dirent *ep;   
  list<string> filesInDir;
  char tableOrderingFile[MAXPATHLEN+1];  
  tableOrderingFile[0] = '\0';
  if ( (dp=opendir(srcDir.c_str())) )
  {    
    char absPath[MAXPATHLEN+1];
    while ((ep = readdir (dp))) {
      if (ep->d_type == DT_REG) {
        strcpy( absPath, srcDir.c_str() );
        strcat( absPath, PATH_SEPARATOR );
        strcat( absPath, ep->d_name);  
        
        if (strcmp(TABLE_ORDERING_FILE, ep->d_name) == 0)
          strcpy( tableOrderingFile, absPath );
        
        // remember all files found in directory
        filesInDir.push_back(absPath);
      }
    }
    closedir(dp);
    
  } else {
    ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Invalid configuration directory: %s", srcDir.c_str());
  }
  
  // open table ordering definition
  if (tableOrderingFile[0] == '\0') {
    ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Cannot find table ordering file: %s", TABLE_ORDERING_FILE);
  }
  
  std::ifstream in(tableOrderingFile);  
  if(!in) {
    ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Cannot open table ordering file: %s", tableOrderingFile);
  }

  // first look for all tables names in the file
  list< string > tableNames;
  char line[255];
  char* table = NULL;
  while ( in.getline(line, 255) ) {
    table = trim(line);
    if (table && *table) {
      // resolve entity prototyp
      string tablename(table);
      // convert to upper case
      transform(tablename.begin(),tablename.end(),tablename.begin(),::toupper);
      string wtablename;  
      wtablename.resize(tablename.length());
      // convert to wchar
      transform(tablename.begin(), tablename.end(), wtablename.begin(), char_to_wide);
      tableNames.push_back(wtablename);
    }
  }

  if (clean) {
    // remove all old data from tables
    for (std::list<string>::reverse_iterator it = tableNames.rbegin(); it != tableNames.rend(); ++it) {
      const BaseEntity& prototype = this->_db.prototype(it->c_str());
      this->_db.removeAll(it->c_str(), prototype.mainSchemaName());
    }
  }
  
  // insert new data records
  int counter = 0;
  for (list<string>::iterator it=tableNames.begin() ; it != tableNames.end(); it++ ) {
    if (insertRecords(srcDir.c_str(), it->c_str()) < 0) {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "CSV input file %s.csv is missing in directory %s.", (table?table:"<undefined"), srcDir.c_str());
    }
    counter++;
  }
  
  return counter;
}

int DBUnit::insertRecords( const char dir[], const string& tablename )
{ 
  // open csv file  
  string filename(dir);
  filename.append("/").append(tablename).append(".csv");
  std::ifstream in(filename.c_str());  
  if(!in) {
    return -1;
  }  
  
  const BaseEntity& prototype = this->_db.prototype(tablename.c_str());
  
  list<const Attribute*> header;
  char line[255];
  in.getline(line, 255);
  if(in && strlen(line) > 0) {
    header = insertEntity(prototype, line);
  } else {
    perror ("No header found in cvs table definition file.");
    in.close();  
    return -1;
  }
  
  // insert rows one by one
  int counter = 0;
  while(in) {
    in.getline(line, 255);
    if(in && strlen(line) > 0) {
      insertEntity(prototype, header, line);
      counter++;
    }
  }  
  
  in.close();  
  return counter;
}
list<const Attribute*> DBUnit::insertEntity( const BaseEntity& prototype, string header )
{
  string column;
  list<const Attribute*> attributes;
  while (header.find(",", 0) != string::npos)
  { 
    size_t  pos = header.find(",", 0);
    column = header.substr(0, pos);
    header.erase(0, pos + 1); 
    const Attribute* attr = prototype.byName(column);    
    attributes.push_back(attr);
  }
  // last column
  if (! header.empty()) {
    attributes.push_back(prototype.byName(header));
  }
  return attributes;
}

void DBUnit::insertEntity( const BaseEntity& prototype, list<const Attribute*> attributes, string line )
{
  unique_ptr<BaseEntity> entity(prototype.clone());
  
  string column;
  list<const Attribute*>::iterator it;
  for (it=attributes.begin() ; it != attributes.end() && line.find(",", 0) != string::npos; it++ ) {
    size_t  pos = line.find(",", 0);
    column = line.substr(0, pos);
    line.erase(0, pos + 1);
    if (caseInsCompare(column, NULL_VALUE)) {
      entity->setNull((*it)->index());
    } else {
      entity->asString((*it)->index(), column);    
    }
  }
  // remember last column
  if (! line.empty() && it != attributes.end()) {
    if (caseInsCompare(line, NULL_VALUE)) {
      entity->setNull((*it)->index());
    } else {
      entity->asString((*it)->index(), line);
    }
  }
  
  // create
  this->_db.insert(*entity, prototype.mainSchemaName());
}

