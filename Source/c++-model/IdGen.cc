/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "IdGen.h"
#include "UsingEjinTypes.h"

#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<IdGen>::TABLE_FIELDS[] =
  {
    Attribute("GEN_KEY", type_text, flag_primary_key),
    Attribute("GEN_VALUE", type_int, flag_not_null),
    Attribute()
  };
  template <> const AttributeSet DataSchema<IdGen>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  IdGen::IdGen( void ): BaseEntity(TABLE_DDL, false)
  {
    this->clearGenKey( );
    this->setGenValue( 0 );
  }
  IdGen::IdGen( const string& key, integer val ): BaseEntity(TABLE_DDL, false)
  {
    this->setGenKey( key );
    this->setGenValue( val );
  }
  IdGen::IdGen( const IdGen& record ): BaseEntity(record)
  {
    this->operator=(record);
  }
  IdGen::~IdGen( void )
  { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  IdGen& IdGen::operator=( const IdGen& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, GenKey);
      COPY_PROPERTY((*this), rhs, GenValue);
    }
    return *this;
  }
  
  // comparison
  bool IdGen::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const IdGen& mrhs = dynamic_cast<const IdGen&> (rhs);
    return getGenKey() == mrhs.getGenKey();
  }
  bool IdGen::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const IdGen& mrhs = dynamic_cast<const IdGen&> (rhs); // throws std::bad_cast if not of type Member&
    return getGenKey() < mrhs.getGenKey();
  }
  
  const char* IdGen::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& IdGen::tableFields( void ) const { return TABLE_DDL; }
  
}
