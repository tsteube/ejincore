/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __STRING_TEMPLATE2_H__
#define __STRING_TEMPLATE2_H__

#include "Declarations.h"

namespace ejin
{
  
  /**
   * Helper class to unmarshal XML using expat.
   */
  class StringTemplate2
  {
    
    // ==========
    // CTOR
    // ==========
  public:
    
    StringTemplate2( void ) {}
    ~StringTemplate2( void ) {}
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    static void setupTemplateLib( );
    static void teardownTemplateLib( void );

    /**
     * Expand template using the channel container data.
     */
    void expand(const string& tpl,
                const string& styles,
                const string& scripts,
                const string& resources,
                Channel* container,
                string& output,
                const string& languageCode,
                const string& username,
                bool readonly );

    /**
     * Expand template using the chat container data.
     */
    void expand(const string& tpl,
                const string& styles,
                const string& scripts,
                const string& resources,
                const Chat& chat,
                string& output,
                const string& languageCode,
                const string& username );
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // disable copy ctor
    StringTemplate2( const StringTemplate2& record );
    // disable assignment
    StringTemplate2& operator=( const StringTemplate2& rhs );
    // disable comparision
    bool operator==( const StringTemplate2& rhs ) const;
    
  };
  
}

#endif // __STRING_TEMPLATE2_H__
