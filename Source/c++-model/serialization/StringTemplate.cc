/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "StringTemplate.h"

#include "Channel.h"
#include "ChannelTemplate.h"

#include "Chat.h"
#include "ChatTemplate.h"

#define U_DISABLE_RENAMING 1

// ignore warning from ctemplate/template.h
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wshorten-64-to-32"
#include <ctemplate/template.h>

#include "CoreFoundation/CoreFoundation.h"

// ===========================
// Class Definition
// ===========================

namespace ejin
{
  // ==========
  // Public Interface
  // ==========

  void StringTemplate::setupTemplateLib( )
  {
  }
 
  void StringTemplate::teardownTemplateLib( )
  {
  }

  void StringTemplate::expand(const string& tpl,
                              const string& styles,
                              const string& scripts,
                              const string& resources,
                              const Chat& chat,
                              string& output,
                              const string& languageCode,
                              const string& username )
  {
    TemplateDictionary dict(tpl);
    if ( ! styles.empty() ) {
      dict.AddIncludeDictionary("STYLES")->SetFilename( styles );
    }
    if ( ! scripts.empty() ) {
      dict.AddIncludeDictionary("SCRIPTS")->SetFilename( scripts );
    }
    
    ChatTemplate(chat, username).putAll( dict );
    ctemplate::ExpandTemplate(tpl, ctemplate::DO_NOT_STRIP, &dict, &output);
  }
  
  void StringTemplate::expand(const string& tpl,
                              const string& styles,
                              const string& scripts,
                              const string& resources,
                              Channel* container,
                              string& output,
                              const string& languageCode,
                              const string& username,
                              bool readonly )
  {
    assert( container );
    assert( container->getHeader() );
    
    TemplateDictionary dict( tpl );
    if ( ! styles.empty() ) {
      dict.AddIncludeDictionary("STYLES")->SetFilename( styles );
    }
    if ( ! scripts.empty() ) {
      dict.AddIncludeDictionary("SCRIPTS")->SetFilename( scripts );
    }
    
    ChannelTemplate(container, username, readonly).putAll( dict );
    ctemplate::ExpandTemplate(tpl, ctemplate::DO_NOT_STRIP, &dict, &output);
#if TARGET_OS_SIMULATOR
    // to debug the template always reload from file
    ctemplate::Template::ClearCache();
#endif
  }
  
}


