/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __XML_ATTRIBUTE_SET_H__
#define __XML_ATTRIBUTE_SET_H__

#include "Declarations.h"

namespace ser
{
  class XmlAttributeSet
  {
    
    // ==========
    // CTOR
    // ==========
  public:
    
    XmlAttributeSet( XmlAttribute* definition );
    XmlAttributeSet( vector<XmlAttribute>& definition );
    XmlAttributeSet( const XmlAttributeSet& source );
    
    bool operator==( const XmlAttributeSet& rhs ) const;
    bool operator!=( const XmlAttributeSet& rhs ) const {
      return ! (*this == rhs);
    }
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    const string toString( void ) const;
    int count( void ) const
    { return (int) this->vec_.size(); }    
    const XmlAttribute* byName( string name ) const
    { return this->map_[name]; }
    const XmlAttribute* byIndex( int index ) const
    {
      if ((index >= 0) && (index < count())) {
        const XmlAttribute* a = &this->vec_[index];
        return a;
      }
      return NULL;
    }    
    
    // ==========
    // Private Interface
    // ==========    
  private:
    
    vector<XmlAttribute> vec_;
    mutable map<string, XmlAttribute*> map_;
    
    void copy( const vector<XmlAttribute>& definition );
    
  };
  
}
#endif // __XML_ATTRIBUTE_SET_H__