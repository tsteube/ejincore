/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "XmlAttributeSet.h"

#include "XmlAttribute.h"

namespace ser
{
  // ==========
  // CTOR
  // ==========
  
  XmlAttributeSet::XmlAttributeSet( XmlAttribute* definition ): vec_(), map_()
  {
    //make fields map from array
    if (definition)
    {
      for (int index=0; definition[index]; index++) {
        this->vec_.push_back( definition[index] );
        this->map_[definition[index]._name] = &definition[index];
      }
    }
  }
  XmlAttributeSet::XmlAttributeSet( vector<XmlAttribute>& definition ): vec_(), map_()
  {
    copy(definition);
  }  
  XmlAttributeSet::XmlAttributeSet( const XmlAttributeSet& source ): vec_(), map_()
  {
    copy(source.vec_);
  }  
  bool XmlAttributeSet::operator==( const XmlAttributeSet& rhs ) const
  {
    if (this == &rhs)
      return true;
    return this->vec_ == rhs.vec_;
  }  
  
  // ==========
  // Public Interface
  // ==========
  
  const string XmlAttributeSet::toString( void ) const
  {
    string s;
    for (int index = 0; index < count(); index++)
    {
      if (const XmlAttribute* f = byIndex(index))
      {
        s += f->name();
        if (index < (count() - 1))
          s += ", ";
      }
    }
    
    return s;
  }
  
  // ==========
  // Private Interface
  // ==========    
  
  void XmlAttributeSet::copy( const vector<XmlAttribute>& definition )
  {
    this->vec_ = definition;
    
    //make fields map from vector
    for (int index = 0; index < (int)vec_.size(); index++)
    {
      XmlAttribute& field = this->vec_[index];
      this->map_[field.name()] = &field;
    }
  }
  
}

