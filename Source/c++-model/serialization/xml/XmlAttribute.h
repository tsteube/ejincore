/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __XML_ATTRIBUTE_H__
#define __XML_ATTRIBUTE_H__

#include "Declarations.h"

/**
 * SQL attribute class for SQLite.
 */
namespace ser
{
  // rename to Node
  class XmlAttribute
  {
    enum node_type {
      ATTRIBUTE,
      ELEMENT
    };
    
  private:
    friend class XmlAttributeSet;
    
    // ==========
    // CTOR
    // ==========
  public:
    
    XmlAttribute( void ): _name(), _row(), _property(), _type(ELEMENT)  { }
    XmlAttribute( string nam, string col ): _name(nam), _row(col), _property(), _type(ELEMENT)  { }
    XmlAttribute( const XmlAttribute& value ): _name(value._name), _row(value._row), _property(value._property), _type(value._type) { }
    
    bool operator==( const XmlAttribute& rhs ) const
    {
      if (this == &rhs)
        return true;
      
      if (this->_name != rhs._name)
        return false;
      if (this->_row != rhs._row)
        return false;
      
      return true;
    }  
    bool operator!=( const XmlAttribute& rhs ) const {
      return ! (*this == rhs);
    }
    operator bool() const
    { return ! this->_name.empty(); }

    // ==========
    // Public Interface
    // ==========
  public:    
    
    const string name( void ) const
    { return this->_name; }
    
    const string row( void ) const
    { return this->_row; }
    
    // ==========
    // Private Interface
    // ==========    
  private:
    
    string _name;
    string _row;
    string _property;
    node_type _type;
    
  };
  
}
#endif // __XML_ATTRIBUTE_H__
