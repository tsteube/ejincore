/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "JsonMarshaller.h"

#include "SerializableEntity.h"
#include <ThirdParty/json.hpp>

#include <sstream>
#include <iostream>
#include <iomanip>

// ===========================
// Class Definition
// ===========================

namespace ejin
{
  const std::string JsonMarshaller::MIME_TYPE = std::string("application/json");

  void JsonMarshaller::unmarshal( ser::ISerializableEntity& container, const char* data, size_t length,
                                 const char* aesKey, size_t aesKeyLength)
  {
    assert( data );
    assert( length>0 );
    
    stringstream ss;
    ss.write (data, length);
    nlohmann::json doc;
    ss >> doc;
    // read to the end
    /*
    char c;
    do {
      ss >> c;
      if (c == '\0' || c == EOF)
        break;
      if (isspace(c))
        continue;
      ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "JSON syntax error at column %d", ss.tellg());
    } while (true);
     */
    if (! container.unmarshalJson( doc )) {
      ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "Can't unmarshal JSON message");
    }
    
    /*
    json_error_t error;
    json_t* root = json_loadb(data, length, 0, &error);
    if (! root) {
      ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "JSON syntax error at column %d in %s", error.column, error.text);
    }
    if (! container.unmarshalJson( root )) {
      ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "Can't unmarshal JSON message %s", error.text);
    }
    json_decref( root );
    */
  }
  
  string JsonMarshaller::marshal( ser::ISerializableEntity& entity, bool pretty )
  {
    json doc;
    entity.marshalJson( doc );
    
    stringstream ss;
    //ss << std::setw(4) << root << std::endl;
    ss << doc << std::endl;
    return ss.str();
    /*
    json_t *root = json_object();
    if (! entity.marshalJson( root )) {
      ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "Can't marshal entity into JSON message");
    }
    char* dump = json_dumps( root, pretty ? (JSON_INDENT(2)|JSON_PRESERVE_ORDER|JSON_COMPACT) : JSON_PRESERVE_ORDER );
    string result( dump );
    free( dump );
    json_decref( root );
    return result;
    */
  }
  
}


