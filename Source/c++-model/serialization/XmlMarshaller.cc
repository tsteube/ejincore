/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "XmlMarshaller.h"

#include "UsingEjinTypes.h"

#include <expat/expat.h>

#include "Modifications.h"
#include "ResultList.h"
#include "Utilities.h"
#include "Channel.h"

#include "Member.h"

const int STACK_SIZE = 10;

typedef enum
{
  ENTITY_NODE,
  VALUE_NODE,
  NONE
} XmlNodeType;

struct XmlNode {
  XmlNode( void ): _pos(0), _type(NONE),  _ref(), _last() {
    memset(&this->_ref, 0, sizeof(this->_ref));
    memset(&this->_last, 0, sizeof(this->_last));
  }
  XmlNode( ser::ISerializableEntity* _entity ): _pos(0), _type(ENTITY_NODE), _ref(), _last() {
    memset(&this->_ref, 0, sizeof(this->_ref));
    memset(&this->_last, 0, sizeof(this->_last));
    this->_ref.entity = _entity;
  }
  XmlNode( sqlite::Value* _value ): _pos(0), _type(VALUE_NODE), _ref(), _last() {
    memset(&this->_ref, 0, sizeof(this->_ref));
    memset(&this->_last, 0, sizeof(this->_last));
    this->_ref.value = _value;
  }
  ~XmlNode( void ) {
    if (_last.el) free(_last.el);
  }
  
  // assignment
  XmlNode& operator=( const XmlNode& rhs ) {
    if (this != &rhs) {
      if (this->_last.el) free(_last.el);
      this->_last.el = rhs._last.el ? strdup(rhs._last.el) : NULL;
      this->_last.pos = rhs._last.pos;
      this->_ref = rhs._ref;
      this->_pos = rhs._pos;
      this->_type = rhs._type;
    }
    return *this;
  }
  
  XmlNodeType type( void ) const { return this->_type; }
  ser::ISerializableEntity* entity( void ) const { return (this->_type == ENTITY_NODE) ? this->_ref.entity : NULL; }
  sqlite::Value* value( void ) const { return (this->_type == VALUE_NODE) ? this->_ref.value : NULL; }
  
  void incr( void ) { this->_pos++; }
  int pos( void ) const { return this->_pos; }
  // here we analyse the number of identifical elements in a row
  // on the same level.
  // We must match elements in the source by position.
  int elmPos( const char* elementName ) {
    if (_last.el && elementName &&
        strcmp(elementName, _last.el) == 0) {
      _last.pos++;
    } else {
      if (_last.el) free(_last.el);
      _last.el = strdup(elementName); // we need to keep for next iteration
      _last.pos = 0;
    }
    return _last.pos;
  }
private:
  // data
  int         _pos;
  XmlNodeType _type;
  union {
    // entity object
    ser::ISerializableEntity* entity;
    // value object
    sqlite::Value* value;
  } _ref;
  struct {
    // element name
    char* el;
    // position in the sequence of elements with the same name
    int   pos;
  } _last;
};

struct DomainParserContext {
public:
  DomainParserContext( ser::ISerializableEntity* entity, const char* const aesKey = NULL, size_t keyLength = 0 ):
  _pos(0), _aesKey(aesKey), _keyLength(keyLength)
  {
    this->_stack[this->_pos] = XmlNode(entity);
  }
  DomainParserContext( sqlite::Value* value, const char* const aesKey = NULL, size_t keyLength = 0 ):
  _pos(0), _aesKey(aesKey), _keyLength(keyLength)
  {
    this->_stack[this->_pos] = XmlNode(value);
  }
  ~DomainParserContext( void )
  {
  }
  XmlNode* top( void )
  { return (this->_pos >= 0 && this->_pos < STACK_SIZE) ? &this->_stack[this->_pos] : NULL;  }
  XmlNode* pop( void )
  { return (this->_pos > 0 && this->_pos <= STACK_SIZE) ? &_stack[this->_pos--] : NULL;  }
  bool push( XmlNode elem ) {
    if (this->_pos >= 0 && this->_pos+1 < STACK_SIZE) {
      if (this->_pos > 0)
        _stack[this->_pos].incr();
      _stack[++this->_pos] = elem;
      return true;
    } else {
      throw "stack overflow";
    }
  }
  
  XmlNode _stack[STACK_SIZE];
  int _pos;
  const char* const _aesKey;
  size_t _keyLength;
};

static void start_unmarshal_hndl( void *data, const char *el, const char **attr ) {
  DomainParserContext* ctx = (DomainParserContext*) data;
  XmlNode* cur = ctx->top();
  if (cur) {
    switch (cur->type()) {
      case ENTITY_NODE :
      {
        ser::ISerializableEntity* elm = cur->entity()->entityByXmlNode(el, cur->elmPos(el));
        if (elm) {
          // evaluate XML attributes
          for( int i=0; attr[i]; i+=2) {
            sqlite::Value* val = elm->valueByXmlNode(attr[i]);
            if (val) {
              val->bytea(attr[i+1], strlen(attr[i+1]));
            }
          }
          ctx->push(elm);
        } else {
          Value* value = cur->entity()->valueByXmlNode(el);
          if (value) {
            ctx->push(value);
          }
        }
      }
        break;
      case VALUE_NODE :
        throw "sub elements in simple value node";
      case NONE :
        break;
    }
  }
}  /* End of start_unmarshal_hndl */

static void end_unmarshal_hndl( void *data, const char *el ) {
  DomainParserContext* ctx = (DomainParserContext*) data;
  XmlNode* cur = ctx->top();
  if (cur) {
    // notify by node type
    switch (cur->type()) {
      case ENTITY_NODE :
        cur->entity()->endXmlNode(el);
        break;
      case VALUE_NODE :
      case NONE :
        break;
    }
    ctx->pop();
  }
}  /* End of end_unmarshal_hndl */

static void char_unmarshal_hndl( void *data, const char *txt, int txtlen ) {
  DomainParserContext* ctx = (DomainParserContext*) data;
  XmlNode* cur = ctx->top();
  if (cur) {
    switch (cur->type()) {
      case VALUE_NODE :
        if ( cur->value()->isNull() )
          cur->value()->bytea(txt, txtlen);
        else
          cur->value()->append(txt, txtlen);
        break;
      case ENTITY_NODE:
      case NONE :
        break;
    }
  }
}  /* End char_unmarshal_hndl */

// ===========================

int Depth;
bool Indent;
bool Opened;

static void XMLCALL
pretty_print_start( void *data, const char *el, const char **attr )
{
  std::ostream*	stream = (std::ostream*) data;
  
  if (Opened) {
    *stream << ">";
    *stream << std::endl;
  }
  for (int i = 0; i < Depth; i++)
    *stream << "  ";
  
  *stream << "<" << el;
  for (int i = 0; attr[i]; i += 2) {
    *stream << " " << attr[i] << "=\"" << attr[i+1] << "\"";
  }
  
  Depth++;
  Indent=false;
  Opened=true;
}

static void XMLCALL
pretty_print_end( void *data, const char *el )
{
  std::ostream*	stream = (std::ostream*) data;
  
  if (Opened)
    *stream << "/>";
  
  Depth--;
  if (Indent) {
    for (int i = 0; i < Depth; i++)
      *stream << "  ";
  } else {
    Indent = true;
  }
  
  if (! Opened)
    *stream << "</" << el << ">";
  
  *stream << std::endl;
  Opened=false;
}

static void XMLCALL
pretty_print_text( void *data, const XML_Char *txt, int txtlen )
{
  std::ostream*	stream = (std::ostream*) data;
  *stream << ">";
  stream->write(txt, txtlen);
  Indent=false;
  Opened=false;
}

static bool unmarshal( XML_Parser _parser, DomainParserContext* pctx, const char* data, size_t length )
{
  if (length > (size_t) std::numeric_limits<int>::max()) {
    ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "Data size overflow %ld", length);
  }
  
  XML_ParserReset(_parser, NULL);
  XML_SetElementHandler(_parser, start_unmarshal_hndl, end_unmarshal_hndl);
  XML_SetCharacterDataHandler(_parser, char_unmarshal_hndl);
  XML_SetUserData(_parser, pctx);
  
  return XML_Parse(_parser, data, static_cast<int>(length), 1);
}

// ===========================
// Class Definition
// ===========================

namespace ejin
{
  const std::string XmlMarshaller::MIME_TYPE = std::string("application/xml");

  // ==========
  // Ctor / Dtor
  // ==========
  
  XmlMarshaller::XmlMarshaller( void ): _parser(XML_ParserCreate(NULL))
  {
    if (! _parser)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Couldn't allocate memory for parser\n");
  }
  XmlMarshaller::~XmlMarshaller( void )
  {
    XML_ParserFree(_parser);
  }
  
  // ==========
  // Public Interface
  // ==========

  void XmlMarshaller::unmarshal( ISerializableEntity& container, const char* data, size_t length, const char* aesKey, size_t aesKeyLength )
  {
    unique_ptr<DomainParserContext> pctx = unique_ptr<DomainParserContext>(new DomainParserContext(&container));
    bool success = ::unmarshal(this->_parser, pctx.get(), data, length);
    if (! success) {
      ejin::_throw_exception(ejin::MAPPING_EXCEPTION, "Parse error at line %d:\n%s\n",
                             (int) XML_GetCurrentLineNumber(_parser),
                             XML_ErrorString(XML_GetErrorCode(_parser)));
    }
  }

  string XmlMarshaller::marshal( ISerializableEntity& entity, bool pretty )
  {
    stringstream buf;
    XmlStream xmlStream(buf);
    entity.marshalXml( xmlStream, NULL, 0 );
    string data( buf.str() );
    
    if (pretty) {
      stringstream buf2;
      
      XML_SetEncoding(_parser, "utf-8");
      XML_SetElementHandler(_parser, pretty_print_start, pretty_print_end);
      XML_SetCharacterDataHandler(_parser, pretty_print_text);
      XML_SetUserData(_parser, &buf2);
      
      buf2 << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << std::endl;
      if (! XML_Parse(_parser, data.c_str(), static_cast<int>(data.size()), 1)) {
        fprintf(stderr, "Parse error at line %d:\n%s\n",
                (int) XML_GetCurrentLineNumber(_parser),
                XML_ErrorString(XML_GetErrorCode(_parser)));
      }
      
      data = string(buf2.str());
    }
    
    return data;
  }

}

// ==========
// localize date/time output
// ==========

std::ostream& operator<<(std::ostream& os, const struct tm& date)
{
  typedef typename std::ostream::char_type          char_t;
  typedef typename std::ostream::traits_type        traits_t;
  typedef std::ostreambuf_iterator<char_t,traits_t> outIter_t;
  
  std::locale loc = os.getloc();
  const std::time_put<char_t,outIter_t>& fac =
  std::use_facet < std::time_put<char_t, outIter_t > > (loc);
  outIter_t nextpos = fac.put(os,os,os.fill(),&date,'x');
  if (nextpos.failed())
    os.setstate(std::ios_base::badbit);
  return os;
}


