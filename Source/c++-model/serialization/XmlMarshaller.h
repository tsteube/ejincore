/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __XML_MARSHALLER_H__
#define __XML_MARSHALLER_H__

#include "Declarations.h"
#include "SerializableEntity.h"

#include <expat/expat.h>

namespace ejin
{
  
  /**
   * Helper class to unmarshal XML using expat.
   */
  class XmlMarshaller: public ser::IRestMarshaller
  {
    
    // ==========
    // CTOR
    // ==========
  public:
    
    XmlMarshaller( void );
    ~XmlMarshaller( void );    
    
    // ==========
    // Public Interface
    // ==========
  public:

    static const std::string MIME_TYPE;
    string mimeType() { return MIME_TYPE; }

    void unmarshal( ser::ISerializableEntity& container, const char* data, size_t length,
                                        const char* aesKey = NULL, size_t aesKeyLength = 0);
    
    string marshal( ser::ISerializableEntity& entity, bool pretty = false );
    
    // ==========
    // Private Interface
    // ==========    
  private:
    
    // disable copy ctor
    XmlMarshaller( const XmlMarshaller& record );
    // disable assignment
    XmlMarshaller& operator=( const XmlMarshaller& rhs );
    // disable comparision
    bool operator==( const XmlMarshaller& rhs ) const;    
    
    // ----
    // parser instance
    XML_Parser _parser;
    
  };
  
}
std::ostream& operator<<(std::ostream& os, const struct tm& date);

#endif // __XML_REST_MARSHALLER_H__
