/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatTemplate.h"

#import <TemplateResources/LocalizationManager.h>

// ignore warning from ctemplate/template.h
#include <ctemplate/template.h>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <fstream>
#include <algorithm>

#include "Chat.h"
#include "Message.h"

#define SET_SECTION_VALUE( dict, buffer, variable) \
dict.SetValueAndShowSection( variable, buffer.str(), "HAS_" + string(variable)); \
buffer.clear();buffer.str("");

#define SET_VALUE( dict, buffer, variable) \
dict.SetValue( variable, buffer.str()); \
buffer.clear();buffer.str("");

#define TEXT( buffer, key ) \
buffer << LocalizationManager::getInstance()->formatString(key);

#define DATE( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatDate(jtime.time());

#define TIME( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatTime(jtime.time());

#define DATETIME( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatDateTime(jtime.time());

static std::string replace_string(std::string subject,
                                  const std::string& search,
                                  const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
  return subject;
}

namespace ejin
{
  ChatTemplate::ChatTemplate( const Chat& chat, const string& username ):
    _chat(chat), _username(username) /*, _locale("en_US.UTF-8")*/
  {
  }

  // ==========
  // Public Interface
  // ==========
  
  TemplateDictionary& ChatTemplate::putAll( TemplateDictionary& dict )
  {
    std::ostringstream buffer;
    buffer.imbue(_locale);
    
    Message* message, *previous, *next;
    for (auto it = _chat.getMessages().begin(); it != _chat.getMessages().end(); ++it ) {
      message = it->get();
      auto pit = std::prev(it);
      previous = (pit != _chat.getMessages().end()) ? pit->get() : NULL;
      auto nit = std::next(it);
      next = (nit != _chat.getMessages().end()) ? nit->get() : NULL;
      putMessage( *dict.AddSectionDictionary("MESSAGE"), previous, message, next );
    }
    return dict;
  }
  
  // --------------
  // Helper Methods

  void ChatTemplate::putMessage( TemplateDictionary& dict, Message* previous, Message* message, Message* next ) const
  {
    //UnicodeString msg;
    //UErrorCode status = U_ZERO_ERROR;
    std::ostringstream buffer;
    buffer.imbue(_locale);

    //Formattable args[1];
    //args[0] = Formattable( "test" );

    if ( message->isEncrypted() ) {
      TEXT( buffer, "chat.unencrypted" );
      SET_VALUE( dict, buffer, "CONTENT" );
    } else {
      dict.SetValue("CONTENT", replace_string(message->getContent(), "\n", "<br/>") );
    }
    /*
    if ( message->hasNo() && message->getNo() > 0 ) {
      dict.SetIntValue("NO", message->getNo() );
    }
    */
    dict.SetIntValue("NO", message->getId() );

    if ( previous == NULL ||
        message->getSendTime2().truncToDay() != previous->getSendTime2().truncToDay()) {
      // new day
      DATE( buffer, message->getSendTime2() );
      SET_SECTION_VALUE( dict, buffer, "DELIMITER");
    }
    
    if ( next == NULL ||
        message->isMyMessage() != next->isMyMessage() ||
        message->getSendTime2().truncToDay() != next->getSendTime2().truncToDay() ||
        message->isDelivered() != next->isDelivered()
        ) {
      // show caption only on last message in the row from the same sender
      dict.ShowSection("HAS_CAPTION");
    }
    
    // meta data
    if ( message->isMyMessage() ) {
      dict.ShowSection("MESSAGE_FROM_ME");
      if ( message->isSend() || message->isPending() ) {
        if ( !message->isSecure() && !message->isPending() ) {
          TEXT( buffer, "chat.send_unencrypted" );
          SET_SECTION_VALUE( dict, buffer, "ERROR_CAPTION" );
        }
        if ( message->hasSendTime2() ) {
          TIME( buffer, message->getSendTime2() );
        }
        if ( message->isDelivered() ) {
          buffer << " &#x2713;";
        }

        SET_VALUE( dict, buffer, "CAPTION" );
      } else {
        TEXT( buffer, "chat.send_error" );
        SET_SECTION_VALUE( dict, buffer, "ERROR_CAPTION" );
      }
    } else {
      dict.ShowSection("MESSAGE_FROM_THEM");
      if ( message->isEncrypted() ) {
        TEXT( buffer, "chat.encrypted_error" );
        SET_SECTION_VALUE( dict, buffer, "ERROR_CAPTION" );
      } else {
        if ( !message->isSecure() ) {
          TEXT( buffer, "chat.send_unencrypted" );
          SET_SECTION_VALUE( dict, buffer, "ERROR_CAPTION" );
        }
        if ( message->hasSendTime2() ) {
          TIME( buffer, message->getSendTime2() );
        }
        SET_VALUE( dict, buffer, "CAPTION" );
      }
    }
  }
}
