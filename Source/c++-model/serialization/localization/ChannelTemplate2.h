/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_TEMPLATE2_H__
#define __CHANNEL_TEMPLATE2_H__

#include "Declarations.h"
#import "json.hpp"

using nlohmann::json;

namespace ejin
{
  /**
   * Puts content and meta data of the channel into a dictionary for later template processing
   */
  class ChannelTemplate2
  {
  public:
    
    ChannelTemplate2( Channel* channel,
                    const string& username,
                    bool readonly );
    ~ChannelTemplate2( void );
    
    // ==========
    // Public Interface
    // ==========
    
    /**
     * Export channel data to the template directory
     */
    json& putAll( json& dict );
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // disable copy ctor
    ChannelTemplate2( const ChannelTemplate2& record );
    // disable assignment
    ChannelTemplate2& operator=( const ChannelTemplate2& rhs );
    // disable comparision
    bool operator==( const ChannelTemplate2& rhs ) const;
    
    // helper methods
    void putHeader( json& dict, ChannelHeader& header );
    void putPost( json& dict, ChannelPost& post, std::ostringstream& buffer );
    void putHeaderComment( json& dict, ChannelComment& comment, std::ostringstream& buffer );
    void putPostComment( json& dict, ChannelComment& comment, std::ostringstream& buffer );
    void putMembers( json& dict, ChannelHeader& header, std::ostringstream& buffer );
    void putResources(json& dict,
                      list< shared_ptr< Resource > >& resources,
                      const string& prefix,
                      std::ostringstream& buffer );
    template<class T> void putEntity( json& dict,
                                     T& entity,
                                     const string& prefix,
                                     std::ostringstream& buffer);
    template<class T> void putMetaDataOfEntity(json& dict,
                                               T& entity,
                                               const string& prefix,
                                               integer modifications,
                                               std::ostringstream& buffer);
    void putMetaDataOfContainer(json& dict,
                                ChannelHeader& channelHeader,
                                const string& prefix,
                                std::ostringstream& buffer);
    
    template<class T> MemberRole getAccessRole( T& entity, const string& username );
    MemberRole getRole( ChannelMember& entity, const string& username );

    // ----
    Channel*                   _channel;
    locale                     _locale;
    string                     _username;
    bool                       _readonly;
    bool                       _invalidSessionKey;
  };
  
}

#endif // __CHANNEL_TEMPLATE2_H__
