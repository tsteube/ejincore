/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHAT_TEMPLATE2_H__
#define __CHAT_TEMPLATE2_H__

#include "Declarations.h"
#import "json.hpp"

using nlohmann::json;

namespace ejin
{
  /**
   * Puts content and meta data of the channel into a dictionary for later template processing
   */
  class ChatTemplate2
  {
  public:
    
    ChatTemplate2( const Chat& chat, const string& username );
    ~ChatTemplate2( void ) {};
    
    // ==========
    // Public Interface
    // ==========

    /**
     * Export channel data to the template directory
     */
    json& putAll( json& dict );

    // ==========
    // Private Interface
    // ==========    
  private:
    
    // disable copy ctor
    ChatTemplate2( const ChatTemplate2& record );
    // disable assignment
    ChatTemplate2& operator=( const ChatTemplate2& rhs );
    // disable comparision
    bool operator==( const ChatTemplate2& rhs ) const;
    
    // helper methods
    void putMessage( json& model, Message* previous, Message* message, Message* next ) const;
    string getString( const char* key ) const;

    // ----
    const Chat&                _chat;
    string                     _username;
    locale                     _locale;
  };
  
}

#endif // __CHAT_TEMPLATE2_H__
