/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHANNEL_TEMPLATE_H__
#define __CHANNEL_TEMPLATE_H__

#include "Declarations.h"

// forward declaration
namespace ctemplate {
  class TemplateDictionary;
}
using ctemplate::TemplateDictionary;

namespace ejin
{
  /**
   * Puts content and meta data of the channel into a dictionary for later template processing
   */
  class ChannelTemplate
  {
  public:
    
    ChannelTemplate( Channel* channel,
                    const string& username,
                    bool readonly );
    ~ChannelTemplate( void );
    
    // ==========
    // Public Interface
    // ==========
    
    /**
     * Export channel data to the template directory
     */
    TemplateDictionary& putAll( TemplateDictionary& dict );
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // disable copy ctor
    ChannelTemplate( const ChannelTemplate& record );
    // disable assignment
    ChannelTemplate& operator=( const ChannelTemplate& rhs );
    // disable comparision
    bool operator==( const ChannelTemplate& rhs ) const;
    
    // helper methods
    void putHeader( TemplateDictionary& dict, ChannelHeader& header );
    void putPost( TemplateDictionary& dict, ChannelPost& post, std::ostringstream& buffer );
    void putHeaderComment( TemplateDictionary& dict, ChannelComment& comment, std::ostringstream& buffer );
    void putPostComment( TemplateDictionary& dict, ChannelComment& comment, std::ostringstream& buffer );
    void putMembers( TemplateDictionary& dict, ChannelHeader& header, std::ostringstream& buffer );
    void putResources(TemplateDictionary& dict,
                      list< shared_ptr< Resource > >& resources,
                      const string& prefix,
                      std::ostringstream& buffer );
    template<class T> void putEntity( TemplateDictionary& dict,
                                     T& entity,
                                     const string& prefix,
                                     std::ostringstream& buffer);
    template<class T> void putMetaDataOfEntity(TemplateDictionary& dict,
                                               T& entity,
                                               const string& prefix,
                                               int modifications,
                                               std::ostringstream& buffer);
    void putMetaDataOfContainer(TemplateDictionary& dict,
                                ChannelHeader& channelHeader,
                                const string& prefix,
                                std::ostringstream& buffer);
    
    template<class T> MemberRole getAccessRole( T& entity, const string& username );
    MemberRole getRole( ChannelMember& entity, const string& username );

    // ----
    Channel*                   _channel;
    locale                     _locale;
    string                     _username;
    bool                       _readonly;
    bool                       _invalidSessionKey;
  };
  
}

#endif // __CHANNEL_TEMPLATE_H__
