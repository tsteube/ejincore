/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __CHAT_TEMPLATE_H__
#define __CHAT_TEMPLATE_H__

#include "Declarations.h"

// forward declaration
namespace ctemplate {
  class TemplateDictionary;
}
using ctemplate::TemplateDictionary;

namespace ejin
{
  /**
   * Puts content and meta data of the channel into a dictionary for later template processing
   */
  class ChatTemplate
  {
  public:
    
    ChatTemplate( const Chat& chat, const string& username );
    ~ChatTemplate( void ) {};
    
    // ==========
    // Public Interface
    // ==========

    /**
     * Export channel data to the template directory
     */
    TemplateDictionary& putAll( TemplateDictionary& dict );
    
    // ==========
    // Private Interface
    // ==========    
  private:
    
    // disable copy ctor
    ChatTemplate( const ChatTemplate& record );
    // disable assignment
    ChatTemplate& operator=( const ChatTemplate& rhs );
    // disable comparision
    bool operator==( const ChatTemplate& rhs ) const;
    
    // helper methods
    void putMessage( TemplateDictionary& dict, Message* previous, Message* message, Message* next ) const;
    string getString( const char* key ) const;

    // ----
    const Chat&                _chat;
    string                     _username;
    locale                     _locale;
  };
  
}

#endif // __CHAT_TEMPLATE_H__
