/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelTemplate2.h"

#import <TemplateResources/LocalizationManager.h>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <fstream>
#include <algorithm>
#include <time.h>

#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelComment.h"
#include "ChannelPost.h"
#include "Media.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "Resource.h"
#include "SqliteValue.h"

#define TIME_BUFFER_SIZE 256

#define SHOW_SECTION( model, variable ) \
model[variable] = true; \

#define SET_SECTION_VALUE( model, buffer, variable) \
model["HAS_" + string(variable)] = true; \
model[variable] = buffer.str(); \
buffer.clear();buffer.str("");

#define SET_VALUE( model, buffer, variable) \
model[variable] = buffer.str(); \
buffer.clear();buffer.str("");

#define TEXT( buffer, key ) \
buffer << LocalizationManager::getInstance()->formatString(key);

#define FILE_EXTENSION_OF( mimeType ) \
LocalizationManager::getInstance()->fileExtensionOfMimeType(mimeType)

#define DATE( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatDate(jtime.time());

#define TIME( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatTime(jtime.time());

#define DATETIME( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatDateTime(jtime.time());

#define ISO_8601( buffer, jtime ) \
buffer << LocalizationManager::getInstance()->formatISO8601Time(jtime.time());

/*
const char * const kImageMimeTypeWindowsBitmapFormat            = "image/bmp";
const char * const kImageMimeTypeTaggedImageFileFormat          = "image/tiff";
const char * const kImageMimeTypeJointPhotographicExpertsGroup  = "image/jpeg";
const char * const kImageMimeTypeGraphicInterchangeFormat       = "image/gif";
const char * const kImageMimeTypePortableNetworkGraphic         = "image/png";
const char * const kImageMimeTypeXWindowsBitmap                 = "image/xbm";
static bool isImageType( std::string mimeType )
{
  if (! mimeType.empty()) {
    const char* type = mimeType.c_str();
    return (strcasecmp(type, kImageMimeTypeWindowsBitmapFormat) == 0 ||
            strcasecmp(type, kImageMimeTypeTaggedImageFileFormat) == 0 ||
            strcasecmp(type, kImageMimeTypeJointPhotographicExpertsGroup) == 0 ||
            strcasecmp(type, kImageMimeTypeGraphicInterchangeFormat) == 0 ||
            strcasecmp(type, kImageMimeTypePortableNetworkGraphic) == 0 ||
            strcasecmp(type, kImageMimeTypeXWindowsBitmap) == 0);
  }
  return false;
}
*/
static char* readable_fs(double size/*in bytes*/, char *buf) {
  int i = 0;
  const char* units[] = {"B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
  while (size > 1024) {
    size /= 1024;
    i++;
  }
  sprintf(buf, "[%.*f %s]", i, size, units[i]);
  return buf;
}
static std::string replace_string(std::string subject,
                                  const std::string& search,
                                  const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
  return subject;
}

namespace ejin
{
  ChannelTemplate2::ChannelTemplate2( Channel* channel,
                                   const string& username,
                                   bool readonly ):
  _channel(channel), _username(username) /*, _locale("en_US.UTF-8")*/
  {
    assert( _channel );
    assert( _channel->getHeader() );
    _invalidSessionKey = _channel->getHeader()->isInvalidSessionKey();
    _readonly = ( readonly ||
                 _channel->getHeader()->getSyncInd() == ejin::kSyncIndDelete ||
                 _channel->getHeader()->getMembership() == ejin::kRejetMemberIndShip ||
                 _channel->getHeader()->isEncrypted() ||
                 _invalidSessionKey );
  }
  ChannelTemplate2::~ChannelTemplate2( void )
  {  };

  // ==========
  // Public Interface
  // ==========
  
  json& ChannelTemplate2::putAll( json& dict )
  {
    assert( _channel );
    assert( _channel->getHeader() );
   
    std::ostringstream buffer;
    buffer.imbue(_locale);

    LocalizationManager* lm = LocalizationManager::getInstance();

    string at = lm->formatDate(time(0));
    buffer << lm->formatString("entity.exported", at);
    SET_VALUE( dict, buffer, "CHANNEL_EXPORTED" );

    buffer << lm->formatString("label.attachment_name", at);
    SET_VALUE( dict, buffer, "ATTACHMENT_LABEL" );
    buffer << lm->formatString("label.type", at);
    SET_VALUE( dict, buffer, "ATTACHMENT_TYPE" );
    buffer << lm->formatString("label.size", at);
    SET_VALUE( dict, buffer, "ATTACHMENT_SIZE" );

    putHeader( dict, *_channel->getHeader() );
    
    if ( _invalidSessionKey ) {
      TEXT( buffer, "channel.locked" );
      SET_SECTION_VALUE( dict, buffer, "INVALID_SESSION_KEY" );
      TEXT( buffer, "channel.invalid_key" );
      SET_VALUE( dict, buffer, "INVALID_SESSION_KEY_TEXT1" );
      TEXT( buffer, "channel.replace_key" );
      SET_VALUE( dict, buffer, "INVALID_SESSION_KEY_TEXT2" );
    }

    // sort for output
    _channel->getComments().sort([](const shared_ptr<ChannelComment> & rhs, const shared_ptr<ChannelComment> & lhs) {
      if (! rhs->hasCreateTime())
        return true;
      if (! lhs->hasCreateTime())
        return false;
      return rhs->getCreateTime() > lhs->getCreateTime();
    });
    json array = json::array();
    for (list< shared_ptr<ChannelComment> >::iterator it = _channel->getComments().begin();
         it != _channel->getComments().end(); it++ ) {
      json sub_dict = json::object();
      putHeaderComment( sub_dict, *it->get(), buffer );
      array.push_back(sub_dict);
    }
    dict["HEADER_COMMENT"] = array;
    // sort for output
    _channel->getPosts().sort([](const shared_ptr<ChannelPost> & rhs, const shared_ptr<ChannelPost> & lhs) {
      if (! rhs->hasCreateTime())
        return true;
      if (! lhs->hasCreateTime())
        return false;
      return rhs->getCreateTime() > lhs->getCreateTime();
    });
    array = json::array();
    for (list< shared_ptr<ChannelPost> >::iterator it = _channel->getPosts().begin();
         it != _channel->getPosts().end(); it++ ) {
      json sub_dict = json::object();
      putPost( sub_dict, *it->get(), buffer );
      array.push_back(sub_dict);
    }
    dict["POST"] = array;
    return dict;
  }
  // --------------
  // Helper Methods
  
  void ChannelTemplate2::putHeader( json& dict, ChannelHeader& channelHeader )
  {
    LocalizationManager* lm = LocalizationManager::getInstance();
    vector<string> params;
    std::ostringstream buffer;
    buffer.imbue(_locale);

    if ( channelHeader.isEncrypted() ) {
      TEXT( buffer, "encrypted" );
      SET_VALUE( dict, buffer, "CHANNEL_NAME" );
    } else {
      dict["CHANNEL_NAME"] = channelHeader.getName();
    }
    
    if ( _username == channelHeader.getOwner() ) {
      TEXT( buffer, "owner" );
      SET_VALUE( dict, buffer, "CHANNEL_ROLE" );
    } else {
      string role = lm->formatString("memberrole."+std::to_string(channelHeader.getRole()));
      if (channelHeader.getMembership() == kMembershipInvited) {
        buffer << lm->formatString("channel.invitation", role);
      } else {
        buffer << lm->formatString("channel.role", role);
      }
      SET_VALUE( dict, buffer, "CHANNEL_ROLE" );
    }
    
    this->putEntity( dict, channelHeader, "HEADER", buffer );
    if ( !channelHeader.isSecure() ) {
      TEXT( buffer, "channel.unsecure" );
      SET_SECTION_VALUE( dict, buffer, "UNSECURE" );
    }
    if ( channelHeader.hasSyncTime() ) { // synced channel
      if ( channelHeader.isOutdated() ) {
        TEXT( buffer, "channel.new_attachments" );
        SET_SECTION_VALUE( dict, buffer, "CHANNEL_STATUS_LINE0" );

        integer outdatedMediaSize = channelHeader.getTotalOutdatedMediaSize( );
        if (outdatedMediaSize > 0) {
          char buf[64];
          dict["CHANNEL_OUTDATED_MEDIA_SIZE"] = readable_fs(outdatedMediaSize, buf);
        }
      }
      
      SHOW_SECTION(dict, "CHANNEL_WAS_SYNCED");
      if ( CHANNEL_HEADER_CONFLICTED_MODIFICATION(channelHeader.getEntityModificationMask()) )
        SHOW_SECTION(dict, "HEADER_HAS_CONFLICT");
      if (CHANNEL_HEADER_RECENT_REMOTE_MODIFICATION(channelHeader.getEntityModificationMask()))
        SHOW_SECTION(dict, "HEADER_WAS_UPDATED");
      
      if ( CHANNEL_MEMBER_CONFLICTED_MODIFICATION(channelHeader.getChannelModificationMask()) )
        SHOW_SECTION(dict, "CHANNEL_MEMBER_HAS_CONFLICT");
      if ( MEDIA_CONFLICTED_MODIFICATION(channelHeader.getEntityModificationMask()) )
        SHOW_SECTION(dict, "CHANNEL_MEDIA_HAS_CONFLICT");
      
      this->putMetaDataOfContainer( dict, channelHeader, "CHANNEL", buffer );
      this->putMetaDataOfEntity( dict, channelHeader, "HEADER", channelHeader.getEntityModificationMask(), buffer );
      this->putMembers( dict, channelHeader, buffer );
      this->putResources( dict, channelHeader.getResources(), "HEADER", buffer );
      
    } else {
      if ( channelHeader.isOutdated() ) {
        if (channelHeader.getMembership() == kMembershipInvited) {
          TEXT( buffer, "channel.accept_invitation" );
          SET_SECTION_VALUE( dict, buffer, "CHANNEL_STATUS_LINE0" );
        } else {
          TEXT( buffer, "channel.sync" );
          SET_SECTION_VALUE( dict, buffer, "CHANNEL_STATUS_LINE0" );
        }
        
        // --------------
        // LINE_1: created
        string at = lm->formatDate(channelHeader.getCreateTime().time());
        if (_username == channelHeader.getOwner()) {
          buffer << lm->formatString("entity.createdByMe", at);
        } else {
          buffer << lm->formatString("entity.created", channelHeader.getOwnerFullname().c_str(), at);
        }
        SET_SECTION_VALUE( dict, buffer, "CHANNEL_STATUS_LINE1" );
        
      } else { // new local channel
        TEXT( buffer, "channel.unpublished" );
        SET_SECTION_VALUE( dict, buffer, "CHANNEL_STATUS_LINE0" );
        
        this->putMetaDataOfContainer( dict, channelHeader, "CHANNEL", buffer );
        this->putMembers( dict, channelHeader, buffer );
        this->putResources( dict, channelHeader.getResources(), "HEADER", buffer );
      }
    }
    
    // enable actions
    int role = getAccessRole( channelHeader, _username );
    if (channelHeader.getMembership() != kMembershipAccepted) {
      role = 0;
    }
    switch ( role ) {
      case 3: //kSyncRoleOwner
      case 2: //kSyncRoleAdmin:
        TEXT( buffer, "label.edit" );
        SET_VALUE( dict, buffer, "EDIT_LABEL" );
        SHOW_SECTION(dict, "HEADER_IS_ADMIN");
        SHOW_SECTION(dict, "HEADER_HAS_ATTACHMENTS");
      case 1: // kSyncRoleWrite:
        TEXT( buffer, "label.add_post" );
        SET_VALUE( dict, buffer, "ADD_POST_LABEL" );
        SHOW_SECTION(dict, "HEADER_IS_WRITER");
      case 0: // kSyncRoleRead:
        break;
    }
    if ( channelHeader.getSyncInd() == ejin::kSyncIndConflict && role < 2 && ! _invalidSessionKey ) {
      // edit entity in case of conflict
      TEXT( buffer, "label.edit" );
      SET_VALUE( dict, buffer, "EDIT_LABEL" );
      SHOW_SECTION(dict, "HEADER_IS_ADMIN");
    }
  }
  
  void ChannelTemplate2::putPost( json& dict, ChannelPost& channelPost, std::ostringstream& buffer )
  {
    MemberRole role = getAccessRole( channelPost, _username );
    switch ( role ) {
      case 3: //kSyncRoleOwner
      case 2: //kSyncRoleAdmin:
        TEXT( buffer, "label.edit" );
        SET_VALUE( dict, buffer, "EDIT_LABEL" );
        SHOW_SECTION(dict, "POST_IS_ADMIN");
        SHOW_SECTION(dict, "POST_HAS_ATTACHMENTS");
      case 1: // kSyncRoleWrite:
        TEXT( buffer, "label.add_comment" );
        SET_VALUE( dict, buffer, "ADD_COMMENT_LABEL" );
        SHOW_SECTION(dict, "POST_IS_WRITER");
      case 0: // kSyncRoleRead:
        break;
    }
    if ( channelPost.getSyncInd() == ejin::kSyncIndConflict && role < 2 && ! _invalidSessionKey ) {
      // edit entity in case of conflict
      TEXT( buffer, "label.edit" );
      SET_VALUE( dict, buffer, "EDIT_LABEL" );
      SHOW_SECTION(dict, "POST_IS_ADMIN");
    }
    this->putEntity( dict, channelPost, "POST", buffer );
    if (CHANNEL_POST_CONFLICTED_MODIFICATION(channelPost.getModificationMask())) {
      SHOW_SECTION(dict, "POST_HAS_CONFLICT");
    }
    if (CHANNEL_POST_RECENT_REMOTE_MODIFICATION(channelPost.getModificationMask()))
      SHOW_SECTION(dict, "POST_WAS_UPDATED");
    if ( MEDIA_CONFLICTED_MODIFICATION(channelPost.getEntityModificationMask()) )
      SHOW_SECTION(dict, "POST_MEDIA_HAS_CONFLICT");
    
    if ( channelPost.getSyncInd() == kSyncIndInsert ) {
      TEXT( buffer, "post.unpublished" );
      SET_SECTION_VALUE( dict, buffer, "POST_STATUS_LINE0" );
    } else {
      this->putMetaDataOfEntity( dict, channelPost, "POST", channelPost.getEntityModificationMask(), buffer );
    }
    
    this->putResources( dict, channelPost.getResources(), "POST", buffer );
    
    // sort for output
    channelPost.getComments().sort([](const shared_ptr<ChannelComment> & rhs, const shared_ptr<ChannelComment> & lhs) {
      if (! rhs->hasCreateTime())
        return true;
      if (! lhs->hasCreateTime())
        return false;
      return rhs->getCreateTime() > lhs->getCreateTime();
    });
    json array = json::array();
    for (list< shared_ptr<ChannelComment> >::iterator it = channelPost.getComments().begin(); it != channelPost.getComments().end(); it++ ) {
      json sub_dict = json::object();
      putPostComment( sub_dict, *it->get(), buffer );
      array.push_back(sub_dict);
    }
    dict["POST_COMMENT"] = array;
  }
  
  void ChannelTemplate2::putHeaderComment( json& dict, ChannelComment& channelComment, std::ostringstream& buffer )
  {
    this->putEntity( dict, channelComment, "HEADER_COMMENT", buffer );
    if (CHANNEL_COMMENT_CONFLICTED_MODIFICATION(channelComment.getModificationMask()))
      SHOW_SECTION(dict, "HEADER_COMMENT_HAS_CONFLICT");
    if (CHANNEL_COMMENT_RECENT_REMOTE_MODIFICATION(channelComment.getModificationMask()))
      SHOW_SECTION(dict, "HEADER_COMMENT_WAS_UPDATED");
    if ( MEDIA_CONFLICTED_MODIFICATION(channelComment.getEntityModificationMask()) )
      SHOW_SECTION(dict, "COMMENT_MEDIA_HAS_CONFLICT");
    
    if ( channelComment.getSyncInd() == kSyncIndInsert ) {
      TEXT( buffer, "comment.unpublished" );
      SET_SECTION_VALUE( dict, buffer, "HEADER_COMMENT_STATUS_LINE0" );
    } else {
      this->putMetaDataOfEntity( dict, channelComment, "HEADER_COMMENT", channelComment.getEntityModificationMask(), buffer );
    }
    
    this->putResources( dict, channelComment.getResources(), "HEADER_COMMENT", buffer );
  }
  
  void ChannelTemplate2::putPostComment( json& dict, ChannelComment& channelComment, std::ostringstream& buffer )
  {
    MemberRole role = getAccessRole( channelComment, _username );
    switch ( role ) {
      case 3: //kSyncRoleOwner
      case 2: //kSyncRoleAdmin:
        TEXT( buffer, "label.edit" );
        SET_VALUE( dict, buffer, "EDIT_LABEL" );
        SHOW_SECTION(dict, "COMMENT_IS_ADMIN");
        SHOW_SECTION(dict, "COMMENT_HAS_ATTACHMENTS");
      case 1: // kSyncRoleWrite:
        TEXT( buffer, "label.add_comment" );
        SET_VALUE( dict, buffer, "ADD_COMMENT_LABEL" );
        SHOW_SECTION(dict, "COMMENT_IS_WRITER");
      case 0: // kSyncRoleRead:
        break;
    }
    if ( channelComment.getSyncInd() == ejin::kSyncIndConflict && role < 2 && ! _invalidSessionKey ) {
      // edit entity in case of conflict
      TEXT( buffer, "label.edit" );
      SET_VALUE( dict, buffer, "EDIT_LABEL" );
      SHOW_SECTION(dict, "COMMENT_IS_ADMIN");
    }
    this->putEntity( dict, channelComment, "COMMENT", buffer );
    if (CHANNEL_COMMENT_CONFLICTED_MODIFICATION(channelComment.getModificationMask()))
      SHOW_SECTION(dict, "COMMENT_HAS_CONFLICT");
    if (CHANNEL_COMMENT_RECENT_REMOTE_MODIFICATION(channelComment.getModificationMask()))
      SHOW_SECTION(dict, "COMMENT_WAS_UPDATED");
    if ( MEDIA_CONFLICTED_MODIFICATION(channelComment.getEntityModificationMask()) )
      SHOW_SECTION(dict, "COMMENT_MEDIA_HAS_CONFLICT");
    
    if ( channelComment.getSyncInd() == kSyncIndInsert ) {
      TEXT( buffer, "comment.unpublished" );
      SET_SECTION_VALUE( dict, buffer, "COMMENT_STATUS_LINE0" );
    } else {
      this->putMetaDataOfEntity( dict, channelComment, "COMMENT", channelComment.getEntityModificationMask(), buffer );
    }
    this->putResources( dict, channelComment.getResources(), "COMMENT", buffer );
  }
  
  void ChannelTemplate2::putMembers( json& dict, ChannelHeader& channelHeader, std::ostringstream& buffer )
  {
    LocalizationManager* lm = LocalizationManager::getInstance();
    vector<string> params;

    list< shared_ptr<ChannelMember> >& members( channelHeader.getMembers() );
    const string& owner( channelHeader.getOwner() );
    
    buffer << LocalizationManager::getInstance()->formatString("label.members", (int) members.size());
    SET_VALUE( dict, buffer, "MEMBER_TITLE" );
    if (! members.empty())
      dict["HAS_MEMBER"] = true;
    TEXT( buffer, "label.add" );
    SET_VALUE( dict, buffer, "ADD_MEMBER_LABEL" );
    
    // sort for output
    members.sort([&owner](const shared_ptr<ChannelMember> & rhs, const shared_ptr<ChannelMember> & lhs) {
      if (rhs->getUsername() == owner)
        return true;
      return rhs->getRole() > lhs->getRole();
    });

    json array = json::array();
    if ( members.size() > 5 ) {
      // display a summary
      auto it = members.begin();

      // -----
      // collect admins
      {
        int counter = 0;
        json sub_dict = json::object();
        for ( ; it != members.end(); ) {
          MemberRole role = getRole( (**it), owner );
          switch ( role ) {
            case kSyncRoleOwner:
              it++;
              continue;
            case kSyncRoleAdmin:
              if ( buffer.tellp() != 0 )
                buffer << " ";
              buffer << (*it)->getUsername();
              it = members.erase(it);
              counter++;
              continue;
            default:
              break;
          }
          break;
        }
        SET_VALUE( sub_dict, buffer, "DESCRIPTION" );
        buffer << LocalizationManager::getInstance()->formatString("label.admins", counter);
        SET_VALUE( sub_dict, buffer, "TITLE" );
        array.push_back(sub_dict);
      }
      // -----
      // collect authors
      {
        int counter = 0;
        json sub_dict = json::object();
        for ( ; it != members.end(); ) {
          if ((*it)->getRole() == kSyncRoleWrite ) {
            if ( buffer.tellp() != 0 )
              buffer << " ";
            buffer << (*it)->getUsername();
            it = members.erase(it);
            counter++;
          } else {
            break;
          }
        }
        SET_VALUE( sub_dict, buffer, "DESCRIPTION" );
        buffer << LocalizationManager::getInstance()->formatString("label.authors", counter);
        SET_VALUE( sub_dict, buffer, "TITLE" );
        array.push_back(sub_dict);
      }
      // -----
      // collect readers
      {
        int counter = 0;
        json sub_dict = json::object();
        for ( ; it != members.end(); ) {
          if ((*it)->getRole() == kSyncRoleRead ) {
            if ( buffer.tellp() != 0 )
              buffer << " ";
            buffer << (*it)->getUsername();
            it = members.erase(it);
            counter++;
          } else {
            break;
          }
        }
        SET_VALUE( sub_dict, buffer, "DESCRIPTION" );
        buffer << LocalizationManager::getInstance()->formatString("label.readers", counter);
        SET_VALUE( sub_dict, buffer, "TITLE" );
        array.push_back(sub_dict);
      }
    }
    dict["SUMMARY"] = array;

    bool hasConflict = false;
    bool wasUpdated = false;
    array = json::array();
    for (list< shared_ptr<ChannelMember> >::const_iterator it = members.begin(); it != members.end(); it++ ) {
      json sub_dict = json::object();
      if (CHANNEL_MEMBER_CONFLICTED_MODIFICATION((*it)->getModificationMask())) {
        SHOW_SECTION(sub_dict, "MEMBER_HAS_CONFLICT");
        hasConflict = true;
      }
      if (CHANNEL_MEMBER_RECENT_REMOTE_MODIFICATION((*it)->getModificationMask())) {
        SHOW_SECTION(sub_dict, "MEMBER_WAS_UPDATED");
        wasUpdated = true;
      }
      //this->putMetaDataOfEntity( *sub_dict, **it, "MEMBER", (*it)->getModificationMask() );
      sub_dict["FULLNAME"] = (*it)->getFullname();
      sub_dict["USERNAME"] = (*it)->getUsername();
      if ((*it)->hasCreateTime()) {
        DATE( buffer, (*it)->getCreateTime() );
        SET_VALUE( sub_dict, buffer, "CREATE_TIME" );
      }
      if ((*it)->hasTags())
        sub_dict["MEMBER_TAGS"] = (*it)->getTags();
      
      string role = lm->formatString("memberrole."+std::to_string(getRole( (**it), owner )));
      if ( (*it)->getMembership() == kMembershipInvited ) {
        buffer << lm->formatString("channel.member_invited", role);
      } else {
        if ( (*it)->hasCreateTime() ) {
          string since = lm->formatDate((*it)->getCreateTime().time());
          buffer << lm->formatString("channel.member_since", role, since);
        } else {
          buffer << lm->formatString("channel.member", role);
        }
      }
      SET_VALUE( sub_dict, buffer, "ROLE_LABEL" );
      switch ((*it)->getMembership()) {
        case 0: // kSyncRoleRead:
          switch ((*it)->getTrustLevel()) {
            case kTrustLevelUnapproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#606060'>&#x2691;</span>";
              break;
            case kTrustLevelIndirectlyApproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#B6C92C'>&#x2691;</span>";
              break;
            case kTrustLevelApproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#279C1E'>&#x2691;</span>";
              break;
            default:
              sub_dict["MEMBERSHIP"] = "<span style='color:#000000'>&#x2691;</span>";
              break;
          }
          break;
        case 1: // kSyncRoleWrite:
          switch ((*it)->getTrustLevel()) {
            case kTrustLevelUnapproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#606060'>&#x2713;</span>";
              break;
            case kTrustLevelIndirectlyApproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#B6C92C'>&#x2713;</span>";
              break;
            case kTrustLevelApproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#279C1E'>&#x2713;</span>";
              break;
            default:
              sub_dict["MEMBERSHIP"] = "<span style='color:#000000'>&#x2713;</span>";
              break;
          }
          break;
        case 2: //kSyncRoleAdmin:
          switch ((*it)->getTrustLevel()) {
            case kTrustLevelUnapproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#606060'>&#xD7;</span>";
              break;
            case kTrustLevelIndirectlyApproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#B6C92C'>&#xD7;</span>";
              break;
            case kTrustLevelApproved:
              sub_dict["MEMBERSHIP"] = "<span style='color:#279C1E'>&#xD7;</span>";
              break;
            default:
              sub_dict["MEMBERSHIP"] = "<span style='color:#000000'>&#xD7;</span>";
              break;
          }
          break;
      }
      array.push_back(sub_dict);
    }
    dict["MEMBER"] = array;

    if (hasConflict) {
      SHOW_SECTION(dict, "MEMBERLIST_HAS_CONFLICT");
    }
    if (wasUpdated) {
      SHOW_SECTION(dict, "MEMBERLIST_WAS_UPDATED");
    }

    int role = getAccessRole( channelHeader, _username );
    switch ( role ) {
      case 3: //kSyncRoleOwner
      case 2: //kSyncRoleAdmin:
        TEXT( buffer, "label.edit" );
        SET_VALUE( dict, buffer, "MEMBER_EDIT_LABEL" );
        SHOW_SECTION(dict, "MEMBER_IS_ADMIN");
        break;
      case 1: // kSyncRoleWrite:
      case 0: // kSyncRoleRead:
        if (hasConflict) {
          TEXT( buffer, "label.edit" );
          SET_VALUE( dict, buffer, "MEMBER_EDIT_LABEL" );
          SHOW_SECTION(dict, "MEMBER_IS_ADMIN");
        }
        break;
    }
  }
  
  void ChannelTemplate2::putResources( json& dict, list< shared_ptr<Resource> >& resources, const string& prefix, std::ostringstream& buffer )
  {
    int count = 0;
    bool foundConflict = false;
    bool foundUpdate = false;
    Media* selectedMedia = NULL;
    json array = json::array();
    for (list< shared_ptr<Resource> >::iterator it = resources.begin(); it != resources.end(); it++ ) {
      Media* media = static_cast< Media* >((*it).get());
      //if ( isImageType( media->getMimeType() ) ) { // && ! media->isAttachmentOutdated() ) {
      json sub_dict = json::object();
      sub_dict["ID"] = media->getId();
      if (media->hasName()) {
        sub_dict["CAPTION"] = media->getName();
      } else {
        TEXT( buffer, "attachment.nocaption" );
        SET_VALUE( sub_dict, buffer, "CAPTION" );
      }
      sub_dict["FILE_EXTENSION"] = FILE_EXTENSION_OF(media->getMimeType());
      sub_dict["MIMETYPE"] = media->getMimeType();
      sub_dict["SIZE"] = media->getSize();
      if (media->getSyncInd() == ejin::kSyncIndConflict) {
        SHOW_SECTION( sub_dict, "MEDIA_HAS_CONFLICT" )
      } else if (foundUpdate) {
        SHOW_SECTION( sub_dict, "MEDIA_WAS_UPDATED" )
      }

      count++;
      if ( ! selectedMedia )
        selectedMedia = media;
      if ( media->isSelected() )
        selectedMedia = media;
      if (media->getSyncInd() == ejin::kSyncIndConflict)
        foundConflict = true;
      if ( MEDIA_RECENT_REMOTE_MODIFICATION( media->getModificationMask() ) )
        foundUpdate = true;

      array.push_back(sub_dict);
    }
    dict[prefix+"_RESOURCE"] = array;
    
    if ( selectedMedia ) {
      json sub_dict = json::object();
      sub_dict["ID"] = selectedMedia->getId();
      sub_dict["CAPTION"] = selectedMedia->getName();
      sub_dict["MIMETYPE"] = selectedMedia->getMimeType();
      sub_dict["FILE_EXTENSION"] = FILE_EXTENSION_OF(selectedMedia->getMimeType());
      sub_dict["SIZE"] = selectedMedia->getSize();
      if (foundConflict) {
        SHOW_SECTION( dict, "MEDIA_HAS_CONFLICT" )
      } else if (foundUpdate) {
        SHOW_SECTION( dict, "MEDIA_WAS_UPDATED" )
      }
      dict[prefix+"_SELECTED_RESOURCE"] = sub_dict;
      
    } else {
      TEXT( buffer, "label.add_attachment" );
      SET_VALUE( dict, buffer, "CAPTION" );
      SHOW_SECTION( dict, prefix+"_NO_RESOURCE" )
    }
    
    buffer << LocalizationManager::getInstance()->formatString("label.attachments", count);
    SET_VALUE( dict, buffer, "ATTACHMENT_LABEL" );
    
    if ( count > 0 ) {
      SHOW_SECTION( dict, prefix+"_HAS_ATTACHMENTS" )
    }
  }
  
  template<class T> void ChannelTemplate2::putEntity( json& dict, T& entity, const string& prefix, std::ostringstream& buffer )
  {
    dict[prefix+"_ID"] = entity.getId();
    dict[prefix+"_GID"] = entity.hasGid() ? entity.getGid() :  "";
    dict[prefix+"_OWNER"] = entity.getOwner();
    if (entity.hasCreateTime()) {
      DATETIME( buffer, entity.getCreateTime() )
      SET_VALUE( dict, buffer, prefix+"_CREATE_TIME" );
      ISO_8601( buffer, entity.getCreateTime() );
      SET_VALUE( dict, buffer, prefix+"_CREATE_ISO_TIME" );
    }
    DATETIME( buffer, entity.getModifyTime() )
    SET_VALUE( dict, buffer, prefix+"_MODIFY_TIME" );
    if ( entity.isEncrypted() ) {
      TEXT( buffer, "encrypted" );
      SET_VALUE( dict, buffer, prefix+"_CONTENT" );
    } else {
      dict[prefix+"_CONTENT"] = replace_string(entity.getContent(), "\n", "<br/>");
    }
  }
  
  template<class T> void ChannelTemplate2::putMetaDataOfEntity(json& dict,
                                                              T& entity,
                                                              const string& prefix,
                                                              integer modifications,
                                                              std::ostringstream& buffer)
  {
    LocalizationManager* lm = LocalizationManager::getInstance();
    vector<string> params;
    
    // --------------
    // LINE_1: created
    {
      string at = lm->formatDate(entity.getCreateTime().time());
      if (_username == entity.getOwner()) {
        buffer << lm->formatString("entity.createdByMe", at);
      } else {
        buffer << lm->formatString("entity.created", entity.getOwnerFullname().c_str(), at);
      }
      SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE1" );
    }

    // --------------
    // LINE_2: last modified
    if ( entity.hasModifiedBy() && entity.getModifyTime() > entity.getCreateTime()) {
      string at = lm->formatDate(entity.getModifyTime().time());
      if (_username == entity.getModifiedBy()) {
        buffer << lm->formatString("entity.updatedByMe", at);
      } else {
        buffer << lm->formatString("entity.updated", entity.getModifiedByFullname().c_str(), at);
      }
      SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE2" );
    }
    
    // --------------
    // LINE_3: conflicts
    {
      params.clear();
      if (CHANNEL_HEADER_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_introduction"));
      if (CHANNEL_MEMBER_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_memberlist"));
      if (CHANNEL_POST_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_post"));
      if (CHANNEL_COMMENT_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_comment"));
      if (MEDIA_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_attachments"));
      if ( params.size() > 0 && ! _invalidSessionKey ) {
        buffer << lm->formatEnumeration("entity.conflicts", params);
        SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE3" );
        return; // stop further status lines
      }
    }
    
    // --------------
    // LINE_4: remote updates
    {
      params.clear();
      if (CHANNEL_HEADER_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_introduction"));
      if (CHANNEL_MEMBER_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_memberlist"));
      if (CHANNEL_POST_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_post"));
      if (CHANNEL_COMMENT_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_comment"));
      if (MEDIA_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_attachments"));
      if ( params.size() > 0 && ! _invalidSessionKey ) {
        buffer << lm->formatEnumeration("entity.remote_updates", params);
        SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE4" );
      }
    }

    // --------------
    // LINE_5: local updates
    {
      params.clear();
      if (CHANNEL_HEADER_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_introduction"));
      if (CHANNEL_MEMBER_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_memberlist"));
      if (CHANNEL_POST_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_post"));
      if (CHANNEL_COMMENT_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_comment"));
      if (MEDIA_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.this_attachments"));
      if ( params.size() > 0 && ! _invalidSessionKey ) {
        buffer << lm->formatEnumeration("entity.local_updates", params);
        SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE5" );
      }
    }
  }
  
  void ChannelTemplate2::putMetaDataOfContainer(json& dict,
                                               ChannelHeader& channelHeader,
                                               const string& prefix,
                                               std::ostringstream& buffer)
  {
    if ( channelHeader.getSyncInd() == kSyncIndInsert )
      return;
    
    LocalizationManager* lm = LocalizationManager::getInstance();
    vector<string> params;
    integer modifications = channelHeader.getChannelModificationMask();
    
    // --------------
    // LINE_1: created
    {
      string at = lm->formatDate(channelHeader.getCreateTime().time());
      if (_username == channelHeader.getOwner()) {
        buffer << lm->formatString("entity.createdByMe", at);
      } else {
        buffer << lm->formatString("entity.created", channelHeader.getOwnerFullname().c_str(), at);
      }
      SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE1" );
    }

    // --------------
    // LINE_2: last changed
    if ( channelHeader.hasModifiedBy() && channelHeader.getModifyTime() > channelHeader.getCreateTime()) {
      string at = lm->formatDate(channelHeader.getModifyTime().time());
      if (_username == channelHeader.getModifiedBy()) {
        buffer << lm->formatString("entity.updatedByMe", at);
      } else {
        buffer << lm->formatString("entity.updated", channelHeader.getModifiedByFullname().c_str(), at);
      }
      SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE2" );
    }
    
    // --------------
    // LINE_3: conflicts
    {
      params.clear();
      if (CHANNEL_HEADER_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.introduction"));
      if (CHANNEL_MEMBER_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.memberlist"));
      if (CHANNEL_POST_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.posts"));
      if (CHANNEL_COMMENT_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.comments"));
      if (MEDIA_CONFLICTED_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.attachments"));
      if ( params.size() > 1 && ! _invalidSessionKey ) {
        buffer << lm->formatEnumeration("entity.conflicts", params);
        SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE3" );
      }
    }

    // --------------
    // LINE_4: remote updates
    {
      params.clear();
      if (CHANNEL_HEADER_RECENT_REMOTE_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.introduction"));
      if (CHANNEL_MEMBER_RECENT_REMOTE_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.memberlist"));
      if (CHANNEL_POST_RECENT_REMOTE_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.posts"));
      if (CHANNEL_COMMENT_RECENT_REMOTE_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.comments"));
      if (MEDIA_RECENT_REMOTE_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.attachments"));
      if ( params.size() > 1 && ! _invalidSessionKey ) {
        buffer << lm->formatEnumeration("entity.remote_updates", params);
        SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE4" );
      }
    }
    
    // --------------
    // LINE_5: local updates
    {
      params.clear();
      if (CHANNEL_HEADER_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.introduction"));
      if (CHANNEL_MEMBER_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.memberlist"));
      if (CHANNEL_POST_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.posts"));
      if (CHANNEL_COMMENT_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.comments"));
      if (MEDIA_PENDING_LOCAL_MODIFICATION(modifications))
        params.push_back(lm->formatString("entity.attachments"));
      if ( params.size() > 1 ) {
        buffer << lm->formatEnumeration("entity.local_updates", params);
        SET_SECTION_VALUE( dict, buffer, prefix+"_STATUS_LINE5" );
      }
    }
  }
  template<class T> MemberRole ChannelTemplate2::getAccessRole( T& entity, const string& username )
  {
    if ( _readonly )
      return kSyncRoleRead;
    if ( ! username.empty() && username == entity.getOwner() )
      return kSyncRoleOwner;
    else
      return entity.getAccessRole();
  }
  MemberRole ChannelTemplate2::getRole( ChannelMember& entity, const string& owner )
  {
    if ( ! owner.empty() && owner == entity.getUsername() )
      return kSyncRoleOwner;
    else
      return (MemberRole)entity.getRole();
  }
}

