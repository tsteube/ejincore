/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_SERIALIZABLE_ENTITY_H__
#define __EJIN_SERIALIZABLE_ENTITY_H__

#include "Declarations.h"

//#include "XMLAttribute.h"
//#include "XMLAttributeSet.h"
#include "xmlwriter.h"

#include <ThirdParty/json.hpp>

using nlohmann::json;

// forward declaration
namespace sqlite {
  class Value;
}

namespace ser
{
  // forward declaration
  class AttributeSet;
  class Attribute;
  class ISerializableEntity;
  
  /**
   *  Helper interface to compare entities on business rules and copy business data. 
   *  If isDataEqualTo returns false (true) copyDataFrom must also return false (true).
   */
  class ISerializableEntity {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    virtual ~ISerializableEntity( void ) { }
    
    virtual sqlite::Value* valueByXmlNode( const char* name ) { return NULL; }
    virtual ISerializableEntity* entityByXmlNode( const char* name, int position ) { return NULL; }
    virtual void endXmlNode( const char* name ) { };
    virtual bool marshalXml( ser::XmlStream& xml, const char* const aesKey, size_t keyLength ) const { return false; }
    
    virtual bool unmarshalJson( const nlohmann::json& node ) { return false; }
    virtual bool marshalJson( nlohmann::json& node ) const { return false; }

    //virtual bool unmarshalJson( json_t* node ) { return false; }
    //virtual bool marshalJson( json_t* node ) const { return false; }

  };
  
  class IRestMarshaller {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    virtual ~IRestMarshaller( void ) { }

    virtual string mimeType() = 0;
    
    virtual void unmarshal( ISerializableEntity& container, const char* data, size_t length,
                                           const char* aesKey = NULL, size_t aesKeyLength = 0) = 0;
    
    virtual string marshal( ISerializableEntity& entity, bool pretty = false ) = 0;
  };

  /**
   *  Helper interface to compare entities on business rules and copy business data. 
   *  If isDataEqualTo returns false (true) copyDataFrom must also return false (true).
   */
  class SerializableEntity: public ISerializableEntity {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    virtual sqlite::Value* valueByXmlNode( const char* name );
    
    // ==========
    // Protected Abstract Interface
    // ==========
  protected:
    
    /** concrete DB relation name */
    virtual const string& xmlName( void ) const = 0;
    /** concrete DB relation fields */
    virtual const ser::XmlAttributeSet& xmlFields( void ) const = 0;
    
    virtual sqlite::Value* value( string property ) const = 0;
    
  };  
  
}

#endif // __EJIN_SERIALIZABLE_ENTITY_H__
