/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "StringTemplate2.h"

#include "Channel.h"
#include "ChannelTemplate2.h"

#include "Chat.h"
#include "ChatTemplate2.h"

#import "inja.hpp"

#define U_DISABLE_RENAMING 1

#include "CoreFoundation/CoreFoundation.h"

// ===========================
// Class Definition
// ===========================

using namespace inja;
using namespace std;
using json = nlohmann::json;

namespace ejin
{
  // ==========
  // Public Interface
  // ==========

  void StringTemplate2::setupTemplateLib( )
  {
  }
 
  void StringTemplate2::teardownTemplateLib( )
  {
  }

  void StringTemplate2::expand(const string& tpl,
                              const string& styles,
                              const string& scripts,
                              const string& resources,
                              const Chat& chat,
                              string& output,
                              const string& languageCode,
                              const string& username )
  {
    json model;
    ChatTemplate2(chat, username).putAll( model );
    //std::cout << setw(2) << model << std::endl;

    Environment env;
    Template temp = env.parse_template(tpl);
    string result = env.render(temp, model);
    output.append(result);
    //std::cout << output << std::endl;
  }
  
  void StringTemplate2::expand(const string& tpl,
                              const string& styles,
                              const string& scripts,
                              const string& resources,
                              Channel* container,
                              string& output,
                              const string& languageCode,
                              const string& username,
                              bool readonly )
  {
    assert( container );
    assert( container->getHeader() );
    json model;
    ChannelTemplate2(container, username, readonly).putAll( model );
    //std::cout << setw(2) << model << std::endl;

    Environment env;
    Template temp = env.parse_template(tpl);
    string result = env.render(temp, model);
    output.append(result);
    //std::cout << result << std::endl;
  }
  
}


