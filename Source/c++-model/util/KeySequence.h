/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_KEY_SEQUENCE_H__
#define __EJIN_KEY_SEQUENCE_H__

#include "Declarations.h"

namespace ejin
{

  /**
   * The global key is of type <code>String</code> and must consist of
   * digits only. This class defines some common operations on this key.
   * <ul>
   * <li>Validation
   * <li>natural increment of the key by 1.
   * <li>natural comparision of global string keys.
   * <ul>
   */
  class KeySequence {

    friend bool operator==(const KeySequence& lhs, const KeySequence& rhs);
    friend bool operator< (const KeySequence& lhs, const KeySequence& rhs);
    friend ostream& operator<<(ostream& ostr, const KeySequence& rhs);

    // ==========
    // Ctor / Dtor
    // ==========
  public:

    // ctors
    KeySequence( void );
    KeySequence( const KeySequence& key );
    explicit KeySequence( const string& key );
    explicit KeySequence( const char* key );
    // dtor
    ~KeySequence( void );

    // ==========
    // Public Interface
    // ==========
  public:

    // unary increment operator
    KeySequence& operator++();
    KeySequence operator++(int);

    // ==========
    // Object Interface
    // ==========

    // assignment
    KeySequence& operator=( const KeySequence& rhs );

    // access current identifier
    operator string() const { return this->key_; }
    operator bool() const { return this->key_ != NULL && *this->key_ != '\0'; }
    const char* c_str() const { return this->key_; }

    // ==========
    // Protected Interface
    // ==========
  protected:

    // comparision
    bool equals( const KeySequence& rhs ) const;
    bool lessThan( const KeySequence& rhs ) const;

    // ==========
    // Protected Interface
    // ==========
  private:
    
    // global unique identifier
    char* key_;
    size_t length_;
  };

  // comparision
  inline bool operator==( const KeySequence& lhs, const KeySequence& rhs ){return lhs.equals(rhs);}
  inline bool operator!=( const KeySequence& lhs, const KeySequence& rhs ){return !operator==(lhs,rhs);}
  inline bool operator< ( const KeySequence& lhs, const KeySequence& rhs ){return lhs.lessThan(rhs);}
  inline bool operator> ( const KeySequence& lhs, const KeySequence& rhs ){return  operator< (rhs,lhs);}
  inline bool operator<=( const KeySequence& lhs, const KeySequence& rhs ){return !operator> (lhs,rhs);}
  inline bool operator>=( const KeySequence& lhs, const KeySequence& rhs ){return !operator< (lhs,rhs);}

  // debugging
  inline ostream& operator<<( ostream& ostr, const KeySequence& rhs ){return ostr << "gid[" << rhs.key_ << "]";}
    
}

#endif // __EJIN_KEY_SEQUENCE_H__