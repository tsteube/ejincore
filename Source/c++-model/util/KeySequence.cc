/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "KeySequence.h"
#include "UsingEjinTypes.h"

extern "C" {
  #include "strnatcmp.h"
}

namespace ejin {
  
  KeySequence::KeySequence( const char* key ): key_(), length_(0)
  {
    if (key) {
      this->length_ = strlen(key);
      this->key_ = new char[this->length_+1];
      int i=0;
      for (; key[i]; i++) {
        if ( isnumber(key[i]) )
          this->key_[i] = key[i];
        else
          ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "invalid key format, must be numeric: %s", key);
      }
      this->key_[i] = '\0';
    } else {
      // default is "0"
      this->length_ = 1;
      this->key_ = new char[2];
      this->key_[0] = '0';
      this->key_[1] = '\0';
    }
  }
  KeySequence::KeySequence( void ): KeySequence("0") { }
  KeySequence::KeySequence( const string& key ): KeySequence(key.c_str()) { }
  KeySequence::KeySequence( const KeySequence& key ): KeySequence(key.key_) { }
  KeySequence::~KeySequence( void )
  { if (this->key_) delete[] this->key_; }
  
  // ==========
  // Public Interface
  // ==========
  
  KeySequence& KeySequence::operator++() {
    
    bool overrun = true;
    char* next = new char[this->length_+1];
    next[this->length_] = '\0';
    for (size_t i=this->length_-1; i>0; i--) {
      if (overrun) {
        if (this->key_[i] == '9') {
          next[i] = '0';
          overrun = true;
        } else { 
          next[i] = this->key_[i]+1;
          overrun = false;
        }
      } else {
        next[i] = this->key_[i];
      }
    }
    
    delete this->key_;
    if (overrun) {
      // '1' prefix
      this->length_++;
      this->key_ = new char[this->length_+1];
      this->key_[0] = '1';
      strncpy(this->key_+1, next, this->length_);
      delete[] next;
    } else {
      this->key_ = next;
    }
    
    return *this;
  }
  KeySequence KeySequence::operator++(int dummy) {
    KeySequence tmp(*this); 
    operator++();
    return tmp;     
  }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  KeySequence& KeySequence::operator=( const KeySequence& rhs ) {    
    if (this != &rhs) {
      this->length_ = rhs.length_;
      delete this->key_;
      this->key_ = new char[this->length_+1];
      memcpy(this->key_, rhs.key_, this->length_+1);
    }
    return *this;
  }

  bool KeySequence::equals( const KeySequence& rhs ) const 
  {
    if (this == &rhs) 
      return true;
    
    return ::strnatcmp(this->key_, rhs.key_) == 0;
  }  

  bool KeySequence::lessThan( const KeySequence& rhs ) const
  {
    if (this == &rhs) 
      return false;

    // compare by natural order
    return ::strnatcmp(this->key_, rhs.key_) < 0;
  }
  
}
