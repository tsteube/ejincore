/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_UTILITIES_H
#define __EJIN_UTILITIES_H

#include "Declarations.h"
#include "SqliteResultSet.h"
#include "EjinDatabase.h"

#include <algorithm>
#include <string>         // std::string
#include <cstddef>        // std::size_t

// ==========
// Ejin Utility Classes
// ==========

extern const char* SyncOperationMapping[];
extern const char* RoleEnumMapping[];
extern const char* MembershipEnumMapping[];
extern const char* DurationPeriodEnumMapping[];

namespace sqlite {
  class ResultSet;
}

namespace ejin {
  class Member;
  using sqlite::ResultSet;
    
  /**
   * Common utility methods.
   */
  class Utilities {    
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    /**
     * Create a HMAC-SHA1 signature
     */ 
    static string sign( const char* data, size_t length, const char* keyBytes );
    
    /**
     * base64 encoding
     */
    static std::vector<char> base64_encode( const std::string& decoded_string );
    static std::vector<char> base64_encode( char const* decoded_string, size_t len );
    static std::vector<char> base64_decode( char const* encoded_string, size_t len );
    static std::vector<char> base64_decode( const std::vector<char>& encoded_string );
    static std::vector<char> base64_decode( const std::string& encoded_string );

    /**
     * Handle session key encryption
     */
    static pair<string,string> generateRSAKey( )
      throw(security_integrity_error);
    static unique_ptr<string> decryptSessionKey( const string& key, const Member& member, const string& privateKey )
      throw(security_integrity_error);
    static unique_ptr<string> encryptSessionKey( const string& key, const Member& member )
      throw(security_integrity_error);
    static const string generateSessionKey( );

    /**
     * Search for the specific element in the collections based on the equals method. If found the corresponding element
     * from the list is returned.
     */ 
    template <typename T>
    static shared_ptr< T > findElementIn( const set< shared_ptr< T > >& all, const T& elm ) {
      shared_ptr<T> item;
      for (typename set< shared_ptr< T > >::const_iterator it = all.begin(); it != all.end(); it++) {
        if (*it->get() == elm) {
          if (! item )
            item = *it;
          else
            ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "unexpected duplicate element in set." );              
        }
      }
      return item;
    }
    template <typename T>
    static shared_ptr< T > findElementIn( const list< shared_ptr< T > >& all, const T& elm ) {
      shared_ptr<T> item;
      for (typename list< shared_ptr< T > >::const_iterator it = all.begin(); it != all.end(); it++) {
        if (*it->get() == elm) {
          if (! item )
            item = *it;
          else {
            ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "unexpected duplicate element in list." );
          }
        }
      }
      return item;
    }    
    template <typename T>
    static T* findElementIn( const ResultSet& all, const T& elm ) {
      T* item = NULL;
      for (vector< sqlite::BaseEntity* >::const_iterator it=all.begin(); it != all.end(); it++) {
        T* resultElm(dynamic_cast<T*>(*it));
        if (*resultElm == elm) {
          if (item == NULL)
            item = resultElm;
          else
            ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "unexpected duplicate element in result set." );              
        }
      }
      return item;
    }
    
    template <typename T>
    static int indexOf( const list< shared_ptr< T > >& all, const T& elm ) {
      int index=0;
      for (typename list< shared_ptr< T > >::const_iterator it = all.begin(); it != all.end(); it++) {
        if (*it->get() == elm) {
          return index;
        }
        index++;
      }
      return -1;
    }    
    template <typename T>
    static int indexOf( const ResultSet& all, const T& elm ) {
      int index=0;
      for (vector< sqlite::BaseEntity* >::const_iterator it=all.begin(); it != all.end(); it++) {
        T* resultElm(dynamic_cast<T*>(*it));
        if (*resultElm == elm) {
          return index;
        }
        index++;
      }
      return -1;
    }
    
    template <typename T>
    static std::shared_ptr<T> findOrAppend( std::list< std::shared_ptr< T > >& list, size_t index )
    {
      if (list.size() > index) {
        typename std::list< std::shared_ptr< T > >::iterator refIt = list.begin();
        std::advance(refIt, index);
        return *refIt;
      } else {
        shared_ptr<T> entity( shared_ptr<T>(new T()) );
        list.push_back(entity);
        return entity;
      }
    }

    /**
     * Compares to collection of business data for equal content.
     * Note that although the equals operation returns the content must not be identical
     * if there are local and remote conflicts data sets.
     */ 
    template <typename T>
    static bool hasEqualContent( const set< shared_ptr< T > >& lhs, const set< shared_ptr< T > >& rhs ) {
      if ( rhs.size() != rhs.size() )
        return false;
      for (typename set< shared_ptr< T > >::const_iterator it = lhs.begin(); it != lhs.end(); it++) {
        T* lhElement(dynamic_cast<T*>(*it));
        shared_ptr<T> rhElement = Utilities::findElementIn( rhs, *lhElement );
        if (! lhElement->hasEqualContent( *rhElement ) ) {
          return false;
        }
      }
      return true;
    }
    template <typename T>
    static bool hasEqualContent( const list< shared_ptr< T > >& lhs, const ResultSet& rhs ) {
      if ( lhs.size() != rhs.size() )
        return false;

      for (typename list< shared_ptr< T > >::const_iterator it = lhs.begin(); it != lhs.end(); it++) {
        shared_ptr<T> lhElement(*it);
        T* rhElement2( Utilities::findElementIn( rhs, *lhElement ) );
        if (! lhElement->hasEqualContent( *rhElement2 ) ) {
          return false;
        }
      }
      return true;
    }
    template <typename T>
    static bool hasEqualOrdering( const list< shared_ptr< T > >& lhs, const ResultSet& rhs ) {
      integer no = 0;
      for (typename list< shared_ptr< T > >::const_iterator it = lhs.begin(); it != lhs.end(); it++, no++) {
        shared_ptr<T> lhElement(*it);
        T* rhElement( Utilities::findElementIn( rhs, *lhElement ) );
        if ( ! rhElement || no != rhElement->getNo() ) {
          return false;
        }
      }
      return true;
    }

    struct split
    {
      enum empties_t { empties_ok, no_empties };
    };
    template <typename Container>
    static Container& split(Container&                            result,
                                const typename Container::value_type& s,
                                const typename Container::value_type& delimiters,
                                split::empties_t                      empties = split::empties_ok )
    {
      result.clear();
      size_t current;
      size_t next = -1;
      do
      {
        if (empties == split::no_empties)
        {
          next = s.find_first_not_of( delimiters, next + 1 );
          if (next == Container::value_type::npos) break;
          next -= 1;
        }
        current = next + 1;
        next = s.find_first_of( delimiters, current );
        result.push_back( s.substr( current, next - current ) );
      }
      while (next != Container::value_type::npos);
      return result;
    }

    template <typename T>
    static list< shared_ptr<T> > mergeLists( Database& db, const list< shared_ptr<T> >& current, const list< shared_ptr<T> >& updates )
    throw(data_access_error)
    {
      list< shared_ptr<T> > result( current );
      
      for (auto it=result.begin(); it != result.end(); ) {
        shared_ptr<T> messageEntity(static_cast<shared_ptr<T>>(*it));
        shared_ptr<T> update = Utilities::findElementIn (updates, *messageEntity);
        if ( update ) {
          if ( !update->isEmpty() ) {
            messageEntity->copyDataFrom( *update );
            db.update( *messageEntity );
          } else {
            db.remove( *messageEntity );
          }
          it++;
        } else {
          it = result.erase( it );
        }
      }
      
      for (auto it=updates.begin(); it != updates.end(); it++) {
        shared_ptr<T> entity(static_cast<shared_ptr<T>>(*it));
        if ( ! Utilities::findElementIn (current, *entity) ) {
          db.insert( *entity );
          result.push_back( entity );
        }
      }
      
      return result;
    }
  
    // ==========
    // Protected Interface
    // ==========
  private:
    
    // disable createing instances
    Utilities( void );
    Utilities( const Utilities& record );
    ~Utilities( void );
    Utilities& operator=( const Utilities& rhs );
    
  };
}

#endif //__EJIN_UTILITIES_H
