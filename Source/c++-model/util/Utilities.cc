/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Utilities.h"
#include "UsingEjinTypes.h"

#include <cstddef>

#include "HMAC_SHA1.h"

static const std::string base64_chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";
static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

const char* SyncOperationMapping[] = { "add","1", "update","2", "remove","3", NULL };
const char* RoleEnumMapping[] = { "read","0", "write","1", "admin","2", NULL };
const char* MembershipEnumMapping[] = { "invited","0", "accepted","1", "rejected","2", NULL };
const char* DurationPeriodEnumMapping[] = { "day","0", "week","1", "month","2", "year","3", NULL };

inline string toHex( unsigned char* src, int length ) 
{
  // convert digest to hex code
  char* final = new char[length*2 + 1];
  if (! final)
    ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");            

#ifndef __clang_analyzer__
  char hexval[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  for(int j = 0; j < length; j++){
    final[j*2] = hexval[((src[j] >> 4) & 0xF)];
    final[(j*2) + 1] = hexval[(src[j]) & 0x0F];
  }
  final[length*2] = NULL;
#endif
  string result(final);
  delete[] final;
  return result;
}

namespace ejin {
  
  // ==========
  // Public Interface
  // ==========
  
  // see http://www.codeproject.com/KB/recipes/HMACSHA1class.aspx
  string Utilities::sign( const char* origData, size_t dataLength, const char* origKeyBytes ) 
  {
    if (dataLength == 0)
      return "";
    
    assert ( origData ); 
    assert ( origKeyBytes );     
    if (dataLength > (size_t) std::numeric_limits<UINT_32>::max()) {
      ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Data size overflow %ld", dataLength);
    }  
    
    const BYTE* data = reinterpret_cast<const unsigned char*>(origData);
    
    size_t keyLength = strlen(origKeyBytes);
    const BYTE* key = reinterpret_cast<const unsigned char*>(origKeyBytes);
    
    BYTE digest[20] ; 
    
    CHMAC_SHA1 HMAC_SHA1(static_cast<UINT_32>(dataLength));
    HMAC_SHA1.HMAC_SHA1(const_cast<unsigned char*>(data), 
                        static_cast<UINT_32>(dataLength), 
                        const_cast<unsigned char*>(key), 
                        static_cast<UINT_32>(keyLength), 
                        digest) ;    
    
    // convert digest to hex code
    return toHex(digest, 20);
  }
  
  std::vector<char> Utilities::base64_encode( const string& string_to_encode )
  {
    return Utilities::base64_encode( string_to_encode.c_str(), string_to_encode.size() );
  }
  std::vector<char> Utilities::base64_encode( char const* bytes_to_encode, size_t in_len )
  {
    std::vector<char> ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];
    
    while (in_len--) {
      char_array_3[i++] = *(bytes_to_encode++);
      if (i == 3) {
        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;
        
        for(i = 0; (i <4) ; i++)
          ret.push_back( base64_chars[char_array_4[i]] );
        i = 0;
      }
    }
    
    if (i)
    {
      for(j = i; j < 3; j++)
        char_array_3[j] = '\0';
      
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;
      
      for (j = 0; (j < i + 1); j++)
        ret.push_back( base64_chars[char_array_4[j]] );
      
      while((i++ < 3))
        ret.push_back( '=' );
      
    }
    
    return ret;
    
  }
  
  std::vector<char> Utilities::base64_decode( char const* encoded_string, size_t in_len )
  {
    size_t i = 0;
    size_t j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    std::vector<char> ret;
    
    while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
      char_array_4[i++] = encoded_string[in_]; in_++;
      if (i ==4) {
        for (i = 0; i <4; i++)
          char_array_4[i] = static_cast<unsigned char>(base64_chars.find(char_array_4[i]));
        
        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];
        
        for (i = 0; (i < 3); i++)
          ret.push_back( char_array_3[i] );
        i = 0;
      }
    }
    
    if (i) {
      for (j = i; j <4; j++)
        char_array_4[j] = 0;
      
      for (j = 0; j <4; j++)
        char_array_4[j] = static_cast<unsigned char>(base64_chars.find(char_array_4[j]));
      
      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];
      
      for (j = 0; (j < i - 1); j++) ret.push_back( char_array_3[j] );
    }
    
    return ret;
  }
  std::vector<char> Utilities::base64_decode( const std::vector<char>& encoded_string )
  {
    return Utilities::base64_decode( &*encoded_string.begin(), encoded_string.size() );
  }
  std::vector<char> Utilities::base64_decode( const std::string& encoded_string )
  {
    return Utilities::base64_decode( &*encoded_string.begin(), encoded_string.size() );
  }
 
}
