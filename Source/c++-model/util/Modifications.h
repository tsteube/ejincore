/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MODIFICATIONS_H
#define __EJIN_MODIFICATIONS_H

#include "Declarations.h"

#include "KeySequence.h"
#include "SerializableEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  using ser::ISerializableEntity;
  using ser::XmlStream;
  
  // forward declaration
  template <typename T> class RESTContainer;
  
  /**
   * Container class wrapping added, modified and deleted entities of the same business type.
   */ 
  template <typename T>
  class Modifications: public ISerializableEntity {
    friend class RESTContainer<T>;
    template <typename E> friend ostream& operator<<( ostream& ostr, const Modifications<E>& rhs );  
    
    // ==========
    // Ctor / Dtor
    // ==========
  public:
    
    // ctors
    Modifications( void );
    Modifications( list< shared_ptr<T> > added, list< shared_ptr<T> > updated,list< shared_ptr<T> > removed );
    // dtor
    ~Modifications( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Modifications<T>& operator=( const Modifications<T>& rhs );
    // comparision
    bool operator==( const Modifications<T>& rhs ) const;    
    // debug
    string toString( void ) const;
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );
    
    //bool unmarshalJson( json_t* node );
    //bool marshalJson( json_t* node ) const;

    ISerializableEntity* entityByXmlNode( const char* name, int position );
    sqlite::Value* valueByXmlNode( const char* name );
    void endXmlNode( const char* name );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const; 
    bool isEmpty( void ) const { return added_.empty() && updated_.empty() && removed_.empty(); };
    size_t size( void ) const { return added_.size() + updated_.size() + removed_.size(); };
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    const sqlite::jtime getSyncTime( void ) const { return this->syncTime_.date(); }
    void setSyncTime( const sqlite::jtime& syncTime ) { this->syncTime_.date(syncTime); }
    void clearSyncTime( void ) { this->syncTime_.setNull(); }
    bool hasSyncTime( void ) const { return ! this->syncTime_.isNull(); }
    
    const string getSyncAnchor( void ) const { return this->syncAnchor_.toString(); }
    void setSyncAnchor( const string& syncAnchor ) { this->syncAnchor_.str( syncAnchor ); }
    void clearSyncAnchor( void ) { this->syncAnchor_.setNull(); }
    bool hasSyncAnchor( void ) const { return ! this->syncAnchor_.isNull(); }
    
    KeySequence getSyncAnchorKey( void ) const { return KeySequence(this->syncAnchor_.toString()); }
    void setSyncAnchorKey( const KeySequence& syncAnchor ) { this->syncAnchor_.str( syncAnchor ); }
    void clearSyncAnchorKey( void ) { this->syncAnchor_.setNull(); }
    bool hasSyncAnchorKey( void ) const { return ! this->syncAnchor_.isNull(); }
    
    const string getUsername( void ) const { return this->username_.toString(); }
    void setUsername( string username ) { this->username_.str(username); }
    void clearUsername( void ) { this->username_.setNull(); }
    bool hasUsername( void ) const { return ! this->username_.isNull(); }
    
    list< shared_ptr<T> >& getAdded( void ) const   { return this->added_; }
    list< shared_ptr<T> >& getUpdated( void ) const { return this->updated_; }
    list< shared_ptr<T> >& getRemoved( void ) const { return this->removed_; }
    
    void setAdded( const list< shared_ptr<T> >& added )      { this->added_ = added; }
    void setUpdated( const list< shared_ptr<T> >& updated )  { this->updated_ = updated; }
    void setRemoved( const list< shared_ptr<T> >& removed )  { this->removed_ = removed; }
    
    // ==========
    // Protected Interface
    // ==========
  private:
    
    // interal XML parsing state
    int state_;
    
    // Last synchronization time
    sqlite::Value syncTime_;
    
    // Last synchronization anchor
    sqlite::Value syncAnchor_;
    
    // user name for synchronization.
    sqlite::Value username_;
    
    // Collection of all added entities
    mutable list< shared_ptr<T> >  added_;
    
    // Collection of all modified entities
    mutable list< shared_ptr<T> >  updated_;
    
    // Collection of all deleted entities
    mutable list< shared_ptr<T> >  removed_;
    
  };
  
  // debugging
  template <typename T>
  ostream& operator<<( ostream& ostr, const Modifications<T>& rhs );  
  
}

#endif // __EJIN_MODIFICATIONS_H
