/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_BUSINESS_DATA_H
#define __EJIN_BUSINESS_DATA_H

#include "ICryptoEntity.h"

// forward declaration
namespace sqlite
{
  class BaseEntity;
}

namespace ejin
{
  using sqlite::BaseEntity;
  
  /**
   *  Helper interface to compare entities on business rules and copy business data. 
   *  If isDataEqualTo returns false (true) copyDataFrom must also return false (true).
   */
  class IBusinessData: public ICryptoEntity {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    virtual ~IBusinessData( void ) { }    
    
    /** Compare content of given entity to this one */
    virtual bool hasEqualContent( const BaseEntity& rhs ) const = 0;

    /**  Copy business data into the entity. */
    virtual bool copyDataFrom( const BaseEntity& arg ) = 0;

    /** Compare the entity on business data. */
    virtual bool isDataEqualTo( const BaseEntity& arg ) const = 0;
    
    /** Clear business data of the entity. */
    virtual void clearData( void ) = 0;
    
    /** Returns true if the entity is empty. */
    virtual bool isEmpty( void ) const = 0;
    
    /**  Prepare instance for archive. */
    virtual void archive( void ) = 0;
    
    /**  Prepare instance for unarchive. */
    virtual void unarchive( void ) = 0;
    
  };
  
}

#endif //__EJIN_BUSINESS_DATA_H