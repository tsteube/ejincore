/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_EXCEPTION_H
#define __EJIN_EXCEPTION_H

#include <stdexcept>

using std::runtime_error;
using std::string;

// see http://developer.apple.com/library/mac/#technotes/tn2185/_index.html
#define PUBLIC  __attribute__((__visibility__("default")))
#define PRIVATE __attribute__((__visibility__("hidden")))

// ==========
// Exception Classes
// ==========
namespace ejin
{
  enum exception_type
  {
    ACCESS_DENIED_EXCEPTION,
    MEMORY_EXCEPTION,
    CONVERSION_EXCEPTION,
    RETRIEVAL_EXCEPTION,
    CONFLICT_EXCEPTION,
    DATA_INTEGRITY_VIOLATION_EXCEPTION,
    SECURITY_INTEGRITY_VIOLATION_EXCEPTION,
    DATA_ACCESS_EXCEPTION,
    IO_EXCEPTION,
    MAPPING_EXCEPTION
  };
  
  void _throw_exception( exception_type type, const char* msg, ... );
  
  /**
   * Common ejin exception class.
   */
  
  class __attribute__((__visibility__("default"))) io_error: public runtime_error {
  public:
    io_error ( const string &err ) : runtime_error (err) {}
  };
  
  class PUBLIC memory_error: public runtime_error {
  public:
    memory_error ( const string &err ) : runtime_error (err) {}
  };
  
  class PUBLIC access_denied_error: public runtime_error {
  public:
    access_denied_error ( const string &err ) : runtime_error (err) {}
  };
  
  class PUBLIC mapping_error: public runtime_error {
  public:
    mapping_error ( const string &err ) : runtime_error (err) {}
  };
  
  class PUBLIC data_access_error: public runtime_error {
  public:
    data_access_error ( const string &err ) : runtime_error (err) {}
  };
  
  class PUBLIC security_integrity_error: public runtime_error {
  public:
    security_integrity_error ( const string &err ) : runtime_error (err) {}
  };
  
  class PUBLIC conversion_error: public data_access_error {
  public:
    conversion_error ( const string &err ) : data_access_error (err) {}
  };
  
  class PUBLIC data_retrieval_error: public data_access_error {
  public:
    data_retrieval_error ( const string &err ) : data_access_error (err) {}
  };
  
  class PUBLIC conflict_error: public data_access_error {
  public:
    conflict_error ( const string &err ) : data_access_error (err) {}
  };
  
  class PUBLIC data_integrity_error: public data_access_error {
  public:
    data_integrity_error ( const string &err ) : data_access_error (err) {}
  };
}
#endif //__EJIN_EXCEPTION_H
