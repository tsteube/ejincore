/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Exceptions.h"

#include "Declarations.h"
#include <sys/errno.h>

#define USE_EXCEPTIONS 1
namespace ejin
{

extern "C" int errno;
void _throw_exception( exception_type type, const char *fmt, ... ) {
#ifdef USE_EXCEPTIONS
  char *p;
	va_list ap;
	if ((p = (char*)malloc(BUFSIZ*sizeof(char))) == NULL) {
    exit(3);
  }
	va_start(ap, fmt);
	(void) vsnprintf(p, BUFSIZ, fmt, ap);
	va_end(ap);
  
  //errno = EIO;
  // set p error
  //perror(p);
  
  // throw cpp exception class
  string msg( p );
  (void) free(p);
  
  switch (type) {
    case ejin::ACCESS_DENIED_EXCEPTION:
      throw access_denied_error(msg);
    case ejin::MEMORY_EXCEPTION:
      throw memory_error(msg);
    case ejin::CONVERSION_EXCEPTION:
      throw conversion_error(msg);
    case ejin::RETRIEVAL_EXCEPTION:
      throw data_retrieval_error(msg);      
    case ejin::CONFLICT_EXCEPTION:
      throw conflict_error(msg);      
    case ejin::DATA_INTEGRITY_VIOLATION_EXCEPTION:
      throw data_integrity_error(msg);      
    case ejin::SECURITY_INTEGRITY_VIOLATION_EXCEPTION:
      throw security_integrity_error(msg);
    case ejin::IO_EXCEPTION:
      throw io_error(msg);      
    case ejin::MAPPING_EXCEPTION:
      throw mapping_error(msg);      
    case ejin::DATA_ACCESS_EXCEPTION:
    default:
      throw data_access_error(msg);
  }
#endif
}

}