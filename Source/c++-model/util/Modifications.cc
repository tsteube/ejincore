/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Modifications.h"
#include "UsingEjinTypes.h"

#include <sstream>

#include "Chat.h"
#include "User.h"
#include "Member.h"
#include "Media.h"
#include "ChannelHeader.h"
#include "Utilities.h"

// simple state engine for XML parsing
enum ProcessingState {
  START,
  CONTAINER,
  ADD_ENTITY,
  UPDATE_ENTITY,
  REMOVE_ENTITY
};

namespace ejin
{
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  template<typename T>
  Modifications<T>::Modifications( list< shared_ptr<T> > added, list< shared_ptr<T> > updated,list< shared_ptr<T> > removed ):
  state_(START), syncTime_(type_time), syncAnchor_(), username_(), added_(added), updated_(updated), removed_(removed) { }
  
  template<typename T>
  Modifications<T>::Modifications( void ):
  state_(START), syncTime_(type_time), syncAnchor_(), username_(), added_(), updated_(), removed_() { }
  
  template<typename T>
  Modifications<T>::~Modifications( void ) 
  { 
    syncAnchor_.setNull();
    username_.setNull();
    added_.clear();
    updated_.clear();
    removed_.clear();
  }
  
  // ==========
  // Object Interface
  // ==========
  
  template<typename T>
  Modifications<T>& Modifications<T>::operator=( const Modifications<T>& rhs ) 
  {
    if (this != &rhs) {
      this->syncTime_ = rhs.syncTime_;
      this->setAdded( rhs.getAdded() );
      this->setUpdated( rhs.getUpdated() );
      this->setRemoved( rhs.getRemoved() );
    }
    return *this;
  }
  
  template<typename T>
  bool Modifications<T>::operator==( const Modifications& rhs ) const
  {
    if (this == &rhs) 
      return true;
    if (this->syncAnchor_ != rhs.syncAnchor_)
      return false;
    if (this->username_ != rhs.username_)
      return false;
    if (this->added_.size() != rhs.added_.size())
      return false;
    if (this->updated_.size() != rhs.updated_.size())
      return false;
    if (this->removed_.size() != rhs.removed_.size())
      return false;
    return true;
  }
  
  template<typename T>
  string Modifications<T>::toString( void ) const
  {
    ostringstream buf;
    buf << "Modifications" << "["
    << "username=" << this->username_.toString()
    << ",syncTime=" << this->syncTime_.toString()
    << ",syncAnchor=" << this->syncAnchor_.toString()
    << "]{"
    << ",#add=" << this->added_.size()
    << ",#up=" << this->updated_.size()
    << ",#rem=" << this->removed_.size()
    << "}";
    return buf.str();
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  template<typename T>
  bool Modifications<T>::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() && j.contains("operations") ) {
      json op = j["operations"];
      if ( op.contains("anchor")) {
        this->setSyncAnchor( op["anchor"] );
      }
     
      if ( op.contains("add")) {
        json j2 = op["add"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<T> entity = Utilities::findOrAppend( this->getAdded(), i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }
      if ( op.contains("update")) {
        json j2 = op["update"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<T> entity = Utilities::findOrAppend( this->getUpdated(), i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }
      if ( op.contains("remove")) {
        json j2 = op["remove"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<T> entity = Utilities::findOrAppend( this->getRemoved(), i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      return true;
    }
    return false;
  }
  /*
  template<typename T>
  bool Modifications<T>::unmarshalJson( json_t* node )
  {
    assert( node );
    
    size_t index;
    json_t* value;
    json_t* operations = json_object_get(node, "operations");
    if (json_is_object(operations))
    {
      node = json_object_get(operations, "anchor");
      if (json_is_string(node)) {
        this->setSyncAnchor( json_string_value(node) );
      }
      node = json_object_get(operations, "add");
      if (json_is_array(node)) {
        json_array_foreach(node, index, value) {
          shared_ptr<T> entity = Utilities::findOrAppend( this->getAdded(), index );
          if (!entity || ! entity->unmarshalJson( value )) {
            return false;
          }
        }
      }
      node = json_object_get(operations, "update");
      if (json_is_array(node)) {
        json_array_foreach(node, index, value) {
          shared_ptr<T> entity = Utilities::findOrAppend( this->getUpdated(), index );
          if (!entity || ! entity->unmarshalJson( value )) {
            return false;
          }
        }
      }
      node = json_object_get(operations, "remove");
      if (json_is_array(node)) {
        json_array_foreach(node, index, value) {
          shared_ptr<T> entity = Utilities::findOrAppend( this->getRemoved(), index );
          if (!entity || ! entity->unmarshalJson( value )) {
            return false;
          }
        }
      }
      return true;
      
    } else {
      return false;
    }
  }
  */

  template<typename T>
  bool Modifications<T>::marshalJson( nlohmann::json& j ) const
  {
    json op;
    if ( this->hasSyncAnchor() ) {
      op["sync_anchor"] = string(this->getSyncAnchor().c_str());
    }
    if ( ! this->added_.empty() ) {
      json a = json::array();
      for (typename list< shared_ptr<T> >::const_iterator it=this->added_.begin();
           it != this->added_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      op["add"] = a;
    }
    if ( ! this->updated_.empty() ) {
      json a = json::array();
      for (typename list< shared_ptr<T> >::const_iterator it=this->updated_.begin();
           it != this->updated_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      op["update"] = a;
    }
    if ( ! this->removed_.empty() ) {
      json a = json::array();
      for (typename list< shared_ptr<T> >::const_iterator it=this->removed_.begin();
           it != this->removed_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      op["remove"] = a;
    }
    j["operations"] = op;

    return true;
  }
  /*
  template<typename T>
  bool Modifications<T>::marshalJson( json_t* node ) const
  {
    assert( node );
    
    json_t* operations = json_object();
    json_object_set_new( node, "operations", operations );

    if (this->hasSyncAnchor()) {
      json_object_set_new( operations, "sync_anchor", json_string( this->getSyncAnchor().c_str() ) );
    }
    if (! this->added_.empty()) {
      json_t* added = json_array();
      for (typename list< shared_ptr<T> >::const_iterator it=this->added_.begin(); it != this->added_.end(); it++) {
        json_t* child = json_object();
        if ((*it)->marshalJson( child )) {
          json_array_append_new( added, child );
        }
      }
      json_object_set_new( operations, "add", added );
    }
    if (! this->updated_.empty()) {
      json_t* updated = json_array();
      for (typename list< shared_ptr<T> >::const_iterator it=this->updated_.begin(); it != this->updated_.end(); it++) {
        json_t* child = json_object();
        if ((*it)->marshalJson( child )) {
          json_array_append_new( updated, child );
        }
      }
      json_object_set_new( operations, "update", updated );
    }
    if (! this->removed_.empty()) {
      json_t* removed = json_array();
      for (typename list< shared_ptr<T> >::const_iterator it=this->removed_.begin(); it != this->removed_.end(); it++) {
        json_t* child = json_object();
        if ((*it)->marshalJson( child )) {
          json_array_append_new( removed, child );
        }
      }
      json_object_set_new( operations, "remove", removed );
    }
    return true;
  }
  */

  // implements a simple state engine to handle entities for add, update and remove
  template<typename T>
  ISerializableEntity* Modifications<T>::entityByXmlNode( const char* name, int position ) 
  {
    // operations@anchor="1"
    switch (this->state_) {
      case START:
      {
        if (strcmp(name, "operations") == 0) 
          this->state_ = CONTAINER;
        return this;
      }
      case CONTAINER:
      {
        if (strcmp(name, "add") == 0) 
          this->state_ = ADD_ENTITY;
        else if (strcmp(name, "update") == 0) 
          this->state_ = UPDATE_ENTITY;
        else if (strcmp(name, "remove") == 0) 
          this->state_ = REMOVE_ENTITY;
        return this;
      }
      case ADD_ENTITY:
      {
        shared_ptr<T> entity(shared_ptr<T>(new T()));
        this->added_.push_back(entity);
        return entity.get();
      }
      case UPDATE_ENTITY:
      {
        shared_ptr<T> entity(shared_ptr<T>(new T()));
        this->updated_.push_back(entity);
        return entity.get();
      }
      case REMOVE_ENTITY:
      {
        shared_ptr<T> entity(shared_ptr<T>(new T()));
        this->removed_.push_back(entity);
        return entity.get();
      }
    }
    return NULL;
  }
  
  template<typename T>
  Value* Modifications<T>::valueByXmlNode( const char* name ) 
  {
    if (this->state_ == CONTAINER) {
      if (strcmp(name, "anchor") == 0) { 
        return &this->syncAnchor_;
      }
      if (strcmp(name, "sync_time") == 0) { 
        return &this->syncTime_;
      }
    }
    return NULL; 
  }
  
  // update state engine
  template<typename T>
  void Modifications<T>::endXmlNode( const char* name ) 
  {  
    switch (this->state_) {
      case ADD_ENTITY:
        if (strcmp(name, "add") == 0) {
          this->state_ = CONTAINER;
        }
        break;
      case UPDATE_ENTITY:
        if (strcmp(name, "update") == 0) {
          this->state_ = CONTAINER;
        }
        break;
      case REMOVE_ENTITY:
        if (strcmp(name, "remove") == 0) {
          this->state_ = CONTAINER;
        }
        break;
      case CONTAINER:
        if (strcmp(name, "operations") == 0) {
          this->state_ = START;
        }
        break;        
      case START:
        break;
    }
  }
  
  // generate core XML structure
  template <typename T>
  bool Modifications<T>::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const 
  {
    xml << ser::prolog("utf-8");
    xml << tag("operations") // start root tag
    << attr("anchor") << this->getSyncAnchor(); // 1. level attribute
    
    // print out added entities if any
    if (! this->added_.empty()) {
      xml << ser::tag("add");
      for (typename list< shared_ptr<T> >::const_iterator it=this->added_.begin(); it != this->added_.end(); it++) {
        (*it)->marshalXml( xml, aesKey, keyLength );
      }
      xml << endtag();
    }
    
    // print out updated entities if any
    if (! this->updated_.empty()) {
      xml << tag("update");
      for (typename list< shared_ptr<T> >::const_iterator it=this->updated_.begin(); it != this->updated_.end(); it++) {
        (*it)->marshalXml( xml, aesKey, keyLength );
      }
      xml << endtag();
    }
    
    // print out added entities if any
    if (! this->removed_.empty()) {
      xml << tag("remove");
      for (typename list< shared_ptr<T> >::const_iterator it=this->removed_.begin(); it != this->removed_.end(); it++) {
        (*it)->marshalXml( xml, aesKey, keyLength );
      }
      xml << endtag();
    }
    
    xml << endtag("operations"); // end root tag
    
    return true;
  }
  
  // ==========
  // Debugging
  // ==========
  
  template<typename T>
  ostream& operator<<( ostream& ostr, const Modifications<T>& rhs ) {
    ostr << rhs.toString();
    return ostr;
  }
  
  // template instantiation
  template class Modifications<Member>;
  template class Modifications<Media>; 
  template class Modifications<ChannelHeader>;
  template class Modifications<Chat>;
  template class Modifications<User>;
  
  template ostream& operator<<( ostream& ostr, const Modifications<Member>& rhs );
  template ostream& operator<<( ostream& ostr, const Modifications<Media>& rhs );
  template ostream& operator<<( ostream& ostr, const Modifications<ChannelHeader>& rhs );
  template ostream& operator<<( ostream& ostr, const Modifications<Chat>& rhs );
}


