/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_RESULT_LIST_H
#define __EJIN_RESULT_LIST_H

#include "SqliteBaseEntity.h"

#include "SerializableEntity.h"

namespace ejin
{
  using ser::ISerializableEntity;
  using ser::XmlStream;
  
  /**
   * Container class wrapping of search result of the same business type.
   */ 
  template <typename T>
  class ResultList: public ISerializableEntity {
    template <typename E> friend ostream& operator<<( ostream& ostr, const ResultList<T>& rhs );  
    
    // ==========
    // Ctor / Dtor
    // ==========
  public:
    
    // ctors
    ResultList( void );
    ResultList( list< shared_ptr<T> > list );
    // dtor
    ~ResultList( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ResultList<T>& operator=( const ResultList<T>& rhs );
    // comparision
    bool operator==( const ResultList<T>& rhs ) const;    
    // debug
    string toString( void ) const;
    
    bool isEmpty( void ) const { return _list.empty(); };
    size_t size( void ) const { return _list.size(); };
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool unmarshalJson( const nlohmann::json& node );
    //bool unmarshalJson( json_t* node );

    ISerializableEntity* entityByXmlNode( const char* name, int position );
    sqlite::Value* valueByXmlNode( const char* name );
    void endXmlNode( const char* name );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const; 
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    list< shared_ptr<T> >& getSet( void ) const   { return this->_list; }
    void setSet( const list< shared_ptr<T> >& list )      { this->_list = list; }
    
    const string getSyncAnchor( void ) const { return this->syncAnchor_.toString(); }
    void setSyncAnchor( const string& syncAnchor ) { this->syncAnchor_.str( syncAnchor ); }
    void clearSyncAnchor( void ) { this->syncAnchor_.setNull(); }
    bool hasSyncAnchor( void ) { return ! this->syncAnchor_.isNull(); }

    // ==========
    // Protected Interface
    // ==========
  private:
    
    // result list
    mutable list< shared_ptr<T> >  _list;
    
    // Last synchronization anchor
    sqlite::Value syncAnchor_;

  };
  
  // debugging
  template <typename T>
  ostream& operator<<( ostream& ostr, const ResultList<T>& rhs );
  
}

#endif // __EJIN_RESULT_LIST_H
