/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ResultList.h"
#include "UsingEjinTypes.h"

#include <sstream>

#include "Utilities.h"
#include "User.h"
#include "Member.h"
#include "Media.h"
#include "ChannelHeader.h"
#include "Message.h"
#include "Product.h"

namespace ejin
{
  // ==========
  // Ctor / Dtor
  // ==========
  
  template<typename T>
  ResultList<T>::ResultList( list< shared_ptr<T> > list ): _list(list) { }
  
  template<typename T>
  ResultList<T>::ResultList( void ): _list() { }
  
  template<typename T>
  ResultList<T>::~ResultList( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  template<typename T>
  ResultList<T>& ResultList<T>::operator=( const ResultList<T>& rhs ) 
  {
    if (this != &rhs) {
      this->setSet( rhs.getSet() );
    }
    return *this;
  }
  
  template<typename T>
  bool ResultList<T>::operator==( const ResultList& rhs ) const
  {
    if (this == &rhs) 
      return true;
    if (this->_list.size() != rhs._list.size())
      return false;
    return true;
  }
  
  template<typename T>
  string ResultList<T>::toString( void ) const
  {
    ostringstream buf;
    buf << "ResultList" << "["
    << ",#result=" << this->_list.size()
    << "}";
    return buf.str();
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  template<typename T>
  bool ResultList<T>::unmarshalJson( const nlohmann::json& j )
  {
    // unmarshal posts
    if ( j.contains("result_set")) {
      json j2 = j["result_set"];
      if ( j2.is_array() ) {
        for (int i=0; i<j2.size(); i++) {
          shared_ptr<T> entity = Utilities::findOrAppend( this->getSet(), i );
          if (!entity || ! entity->unmarshalJson( j2[i] )) {
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }
  /*
  template<typename T>
  bool ResultList<T>::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* resultSet = json_object_get(node, "result_set");
    if (json_is_array(resultSet))
    {
      size_t index;
      json_t* value;
      json_array_foreach(resultSet, index, value) {
        shared_ptr<T> entity = Utilities::findOrAppend( this->getSet(), index );
        if (!entity || ! entity->unmarshalJson( value )) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  */

  // implements a simple state engine to handle entities for add, update and remove
  template<typename T>
  ISerializableEntity* ResultList<T>::entityByXmlNode( const char* name, int position ) 
  {
    if (strcmp(name, "result_set") == 0) 
      return this;
    
    this->_list.push_back( shared_ptr<T>(new T()) );
    return this->_list.back().get();
  }
  
  template<typename T>
  Value* ResultList<T>::valueByXmlNode( const char* name ) 
  {
    return NULL; 
  }
  
  // update state engine
  template<typename T>
  void ResultList<T>::endXmlNode( const char* name ) 
  {  
  }
  
  // generate core XML structure
  template <typename T>
  bool ResultList<T>::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const 
  {
    xml << ser::prolog("utf-8");
    xml << tag("result_set"); // start root tag
    
    // print out added entities if any
    if (! this->_list.empty()) {
      for (typename list< shared_ptr<T> >::const_iterator it=this->_list.begin(); it != this->_list.end(); it++) {
        (*it)->marshalXml( xml, aesKey, keyLength );
      }
    }
    
    xml << endtag("result_set"); // end root tag
    
    return true;
  }
  
  // ==========
  // Debugging
  // ==========
  
  template<typename T>
  ostream& operator<<( ostream& ostr, const ResultList<T>& rhs ) {
    ostr << rhs.toString();
    return ostr;
  }
  
  // template instantiation
  template class ResultList<User>;
  template class ResultList<Media>; 
  template class ResultList<ChannelHeader>;
  template class ResultList<Message>;
  template class ResultList<Product>;

  template ostream& operator<<( ostream& ostr, const ResultList<User>& rhs );
  template ostream& operator<<( ostream& ostr, const ResultList<Media>& rhs );
  template ostream& operator<<( ostream& ostr, const ResultList<ChannelHeader>& rhs );
  template ostream& operator<<( ostream& ostr, const ResultList<Message>& rhs );
  template ostream& operator<<( ostream& ostr, const ResultList<Product>& rhs );

}


