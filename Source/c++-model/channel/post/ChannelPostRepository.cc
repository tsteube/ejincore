/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPostRepository.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "ChannelHeader.h"
#include "ChannelMedia.h"
#include "ChannelPost.h"
#include "ChannelCommentRepository.h"
#include "ChannelMediaRepository.h"
#include "ChangeLogRepository.h"
#include "SqliteValue.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool ChannelPostRepository::detectConflict( const string& postGid ) const
  throw(data_access_error)
  {
    assert (! postGid.empty());
    
    return (bool) this->loadChannelPostInternal( Value(postGid.c_str()), Value(true) );
  }
  
  bool ChannelPostRepository::clearConflict( const string& postGid )
  throw(data_access_error)
  {
    assert (! postGid.empty());
    shared_ptr<ChannelPost> channelPostEntity( this->loadChannelPostInternal( Value(postGid.c_str()), Value(false) ) );
    shared_ptr<ChannelPost> conflict( this->loadChannelPostInternal( Value(postGid.c_str()), Value(true) ) );
    
    bool cleared = false;
    if ( conflict ) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      if ( channelPostEntity ) {
        if ( channelPostEntity->isEmpty() ) {
          this->db_.remove( *channelPostEntity );
        } else {
          channelPostEntity->setSyncInd( kSyncIndSynchronous );
          channelPostEntity->setModificationMask( kNoneModification );
          this->db_.update( *channelPostEntity );
        }
      }
      cleared = true;
    }
    return cleared;
  }
  
  bool ChannelPostRepository::update( ChannelHeader& channelHeader, list< shared_ptr<ChannelPost> > channelPosts, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasId() );
    assert( channelHeader.hasOwner() );
    
    if (channelPosts.empty())
      return false;
    
    // call entity finder
    Value hidden(false);
    Value channelId(channelHeader.getId());
    unique_ptr<ResultSet> allChannelPostEntities(db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelMediaEntities( db_.find(ChannelMedia::TABLE_NAME, "findAllChannelResources", &channelId, &hidden, NULL) );
    
    // update algorithm
    bool updated = false;
    unique_ptr<ChannelPost> clone;
    list< shared_ptr<ChannelPost> > channelPostsToUpdate( channelPosts );
    
    for (vector< BaseEntity* >::iterator it=allChannelPostEntities->begin(); it != allChannelPostEntities->end(); it++) {
      ChannelPost* channelPostEntity(static_cast<ChannelPost*>(*it));
      
      shared_ptr<ChannelPost> channelPost = Utilities::findElementIn (channelPostsToUpdate, *channelPostEntity);
      if ( ! channelPost )
      {
        // no change request on channel post
        continue;
      }
      assert( channelPost->hasGid() );
      if ( channelPost->getOperation() == kSyncRemove )  {
        channelPostEntity->setModifiedBy( channelPost->getModifiedBy() );
        // delete post entity
        if ( removeChannelPost( *channelPostEntity, allChannelMediaEntities.get() ) ) {
          clearDeleteOwnerInd( *channelPostEntity );
          this->changeLogRepository_.remove( *channelPostEntity, channelHeader.getSyncAnchor(), syncTime );
          updated = true;
        }
      } else {
        // update post entity
        if ( updateChannelPost( *channelPostEntity, *channelPost, allChannelMediaEntities.get() ) ) {
          clearDeleteOwnerInd( *channelPostEntity );
          this->changeLogRepository_.update( *channelPostEntity, channelHeader.getSyncAnchor(), syncTime );
          updated = true;
        }
      }
      channelPostsToUpdate.remove(channelPost);
    }
    
    if ( !channelPostsToUpdate.empty() )
    {
      for (list< shared_ptr<ChannelPost> >::iterator postIt = channelPostsToUpdate.begin(); postIt != channelPostsToUpdate.end(); postIt++) {
        (*postIt)->setChannelId( channelHeader.getId() );
        shared_ptr<ChannelPost> channelPostEntity( addChannelPost( **postIt, allChannelMediaEntities.get(), channelHeader.getSyncAnchor() ));
        if ( channelPostEntity ){
          clearDeleteOwnerInd( *channelPostEntity );
          if (channelHeader.hasLastSyncTime()) {
            // no tracking on first sync
            this->changeLogRepository_.add( *channelPostEntity, channelHeader.getSyncAnchor(), syncTime );
          }
          updated = true;
        }
      }
    }

    return updated;
  }
  
  void ChannelPostRepository::deleteAllChannelPosts( integer channel_Id )
  throw(data_access_error)
  {
    assert( channel_Id > 0 );
    
    // call entity finder
    Value channelId(channel_Id);
    Value hidden(false);
    unique_ptr<ResultSet> allChannelPostEntities(db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelPostMediaEntities( db_.find(ChannelMedia::TABLE_NAME, "findAllChannelPostResources", &channelId, &hidden, NULL) );
    
    for (vector< BaseEntity* >::iterator it = allChannelPostEntities->begin(); it != allChannelPostEntities->end(); it++) {
      ChannelPost* postEntity(static_cast<ChannelPost*>(*it));
      removeChannelPost( *postEntity, allChannelPostMediaEntities.get() );
    }
    
  }
  
  void ChannelPostRepository::commit( ChannelHeader& channelHeader, list< shared_ptr<ChannelPost> > channelPosts, const string& syncAnchor, const string& username, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasId() );
    assert( ! syncAnchor.empty() );
    
    if (channelPosts.empty())
      return;
    
    // call entity finder
    Value hidden(false);
    Value channelId(channelHeader.getId());
    unique_ptr<ResultSet> allChannelPostEntities(db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelPostMediaEntities( db_.find(ChannelMedia::TABLE_NAME, "findAllChannelPostResources", &channelId, &hidden, NULL) );
    
    for (vector< BaseEntity* >::iterator it=allChannelPostEntities->begin(); it != allChannelPostEntities->end(); it++) {
      ChannelPost* channelPostEntity(static_cast<ChannelPost*>(*it));
      
      shared_ptr<ChannelPost> post = Utilities::findElementIn (channelPosts, *channelPostEntity);
      if ( ! post )
      {
        // no change request on channel post
        continue;
      }
      
      assert( post->hasId() );
      assert( post->hasGid() );
      
      channelPostEntity->setModifyTime( syncTime );
      SyncInd syncInd = (SyncInd) channelPostEntity->getSyncInd();
      switch ( syncInd )
      {
        case kSyncIndSynchronous:
          channelPostEntity->setModifiedBy( username );
          channelPostEntity->setModifiedBy( username );
          this->db_.update( *channelPostEntity );
          break;
        case kSyncIndConflict:
          channelPostEntity->setModifiedBy( username );
          channelPostEntity->setModificationMask( kChannelPostConflictedModification );
          this->db_.update( *channelPostEntity );
          this->changeLogRepository_.update( *channelPostEntity, syncAnchor, syncTime );
          break;
        case kSyncIndInsert:
          // update global identifier
          channelPostEntity->setGid( post->getGid() );
          channelPostEntity->setCreateTime( syncTime );
          channelPostEntity->setSecure( post->isSecure() );
        case kSyncIndUpdate:
          channelPostEntity->setIV( post->getIV() );
          channelPostEntity->setSyncInd( kSyncIndSynchronous );
          channelPostEntity->setModifiedBy( username );
          channelPostEntity->setModificationMask( kNoneModification );
          this->db_.update( *channelPostEntity );
          if ( syncInd == kSyncIndInsert ) {
            this->changeLogRepository_.add( *channelPostEntity, syncAnchor, syncTime );
          } else {
            this->changeLogRepository_.update( *channelPostEntity, syncAnchor, syncTime );
          }
          break;
        case kSyncIndDelete:
          channelPostEntity->setModifiedBy( username );
          this->removeChannelPost( *channelPostEntity, allChannelPostMediaEntities.get() );
          this->changeLogRepository_.remove( *channelPostEntity, syncAnchor, syncTime );
          continue;
      }
      
      // commit resources
      for (vector< BaseEntity* >::iterator mediaEntityIt=allChannelPostMediaEntities->begin(); mediaEntityIt != allChannelPostMediaEntities->end(); mediaEntityIt++) {
        ChannelMedia* mediaEntity(dynamic_cast<ChannelMedia*>(*mediaEntityIt)); // up cast
        if (mediaEntity->getPostId() && channelPostEntity->getId() == mediaEntity->getPostId()) {
          shared_ptr<Resource> media = Utilities::findElementIn (post->getResources(),
                                                                 *static_cast<Resource*>(mediaEntity)); // down cast
          //assert( media.get() );
          this->channelMediaRepository_.commit( *mediaEntity, dynamic_cast<ChannelMedia*>(media.get()),
                                               syncAnchor, username, syncTime );
        }
      }
      
    }
    
  }
  
  bool ChannelPostRepository::revert( const string& gid, bool force )
  throw(data_access_error)
  {
    assert( ! gid.empty() );
    
    bool reverted = false;
    Value gidValue(gid.c_str());
    shared_ptr<ChannelPost> channelPostEntity( this->loadChannelPostInternal( gidValue, Value(false) ) );
    if ( channelPostEntity != NULL )
    {
      // ensure that there a no pending conflicts
      if ( clearConflict( gid ) ) {
        // reload after conflict has been cleared
        channelPostEntity = this->loadChannelPostInternal( gidValue, Value(false) );
      }
      
      switch ( channelPostEntity->getSyncInd() ) {
        case kSyncIndInsert:
        {
          if (force) {
            db_.remove( *channelPostEntity );
            reverted = true;
          } else {
            cloneChannelPost( *channelPostEntity, NULL);
            reverted = true;
          }
          break;
        }
        case kSyncIndSynchronous:
        case kSyncIndUpdate:
        case kSyncIndDelete:
        {
          // load entity from backup
          shared_ptr<ChannelPost> channelPostEntityBak( shared_ptr<ChannelPost>(new ChannelPost()) );
          channelPostEntityBak->setId(channelPostEntity->getId());
          db_.refresh(*channelPostEntityBak, channelPostEntityBak->backupSchemaName() );
          
          if ( force ) {
            // force update entity with data from backup
            channelPostEntity->copyDataFrom( *channelPostEntityBak );
            channelPostEntity->setSyncInd( kSyncIndSynchronous );
            channelPostEntity->setModificationMask( kNoneModification );
            this->db_.update( *channelPostEntity );
            reverted = true;
          } else {
            reverted = cloneChannelPost( *channelPostEntity,
                                        channelPostEntityBak.get() );
          }
          break;
        }
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "post[id=%d] in conflict state", channelPostEntity->getId());
      }
      
      // revert new dependent entities
      if (channelCommentRepository_.revertNewComments( channelPostEntity->getChannelId(), channelPostEntity->getId(), force )) {
        reverted = true;
      }
      if (channelMediaRepository_.revertNewMedias( channelPostEntity->getChannelId(), channelPostEntity->getId(), 0, force )) {
        reverted = true;
      }
      
    } else {
      // restore old member if found in backup
      unique_ptr<ResultSet> values( db_.find(ChannelPost::TABLE_NAME, "findPostByGidBak", &gidValue, NULL) );
      vector< BaseEntity* >::iterator it = values->begin();
      if (it != values->end()) {
        this->db_.insert( **it );
        reverted = true;
      }
      
    }
    return reverted;
  }
  
  bool ChannelPostRepository::revertNewPosts( integer channelId, bool force )
  throw(data_access_error)
  {
    bool reverted = false;
    Value id(channelId);
    Value hidden(false);
    unique_ptr<ResultSet> values( db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &id, &hidden, NULL) );
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); it++) {
      ChannelPost* channelPostEntity(static_cast< ChannelPost* >(*it));
      if ( channelPostEntity->getSyncInd() == kSyncIndInsert ) {
        if (force) {
          db_.remove( *channelPostEntity );
          reverted = true;
        } else {
          cloneChannelPost(*channelPostEntity, NULL);
          reverted = true;
        }
      }
    }
    return reverted;
  }
  
  bool ChannelPostRepository::deleteNewPost( integer id )
  throw(data_access_error)
  {
    bool deleted = false;
    
    Value postId( id );
    unique_ptr<ResultSet> values( db_.find(ChannelPost::TABLE_NAME, "findPostById", &postId, NULL) );
    vector< sqlite::BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      shared_ptr<ChannelPost> channelPostEntity( shared_ptr<ChannelPost>(static_cast< ChannelPost* >(*it)) );
      it = values->take(it);
      if ( !channelPostEntity->hasGid() ) {
        db_.remove( *channelPostEntity );
        deleted = true;
      }
    }
    return deleted;
  }
  
  // ==========
  // Private Interface
  // ==========
  
  shared_ptr<ChannelPost> ChannelPostRepository::addChannelPost( ChannelPost& channelPost, ResultSet* allChannelMediaEntities, const string& syncAnchor )
  throw(data_access_error)
  {
    assert( channelPost.hasGid() );
    if ( channelPost.isEmpty() ) {
      return shared_ptr<ChannelPost>();
    }
    
    // create new entity with unique global key
    shared_ptr<ChannelPost> channelPostEntity = shared_ptr<ChannelPost>(new ChannelPost( channelPost ));
    channelPostEntity->clearId(); // generate new local database identifier
    channelPostEntity->setSyncInd( kSyncIndSynchronous );
    channelPostEntity->setModifiedBy( channelPost.getOwner() );
    channelPostEntity->setContentModifyTime( channelPost.getModifyTime() );
    channelPostEntity->setModificationMask( kChannelPostRecentRemoteModification );
    channelPostEntity->getResources().clear(); // will be created later
    
    // persist entity
    this->db_.insert( *channelPostEntity );
    
    // set id
    channelPost.setId( channelPostEntity->getId() );
    channelPost.setSyncInd( channelPostEntity->getSyncInd() );
    channelPost.setModifiedBy( channelPostEntity->getModifiedBy() );

    updateResources( channelPost, allChannelMediaEntities );
    
    return channelPostEntity;
  }
  
  bool ChannelPostRepository::updateChannelPost( ChannelPost& channelPostEntity, ChannelPost& channelPost, ResultSet* allChannelPostMediaEntities )
  throw(data_access_error)
  {
    assert( channelPostEntity.hasId() );
    assert( ! channelPostEntity.isHidden() );
    assert( channelPost.hasGid() );
    
    bool result = false;
    switch ( channelPostEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // must be unknown on the server
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of ChannelPost[id=%d]", channelPostEntity.getId());
      case kSyncIndSynchronous:
        // entity was updated on the server
        if ( channelPost.hasContent() && ! channelPostEntity.hasEqualContent( channelPost ) ) {
          channelPostEntity.setModificationMask(kChannelPostRecentRemoteModification);
          result = true;
        }
        channelPostEntity.copyDataFrom( channelPost );
        this->db_.update( channelPostEntity );
        break;
      case kSyncIndUpdate:
        if (channelPost.hasContent()) {
          if ( channelPostEntity.hasEqualContent( channelPost ) )
          {
            // update only some dates and state data
            channelPostEntity.copyDataFrom( channelPost );
            channelPostEntity.setSyncInd( kSyncIndSynchronous );
            channelPostEntity.setModificationMask( kNoneModification );
            this->db_.update( channelPostEntity );
            result = true;
          }
          else
          {
            // conflict detected; save local entity for reference
            cloneChannelPost( channelPostEntity, &channelPost );
            result = true;
          }
        }
        break;
      case kSyncIndDelete:
        if ( channelPostEntity.hasEqualContent( channelPost ) )
        {
          // no change on server side but removed on client
        }
        else
        {
          // conflict detected; save local entity for reference
          cloneChannelPost( channelPostEntity, &channelPost );
          result = true;
        }
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelPost> conflict( this->loadChannelPostInternal( Value(channelPostEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelPostEntity.getId());
        }
        if ( conflict->hasEqualContent( channelPost ) )
        {
          // resolve conflict
          switch ( conflict->getSyncInd() ) {
            case kSyncIndSynchronous:
            case kSyncIndConflict:
              _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[postId=%d]", channelPostEntity.getId());
            case kSyncIndInsert:
            case kSyncIndUpdate:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // client and server are in sync now again
              // update only some dates and state data
              channelPostEntity.copyDataFrom( channelPost );
              channelPostEntity.setSyncInd( kSyncIndSynchronous );
              channelPostEntity.setModificationMask( kNoneModification );
              this->db_.update( channelPostEntity );
              result = true;
              break;
            case kSyncIndDelete:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // remember that the member was already deleted on the client
              channelPostEntity.setSyncInd( kSyncIndDelete );
              this->db_.update( channelPostEntity );
              result = true;
              break;
          }
        } else {
          // keep original local conflict data
          // and mark entity as removed from server
          channelPostEntity.copyDataFrom( channelPost );
          channelPostEntity.setSyncInd( kSyncIndConflict );
          this->db_.update( channelPostEntity );
          result = true;
        }
        break;
    }
    
    // update id
    channelPost.setId( channelPostEntity.getId() );
    channelPost.setChannelId( channelPostEntity.getChannelId() );
    
    // check for ordering changes first
    if (allChannelPostMediaEntities) {
      if (! Utilities::hasEqualOrdering( channelPost.getResources(), *allChannelPostMediaEntities )) {
        channelPostEntity.setModificationMask( channelPostEntity.getModificationMask() | kChannelPostRecentRemoteModification );
        this->db_.update( channelPostEntity );
      }
    }
    
    // update resources
    updateResources( channelPost, allChannelPostMediaEntities );
    
    return result;
  }
  
  bool ChannelPostRepository::removeChannelPost( ChannelPost& channelPostEntity, ResultSet* allChannelPostMediaEntities )
  throw(data_access_error)
  {
    assert( channelPostEntity.hasId() );
    assert( ! channelPostEntity.isHidden() );
    
    switch ( channelPostEntity.getSyncInd() ) {
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        cloneChannelPost( channelPostEntity, NULL );
        break;
      case kSyncIndInsert:
        // new comment on client
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        deleteChannelPost( channelPostEntity, allChannelPostMediaEntities );
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelPost> conflict( this->loadChannelPostInternal( Value(channelPostEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[postId=%d]", channelPostEntity.getId());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndConflict:
          case kSyncIndInsert:
            _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[postId=%d]", channelPostEntity.getId());
          case kSyncIndDelete:
            // entity was removed on client and server
            this->db_.remove( *conflict );
            deleteChannelPost( channelPostEntity, allChannelPostMediaEntities );
            break;
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            // and mark entity as removed from server
            channelPostEntity.clearData();
            channelPostEntity.setSyncInd( kSyncIndConflict );
            this->db_.update( channelPostEntity );
            break;
        }
        break;
    }
    return true;
  }
  
  void ChannelPostRepository::deleteChannelPost( ChannelPost& channelPostEntity, ResultSet* allChannelPostMediaEntities )
  throw(data_access_error)
  {
    // 1. delete resources
    channelPostEntity.getResources().clear();
    this->updateResources( channelPostEntity, allChannelPostMediaEntities );
    
    // 2. remove post of this posts
    this->channelCommentRepository_.deleteAllPostComments( channelPostEntity.getChannelId(), channelPostEntity.getId() );
    
    // 3. delete entity itself
    this->db_.remove( channelPostEntity );
  }
  
  bool ChannelPostRepository::cloneChannelPost( ChannelPost& channelPostEntity, const ChannelPost* channelPost )
  throw(data_access_error)
  {
    if ( channelPost == NULL || ! channelPostEntity.hasEqualContent( *channelPost ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<ChannelPost> clone = unique_ptr<ChannelPost>(new ChannelPost( channelPostEntity ));
      clone->clearId( );
      clone->setMasterId( channelPostEntity.getId() );
      clone->setModificationMask( clone->getModificationMask()|kChannelPostConflictedModification );
      
      // update only some dates and states
      if ( channelPost )
      {
        // update dates and states
        channelPostEntity.copyDataFrom( *channelPost );
      }
      else
      {
        // mark entity as removed from server
        channelPostEntity.clearData();
      }
      channelPostEntity.setSyncInd( kSyncIndConflict );
      channelPostEntity.setModificationMask( kChannelPostConflictedModification|kChannelPostRecentRemoteModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( channelPostEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      return true;
    }
    return false;
  }
  
  bool ChannelPostRepository::updateResources( ChannelPost& channelPost, ResultSet* allChannelPostMediaEntities )
  throw(data_access_error)
  {
    bool updated = false;
    list< shared_ptr<Resource> > resources(channelPost.getResources());
    
    if (allChannelPostMediaEntities) {
      for (vector< BaseEntity* >::iterator it = allChannelPostMediaEntities->begin(); it != allChannelPostMediaEntities->end(); it++ ) {
        ChannelMedia* mediaEntity(static_cast<ChannelMedia*>(*it));
        
        shared_ptr<Resource> media = Utilities::findElementIn (resources, *static_cast<Resource*>(mediaEntity));
        if ( media ) {
          // ensure correct relationship
          media->setChannelId( channelPost.getChannelId() );
          media->setPostId( channelPost.getId() );
          media->clearCommentId();
          media->setModifiedBy( channelPost.getModifiedBy() );
          media->setNo( Utilities::indexOf (channelPost.getResources(), *media) );
          
          // update membership
          this->channelMediaRepository_.update( *mediaEntity, *dynamic_cast<ChannelMedia*>(media.get()) );
          updated = true;
          
          resources.remove(media);
        } else {
          // resources gets deleted on media list sync later
        }
      }
    }
    
    for (list< shared_ptr<Resource> >::iterator mediaIt = resources.begin(); mediaIt != resources.end(); mediaIt++ ) {
      // ensure correct relationship
      (*mediaIt)->setChannelId( channelPost.getChannelId() );
      (*mediaIt)->setPostId( channelPost.getId() );
      (*mediaIt)->clearCommentId( );
      (*mediaIt)->setModifiedBy( channelPost.getModifiedBy() );
      (*mediaIt)->setNo( Utilities::indexOf (channelPost.getResources(), **mediaIt) );
      // add membership
      
      this->channelMediaRepository_.add( *dynamic_cast<ChannelMedia*>((*mediaIt).get()) );
      updated = true;
    }
    
    return updated;
  }
  
  void ChannelPostRepository::clearDeleteOwnerInd( ChannelPost& channelPostEntity )
  throw(data_access_error)
  {
    // reset delete marker of owner entites
    const Value channelId(channelPostEntity.getChannelId());
    this->db_.valueOf(ChannelHeader::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
  }

}
