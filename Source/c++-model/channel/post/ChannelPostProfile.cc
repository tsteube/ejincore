/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPostProfile.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "Media.h"
#include "ChannelMedia.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "SqliteValue.h"
#include "ChannelFullText.h"

namespace ejin
{

  static bool compare_no (shared_ptr<Resource> first, shared_ptr<Resource> second);
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<ChannelPost> > ChannelPostProfile::loadAll( const ChannelHeader& channel, ResourceType resourceType ) const 
  throw(data_access_error)
  {
    return this->load( channel, list<integer>(), resourceType );
  }
  
  list< shared_ptr<ChannelPost> > ChannelPostProfile::load( const ChannelHeader& channel, const list<integer>& channelPostIds, ResourceType resourceType ) const
  throw(data_access_error)
  {
    assert( channel.hasId() );
    assert( channel.hasOwner() );
    
    // load all posts of the given channel
    list< shared_ptr<ChannelPost> > posts(this->resolveAllChannelPostInternal( Value(channel.getId()), channelPostIds, resourceType ));
    return posts;
  }
  
  EntityModification ChannelPostProfile::getEntityModification( integer postId ) const throw(data_access_error)
  {
    const Value _postId(postId);
    unique_ptr<Value> result( db_.valueOf(ChannelPost::TABLE_NAME, "entityModificationMask", &_postId, NULL) );
    integer mask = result->num();
    result = db_.valueOf(ChannelPost::TABLE_NAME, "entityModificationTime", &_postId, NULL);
    jtime modifyTime( result->date() );
    string modifyBy = ""; // TODO
    return EntityModification( mask, modifyTime, modifyBy, false );
  }
  
  EntityModification ChannelPostProfile::getContainerModification( integer postId ) const throw(data_access_error)
  {
    const Value _postId(postId);
    unique_ptr<Value> result( db_.valueOf(ChannelPost::TABLE_NAME, "containerModificationMask", &_postId, NULL) );
    integer mask = result->num();
    result = db_.valueOf(ChannelPost::TABLE_NAME, "containerModificationTime", &_postId, NULL);
    jtime modifyTime( result->date() );
    string modifyBy = ""; // TODO
    return EntityModification( mask, modifyTime, modifyBy, false );
  }
  
  MemberRole ChannelPostProfile::getAccessRole( integer postId, const char* username ) const throw(data_access_error)
  {
    const Value _postId(postId);
    const Value _username(username);
    unique_ptr<Value> result( db_.valueOf(ChannelPost::TABLE_NAME, "accessRole", &_postId, &_username, NULL) );
    return (MemberRole)result->num();
  }
  
  list<integer> ChannelPostProfile::searchFullText( const string& pattern )
  throw(data_access_error)
  {
    // call entity finder
    Value regex( pattern );
    Value source( "CHANNEL_POST" );
    unique_ptr<ResultSet> values( this->db_.find(ChannelFullText::TABLE_NAME, "searchEntityFullText", &source, &regex, NULL) );
    list< integer > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      ChannelFullText* channelFullText = static_cast< ChannelFullText* >( *it );
      result.push_back(channelFullText->getEntityId());
    }
    return result;
  }
  
  // ==========
  // Protected Interface
  // ==========
  
  list< shared_ptr<ChannelPost> > ChannelPostProfile::resolveAllChannelPostInternal( const Value& channelId, const list<integer>& list, ResourceType resourceType ) const
  throw(data_access_error)
  {
    Value hidden(false);
    std::list< shared_ptr<ChannelPost> > result;
    
    // select entity finder
    unique_ptr<ResultSet> channelResources;
    switch (resourceType) {
      case kNoResource:
        break;
      case kResourceTypeMedia:
        channelResources = db_.find(Media::TABLE_NAME, "findAllChannelPostMedias", &channelId, NULL);
        break;
      case kResourceTypeChannelMedia:
        channelResources = db_.find(ChannelMedia::TABLE_NAME, "findAllChannelPostResources", &channelId, &hidden, NULL);
        break;
    }
    
    if (list.empty()) {
      // load all channel posts of the specified channel
      
      unique_ptr<ResultSet> values( db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &hidden, NULL) );
      for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
        result.push_back(shared_ptr<ChannelPost>(static_cast< ChannelPost* >(*it)));
        it = values->take(it);
      }
      
    } else {
      // load individual list of channel posts by identifier list
      
      shared_ptr<ChannelPost> post;
      for (std::list<integer>::const_iterator it=list.begin(); it != list.end(); it++) {
        post = shared_ptr<ChannelPost>(new ChannelPost());
        post->setId(*it);
        // refresh from database
        db_.refresh(*post);
        // add to result list
        result.push_back(post);
      }
      
    }
    
    // merge resources into channel post resource list
    Resource *channelResource;
    for (std::list< shared_ptr<ChannelPost> >::iterator it1=result.begin(); it1 != result.end(); it1++) {
      if ( channelResources.get() != NULL) {
        for (vector< BaseEntity* >::iterator it2 = channelResources->begin(); it2 != channelResources->end(); ) {
          channelResource = static_cast< Resource* >(*it2);
          if (channelResource->hasPostId() && channelResource->getPostId() == (*it1)->getId()) {
            switch (channelResource->getSyncInd()) {
              case kSyncIndInsert:
              case kSyncIndSynchronous:
              case kSyncIndUpdate:
              case kSyncIndConflict:
                (*it1)->getResources().push_back(shared_ptr<Resource>(channelResource));
                it2 = channelResources->take(it2);
                break;
              case kSyncIndDelete:
                it2++;
                break;
            }
          } else {
            it2++;
          }
        }
        // sort resource list
        (*it1)->getResources().sort( compare_no );
      }
    }
    
    return result;
  }
  
  shared_ptr<ChannelPost> ChannelPostProfile::loadChannelPostInternal( const Value& postGid, const Value& hidden ) const throw(data_access_error) 
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(ChannelPost::TABLE_NAME, "findPostByGid", &postGid, &hidden, NULL) );
    
    shared_ptr<ChannelPost> post;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      post = shared_ptr<ChannelPost>(static_cast< ChannelPost* >(*it));
      it = values->take(it);
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate channel post [gid=%d].",  postGid.num());  
    
    return post;
  }
  
  bool compare_no (shared_ptr<Resource> first, shared_ptr<Resource> second)
  {
    return first->getNo() < second->getNo();
  }
}
