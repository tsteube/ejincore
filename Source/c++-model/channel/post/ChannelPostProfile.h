/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_POST_PROFILE_H__
#define __EJIN_CHANNEL_POST_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

namespace ejin
{
  //struct cmpChannelPost;
  
  /**
   * Methods to resolve mchannel comment entities by state.
   */
  class ChannelPostProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChannelPostProfile( Database& db ): db_( db ) {}
    virtual ~ChannelPostProfile( void ) {}
    
    /*
     * Loads a collection of current channel comment instances with resource references.
     */ 
    list< shared_ptr<ChannelPost> > load( const ChannelHeader& channel, const list<integer>& channelPostIds, ResourceType resourceType = kResourceTypeChannelMedia ) const
    throw(data_access_error);
    
    /*
     * Loads all current channel comment with resource references.
     */ 
    list< shared_ptr<ChannelPost> > loadAll( const ChannelHeader& channel, ResourceType resourceType = kResourceTypeChannelMedia ) const
    throw(data_access_error);    
    
    /**
     * Returns the current modificationMask of the post with the specified identifier
     */
    EntityModification getEntityModification( integer postId ) const
    throw(data_access_error);
    
    /**
     * Returns the current modificationMask of the post with the specified identifier
     */
    EntityModification getContainerModification( integer postId ) const
    throw(data_access_error);

    /**
     * Returns the overall access role of the specified entity instance
     */
    MemberRole getAccessRole( integer postId, const char* username )
    const throw(data_access_error);
    
    /**
     * Full Text Search for channel posts.
     */
    list<integer> searchFullText( const string& pattern )
    throw(data_access_error);

    // ==========
    // Protected Interface
    // ==========
  protected:
    
    /*
     * Load the list of channel comments from the database. <br>
     */ 
    list< shared_ptr<ChannelPost> > resolveAllChannelPostInternal( const sqlite::Value& channelId, const list<integer>& list, ResourceType resourceType ) const
    throw(data_access_error);
    
    /**
     * Load entity instance from persistence store.
     */ 
    shared_ptr<ChannelPost> loadChannelPostInternal( const sqlite::Value& commentGid, const sqlite::Value& hidden ) const
    throw(data_access_error);
    
    // --------
    // Member Variables 
    
    Database& db_;
    
  private:
    
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ChannelPostProfile );
    
  };
  
}

#endif // __EJIN_CHANNEL_POST_PROFILE_H__
