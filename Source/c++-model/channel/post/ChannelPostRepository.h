/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_POST_REPOSITORY_H__
#define __EJIN_CHANNEL_POST_REPOSITORY_H__

#include "ChannelPostProfile.h"

namespace ejin
{

  /**
   * Update operations on the channelPost entity. There are a create, update and delete operation to manages channelPosts bound to a
   * channel instance.
   */
  class ChannelPostRepository: public ChannelPostProfile {

    // ==========
    // Public Interface
    // ==========
  public:

    // ctor
    ChannelPostRepository( Database& db, ChangeLogRepository& changeLogRepository, ChannelCommentRepository& channelCommentRepository, ChannelMediaRepository& channelMediaRepository ):
    ChannelPostProfile(db), channelCommentRepository_(channelCommentRepository), channelMediaRepository_(channelMediaRepository), changeLogRepository_(changeLogRepository) {}
    // dtor
    ~ChannelPostRepository( void ) {};

    /**
     * Returns true if the specified post instance has been synchronized with the server reference entity although
     * the entity contains unsynchronized local changes.
     */
    bool detectConflict( const string& postGid ) const
    throw(data_access_error);

    /**
     * Revert local changes to synchronize the post entity with the server reference.
     */
    bool clearConflict( const string& postGid )
    throw(data_access_error);

    /**
     * Update the specified channel post instances in the repository. This method will compare all current channel
     * posts of the specified channel identifier and updates its content by the given reference data.
     */
    bool update( ChannelHeader& channelHeader, list< shared_ptr<ChannelPost> > channelPosts, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Delete all posts and resources of the specified channel identifiers.
     */
    void deleteAllChannelPosts( integer postId )
    throw(data_access_error);

    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */
    void commit( ChannelHeader& channelHeader, list< shared_ptr<ChannelPost> > channelPosts, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Restore original channel post instance
     * and mark local changes as conflicted data set.
     */
    bool revert( const string& gid, bool force = false )
      throw(data_access_error);

    /**
     * Revert all new local post instances of the given post identifier.
     */
    bool revertNewPosts( integer channelId, bool force )
    throw(data_access_error);

    /**
     * Remove new channel post
     */
    bool deleteNewPost( integer postId )
    throw(data_access_error);

    // ==========
    // Private Interface
    // ==========
  private:

    ChannelCommentRepository& channelCommentRepository_;
    ChannelMediaRepository& channelMediaRepository_;
    ChangeLogRepository& changeLogRepository_;

    /*
     * Add a new channel post entity to the persistent store
     */
    shared_ptr<ChannelPost> addChannelPost( ChannelPost& channelPost, sqlite::ResultSet* allChannelMediaEntities, const string& syncAnchor )
    throw(data_access_error);

    /*
     * Updates a channel post entity in the persistent store
     */
    bool updateChannelPost( ChannelPost& channelPostEntity, ChannelPost& channelPost, sqlite::ResultSet* allChannelMediaEntities )
    throw(data_access_error);

    /*
     * Removes a channel post entity from the persistent store
     */
    bool removeChannelPost( ChannelPost& channelPostEntity, sqlite::ResultSet* allChannelPostMediaEntities )
    throw(data_access_error);

    /*
     * Removes a channel post entity from the persistent store
     */
    void deleteChannelPost( ChannelPost& channelPostEntity, sqlite::ResultSet* allChannelPostMediaEntities )
    throw(data_access_error);

    /*
     * Clone channel post data to save local data on conflict
     */
    bool cloneChannelPost( ChannelPost& channelPostEntity, const ChannelPost* channelPost )
    throw(data_access_error);

    /*
     * Update channel resources
     */
    bool updateResources( ChannelPost& channelPostEntity, sqlite::ResultSet* allChannelPostMediaEntities )
    throw(data_access_error);

    /*
     * Reset delete marker on owner entites
     */
    void clearDeleteOwnerInd( ChannelPost& channelPostEntity )
    throw(data_access_error);
    
  };

}

#endif // __EJIN_CHANNEL_POST_REPOSITORY_H__
