/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelPost.h"
#include "UsingEjinTypes.h"

#include "Media.h"
#include "ChannelMedia.h"
#include "ChannelComment.h"
#include "Utilities.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::ISerializableEntity;
  using ser::tag;
  using ser::attr;
  using ser::chardata;
  using ser::endtag;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<ChannelPost>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("GID", type_text, flag_not_null),
    Attribute("CHANNEL_ID", type_int, flag_none),
    Attribute("OWNER", type_text, flag_not_null),
    Attribute("OWNER_FULLNAME", type_text, flag_none),
    Attribute("MODIFIED_BY", type_text, flag_none),
    Attribute("MODIFIED_BY_FULLNAME", type_text, flag_none),
    Attribute("STATE", type_int, flag_none, Value((integer)0)),
    Attribute("CONTENT", type_text, flag_none),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("CONTENT_MODIFY_TIME", type_time, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute("SECURE", type_bool, flag_not_null, Value(false)),
    Attribute("ENCRYPTED", type_bool, flag_not_null, Value(false)),
    Attribute("IV", type_text, flag_none),
    Attribute()
  };
  template <> const AttributeSet DataSchema<ChannelPost>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelPost::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findPostById"] = "ID = ?";
      tMap["findPostByGid"] = "GID = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["findPostsByChannel"] = "CHANNEL_ID = ? AND (MASTER_ID IS NOT NULL) = ? ORDER BY ID";
      tMap["BackupSet"] = "CHANNEL_ID = ?1 AND MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findPostByGidBak"] = "GID = ?";
      tMap["BackupSetBak"] = "CHANNEL_ID = ?1 AND MASTER_ID IS NULL";
    }
    return tMap;
  }
  const map<string,string> ChannelPost::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "SELECT "
      "BITWISE_OR("
      "post_modification_mask,"
      "media_modification_mask"
      ") "
      "FROM " << schema << ".post_modification_view "
      "WHERE post_id = ?";
      tMap["entityModificationMask"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT "
      "MAX_CELL("
      "post_modify_time,"
      "media_modify_time"
      ") "
      "FROM " << schema << ".post_modification_view "
      "WHERE post_id = ?";
      tMap["entityModificationTime"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT "
      "BITWISE_OR("
      "post_modification_mask,"
      "comment_modification_mask,"
      "media_modification_mask"
      ") "
      "FROM " << schema << ".post_modification_view "
      "WHERE post_id = ?";
      tMap["containerModificationMask"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT "
      "MAX_CELL("
      "post_modify_time,"
      "comment_modify_time,"
      "media_modify_time"
      ") "
      "FROM " << schema << ".post_modification_view "
      "WHERE post_id = ?";
      tMap["containerModificationTime"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT MAX(COALESCE(modified_by, owner)) "
      "FROM " << schema << ".post_tbl "
      "WHERE channel_id = ? AND modify_time = ? AND master_id IS NULL";
      tMap["lastModifiedBy"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT "
      "COALESCE( "
      "(CASE WHEN c.owner == ?2 THEN 3 ELSE NULL END), " // 1. channel owner -> ADMIN
      "(CASE WHEN c.role == 0 THEN 0 ELSE NULL END), "     // 2. channel reader role only -> READER
      "(SELECT 3 from " << schema << ".post_hierarchy_view where post_id = ?1 AND ?2 IN (channel_owner,post_owner)), "
      // 3. owner in hierarchy
      "c.role "                                            // 4. entity role
      ") "
      "FROM " << schema << ".post_tbl p JOIN " << schema << ".channel_tbl c on (p.channel_id = c.id) where p.id = ?1";
      tMap["accessRole"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "UPDATE " << schema << ".post_tbl SET sync_ind = 0, modification_mask = 0 "
      "WHERE id = ?1 AND sync_ind = 3 AND master_id IS NULL";
      tMap["clearDeleteSyncInd"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  const string ChannelPost::restoreSet( void ) const {
    return "CHANNEL_ID = ?1";
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelPost::ChannelPost( void ):
  BaseEntity(TABLE_DDL, false), operation_(type_int, SyncOperationMapping), comments_(), resources_()
  {
    this->setSyncInd( kSyncIndInsert );
    this->clearMasterId( );
    this->setState( (integer) 0 );
    this->setModificationMask((integer) 0);
    this->setAccessRole( kSyncRoleRead );
    this->setSecure( false );
    this->setEncrypted( false );
  }
  ChannelPost::ChannelPost( const string& gid ): ChannelPost()
  {
    setGid(gid);
  }
  ChannelPost::ChannelPost( const ChannelPost& record ): ChannelPost()
  {
    this->operator=(record);
  }
  ChannelPost::~ChannelPost( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  ChannelPost& ChannelPost::operator=( const ChannelPost& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      this->operation_ = rhs.operation_;
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Gid);
      
      COPY_PROPERTY((*this), rhs, ChannelId);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, MasterId);
      
      COPY_PROPERTY((*this), rhs, Owner);
      COPY_PROPERTY((*this), rhs, OwnerFullname);
      COPY_PROPERTY((*this), rhs, ModifiedBy);
      COPY_PROPERTY((*this), rhs, ModifiedByFullname);
      COPY_PROPERTY((*this), rhs, State);
      COPY_PROPERTY((*this), rhs, Content);
      
      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      COPY_PROPERTY((*this), rhs, ContentModifyTime);
      
      COPY_BOOL_PROPERTY((*this), rhs, Secure);
      COPY_BOOL_PROPERTY((*this), rhs, Encrypted);
      COPY_PROPERTY((*this), rhs, IV);
      
      this->accessRole_ = rhs.accessRole_;
      COPY_PROPERTY((*this), rhs, ModificationMask);
      
      // use copy constructor according to original type
      for(auto it = rhs.resources_.begin();
          it != rhs.resources_.end(); it++) {
        ChannelMedia* channelMedia = dynamic_cast<ChannelMedia*>((*it).get());
        if (channelMedia) {
          this->resources_.push_back(shared_ptr< Resource >(new ChannelMedia(*channelMedia)));
        }
        Media* media = dynamic_cast<Media*>((*it).get());
        if (media) {
          this->resources_.push_back(shared_ptr< Resource >(new Media(*media)));
        }
      }
    }
    return *this;
  }
  
  // comparison
  bool ChannelPost::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChannelPost& mrhs = dynamic_cast<const ChannelPost&> (rhs);
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() == mrhs.getGid();
    if (this->hasId() && mrhs.hasId())
      return this->getId() == mrhs.getId();
    return false;
  }
  bool ChannelPost::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChannelPost& mrhs = dynamic_cast<const ChannelPost&> (rhs); // throws std::bad_cast if not of type ChannelPost&
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() < mrhs.getGid();
    if (this->hasId() && mrhs.hasId())
      return this->getId() < mrhs.getId();
    return false;
  }
  // debug
  string ChannelPost::toString( void ) const
  {
    std::stringstream buf;
    buf << BaseEntity::toString();
    buf << ",op=" << this->operation_.toString();
    buf << "{" <<
    ",#resources=" << this->resources_.size() <<
    "}";
    return buf.str();
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  const char* ChannelPost::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelPost::tableFields( void ) const { return TABLE_DDL; }
  
  const string& ChannelPost::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& ChannelPost::xmlFields( void ) const { return XML_DDL; }
  
  bool ChannelPost::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const ChannelPost& mrhs = dynamic_cast<const ChannelPost&> (rhs);
      if (mrhs.hasContent()) {
        COPY_PROPERTY((*this), mrhs, State);
        COPY_PROPERTY((*this), mrhs, Content);
        COPY_PROPERTY((*this), mrhs, ModifyTime);
        COPY_PROPERTY((*this), mrhs, OwnerFullname);
        COPY_PROPERTY((*this), mrhs, ModifiedBy);
        COPY_PROPERTY((*this), mrhs, ModifiedByFullname);
        COPY_PROPERTY((*this), mrhs, IV);
        this->setContentModifyTime(mrhs.getModifyTime());
        return true;
      }
    }
    return false;
  }
  bool ChannelPost::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    ChannelPost& rhs( (ChannelPost&)arg );
    
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( this->getGid() != rhs.getGid() )
      return false;
    if ( this->getContent() != rhs.getContent() )
      return false;
    if ( this->getState() != rhs.getState() )
      return false;
    return true;
  }
  bool ChannelPost::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void ChannelPost::archive( void )
  {
    this->clearId( );
    this->setSyncInd( kSyncIndSynchronous );
    this->setModificationMask( kNoneModification );
  }
  void ChannelPost::unarchive( void )
  {
    this->clearId( );
    this->clearGid( );
    this->clearOwnerFullname( );
    this->clearModifiedBy( );
    this->clearModifiedByFullname( );
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->clearContentModifyTime( );
    this->setSyncInd( kSyncIndInsert );
    this->setModificationMask( kChannelPostPendingLocalModification );
    this->clearMasterId( );
    this->setSecure( false );
    this->setEncrypted( false );
    this->clearIV( );
  }
  void ChannelPost::clearData( void )
  {
    this->setState( kSyncIndSynchronous );
    this->clearContent( );
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->setSecure( false );
    this->setEncrypted( false );
    this->clearIV( );
    this->resources_.clear();
  }
  bool ChannelPost::isEmpty( void ) const
  {
    return ! hasContent() && this->resources_.empty();
  }
  
  // ==========
  // Crypto Interface
  // ==========
  
  bool ChannelPost::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if ( crypto && crypto->hasSessionKey( ) ) {
      if (this->hasContent()) {
        this->setContent( crypto->decrypt( this->getContent() ) );
      }
      this->setSecure( true );
      this->setEncrypted( false );
      COPY_PROPERTY((*this), (*crypto), IV);
      this->contentSize_ = 0;
      return true;
    } else {
      this->setSecure( false );
      this->setEncrypted( false );
      this->clearIV( );
      return false;
    }
  }
  
  bool ChannelPost::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if ( crypto && crypto->hasSessionKey( ) ) {
      switch( this->getSyncInd() ) {
        case kSyncIndSynchronous:
          COPY_PROPERTY((*crypto), (*this), IV); // use old IV
          break;
        case kSyncIndInsert:
        case kSyncIndUpdate:
          crypto->createIV( ); // generate new IV
          break;
        case kSyncIndDelete:
        case kSyncIndConflict:
          assert(false);
          break;
      }
      if (this->hasContent()) {
        this->setContent( crypto->encrypt( this->getContent() ) );
        this->contentSize_ = this->value("CONTENT")->size();
      }
      this->setSecure( true );
      this->setEncrypted( true );
      COPY_PROPERTY((*this), (*crypto), IV);
      return true;
    } else {
      if (this->isSecure()) {
        // do not publish unencrypted updates to the server in a secure channel
        _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "no session key to encrypt message");
      }
      this->setSecure( false );
      this->setEncrypted( false );
      this->clearIV( );
      this->contentSize_ = 0;
      return false;
    }
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON Mapping
  
  bool ChannelPost::marshalJson( nlohmann::json& j ) const
  {    
    if ( this->hasGid() ) {
      j["id"] = string(this->value("GID")->str(), this->value("GID")->size());
    }
    if ( this->getState() > 0 ) {
      j["state"] = this->value("STATE")->num();
    }
    if ( this->hasContent() ) {
      j["content"] = string( this->value("CONTENT")->str(), this->value("CONTENT")->size() );
    }

    // print out resources if any
    if ( ! this->resources_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<Resource> >::const_iterator it=this->resources_.begin();
           it != this->resources_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      j["resource"] = a;
    }

    // print out comments if any
    if ( ! this->comments_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin();
           it != this->comments_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      j["comment"] = a;
    }

    return true;
  }
  /*
  bool ChannelPost::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if ( this->hasGid() ) {
      json_object_set_new( node, "id", json_stringn_nocheck( this->value("GID")->str(),
                                                            this->value("GID")->size() ) );
    }
    if ( this->getState() > 0 ) {
      json_object_set_new( node, "state", json_integer( this->value("STATE")->num() ) );
    }
    if ( this->hasContent() ) {
      json_object_set_new( node, "content", json_stringn_nocheck( this->value("CONTENT")->str(),
                                                                  this->value("CONTENT")->size() ) );
    }

    // print out resources if any
    if ( ! this->resources_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( node, "resource", array );
      for (list< shared_ptr<Resource> >::const_iterator it=this->resources_.begin();
           it != this->resources_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }
    
    // print out comments if any
    if ( ! this->comments_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( node, "comment", array );
      for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin();
           it != this->comments_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }
    
    return true;
  }
  */

  bool ChannelPost::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("id")) {
        this->value("GID")->str( j["id"] );
      }
      if ( j.contains("owner")) {
        this->value("OWNER")->str( j["owner"] );
      }
      if ( j.contains("owner_fullname")) {
        this->value("OWNER_FULLNAME")->str( j["owner_fullname"] );
      }
      if ( j.contains("modified_by")) {
        this->value("MODIFIED_BY")->str( j["modified_by"] );
      }
      if ( j.contains("modified_by_fullname")) {
        this->value("MODIFIED_BY_FULLNAME")->str( j["modified_by_fullname"] );
      }
      if ( j.contains("content")) {
        this->value("CONTENT")->str( j["content"] );
      }
      if ( j.contains("state")) {
        this->value("STATE")->num( j["state"] );
      }
      if ( j.contains("create_time")) {
        this->value("CREATE_TIME")->str( j["create_time"] );
      }
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }
      if ( j.contains("op")) {
        this->operation_.str( j["op"] );
      }

      // unmarshal resources
      if ( j.contains("resource")) {
        json j2 = j["resource"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<Resource> entity;
            if (this->resources_.size() > i) {
              typename std::list< std::shared_ptr< Resource > >::iterator refIt = this->resources_.begin();
              std::advance(refIt, i);
              entity = *refIt;
            } else {
              entity = shared_ptr<Resource>(new ChannelMedia());
              this->resources_.push_back(entity);
            }
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      // unmarshal approved members
      if ( j.contains("comment")) {
        json j2 = j["comment"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<ChannelComment> entity = Utilities::findOrAppend( this->comments_, i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      return true;
    }
    
    return false;
  }
  /*
  bool ChannelPost::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "id");
    if ( json_is_string(value) ) {
      this->value("GID")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner");
    if ( json_is_string(value) ) {
      this->value("OWNER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner_fullname");
    if ( json_is_string(value) ) {
      this->value("OWNER_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by_fullname");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "content");
    if ( json_is_string(value) ) {
      this->value("CONTENT")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "state");
    if ( json_is_number(value) ) {
      this->value("STATE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "create_time");
    if ( json_is_string(value) ) {
      this->value("CREATE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "op");
    if ( json_is_string(value) ) {
      this->operation_.bytea( json_string_value(value), json_string_length(value) );
    }
    
    // unmarshal resources
    value = json_object_get(node, "resource");
    if (json_is_array(value)) {
      size_t index;
      json_t* child;
      json_array_foreach(value, index, child) {
        shared_ptr<Resource> entity;
        if (this->resources_.size() > index) {
          typename std::list< std::shared_ptr< Resource > >::iterator refIt = this->resources_.begin();
          std::advance(refIt, index);
          entity = *refIt;
        } else {
          entity = shared_ptr<Resource>(new ChannelMedia());
          this->resources_.push_back(entity);
        }
        if (! entity || ! entity->unmarshalJson( child )) {
          return false;
        }
      }
    }
    
    // unmarshal approved members
    value = json_object_get(node, "comment");
    if (json_is_array(value)) {
      size_t index;
      json_t* child;
      json_array_foreach(value, index, child) {
        shared_ptr<ChannelComment> entity = Utilities::findOrAppend( this->comments_, index );
        if (! entity || ! entity->unmarshalJson( child )) {
          return false;
        }
      }
    }
    
    return true;
  }
  */

  // XML Mapping
  
  const string ChannelPost::XML_NAME   = "post";
  const ser::XmlAttribute ChannelPost::XML_FIELDS[] =
  {
    ser::XmlAttribute("id", "GID" ),
    ser::XmlAttribute("owner", "OWNER" ),
    ser::XmlAttribute("owner_fullname", "OWNER_FULLNAME" ),
    ser::XmlAttribute("modified_by", "MODIFIED_BY" ),
    ser::XmlAttribute("modified_by_fullname", "MODIFIED_BY_FULLNAME" ),
    ser::XmlAttribute("content", "CONTENT" ),
    ser::XmlAttribute("state", "STATE" ),
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute("create_time", "CREATE_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet ChannelPost::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  Value* ChannelPost::valueByXmlNode( const char* name )
  {
    if ( strcmp("op", name) == 0 ) {
      return &operation_;
    } else {
      return SerializableEntity::valueByXmlNode(name);
    }
  }
  
  ISerializableEntity* ChannelPost::entityByXmlNode( const char* name, int position )
  {
    ISerializableEntity* result;
    
    if (strcmp(name, "comment") == 0) {
      shared_ptr<ChannelComment> comment;
      if ((int) this->comments_.size() > position) {
        // return existing node
        list< shared_ptr<ChannelComment> >::iterator i = this->comments_.begin();
        advance(i, position);
        comment = *i;
      } else {
        // create new node
        comment = shared_ptr<ChannelComment>(new ChannelComment());
        this->comments_.push_back(comment);
      }
      return comment.get();
      
    } else if ( strcmp("resource", name) == 0 ) {
      
      shared_ptr<Resource> media;
      if ((int) this->getResources().size() > position) {
        // return existing node
        list< shared_ptr<Resource> >::iterator i = this->getResources().begin();
        std::advance(i, position);
        media = *i;
      } else {
        // create new node
        media = shared_ptr<ChannelMedia>(new ChannelMedia());
        this->getResources().push_back(media);
      }
      result = media.get();
      
    } else {
      result = SerializableEntity::entityByXmlNode(name, position);
      
    }
    return result;
  }
  
  bool ChannelPost::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("post"); // start element tag
    
    // serialize properties
    if ( this->hasGid() ) {
      xml << attr("id") << this->getGid();
    }
    if ( this->hasState() ) {
      xml << attr("state") << this->getState();
    }
    if ( this->hasContent() ) {
      xml << tag("content") << chardata() << this->getContent() << endtag();
    }
    
    // print out resources if any
    for (list< shared_ptr<Resource> >::const_iterator it=this->resources_.begin(); it != this->resources_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    // print out comment if any
    for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin(); it != this->comments_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    xml << ser::endtag(); // end element tag
    return true;
  }
  
}
