/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_POST_ENTITY_H__
#define __EJIN_CHANNEL_POST_ENTITY_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "SqliteValue.h"
#include "SqliteUtil.h"
#include "IBusinessData.h"

namespace ejin
{
  
  /**
   * Entity Class for POST_TBL
   */
  class ChannelPost: public BaseEntity, public IBusinessData, public DataSchema<ChannelPost> {
    friend class Database;
    friend class ChannelCommentRepository;
    friend class ChannelPostRepository;
    friend class ChannelPostProfile;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "POST_TBL";

    // ctors
    ChannelPost( void );
    ChannelPost( const string& gid );
    ChannelPost( const ChannelPost& record );
    // dtor
    ~ChannelPost( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChannelPost& operator=( const ChannelPost& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ChannelPost(*this); }
    // debug
    string toString( void ) const;
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool copyDataFrom( const BaseEntity& rhs );
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool isEmpty( void ) const;
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );
    
    ISerializableEntity* entityByXmlNode( const char* name, int position );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    sqlite::Value* valueByXmlNode( const char* name );
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_STRING_PROPERTY_INTF( Gid, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( ChannelId, 2 )
    ACCESS_STRING_PROPERTY_INTF( Owner, 3 )
    ACCESS_STRING_PROPERTY_INTF( OwnerFullname, 4 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedBy, 5 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedByFullname, 6 )
    ACCESS_INTEGER_PROPERTY_INTF( State, 7 )
    ACCESS_STRING_PROPERTY_INTF( Content, 8 )
    ACCESS_TIME_PROPERTY_INTF( CreateTime, 9 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 10 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 11 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 12 )
    ACCESS_TIME_PROPERTY_INTF( ContentModifyTime, 13 )
    ACCESS_INTEGER_PROPERTY_INTF( ModificationMask, 14 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Secure, 15 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Encrypted, 16 )
    ACCESS_STRING_PROPERTY_INTF( IV, 17 )
    bool isHidden() { return this->getMasterId() > 0; }
    
    // ==========
    // Relations
    // ==========
    list< shared_ptr<ChannelComment> >& getComments( void ) { return this->comments_; }
    void setComments( const list< shared_ptr<ChannelComment> >& comments ) { this->comments_ = comments; }
    
    list< shared_ptr<Resource> >& getResources( void ) { return this->resources_; }
    void setResources( const list< shared_ptr<Resource> >& resources ) { this->resources_ = resources; }
    
    // ==========
    // Metadata
    // ==========
    integer getEntityModificationMask( void ) const { return this->entityModificationMask_; }
    void setEntityModificationMask( integer mask ) { this->entityModificationMask_ = mask; }
    integer getContainerModificationMask( void ) const { return this->containerModificationMask_; }
    void setContainerModificationMask( integer mask ) { this->containerModificationMask_ = mask; }
    
    const SyncOperation getOperation( void ) const { return (SyncOperation) this->operation_.num(); }
    void setOperation( SyncOperation operation ) { this->operation_.num(operation); }
    void clearOperation( void ) { this->operation_.setNull(); }
    bool hasOperation( void ) const { return ! this->operation_.isNull(); }
    
    MemberRole getAccessRole( void ) const { return this->accessRole_; }
    void setAccessRole( MemberRole role ) { this->accessRole_ = role; }
    
    integer getContentSize( void ) const { return (this->contentSize_ > 0) ? this->contentSize_ : this->value("CONTENT")->size(); }
    void setContentSize( integer size ) { this->contentSize_ = size; }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Member Variables
    
    integer    containerModificationMask_;
    integer    entityModificationMask_;
    MemberRole accessRole_;
    integer    contentSize_;

    // operation marker
    sqlite::Value operation_;
    // members
    list< shared_ptr<ChannelComment> > comments_;
    // resources
    list< shared_ptr<Resource> > resources_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif // __EJIN_CHANNEL_POST_ENTITY_H__
