/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelRepository.h"
#include "UsingEjinTypes.h"

#include "ChannelProfileView.h"
#include "ChannelSkeletonView.h"
#include "KeySequence.h"
#include "Channel.h"
#include "ChannelHeader.h"
#include "ChannelMember.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "Media.h"
#include "Member.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "Utilities.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool ChannelRepository::detectMemberConflict( const string& channelGid ) const throw(data_access_error)
  {
    assert ( ! channelGid.empty() );
    
    // call entity finder
    Value channel(channelGid.c_str());
    unique_ptr<ResultSet> conflicts( db_.find(ChannelProfileView::TABLE_NAME, "detectChannelMemberConflict", &channel, NULL) );
    return conflicts->size() > 0;
  }

  bool ChannelRepository::detectChannelConflict( const string& channelGid ) const throw(data_access_error)
  {
    if ( channelGid.empty() )
      return false;
    
    // call entity finder
    Value channel(channelGid.c_str());
    unique_ptr<ResultSet> conflicts( db_.find(ChannelProfileView::TABLE_NAME, "detectChannelConflict", &channel, NULL) );
    return conflicts->size() > 0;
  }
    
  bool ChannelRepository::clearConflict( const string& channelGid ) throw(data_access_error)
  {
    if ( channelGid.empty() )
      return false;
    
    // load channel header first
    shared_ptr<ChannelHeader> channelHeader( this->headerRepository_.loadByGid( channelGid ) );
    if ( ! channelHeader )
    {
      // clear channel header conflict
      return this->headerRepository_.clearConflict(channelGid);
    }
    
    // call database procedure to get all conflicts posts and comments
    bool cleared = false;
    Value channelId(channelHeader->getId());
    
    // break channel media conflicts first
    unique_ptr<ResultSet> mediaConflicts( db_.find(Media::TABLE_NAME, "findConflictedByChannelId", &channelId, NULL));
    for (vector< BaseEntity* >::iterator it2 = mediaConflicts->begin(); it2 != mediaConflicts->end(); it2++) {
      if (this->mediaRepository_.clearConflict( static_cast< Media* >( *it2 )->getGid() ))
        cleared = true;
    }
    
    // clear channel component conflicts
    unique_ptr<ResultSet> conflicts( db_.find(ChannelSkeletonView::TABLE_NAME, "conflictedChannelProfileById", &channelId, NULL) );
    for (vector< BaseEntity* >::iterator it = conflicts->begin(); it != conflicts->end(); it++) {
      ChannelSkeletonView* view = static_cast< ChannelSkeletonView* >(*it);
      if ( view->hasChannelGid() ) {
        // clear channel header conflict
        if (this->headerRepository_.clearConflict(view->getChannelGid()))
          cleared = true;
      }
      if ( view->hasPostGid() ) {
        // clear channel post conflict
        if (this->postRepository_.clearConflict(view->getPostGid()))
          cleared = true;
      } else if ( view->hasCommentGid() ) {
        // clear channel comment conflict
        if (this->commentRepository_.clearConflict(view->getCommentGid()))
          cleared = true;
      } else if ( view->hasCommentId() ) {
        // remove new comment
        if (this->commentRepository_.deleteNewComment(view->getCommentId()))
          cleared = true;
      } else if ( view->hasPostId() ) {
        // remove new posts
        if (this->postRepository_.deleteNewPost(view->getPostId()))
          cleared = true;
      }
    }
    return cleared;
  }
  
  Channel& ChannelRepository::update( Channel& channel, const string& username, const jtime& syncTime ) throw(data_access_error)
  {
    assert( ! channel.getGid().empty() );
    assert( ! channel.getSyncAnchor().empty() );
    assert( channel.getOperation() != kSyncNone );
    
    shared_ptr<ChannelHeader> header;
    
    switch ( channel.getOperation() ) {
      case kSyncInsert:
        assert( channel.getHeader() );
      case kSyncUpdate:
      {
        if ( channel.getHeader() )
        {
          header = channel.getHeader();
        }
        else
        {
          header = this->headerRepository_.loadByGid( channel.getGid() );
          if (! header ) {
            break;
          }
          KeySequence headerSyncAnchor(header->getSyncAnchor());
          KeySequence channelSyncAnchor(channel.getSyncAnchor());
          if ( headerSyncAnchor >= channelSyncAnchor ) {
            if ( header->isOutdated() ) {
              // fix state
              header->setOutdated( false );
              this->db_.update( *header );
            }
            break;
          }
        }

        // ensure sync anchor is set correctly
        header->setSyncAnchor( channel.getSyncAnchor() );
        header = this->headerRepository_.update( *header, true, syncTime );
        this->postRepository_.update( *header, channel.getPosts(), syncTime );
        this->commentRepository_.update( *header, channel.getPosts(), channel.getComments(), syncTime );

        // update last sync time
        setSyncTime(header->getId(), syncTime);

        if (! header->hasSyncTime() ) {
          // clear modification mask on first sync
          this->clearRecentRemoteModificationMask( header->getId() );
        }

        break;
      }
      case kSyncRemove:
        header = shared_ptr<ChannelHeader>(new ChannelHeader( ));
        header->setGid( channel.getGid() );
        header->setSyncAnchor( channel.getSyncAnchor() );
        header->setSyncTime( channel.getSyncTime() );
        header->setModifiedBy( username );
        this->headerRepository_.deleteChannel( *header, syncTime );
        break;
      case kSyncNone:
        break;
    }
    
    assert( header );
    return channel;
  }
  
  void ChannelRepository::commit( Channel& channel, const string& syncAnchor, const string& username, const jtime& syncTime ) throw(data_access_error)
  {
    assert( ! channel.getGid().empty() );
    assert( ! syncAnchor.empty() );
    assert( channel.getOperation() != kSyncNone );
    
    shared_ptr<ChannelHeader> header;
    switch ( channel.getOperation() ) {
      case kSyncInsert:
      case kSyncRemove:
        assert( channel.getHeader() );
      case kSyncUpdate:        
        if ( channel.getHeader() )
        {
          header = channel.getHeader();
          // ensure sync anchor is set correctly
          this->headerRepository_.commit( *header, syncAnchor, username, syncTime );
        }
        else
        {
          header = this->headerRepository_.loadByGid( channel.getGid() );
          // remember current sync anchor
          header->setSyncAnchor( syncAnchor );
          header->setModifiedBy( username );
          db_.update(*header);
        }
        
        this->postRepository_.commit( *header, channel.getPosts(), syncAnchor, username, syncTime );
        this->commentRepository_.commit( *header, channel.getAllComments(), syncAnchor, username, syncTime );
        
        // update last sync time
        if (channel.getOperation() != kSyncRemove) {
          setSyncTime(header->getId(), syncTime);
        }
        break;        
      case kSyncNone:
        break;
    }
  }
  
  bool ChannelRepository::createAESKey( ChannelHeader& header )
    throw(security_integrity_error,data_access_error)
  {
    return this->headerRepository_.createAESKey( header );
  }
  
  bool ChannelRepository::publishAESKey( ChannelHeader& header, const string& aesKey, bool force )
  throw(security_integrity_error,data_access_error)
  {
    return this->headerRepository_.publishAESKey( header, aesKey, force );
  }
  
  void ChannelRepository::setSyncTime( integer channelId, const jtime& syncTime )
  throw(data_access_error)
  {
    shared_ptr<ChannelHeader> headerEntity( this->headerRepository_.loadById( channelId ) );
    if ( ! headerEntity )
    {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "channel with unique identifier %d not found", channelId);
    }
    
    if ( headerEntity->hasSyncTime() ) {
      headerEntity->setLastSyncTime( headerEntity->getSyncTime() );
    }
    headerEntity->setSyncTime( syncTime );
    // persist entity
    this->db_.update( *headerEntity );
    
  }
  
  bool ChannelRepository::clearRecentRemoteModificationMask( integer channelId )
  throw(data_access_error)
  {
    assert( channelId > 0 );
    ChannelHeader channelHeader( channelId);
    bool success = false;
    if (this->db_.isOpen() && this->db_.contains( channelHeader )) {
      this->db_.refresh( channelHeader );
      
      Value hidden(false);
      Value cId(channelHeader.getId());
      
      // 1. process channel header entity
      if ( (channelHeader.getModificationMask() & kChannelHeaderRecentRemoteModification) != 0 ) {
        channelHeader.setModificationMask( channelHeader.getModificationMask() ^ kChannelHeaderRecentRemoteModification );
        this->db_.update( channelHeader );
        success = true;
      }
      
      // 2. process channel member list
      unique_ptr<ResultSet> channelMembers = this->db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &cId, &hidden, NULL);
      for (auto it = channelMembers->begin(); it != channelMembers->end(); it++) {
        ChannelMember* member = static_cast< ChannelMember* >( *it );
        if ( (member->getModificationMask() & kChannelMemberRecentRemoteModification) != 0 ) {
          member->setModificationMask( member->getModificationMask() ^ kChannelMemberRecentRemoteModification );
          this->db_.update( *member );
          success = true;
        }
      }
      
      // 3. process channel post list
      unique_ptr<ResultSet> channelPosts = this->db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &cId, &hidden, NULL);
      for (auto it = channelPosts->begin(); it != channelPosts->end(); it++) {
        ChannelPost* post = static_cast< ChannelPost* >( *it );
        if ( (post->getModificationMask() & kChannelPostRecentRemoteModification) != 0 ) {
          post->setModificationMask( post->getModificationMask() ^ kChannelPostRecentRemoteModification );
          this->db_.update( *post );
          success = true;
        }
      }
      
      // 4. process channel comment list
      unique_ptr<ResultSet> channelComments = this->db_.find(ChannelComment::TABLE_NAME, "findAllCommentsByChannel", &cId, &hidden, NULL);
      for (auto it = channelComments->begin(); it != channelComments->end(); it++) {
        ChannelComment* comment = static_cast< ChannelComment* >( *it );
        if ( (comment->getModificationMask() & kChannelCommentRecentRemoteModification) != 0 ) {
          comment->setModificationMask( comment->getModificationMask() ^ kChannelCommentRecentRemoteModification );
          this->db_.update( *comment );
          success = true;
        }
      }
      
      //unique_ptr<ResultSet> channelResources = this->find(Media::TABLE_NAME, "findMediaByHeaderId", &cId, NULL);
    }
    return success;
  }
  
}
