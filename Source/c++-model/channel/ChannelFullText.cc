/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelFullText.h"
#include "UsingEjinTypes.h"

#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  
  template <> const Attribute DataSchema<ChannelFullText>::TABLE_FIELDS[] =
  {
    Attribute("ROWID", type_int, flag_primary_key),
    Attribute("CHANNEL_ID", type_int, flag_not_null),
    Attribute("ENTITY_ID", type_int, flag_not_null),
    Attribute("SOURCE", type_text, flag_not_null),
    Attribute("NAME", type_text, flag_none),
    Attribute("CONTENT", type_text, flag_none),
    Attribute()
  };
  template <> const AttributeSet DataSchema<ChannelFullText>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelFullText::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) ) {
      tMap["searchChannelFullText"] = "name match ?1 OR content match ?1 ORDER BY RANK";
      tMap["searchEntityFullText"] = "source = ?1 AND (name match ?2 OR content match ?2) ORDER BY RANK";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelFullText::ChannelFullText( void ): BaseEntity(TABLE_DDL, true)
  { }
  ChannelFullText::ChannelFullText( const ChannelFullText& record ): BaseEntity(record)
  { this->operator=(record); }
  ChannelFullText::~ChannelFullText( void )
  { }
  
  // ==========
  // Object Interface
  // ==========
  
  ChannelFullText& ChannelFullText::operator=( const ChannelFullText& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, ChannelId);
      COPY_PROPERTY((*this), rhs, EntityId);
      COPY_PROPERTY((*this), rhs, Source);
      COPY_PROPERTY((*this), rhs, Name);
      COPY_PROPERTY((*this), rhs, Content);
    }
    return *this;
  }
  
  // comparison
  bool ChannelFullText::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChannelFullText& mrhs = dynamic_cast<const ChannelFullText&> (rhs);
    return getSource() == mrhs.getSource() && getEntityId() == mrhs.getEntityId();
  }
  bool ChannelFullText::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChannelFullText& mrhs = dynamic_cast<const ChannelFullText&> (rhs); // throws std::bad_cast if not of type Member&
    if ( getSource() != mrhs.getSource() )
      return getSource() < mrhs.getSource();
    if ( getEntityId() != mrhs.getEntityId() )
      return getEntityId() < mrhs.getEntityId();
    return 0;
  }
  
  const char* ChannelFullText::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelFullText::tableFields( void ) const { return TABLE_DDL; }
  
}

