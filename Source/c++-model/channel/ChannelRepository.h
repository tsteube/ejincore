/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_REPOSITORY_ENTITY_H__
#define __EJIN_CHANNEL_REPOSITORY_ENTITY_H__

#include "ChannelProfile.h"

namespace ejin
{
  
  /**
   * Update operation on the channel header entity. There are a create, update and delete methods to manages channels.
   */
  class ChannelRepository: public ChannelProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    ChannelRepository( Database& db, ChannelHeaderRepository& headerRepository, ChannelPostRepository& postRepository, ChannelCommentRepository& commentRepository, MediaRepository& mediaRepository, MemberRepository& memberRepository ):
    ChannelProfile( db, headerRepository, postRepository, commentRepository, mediaRepository, memberRepository ) {}
    // dtor
    virtual ~ChannelRepository( void ) {}
    
    /**
     * Returns true if the specified channel header instance has been synchronized with the server reference entity
     * although the entity contains unsynchronized local changes.
     */ 
    bool detectMemberConflict( const string& channelGid ) const
    throw(data_access_error);    

    /**
     * Returns true if the specified channel header instance has been synchronized with the server reference entity
     * although the entity contains unsynchronized local changes.
     */ 
    bool detectChannelConflict( const string& channelGid ) const
    throw(data_access_error);    
    
    /**
     * Revert local changes to synchronize the channel header entity with the server reference.
     */ 
    bool clearConflict( const string& channelGid )
    throw(data_access_error);    
    
    /**
     * Update channel data in the repository.
     */
    Channel& update( Channel& channel, const string& username, const jtime& syncTime  )
    throw(data_access_error);    
    
    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */ 
    void commit( Channel& channel, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);    
    
    /**
     * Set encrypted channel session key only for new channel member.
     */
    bool publishAESKey( ChannelHeader& header, const string& aesKey, bool force )
    throw(security_integrity_error,data_access_error);
    
    bool createAESKey( ChannelHeader& header )
    throw(security_integrity_error,data_access_error);
    
    /**
     * Updates the last synchronization date time of the channel itself.
     */
    void setSyncTime( integer channelId, const jtime& localTime )
    throw(data_access_error);
    
    /**
     * clear remote modification bit
     */
    bool clearRecentRemoteModificationMask( integer channelId )
    throw(data_access_error);    

  };
  
}

#endif // __EJIN_CHANNEL_REPOSITORY_ENTITY_H__
