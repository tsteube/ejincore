/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Channel.h"
#include "UsingEjinTypes.h"

#include <iostream>
#include <sstream>

#include "Utilities.h"
#include "SqliteBaseEntity.h"
#include "IBusinessData.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "Resource.h"
#include "SqliteValue.h"
#include "RSACrypto.h"

namespace ejin
{
  using ser::tag;
  using ser::attr;
  using ser::chardata;
  using ser::endtag;
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Channel::Channel( void ): 
  id_(type_int), gid_(type_text), lastSyncTime_(type_time), syncTime_(type_time), syncAnchor_(type_text), operation_(type_int, SyncOperationMapping), header_(), posts_(), comments_(), crypto_(), secure_(false)
  {
  }
  Channel::Channel( const integer id, const string& gid, bool secure ): Channel()
  {
    this->id_.num( id );
    this->gid_.str( gid );
    this->secure_ = secure;
  }
  Channel::Channel( const integer id, const string& gid, const string& syncAnchor, bool secure ): Channel()
  {
    this->secure_ = secure;
    this->id_.num( id );
    this->gid_.str( gid );
    this->syncAnchor_.str( syncAnchor );
  }
  Channel::Channel( shared_ptr<ChannelHeader> header, list< shared_ptr<ChannelPost> > posts, list< shared_ptr<ChannelComment> > comments): Channel()
  {
    this->header_ = header;
    this->posts_ = posts;
    this->comments_ = comments;
  }
  Channel::Channel( const integer id, const string& gid, shared_ptr<ChannelHeader> header, list< shared_ptr<ChannelPost> > posts, list< shared_ptr<ChannelComment> > comments): Channel( )
  {
    this->id_.num(id);
    this->gid_.str(gid.c_str());
    this->header_ = header;
    this->posts_ = posts;
    this->comments_ = comments;
  }
  Channel::Channel( const Channel& channel ): Channel()
  {
    this->gid_ = channel.gid_;
    this->secure_ = channel.secure_;
    this->crypto_ = channel.crypto_;
    this->lastSyncTime_ = channel.lastSyncTime_;
    this->syncTime_ = channel.syncTime_;
    this->syncAnchor_ = channel.syncAnchor_;
    this->operation_ = channel.operation_;
    this->header_ = channel.header_;
    this->posts_ = channel.posts_;
    this->comments_ = channel.comments_;
  }
  Channel::~Channel( void ) { }  
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Channel& Channel::operator=( const Channel& rhs ) {    
    if (this != &rhs) {
      this->gid_ = rhs.gid_;
      this->operation_ = rhs.operation_;
      this->syncTime_ = rhs.syncTime_;
      this->lastSyncTime_ = rhs.lastSyncTime_;
      this->syncAnchor_ = rhs.syncAnchor_;
      this->header_ = rhs.header_;
      this->posts_ = rhs.posts_;
      this->comments_ = rhs.comments_;
    }
    return *this;
  }
  
  // comparison
  bool Channel::operator==( const Channel& rhs ) const {
    if (this == &rhs) 
      return true;
    
    return (this->gid_ == rhs.gid_);
  }  
  bool Channel::operator<( const Channel& rhs ) const {
    if (this == &rhs) 
      return false;
    
    if (this->gid_.num() < rhs.gid_.num())
      return true;
    return false;
  }
  // debug
  string Channel::toString( void ) const {
    std::ostringstream buf;
    buf << "Channel[" <<
    "gid=" << this->gid_.toString() <<
    ",op=" << this->operation_.toString() <<
    ",syncTime=" << this->syncTime_.toString() <<
    ",lastSyncTime=" << this->lastSyncTime_.toString() <<
    ",syncAnchor=" << this->syncAnchor_.toString() <<
    ",header=" << ((bool)this->header_) <<
    "]{"
    "#posts=" << (this->posts_.size()) <<
    ",#comments=" << (this->comments_.size()) <<
    "}";
    return buf.str();
  }

  // ==========
  // IBusinessData Interface
  // ==========
  
  bool Channel::isEmpty( void ) const 
  {
    return (! this->header_ || this->header_->isEmpty()) && 
    this->posts_.empty() && 
    this->comments_.empty();
  }
  
  void Channel::clearBusinessData( void )
  {
    if (this->header_)
      this->header_.reset();
    
    for (list< shared_ptr<ChannelPost> >::iterator it=this->posts_.begin(); it != this->posts_.end(); it++) {
      (*it)->clearData();
    }    
    
    for (list< shared_ptr<ChannelComment> >::iterator it=this->comments_.begin(); it != this->comments_.end(); it++) {
      (*it)->clearData();
    }
  }
  
  list< shared_ptr<ChannelComment> > Channel::getAllComments( void ) const 
  {
    list< shared_ptr<ChannelComment> > allComments;
    
    // 1. comments defined inside the posts
    for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin(); it != this->posts_.end(); it++) {      
      const list< shared_ptr<ChannelComment> > postComments((*it)->getComments());
      allComments.insert(allComments.end(), postComments.begin(), postComments.end());
    }    
    
    // 2 top level comments
    allComments.insert(allComments.end(), this->comments_.begin(), this->comments_.end());
    
    return allComments;
  }
  
  list< shared_ptr<Resource> > Channel::getAllResources( void ) const
  {
    list< shared_ptr<Resource> > allMedias;
    
    // 1. resources defined inside the header
    if (this->header_) {
      const list< shared_ptr<Resource> > headerMedias(this->header_->getResources());
      allMedias.insert(allMedias.end(), headerMedias.begin(), headerMedias.end());
    }
    
    // 2. resources defined inside the posts
    for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin(); it != this->posts_.end(); it++) {      
      const list< shared_ptr<Resource> > postMedias((*it)->getResources());
      allMedias.insert(allMedias.end(), postMedias.begin(), postMedias.end());
      
      for (list< shared_ptr<ChannelComment> >::const_iterator it1=(*it)->getComments().begin(); it1 != (*it)->getComments().end(); it1++) {      
        const list< shared_ptr<Resource> > commentMedias((*it1)->getResources());
        allMedias.insert(allMedias.end(), commentMedias.begin(), commentMedias.end());
      }    
    }    
    
    // 3. resources defined inside the comments
    for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin(); it != this->comments_.end(); it++) {      
      const list< shared_ptr<Resource> > commentMedias((*it)->getResources());
      allMedias.insert(allMedias.end(), commentMedias.begin(), commentMedias.end());
    }    
    
    return allMedias;
  }
  
  // ==========
  // Crypto Interface
  // ==========
  
  bool Channel::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    bool result = true;
    if (this->header_) {
      result = this->header_->decrypt( crypto );
    }
    for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin(); it != this->posts_.end(); it++) {
      if (! (*it)->decrypt( crypto )) {
        result = false;
      }
      for (list< shared_ptr<ChannelComment> >::const_iterator it2=(*it)->getComments().begin();
           it2 != (*it)->getComments().end(); it2++) {
        if (! (*it2)->decrypt( crypto )) {
          result = false;
        }
      }
    }
    for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin(); it != this->comments_.end(); it++) {
      if (! (*it)->decrypt( crypto )) {
        result = false;
      }
    }
    return result;
  }
  
  bool Channel::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    bool result = true;
    if (this->header_) {
      result = this->header_->encrypt( crypto );
    }
    for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin(); it != this->posts_.end(); it++) {
      if (! (*it)->encrypt( crypto )) {
        result = false;
      }
      for (list< shared_ptr<ChannelComment> >::const_iterator it2=(*it)->getComments().begin();
           it2 != (*it)->getComments().end(); it2++) {
        if (! (*it2)->encrypt( crypto )) {
          result = false;
        }
      }
    }
    for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin(); it != this->comments_.end(); it++) {
      if (! (*it)->encrypt( crypto )) {
        result = false;
      }
    }
    return result;
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON mapping
  
  bool Channel::marshalJson( nlohmann::json& node ) const
  {
    json j;
    if ( this->hasGid() ) {
      j["id"] = string(gid_.str(), gid_.size());
    }
    
    if ( this->header_ ) {
      json j2;
      if ( this->header_->marshalJson( j2 ) ) {
        j["header"] = j2;
      }
    }

    // print out posts
    if ( ! this->posts_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin();
           it != this->posts_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      j["post"] = a;
    }
    
    // print out comments
    if ( ! this->comments_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin();
           it != this->comments_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      j["comment"] = a;
    }
    node["channel"] = j;
    
    return true;
  }
  /*
  bool Channel::marshalJson( json_t* node ) const
  {
    assert( node );

    json_t* channel = json_object();
    json_object_set_new( node, "channel", channel );
    
    if ( this->hasGid() ) {
      json_object_set_new( channel, "id", json_stringn_nocheck( gid_.str(), gid_.size() ) );
    }

    if ( this->header_ ) {
      json_t* header = json_object();
      if ( this->header_->marshalJson( header ) ) {
        json_object_set_new( channel, "header", header );
      }
    }
    
    // print out posts
    if ( ! this->posts_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( channel, "post", array );
      for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin();
           it != this->posts_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }

    // print out comments
    if ( ! this->comments_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( channel, "comment", array );
      for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin();
           it != this->comments_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }
    
    return true;
  }
  */

  bool Channel::unmarshalJson( const nlohmann::json& node )
  {
    json j;
    if ( node.contains("channel")) {
      j = node["channel"];
    } else if ( node.contains("channelIdentity")) {
      j = node["channelIdentity"];
    }
    if ( j.is_object() ) {
      if ( j.contains("id")) {
        this->gid_.str( j["id"] );
      }
      if ( j.contains("anchor")) {
        this->syncAnchor_.str( j["anchor"] );
      }
      if ( j.contains("sync_time")) {
        this->syncAnchor_.str( j["sync_time"] );
      }

      // unmarshal header
      if ( j.contains("header")) {
        json j2 = j["header"];
        if ( j2.is_object() ) {
          if (! this->header_ )
            this->header_ = shared_ptr<ChannelHeader>(new ChannelHeader());
          if (! header_->unmarshalJson( j2 )) {
            return false;
          }
          this->header_->setSecure( this->header_->isEncrypted() );
        }
      }

      // unmarshal comments
      if ( j.contains("comment")) {
        json j2 = j["comment"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<ChannelComment> entity = Utilities::findOrAppend( this->comments_, i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      // unmarshal posts
      if ( j.contains("post")) {
        json j2 = j["post"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<ChannelPost> entity = Utilities::findOrAppend( this->posts_, i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }
      
      return true;
    }
    return false;
  }
  /*
  bool Channel::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* channel = json_object_get(node, "channel");
    if (! channel)
      channel = json_object_get(node, "channelIdentity");
    if (json_is_object(channel))
    {
      json_t* value;
      value = json_object_get(channel, "id");
      if ( json_is_string(value) ) {
        this->gid_.bytea( json_string_value(value), json_string_length(value) );
      }
      value = json_object_get(channel, "anchor");
      if ( json_is_string(value) ) {
        this->syncAnchor_.bytea( json_string_value(value), json_string_length(value) );
      }
      value = json_object_get(channel, "sync_time");
      if ( json_is_string(value) ) {
        this->syncTime_.bytea( json_string_value(value), json_string_length(value) );
      }
      
      value = json_object_get(channel, "header");
      if ( value ) {
        if (! this->header_ )
          this->header_ = shared_ptr<ChannelHeader>(new ChannelHeader());
        if (! header_->unmarshalJson( value )) {
          return false;
        }
        this->header_->setSecure( this->header_->isEncrypted() );
      }
      
      // unmarshal comments
      value = json_object_get(channel, "comment");
      if (json_is_array(value)) {
        size_t index;
        json_t* child;
        json_array_foreach(value, index, child) {
          shared_ptr<ChannelComment> entity = Utilities::findOrAppend( this->comments_, index );
          if (! entity || ! entity->unmarshalJson( child )) {
            return false;
          }
        }
      }
      
      // unmarshal posts
      value = json_object_get(channel, "post");
      if (json_is_array(value)) {
        size_t index;
        json_t* child;
        json_array_foreach(value, index, child) {
          shared_ptr<ChannelPost> entity = Utilities::findOrAppend( this->posts_, index );
          if (! entity || ! entity->unmarshalJson( child )) {
            return false;
          }
        }
      }
      
      return true;
    }
    return false;
  }
  */

  // XML Mapping
  
  // implements a simple state engine to handle entities for add, update and remove
  ISerializableEntity* Channel::entityByXmlNode( const char* name, int position ) 
  {
    if (strcmp(name, "header") == 0) {
      
      if (! this->header_) {
        this->header_ = shared_ptr<ChannelHeader>(new ChannelHeader());
      }
      return this->header_.get();
      
    } else if (strcmp(name, "post") == 0) {
      shared_ptr<ChannelPost> post;
      if ((int) this->posts_.size() > position) {
        // return existing node
        list< shared_ptr<ChannelPost> >::iterator i = this->posts_.begin();
        advance(i, position);
        post = *i;
      } else {
        // create new node
        post = shared_ptr<ChannelPost>(new ChannelPost());
        this->posts_.push_back(post);
      }
      return post.get();
      
    } else if (strcmp(name, "comment") == 0) {
      shared_ptr<ChannelComment> comment;
      if ((int) this->comments_.size() > position) {
        // return existing node
        list< shared_ptr<ChannelComment> >::iterator i = this->comments_.begin();
        std::advance(i, position);
        comment = *i;
      } else {
        // create new node
        comment = shared_ptr<ChannelComment>(new ChannelComment());
        this->comments_.push_back(comment);
      }
      return comment.get();
    }
    return this;
  }
  
  Value* Channel::valueByXmlNode( const char* name ) 
  {
    if (strcmp(name, "id") == 0) { 
      return &this->gid_;
    }
    if (strcmp(name, "anchor") == 0) { 
      return &this->syncAnchor_;
    }
    if (strcmp(name, "sync_time") == 0) {
      return &this->syncTime_;
    }
    return NULL; 
  }
  
  // generate core XML structure
  bool Channel::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const 
  {
    xml << ser::prolog("utf-8");
    xml << tag("channel"); // start root tag
    
    if ( this->hasGid() ) {
      xml << attr("id") << this->getGid(); // 1. level attribute
    }
    
    // print out header entitiy
    if ( this->header_ ) {
      this->header_->marshalXml( xml, aesKey, keyLength );
    }
    
    // print out post entities if any
    for (list< shared_ptr<ChannelPost> >::const_iterator it=this->posts_.begin(); it != this->posts_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    // print out comment entities if any
    for (list< shared_ptr<ChannelComment> >::const_iterator it=this->comments_.begin(); it != this->comments_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    xml << endtag(); // end root tag
    
    return true;
  }
  
  // ==========
  // Debugging
  // ==========
  
  ostream& operator<<( ostream& ostr, const Channel& rhs ) {
    return ostr << rhs.toString();
  }

}
