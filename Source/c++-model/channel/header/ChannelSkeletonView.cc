/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelSkeletonView.h"
#include "UsingEjinTypes.h"

#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<ChannelSkeletonView>::TABLE_FIELDS[] =
  {
    Attribute("CHANNEL_GID", type_text, flag_primary_key),
    Attribute("CHANNEL_ID", type_int, flag_not_null),
    Attribute("CHANNEL_SYNC_IND", type_int, flag_not_null),
    Attribute("CHANNEL_MEMBER_SYNC_IND", type_int, flag_not_null),
    Attribute("CHANNEL_MEDIA_SYNC_IND", type_int, flag_not_null),
    Attribute("POST_GID", type_text, flag_not_null),
    Attribute("POST_ID", type_int, flag_not_null),
    Attribute("POST_SYNC_IND", type_int, flag_not_null),
    Attribute("POST_MEDIA_SYNC_IND", type_int, flag_not_null),
    Attribute("COMMENT_GID", type_text, flag_not_null),
    Attribute("COMMENT_ID", type_int, flag_not_null),
    Attribute("COMMENT_SYNC_IND", type_int, flag_not_null),
    Attribute("COMMENT_MEDIA_SYNC_IND", type_int, flag_not_null),
    Attribute()
  };
  template <> const AttributeSet DataSchema<ChannelSkeletonView>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelSkeletonView::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["modifiedChannelProfileById"] = "CHANNEL_ID = ?1";
      tMap["conflictedChannelProfileById"] = "CHANNEL_ID = ?1 AND (CHANNEL_SYNC_IND = 4 OR CHANNEL_MEMBER_SYNC_IND = 4 OR POST_SYNC_IND = 4 OR COMMENT_SYNC_IND = 4)";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelSkeletonView::ChannelSkeletonView( void ):
  BaseEntity(TABLE_DDL, true) { }
  ChannelSkeletonView::ChannelSkeletonView( const ChannelSkeletonView& record ):
  ChannelSkeletonView() { this->operator=(record); }
  ChannelSkeletonView::~ChannelSkeletonView( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  ChannelSkeletonView& ChannelSkeletonView::operator=( const ChannelSkeletonView& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, ChannelGid);
      COPY_PROPERTY((*this), rhs, ChannelId);
      COPY_PROPERTY((*this), rhs, ChannelSyncInd);
      COPY_PROPERTY((*this), rhs, ChannelMemberSyncInd);
      COPY_PROPERTY((*this), rhs, ChannelMediaSyncInd);
      COPY_PROPERTY((*this), rhs, PostGid);
      COPY_PROPERTY((*this), rhs, PostId);
      COPY_PROPERTY((*this), rhs, PostSyncInd);
      COPY_PROPERTY((*this), rhs, PostMediaSyncInd);
      COPY_PROPERTY((*this), rhs, CommentGid);
      COPY_PROPERTY((*this), rhs, CommentId);
      COPY_PROPERTY((*this), rhs, CommentSyncInd);
      COPY_PROPERTY((*this), rhs, CommentMediaSyncInd);
    }
    return *this;
  }
  
  // comparison
  bool ChannelSkeletonView::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChannelSkeletonView& mrhs = dynamic_cast<const ChannelSkeletonView&> (rhs);
    return getChannelId() == mrhs.getChannelId() &&
    getPostId() == mrhs.getPostId() &&
    getCommentId() == mrhs.getCommentId();
  }
  bool ChannelSkeletonView::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChannelSkeletonView& mrhs = dynamic_cast<const ChannelSkeletonView&> (rhs); // throws std::bad_cast if not of type Member&
    
    if (this->getChannelId() < mrhs.getChannelId())
      return true;
    if (this->getPostId() < mrhs.getPostId())
      return true;
    if (this->getCommentId() < mrhs.getCommentId())
      return true;
    return false;
  }
  
  const char* ChannelSkeletonView::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelSkeletonView::tableFields( void ) const { return TABLE_DDL; }
  
}
