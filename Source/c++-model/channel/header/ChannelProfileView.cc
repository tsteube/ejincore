/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelProfileView.h"
#include "UsingEjinTypes.h"

#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<ChannelProfileView>::TABLE_FIELDS[] =
  {
    Attribute("CHANNEL_ID", type_int, flag_primary_key),
    Attribute("CHANNEL_GID", type_text, flag_not_null),
    Attribute("CHANNEL_MEMBERSHIP", type_int, flag_not_null),
    Attribute("CHANNEL_ROLE", type_int, flag_not_null),
    Attribute("CHANNEL_OUTDATED", type_int, flag_not_null),
    Attribute("CHANNEL_SYNC_ANCHOR", type_text, flag_not_null),
    Attribute("CHANNEL_SYNC_IND", type_int, flag_not_null),
    Attribute("MEMBER_SYNC_IND", type_int, flag_not_null),
    Attribute("POST_SYNC_IND", type_int, flag_not_null),
    Attribute("COMMENT_SYNC_IND", type_int, flag_not_null),
    Attribute("MEDIA_SYNC_IND", type_int, flag_not_null),
    Attribute()
  };
  template <> const AttributeSet DataSchema<ChannelProfileView>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelProfileView::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["allChannels"] =
        "channel_membership = 1";
      tMap["unchangedChannels"] =
        "channel_membership  = 1 AND "
        "channel_sync_ind    = 0 AND "
        "member_sync_ind     = 0 AND "
        "post_sync_ind       = 0 AND "
        "comment_sync_ind    = 0 AND "
        "media_sync_ind      = 0 ";
      tMap["addedChannels"] =
        "channel_sync_ind = 1";
      tMap["modifiedChannels"] =
        "channel_membership = 1 AND "
        "channel_sync_ind in (0,2) AND "
        "( "
        " channel_sync_ind = 2 OR "
        " member_sync_ind     in (1,2,3) OR "
        " post_sync_ind       in (1,2,3) OR "
        " comment_sync_ind    in (1,2,3) OR "
        " media_sync_ind      in (1,2,3) "
        ") ";
      tMap["removedChannels"] =
        "channel_membership = 1 AND "
        "channel_sync_ind = 3 AND "
        "( "
        " member_sync_ind    != 4 OR "
        " post_sync_ind      != 4 OR "
        " comment_sync_ind   != 4 OR "
        " media_sync_ind     != 4 "
        ") ";
      tMap["conflictedChannels"] =
        "channel_membership = 1 AND "
        "( "
        " channel_sync_ind   = 4 OR "
        " member_sync_ind    = 4 OR "
        " post_sync_ind      = 4 OR "
        " comment_sync_ind   = 4 OR "
        " media_sync_ind     = 4 "
        ") ";
      tMap["detectChannelConflict"] =
        "channel_gid = ?1 AND "
        "( "
        " channel_sync_ind    = 4 OR "
        " member_sync_ind     = 4 OR "
        " post_sync_ind       = 4 OR "
        " comment_sync_ind    = 4 OR "
        " media_sync_ind      = 4 "
        ") ";
      tMap["detectChannelMemberConflict"] =
        "channel_gid = ?1 AND member_sync_ind = 4";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelProfileView::ChannelProfileView( void ): BaseEntity(TABLE_DDL, true)
  { }
  ChannelProfileView::ChannelProfileView( const ChannelProfileView& record ): BaseEntity(record)
  { this->operator=(record); }
  ChannelProfileView::~ChannelProfileView( void )
  { }
  
  // ==========
  // Object Interface
  // ==========
  
  ChannelProfileView& ChannelProfileView::operator=( const ChannelProfileView& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Gid);
      COPY_PROPERTY((*this), rhs, Membership);
      COPY_PROPERTY((*this), rhs, Role);
      COPY_PROPERTY((*this), rhs, Outdated);
      COPY_PROPERTY((*this), rhs, SyncAnchor);
      COPY_PROPERTY((*this), rhs, HeaderSyncInd);
      COPY_PROPERTY((*this), rhs, MemberSyncInd);
      COPY_PROPERTY((*this), rhs, PostSyncInd);
      COPY_PROPERTY((*this), rhs, CommentSyncInd);
      COPY_PROPERTY((*this), rhs, MediaSyncInd);
    }
    return *this;
  }
  
  // comparison
  bool ChannelProfileView::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChannelProfileView& mrhs = dynamic_cast<const ChannelProfileView&> (rhs);
    return getId() == mrhs.getId();
  }
  bool ChannelProfileView::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChannelProfileView& mrhs = dynamic_cast<const ChannelProfileView&> (rhs); // throws std::bad_cast if not of type Member&
    return this->getId() < mrhs.getId();
  }
  
  const char* ChannelProfileView::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelProfileView::tableFields( void ) const { return TABLE_DDL; }
  
}
