/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderRepository.h"
#include "UsingEjinTypes.h"

#include "ChannelProfileView.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "ChannelMediaRepository.h"
#include "ChannelMemberRepository.h"
#include "ChangeLogRepository.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelMember.h"
#include "ChannelMedia.h"
#include "KeySequence.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "RSACrypto.h"

#include "Media.h"
#include "Member.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool ChannelHeaderRepository::detectChannelConflict( const string& channelGid ) const
  throw(data_access_error)
  {
    assert (! channelGid.empty());
    
    // call entity finder
    Value channel(channelGid.c_str());
    unique_ptr<ResultSet> conflicts( db_.find(ChannelProfileView::TABLE_NAME, "detectChannelConflict", &channel, NULL) );
    return conflicts->size() > 0;
  }
  
  bool ChannelHeaderRepository::clearConflict( const string& channelGid )
  throw(data_access_error)
  {
    assert (! channelGid.empty());
    shared_ptr<ChannelHeader> channelHeaderEntity( this->loadChannelHeaderInternal( Value(channelGid.c_str()), Value(false) ) );
    shared_ptr<ChannelHeader> conflict( this->loadChannelHeaderInternal( Value(channelGid.c_str()), Value(true) ) );
    
    bool cleared = false;
    if ( conflict ) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      if ( channelHeaderEntity ) {
        if ( channelHeaderEntity->isEmpty() ) {
          this->db_.remove( *channelHeaderEntity );
        } else {
          channelHeaderEntity->setSyncInd( kSyncIndSynchronous );
          channelHeaderEntity->setModificationMask( kNoneModification );
          this->db_.update( *channelHeaderEntity );
        }
      }
      cleared = true;
    }
    
    if ( channelHeaderEntity )
    {
      // search only for hidden/conflicted members
      Value channelId(channelHeaderEntity->getId());
      Value hidden(true);
      unique_ptr<ResultSet> allChannelMemberEntities = db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL);
      for (vector< BaseEntity* >::iterator it = allChannelMemberEntities->begin(); it != allChannelMemberEntities->end(); it++ ) {
        if ( this->channelMemberRepository_.clearConflict( *static_cast<ChannelMember*>(*it) ) ) {
          cleared = true;
        }
      }
    }
    
    return cleared;
  }
  
  shared_ptr<ChannelHeader> ChannelHeaderRepository::update( ChannelHeader& channelHeader, bool full, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasGid() );
    
    // call entity finder
    Value hidden(false);
    Value channelGid(channelHeader.getGid().c_str());
    shared_ptr<ChannelHeader> channelHeaderEntity(this->loadChannelHeaderInternal( channelGid, hidden ));
    unique_ptr<ResultSet> allChannelMemberEntities;
    unique_ptr<ResultSet> allChannelHeaderMediaEntities;
    
    // update algorithm
    unique_ptr<ChannelHeader> clone;
    if ( channelHeaderEntity ) {
      
      // setup sync anchor keys
      KeySequence entitySyncAnchor(channelHeaderEntity->getSyncAnchor());
      KeySequence nextSyncAnchor = entitySyncAnchor;
      KeySequence headerSyncAnchor;
      if ( channelHeader.hasSyncAnchor() ) {
        headerSyncAnchor = KeySequence(channelHeader.getSyncAnchor());
        if (headerSyncAnchor > entitySyncAnchor) {
          nextSyncAnchor = headerSyncAnchor;
        }
      }
      
      if ( nextSyncAnchor >= entitySyncAnchor ) {
        
        // load all channel posts of the specified channel instance
        Value channelId(channelHeaderEntity->getId());
        allChannelMemberEntities = db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL);
        allChannelHeaderMediaEntities =  db_.find(ChannelMedia::TABLE_NAME, "findAllChannelResources", &channelId, &hidden, NULL);
        
        if ( updateChannelHeader( *channelHeaderEntity, channelHeader, nextSyncAnchor, full, syncTime, allChannelHeaderMediaEntities.get(), allChannelMemberEntities.get() ) ) {
          if (full) {
            this->changeLogRepository_.update( *channelHeaderEntity, nextSyncAnchor, syncTime );
          } else {
            this->changeLogRepository_.outdated( *channelHeaderEntity, channelHeader.getSyncAnchor(), syncTime );
          }
        }
        
      } else {
        channelHeaderEntity.reset();
        
      }
      
    } else {
      
      if (! channelHeader.isEmpty()) {
        // add channel post entity
        channelHeaderEntity = addChannelHeader( channelHeader, full, syncTime );
        if ( channelHeaderEntity ) {
          this->changeLogRepository_.add( *channelHeaderEntity, channelHeader.getSyncAnchor(), syncTime );
        }
      } else {
        channelHeaderEntity.reset();
      }
    }
    
    return channelHeaderEntity;
  }
  
  void ChannelHeaderRepository::commit( ChannelHeader& channelHeader, const string& syncAnchor, const string& username, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasId() );
    assert( channelHeader.hasGid() );
    assert( ! syncAnchor.empty() );
    
    // resolve original channel for update
    ChannelHeader channelHeaderEntity;
    channelHeaderEntity.setId(channelHeader.getId());
    this->db_.refresh(channelHeaderEntity);
    
    // load all channel posts of the specified channel instance
    Value hidden(false);
    Value channelId(channelHeader.getId());
    unique_ptr<ResultSet> allChannelMemberEntities( db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL) );
    unique_ptr<ResultSet> allChannelHeaderMediaEntities(  db_.find(ChannelMedia::TABLE_NAME, "findChannelHeaderResources", &channelId, &hidden, NULL) );
    
    // update algorithm
    channelHeaderEntity.setSyncAnchor( syncAnchor );
    channelHeaderEntity.setModifyTime( syncTime );
    channelHeaderEntity.setModifiedBy( username );
    SyncInd syncInd = (SyncInd) channelHeaderEntity.getSyncInd();
    switch ( syncInd )
    {
      case kSyncIndSynchronous:
        channelHeaderEntity.setRole( channelHeader.getRole() );
        channelHeaderEntity.setSessionKey( channelHeader.getSessionKey() );
        this->db_.update( channelHeaderEntity );
        break;
      case kSyncIndConflict:
        channelHeaderEntity.setModificationMask( kChannelHeaderConflictedModification );
        this->db_.update( channelHeaderEntity );
        this->changeLogRepository_.update( channelHeaderEntity, syncAnchor, syncTime );
        break;
      case kSyncIndInsert:
        // update global identifier
        channelHeaderEntity.setGid( channelHeader.getGid() );
        channelHeaderEntity.setSecure( channelHeader.isSecure() );
        // update sync time to now
        channelHeaderEntity.setCreateTime( syncTime );
      case kSyncIndUpdate:
        channelHeaderEntity.setIV( channelHeader.getIV() );
        channelHeaderEntity.setSyncInd( kSyncIndSynchronous );
        channelHeaderEntity.setRole( channelHeader.getRole() );
        channelHeaderEntity.setMembership( channelHeader.getMembership() );
        if (channelHeader.hasSessionKey()) {
          channelHeaderEntity.setSessionKey( channelHeader.getSessionKey() );
        } else {
          channelHeaderEntity.clearSessionKey( );
        }
        channelHeaderEntity.setModificationMask( kNoneModification );
        this->db_.update( channelHeaderEntity );
        if ( syncInd == kSyncIndInsert ) {
          this->changeLogRepository_.add( channelHeaderEntity, syncAnchor, syncTime );
        } else {
          this->changeLogRepository_.update( channelHeaderEntity, syncAnchor, syncTime );
        }
        break;
      case kSyncIndDelete:
        this->removeChannelHeader( channelHeaderEntity, KeySequence(syncAnchor), allChannelHeaderMediaEntities.get(), allChannelMemberEntities.get(), syncTime );
        this->changeLogRepository_.remove( channelHeaderEntity, syncAnchor, syncTime );
        return;
    }
    
    // commit members
    for (vector< BaseEntity* >::iterator it = allChannelMemberEntities->begin(); it != allChannelMemberEntities->end(); it++ ) {
      ChannelMember* memberEntity(static_cast<ChannelMember*>(*it));
      shared_ptr<ChannelMember> member =
      Utilities::findElementIn (channelHeader.getMembers(), *static_cast<ChannelMember*>(memberEntity));
      this->channelMemberRepository_.commit( *memberEntity, member.get(), username, syncAnchor, syncTime );
    }
    
    // commit resources
    for (vector< BaseEntity* >::iterator mediaEntityIt=allChannelHeaderMediaEntities->begin(); mediaEntityIt != allChannelHeaderMediaEntities->end(); mediaEntityIt++) {
      ChannelMedia* mediaEntity(dynamic_cast<ChannelMedia*>(*mediaEntityIt)); // up cast
      shared_ptr<Resource> media =
      Utilities::findElementIn (channelHeader.getResources(), *static_cast<Resource*>(mediaEntity)); // down cast
      this->channelMediaRepository_.commit( *mediaEntity, dynamic_cast<ChannelMedia*>(media.get()),
                                           syncAnchor, username, syncTime );
    }
  }
  
  shared_ptr<ChannelHeader> ChannelHeaderRepository::updateHeaderOnly( ChannelHeader& channelHeader, const jtime& syncTime ) throw(data_access_error)
  {
    assert( channelHeader.hasGid() );
    
    // ignore sync anchor here
    shared_ptr<ChannelHeader> channelHeaderEntity(update( channelHeader, false, syncTime ));
    return channelHeaderEntity;
  }
  
  bool ChannelHeaderRepository::deleteChannel( ChannelHeader& channelHeader, const jtime& syncTime ) throw(data_access_error)
  {
    assert( channelHeader.hasGid() );
    assert( channelHeader.hasSyncAnchor() );
    
    // resolve original channel for update
    shared_ptr<ChannelHeader> channelHeaderEntity( this->loadChannelHeaderInternal( Value( channelHeader.getGid().c_str() ), Value( false ) ) );
    if ( ! channelHeaderEntity )
    {
      return false;
    }
    
    Value hidden(false);
    Value channelId(channelHeaderEntity->getId());
    
    // load all channel posts of the specified channel instance
    unique_ptr<ResultSet> allChannelMemberEntities( db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL) );
    unique_ptr<ResultSet> allChannelHeaderMediaEntities(  db_.find(ChannelMedia::TABLE_NAME, "findChannelHeaderResources", &channelId, &hidden, NULL) );
    
    channelHeaderEntity->setSyncAnchor( channelHeader.getSyncAnchor() );
    channelHeaderEntity->setModifiedBy( channelHeader.getModifiedBy() );
    bool result = removeChannelHeader( *channelHeaderEntity, KeySequence(channelHeader.getSyncAnchor()), allChannelHeaderMediaEntities.get(), allChannelMemberEntities.get(), syncTime );
    this->changeLogRepository_.remove( *channelHeaderEntity, channelHeader.getSyncAnchor(), syncTime );
    return result;
  }
  
  bool ChannelHeaderRepository::revert( const string& gid, const string& member, bool force )
  throw(data_access_error)
  {
    assert( ! gid.empty() );
    
    Value gidValue(gid.c_str());
    shared_ptr<ChannelHeader> channelHeaderEntity( this->loadChannelHeaderInternal( gidValue, Value(false) ) );
    if ( channelHeaderEntity != NULL ) {
      return channelMemberRepository_.revert( channelHeaderEntity->getId(), member, force );
    }
    return false;
  }
  
  bool ChannelHeaderRepository::revert( const string& gid, bool force )
  throw(data_access_error)
  {
    assert( ! gid.empty() );
    
    bool reverted = false;
    Value gidValue(gid.c_str());
    shared_ptr<ChannelHeader> channelHeaderEntity( this->loadChannelHeaderInternal( gidValue, Value(false) ) );
    if ( channelHeaderEntity != NULL )
    {
      // ensure that there a no pending conflicts
      if ( clearConflict( gid ) ) {
        // reload after conflict has been cleared
        channelHeaderEntity = this->loadChannelHeaderInternal( gidValue, Value(false) );
      }
      
      switch ( channelHeaderEntity->getSyncInd() ) {
        case kSyncIndInsert:
        {
          if (force) {
            db_.remove( *channelHeaderEntity );
            reverted = true;
          } else {
            cloneChannelHeader(*channelHeaderEntity, NULL, NULL);
            reverted = true;
          }
          break;
        }
        case kSyncIndSynchronous:
        case kSyncIndUpdate:
        case kSyncIndDelete:
        {
          // load entity from backup
          shared_ptr<ChannelHeader> channelHeaderEntityBak( shared_ptr<ChannelHeader>(new ChannelHeader()) );
          channelHeaderEntityBak->setId(channelHeaderEntity->getId());
          db_.refresh(*channelHeaderEntityBak, channelHeaderEntityBak->backupSchemaName() );
          
          if ( force ) {
            // force update entity with data from backup
            channelHeaderEntity->copyMetaDataFrom( *channelHeaderEntityBak );
            channelHeaderEntity->copyDataFrom( *channelHeaderEntityBak );
            channelHeaderEntity->setSyncInd( kSyncIndSynchronous );
            channelHeaderEntity->setModificationMask( kNoneModification );
            this->db_.update( *channelHeaderEntity );
            reverted = true;
          } else {
            KeySequence syncAnchor(channelHeaderEntity->getSyncAnchor());
            channelHeaderEntityBak->copyMetaDataFrom( *channelHeaderEntity );
            if (cloneChannelHeader(*channelHeaderEntity,
                                   channelHeaderEntityBak.get(), &syncAnchor)) {
              reverted = true;
            }
          }
          break;
        }
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "header[id=%d] in conflict state", channelHeaderEntity->getId());
      }
      
      // search only for hidden/conflicted members
      Value channelId(channelHeaderEntity->getId());
      Value hidden(false);
      unique_ptr<ResultSet> allChannelMemberEntities =
      db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL);
      for ( vector< BaseEntity* >::iterator it = allChannelMemberEntities->begin();
           it != allChannelMemberEntities->end();
           it++ ) {
        ChannelMember* channelMemberEntity = static_cast<ChannelMember*>(*it);
        if (channelMemberRepository_.revert(channelHeaderEntity->getId(), channelMemberEntity->getUsername(), force)) {
          reverted = true;
        }
      }
      
      // revert new dependent entities
      if (channelPostRepository_.revertNewPosts( channelHeaderEntity->getId(), force )) {
        reverted = true;
      }
      if (channelCommentRepository_.revertNewComments( channelHeaderEntity->getId(), 0, force )) {
        reverted = true;
      }
      if (channelMediaRepository_.revertNewMedias( channelHeaderEntity->getId(), 0, 0, force )) {
        reverted = true;
      }
      
    } else {
      // restore old member if found in backup
      unique_ptr<ResultSet> values( db_.find(ChannelHeader::TABLE_NAME, "findChannelByGidBak", &gidValue, NULL) );
      vector< BaseEntity* >::iterator it = values->begin();
      if (it != values->end()) {
        this->db_.insert( **it );
        reverted = true;
      }
      
    }
    
    return reverted;
  }
  
  bool ChannelHeaderRepository::createAESKey( ChannelHeader& header )
  throw(security_integrity_error,data_access_error)
  {
    if (header.isSecure()) {
      AESCrypto crypto;
      crypto.setRandomSessionKey();
      return this->publishAESKey( header, crypto.getSessionKey( ), true );
    }
    return false;
  }
  
  list<string> ChannelHeaderRepository::detectLostKey( ChannelHeader& header )
    throw(security_integrity_error,data_access_error)
  {
    list<string> result;
    Value uid(header.getId());
    Value hidden(false);
    unique_ptr<ResultSet> allChannelMemberEntities = db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &uid, &hidden, NULL);
    for (vector< BaseEntity* >::iterator it = allChannelMemberEntities->begin();
         it != allChannelMemberEntities->end(); it++ ) {
      auto res = channelMemberRepository_.detectLostKey(header.getId(), static_cast<ChannelMember*>(*it)->getUsername());
      result.insert(result.end(), res.begin(), res.end());
    }
    return result;
  }

  bool ChannelHeaderRepository::publishAESKey( ChannelHeader& header, const string& aesKey, bool force )
  throw(security_integrity_error,data_access_error)
  {
    bool changed = false;
    if ( ! aesKey.empty() ) {
      switch (header.getRole()) {
        case kSyncRoleOwner:
        case kSyncRoleAdmin:
        {
          Value uid(header.getId());
          Value hidden(false);
          unique_ptr<ResultSet> allChannelMemberEntities = db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &uid, &hidden, NULL);
          for (vector< BaseEntity* >::iterator it = allChannelMemberEntities->begin();
               it != allChannelMemberEntities->end(); it++ ) {
            if ( channelMemberRepository_.setAESKey(header.getId(), static_cast<ChannelMember*>(*it)->getUsername(), aesKey, force) )
              changed = true;
          }
          break;
        }
        case kSyncRoleRead:
        case kSyncRoleWrite:
          break;
      }
    }
    return changed;
  }
  
  bool ChannelHeaderRepository::importSessionKey( integer id, const string& username, const string& aesKey )
  throw(data_access_error)
  {
    if ( id <= 0 || aesKey.empty() ) {
      return false;
    }
    Value channelId( id );
    shared_ptr<ChannelHeader> channelHeaderEntity = this->loadById( channelId );
    if (channelHeaderEntity) {
      channelHeaderEntity->setSessionKey2( aesKey );
      channelHeaderEntity->setInvalidSessionKey( false );
      this->db_.update( *channelHeaderEntity );
      return true;
    }
    return false;
  }

  // ==========
  // Private Interface
  // ==========
  
  shared_ptr<ChannelHeader> ChannelHeaderRepository::addChannelHeader( ChannelHeader& channelHeader, bool full, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasGid() );
    
    // clear potential old conflict data set first
    clearConflict( channelHeader.getGid() );
    
    // create new entity with unique global key
    shared_ptr<ChannelHeader> channelHeaderEntity = shared_ptr<ChannelHeader>(new ChannelHeader( channelHeader ));
    channelHeaderEntity->clearId(); // generate new local database identifier
    channelHeaderEntity->setSyncInd( kSyncIndSynchronous );
    channelHeaderEntity->setContentModifyTime( channelHeader.getModifyTime() );
    channelHeaderEntity->setModificationMask(kChannelHeaderRecentRemoteModification);
    channelHeaderEntity->setModifiedBy( channelHeader.getOwner() );
    channelHeaderEntity->getMembers().clear(); // will be created later
    channelHeaderEntity->getResources().clear(); // will be created later
    
    if (! full) {
      // mark as outdated to fully sync later
      channelHeaderEntity->setOutdated( true );
      channelHeaderEntity->clearSyncAnchor( );
    }
    
    // persist entity
    this->db_.insert( *channelHeaderEntity );
    
    // set id
    channelHeader.setId( channelHeaderEntity->getId() );
    channelHeader.setSyncInd( channelHeaderEntity->getSyncInd() );
    channelHeader.setModifiedBy( channelHeaderEntity->getModifiedBy() );
    
    updateResources( channelHeader, NULL );
    updateMembers( channelHeader, NULL, syncTime );
    
    return channelHeaderEntity;
  }
  
  bool ChannelHeaderRepository::updateChannelHeader( ChannelHeader& channelHeaderEntity, ChannelHeader& channelHeader, KeySequence& syncAnchor, bool full, const jtime& syncTime, ResultSet* allChannelHeaderMediaEntities, ResultSet* allChannelMemberEntities )
  throw(data_access_error)
  {
    assert( channelHeaderEntity.hasId() );
    assert( ! channelHeaderEntity.isHidden() );
    assert( channelHeader.hasGid() );
    const KeySequence entitySyncAnchor( channelHeaderEntity.getSyncAnchor() );
    
    if (! full && entitySyncAnchor < syncAnchor) {
      // mark as not fully updated
      channelHeaderEntity.setOutdated( true );
      // and do not increment sync anchor
      syncAnchor = entitySyncAnchor;
    } else {
      channelHeaderEntity.setOutdated( false );
    }
    
    bool result = false;
    switch ( channelHeaderEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // must be unknown on the server
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of ChannelHeader[id=%d]", channelHeaderEntity.getId());
      case kSyncIndSynchronous:
        // entity was updated on the server
        channelHeaderEntity.copyMetaDataFrom( channelHeader );
        channelHeaderEntity.setSyncAnchor( syncAnchor );
        if ( channelHeader.hasContent() && ! channelHeaderEntity.hasEqualContent( channelHeader ) ) {
          channelHeaderEntity.setModificationMask(kChannelHeaderRecentRemoteModification);
          result = true;
        }
        channelHeaderEntity.copyDataFrom( channelHeader );
        this->db_.update( channelHeaderEntity );
        break;
      case kSyncIndUpdate:
        if (channelHeader.hasContent()) {
          if ( channelHeaderEntity.hasEqualContent( channelHeader ) )
          {
            // update only some dates and state data
            channelHeaderEntity.copyMetaDataFrom( channelHeader );
            channelHeaderEntity.copyDataFrom( channelHeader );
            channelHeaderEntity.setSyncInd( kSyncIndSynchronous );
            channelHeaderEntity.setSyncAnchor( syncAnchor );
            channelHeaderEntity.setModificationMask( kNoneModification );
            this->db_.update( channelHeaderEntity );
            result = true;
            break;
          } else {
            // conflict detected; save local entity for reference
            cloneChannelHeader( channelHeaderEntity, &channelHeader, &syncAnchor );
            result = true;
          }
        }
        channelHeaderEntity.copyMetaDataFrom( channelHeader );
        channelHeaderEntity.setSyncAnchor( syncAnchor );
        this->db_.update( channelHeaderEntity );
        break;
      case kSyncIndDelete:
        if ( channelHeader.hasContent() )
        {
          // conflict detected; save local entity for reference
          cloneChannelHeader( channelHeaderEntity, &channelHeader, &syncAnchor );
          result = true;
        } else {
          // no change on server side but removed on client
          channelHeaderEntity.setSyncAnchor( syncAnchor );
          // revert delete on client to avoid deleting unseen data
          channelHeaderEntity.setSyncInd( kSyncIndSynchronous );
          channelHeaderEntity.setModificationMask( kNoneModification );
          this->db_.update( channelHeaderEntity );
          result = true;
        }
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelHeader> conflict( this->loadChannelHeaderInternal( Value(channelHeaderEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelHeaderEntity.getId());
        }
        if ( conflict->hasEqualContent( channelHeader ) )
        {
          // resolve conflict
          switch ( conflict->getSyncInd() ) {
            case kSyncIndSynchronous:
            case kSyncIndConflict:
              _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelHeaderEntity.getId());
            case kSyncIndInsert:
            case kSyncIndUpdate:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // client and server are in sync now again
              // update only some dates and state data
              channelHeaderEntity.copyMetaDataFrom( channelHeader );
              channelHeaderEntity.copyDataFrom( channelHeader );
              channelHeaderEntity.setSyncInd( kSyncIndSynchronous );
              channelHeaderEntity.setSyncAnchor( syncAnchor );
              channelHeaderEntity.setModificationMask( kNoneModification );
              this->db_.update( channelHeaderEntity );
              result = true;
              break;
            case kSyncIndDelete:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // remember that the member was already deleted on the client
              channelHeaderEntity.setSyncInd( kSyncIndDelete );
              this->db_.update( channelHeaderEntity );
              result = true;
              break;
          }
        } else {
          // keep original local conflict data
          // update only some dates and state data
          if ( channelHeader.hasContent() ) {
            channelHeaderEntity.copyMetaDataFrom( channelHeader );
            channelHeaderEntity.copyDataFrom( channelHeader );
          } else {
            channelHeaderEntity.clearData( );
          }
          channelHeaderEntity.setSyncInd( kSyncIndConflict );
          channelHeaderEntity.setSyncAnchor( syncAnchor );
          this->db_.update( channelHeaderEntity );
          result = true;
        }
        break;
    }
    
    // update id
    channelHeader.setId( channelHeaderEntity.getId() );
    
    if ( full ) {
      
      // check for ordering changes first
      if (allChannelHeaderMediaEntities) {
        if (! Utilities::hasEqualOrdering( channelHeader.getResources(), *allChannelHeaderMediaEntities )) {
          channelHeaderEntity.setModificationMask( channelHeaderEntity.getModificationMask() | kChannelHeaderRecentRemoteModification );
          this->db_.update( channelHeaderEntity );
        }
      }
      
      // update resources
      updateResources( channelHeader, allChannelHeaderMediaEntities );
      
      // update members
      updateMembers( channelHeader, allChannelMemberEntities, syncTime );
    }
    
    return result;
  }
  
  bool ChannelHeaderRepository::removeChannelHeader( ChannelHeader& channelHeaderEntity, const KeySequence& syncAnchor, ResultSet* allChannelHeaderMediaEntities, ResultSet* allChannelMemberEntities, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeaderEntity.hasId() );
    assert( ! channelHeaderEntity.isHidden() );
    
    switch ( channelHeaderEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // must be unknown on the server
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of ChannelHeader[id=%d]", channelHeaderEntity.getId());
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        channelHeaderEntity.setSyncInd(kSyncIndInsert);
        cloneChannelHeader( channelHeaderEntity, NULL, &syncAnchor );
        return false;
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        deleteChannelHeader( channelHeaderEntity, allChannelHeaderMediaEntities, allChannelMemberEntities, syncTime);
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelHeader> conflict( this->loadChannelHeaderInternal( Value(channelHeaderEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelHeaderEntity.getId());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndInsert:
          case kSyncIndConflict:
            _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelHeaderEntity.getId());
          case kSyncIndDelete:
            // entity was removed on client and server
            this->db_.remove( *conflict );
            deleteChannelHeader( channelHeaderEntity, allChannelHeaderMediaEntities, allChannelMemberEntities, syncTime);
            break;
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            // and mark entity as removed from server
            channelHeaderEntity.clearData();
            channelHeaderEntity.setSyncInd( kSyncIndConflict );
            channelHeaderEntity.setSyncAnchor( syncAnchor );
            this->db_.update( channelHeaderEntity );
            break;
        }
        break;
    }
    return true;
  }
  
  void ChannelHeaderRepository::deleteChannelHeader( ChannelHeader& channelHeaderEntity, ResultSet* allChannelHeaderMediaEntities, ResultSet* allChannelMemberEntities, const jtime& syncTime )
  throw(data_access_error)
  {
    
    // 1. delete resources
    channelHeaderEntity.getResources().clear();
    this->updateResources( channelHeaderEntity, allChannelHeaderMediaEntities );
    
    // 2. delete members
    channelHeaderEntity.getMembers().clear();
    this->updateMembers( channelHeaderEntity, allChannelMemberEntities, syncTime );
    
    // Note: all posts and comments of this channel gets removed by foreign key constraints
    //this->channelPostRepository_.deleteAllChannelPosts( channelHeaderEntity.getId() );
    
    // 3. delete entity itself
    this->db_.remove(channelHeaderEntity);
  }
  
  bool ChannelHeaderRepository::cloneChannelHeader( ChannelHeader& channelHeaderEntity, const ChannelHeader* channelHeader, const KeySequence* syncAnchor )
  throw(data_access_error)
  {
    if ( channelHeader == NULL || ! channelHeaderEntity.hasEqualContent( *channelHeader ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<ChannelHeader> clone = unique_ptr<ChannelHeader>(new ChannelHeader( channelHeaderEntity ));
      clone->clearId( );
      clone->setMasterId( channelHeaderEntity.getId() );
      clone->setOutdated( false );
      clone->setModificationMask( clone->getModificationMask()|kChannelHeaderConflictedModification );
      
      // update only some dates and states
      if ( channelHeader )
      {
        // update dates and states
        channelHeaderEntity.copyMetaDataFrom( *channelHeader );
        channelHeaderEntity.copyDataFrom( *channelHeader );
      }
      else
      {
        // mark entity as removed from server
        channelHeaderEntity.clearData();
      }
      channelHeaderEntity.setSyncInd( kSyncIndConflict );
      if ( syncAnchor )
        channelHeaderEntity.setSyncAnchor( *syncAnchor );
      channelHeaderEntity.setModificationMask( kChannelHeaderConflictedModification|kChannelHeaderRecentRemoteModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( channelHeaderEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      return true;
    }
    return false;
  }
  
  bool ChannelHeaderRepository::updateResources( const ChannelHeader& channelHeader2, ResultSet* allChannelHeaderMediaEntities )
  throw(data_access_error)
  {
    bool updated = false;
    ChannelHeader& channelHeader(const_cast<ChannelHeader&>(channelHeader2));
    list< shared_ptr<Resource> > resources(channelHeader.getResources());
    
    if (allChannelHeaderMediaEntities) {
      for (vector< BaseEntity* >::iterator it = allChannelHeaderMediaEntities->begin(); it != allChannelHeaderMediaEntities->end(); it++ ) {
        ChannelMedia* mediaEntity(static_cast<ChannelMedia*>(*it));
        shared_ptr<Resource> media = Utilities::findElementIn (resources, *static_cast<Resource*>(mediaEntity) );
        if ( media ) {
          // ensure correct relationship
          media->setChannelId( channelHeader.getId() );
          media->clearPostId();
          media->clearCommentId();
          media->setModifiedBy( channelHeader.getModifiedBy() );
          media->setNo( Utilities::indexOf (channelHeader.getResources(), *static_cast<Resource*>(media.get()) ) );
          
          // update membership
          this->channelMediaRepository_.update( *mediaEntity, *dynamic_cast<ChannelMedia*>(media.get()) );
          updated = true;
          
          resources.remove(media);
        } else {
          // resources gets deleted on media list sync later
        }
      }
    }
    
    for ( list< shared_ptr<Resource> >::iterator mediaIt = resources.begin(); mediaIt != resources.end(); mediaIt++ ) {
      // ensure correct relationship
      (*mediaIt)->setChannelId( channelHeader.getId() );
      (*mediaIt)->setNo( Utilities::indexOf (channelHeader.getResources(), **mediaIt) );
      (*mediaIt)->setModifiedBy( channelHeader.getModifiedBy() );

      // add membership
      this->channelMediaRepository_.add( *dynamic_cast<ChannelMedia*>((*mediaIt).get()) );
      updated = true;
    }
    
    return updated;
  }
  
  bool ChannelHeaderRepository::updateMembers( ChannelHeader& channelHeader, ResultSet* allChannelMemberEntities, const jtime& syncTime )
  throw(data_access_error)
  {
    bool updated = false;
    // ensure correct relationship
    for (list< shared_ptr<ChannelMember> >::iterator it = channelHeader.getMembers().begin(); it != channelHeader.getMembers().end(); it++ ) {
      (*it)->setChannelId( channelHeader.getId() );
    }
    list< shared_ptr<ChannelMember> > members(const_cast<ChannelHeader&>(channelHeader).getMembers());
    if (allChannelMemberEntities) {
      for (vector< BaseEntity* >::iterator it = allChannelMemberEntities->begin(); it != allChannelMemberEntities->end(); it++ ) {
        ChannelMember* memberEntity(static_cast<ChannelMember*>(*it));
        
        shared_ptr<ChannelMember> member = Utilities::findElementIn (members, *memberEntity);
        if (! member ) {
          if (memberEntity->getChannelId() == channelHeader.getId()) {
            // delete resource
            memberEntity->setModifiedBy( channelHeader.getModifiedBy() );
            if ( this->channelMemberRepository_.remove( *memberEntity, channelHeader.getSyncAnchor(), syncTime ) )
            {
              updated = true;
            }
          }
          
        } else {
          members.remove(member);
          // update membership
          member->setModifiedBy( channelHeader.getModifiedBy() );
          this->channelMemberRepository_.update( *memberEntity, *member, channelHeader.getSyncAnchor(), syncTime );
          updated = true;
        }
      }
    }
    
    for (list< shared_ptr<ChannelMember> >::iterator it = members.begin(); it != members.end(); it++ ) {
      // add membership
      (*it)->setModifiedBy( channelHeader.getModifiedBy() );
      this->channelMemberRepository_.add( **it, channelHeader.getSyncAnchor(), syncTime );
      updated = true;
    }
    
    return updated;
  }
  
}
