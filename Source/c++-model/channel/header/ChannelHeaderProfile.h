/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_HEADER_PROFILE_H__
#define __EJIN_CHANNEL_HEADER_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

namespace ejin
{
  
  /**
   * Methods to resolve mchannel comment entities by state.
   */
  class ChannelHeaderProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChannelHeaderProfile( Database& db, ChannelMemberRepository& channelMemberRepository ):
    db_( db ), channelMemberRepository_(channelMemberRepository) {}
    virtual ~ChannelHeaderProfile( void ) {}

    /**
     * Find all visible channel header entities.
     */ 
    list< shared_ptr<ChannelHeader> > findAcceptedAll( void ) const
    throw(data_access_error);    
    
    /**
     * Find all unchanged channel header entities.
     */ 
    list< shared_ptr<ChannelHeader> > findUnchanged( void ) const 
    throw(data_access_error);    
    
    /**
     * Find all added channel header entities.
     */ 
    list< shared_ptr<ChannelHeader> > findAdded( void ) const 
    throw(data_access_error);    
    
    /**
     * Find all modified channel header entities.
     */ 
    list< shared_ptr<ChannelHeader> > findUpdated( void ) const 
    throw(data_access_error);    
    
    /**
     * Find all deleted channel header entities.
     */ 
    list< shared_ptr<ChannelHeader> > findRemoved( void ) const 
    throw(data_access_error);    
    
    /**
     * Find all conflicted channel data sets. If the channel instance was edited locally and on the server both data
     * sets are kept to resolve the conflict by user interaction.
     */ 
    list< shared_ptr<ChannelHeader> > findConflicted( void ) const 
    throw(data_access_error);  
    
    /**
     * Find all channel header entities that are known to be changed on the server. These channels must be synchronized.
     */ 
    list< shared_ptr<ChannelHeader> > findOutdated( void ) const 
    throw(data_access_error);    
    
    /**
     * Find all encrypted channel header entities.
     */
    list< shared_ptr<ChannelHeader> > findInvalidSessionKeys( void ) const
    throw(data_access_error);
    
    /**
     * Find all new channels the specified member has been invited
     */
    list< string > findInvitations( ) const
    throw(data_access_error);    
    
    /**
     * Returns global identifiers of channel that the current user will accept 
     */
    list< string > findChannelToAccept( ) const
    throw(data_access_error);    
    
    /**
     * Returns global identifiers of channel that the current user will reject
     */
    list< string > findChannelToReject( ) const
    throw(data_access_error);    
    
    /**
     * Returns true if the current member has already accepted the specified channel
     */
    bool isMemberOf( const string& username, const string& channelGid ) const
    throw(data_access_error);    
    
    /*
     * Load a single channel header entity with all resources and member references.
     */ 
    shared_ptr<ChannelHeader> loadByGid( const string& channelGid, ResourceType resourceType = kResourceTypeChannelMedia ) const
    throw(data_access_error);
    
    /**
     * Load entity instance from persistence store by unique identifier
     */ 
    shared_ptr<ChannelHeader> loadById( const sqlite::Value& channelId, ResourceType resourceType = kResourceTypeChannelMedia ) const
    throw(data_access_error);
    
    /*
     * Fill raw channel header entity with all resources and member references.
     */
    shared_ptr<ChannelHeader> resolve( shared_ptr<ChannelHeader> header, ResourceType resourceType ) const
    throw(data_access_error);
    
    /**
     * Returns the current container modificationMask of the channel with the specified identifier
     */
    EntityModification getEntityModification( integer channelId ) const
    throw(data_access_error);

    /**
     * Returns the current channel modificationMask of the channel with the specified identifier
     */
    EntityModification getChannelModification( integer channelId ) const
    throw(data_access_error);
    
    /**
     * Returns the overall access role of the specified entity instance
     */
    MemberRole getAccessRole( integer channelId, const char* username )
    const throw(data_access_error);    

    /**
     * Resolve the session key for the specified channel and user.
     * Decrypt the key with the user private key.
     */
    bool resolveSessionKey( integer channelId, const string& username, RSACrypto* crypto, bool required ) const
    throw(security_integrity_error,data_access_error);
    
    /**
     * Resolve the session key for the specified channel and user.
     * Decrypt the key with the user private key.
     */
    bool resolveSessionKey( const string& channelGid, const string& username, RSACrypto* crypto, bool required ) const
    throw(security_integrity_error,data_access_error);
    
    /**
     * Resolve the session key for the specified channel and user.
     * Decrypt the key with the user private key.
     */
    bool resolveSessionKey( const ChannelHeader& channelHeader, const string& username, RSACrypto* crypto, bool required ) const
    throw(security_integrity_error,data_access_error);
    
    /**
     * Full Text Search for channel header.
     */
    list<integer> searchFullText( const string& pattern )
    throw(data_access_error);

    /**
     * Read current session key
     */
    string exportSessionKey( integer channelId, const string& username  ) const
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    /*
     * Load the list of channel comments from the database. <br>
     */ 
    list< shared_ptr<ChannelHeader> > refreshEntity( const list<integer>& list ) const 
    throw(data_access_error);
    
    /**
     * Load entity instance from persistence store.
     */ 
    shared_ptr<ChannelHeader> loadChannelHeaderInternal( const sqlite::Value& channelGid, const sqlite::Value& hidden ) const 
    throw(data_access_error);
    
    // --------
    // Member Variables 
    
    ChannelMemberRepository&  channelMemberRepository_;
    Database& db_;

  private:
    
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ChannelHeaderProfile );

  };
  
}

#endif // __EJIN_CHANNEL_HEADER_PROFILE_H__
