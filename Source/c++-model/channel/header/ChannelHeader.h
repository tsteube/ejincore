/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_HEADER_ENTITY_H__
#define __EJIN_CHANNEL_HEADER_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"
#include "Exceptions.h"

namespace ejin
{
  
  /**
   * Entity Class for CHANNEL_TBL
   */
  class ChannelHeader: public BaseEntity, public IBusinessData, public DataSchema<ChannelHeader> {
    friend class Database;
    friend class ChannelHeaderProfile;
    friend class ChannelHeaderRepository;
    friend class MediaRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "CHANNEL_TBL";
    
    // ctors
    ChannelHeader( void );
    ChannelHeader( const integer id );
    ChannelHeader( const string& gid );
    ChannelHeader( const ChannelHeader& record );
    // dtors
    ~ChannelHeader( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChannelHeader& operator=( const ChannelHeader& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ChannelHeader(*this); }
    // debug
    std::string toString( void ) const;
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool copyMetaDataFrom( const BaseEntity& rhs );
    bool copyDataFrom( const BaseEntity& rhs );
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool isEmpty( void ) const;
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );
    
    void endXmlNode( const char* name );
    ISerializableEntity* entityByXmlNode( const char* name, int position );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const integer getChannelId( void ) const { return getId(); }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return hasGid() ? getGid().c_str() : ""; }
    bool hasChannelGid( void ) const { return hasGid(); }
    
    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_STRING_PROPERTY_INTF( Gid, 1 )
    ACCESS_STRING_PROPERTY_INTF( Owner, 2 )
    ACCESS_STRING_PROPERTY_INTF( OwnerFullname, 3 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedBy, 4 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedByFullname, 5 )
    ACCESS_STRING_PROPERTY_INTF( Name, 6 )
    ACCESS_INTEGER_PROPERTY_INTF( State, 7 )
    ACCESS_STRING_PROPERTY_INTF( Tags, 8 )
    ACCESS_STRING_PROPERTY_INTF( SessionKey, 9 )
    ACCESS_STRING_PROPERTY_INTF( SessionKey2, 10 )
    ACCESS_BOOLEAN_PROPERTY_INTF( InvalidSessionKey, 11 )
    ACCESS_INTEGER_PROPERTY_INTF( Role, 12 )
    ACCESS_INTEGER_PROPERTY_INTF( Membership, 13 )
    ACCESS_STRING_PROPERTY_INTF( Content, 14 )
    ACCESS_TIME_PROPERTY_INTF( SyncTime, 15 )
    ACCESS_TIME_PROPERTY_INTF( LastSyncTime, 16 )
    ACCESS_TIME_PROPERTY_INTF( CreateTime, 17 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 18 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 19 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 20 )
    ACCESS_INTEGER_PROPERTY_INTF( MembershipInd, 21 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 22 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Outdated, 23 )
    ACCESS_TIME_PROPERTY_INTF( ContentModifyTime, 24 )
    ACCESS_INTEGER_PROPERTY_INTF( ModificationMask, 25 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Secure, 26 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Encrypted, 27 )
    ACCESS_STRING_PROPERTY_INTF( IV, 28 )
    bool isHidden() { return this->getMasterId() > 0; }
    
    // ==========
    // Relations
    // ==========
    list< shared_ptr<ChannelMember> >& getMembers( void ) { return this->members_; }
    void setMembers( const list< shared_ptr<ChannelMember> >& resources ) { this->members_ = resources; }
    
    list< shared_ptr<Resource> >& getResources( void ) { return this->resources_; }
    void setResources( const list< shared_ptr<Resource> >& resources ) { this->resources_ = resources; }
    
    // ==========
    // Metadata
    // ==========
    
    integer getEntityModificationMask( void ) const { return this->entityModificationMask_; }
    void setEntityModificationMask( integer mask ) { this->entityModificationMask_ = mask; }
    
    sqlite::jtime getEntityModificationTime( void ) const { return this->entityModificationTime_; }
    void setEntityModificationTime( sqlite::jtime time ) { this->entityModificationTime_ = time; }
    
    string getEntityModificationBy( void ) const { return this->entityModificationBy_; }
    void setEntityModificationBy( string by ) { this->entityModificationBy_ = by; }
    
    integer getChannelModificationMask( void ) const { return this->channelModificationMask_; }
    void setChannelModificationMask( integer mask ) { this->channelModificationMask_ = mask; }
    
    bool getChannelIsOutdated( void ) const { return this->channelIsOutdated_; }
    void setChannelIsOutdated( bool flag ) { this->channelIsOutdated_ = flag; }
    
    sqlite::jtime getChannelModificationTime( void ) const { return this->channelModificationTime_; }
    void setChannelModificationTime( sqlite::jtime time ) { this->channelModificationTime_ = time; }
    
    string getChannelModificationBy( void ) const { return this->channelModificationBy_; }
    void setChannelModificationBy( string by ) { this->channelModificationBy_ = by; }
    
    MemberRole getAccessRole( void ) const { return this->accessRole_; }
    void setAccessRole( MemberRole role ) { this->accessRole_ = role; }
    
    integer getContentSize( void ) const { return (this->contentSize_ > 0) ? this->contentSize_ : this->value("CONTENT")->size(); }
    void setContentSize( integer size ) { this->contentSize_ = size; }
    
    integer getTotalOutdatedMediaSize( void ) const { return this->totalOutdatedMediaSize_; }
    void setTotalOutdatedMediaSize( integer size ) { this->totalOutdatedMediaSize_ = size; }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;

    // Database schema
    const char* tableName( void ) const;
    
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Member Variables
    
    integer            entityModificationMask_;
    sqlite::jtime      entityModificationTime_;
    string             entityModificationBy_;
    integer            channelModificationMask_;
    bool               channelIsOutdated_;
    sqlite::jtime      channelModificationTime_;
    string             channelModificationBy_;
    MemberRole         accessRole_;
    integer            contentSize_;
    integer            totalOutdatedMediaSize_;

    list< shared_ptr<ChannelMember> > members_;
    list< shared_ptr<Resource> >      resources_;
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif // __EJIN_CHANNEL_HEADER_ENTITY_H__
