/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeaderProfile.h"
#include "UsingEjinTypes.h"

#include <iostream>

#include "Utilities.h"
#include "ChannelProfileView.h"
#include "ChannelMemberRepository.h"
#include "ChannelHeaderRepository.h"
#include "Media.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "ChannelHeader.h"
#include "ChannelComment.h"
#include "ChannelPost.h"
#include "SqliteValue.h"
#include "RSACrypto.h"
#include "ChannelFullText.h"

namespace ejin
{
  static bool compare_resource (shared_ptr<Resource> first, shared_ptr<Resource> second);
  static bool compare_member (shared_ptr<ChannelMember> first, shared_ptr<ChannelMember> second);
  
  // extract the unique integer from the list of base entities
  static list< integer > extractChannelIds( unique_ptr<ResultSet>&  values ) 
  {
    // extract unique identifier into a set
    list< integer > args;
    ejin::ChannelProfileView* mv;
    for (vector< sqlite::BaseEntity* >::iterator it=values->begin(); it != values->end(); it++) {
      mv = static_cast< ejin::ChannelProfileView* >(*it);
      args.push_back(mv->getId());
    }
    
    // resolve entities from unique identifiers
    return args;
  }
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findAcceptedAll( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find( ChannelProfileView::TABLE_NAME, "allChannels", NULL ) );
    
    // extract unique identifier into a set
    list< integer > args( extractChannelIds( values ) );
    
    // resolve entities from unique identifiers
    return refreshEntity(args);
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findUnchanged( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find( ChannelProfileView::TABLE_NAME, "unchangedChannels", NULL ) );
    
    // extract unique identifier into a set
    list< integer > args( extractChannelIds( values ) );
    
    // resolve entities from unique identifiers
    return refreshEntity(args);
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findAdded( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find( ChannelProfileView::TABLE_NAME, "addedChannels", NULL ) );
    
    // extract unique identifier into a set
    list< integer > args( extractChannelIds( values ) );
    
    // resolve entities from unique identifiers
    return refreshEntity(args);
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findUpdated( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelProfileView::TABLE_NAME, "modifiedChannels", NULL) );
    
    // extract unique identifier into a set
    list< integer > args(extractChannelIds(values));
    
    // resolve entities from unique identifiers
    return refreshEntity(args);
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findRemoved( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelProfileView::TABLE_NAME, "removedChannels", NULL) );
    
    // extract unique identifier into a set
    list< integer > args(extractChannelIds(values));
    
    // resolve entities from unique identifiers
    return refreshEntity(args);
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findConflicted( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelProfileView::TABLE_NAME, "conflictedChannels", NULL) );
    
    // extract unique identifier into a set
    list< integer > args(extractChannelIds(values));
    
    // resolve entities from unique identifiers
    return refreshEntity(args);
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findOutdated( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelHeader::TABLE_NAME, "outdatedChannels", NULL) );
    // conversion
    list< shared_ptr<ChannelHeader> > result;
    for (vector< sqlite::BaseEntity* >::iterator it=values->begin(); it != values->end(); ) {
      result.push_back( shared_ptr<ChannelHeader>(static_cast< ejin::ChannelHeader* >(*it)) );
      it = values->take(it);
    }
    
    return result;
  }
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::findInvalidSessionKeys( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelHeader::TABLE_NAME, "findInvalidSessionKeys", NULL) );
    // conversion
    list< shared_ptr<ChannelHeader> > result;
    for (vector< sqlite::BaseEntity* >::iterator it=values->begin(); it != values->end(); ) {
      result.push_back( shared_ptr<ChannelHeader>(static_cast< ejin::ChannelHeader* >(*it)) );
      it = values->take(it);
    }
    
    return result;
  }
  
  list< string > ChannelHeaderProfile::findInvitations( ) const
  throw(data_access_error)
  {
    // call entity finder
    Value membership( (integer) kMembershipInvited );
    unique_ptr<ResultSet> values( this->db_.find(ChannelHeader::TABLE_NAME, "findByMembership", &membership, NULL ) );
    
    // extract global unique identifier into a set
    list< string > args;
    ejin::ChannelHeader* mv;
    for (vector< sqlite::BaseEntity* >::iterator it=values->begin(); it != values->end(); it++) {
      mv = static_cast< ejin::ChannelHeader* >(*it);
      args.push_back(mv->getGid());
    }
    return args;
  }
 
  list< string > ChannelHeaderProfile::findChannelToAccept( ) const
  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelHeader::TABLE_NAME, "channelToAccept", NULL ) );
    
    // extract global unique identifier into a set
    list< string > args;
    ejin::ChannelHeader* mv;
    for (vector< sqlite::BaseEntity* >::iterator it=values->begin(); it != values->end(); it++) {
      mv = static_cast< ejin::ChannelHeader* >(*it);
      args.push_back(mv->getGid());
    }
    return args;
  }
  
  list<integer> ChannelHeaderProfile::searchFullText( const string& pattern )
  throw(data_access_error)
  {
    // call entity finder
    Value regex( pattern );
    Value source( "CHANNEL" );
    unique_ptr<ResultSet> values( this->db_.find(ChannelFullText::TABLE_NAME, "searchEntityFullText", &source, &regex, NULL) );
    list< integer > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      ChannelFullText* channelFullText = static_cast< ChannelFullText* >( *it );
      result.push_back(channelFullText->getEntityId());
    }
    return result;
  }
  
  list< string > ChannelHeaderProfile::findChannelToReject( ) const
  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(ChannelHeader::TABLE_NAME, "channelToReject", NULL ) );
    
    // extract global unique identifier into a set
    list< string > args;
    ejin::ChannelHeader* mv;
    for (vector< sqlite::BaseEntity* >::iterator it=values->begin(); it != values->end(); it++) {
      mv = static_cast< ejin::ChannelHeader* >(*it);
      args.push_back(mv->getGid());
    }
    return args;
  }
  
  bool ChannelHeaderProfile::isMemberOf( const string& username, const string& channelGid ) const
  throw(data_access_error)
  {
    assert( ! username.empty() );
    assert( ! channelGid.empty() );
    
    shared_ptr<ChannelHeader> header( this->loadByGid( channelGid ) );
    if ( header ) {
      if (username == header->getOwner())
        return true;
      header = this->resolve( header, kResourceTypeChannelMedia );
      for (list< shared_ptr<ChannelMember> >::iterator it = header->getMembers().begin(); it != header->getMembers().end(); it++ ) {
        if ( username == (*it)->getUsername() ) {
          return (*it)->getMembership() == kMembershipAccepted;
        }
      }
      return header->getMembership() == kMembershipAccepted;
    }
    return false;
  }
  
  shared_ptr<ChannelHeader> ChannelHeaderProfile::loadByGid( const string& channelGid, ResourceType resourceType ) const
  throw(data_access_error)
  {
    assert( ! channelGid.empty() );
    
    Value hidden(false);
    shared_ptr<ChannelHeader> header(this->loadChannelHeaderInternal( Value(channelGid.c_str()), hidden )); 
    return header ? resolve( header, resourceType ) : shared_ptr<ChannelHeader>();
  }

  shared_ptr<ChannelHeader> ChannelHeaderProfile::loadById( const Value& channelId, ResourceType resourceType ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(ChannelHeader::TABLE_NAME, "findChannelById", &channelId, NULL) );
    
    shared_ptr<ChannelHeader> header;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      header = shared_ptr<ChannelHeader>(static_cast< ChannelHeader* >(*it));
      it = values->take(it);
    }
    return header ? resolve( header, resourceType ) : shared_ptr<ChannelHeader>();
  }

  shared_ptr<ChannelHeader> ChannelHeaderProfile::resolve( shared_ptr<ChannelHeader> header, ResourceType resourceType ) const
  throw(data_access_error)
  {
    assert( header );
    
    Value hidden(false);
    Value channelId(header->getId());
    
    // resolve all members
    ChannelMember *channelMember;
    unique_ptr<ResultSet> channelMembers( db_.find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL) );
    for (vector< BaseEntity* >::iterator it = channelMembers->begin(); it != channelMembers->end(); ) {
      channelMember = static_cast< ChannelMember* >(*it);
      switch (channelMember->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
          header->getMembers().push_back(shared_ptr<ChannelMember>( channelMember ));
          it = channelMembers->take(it);
          break;
        case kSyncIndDelete:
          it++;
          break;
      }                  
    }
    // sort resource list
    header->getMembers().sort( compare_member );
    
    // select entity finder
    unique_ptr<ResultSet> channelResources;
    switch (resourceType) {
      case kNoResource:
        break;
      case kResourceTypeMedia:
        channelResources = db_.find(Media::TABLE_NAME, "findChannelHeaderMedias", &channelId, NULL);
        break;
      case kResourceTypeChannelMedia:
        channelResources = db_.find(ChannelMedia::TABLE_NAME, "findChannelHeaderResources", &channelId, &hidden, NULL);
        break;
    }

    // resolve all resources
    if (channelResources.get() != NULL) {
      Resource *channelResource;
      for (vector< BaseEntity* >::iterator it = channelResources->begin(); it != channelResources->end(); ) {
        channelResource = static_cast< Resource* >(*it);
        switch (channelResource->getSyncInd()) {
          case kSyncIndSynchronous:
          case kSyncIndInsert:
          case kSyncIndUpdate:
          case kSyncIndConflict:
            header->getResources().push_back(shared_ptr<Resource>( channelResource ));
            it = channelResources->take(it);
            break;
          case kSyncIndDelete:
            it++;
            break;
        }
      }
      // sort resource list
      header->getResources().sort( compare_resource );
    }
    
    return header;
  }
  
  EntityModification ChannelHeaderProfile::getEntityModification( integer channelId ) const throw(data_access_error)
  {
    const Value _channelId(channelId);
    unique_ptr<Value> result( db_.valueOf(ChannelHeader::TABLE_NAME, "entityModificationMask", &_channelId, NULL) );
    integer mask = result->num();
    result = db_.valueOf(ChannelHeader::TABLE_NAME, "isOutdated", &_channelId, NULL);
    bool isOutdated = result->bol();
    result = db_.valueOf(ChannelHeader::TABLE_NAME, "entityModificationTime", &_channelId, NULL);
    jtime modifyTime( result->date() );
    const Value _modifyTime( result->num() );
    
    string modifiedBy;
    result = db_.valueOf(ChannelHeader::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
    modifiedBy = result->isNull() ? "" : result->str();
    if ( modifiedBy.empty() ) {
      result = db_.valueOf(ChannelMember::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
      modifiedBy = result->isNull() ? "" : result->str();
    }
    if ( modifiedBy.empty() ) {
      result = db_.valueOf(Media::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
      modifiedBy = result->isNull() ? "" : result->str();
    }
    return EntityModification( mask, modifyTime, modifiedBy, isOutdated );
  }

  EntityModification ChannelHeaderProfile::getChannelModification( integer channelId ) const throw(data_access_error)
  {
    const Value _channelId(channelId);
    unique_ptr<Value> result( db_.valueOf(ChannelHeader::TABLE_NAME, "channelModificationMask", &_channelId, NULL) );
    integer mask = result->num();
    result = db_.valueOf(ChannelHeader::TABLE_NAME, "channelIsOutdated", &_channelId, NULL);
    bool isOutdated = result->bol();
    result = db_.valueOf(ChannelHeader::TABLE_NAME, "channelModificationTime", &_channelId, NULL);
#if defined(__i386__) || defined(__ppc__) || defined(__arm__)
    jtime modifyTime( result->num() );
    const Value _modifyTime( result->num() );
#else
#if defined(__x86_64__) || defined(__ppc64__) || defined(__arm64__)
    jtime modifyTime( result->num() * 1000 ); // convert from seconds to millis from epoch
    const Value _modifyTime( result->num() * 1000 );
#else
#error UNKNOWN ARCHITECTURE
#endif
#endif
    
    string modifiedBy;
    result = db_.valueOf(ChannelHeader::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
    modifiedBy = result->isNull() ? "" : result->str();
    if ( modifiedBy.empty() ) {
      result = db_.valueOf(ChannelPost::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
      modifiedBy = result->isNull() ? "" : result->str();
    }
    if ( modifiedBy.empty() ) {
      result = db_.valueOf(ChannelComment::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
      modifiedBy = result->isNull() ? "" : result->str();
    }
    if ( modifiedBy.empty() ) {
      result = db_.valueOf(ChannelMember::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
      modifiedBy = result->isNull() ? "" : result->str();
    }
    if ( modifiedBy.empty() ) {
      result = db_.valueOf(Media::TABLE_NAME, "lastModifiedBy", &_channelId, &_modifyTime, NULL);
      modifiedBy = result->isNull() ? "" : result->str();
    }
    return EntityModification( mask, modifyTime, modifiedBy, isOutdated );
  }
  
  MemberRole ChannelHeaderProfile::getAccessRole( integer channelId, const char* username ) const throw(data_access_error)
  {
    const Value _channelId(channelId);
    const Value _username(username);
    unique_ptr<Value> result( db_.valueOf(ChannelHeader::TABLE_NAME, "accessRole", &_channelId, &_username, NULL) );
    return (MemberRole)result->num();
  }
  bool ChannelHeaderProfile::resolveSessionKey(integer id,
                                               const string& username,
                                               RSACrypto* crypto,
                                               bool required) const
  throw(security_integrity_error,data_access_error)
  {
    Value channelId( id );
    shared_ptr<ChannelHeader> header = this->loadById( channelId );
    return header ? this->resolveSessionKey(*header, username, crypto, required) : false;
  }

  bool ChannelHeaderProfile::resolveSessionKey(const string& gid,
                                               const string& username,
                                               RSACrypto* crypto,
                                               bool required) const
  throw(security_integrity_error,data_access_error)
  {
    Value channelGid( gid.c_str() );
    Value hidden( false );
    shared_ptr<ChannelHeader> header = this->loadChannelHeaderInternal( channelGid, hidden );
    return header ? this->resolveSessionKey(*header, username, crypto, required) : false;
  }

  bool ChannelHeaderProfile::resolveSessionKey(const ChannelHeader& header,
                                               const string& username,
                                               RSACrypto* crypto,
                                               bool required) const
  throw(security_integrity_error,data_access_error)
  {
    if ( ! header.isSecure() ) { // no encryption
      return false;
    }
    
    if ( crypto == NULL || username.empty() ) {
      if (required) {
        _throw_exception( ejin::SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "Session key is required for encryption" );
      }
      return false;
    }
    
    if ( channelMemberRepository_.isRSAKeyRevoked(username) ) {
      return false;
    }

    try {
      if ( header.hasSessionKey() && crypto->hasPrivateKeyASN1() ) {
        crypto->setEncryptedSessionKey( header.getSessionKey() );
        return true;
      } else if ( channelMemberRepository_.resolveSessionKey( header.getId(), username, crypto ) ) {
        return true;
      }
    } catch (runtime_error e) {
      if ( header.hasSessionKey2() && crypto->hasPrivateKeyASN1() ) {
        crypto->setEncryptedSessionKey( header.getSessionKey2() );
        return true;
      }
    }
    
    
    if (required) {
      _throw_exception( ejin::SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "Session key is required for encryption" );
    }
    return false;
  }
  string ChannelHeaderProfile::exportSessionKey( integer id, const string& username  ) const throw(data_access_error)
  {
    Value channelId( id );
    shared_ptr<ChannelHeader> header = this->loadById( channelId );
    if (header && header->hasSessionKey() ) {
      return header->getSessionKey();
    }
    return this->channelMemberRepository_.exportSessionKey(id, username);
  }

  // ==========
  // Protected Interface
  // ==========
  
  list< shared_ptr<ChannelHeader> > ChannelHeaderProfile::refreshEntity( const list<integer>& list ) const
  throw(data_access_error)
  {
    std::list< shared_ptr<ChannelHeader> > result;
    // convert set of unique identifiers to channel header entities and add to the result list
    shared_ptr<ChannelHeader> header;
    for (std::list<integer>::const_iterator it=list.begin(); it != list.end(); it++) {
      header = shared_ptr<ChannelHeader>(new ChannelHeader());
      header->setId(*it);
      // refresh from database
      db_.refresh(*header);
      // add to result list
      result.push_back(header);
    }
    
    return result;
  }
  
  shared_ptr<ChannelHeader> ChannelHeaderProfile::loadChannelHeaderInternal( const Value& channelGid, const Value& hidden ) const throw(data_access_error) 
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(ChannelHeader::TABLE_NAME, "findChannelByGid", &channelGid, &hidden, NULL) );
    
    shared_ptr<ChannelHeader> header;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      header = shared_ptr<ChannelHeader>(static_cast< ChannelHeader* >(*it));
      it = values->take(it);
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate channel header [gid=%d].",  channelGid.num());  
    
    return header;
  }  
  
  bool compare_resource (shared_ptr<Resource> first, shared_ptr<Resource> second)
  {
    return first->getNo() < second->getNo();
  }
  bool compare_member (shared_ptr<ChannelMember> first, shared_ptr<ChannelMember> second)
  {
    return first->getUsername() < second->getUsername();
  }
  
}
