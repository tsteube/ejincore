/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelHeader.h"
#include "UsingEjinTypes.h"

#include "Media.h"
#include "ChannelMedia.h"
#include "ChannelMember.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::ISerializableEntity;
  using ser::tag;
  using ser::attr;
  using ser::chardata;
  using ser::endtag;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<ChannelHeader>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("GID", type_text, flag_not_null),
    Attribute("OWNER", type_text, flag_not_null),
    Attribute("OWNER_FULLNAME", type_text, flag_none),
    Attribute("MODIFIED_BY", type_text, flag_none),
    Attribute("MODIFIED_BY_FULLNAME", type_text, flag_none),
    Attribute("NAME", type_text, flag_none),
    Attribute("STATE", type_int, flag_none, Value((integer)0)),
    Attribute("TAGS", type_text, flag_none),
    Attribute("SESSION_KEY", type_text, flag_none),
    Attribute("SESSION_KEY_2", type_text, flag_none),
    Attribute("INVALID_SESSION_KEY", type_bool, flag_not_null, Value(false)),
    Attribute("ROLE", type_int, flag_not_null, Value((integer)0), RoleEnumMapping),
    Attribute("MEMBERSHIP", type_int, flag_not_null, Value((integer)1), MembershipEnumMapping),
    Attribute("CONTENT", type_text, flag_none),
    Attribute("SYNC_TIME", type_time, flag_none),
    Attribute("LAST_SYNC_TIME", type_time, flag_none),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_ANCHOR", type_text, flag_not_null),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("MEMBERSHIP_IND", type_int, flag_not_null, Value((integer)0)),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("OUTDATED", type_bool, flag_not_null|flag_transient, Value(false)),
    Attribute("CONTENT_MODIFY_TIME", type_time, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute("SECURE", type_bool, flag_not_null, Value(false)),
    Attribute("ENCRYPTED", type_bool, flag_not_null, Value(false)),
    Attribute("IV", type_text, flag_none),
    Attribute()
  };
  template <> const AttributeSet DataSchema<ChannelHeader>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelHeader::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findChannelById"] = "ID = ?";
      tMap["findChannelByGid"] = "GID = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["findInvalidSessionKeys"] = "INVALID_SESSION_KEY = 1 AND GID IS NOT NULL AND MASTER_ID IS NULL";
      tMap["outdatedChannels"] = "MEMBERSHIP = 1 AND OUTDATED = 1 AND MASTER_ID IS NULL";
      tMap["findAcceptedChannelsIn"] = "MEMBERSHIP = 1 AND GID in ( ? )";
      tMap["findAllOrderBySyncAnchorDesc"] = "MASTER_ID IS NULL ORDER BY sync_anchor DESC";
      tMap["findByMembership"] = "MASTER_ID IS NULL AND GID IS NOT NULL AND MEMBERSHIP = ?";
      tMap["channelToAccept"] = "MEMBERSHIP != 1 AND MEMBERSHIP_IND = 1";
      tMap["channelToReject"] = "MEMBERSHIP != 2 AND MEMBERSHIP_IND = 2";
      tMap["BackupSet"] = "ID = ?1 AND GID IS NOT NULL AND MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findChannelByGidBak"] = "GID = ?";
      tMap["BackupSetBak"] = "ID = ?1 AND GID IS NOT NULL AND MASTER_ID IS NULL";
    }
    return tMap;
  }
  const map<string,string> ChannelHeader::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      
      std::ostringstream ss;
      ss << "SELECT "
      "BITWISE_OR( channel_modification_mask,"
      "member_modification_mask,"
      "media_modification_mask"
      ") FROM " << schema << ".header_modification_view "
      "WHERE channel_id = ?";
      tMap["entityModificationMask"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT "
      "MAX_CELL("
      "channel_modify_time,"
      "member_modify_time,"
      "media_modify_time"
      ") FROM " << schema << ".header_modification_view "
      "WHERE channel_id = ?";
      tMap["entityModificationTime"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT "
      "BITWISE_OR("
      "channel_modification_mask,"
      "member_modification_mask,"
      "post_modification_mask,"
      "comment_modification_mask,"
      "media_modification_mask"
      ") FROM " << schema << ".channel_modification_view "
      "WHERE channel_id = ?";
      tMap["channelModificationMask"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT "
      "MAX(MAX(c.outdated, m.attach_outdated)) "
      "FROM " << schema << ".channel_tbl c JOIN " << schema << ".media_tbl m ON c.id = m.channel_id "
      "WHERE c.id = ? AND m.transfer_key IS NOT NULL";
      tMap["channelIsOutdated"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT "
      "c.outdated "
      "FROM " << schema << ".channel_tbl c "
      "WHERE c.id = ?";
      tMap["isOutdated"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT "
      "MAX_CELL("
      "channel_modify_time,"
      "member_modify_time,"
      "post_modify_time,"
      "comment_modify_time,"
      "media_modify_time"
      ") FROM " << schema << ".channel_modification_view "
      "WHERE channel_id = ?";
      tMap["channelModificationTime"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT COALESCE(modified_by, owner) "
      "FROM " << schema << ".channel_tbl "
      "WHERE id = ? AND modify_time = ? AND master_id IS NULL";
      tMap["lastModifiedBy"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT "
      "COALESCE( "
      "(CASE WHEN c.owner == ?2 THEN 3 ELSE NULL END), "   // 1. channel owner -> ADMIN
      "(CASE WHEN c.role == 0 THEN 0 ELSE NULL END), "     // 2. channel reader role only -> READER
      "c.role "                                            // 3. entity role
      ") "
      "FROM " << schema << ".channel_tbl c where id = ?1";
      tMap["accessRole"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT "
      "count(c.id) FROM " << schema << ".channel_tbl c LEFT OUTER JOIN " << schema << ".channel_member_tbl cm "
      "ON ( c.id = cm.channel_id and cm.username = ?1 ) WHERE c.outdated = 0 AND c.sync_ind != 1 AND c.master_id IS NULL AND cm.username IS NULL;";
      tMap["missingMembership"] = ss.str();
 
      ss.str("");ss.clear();
      ss << "UPDATE " << schema << ".channel_tbl SET sync_ind = 0, modification_mask = 0 "
      "WHERE id = ?1 AND sync_ind = 3 AND master_id IS NULL";
      tMap["clearDeleteSyncInd"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  const string ChannelHeader::restoreSet( void ) const {
    return "ID = ?1";
  }

  // ==========
  // Crypto Interface
  // ==========
  
  bool ChannelHeader::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if (this->isSecure()) {
      if ( crypto && crypto->hasSessionKey( ) ) {
        if (this->isEncrypted()) {
          if (this->hasName()) {
            this->setName( crypto->decrypt( this->getName() ) );
          }
          if (this->hasContent()) {
            this->setContent( crypto->decrypt( this->getContent() ) );
          }
          for (list< shared_ptr<ChannelMember> >::const_iterator it=this->members_.begin(); it != this->members_.end(); it++) {
            (*it)->decrypt( crypto );
          }
          this->setEncrypted( false );
          this->contentSize_ = 0;
          COPY_PROPERTY((*this), (*crypto), IV);
        }
        this->setInvalidSessionKey( false );
        return true;
      } else {
        this->setInvalidSessionKey( true );
        this->clearIV( );
      }
    } else {
      this->clearIV( );
    }
    return false;
  }
  
  bool ChannelHeader::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if ( this->isSecure() ) {
      if ( crypto && crypto->hasSessionKey( ) ) {
        switch( this->getSyncInd() ) {
          case kSyncIndSynchronous:
            COPY_PROPERTY((*crypto), (*this), IV); // use old IV
            break;
          case kSyncIndInsert:
          case kSyncIndUpdate:
            crypto->createIV( ); // generate new IV
            break;
          case kSyncIndDelete:
          case kSyncIndConflict:
            assert(false);
            break;
        }
        if (this->hasName()) {
          this->setName( crypto->encrypt( this->getName() ) );
        }
        if (this->hasContent()) {
          this->contentSize_ = this->value("CONTENT")->size();
          this->setContent( crypto->encrypt( this->getContent() ) );
        }
        for (list< shared_ptr<ChannelMember> >::const_iterator it=this->members_.begin(); it != this->members_.end(); it++) {
          (*it)->encrypt( crypto );
        }
        this->setEncrypted( true );
        this->setInvalidSessionKey( false );
        COPY_PROPERTY((*this), (*crypto), IV);
        return true;
      } else {
        _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "no session key to encrypt message");
      }
    } else {
      this->contentSize_ = 0;
      this->setEncrypted( false );
      this->clearIV( );
    }
    return false;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelHeader::ChannelHeader( void ):
  BaseEntity(TABLE_DDL, false), members_(), resources_()
  {
    this->setRole( kSyncRoleRead );
    this->setAccessRole( kSyncRoleRead );
    this->setMembership( kMembershipAccepted );
    this->setSyncInd( kSyncIndInsert );
    this->setMembershipInd( kMembershipIndInSync );
    this->clearMasterId( );
    this->setOutdated( false );
    this->setSecure( false );
    this->setEncrypted( false );
    this->setInvalidSessionKey( false );
    this->setState( (integer) 0 );
    this->setModificationMask((integer) 0);
  }
  ChannelHeader::ChannelHeader( const integer id ): ChannelHeader()
  {
    setId(id);
  }
  ChannelHeader::ChannelHeader( const string& gid ): ChannelHeader()
  {
    setGid(gid);
  }
  ChannelHeader::ChannelHeader( const ChannelHeader& record ): ChannelHeader()
  {
    this->operator=(record);
  }
  ChannelHeader::~ChannelHeader( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  ChannelHeader& ChannelHeader::operator=( const ChannelHeader& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Gid);
      
      COPY_PROPERTY((*this), rhs, SyncAnchor);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, MasterId);
      COPY_BOOL_PROPERTY((*this), rhs, Outdated);
      COPY_BOOL_PROPERTY((*this), rhs, Secure);
      COPY_BOOL_PROPERTY((*this), rhs, Encrypted);
      COPY_PROPERTY((*this), rhs, IV);
      
      COPY_PROPERTY((*this), rhs, Owner);
      COPY_PROPERTY((*this), rhs, OwnerFullname);
      COPY_PROPERTY((*this), rhs, ModifiedBy);
      COPY_PROPERTY((*this), rhs, ModifiedByFullname);
      COPY_PROPERTY((*this), rhs, Name);
      COPY_PROPERTY((*this), rhs, Content);
      COPY_PROPERTY((*this), rhs, State);
      COPY_PROPERTY((*this), rhs, Tags);
      COPY_PROPERTY((*this), rhs, SessionKey);
      COPY_PROPERTY((*this), rhs, SessionKey2);
      COPY_BOOL_PROPERTY((*this), rhs, InvalidSessionKey);
      COPY_PROPERTY((*this), rhs, Role);
      COPY_PROPERTY((*this), rhs, Membership);
      
      COPY_PROPERTY((*this), rhs, SyncTime);
      COPY_PROPERTY((*this), rhs, LastSyncTime);
      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      COPY_PROPERTY((*this), rhs, ContentModifyTime);
      
      this->accessRole_ = rhs.accessRole_;
      COPY_PROPERTY((*this), rhs, ModificationMask);
      
      CLONE_LIST_PROPERTY(ChannelMember, (*this), rhs, members_);
      
      // use copy constructor according to original type
      for(auto it = rhs.resources_.begin();
          it != rhs.resources_.end(); it++) {
        ChannelMedia* channelMedia = dynamic_cast<ChannelMedia*>((*it).get());
        if (channelMedia) {
          this->resources_.push_back(shared_ptr< Resource >(new ChannelMedia(*channelMedia)));
        }
        Media* media = dynamic_cast<Media*>((*it).get());
        if (media) {
          this->resources_.push_back(shared_ptr< Resource >(new Media(*media)));
        }
      }
    }
    return *this;
  }
  
  // comparison
  bool ChannelHeader::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChannelHeader& mrhs = dynamic_cast<const ChannelHeader&> (rhs);
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() == mrhs.getGid();
    if (this->hasId() && mrhs.hasId())
      return this->getId() == mrhs.getId();
    return false;
  }
  bool ChannelHeader::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChannelHeader& mrhs = dynamic_cast<const ChannelHeader&> (rhs); // throws std::bad_cast if not of type ChannelHeader&
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() < mrhs.getGid();
    if (this->hasId() && mrhs.hasId())
      return this->getId() < mrhs.getId();
    return false;
  }
  // deub
  string ChannelHeader::toString( void ) const
  {
    stringstream buf;
    buf << BaseEntity::toString();
    buf << "{" <<
    "#members=" << this->members_.size() <<
    ",#resources=" << this->resources_.size() <<
    "}";
    return buf.str();
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  const char* ChannelHeader::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelHeader::tableFields( void ) const { return TABLE_DDL; }
  
  const string& ChannelHeader::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& ChannelHeader::xmlFields( void ) const { return XML_DDL; }
  
  bool ChannelHeader::copyMetaDataFrom( const BaseEntity& rhs )
  {
    const ChannelHeader& mrhs = dynamic_cast<const ChannelHeader&> (rhs);
    COPY_PROPERTY((*this), mrhs, Role);
    COPY_PROPERTY((*this), mrhs, Membership);
    if (mrhs.hasSessionKey()) {
      COPY_PROPERTY((*this), mrhs, SessionKey);
    }
    if (mrhs.hasSyncTime()) {
      COPY_PROPERTY((*this), mrhs, SyncTime);
    }
    if (mrhs.hasLastSyncTime()) {
      COPY_PROPERTY((*this), mrhs, LastSyncTime);
    }
    COPY_PROPERTY((*this), mrhs, ModifyTime);
    COPY_PROPERTY((*this), mrhs, OwnerFullname);
    COPY_PROPERTY((*this), mrhs, ModifiedBy);
    COPY_PROPERTY((*this), mrhs, ModifiedByFullname);
    COPY_BOOL_PROPERTY((*this), mrhs, Secure);
    COPY_BOOL_PROPERTY((*this), mrhs, InvalidSessionKey);
    if (mrhs.hasContent()) {
      COPY_BOOL_PROPERTY((*this), mrhs, Encrypted);
      this->setContentModifyTime(mrhs.getModifyTime());
    }
    
    return true;
  }
  bool ChannelHeader::copyDataFrom( const BaseEntity& rhs )
  {
    if ( ! this->isDataEqualTo(rhs)) {
      const ChannelHeader& mrhs = dynamic_cast<const ChannelHeader&> (rhs);
      if (mrhs.hasContent()) {
        if ( mrhs.isEncrypted() ) {
          // do not override old content with encrypted content
          this->setEncrypted( false );
        } else {
          COPY_PROPERTY((*this), mrhs, Name);
          COPY_PROPERTY((*this), mrhs, Content);
        }
        COPY_PROPERTY((*this), mrhs, State);
        COPY_PROPERTY((*this), mrhs, Tags);
        COPY_PROPERTY((*this), mrhs, IV);
        return true;
      }
    }
    return false;
  }
  bool ChannelHeader::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    ChannelHeader& rhs( (ChannelHeader&)arg );
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( rhs.isEncrypted() )
      return false;
    if ( this->getGid() != rhs.getGid() )
      return false;
    if ( this->getName() != rhs.getName() )
      return false;
    if ( this->getContent() != rhs.getContent() )
      return false;
    if ( this->getTags() != rhs.getTags() )
      return false;
    if ( this->getRole() != rhs.getRole() )
      return false;
    if ( this->getState() != rhs.getState() )
      return false;
    return true;
  }
  bool ChannelHeader::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void ChannelHeader::archive( void )
  {
    this->clearId( );
    this->setSyncInd( kSyncIndSynchronous );
    this->setOutdated( false );
    this->setModificationMask( kNoneModification );
  }
  void ChannelHeader::unarchive( void )
  {
    this->clearId( );
    this->clearGid( ); // new channel
    this->clearOwnerFullname( );
    this->clearModifiedBy( );
    this->clearModifiedByFullname( );
    this->clearSessionKey( );
    this->clearSessionKey2( );
    this->setInvalidSessionKey( false );
    this->setRole( kSyncRoleAdmin );
    this->setMembership( kMembershipAccepted );
    this->clearSyncTime( );
    this->clearLastSyncTime( );
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->clearContentModifyTime( );
    this->clearSyncAnchor( );
    this->setMembershipInd( kMembershipIndInSync );
    this->setSyncInd( kSyncIndInsert );
    this->setOutdated( false );
    this->setModificationMask( kChannelHeaderPendingLocalModification );
    this->clearMasterId( );
    this->setSecure( true );
    this->setEncrypted( false );
    this->clearIV( );
  }
  void ChannelHeader::clearData( void )
  {
    this->clearContent( );
    this->setState( (integer) 0 );
    this->clearTags( );
    this->clearSessionKey( );
    this->clearSessionKey2( );
    this->setInvalidSessionKey( false );
    this->setRole( kSyncRoleRead );
    this->setMembership( kMembershipAccepted );
    this->clearSyncTime( );
    this->clearLastSyncTime( );
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->clearContentModifyTime( );
    this->setSecure( false );
    this->setEncrypted( false );
    this->clearIV( );
    this->resources_.clear();
    this->members_.clear();
  }
  bool ChannelHeader::isEmpty( void ) const
  {
    return ! hasContent() && this->resources_.empty() && this->members_.empty();
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON Mapping
  
  bool ChannelHeader::marshalJson( nlohmann::json& j ) const
  {    
    if ( this->hasGid() ) {
      j["id"] = string(this->value("GID")->str(), this->value("GID")->size());
    }
    if ( this->getState() > 0 ) {
      j["state"] = this->value("STATE")->num();
    }
    if (this->isEncrypted()) {
      j["encrypted"] = true;
    }
    if ( this->hasName() ) {
      j["name"] = string( this->value("NAME")->str(), this->value("NAME")->size() );
    }
    if ( this->hasContent() ) {
      j["content"] = string( this->value("CONTENT")->str(), this->value("CONTENT")->size() );
    }
    if ( this->hasTags() ) {
      j["tags"] = string( this->value("TAGS")->str(), this->value("TAGS")->size() );
    }

    // print out resources if any
    if ( ! this->resources_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<Resource> >::const_iterator it=this->resources_.begin();
           it != this->resources_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      j["resource"] = a;
    }

    // print out comments if any
    if ( ! this->members_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<ChannelMember> >::const_iterator it=this->members_.begin();
           it != this->members_.end(); it++) {
        json j2;
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        } else {
          a.push_back({});
        }
      }
      j["member"] = a;
    }
    
    return true;
  }
  /*
  bool ChannelHeader::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if ( this->hasGid() ) {
      json_object_set_new( node, "id", json_stringn_nocheck( this->value("GID")->str(), this->value("GID")->size() ) );
    }
    if ( this->getState() > 0 ) {
      json_object_set_new( node, "state", json_integer( this->value("STATE")->num() ) );
    }
    if (this->isEncrypted()) {
      json_object_set_new( node, "encrypted", json_true() );
    }
    if ( this->hasName() ) {
      json_object_set_new( node, "name", json_stringn_nocheck( this->value("NAME")->str(), this->value("NAME")->size() ) );
    }
    if ( this->hasContent() ) {
      json_object_set_new( node, "content", json_stringn_nocheck( this->value("CONTENT")->str(), this->value("CONTENT")->size() ) );
    }
    if ( this->hasTags() ) {
      json_object_set_new( node, "tags", json_stringn_nocheck( this->value("TAGS")->str(), this->value("TAGS")->size() ) );
    }
    
    // print out resources if any
    if ( ! this->resources_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( node, "resource", array );
      for (list< shared_ptr<Resource> >::const_iterator it=this->resources_.begin();
           it != this->resources_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }
    
    // print out comments if any
    if ( ! this->members_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( node, "member", array );
      for (list< shared_ptr<ChannelMember> >::const_iterator it=this->members_.begin();
           it != this->members_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }
    
    return true;
  }
  */
  bool ChannelHeader::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("id")) {
        this->value("GID")->str( j["id"] );
      }
      if ( j.contains("anchor")) {
        this->value("SYNC_ANCHOR")->str( j["anchor"] );
      }
      if ( j.contains("owner")) {
        this->value("OWNER")->str( j["owner"] );
      }
      if ( j.contains("owner_fullname")) {
        this->value("OWNER_FULLNAME")->str( j["owner_fullname"] );
      }
      if ( j.contains("modified_by")) {
        this->value("MODIFIED_BY")->str( j["modified_by"] );
      }
      if ( j.contains("modified_by_fullname")) {
        this->value("MODIFIED_BY_FULLNAME")->str( j["modified_by_fullname"] );
      }
      if ( j.contains("name")) {
        this->value("NAME")->str( j["name"] );
      }
      if ( j.contains("content")) {
        this->value("CONTENT")->str( j["content"] );
      }
      if ( j.contains("state")) {
        this->value("STATE")->num( j["state"] );
      }
      if ( j.contains("tags")) {
        this->value("TAGS")->str( j["tags"] );
      }
      if ( j.contains("key")) {
        this->value("SESSION_KEY")->str( j["key"] );
      }
      if ( j.contains("role")) {
        this->value("ROLE")->str( j["role"] );
      }
      if ( j.contains("membership")) {
        this->value("MEMBERSHIP")->str( j["membership"] );
      }
      if ( j.contains("encrypted")) {
        this->value("ENCRYPTED")->bol( j["encrypted"] );
        this->setSecure( this->isEncrypted() );
      }
      if ( j.contains("create_time")) {
        this->value("CREATE_TIME")->str( j["create_time"] );
      }
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }

      // unmarshal members
      if ( j.contains("member")) {
        json j2 = j["member"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<ChannelMember> entity = Utilities::findOrAppend( this->members_, i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      // unmarshal resources
      if ( j.contains("resource")) {
        json j2 = j["resource"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<Resource> entity;
            if (this->resources_.size() > i) {
              typename std::list< std::shared_ptr< Resource > >::iterator refIt = this->resources_.begin();
              std::advance(refIt, i);
              entity = *refIt;
            } else {
              entity = shared_ptr<Resource>(new ChannelMedia());
              this->resources_.push_back(entity);
            }
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      return true;
    }
    
    return false;
  }
  /*
  bool ChannelHeader::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "id");
    if ( json_is_string(value) ) {
      this->value("GID")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "anchor");
    if ( json_is_string(value) ) {
      this->value("SYNC_ANCHOR")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner");
    if ( json_is_string(value) ) {
      this->value("OWNER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner_fullname");
    if ( json_is_string(value) ) {
      this->value("OWNER_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by_fullname");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "name");
    if ( json_is_string(value) ) {
      this->value("NAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "content");
    if ( json_is_string(value) ) {
      this->value("CONTENT")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "state");
    if ( json_is_number(value) ) {
      this->value("STATE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "tags");
    if ( json_is_string(value) ) {
      this->value("TAGS")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "key");
    if ( json_is_string(value) ) {
      this->value("SESSION_KEY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "role");
    if ( json_is_string(value) ) {
      this->value("ROLE")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "membership");
    if ( json_is_string(value) ) {
      this->value("MEMBERSHIP")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "encrypted");
    if ( json_is_string(value) ) {
      this->value("ENCRYPTED")->bytea( json_string_value(value), json_string_length(value) );
      this->setSecure( this->isEncrypted() );
    }
    value = json_object_get(node, "create_time");
    if ( json_is_string(value) ) {
      this->value("CREATE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    
    // unmarshal members
    value = json_object_get(node, "member");
    if (json_is_array(value)) {
      size_t index;
      json_t* child;
      json_array_foreach(value, index, child) {
        shared_ptr<ChannelMember> entity = Utilities::findOrAppend( this->members_, index );
        if (! entity || ! entity->unmarshalJson( child )) {
          return false;
        }
      }
    }
    
    // unmarshal resources
    value = json_object_get(node, "resource");
    if (json_is_array(value)) {
      size_t index;
      json_t* child;
      json_array_foreach(value, index, child) {
        shared_ptr<Resource> entity;
        if (this->resources_.size() > index) {
          typename std::list< std::shared_ptr< Resource > >::iterator refIt = this->resources_.begin();
          std::advance(refIt, index);
          entity = *refIt;
        } else {
          entity = shared_ptr<Resource>(new ChannelMedia());
          this->resources_.push_back(entity);
        }
        if (! entity || ! entity->unmarshalJson( child )) {
          return false;
        }
      }
    }
    
    return true;
  }
  */

  // XML Mapping
  
  const string ChannelHeader::XML_NAME   = "header";
  const ser::XmlAttribute ChannelHeader::XML_FIELDS[] =
  {
    ser::XmlAttribute("id", "GID" ),
    ser::XmlAttribute("anchor", "SYNC_ANCHOR" ),
    ser::XmlAttribute("owner", "OWNER" ),
    ser::XmlAttribute("owner_fullname", "OWNER_FULLNAME" ),
    ser::XmlAttribute("modified_by", "MODIFIED_BY" ),
    ser::XmlAttribute("modified_by_fullname", "MODIFIED_BY_FULLNAME" ),
    ser::XmlAttribute("name", "NAME" ),
    ser::XmlAttribute("content", "CONTENT" ),
    ser::XmlAttribute("state", "STATE" ),
    ser::XmlAttribute("tags", "TAGS" ),
    ser::XmlAttribute("key", "SESSION_KEY" ),
    ser::XmlAttribute("role", "ROLE" ),
    ser::XmlAttribute("membership", "MEMBERSHIP" ),
    ser::XmlAttribute("encrypted", "ENCRYPTED" ),
    //ser::XmlAttribute("key", "PKEY" ),
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute("create_time", "CREATE_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet ChannelHeader::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  void ChannelHeader::endXmlNode( const char* name )
  {
    if ( strcmp("header", name) == 0 || strcmp("channel", name) == 0 ) {
      this->setSecure( this->isEncrypted() );
    }
  }
  ISerializableEntity* ChannelHeader::entityByXmlNode( const char* name, int position )
  {
    ISerializableEntity* result;
    if ( strcmp("member", name) == 0 ) {
      shared_ptr<ChannelMember> member;
      if ((int) this->getMembers().size() > position) {
        // return existing node
        list< shared_ptr<ChannelMember> >::iterator i = this->getMembers().begin();
        std::advance(i, position);
        member = *i;
      } else {
        // create new node
        member = shared_ptr<ChannelMember>(new ChannelMember());
        this->getMembers().push_back(member);
      }
      return member.get();
      
    } else if ( strcmp("resource", name) == 0 ) {
      shared_ptr<Resource> media;
      if ((int) this->getResources().size() > position) {
        // return existing node
        list< shared_ptr<Resource> >::iterator i = this->getResources().begin();
        std::advance(i, position);
        media = *i;
      } else {
        // create new node
        media = shared_ptr<ChannelMedia>(new ChannelMedia());
        this->getResources().push_back(media);
      }
      return media.get();
      
    } else {
      result = SerializableEntity::entityByXmlNode(name, position);
      
    }
    return result;
  }
  
  bool ChannelHeader::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("header"); // start element tag
    
    // serialize properties
    if ( this->hasGid() ) {
      xml << attr("id") << this->getGid();
    }
    if ( this->hasState() ) {
      xml << attr("state") << this->getState();
    }
    if ( this->isEncrypted() ) {
      xml << attr("encrypted") << "true";
    }
    if ( this->hasContent() ) {
      xml << tag("content") << chardata() << this->getContent() << endtag();
    }
    if ( this->hasName() ) {
      xml << tag("name")  << chardata() << this->getName() << endtag();
    }
    if ( this->hasTags() ) {
      xml << tag("tags") << chardata() << this->getTags() << endtag();
    }
    
    // print out resources if any
    for (list< shared_ptr<Resource> >::const_iterator it=this->resources_.begin(); it != this->resources_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    // print out resources if any
    for (list< shared_ptr<ChannelMember> >::const_iterator it=this->members_.begin(); it != this->members_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    xml << ser::endtag(); // end element tag
    return true;
  }
  
}
