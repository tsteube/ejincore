/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_HEADER_REPOSITORY_H__
#define __EJIN_CHANNEL_HEADER_REPOSITORY_H__

#include "ChannelHeaderProfile.h"

namespace ejin
{

  /**
   * Update operations on the channelHeader entity. There are a create, update and delete operation to manages channelHeaders bound to a
   * channel instance.
   */
  class ChannelHeaderRepository: public ChannelHeaderProfile {

    // ==========
    // Public Interface
    // ==========
  public:

    // ctor
    ChannelHeaderRepository( Database& db, ChangeLogRepository& changeLogRepository, ChannelMemberRepository& channelMemberRepository, ChannelPostRepository& channelPostRepository, ChannelCommentRepository& channelCommentRepository, ChannelMediaRepository& channelMediaRepository ):
    ChannelHeaderProfile(db,channelMemberRepository), channelPostRepository_(channelPostRepository), channelCommentRepository_(channelCommentRepository), channelMediaRepository_(channelMediaRepository), changeLogRepository_(changeLogRepository) {}
    // dtor
    ~ChannelHeaderRepository( void ) {};

    /**
     * Returns true if the specified channel instance has been synchronized with the server reference entity although
     * the entity contains unsynchronized local changes.
     */
    bool detectChannelConflict( const string& channelGid ) const
    throw(data_access_error);

    /**
     * Revert local changes to synchronize the channel entity with the server reference.
     */
    bool clearConflict( const string& channelGid )
    throw(data_access_error);

    /**
     * Update the specified channel channel instances in the repository. This method will compare all current channel
     * channels of the specified channel identifier and updates its content by the given reference data.
     */
    shared_ptr<ChannelHeader> update( ChannelHeader& channelHeader, bool full, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */
    void commit( ChannelHeader& channelHeader, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Update the specified channel header instance in the repository. The channel will be marked as outdated because of
     * the missing posts and comments. These information must be sync explicitly.
     */
    shared_ptr<ChannelHeader> updateHeaderOnly( ChannelHeader& channelHeader, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Remove the specified channel header instance in the repository.
     */
    bool deleteChannel( ChannelHeader& channelHeader, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Restore original channel header instance
     * and mark local changes as conflicted data set.
     */
    bool revert( const string& gid, bool force  = false )
    throw(data_access_error);
    
    bool revert( const string& gid, const string& member, bool force )
    throw(data_access_error);

    list<string> detectLostKey( ChannelHeader& header )
    throw(security_integrity_error,data_access_error);
    
    /**
     * Set encrypted channel session key only for new channel member.
     */
    bool createAESKey( ChannelHeader& header )
    throw(security_integrity_error,data_access_error);

    /**
     * Set encrypted channel session key only for new channel member.
     */
    bool publishAESKey( ChannelHeader& header, const string& aesKey, bool force )
    throw(security_integrity_error,data_access_error);
    
    /**
     * Override session key.
     */
    bool importSessionKey( integer channelId, const string& username, const string& aesKey )
    throw(data_access_error);

    // ==========
    // Private Interface
    // ==========
  private:

    ChannelPostRepository&    channelPostRepository_;
    ChannelCommentRepository&    channelCommentRepository_;
    ChannelMediaRepository&   channelMediaRepository_;
    ChangeLogRepository& changeLogRepository_;

    /*
     * Add a new channel channel entity to the persistent store
     */
    shared_ptr<ChannelHeader> addChannelHeader( ChannelHeader& channelHeader, bool full, const jtime& syncTime )
    throw(data_access_error);

    /*
     * Updates a channel channel entity in the persistent store
     */
    bool updateChannelHeader( ChannelHeader& channelHeaderEntity, ChannelHeader& channelHeader, KeySequence& nextSyncAnchor, bool full, const jtime& syncTime, sqlite::ResultSet* allChannelHeaderMediaEntities, sqlite::ResultSet* allChannelMemberEntities )
    throw(data_access_error);

    /*
     * Removes a channel channel entity from the persistent store
     */
    bool removeChannelHeader( ChannelHeader& channelHeaderEntity, const KeySequence& nextSyncAnchor, sqlite::ResultSet* allChannelHeaderMediaEntities, sqlite::ResultSet* allChannelMemberEntities, const jtime& syncTime )
    throw(data_access_error);

    /*
     * Delete complete channel with all dependent entities
     */
    void deleteChannelHeader( ChannelHeader& channelHeaderEntity, sqlite::ResultSet* allChannelHeaderMediaEntities, sqlite::ResultSet* allChannelMemberEntities, const jtime& syncTime )
    throw(data_access_error);

    /*
     * Clone channel header data to save local data on conflict
     */
    bool cloneChannelHeader( ChannelHeader& channelHeaderEntity, const ChannelHeader* channelHeader, const KeySequence* nextSyncAnchor )
    throw(data_access_error);

    /*
     * Update channel resources
     */
    bool updateResources( const ChannelHeader& channel, sqlite::ResultSet* allChannelMemberEntities )
    throw(data_access_error);

    /*
     * Update channel members
     */
    bool updateMembers( ChannelHeader& channel, sqlite::ResultSet* allChannelMember, const jtime& syncTime )
    throw(data_access_error);

  };

}

#endif // __EJIN_CHANNEL_HEADER_REPOSITORY_H__
