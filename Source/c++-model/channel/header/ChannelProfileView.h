/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_PROFILE_VIEW_H__
#define __EJIN_CHANNEL_PROFILE_VIEW_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"

namespace ejin
{
  /**
   * Entity Class for CHANNEL_PROFILE_VIEW
   */
  class ChannelProfileView: public sqlite::BaseEntity, DataSchema<ChannelProfileView> {
    friend class Database;
    friend class ChannelRepository;
    friend class ChannelHeaderProfile;
    friend class ChannelHeaderRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "CHANNEL_PROFILE_VIEW";

    // ctor
    ChannelProfileView( void );
    ChannelProfileView( const ChannelProfileView& record );
    // dtor
    ~ChannelProfileView( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChannelProfileView& operator=( const ChannelProfileView& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ChannelProfileView(*this); }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_STRING_PROPERTY_INTF( Gid, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( Membership, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( Role, 3 )
    ACCESS_INTEGER_PROPERTY_INTF( Outdated, 4 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 5 )
    ACCESS_INTEGER_PROPERTY_INTF( HeaderSyncInd, 6 )
    ACCESS_INTEGER_PROPERTY_INTF( MemberSyncInd, 7 )
    ACCESS_INTEGER_PROPERTY_INTF( PostSyncInd, 8 )
    ACCESS_INTEGER_PROPERTY_INTF( CommentSyncInd, 9 )
    ACCESS_INTEGER_PROPERTY_INTF( MediaSyncInd, 10 )
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
  };
  
}

#endif // __EJIN_CHANNEL_PROFILE_VIEW_H__
