/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_FULL_TEXT_H__
#define __EJIN_CHANNEL_FULL_TEXT_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"

namespace ejin
{
  /**
   * Entity Class for FULL_TEXT_CHANNEL_FTS
   */
  class ChannelFullText: public sqlite::BaseEntity, DataSchema<ChannelFullText> {
    friend class Database;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "FULL_TEXT_CHANNEL_FTS";
    
    // ctor
    ChannelFullText( void );
    ChannelFullText( const ChannelFullText& record );
    // dtor
    ~ChannelFullText( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChannelFullText& operator=( const ChannelFullText& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ChannelFullText(*this); }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_INTEGER_PROPERTY_INTF( RowId, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( ChannelId, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( EntityId, 2 )
    ACCESS_STRING_PROPERTY_INTF( Source, 3 )
    ACCESS_STRING_PROPERTY_INTF( Name, 4 )
    ACCESS_STRING_PROPERTY_INTF( Content, 5 )
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
  };
  
}

#endif // __EJIN_CHANNEL_FULL_TEXT_H__
