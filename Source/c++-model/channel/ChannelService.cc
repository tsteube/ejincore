/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelService.h"
#include "UsingEjinTypes.h"

#include "Channel.h"
#include "Attachment.h"
#include "Thumb.h"
#include "Media.h"
#include "Member.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMember.h"
#include "Utilities.h"
#include "ChannelRepository.h"
#include "SyncSource.h"
#include "Modifications.h"
#include "SqliteValue.h"

#include <list>

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
  
  string ChannelService::getLock( void ) const
  {
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    assert (source->hasSyncAnchor());
    return source->getSyncAnchor();
  }
  
  void ChannelService::setLock( const string& syncAnchor, const string& username, const jtime& localTime )
  throw(data_access_error)
  {
    // increment overall sync anchor
    setSyncSource( syncAnchor, username, localTime );
  }
  
  void ChannelService::setSessionKeyValidity( integer channelId, bool valid )
  throw(data_access_error)
  {
    shared_ptr<ChannelHeader> headerEntity( this->headerRepository_.loadById( channelId ) );
    if ( ! headerEntity )
    {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "channel with unique identifier %d not found", channelId);
    }
    
    // persist entity
    headerEntity->setInvalidSessionKey( ! valid );
    this->db_.update( *headerEntity );
  }
  
  string ChannelService::exportModifiedChannelHeader( Modifications<ChannelHeader>& container )
  throw(conflict_error, data_access_error)
  {
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    
    container.setAdded( this->headerRepository_.findAdded() );
    container.setUpdated( this->headerRepository_.findUpdated() );
    container.setRemoved( this->headerRepository_.findRemoved() );
    
    container.setSyncAnchor( source->getSyncAnchor() );
    container.setUsername( source->getUsername() );
    
    return source->getSyncAnchor();
  }
  
  list< string > ChannelService::importUpdatedChannelHeader( Modifications<ChannelHeader>& container, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( ! container.getSyncAnchor().empty() );
    
    list< string > conflicts;
    
    // add channel header
    for (list< shared_ptr<ChannelHeader> >::iterator it=container.getAdded().begin(); it != container.getAdded().end(); it++) {
      shared_ptr<ChannelHeader> header( this->headerRepository_.updateHeaderOnly( **it, syncTime ) );
      if ( header ) {
        if  ( header->getSyncInd() == kSyncIndConflict )
        {
          conflicts.push_back( header->getGid() );
        }
      }
    }
    
    // update channel header
    for (list< shared_ptr<ChannelHeader> >::iterator it=container.getUpdated().begin(); it != container.getUpdated().end(); it++) {
      shared_ptr<ChannelHeader> header( this->headerRepository_.updateHeaderOnly( **it, syncTime ) );
      if ( header ) {
        if  ( header->getSyncInd() == kSyncIndConflict )
        {
          conflicts.push_back( header->getGid() );
        }
        else if (header->getSyncInd() == kSyncIndSynchronous && ! header->isOutdated() ) {
          setSyncTime(header->getId(), syncTime);
        }
      }
    }
    
    // remove channel header
    for (list< shared_ptr<ChannelHeader> >::iterator it=container.getRemoved().begin(); it != container.getRemoved().end(); it++) {
      if ( ! this->headerRepository_.deleteChannel( (**it), syncTime ) )
      {
        conflicts.push_back( (*it)->getGid() );
      }
    }
    
    return conflicts;
  }
  
  string ChannelService::exportModifiedChannel( integer channelId, Channel& channel)
  throw(data_access_error)
  {
    assert( channelId > 0 );
    
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    
    // detect conflicts
    if ( channel.hasGid() )
    {
      if ( detectChannelConflict( channel.getGid() ) )
      {
        return "";
      }
    }
    
    shared_ptr<Channel> result( this->loadModificiations( channelId ) );
    if ( result ) {
      channel = *result;
      return source->getSyncAnchor();
    }
    return "";
  }

  list< string > ChannelService::findConflicted( void ) const
  throw(data_access_error)
  {
    list< string > conflicts;
    list< shared_ptr<ChannelHeader> > rawConflicts( this->headerRepository_.findConflicted() );
    for (list< shared_ptr<ChannelHeader> >::iterator it=rawConflicts.begin() ; it != rawConflicts.end(); it++ ) {
      conflicts.push_back( (*it)->getGid() );
    }
    return conflicts;
  }
  
  bool ChannelService::clearConflicts( const list< string >& channelGids )
  throw(data_access_error)
  {
    bool success = true;
    for (list< string >::const_iterator it=channelGids.begin(); it != channelGids.end(); it++) {
      if (! this->clearConflict( *it )) {
        success = false;
      }
    }
    return success;
  }
  
  bool ChannelService::importChannelUpdates( Channel& channel, const string& username, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channel.hasGid() );
    assert( channel.hasSyncAnchor() );
    
    channel.setOperation( kSyncUpdate );
    update( channel, username, syncTime );
    
    bool conflict = detectChannelConflict( channel.getGid() );

    return !conflict;
  }
  
  void ChannelService::commitChannel( Channel& channel, const string& syncAnchor, const string& username, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channel.hasGid() );
    assert( ! syncAnchor.empty() );
    
    this->commit( channel, syncAnchor, username, syncTime );
  }
  
  
  enum RevertLeaf { kNoLeaf, kNewChannel, kNewPost, kNewComment, kNewMedia, kNewMember };
  bool ChannelService::revert( const string& path, bool force )
  throw(data_access_error)
  {
    RevertLeaf leaf = kNoLeaf;
    std::list<string> segments;
    Utilities::split(segments, path, "/");
    std::list<string>::iterator it=segments.begin();
    if ( it != segments.end() && (*it).empty() ) it++; // skip leading empty segment
    if ( it != segments.end() && strcasecmp((*it).c_str(), "channel" ) == 0)
    {
      std::string headerGid;
      std::string postGid;
      std::string commentGid;
      std::string mediaGid;
      std::string member;
      do {
        if (strcasecmp((*it).c_str(), "channel" ) == 0) {
          it++;
          if (it != segments.end())
            headerGid = *it++;
          else
            leaf = kNewChannel;
        } else if (strcasecmp((*it).c_str(), "post" ) == 0) {
          it++;
          if (it != segments.end())
            postGid = *it++;
          else
            leaf = kNewPost;
        } else if (strcasecmp((*it).c_str(), "comment" ) == 0) {
          it++;
          if (it != segments.end())
            commentGid = *it++;
          else
            leaf = kNewComment;
        } else if (strcasecmp((*it).c_str(), "media" ) == 0) {
          it++;
          if (it != segments.end())
            mediaGid = *it++;
          else
            leaf = kNewMedia;
        } else if (strcasecmp((*it).c_str(), "member" ) == 0) {
          it++;
          if (it != segments.end())
            member = *it++;
          else
            leaf = kNewMember;
        }
      } while ( it != segments.end() );
      
      if ( ! mediaGid.empty()) {
        return this->mediaRepository_.revert( mediaGid, force );
      }
      if ( ! commentGid.empty()) {
        return this->commentRepository_.revert( commentGid, force );
      }
      if ( ! postGid.empty()) {
        return this->postRepository_.revert( postGid, force );
      }
      if ( ! member.empty() ) {
        return this->headerRepository_.revert( headerGid, member, force );
      }
      if ( ! headerGid.empty()) {
        return this->headerRepository_.revert( headerGid, force );
      }
    }
    return false;
  }

  template<typename T>
  bool ChannelService::applyCrypto( Modifications<T>& container, RSACrypto* crypto, RSACrypto::Mode mode, bool required ) const
    throw(data_access_error,security_integrity_error)
  {
    bool result = false;
    if ( this->applyCrypto(container.getAdded(), container.getUsername(), crypto, mode, required) )
      result = true;
    if ( this->applyCrypto(container.getUpdated(), container.getUsername(), crypto, mode, required) )
      result = true;
		return result;
  }
  template<typename T>
  bool ChannelService::applyCrypto(list< shared_ptr<T> >& container,
                                   const string& username, RSACrypto* crypto, RSACrypto::Mode mode, bool required ) const
    throw(data_access_error,security_integrity_error)
  {
    bool result = false;
    for (typename list< shared_ptr<T> >::iterator it=container.begin(); it != container.end(); it++) {
      ICryptoEntity* entity = dynamic_cast<ICryptoEntity*>(it->get());
			if (applyCrypto(*entity, username, crypto, mode, required)) {
				result = true;
			}
    }
		return result;
  }
  string ChannelService::exportSessionKey( integer channelId, const string& username  ) const throw(data_access_error)
  {
    return this->headerRepository_.exportSessionKey(channelId, username);
  }
  bool ChannelService::importSessionKey( integer channelId, const string& username, const string& key ) const throw(data_access_error)
  {
    return this->headerRepository_.importSessionKey(channelId, username, key);
  }
  bool ChannelService::applyCrypto( ICryptoEntity& entity, const string& username, RSACrypto* crypto, RSACrypto::Mode mode, bool required ) const throw(data_access_error,security_integrity_error)
  {
    bool resolved = false;
    ChannelHeader* channelHeader = dynamic_cast<ChannelHeader*>(&entity);
    try {
      
      resolved = resolved ||
      (channelHeader && this->headerRepository_.resolveSessionKey(*channelHeader, username, crypto, required));
      resolved = resolved ||
      (entity.hasChannelId() && this->headerRepository_.resolveSessionKey(entity.getChannelId(), username, crypto, required));
      resolved = resolved ||
      (entity.hasChannelGid() && this->headerRepository_.resolveSessionKey(entity.getChannelGid(), username, crypto, required));
      
      switch (mode) {
        case RSACrypto::Mode::ENCRYPT:
          return entity.encrypt(crypto);
        case RSACrypto::Mode::DECRYPT:
          return entity.decrypt(crypto);
        case RSACrypto::Mode::NO_ENCRYPTION:
          return false;
      }
      
    } catch (const ejin::security_integrity_error& e) {
      
      // mark channel session key as invalid
      shared_ptr<ChannelHeader> headerEntity;
      if ( channelHeader )
        headerEntity = this->headerRepository_.loadById( channelHeader->getId() );
      else if ( entity.hasChannelId() )
        headerEntity = this->headerRepository_.loadById( entity.getChannelId() );
      else if ( entity.hasChannelGid() )
        headerEntity = this->headerRepository_.loadByGid( entity.getChannelGid(), kNoResource );
      if ( headerEntity )
      {
        headerEntity->setInvalidSessionKey( true );
        headerEntity->clearIV();
        this->db_.update( *headerEntity );
      }
      
      if ( required ) {
        throw e;
      } else {
        return false;
      }
    }
  }

  // ==========
  // Helper methods
  // ==========

  unique_ptr<SyncSource> ChannelService::getSyncSource( void ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceChannel));
    this->db_.refresh( *entity ); // throws data_access_error if not found
    return entity;
  }
  
  void ChannelService::setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceChannel));
    this->db_.refresh( *entity ); // throws data_access_error if not found
    entity->setSyncAnchor(syncAnchor);
    entity->setModifyTime(localTime);
    entity->setUsername(username);
    this->db_.update( *entity );
  }
  
  // template instantiation
  template bool ChannelService::applyCrypto( Modifications<Media>& container, RSACrypto* crypto, RSACrypto::Mode mode, bool ) const;
  template bool ChannelService::applyCrypto( Modifications<ChannelHeader>& container, RSACrypto* crypto, RSACrypto::Mode mode, bool ) const;

  template bool ChannelService::applyCrypto( list< shared_ptr<Media> >& container,
                                            const string& username, RSACrypto* crypto, RSACrypto::Mode mode, bool ) const;
  template bool ChannelService::applyCrypto( list< shared_ptr<ChannelHeader> >& container,
                                            const string& username, RSACrypto* crypto, RSACrypto::Mode mode, bool ) const;

}
