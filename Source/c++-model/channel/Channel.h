/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_H__
#define __EJIN_CHANNEL_H__

#include <list>

#include "Declarations.h"
#include "SerializableEntity.h"
#include "SqliteValue.h"
#include "SqliteUtil.h"
#include "ICryptoEntity.h"

namespace ejin
{  
  using std::list;
  using sqlite::Value;
  using ser::XmlStream;
  using ser::ISerializableEntity;
  
  // forward declaration
  class Database;  
  class ChannelHeader;
  class ChannelPost;
  class ChannelComment;
  class Resource;
  class RSACrypto;
  
  /**
   * This wrapper class contains the top level channel entity together with posts and comments on the channel. This class
   * is used to provide a channel profile with posts and comments.
   */
  class Channel: public ISerializableEntity, public ICryptoEntity {
    friend std::ostream& operator<<( std::ostream& ostr, const Channel& rhs );
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctors
    Channel( void );
    Channel( const integer id, const string& gid, bool secure = false );
    Channel( const integer id, const string& gid, const string& syncAnchor, bool secure = false );
    Channel( shared_ptr<ChannelHeader> header, list< shared_ptr<ChannelPost> > posts, list< shared_ptr<ChannelComment> > comments );
    Channel( const integer id, const string& gid, shared_ptr<ChannelHeader> header, list< shared_ptr<ChannelPost> > posts, list< shared_ptr<ChannelComment> > comments );
    Channel( const Channel& channel );
    // dtor
    ~Channel( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Channel& operator=( const Channel& rhs );
    // comparision
    bool operator==( const Channel& rhs ) const;    
    bool operator<( const Channel& rhs ) const;
    // debug
    string toString( void ) const;
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool isEmpty( void ) const;
    void clearBusinessData( void );
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );
    
    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );

    ISerializableEntity* entityByXmlNode( const char* name, int position );
    Value* valueByXmlNode( const char* name );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const; 
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const integer getChannelId( void ) const { return getId(); }
    bool hasChannelId( void ) const { return hasId(); }
    
    const string getChannelGid( void ) const { return getGid(); }
    bool hasChannelGid( void ) const { return hasGid(); }

    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);

    // ==========
    // Getter / Setter
    // ==========
  public:
    
    const integer getId( void ) const { return this->id_.num(); }
    void setId( const integer id ) { this->id_.num(id); }
    void clearId( void ) { this->id_.setNull(); }
    bool hasId( void ) const { return ! this->id_.isNull(); }
    
    const string getGid( void ) const { return this->gid_.toString(); }
    void setGid( const string& gid ) { this->gid_.str(gid); }
    void clearGid( void ) { this->gid_.setNull(); }
    bool hasGid( void ) const { return ! this->gid_.isNull(); }
    
    const sqlite::jtime getSyncTime( void ) const { return this->syncTime_.date(); }
    void setSyncTime( const sqlite::jtime& syncTime ) { this->syncTime_.date(syncTime); }
    void clearSyncTime( void ) { this->syncTime_.setNull(); }
    bool hasSyncTime( void ) const { return ! this->syncTime_.isNull(); }
    
    const sqlite::jtime getLastSyncTime( void ) const { return this->syncTime_.date(); }
    void setLastSyncTime( sqlite::jtime& syncTime ) { this->syncTime_.date(syncTime); }
    void clearLastSyncTime( void ) { this->syncTime_.setNull(); }
    bool hasLastSyncTime( void ) const { return ! this->syncTime_.isNull(); }
    
    const string getSyncAnchor( void ) const { return this->syncAnchor_.toString(); }
    void setSyncAnchor( const string& syncAnchor ) { this->syncAnchor_.str(syncAnchor); }
    void clearSyncAnchor( void ) { this->syncAnchor_.setNull(); }
    bool hasSyncAnchor( void ) const { return ! this->syncAnchor_.isNull(); }
    
    const SyncOperation getOperation( void ) const { return (SyncOperation) this->operation_.num(); }
    void setOperation( SyncOperation operation ) { this->operation_.num(operation); }
    void clearOperation( void ) { this->operation_.setNull(); }
    bool hasOperation( void ) const { return ! this->operation_.isNull(); }
    
    shared_ptr<ChannelHeader> getHeader( void ) const { return this->header_; }
    void setHeader( shared_ptr<ChannelHeader> header ) { this->header_ = header; }
    void clearHeader( void ) { this->header_.reset(); }
    bool hasHeader( void ) const { return (bool)this->header_; }
    
    list< shared_ptr<ChannelPost> >& getPosts( void ) { return this->posts_; }
    void setPosts(const list< shared_ptr<ChannelPost> >& posts) { this->posts_ = posts; }
    
    list< shared_ptr<ChannelComment> >& getComments( void ) { return this->comments_; }
    void setComments(const list< shared_ptr<ChannelComment> >& comments) { this->comments_ = comments; }
    
    list< shared_ptr<ChannelComment> > getAllComments( void ) const;
    list< shared_ptr<Resource> > getAllResources( void ) const;
    
    RSACrypto* getCrypto() const { return this->crypto_; }
    void setCrypto( RSACrypto* crypto ) { this->crypto_ = crypto; }
    void clearCrypto( void ) { this->crypto_ = NULL; }
    bool hasCrypto( void ) const { return (bool)this->crypto_; }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // Unique local identifier of the channel.
    Value id_;
    
    // Unique global identifier of the channel.
    Value gid_;

    // Current synchronization time
    Value syncTime_;
    
    // Last synchronization time
    Value lastSyncTime_;
    
    // Last synchronization anchor
    Value syncAnchor_;
    
    // operation marker
    Value operation_;
    
    // encrypted channel
    bool secure_;
    
    // crypto reference
    RSACrypto* crypto_;
    
    // Top level channel object
    shared_ptr<ChannelHeader> header_;
    
    // List of posts of the channel.
    list< shared_ptr<ChannelPost> > posts_;
    
    // List of comments on any post of the channel.
    list< shared_ptr<ChannelComment> > comments_;
    
  };  
  
  // debugging
  std::ostream& operator<<( std::ostream& ostr, const Channel& rhs );  
  
}

#endif // __EJIN_CHANNEL_H__
