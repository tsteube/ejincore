/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMember.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "SqliteValue.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::tag;
  using ser::attr;
  using ser::chardata;
  using ser::endtag;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<ChannelMember>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("CHANNEL_ID", type_int, flag_not_null),
    Attribute("USERNAME", type_text, flag_not_null),
    Attribute("FULLNAME", type_text, flag_none),
    Attribute("MODIFIED_BY", type_text, flag_none),
    Attribute("MODIFIED_BY_FULLNAME", type_text, flag_none),
    Attribute("SESSION_KEY", type_text, flag_none),
    Attribute("PKEY_FINGER_PRINT", type_text, flag_none),
    Attribute("TAGS", type_text, flag_none),
    Attribute("ROLE", type_int, flag_not_null, Value((integer)0), RoleEnumMapping),
    Attribute("MEMBERSHIP", type_int, flag_not_null, Value((integer)0), MembershipEnumMapping),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute("SECURE", type_bool, flag_not_null, Value(false)),
    Attribute("ENCRYPTED", type_bool, flag_not_null, Value(false)),
    Attribute("IV", type_text, flag_none),
    Attribute()
  };
  template <> const AttributeSet DataSchema<ChannelMember>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelMember::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findChannelMemberById"] = "ID = ?";
      tMap["findByUsername"] = "USERNAME = ?";
      tMap["findByChannelAndMember"] = "CHANNEL_ID = ? AND USERNAME = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["findChannelMembers"] = "CHANNEL_ID = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["BackupSet"] = "CHANNEL_ID = ?1 AND MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["BackupSetBak"] = "CHANNEL_ID = ?1 AND MASTER_ID IS NULL";
    }
    return tMap;
  }
  const map<string,string> ChannelMember::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "SELECT MAX(modified_by) "
      "FROM " << schema << ".channel_member_tbl "
      "WHERE channel_id = ? AND modify_time = ? AND master_id IS NULL";
      tMap["lastModifiedBy"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT count(*) > 0 "
      "FROM " << schema << ".channel_member_tbl "
      "WHERE username = ? AND sync_ind in ( 1, 2, 4 ) AND master_id IS NULL";
      tMap["requiredMember"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  const string ChannelMember::restoreSet( void ) const {
    return "CHANNEL_ID = ?1";
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelMember::ChannelMember( void ): BaseEntity(TABLE_DDL, false)
  {
    this->setRole( kSyncRoleRead );
    this->setMembership( kMembershipInvited );
    this->setSyncInd( kSyncIndInsert );
    this->clearMasterId( );
    this->setSecure( false );
    this->setEncrypted( false );
    this->setModificationMask((integer) 0);
  }
  ChannelMember::ChannelMember( const string& username ): ChannelMember()
  {
    setUsername(username);
  }
  ChannelMember::ChannelMember( const integer channelId, const string& username ): ChannelMember()
  {
    setChannelId(channelId);
    setUsername(username);
  }
  ChannelMember::ChannelMember( const ChannelMember& record ): BaseEntity(record)
  {
    this->operator=(record);
  }
  ChannelMember::~ChannelMember( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  ChannelMember& ChannelMember::operator=( const ChannelMember& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, ChannelId);
      COPY_PROPERTY((*this), rhs, Username);
      COPY_PROPERTY((*this), rhs, Fullname);
      COPY_PROPERTY((*this), rhs, ModifiedBy);
      COPY_PROPERTY((*this), rhs, ModifiedByFullname);
      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      COPY_PROPERTY((*this), rhs, Role);
      COPY_PROPERTY((*this), rhs, Membership);
      COPY_PROPERTY((*this), rhs, Tags);
      COPY_PROPERTY((*this), rhs, SessionKey);
      COPY_PROPERTY((*this), rhs, FingerPrint);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, MasterId);
      
      COPY_BOOL_PROPERTY((*this), rhs, Secure);
      COPY_BOOL_PROPERTY((*this), rhs, Encrypted);
      COPY_PROPERTY((*this), rhs, IV);
      COPY_PROPERTY((*this), rhs, ModificationMask);
    }
    return *this;
  }
  
  // comparison
  bool ChannelMember::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChannelMember& mrhs = dynamic_cast<const ChannelMember&> (rhs);
    if (this->hasId() && mrhs.hasId())
      return this->getId() == mrhs.getId();
    else
      return (this->getUsername() == mrhs.getUsername()) &&
      (this->getChannelId() == mrhs.getChannelId());
  }
  bool ChannelMember::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChannelMember& mrhs = dynamic_cast<const ChannelMember&> (rhs); // throws std::bad_cast if not of type ChannelMember&
    if (this->getChannelId() < mrhs.getChannelId())
      return true;
    if (this->getUsername() < mrhs.getUsername())
      return true;
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  const char* ChannelMember::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelMember::tableFields( void ) const { return TABLE_DDL; }
  
  const string& ChannelMember::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& ChannelMember::xmlFields( void ) const { return XML_DDL; }
  
  bool ChannelMember::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const ChannelMember& mrhs = dynamic_cast<const ChannelMember&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Tags);
      COPY_PROPERTY((*this), mrhs, SessionKey);
      COPY_PROPERTY((*this), mrhs, FingerPrint);
      COPY_PROPERTY((*this), mrhs, Role);
      COPY_PROPERTY((*this), mrhs, Membership);
      COPY_PROPERTY((*this), mrhs, ModifyTime);
      COPY_PROPERTY((*this), mrhs, Fullname);
      COPY_PROPERTY((*this), mrhs, ModifiedBy);
      COPY_PROPERTY((*this), mrhs, ModifiedByFullname);
      COPY_PROPERTY((*this), mrhs, IV);
      
      return true;
    }
    return false;
  }
  bool ChannelMember::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    ChannelMember& rhs( (ChannelMember&)arg );
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( this->getRole() != rhs.getRole() )
      return false;
    if ( this->getTags() != rhs.getTags() )
      return false;
    return true;
  }
  bool ChannelMember::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void ChannelMember::archive( void )
  {
    this->clearId( );
    this->setSyncInd( kSyncIndSynchronous );
    this->setModificationMask( kNoneModification );
  }
  void ChannelMember::unarchive( void )
  {
    this->clearId( );
    this->setSyncInd( kSyncIndInsert );
    this->setMembership( (this->getRole() ==  kSyncRoleOwner) ? kMembershipAccepted : kMembershipInvited );
    this->setModificationMask( kChannelMemberPendingLocalModification );
    this->clearFullname( );
    this->clearModifiedBy( );
    this->clearModifiedByFullname( );
    this->clearSessionKey( );
    this->clearFingerPrint( );
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->clearMasterId( );
    this->setSecure( false );
    this->setEncrypted( false );
    this->clearIV( );
  }
  void ChannelMember::clearData( void )
  {
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->clearTags( );
    this->clearSessionKey( );
    this->clearFingerPrint( );
    this->setRole( kSyncRoleRead );
    this->setMembership( kMembershipInvited );
    this->setSecure( false );
    this->setEncrypted( false );
    this->clearIV( );
  }
  bool ChannelMember::isEmpty( void ) const
  {
    return ! hasModifyTime() && ! hasCreateTime();
  }
  
  // ==========
  // Crypto Interface
  // ==========
  
  bool ChannelMember::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if ( crypto && crypto->hasSessionKey( ) ) {
      if (this->hasTags()) {
        this->setTags( crypto->decrypt( this->getTags() ) );
      }
      this->setSecure( true );
      this->setEncrypted( false );
      COPY_PROPERTY((*this), (*crypto), IV);
      return true;
    } else {
      this->setSecure( false );
      this->setEncrypted( false );
      this->clearIV( );
      return false;
    }
  }
  
  bool ChannelMember::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    bool result = false;
    if ( crypto && crypto->hasSessionKey( ) ) {
      switch( this->getSyncInd() ) {
        case kSyncIndSynchronous:
          COPY_PROPERTY((*crypto), (*this), IV); // use old IV
          break;
        case kSyncIndInsert:
        case kSyncIndUpdate:
          crypto->createIV( ); // generate new IV
          break;
        case kSyncIndDelete:
        case kSyncIndConflict:
          assert(false);
          break;
      }
      if (this->hasTags()) {
        this->setTags( crypto->encrypt( this->getTags() ) );
      }
      this->setSecure( true );
      this->setEncrypted( true );
      COPY_PROPERTY((*this), (*crypto), IV);
      result = true;
    } else {
      if (this->isSecure()) {
        _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "no session key to encrypt message");
      }
      this->setEncrypted( false );
      this->clearIV( );
    }
    return result;
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON mapping
  
  bool ChannelMember::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasUsername() ) {
      j["username"] = string( this->value("USERNAME")->str(), this->value("USERNAME")->size() );
    }
    if ( this->hasRole() ) {
      switch (this->getRole()) {
        case kSyncRoleRead:
          j["role"] = "read";
          break;
        case kSyncRoleWrite:
          j["role"] = "write";
          break;
        case kSyncRoleAdmin:
        case kSyncRoleOwner:
          j["role"] = "admin";
          break;
      }
    }
    if ( this->hasMembership() ) {
      if (this->getMembership() == kMembershipInvited) {
        j["membership"] = "invited";
      }
    }
    if ( this->hasSessionKey() ) {
      j["key"] = string( this->value("SESSION_KEY")->str(), this->value("SESSION_KEY")->size() );
    }
    if ( this->hasTags() ) {
      j["tags"] = string( this->value("TAGS")->str(), this->value("TAGS")->size() );
    }

    return true;
  }
  /*
  bool ChannelMember::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if (this->hasUsername()) {
      json_object_set_new( node, "username", json_stringn_nocheck( this->value("USERNAME")->str(),
                                                                   this->value("USERNAME")->size() ) );
    }
    if ( this->hasRole() ) {
      switch (this->getRole()) {
        case kSyncRoleRead:
          json_object_set_new( node, "role", json_string( "read" ) );
          break;
        case kSyncRoleWrite:
          json_object_set_new( node, "role", json_string( "write" ) );
          break;
        case kSyncRoleAdmin:
        case kSyncRoleOwner:
          json_object_set_new( node, "role", json_string( "admin" ) );
          break;
      }
    }
    
    if ( this->hasMembership() ) {
      if (this->getMembership() == kMembershipInvited) {
        json_object_set_new( node, "membership", json_string( "invited" ) );
      }
    }
    
    if ( this->hasSessionKey() ) {
      json_object_set_new( node, "key", json_stringn_nocheck( this->value("SESSION_KEY")->str(),
                                                             this->value("SESSION_KEY")->size() ) );
    }
    if ( this->hasTags() ) {
      json_object_set_new( node, "tags", json_stringn_nocheck( this->value("TAGS")->str(),
                                                              this->value("TAGS")->size() ) );
    }
    
    return true;
  }
  */

  bool ChannelMember::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("role")) {
        this->value("ROLE")->str( j["role"] );
      }
      if ( j.contains("membership")) {
        this->value("MEMBERSHIP")->str( j["membership"] );
      }
      if ( j.contains("finger_print")) {
        this->value("PKEY_FINGER_PRINT")->str( j["finger_print"] );
      }
      if ( j.contains("tags")) {
        this->value("TAGS")->str( j["tags"] );
      }
      if ( j.contains("username")) {
        this->value("USERNAME")->str( j["username"] );
      }
      if ( j.contains("fullname")) {
        this->value("FULLNAME")->str( j["fullname"] );
      }
      if ( j.contains("modified_by")) {
        this->value("MODIFIED_BY")->str( j["modified_by"] );
      }
      if ( j.contains("modified_by_fullname")) {
        this->value("MODIFIED_BY_FULLNAME")->str( j["modified_by_fullname"] );
      }
      if ( j.contains("create_time")) {
        this->value("CREATE_TIME")->str( j["create_time"] );
      }
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }

      return true;
    }
    
    return false;
  }
  /*
  bool ChannelMember::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "role");
    if ( json_is_string(value) ) {
      this->value("ROLE")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "membership");
    if ( json_is_string(value) ) {
      this->value("MEMBERSHIP")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "finger_print");
    if ( json_is_string(value) ) {
      this->value("PKEY_FINGER_PRINT")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "tags");
    if ( json_is_string(value) ) {
      this->value("TAGS")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "username");
    if ( json_is_string(value) ) {
      this->value("USERNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "fullname");
    if ( json_is_string(value) ) {
      this->value("FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by_fullname");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "create_time");
    if ( json_is_string(value) ) {
      this->value("CREATE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    
    return true;
  }
  */

  // XML Mapping
  
  const string ChannelMember::XML_NAME   = "member";
  const ser::XmlAttribute ChannelMember::XML_FIELDS[] =
  {
    ser::XmlAttribute("role", "ROLE" ),
    ser::XmlAttribute("membership", "MEMBERSHIP" ),
    ser::XmlAttribute("finger_print", "PKEY_FINGER_PRINT" ),
    ser::XmlAttribute("tags", "TAGS" ),
    ser::XmlAttribute("username", "USERNAME" ),
    ser::XmlAttribute("fullname", "FULLNAME" ),
    ser::XmlAttribute("modified_by", "MODIFIED_BY" ),
    ser::XmlAttribute("modified_by_fullname", "MODIFIED_BY_FULLNAME" ),
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute("create_time", "CREATE_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet ChannelMember::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  bool ChannelMember::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("member"); // start element tag
    
    // serialize properties
    if ( this->hasUsername() ) {
      xml << attr("username") << this->getUsername();
    }
    if ( this->hasRole() ) {
      xml << attr("role");
      switch (this->getRole()) {
        case kSyncRoleRead:
          xml << "read";
          break;
        case kSyncRoleWrite:
          xml << "write";
          break;
        case kSyncRoleAdmin:
        case kSyncRoleOwner:
          xml << "admin";
          break;
        default:
          xml << "undefined";
          break;
      }
    }
    /*
     if ( this->hasMembership() ) {
     if (this->getMembership() == kMembershipInvited) {
     xml << attr("membership") << "invited";
     }
     }
     */
    if ( this->hasSessionKey() ) {
      xml << tag("key") << chardata() << this->getSessionKey() << endtag();
    }
    if ( this->hasTags() ) {
      xml << tag("tags") << chardata() << this->getTags() << endtag();
    }
    
    xml << ser::endtag(); // end element tag
    return true;
  }
  
}
