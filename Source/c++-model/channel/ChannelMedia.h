/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_MEDIA_ENTITY_H__
#define __EJIN_CHANNEL_MEDIA_ENTITY_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "IBusinessData.h"
#include "Resource.h"

namespace ejin
{
  // forward declaration
  class Database;
  class ChannelHeaderProfile;
  class ChannelHeaderRepository;
  class ChannelPostProfile;
  class ChannelPostRepository;
  class ChannelCommentProfile;
  class ChannelCommentRepository;
  
  /**
   * Entity Class for MEDIA_TBL
   */
  class ChannelMedia: public Resource, DataSchema<ChannelMedia> {
    friend class Database;
    friend class MediaProfile;
    friend class ChannelHeaderProfile;
    friend class ChannelHeaderRepository;
    friend class ChannelPostProfile;
    friend class ChannelPostRepository;
    friend class ChannelCommentProfile;
    friend class ChannelCommentRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    // IMPORT NOTE: There are 2 mappins on MEDIA_TBL therefore we use different table names (lower/upper letter)!!!!
    constexpr static const char* const TABLE_NAME = "media_tbl";
    
    // ctors
    ChannelMedia( void );
    ChannelMedia( const string& mediaGid );
    ChannelMedia( const ChannelMedia& record );
    // dtor
    ~ChannelMedia( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChannelMedia& operator=( const ChannelMedia& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ChannelMedia(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool copyDataFrom( const BaseEntity& rhs );
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool hasContent() const { return false; }
    bool isEmpty() const { return false; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& j ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;
    
    // Database DDL
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Member Variables
    
    // ------
    // Static Variables
    
    // XML schema
    const static string               XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif // __EJIN_CHANNEL_MEDIA_ENTITY_H__
