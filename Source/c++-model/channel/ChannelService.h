/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_SERVICE_H__
#define __EJIN_CHANNEL_SERVICE_H__

#include "ChannelRepository.h"
#include "RSACrypto.h"
#include "ICryptoEntity.h"

namespace ejin
{
  using sqlite::ResultSet;
  // forward declaration
  class SyncSource;
  template <typename T> class Modifications;
  
  /**
   * The channel service provides serialized views on the channel repository. These methods will be used to read and
   * update the private list of channel entities.
   */
  class ChannelService: public ChannelRepository {
    
    // ==========
    // Public Interface
    // ==========    
  public:
    
    // constructors
    ChannelService( Database& db, ChannelHeaderRepository& headerRepository, ChannelPostRepository& postRepository, ChannelCommentRepository& commentRepository, MediaRepository& mediaRepository, MemberRepository& memberRepository ):
    ChannelRepository( db, headerRepository, postRepository, commentRepository, mediaRepository, memberRepository ) {}
    ~ChannelService( void ) {};
    
    /** Returns the synchronization anchor of the last synchronization of channels. */
    string getLock( void ) const;
    
    /**
     * Updates the synchronization anchor of the list of visible channels.
     */
    void setLock( const string& syncAnchor, const string& username, const jtime& localTime )
    throw(data_access_error);
    
    /**
     * Mark channel as encrypted
     */
    void setSessionKeyValidity( integer channelId, bool valid )
    throw(data_access_error);
    
    /**
     * Search for all recent changes of channel header entities since the last synchronization. This method returns the
     * last remembered synchronization anchor.
     */ 
    string exportModifiedChannelHeader( Modifications<ChannelHeader>& container )
    throw(conflict_error, data_access_error);    
    
    /**
     * Updates all changes on the given channel header instances into the persistence store. If conflicts are detected
     * this method returns false.
     */
    list< string > importUpdatedChannelHeader( Modifications<ChannelHeader>& channel, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Search for all recent changes of the specified channel instance since the last synchronization. This method
     * returns the last remembered synchronization anchor.
     */ 
    string exportModifiedChannel( integer channelId, Channel& container )
    throw(data_access_error);    
    
    /**
     * Updates all changes on the given channel instance into the persistence store. If conflicts are detected this
     * method returns false.
     */ 
    bool importChannelUpdates( Channel& container, const string& username, const jtime& syncTime  )
    throw(data_access_error);
    
    /**
     * Find all conflicted channel data sets. If the channel instance was edited locally and on the server both data
     * sets are kept to resolve the conflict by user interaction.
     */
    list< string > findConflicted( void ) const
    throw(data_access_error);

    /**
     * Revert local changes to synchronize the channel entity with the server reference.
     */ 
    bool clearConflicts( const list< string >& channelGids )
    throw(data_access_error);
    
    /**
     * Commit all changes on the given channel instances into the persistence store. This method will update the
     * synchronization anchor and global identifiers.
     */
    void commitChannel( Channel& container, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Revert entity according to the specified target channel entity path; separated by '/'
     * @param path Channel entity path Channel/{gid}[/Post/{gid}][/Comment/{gid}][/Media/{gid}][/Member|Post|Comment|Media]
     * @param force if true all local changes gets reverted completely otherwise the data set is marked as conflicted
     */
    bool revert( const string& path, bool force = false )
    throw(data_access_error);
    
    /**
     * Apply decrypt or encrypt algorithm to the model entity
     */
    template<typename T>
    bool applyCrypto( Modifications<T>& container, RSACrypto* crypto, RSACrypto::Mode mode, bool required = true ) const
    throw(data_access_error,security_integrity_error);
    
    /**
     * Apply decrypt or encrypt algorithm to the model entity
     */
    template<typename T>
    bool applyCrypto( list< shared_ptr<T> >& container,
                     const string& username, RSACrypto* crypto, RSACrypto::Mode mode, bool required = true ) const
    throw(data_access_error,security_integrity_error);

    /**
     * Apply decrypt or encrypt algorithm to the model entity
     */
    bool applyCrypto( ICryptoEntity& container, const string& username, RSACrypto* crypto, RSACrypto::Mode mode, bool required = true ) const
    throw(data_access_error,security_integrity_error);
    
    /**
     * Load session key of the target channel.
     */
    string exportSessionKey( integer channelId, const string& username  ) const throw(data_access_error);
    
    /**
     * Override session key of target channel
     */
    bool importSessionKey( integer channelId, const string& username, const string& key ) const throw(data_access_error);

    // ==========
    // Private Interface
    // ==========    
  private:
    
    /**
     * Return the overall media synchronization state.
     */
    unique_ptr<SyncSource> getSyncSource( void ) const
    throw(data_access_error);
    
    /**
     * Updates the overall media synchronization state.
     */ 
    void setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
    throw(data_access_error);
    
  };
  
}

#endif // __EJIN_CHANNEL_SERVICE_H__
