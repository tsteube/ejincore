/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelProfile.h"
#include "UsingEjinTypes.h"

#include "ChannelProfileView.h"
#include "ChannelSkeletonView.h"
#include "Channel.h"
#include "Media.h"
#include "Member.h"
#include "ChannelHeader.h"
#include "ChannelMember.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelFullText.h"
#include "Utilities.h"
#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
  
  shared_ptr<Channel> ChannelProfile::loadModificiations( integer channelId ) throw(data_access_error)
  {
    assert ( channelId > 0 );
    
    // load the core channel entity
    shared_ptr<ChannelHeader> channelHeader(shared_ptr<ChannelHeader>( new ChannelHeader() ));
    channelHeader->setId(channelId);    
    this->db_.refresh(*channelHeader);
    this->headerRepository_.resolve( channelHeader, kResourceTypeChannelMedia );
    // call database view
    Value channelIdVal(channelId);
    unique_ptr<ResultSet> profileIds( db_.find(ChannelSkeletonView::TABLE_NAME, "modifiedChannelProfileById", &channelIdVal, NULL) );
    
    // extract unique channel post and comment identifiers
    set<integer> channelPostIds;
    set<integer> channelCommentIds;
    
    SyncInd syncInd = (SyncInd) channelHeader->getSyncInd();
    for (vector< BaseEntity* >::iterator it = profileIds->begin(); it != profileIds->end(); it++) {
      ChannelSkeletonView* view = static_cast< ChannelSkeletonView* >(*it);
      
      if (view->hasCommentId() && view->getCommentId() > 0) {
        if (view->getCommentSyncInd() > 0) {
          channelCommentIds.insert(view->getCommentId());
        }
      } else if (view->hasPostId() && view->getPostId() > 0) {
        if (view->getPostSyncInd() > 0) {
          channelPostIds.insert(view->getPostId());
        }
      } else {
        if (view->getChannelSyncInd() > 0 && channelHeader->getSyncInd() == kSyncIndSynchronous) {
          syncInd = kSyncIndUpdate;
        }
      }
    }
    
    // now load channel posts and comments by identifiers
    list< shared_ptr<ChannelPost> > posts;
    list< shared_ptr<ChannelComment> > comments;
    
    if ( ! channelPostIds.empty() )
    {
      posts = this->postRepository_.load( *channelHeader,
                                         list< integer >(channelPostIds.begin(), channelPostIds.end()) );
    }    
    if ( ! channelCommentIds.empty() )
    {
      comments = this->commentRepository_.load( *channelHeader,
                                               list< integer >(channelCommentIds.begin(), channelCommentIds.end()) );
    }
    
    if ( syncInd == kSyncIndSynchronous ) {
      // check for member updates
      for ( list< shared_ptr<ChannelMember> >::iterator it = channelHeader->getMembers().begin();
            it != channelHeader->getMembers().end();
            it++ ) {
        if ( (*it)->getSyncInd() != kSyncIndSynchronous ) {
          syncInd = kSyncIndUpdate;
        }
      }
    }

    // create result object
    return this->createModifiedSkeleton( syncInd, channelHeader, posts, comments );
  }
  
  list<integer> ChannelProfile::searchFullText( const string& pattern )
  throw(data_access_error)
  {
    // call entity finder
    Value regex( pattern );
    unique_ptr<ResultSet> values( this->db_.find(ChannelFullText::TABLE_NAME, "searchChannelFullText", &regex, NULL) );
    list< integer > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      ChannelFullText* channelFullText = static_cast< ChannelFullText* >( *it );
      result.push_back(channelFullText->getChannelId());
    }
    return result;
  }

  shared_ptr<Channel> ChannelProfile::exportChannel( const string& channelGid )
  throw(data_access_error)
  {
    // load channel header first
    shared_ptr<ChannelHeader> channelHeader = this->headerRepository_.loadByGid( channelGid, kResourceTypeChannelMedia );
    if ( ! channelHeader )
    {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "channel[gid=%s] not found", channelGid.c_str());
    }
    
    list< shared_ptr<ChannelPost> > posts( this->postRepository_.loadAll(*channelHeader, kResourceTypeMedia) );
    list< shared_ptr<ChannelComment> > comments( this->commentRepository_.loadAll(*channelHeader, kResourceTypeMedia) );
    
    shared_ptr<Channel> channel( createChannel( channelHeader, posts, comments ) );
    assert( channel );
    return channel;
  }

  shared_ptr<Channel> ChannelProfile::exportExtendedChannel( const integer channelId, const char* username )
    throw(data_access_error)
  {
    // load channel header first
    shared_ptr<ChannelHeader> channelHeader = this->headerRepository_.loadById( channelId, kResourceTypeMedia );
    if ( ! channelHeader )
    {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "channel[id=%ld] not found", channelId);
    }

    if ( channelHeader->getSyncInd() == kSyncIndConflict && ! channelHeader->hasContent() ) {
      // channel has been removed on the server!
      shared_ptr<Channel> channel( createExtendedChannel( channelHeader,
                                                         list< shared_ptr<ChannelPost> >(),
                                                         list< shared_ptr<ChannelComment> >(),
                                                         username ) );
      assert( channel );
      return channel;
      
    } else {
      // ----
      // filter out deleted channel posts:
      auto posts( this->postRepository_.loadAll(*channelHeader, kResourceTypeMedia) );
      list< shared_ptr<ChannelPost> > visiblePosts(posts.size());
      auto it = std::copy_if( posts.begin(), posts.end(), visiblePosts.begin(),
                             [](shared_ptr<ChannelPost> post){return post->getSyncInd() != kSyncIndDelete;} );
      visiblePosts.resize(std::distance(visiblePosts.begin(),it));  // shrink container to new size
      
      // transform to map of visible posts by postId
      map< integer, shared_ptr<ChannelPost> > Id2Post;
      std::transform( visiblePosts.begin(), visiblePosts.end(), std::inserter(Id2Post, Id2Post.end()),
                     [](shared_ptr<ChannelPost> post){ return std::make_pair(post->getId(), post); } );
      
      // ----
      // filter out deleted channel comments:
      auto comments( this->commentRepository_.loadAll(*channelHeader, kResourceTypeMedia) );
      list< shared_ptr<ChannelComment> > visibleComments(comments.size());
      auto it2 = std::copy_if( comments.begin(), comments.end(), visibleComments.begin(),
                              [Id2Post](shared_ptr<ChannelComment> comment) {
                                if ( comment->getSyncInd() == kSyncIndDelete ) {
                                  return false;
                                }
                                if (comment->hasPostId()) {
                                  if ( Id2Post.find( comment->getPostId() ) == Id2Post.end())
                                    return false;
                                  const shared_ptr<ChannelPost> post = Id2Post.at(comment->getPostId());
                                  if ( post->getSyncInd() == kSyncIndConflict && ! post->hasContent() ) {
                                    return false;
                                  }
                                }
                                return true;
                              } );
      visibleComments.resize(std::distance(visibleComments.begin(),it2));  // shrink container to new size
      
      shared_ptr<Channel> channel( createExtendedChannel( channelHeader, visiblePosts, visibleComments, username ) );
      assert( channel );
      return channel;
      
    }
  }

  // ==========
  // Protected Interface
  // ==========
  
  shared_ptr<Channel> ChannelProfile::createModifiedSkeleton( SyncInd syncInd,
                                                              shared_ptr<ChannelHeader> channelHeader, 
                                                              list< shared_ptr<ChannelPost> > posts,
                                                              list< shared_ptr<ChannelComment> > comments )
  throw(data_access_error)
  {
    shared_ptr<Channel> channel( shared_ptr<Channel>(new Channel(channelHeader->getId(), channelHeader->getGid(), channelHeader->isSecure()) ) );
    
    shared_ptr<ChannelHeader> header;
    switch ( syncInd )
    {
      case kSyncIndDelete:
        channel->setOperation( kSyncRemove );
        // just return the channel with global identifier to remove
        header = shared_ptr<ChannelHeader>(new ChannelHeader());
        header->setId( channelHeader->getId() );
        header->setGid( channelHeader->getGid() );
        channel->setHeader( header );
        return channel;
      case kSyncIndInsert:
        channel->setOperation( kSyncInsert );
        // full header information
        channel->setHeader( channelHeader );
        break;
      case kSyncIndUpdate:
        channel->setOperation( kSyncUpdate );
        // full header information
        channel->setHeader( channelHeader );
        break;
      case kSyncIndSynchronous:
      case kSyncIndConflict:
        // expect updates of any post or comment
        channel->setOperation( kSyncUpdate );        
        break;
      default:
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of ChannelHeader[gid=%s]", channelHeader->getGid().c_str());
    }
    
    for (list< shared_ptr<ChannelPost> >::iterator it = posts.begin(); it != posts.end(); it++ ) {
      shared_ptr<ChannelPost> post;
      switch ( (*it)->getSyncInd() )
      {
        case kSyncIndDelete:
          // just remember the global post id to remove
          post = shared_ptr<ChannelPost>(new ChannelPost());
          post->setId( (*it)->getId() );
          post->setGid( (*it)->getGid() );
          break;
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
          // full post information
          post = shared_ptr<ChannelPost>(new ChannelPost( **it ));
          break;
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync operation[ind=%s]", (*it)->getSyncInd());
      }
      
      if ( post ) {
        channel->getPosts().insert(channel->getPosts().begin(), post);
      }
    }
    
    for (list< shared_ptr<ChannelComment> >::iterator it = comments.begin(); it != comments.end(); it++ ) {
      shared_ptr<ChannelComment> comment;
      switch ( (*it)->getSyncInd() )
      {
        case kSyncIndDelete:
          // just remember the global comment id to remove
          comment = shared_ptr<ChannelComment>(new ChannelComment());
          comment->setId( (*it)->getId() );
          comment->setGid( (*it)->getGid() );
          break;
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
          // full post information
          comment = shared_ptr<ChannelComment>(new ChannelComment( **it ));
          break;
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync operation[ind=%s]", (*it)->getSyncInd());
      }
      
      if ( comment->hasGid() ) {
        // add to top level list of comments
        channel->getComments().insert(channel->getComments().begin(), comment);
        
      } else {
        // 1. find the corresponding post in list of modified posts
        ChannelPost ref;
        if ( (*it)->hasPostId() ) {
          ref.setId( (*it)->getPostId() );
        }
        shared_ptr<ChannelPost> post = Utilities::findElementIn( channel->getPosts(), ref );
        if ( post ) {
          post->getComments().insert(post->getComments().begin(), comment);
        } else {
          // 2. set global gid of modified post
          if (comment->hasPostId()) {
            post = shared_ptr<ChannelPost>(new ChannelPost());
            post->setId( comment->getPostId() );
            // refresh from database
            db_.refresh(*post);
            if ( post->hasGid() )
              comment->setPostGid( post->getGid() );
          }
          channel->getComments().insert(channel->getComments().begin(), comment);
        }
      }
      
    }

    return channel->isEmpty() ? shared_ptr<Channel>() : channel;
  }
  
  shared_ptr<Channel> ChannelProfile::createChannel( shared_ptr<ChannelHeader> channelHeader,
                                                     list< shared_ptr<ChannelPost> > posts,
                                                     list< shared_ptr<ChannelComment> > comments )
  throw(data_access_error)
  {
    // update in/out argument
    shared_ptr<Channel> channel( shared_ptr<Channel>(new Channel(channelHeader->getId(), channelHeader->getGid(), channelHeader->isSecure()) ) );
    
    channel->setHeader( channelHeader );
    
    channel->getPosts().insert(channel->getPosts().end(), posts.begin(), posts.end());
    channel->getComments().insert(channel->getComments().end(), comments.begin(), comments.end());
    
    // post validation
    for (list< shared_ptr<ChannelComment> >::iterator it = comments.begin(); it != comments.end(); it++ ) {
      if (! (*it)->hasGid()) {
        // 1. find the corresponding post in list of modified posts
        ChannelPost ref;
        ref.setId( (*it)->getPostId() );
        shared_ptr<ChannelPost> post = Utilities::findElementIn( posts, ref );
        assert( post );
      }
    }
    
    return channel;
  }

  shared_ptr<Channel> ChannelProfile::createExtendedChannel(shared_ptr<ChannelHeader> channelHeader,
                                                            list< shared_ptr<ChannelPost> > posts,
                                                            list< shared_ptr<ChannelComment> > comments,
                                                            const char* username)
  throw(data_access_error)
  {
    // update in/out argument
    shared_ptr<Channel> channel( shared_ptr<Channel>(new Channel(channelHeader->getId(), channelHeader->getGid(), channelHeader->isSecure()) ) );
    
    channel->setHeader( channelHeader );
    channel->getHeader( )->setAccessRole( this->headerRepository_.getAccessRole( channelHeader->getId(), username ) );

    EntityModification headerMetadata( this->headerRepository_.getEntityModification( channelHeader->getId() ) );
    channel->getHeader( )->setEntityModificationMask( headerMetadata.mask );
    channel->getHeader( )->setEntityModificationTime( headerMetadata.time );
    channel->getHeader( )->setEntityModificationBy( headerMetadata.by );
    channel->getHeader( )->setChannelIsOutdated( headerMetadata.isOutdated );
    EntityModification channelMetadata( this->headerRepository_.getChannelModification( channelHeader->getId() ) );
    channel->getHeader( )->setChannelModificationMask( channelMetadata.mask );
    channel->getHeader( )->setChannelModificationTime( channelMetadata.time );
    channel->getHeader( )->setChannelModificationBy( channelMetadata.by );
    channel->getHeader( )->setChannelIsOutdated( channelMetadata.isOutdated );
    
    {
      Value memberValue(channelHeader->getOwner());
      unique_ptr<Value> result( db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL) );
      channel->getHeader( )->setOwnerFullname(result->toString());
      memberValue = Value(channelHeader->getModifiedBy());
      result = db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL);
      channel->getHeader( )->setModifiedByFullname(result->toString());
    }
    
    // resolve current trust level
    for ( list< shared_ptr<ChannelMember> >::iterator channelMemberIt = channelHeader->getMembers().begin();
         channelMemberIt != channelHeader->getMembers().end();
         channelMemberIt++ ) {
      shared_ptr<Member> member( this->memberRepository_.loadByUsername( (*channelMemberIt)->getUsername().c_str() ) );
      if ( member ) {
        if ( strcmp(username, member->getUsername().c_str()) == 0 ) {
          (*channelMemberIt)->setTrustLevel(kTrustLevelApproved);
        } else {
          const Value userValue(username);
          const Value memberValue(member->getUsername());
          unique_ptr<Value> result( db_.valueOf(Member::TABLE_NAME, "trustLevel", &userValue, &memberValue, NULL) );
          if ( result.get() ) {
            (*channelMemberIt)->setTrustLevel((TrustLevel)result->num());
          }
        }
        const Value memberValue(member->getUsername());
        unique_ptr<Value> result( db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL) );
        (*channelMemberIt)->setFullname(result->toString());
      } else {
        (*channelMemberIt)->setFullname((*channelMemberIt)->getUsername().c_str());
        (*channelMemberIt)->setDeleted(true);
      }
    }

    // sum up all outdated media size
    const Value _channelId(channelHeader->getId());
    unique_ptr<Value> result( db_.valueOf(Media::TABLE_NAME, "outdatedChannelMediaSize", &_channelId,  NULL) );
    channelHeader->setTotalOutdatedMediaSize( result->num() );

    // copy set of comments to modify later
    list< shared_ptr<ChannelComment> > comments2( comments );

    // assign comments to corresponding post
    for ( list< shared_ptr<ChannelPost> >::iterator postIt = posts.begin();
          postIt != posts.end();
          postIt++ ) {
      EntityModification postMetadata = this->postRepository_.getEntityModification( (*postIt)->getId() );
      (*postIt)->setEntityModificationMask( postMetadata.mask );
      EntityModification postMetadata2 = this->postRepository_.getContainerModification( (*postIt)->getId() );
      (*postIt)->setContainerModificationMask( postMetadata2.mask );
      (*postIt)->setAccessRole( this->postRepository_.getAccessRole( (*postIt)->getId(), username ) );
      {
        Value memberValue((*postIt)->getOwner());
        unique_ptr<Value> result( db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL) );
        (*postIt)->setOwnerFullname(result->toString());
        memberValue = Value((*postIt)->getModifiedBy());
        result = db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL);
        (*postIt)->setModifiedByFullname(result->toString());
      }
      for ( list< shared_ptr<ChannelComment> >::iterator commentIt = comments2.begin();
            commentIt != comments2.end(); ) {
        if ( (*commentIt)->getPostId() == (*postIt)->getId() ) {
          EntityModification commentMetadata = this->commentRepository_.getEntityModification( (*commentIt)->getId() );
          (*commentIt)->setEntityModificationMask( commentMetadata.mask );
          (*commentIt)->setAccessRole( this->commentRepository_.getAccessRole( (*commentIt)->getId(), username ) );
          {
            Value memberValue((*commentIt)->getOwner());
            unique_ptr<Value> result( db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL) );
            (*commentIt)->setOwnerFullname(result->toString());
            memberValue = Value((*commentIt)->getModifiedBy());
            result = db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL);
            (*commentIt)->setModifiedByFullname(result->toString());
          }
          (*postIt)->getComments().push_back( (*commentIt) );
          commentIt = comments2.erase(commentIt);
        } else {
          ++commentIt;
        }
      }
    }
    
    // add all posts
    channel->getPosts().insert(channel->getPosts().end(), posts.begin(), posts.end());

    // add all remaining header comments
    for (list< shared_ptr<ChannelComment> >::iterator commentIt = comments2.begin();
         commentIt != comments2.end(); commentIt++ ) {
      EntityModification commentMetadata = this->commentRepository_.getEntityModification( (*commentIt)->getId() );
      (*commentIt)->setEntityModificationMask( commentMetadata.mask );
      (*commentIt)->setAccessRole( this->commentRepository_.getAccessRole( (*commentIt)->getId(), username ) );
      {
        Value memberValue((*commentIt)->getOwner());
        unique_ptr<Value> result( db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL) );
        (*commentIt)->setOwnerFullname(result->toString());
        memberValue = Value((*commentIt)->getModifiedBy());
        result = db_.valueOfWithDefault(Member::TABLE_NAME, "fullname", &memberValue, &memberValue, NULL);
        (*commentIt)->setModifiedByFullname(result->toString());
      }
      channel->getComments().push_back( (*commentIt) );
    }
  
    // post validation
    for (list< shared_ptr<ChannelComment> >::iterator it = comments.begin(); it != comments.end(); it++ ) {
      if (! (*it)->hasGid()) {
        // 1. find the corresponding post in list of modified posts
        ChannelPost ref;
        ref.setId( (*it)->getPostId() );
        shared_ptr<ChannelPost> post = Utilities::findElementIn( posts, ref );
        assert( post );        
      }
    }
    
    return channel;
  }
  
}  
