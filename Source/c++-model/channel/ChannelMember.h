/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_MEMBER_ENTITY_H__
#define __EJIN_CHANNEL_MEMBER_ENTITY_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "IBusinessData.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class ChannelHeaderProfile;
  class ChannelHeaderRepository;
  
  /**
   * Entity Class for CHANNEL_MEMBER_TBL
   */
  class ChannelMember: public BaseEntity, public IBusinessData, public DataSchema<ChannelMember> {
    friend class Database;
    friend class MemberProfile;
    friend class ChannelHeaderProfile;
    friend class ChannelHeaderRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "CHANNEL_MEMBER_TBL";

    // ctors
    ChannelMember( void );
    ChannelMember( const string& username );
    ChannelMember( const integer channelId, const string& username );
    ChannelMember( const ChannelMember& record );
    // dtor
    ~ChannelMember( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChannelMember& operator=( const ChannelMember& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ChannelMember(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool copyDataFrom( const BaseEntity& rhs );
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool hasContent() const { return false; }
    bool isEmpty( void ) const;
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );

    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( ChannelId, 1 )
    ACCESS_STRING_PROPERTY_INTF( Username, 2 )
    ACCESS_STRING_PROPERTY_INTF( Fullname, 3 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedBy, 4 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedByFullname, 5 )
    ACCESS_STRING_PROPERTY_INTF( SessionKey, 6 )
    ACCESS_STRING_PROPERTY_INTF( FingerPrint, 7 )
    ACCESS_STRING_PROPERTY_INTF( Tags, 8 )
    ACCESS_INTEGER_PROPERTY_INTF( Role, 9 )
    ACCESS_INTEGER_PROPERTY_INTF( Membership, 10 )
    ACCESS_TIME_PROPERTY_INTF( CreateTime, 11 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 12 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 13 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 14 )
    ACCESS_INTEGER_PROPERTY_INTF( ModificationMask, 15 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Secure, 16 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Encrypted, 17 )
    ACCESS_STRING_PROPERTY_INTF( IV, 18 )
    bool isHidden() { return this->getMasterId() > 0; }
    
    // ==========
    // Metadata
    // ==========
    integer getHierarchyModificationMask( void ) const { return this->getModificationMask( ); }
    void setHierarchyModificationMask( integer mask ) { this->setModificationMask( mask ); }
    
    TrustLevel getTrustLevel( void ) const { return this->trustLevel_; }
    void setTrustLevel( TrustLevel level ) { this->trustLevel_ = level; }
    
    bool isDeleted( void ) const { return this->deleted_; }
    void setDeleted( bool deleted ) { this->deleted_ = deleted; }

    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Member Variables
    
    TrustLevel   trustLevel_;
    string       fullname_;
    bool         deleted_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif // __EJIN_CHANNEL_MEMBER_ENTITY_H__
