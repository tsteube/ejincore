/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelCommentRepository.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "ChannelComment.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelMedia.h"
#include "ChannelMediaRepository.h"
#include "ChangeLogRepository.h"
#include "SqliteValue.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool ChannelCommentRepository::detectConflict( const string& commentGid ) const
  throw(data_access_error)
  {
    assert (! commentGid.empty());
    
    return (bool)this->loadChannelCommentInternal( Value(commentGid.c_str()), Value(true) );
  }
  
  bool ChannelCommentRepository::clearConflict( const string& commentGid )
  throw(data_access_error)
  {
    assert (! commentGid.empty());
    shared_ptr<ChannelComment> channelCommentEntity( this->loadChannelCommentInternal( Value(commentGid.c_str()), Value(false) ) );
    shared_ptr<ChannelComment> conflict( this->loadChannelCommentInternal( Value(commentGid.c_str()), Value(true) ) );
    
    bool cleared = false;
    if ( conflict ) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      if ( channelCommentEntity ) {
        if ( channelCommentEntity->isEmpty() ) {
          this->db_.remove( *channelCommentEntity );
        } else {
          channelCommentEntity->setSyncInd( kSyncIndSynchronous );
          channelCommentEntity->setModificationMask( kNoneModification );
          this->db_.update( *channelCommentEntity );
        }
      }
      cleared = true;
    }
    return cleared;
  }
  
  bool ChannelCommentRepository::update( ChannelHeader& channelHeader, list< shared_ptr<ChannelPost> > channelPosts, list< shared_ptr<ChannelComment> > channelComments, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasId() );
    assert( channelHeader.hasOwner() );
    
    if (channelComments.empty())
      return false;
    
    // call entity finder
    Value hidden(false);
    Value channelId(channelHeader.getId());
    unique_ptr<ResultSet> allChannelPostEntities(db_.find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelCommentEntities(db_.find(ChannelComment::TABLE_NAME, "findAllCommentsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelMediaEntities( db_.find(ChannelMedia::TABLE_NAME, "findAllChannelResources", &channelId, &hidden, NULL) );
    
    // update algorithm
    bool updated = false;
    unique_ptr<ChannelComment> clone;
    list< shared_ptr<ChannelComment> > channelCommentsToUpdate( channelComments );
    
    for (vector< BaseEntity* >::iterator it=allChannelCommentEntities->begin(); it != allChannelCommentEntities->end(); it++) {
      ChannelComment* channelCommentEntity(static_cast<ChannelComment*>(*it));
      
      shared_ptr<ChannelComment> channelComment = Utilities::findElementIn (channelCommentsToUpdate, *channelCommentEntity);
      if ( ! channelComment )
      {
        // no change request on channel comment
        continue;
      }
      assert( channelComment->hasGid() );
      if ( channelComment->getOperation() == kSyncRemove )  {
        channelCommentEntity->setModifiedBy( channelComment->getModifiedBy() );
        // delete comment entity
        if ( removeChannelComment( *channelCommentEntity, allChannelMediaEntities.get() ) ) {
          this->clearDeleteOwnerInd( *channelCommentEntity );
          this->changeLogRepository_.remove( *channelCommentEntity, channelHeader.getSyncAnchor(), syncTime );
          updated = true;
        }
      } else {
        // update comment entity
        if ( updateChannelComment( *channelCommentEntity, *channelComment, allChannelMediaEntities.get() ) ) {
          this->clearDeleteOwnerInd( *channelCommentEntity );
          this->changeLogRepository_.update( *channelCommentEntity, channelHeader.getSyncAnchor(), syncTime );
          updated = true;
        }
      }
      channelCommentsToUpdate.remove(channelComment);
    }
    
    if ( !channelCommentsToUpdate.empty() )
    {
      for (list< shared_ptr<ChannelComment> >::iterator commentIt = channelCommentsToUpdate.begin(); commentIt != channelCommentsToUpdate.end(); commentIt++) {
        // map post global identifier to local post identifier
        if ( (*commentIt)->hasPostGid() ) {
          ChannelPost post( (*commentIt)->getPostGid() );
          ChannelPost* channelPost = Utilities::findElementIn( *allChannelPostEntities, post );
          if ( ! channelPost ) {
            continue; // post has already been deleted before
          }
          (*commentIt)->setPostId( channelPost->getId() );
        }
        (*commentIt)->setChannelId( channelHeader.getId() );
        shared_ptr<ChannelComment> channelCommentEntity( addChannelComment( **commentIt, allChannelMediaEntities.get(), channelHeader.getSyncAnchor() ) );
        if ( channelCommentEntity ) {
          this->clearDeleteOwnerInd( *channelCommentEntity );
          if (channelHeader.hasLastSyncTime()) {
            // no tracking on first sync
            this->changeLogRepository_.add( *channelCommentEntity, channelHeader.getSyncAnchor(), syncTime );
          }
          updated = true;
        }
      }
    }

    return updated;
  }
  void ChannelCommentRepository::deleteAllPostComments( integer channel_Id, integer postId )
  throw(data_access_error)
  {
    assert( channel_Id > 0 );
    assert( postId > 0 );
    
    // call entity finder
    Value channelId(channel_Id);
    Value hidden(false);
    unique_ptr<ResultSet> allChannelCommentEntities(db_.find(ChannelComment::TABLE_NAME, "findAllCommentsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelCommentMediaEntities( db_.find(ChannelMedia::TABLE_NAME, "findAllChannelCommentResources", &channelId, &hidden, NULL) );
    
    for (vector< BaseEntity* >::iterator it = allChannelCommentEntities->begin(); it != allChannelCommentEntities->end(); it++) {
      ChannelComment* commentEntity(static_cast<ChannelComment*>(*it));
      if (commentEntity->hasPostId() && commentEntity->getPostId() == postId) {
        removeChannelComment( *commentEntity, allChannelCommentMediaEntities.get() );
      }
    }
    
  }
  
  void ChannelCommentRepository::commit( ChannelHeader& channelHeader, list< shared_ptr<ChannelComment> > channelComments, const string& syncAnchor, const string& username, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelHeader.hasId() );
    assert( ! syncAnchor.empty() );
    
    if (channelComments.empty())
      return;
    
    // call entity finder
    Value hidden(false);
    Value channelId(channelHeader.getId());
    unique_ptr<ResultSet> allChannelCommentEntities(db_.find(ChannelComment::TABLE_NAME, "findAllCommentsByChannel", &channelId, &hidden, NULL));
    unique_ptr<ResultSet> allChannelCommentMediaEntities( db_.find(ChannelMedia::TABLE_NAME, "findAllChannelCommentResources", &channelId, &hidden, NULL) );
    
    for (vector< BaseEntity* >::iterator it=allChannelCommentEntities->begin(); it != allChannelCommentEntities->end(); it++) {
      ChannelComment* channelCommentEntity(static_cast<ChannelComment*>(*it));
      
      shared_ptr<ChannelComment> comment = Utilities::findElementIn (channelComments, *channelCommentEntity);
      if ( ! comment )
      {
        // no change request on channel comment
        continue;
      }
      
      assert( comment->hasId() );
      assert( comment->hasGid() );
      channelCommentEntity->setModifyTime( syncTime );
      SyncInd syncInd = (SyncInd) channelCommentEntity->getSyncInd();
      switch ( syncInd )
      {
        case kSyncIndSynchronous:
          channelCommentEntity->setModifiedBy( username );
          this->db_.update( *channelCommentEntity );
          break;
        case kSyncIndConflict:
          channelCommentEntity->setModificationMask( kChannelCommentConflictedModification );
          this->db_.update( *channelCommentEntity );
          this->changeLogRepository_.update( *channelCommentEntity, syncAnchor, syncTime );
          break;
        case kSyncIndInsert:
          // update global identifier
          channelCommentEntity->setGid( comment->getGid() );
          channelCommentEntity->setCreateTime( syncTime );
          channelCommentEntity->setSecure( comment->isSecure() );
        case kSyncIndUpdate:
          channelCommentEntity->setIV( comment->getIV() );
          channelCommentEntity->setSyncInd( kSyncIndSynchronous );
          channelCommentEntity->setModifiedBy( username );
          channelCommentEntity->setModificationMask( kNoneModification );
          this->db_.update( *channelCommentEntity );
          if ( syncInd == kSyncIndInsert ) {
            this->changeLogRepository_.add( *channelCommentEntity, syncAnchor, syncTime );
          } else {
            this->changeLogRepository_.update( *channelCommentEntity, syncAnchor, syncTime );
          }
          break;
        case kSyncIndDelete:
          channelCommentEntity->setModifiedBy( username );
          this->removeChannelComment( *channelCommentEntity, allChannelCommentMediaEntities.get() );
          this->changeLogRepository_.remove( *channelCommentEntity, syncAnchor, syncTime );
          continue;
      }
      
      // commit resources
      for (vector< BaseEntity* >::iterator mediaEntityIt=allChannelCommentMediaEntities->begin(); mediaEntityIt != allChannelCommentMediaEntities->end(); mediaEntityIt++) {
        
        ChannelMedia* mediaEntity(dynamic_cast<ChannelMedia*>(*mediaEntityIt)); // up cast
        if (mediaEntity->hasCommentId() && channelCommentEntity->getId() == mediaEntity->getCommentId()) {
          shared_ptr<Resource> media = Utilities::findElementIn (comment->getResources(),
                                                                 *static_cast<Resource*>(mediaEntity)); // down cast
          //assert( media.get() );
          this->channelMediaRepository_.commit( *mediaEntity, dynamic_cast<ChannelMedia*>(media.get()),
                                               syncAnchor, username, syncTime );
        }
      }
    }
  }
  
  bool ChannelCommentRepository::revert( const string& gid, bool force )
  throw(data_access_error)
  {
    assert( ! gid.empty() );
    
    bool reverted = false;
    Value gidValue(gid.c_str());
    shared_ptr<ChannelComment> channelCommentEntity( this->loadChannelCommentInternal( gidValue, Value(false) ) );
    if ( channelCommentEntity != NULL )
    {
      // ensure that there a no pending conflicts
      if ( clearConflict( gid ) ) {
        // reload after conflict has been cleared
        channelCommentEntity = this->loadChannelCommentInternal( gidValue, Value(false) );
      }
      
      switch ( channelCommentEntity->getSyncInd() ) {
        case kSyncIndInsert:
        {
          if (force) {
            db_.remove( *channelCommentEntity );
            reverted = true;
          } else {
            cloneChannelComment(*channelCommentEntity, NULL);
            reverted = true;
          }
          break;
        }
        case kSyncIndSynchronous:
        case kSyncIndUpdate:
        case kSyncIndDelete:
        {
          // load entity from backup
          shared_ptr<ChannelComment> channelCommentEntityBak( shared_ptr<ChannelComment>(new ChannelComment()) );
          channelCommentEntityBak->setId(channelCommentEntity->getId());
          db_.refresh(*channelCommentEntityBak, channelCommentEntityBak->backupSchemaName() );
          
          if ( force ) {
            // force update entity with data from backup
            channelCommentEntity->copyDataFrom( *channelCommentEntityBak );
            channelCommentEntity->setSyncInd( kSyncIndSynchronous );
            channelCommentEntity->setModificationMask( kNoneModification );
            this->db_.update( *channelCommentEntity );
            reverted = true;
          } else {
            reverted = cloneChannelComment(*channelCommentEntity,
                                           channelCommentEntityBak.get() );
          }
          break;
        }
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "comment[id=%d] in conflict state", channelCommentEntity->getId());
      }
      
      // revert new dependent entities
      if (channelMediaRepository_.revertNewMedias( channelCommentEntity->getChannelId(), channelCommentEntity->getPostId(), channelCommentEntity->getId(), force )) {
        reverted = true;
      }
      
    } else {
      // restore old member if found in backup
      unique_ptr<ResultSet> values( db_.find(ChannelComment::TABLE_NAME, "findCommentByGidBak", &gidValue, NULL) );
      vector< BaseEntity* >::iterator it = values->begin();
      if (it != values->end()) {
        this->db_.insert( **it );
        reverted = true;
      }
      
    }
    return reverted;
  }
  
  bool ChannelCommentRepository::revertNewComments( integer channelId, integer postId, bool force )
  throw(data_access_error)
  {
    unique_ptr<ResultSet> values;
    if ( postId > 0 ) {
      Value id(postId);
      Value hidden(false);
      values = db_.find(ChannelComment::TABLE_NAME, "findCommentsByPost", &id, &hidden, NULL);
    } else {
      Value id(channelId);
      Value hidden(false);
      values = db_.find(ChannelComment::TABLE_NAME, "findAllCommentsByChannel", &id, &hidden, NULL);
    }
    bool reverted = false;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); it++) {
      ChannelComment* channelCommentEntity(static_cast< ChannelComment* >(*it));
      if ( channelCommentEntity->getSyncInd() == kSyncIndInsert ) {
        if (force) {
          db_.remove( *channelCommentEntity );
          reverted = true;
        } else {
          cloneChannelComment(*channelCommentEntity, NULL);
          reverted = true;
        }
      }
    }
    return reverted;
  }
  
  bool ChannelCommentRepository::deleteNewComment( integer id )
  throw(data_access_error)
  {
    bool deleted = false;
    
    Value postId( id );
    unique_ptr<ResultSet> values( db_.find(ChannelComment::TABLE_NAME, "findCommentById", &postId, NULL) );
    vector< sqlite::BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      shared_ptr<ChannelComment> channelCommentEntity( shared_ptr<ChannelComment>(static_cast< ChannelComment* >(*it)) );
      it = values->take(it);
      if ( !channelCommentEntity->hasGid() ) {
        db_.remove( *channelCommentEntity );
        deleted = true;
      }
    }
    return deleted;
  }  

  // ==========
  // Private Interface
  // ==========
  
  shared_ptr<ChannelComment> ChannelCommentRepository::addChannelComment(ChannelComment& channelComment,
                                                                         ResultSet* allChannelMediaEntities,
                                                                         const string& syncAnchor )
  throw(data_access_error)
  {
    assert( channelComment.hasGid() );
    if ( channelComment.isEmpty() ) {
      return shared_ptr<ChannelComment>();
    }
    
    // create new entity with unique global key
    shared_ptr<ChannelComment> channelCommentEntity = shared_ptr<ChannelComment>(new ChannelComment( channelComment ));
    channelCommentEntity->clearId(); // generate new local database identifier
    channelCommentEntity->setSyncInd( kSyncIndSynchronous );
    channelCommentEntity->setModifiedBy( channelComment.getOwner() );
    channelCommentEntity->setContentModifyTime( channelComment.getModifyTime() );
    channelCommentEntity->setModificationMask( kChannelCommentRecentRemoteModification );
    channelCommentEntity->getResources().clear(); // will be created later
    
    // persist entity
    this->db_.insert( *channelCommentEntity );
    
    // set id
    channelComment.setId( channelCommentEntity->getId() );
    channelComment.setSyncInd( channelCommentEntity->getSyncInd() );
    channelComment.setModifiedBy( channelCommentEntity->getModifiedBy() );

    updateResources( channelComment, allChannelMediaEntities );
    
    return channelCommentEntity;
  }
  
  bool ChannelCommentRepository::updateChannelComment( ChannelComment& channelCommentEntity, ChannelComment& channelComment, ResultSet* allChannelCommentMediaEntities )
  throw(data_access_error)
  {
    assert( channelCommentEntity.hasId() );
    assert( ! channelCommentEntity.isHidden() );
    assert( channelComment.hasGid() );
    
    bool result = false;
    switch ( channelCommentEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // must be unknown on the server
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of ChannelComment[id=%d]", channelCommentEntity.getId());
      case kSyncIndSynchronous:
        // entity was updated on the server
        if ( channelComment.hasContent() && ! channelCommentEntity.hasEqualContent( channelComment ) ) {
          channelCommentEntity.setModificationMask( kChannelCommentRecentRemoteModification );
          result = true;
        }
        channelCommentEntity.copyDataFrom( channelComment );
        this->db_.update( channelCommentEntity );
        break;
      case kSyncIndUpdate:
        if (channelComment.hasContent()) {
          if ( channelCommentEntity.hasEqualContent( channelComment ) )
          {
            // update only some dates and state data
            channelCommentEntity.copyDataFrom( channelComment );
            channelCommentEntity.setSyncInd( kSyncIndSynchronous );
            channelCommentEntity.setModificationMask( kNoneModification );
            this->db_.update( channelCommentEntity );
            result = true;
          }
          else
          {
            // conflict detected; save local entity for reference
            cloneChannelComment( channelCommentEntity, &channelComment );
            result = true;
          }
        }
        break;
      case kSyncIndDelete:
        if ( channelCommentEntity.hasEqualContent( channelComment ) )
        {
          // no change on server side but removed on client
        }
        else
        {
          // conflict detected; save local entity for reference
          cloneChannelComment( channelCommentEntity, &channelComment );
          result = true;
        }
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelComment> conflict( this->loadChannelCommentInternal( Value(channelCommentEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelCommentEntity.getId());
        }
        if ( conflict->hasEqualContent( channelComment ) )
        {
          // resolve conflict
          switch ( conflict->getSyncInd() ) {
            case kSyncIndSynchronous:
            case kSyncIndConflict:
              _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[commentId=%d]", channelCommentEntity.getId());
            case kSyncIndInsert:
            case kSyncIndUpdate:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // client and server are in sync now again
              // update only some dates and state data
              channelCommentEntity.copyDataFrom( channelComment );
              channelCommentEntity.setSyncInd( kSyncIndSynchronous );
              channelCommentEntity.setModificationMask( kNoneModification );
              this->db_.update( channelCommentEntity );
              result = true;
              break;
            case kSyncIndDelete:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // remember that the member was already deleted on the client
              channelCommentEntity.setSyncInd( kSyncIndDelete );
              this->db_.update( channelCommentEntity );
              result = true;
              break;
          }
        } else {
          // keep original local conflict data
          // and mark entity as removed from server
          channelCommentEntity.copyDataFrom( channelComment );
          channelCommentEntity.setSyncInd( kSyncIndConflict );
          this->db_.update( channelCommentEntity );
          result = true;
        }
        break;
    }
    
    // update id
    channelComment.setId( channelCommentEntity.getId() );
    channelComment.setChannelId( channelCommentEntity.getChannelId() );
    if ( channelCommentEntity.hasPostId() ) {
      channelComment.setPostId( channelCommentEntity.getPostId() );
    }
    
    // check for ordering changes first
    if (allChannelCommentMediaEntities) {
      if (! Utilities::hasEqualOrdering( channelComment.getResources(), *allChannelCommentMediaEntities )) {
        channelCommentEntity.setModificationMask( channelCommentEntity.getModificationMask() | kChannelCommentRecentRemoteModification );
        this->db_.update( channelCommentEntity );
      }
    }
    
    // update resources
    updateResources( channelComment, allChannelCommentMediaEntities );
    
    return result;
  }
  
  bool ChannelCommentRepository::removeChannelComment( ChannelComment& channelCommentEntity, ResultSet* allChannelCommentMediaEntities )
  throw(data_access_error)
  {
    assert( channelCommentEntity.hasId() );
    assert( ! channelCommentEntity.isHidden() );
    
    switch ( channelCommentEntity.getSyncInd() ) {
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        cloneChannelComment( channelCommentEntity, NULL );
        break;
      case kSyncIndInsert:
        // new comment on client
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        deleteChannelComment( channelCommentEntity, allChannelCommentMediaEntities );
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelComment> conflict( this->loadChannelCommentInternal( Value(channelCommentEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[commentId=%d]", channelCommentEntity.getId());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndConflict:
          case kSyncIndInsert:
            _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[commentId=%d]", channelCommentEntity.getId());
          case kSyncIndDelete:
            // entity was removed on client and server
            this->db_.remove( *conflict );
            deleteChannelComment( channelCommentEntity, allChannelCommentMediaEntities );
            break;
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            // and mark entity as removed from server
            channelCommentEntity.clearData();
            channelCommentEntity.setSyncInd( kSyncIndConflict );
            this->db_.update( channelCommentEntity );
            break;
        }
        break;
    }
    return true;
  }
  
  void ChannelCommentRepository::deleteChannelComment( ChannelComment& channelCommentEntity, ResultSet* allChannelCommentMediaEntities )
  throw(data_access_error)
  {
    // 1. delete resources
    channelCommentEntity.getResources().clear();
    this->updateResources( channelCommentEntity, allChannelCommentMediaEntities );
    
    // 2. delete entity itself
    this->db_.remove( channelCommentEntity );
  }
  
  bool ChannelCommentRepository::cloneChannelComment( ChannelComment& channelCommentEntity, const ChannelComment* channelComment )
  throw(data_access_error)
  {
    if ( channelComment == NULL || ! channelCommentEntity.hasEqualContent( *channelComment ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<ChannelComment> clone = unique_ptr<ChannelComment>(new ChannelComment( channelCommentEntity ));
      clone->clearId( );
      clone->setMasterId( channelCommentEntity.getId() );
      clone->setModificationMask( clone->getModificationMask()|kChannelCommentConflictedModification );
      
      // update only some dates and states
      if ( channelComment )
      {
        // update dates and states
        channelCommentEntity.copyDataFrom( *channelComment );
      }
      else
      {
        // mark entity as removed from server
        channelCommentEntity.clearData();
      }
      channelCommentEntity.setSyncInd( kSyncIndConflict );
      channelCommentEntity.setModificationMask( kChannelCommentConflictedModification|kChannelCommentRecentRemoteModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( channelCommentEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      return true;
    }
    return false;
  }
  
  bool ChannelCommentRepository::updateResources( ChannelComment& channelComment, ResultSet* allChannelCommentEntities )
  throw(data_access_error)
  {
    bool updated = false;
    list< shared_ptr<Resource> > resources(channelComment.getResources());
    if (allChannelCommentEntities) {
      for (vector< BaseEntity* >::iterator it = allChannelCommentEntities->begin(); it != allChannelCommentEntities->end(); it++ ) {
        ChannelMedia* mediaEntity(static_cast<ChannelMedia*>(*it));
        
        shared_ptr<Resource> media = Utilities::findElementIn (resources, *static_cast<Resource*>(mediaEntity));
        if ( media ) {
          // ensure correct relationship
          media->setChannelId( channelComment.getChannelId() );
          if ( channelComment.hasPostId() )
            media->setPostId( channelComment.getPostId() );
          media->setCommentId( channelComment.getId() );
          media->setModifiedBy( channelComment.getModifiedBy() );
          media->setNo( Utilities::indexOf (channelComment.getResources(), *static_cast<Resource*>(media.get()) ) );
          
          // update membership
          this->channelMediaRepository_.update( *mediaEntity, *dynamic_cast<ChannelMedia*>(media.get()) );
          updated = true;
          
          resources.remove(media);
        } else {
          // resources gets deleted on media list sync later
        }
      }
    }
    
    for (list< shared_ptr<Resource> >::iterator mediaIt = resources.begin(); mediaIt != resources.end(); mediaIt++ ) {
      // ensure correct relationship
      (*mediaIt)->setChannelId( channelComment.getChannelId() );
      if ( channelComment.hasPostId() )
        (*mediaIt)->setPostId( channelComment.getPostId() );
      (*mediaIt)->setCommentId( channelComment.getId() );
      (*mediaIt)->setModifiedBy( channelComment.getModifiedBy() );
      (*mediaIt)->setNo( Utilities::indexOf (channelComment.getResources(), **mediaIt) );
      
      // add media
      this->channelMediaRepository_.add( *dynamic_cast<ChannelMedia*>((*mediaIt).get()) );
      updated = true;
    }
    
    return updated;
  }
  
  void ChannelCommentRepository::clearDeleteOwnerInd( ChannelComment& channelCommentEntity )
  throw(data_access_error)
  {
    // reset delete marker of owner entites
    const Value channelId(channelCommentEntity.getChannelId());
    this->db_.valueOf(ChannelHeader::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
    if ( channelCommentEntity.hasPostId() ) {
      const Value postId(channelCommentEntity.getPostId());
      this->db_.valueOf(ChannelPost::TABLE_NAME, "clearDeleteSyncInd", &postId, NULL);
    }
  }
  
}

