/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelCommentProfile.h"
#include "UsingEjinTypes.h"

#include "ChannelHeader.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "Media.h"
#include "SqliteValue.h"
#include "SqliteResultSet.h"
#include "ChannelFullText.h"

namespace ejin
{
  static bool compare_no (shared_ptr<Resource> first, shared_ptr<Resource> second);
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<ChannelComment> > ChannelCommentProfile::loadAll( const ChannelHeader& channel, ResourceType resourceType  ) const
  throw(data_access_error)
  {
    return this->load( channel, list<integer>(), resourceType );
  }
  
  list< shared_ptr<ChannelComment> > ChannelCommentProfile::load( const ChannelHeader& channel, const list<integer>& channelCommentIds, ResourceType resourceType ) const
  throw(data_access_error)
  {
    assert( channel.hasId() );
    assert( channel.hasOwner() );
    
    // load all comments of the given channel
    list< shared_ptr<ChannelComment> > comments(this->resolveAllChannelCommentInternal( Value(channel.getId()), channelCommentIds, resourceType ));
    return comments;
  }

  EntityModification ChannelCommentProfile::getEntityModification( integer commentId ) const throw(data_access_error)
  {
    const Value _commentId(commentId);
    unique_ptr<Value> result( db_.valueOf(ChannelComment::TABLE_NAME, "entityModificationMask", &_commentId, NULL) );
    integer mask = result->num();
    result = db_.valueOf(ChannelComment::TABLE_NAME, "entityModificationTime", &_commentId, NULL);
    jtime modifyTime( result->date() );
    string modifyBy = ""; // TODO
    return EntityModification( mask, modifyTime, modifyBy, false );
  }
  
  MemberRole ChannelCommentProfile::getAccessRole( integer commentId, const char* username ) const throw(data_access_error)
  {
    const Value _commentId(commentId);
    const Value _username(username);
    unique_ptr<Value> result( db_.valueOf(ChannelComment::TABLE_NAME, "accessRole", &_commentId, &_username, NULL) );
    return (MemberRole)result->num();
  }
  
  list<integer> ChannelCommentProfile::searchFullText( const string& pattern )
  throw(data_access_error)
  {
    // call entity finder
    Value regex( pattern );
    Value source( "CHANNEL_COMMENT" );
    unique_ptr<ResultSet> values( this->db_.find(ChannelFullText::TABLE_NAME, "searchEntityFullText", &source, &regex, NULL) );
    list< integer > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      ChannelFullText* channelFullText = static_cast< ChannelFullText* >( *it );
      result.push_back(channelFullText->getEntityId());
    }
    return result;
  }
  
  // ==========
  // Protected Interface
  // ==========
  
  list< shared_ptr<ChannelComment> > ChannelCommentProfile::resolveAllChannelCommentInternal( const Value& channelId, const list<integer>& list, ResourceType resourceType ) const
  throw(data_access_error)
  {
    Value hidden(false);
    std::list< shared_ptr<ChannelComment> > result;
    
    // select entity finder
    unique_ptr<ResultSet> channelResources;
    switch (resourceType) {
      case kNoResource:
        break;
      case kResourceTypeMedia:
        channelResources = db_.find(Media::TABLE_NAME, "findAllChannelCommentMedias", &channelId, NULL);
        break;
      case kResourceTypeChannelMedia:
        channelResources = db_.find(ChannelMedia::TABLE_NAME, "findAllChannelCommentResources", &channelId, &hidden, NULL);
        break;
    }
    
    if (list.empty()) {
      // load all channel comments of the specified channel
      
      unique_ptr<ResultSet> values( db_.find(ChannelComment::TABLE_NAME, "findAllCommentsByChannel", &channelId, &hidden, NULL) );
      for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
        result.push_back(shared_ptr<ChannelComment>(static_cast< ChannelComment* >(*it)));
        it = values->take(it);
      }
      
    } else {
      // load individual list of channel comments by identifier list
      
      shared_ptr<ChannelComment> comment;
      for (std::list<integer>::const_iterator it=list.begin(); it != list.end(); it++) {
        comment = shared_ptr<ChannelComment>(new ChannelComment());
        comment->setId(*it);
        // refresh from database
        db_.refresh(*comment);
        // add to result list
        result.push_back(comment);
      }
      
    }
    
    // merge resources into channel comment resource list
    Resource *channelResource;
    for (std::list< shared_ptr<ChannelComment> >::iterator it1=result.begin(); it1 != result.end(); it1++) {
      if ( channelResources.get() != NULL) {
        for (vector< BaseEntity* >::iterator it2 = channelResources->begin(); it2 != channelResources->end(); ) {
          channelResource = static_cast< Resource* >(*it2);
          if (channelResource->hasCommentId() && channelResource->getCommentId() == (*it1)->getId()) {
            switch (channelResource->getSyncInd()) {
              case kSyncIndSynchronous:
              case kSyncIndInsert:
              case kSyncIndUpdate:
              case kSyncIndConflict:
                (*it1)->getResources().push_back(shared_ptr<Resource>(channelResource));
                it2 = channelResources->take(it2);
                break;
              case kSyncIndDelete:
                it2++;
                break;
            }
          } else {
            it2++;
          }
        }
        // sort resource list
        (*it1)->getResources().sort( compare_no );
      }
    }
    
    return result;
  }
  
  shared_ptr<ChannelComment> ChannelCommentProfile::loadChannelCommentInternal( const Value& commentGid, const Value& hidden ) const throw(data_access_error) 
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(ChannelComment::TABLE_NAME, "findCommentByGid", &commentGid, &hidden, NULL) );
    
    shared_ptr<ChannelComment> comment;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      comment = shared_ptr<ChannelComment>(static_cast< ChannelComment* >(*it));
      it = values->take(it);
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate channel comment [gid=%d].",  commentGid.num());  
    
    return comment;
  }
  
  bool compare_no (shared_ptr<Resource> first, shared_ptr<Resource> second)
  {
    return first->getNo() < second->getNo();
  }
  
}
