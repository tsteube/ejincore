/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_COMMENT_REPOSITORY_H__
#define __EJIN_CHANNEL_COMMENT_REPOSITORY_H__

#include "ChannelCommentProfile.h"

namespace ejin
{

  /**
   * Update operations on the channelComment entity. There are a create, update and delete operation to manages channelComments bound to a
   * channel instance.
   */
  class ChannelCommentRepository: public ChannelCommentProfile {

    // ==========
    // Public Interface
    // ==========
  public:

    // ctor
    ChannelCommentRepository( Database& db, ChangeLogRepository& changeLogRepository, ChannelMediaRepository& channelMediaRepository ):
    ChannelCommentProfile(db), channelMediaRepository_(channelMediaRepository), changeLogRepository_(changeLogRepository) {}
    // dtor
    ~ChannelCommentRepository( void ) { };

    /**
     * Returns true if the specified comment instance has been synchronized with the server reference entity although
     * the entity contains unsynchronized local changes.
     */
    bool detectConflict( const string& commentGid ) const
    throw(data_access_error);

    /**
     * Revert local changes to synchronize the comment entity with the server reference.
     */
    bool clearConflict( const string& commentGid )
    throw(data_access_error);

    /**
     * Update the specified channel comment instances in the repository. This method will compare all current channel
     * comments of the specified channel identifier and updates its content by the given reference data.
     */
    bool update( ChannelHeader& channelHeader, list< shared_ptr<ChannelPost> > channelPosts, list< shared_ptr<ChannelComment> > channelComments, const jtime& syncTime  )
    throw(data_access_error);

    /**
     * Delete all comments and resources of the specified channel identifier.
     */
    void deleteAllPostComments( integer channelId, integer postId )
    throw(data_access_error);

    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */
    void commit( ChannelHeader& channelHeader, list< shared_ptr<ChannelComment> > channelComments, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Restore original channel comment instance
     * and mark local changes as conflicted data set.
     */
    bool revert( const string& gid, bool force = false )
    throw(data_access_error);
    
    /**
     * Revert all new local post instances of the given comment identifier.
     */
    bool revertNewComments( integer channelId, integer postId, bool force )
    throw(data_access_error);
    
    /**
     * Remove new channel comment
     */
    bool deleteNewComment( integer commentId )
    throw(data_access_error);

    // ==========
    // Private Interface
    // ==========
  private:

    ChannelMediaRepository& channelMediaRepository_;
    ChangeLogRepository& changeLogRepository_;

    /*
     * Add a new channel channel entity to the persistent store
     */
    shared_ptr<ChannelComment> addChannelComment( ChannelComment& channelComment, sqlite::ResultSet* allChannelMediaEntities, const string& syncAnchor )
    throw(data_access_error);

    /*
     * Updates a channel comment entity in the persistent store
     */
    bool updateChannelComment( ChannelComment& channelCommentEntity, ChannelComment& channelComment, sqlite::ResultSet* allChannelCommentMediaEntities )
    throw(data_access_error);

    /*
     * Removes a channel comment entity from the persistent store
     */
    bool removeChannelComment( ChannelComment& channelCommentEntity, sqlite::ResultSet* allChannelCommentMediaEntities )
    throw(data_access_error);

    /*
     * Removes a channel comment entity from the persistent store
     */
    void deleteChannelComment( ChannelComment& commentEntity, sqlite::ResultSet* allChannelCommentMediaEntities )
    throw(data_access_error);

    /*
     * Clone channel comment data to save local data on conflict
     */
    bool cloneChannelComment( ChannelComment& channelCommentEntity, const ChannelComment* channelComment )
    throw(data_access_error);

    /*
     * Update channel resources
     */
    bool updateResources( ChannelComment& comment, sqlite::ResultSet* allChannelCommentMediaEntities )
    throw(data_access_error);

    /*
     * Reset delete marker on owner entites
     */
    void clearDeleteOwnerInd( ChannelComment& channelCommentEntity )
    throw(data_access_error);

  };

}

#endif // __EJIN_CHANNEL_COMMENT_REPOSITORY_H__
