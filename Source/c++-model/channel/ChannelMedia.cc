/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMedia.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "SqliteValue.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::tag;
  using ser::attr;
  using ser::chardata;
  using ser::endtag;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<ChannelMedia>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("GID", type_text, flag_not_null),
    Attribute("NO", type_int, flag_not_null),
    Attribute("CHANNEL_ID", type_int, flag_none),
    Attribute("POST_ID", type_int, flag_none),
    Attribute("COMMENT_ID", type_int, flag_none),
    Attribute("OWNER", type_text, flag_not_null),
    Attribute("OWNER_FULLNAME", type_text, flag_not_null),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_ANCHOR", type_text, flag_not_null),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient),
    Attribute("ATTACH_OUTDATED", type_bool, flag_not_null|flag_transient),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute("SELECTED", type_bool, flag_none),
    Attribute("MODIFIED_BY", type_text, flag_none),
    Attribute("MODIFIED_BY_FULLNAME", type_text, flag_none),
    Attribute("SECURE", type_bool, flag_not_null, Value(false)),
    Attribute("ENCRYPTED", type_bool, flag_not_null, Value(false)),
    Attribute("IV", type_text, flag_none),
    Attribute(),
  };
  template <> const AttributeSet DataSchema<ChannelMedia>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChannelMedia::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findChannelMediaByGid"] = "GID = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["findChannelResources"] = "CHANNEL_ID = ?";
      tMap["findAllChannelResources"] = "CHANNEL_ID = ? AND (MASTER_ID IS NOT NULL) = ? ORDER BY NO";
      tMap["findChannelHeaderResources"] = "CHANNEL_ID = ? AND POST_ID IS NULL AND COMMENT_ID IS NULL AND NO >= 0 AND (MASTER_ID IS NOT NULL) = ? ORDER BY NO";
      tMap["findAllChannelPostResources"] = "CHANNEL_ID = ? AND POST_ID IS NOT NULL AND COMMENT_ID IS NULL AND (MASTER_ID IS NOT NULL) = ? ORDER BY NO";
      tMap["findAllChannelCommentResources"] = "CHANNEL_ID = ? AND COMMENT_ID IS NOT NULL AND (MASTER_ID IS NOT NULL) = ? ORDER BY NO";
      tMap["BackupSet"] = "CHANNEL_ID = ?1 AND GID IS NOT NULL AND MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["BackupSetBak"] = "CHANNEL_ID = ?1 AND GID IS NOT NULL AND MASTER_ID IS NULL";
    }
    return tMap;
  }
  const string ChannelMedia::restoreSet( void ) const {
    return "CHANNEL_ID = ?1";
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChannelMedia::ChannelMedia( void ): Resource(TABLE_DDL, false)
  {
    this->setSyncInd( kSyncIndSynchronous );
    this->setAttachmentOutdated( false );
    this->setModificationMask( kMediaPendingLocalModification );
  }
  ChannelMedia::ChannelMedia( const string& mediaGid ): ChannelMedia()
  {
    setGid(mediaGid);
  }
  ChannelMedia::ChannelMedia( const ChannelMedia& record ): Resource(record)
  {
    this->operator=(record);
  }
  ChannelMedia::~ChannelMedia( void ) { }
  
  const char* ChannelMedia::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& ChannelMedia::tableFields( void ) const { return TABLE_DDL; }
  
  const string& ChannelMedia::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& ChannelMedia::xmlFields( void ) const { return XML_DDL; }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  ChannelMedia& ChannelMedia::operator=( const ChannelMedia& rhs ) {
    if (this != &rhs) {
      Resource::operator=(rhs);
    }
    return *this;
  }
  
  // comparison
  bool ChannelMedia::equals( const BaseEntity& rhs ) const {
    return Resource::equals(rhs);
  }
  bool ChannelMedia::lessThan( const BaseEntity& rhs ) const {
    return Resource::lessThan(rhs);
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  bool ChannelMedia::copyDataFrom( const BaseEntity& rhs )
  {
    if ( Resource::copyDataFrom(rhs) ) {
      const ChannelMedia& mrhs = dynamic_cast<const ChannelMedia&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Owner);
      COPY_PROPERTY((*this), mrhs, No);
      COPY_BOOL_PROPERTY((*this), mrhs, Selected);
      
      return true;
    }
    return false;
  }
  bool ChannelMedia::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    if (! Resource::hasEqualContent( arg ))
      return false;
    
    ChannelMedia& rhs( (ChannelMedia&)arg );
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( this->getOwner() != rhs.getOwner() )
      return false;
    return true;
  }
  bool ChannelMedia::isDataEqualTo( const BaseEntity& rhs ) const
  {
    return Resource::isDataEqualTo( rhs );
  }
  void ChannelMedia::archive( void )
  {
  }
  void ChannelMedia::unarchive( void )
  {
  }
  void ChannelMedia::clearData( void )
  {
    Resource::clearData( );
    this->clearCreateTime( );
    this->clearModifyTime( );
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON Mapping
  
  bool ChannelMedia::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasGid() ) {
      j["id"] = string(this->value("GID")->str(), this->value("GID")->size());
    }
    return true;
  }
  /*
  bool ChannelMedia::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if (this->hasGid()) {
      json_object_set_new( node, "id", json_stringn_nocheck( this->value("GID")->str(), this->value("GID")->size() ) );
    }
    
    return true;
  }
  */

  bool ChannelMedia::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("id")) {
        this->value("GID")->str( j["id"] );
      }
      if ( j.contains("anchor")) {
        this->value("ANCHOR")->str( j["anchor"] );
      }
      if ( j.contains("owner")) {
        this->value("OWNER")->str( j["owner"] );
      }
      if ( j.contains("create_time")) {
        this->value("CREATE_TIME")->str( j["create_time"] );
      }
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }
      if ( j.contains("encrypted")) {
        this->value("ENCRYPTED")->bol( j["encrypted"] );
      }

      return true;
    }
    
    return false;
  }
  /*
  bool ChannelMedia::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "id");
    if ( json_is_string(value) ) {
      this->value("GID")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "anchor");
    if ( json_is_string(value) ) {
      this->value("ANCHOR")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner");
    if ( json_is_string(value) ) {
      this->value("OWNER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "create_time");
    if ( json_is_string(value) ) {
      this->value("CREATE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "encrypted");
    if ( json_is_string(value) ) {
      this->value("ENCRYPTED")->bytea( json_string_value(value), json_string_length(value) );
    }
    
    return true;
  }
  */

  // XML Mapping
  
  const string ChannelMedia::XML_NAME   = "resource";
  const ser::XmlAttribute ChannelMedia::XML_FIELDS[] =
  {
    ser::XmlAttribute("id", "GID" ),
    ser::XmlAttribute("anchor", "SYNC_ANCHOR" ),
    ser::XmlAttribute("owner", "OWNER" ),
    ser::XmlAttribute("encrypted", "ENCRYPTED" ),
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute("create_time", "CREATE_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet ChannelMedia::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  bool ChannelMedia::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("resource"); // start element tag
    
    // serialize properties
    if ( this->hasGid() ) {
      xml << attr("id") << this->getGid();
    }
    xml << ser::endtag("resource"); // end element tag
    return true;
  }
  
}
