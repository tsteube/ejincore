/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_PROFILE_ENTITY_H__
#define __EJIN_CHANNEL_PROFILE_ENTITY_H__

#include "Declarations.h"
#include "EjinDatabase.h"
#include "ChannelHeaderRepository.h"
#include "ChannelPostRepository.h"
#include "ChannelCommentRepository.h"
#include "MediaRepository.h"
#include "MemberRepository.h"

namespace ejin
{
  // forward declaration
  class Channel;
  
  /**
   * Update operation on the channel header entity. 
   * There are a create, update and delete methods to manages channels.
   */
  class ChannelProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChannelProfile( Database& db, ChannelHeaderRepository& headerRepository, ChannelPostRepository& postRepository, ChannelCommentRepository& commentRepository, MediaRepository& mediaRepository, MemberRepository& memberRepository ):
    db_(db), headerRepository_(headerRepository), postRepository_(postRepository), commentRepository_(commentRepository), mediaRepository_(mediaRepository), memberRepository_(memberRepository)
    { }
    virtual ~ChannelProfile( void ) { }
    
    /**
     * Full Text Search for channel.
     */
    list<integer> searchFullText( const string& pattern )
    throw(data_access_error);

    /**
     * Load channel with all posts and comments entities.
     */
    shared_ptr<Channel> exportExtendedChannel( const integer channelId, const char* username )
    throw(data_access_error);

    /**
     * Load channel with all posts and comments entities.
     */
    shared_ptr<Channel> exportChannel( const string& channelGid )
    throw(data_access_error);
    
    /**
     * Load channel with modified posts and comments entities.
     */
    shared_ptr<Channel> loadModificiations( integer channelId )
    throw(data_access_error);    
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    /**
     * Create the channel container skeleton with all modifications found in the given entities.
     */ 
    shared_ptr<Channel> createModifiedSkeleton( SyncInd syncInd, 
                                                shared_ptr<ChannelHeader> channelHeader, 
                                                list< shared_ptr<ChannelPost> > posts,
                                                list< shared_ptr<ChannelComment> > comments )
    throw(data_access_error);    

    /**
     * Create the channel container with all given entities.
     */ 
    shared_ptr<Channel> createChannel( shared_ptr<ChannelHeader> channelHeader, 
                                       list< shared_ptr<ChannelPost> > posts,
                                       list< shared_ptr<ChannelComment> > comments )
    throw(data_access_error);    
    
    /**
     * Create the channel container with all given entities.
     */
    shared_ptr<Channel> createExtendedChannel( shared_ptr<ChannelHeader> channelHeader,
                                               list< shared_ptr<ChannelPost> > posts,
                                               list< shared_ptr<ChannelComment> > comments,
                                               const char* username )
    throw(data_access_error);
    
    // --------
    // Member Variables 
    
    Database&                 db_;
    ChannelHeaderRepository&  headerRepository_;
    ChannelPostRepository&    postRepository_;
    ChannelCommentRepository& commentRepository_;
    MediaRepository&          mediaRepository_;
    MemberRepository&         memberRepository_;
    
  private:
    
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ChannelProfile );
    
  };  
  
}

#endif // __EJIN_CHANNEL_REPOSITORY_ENTITY_H__
