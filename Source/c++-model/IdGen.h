/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_ID_GEN_H__
#define __EJIN_ID_GEN_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"

namespace ejin
{
  
  /**
   * ejin Entity for ID_GEN
   */
  class IdGen: public sqlite::BaseEntity, DataSchema<IdGen> {
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "ID_GEN";
    
    // constructors
    IdGen( void );
    IdGen( const string& key, integer value );
    IdGen( const IdGen& record );
    // dtor
    ~IdGen( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    IdGen& operator=( const IdGen& rhs );
    // clone
    sqlite::BaseEntity* clone( void ) const { return new IdGen(*this); }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_STRING_PROPERTY_INTF( GenKey, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( GenValue, 1 )
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // schema DDL
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    bool useSequenceTable() const { return false; }
    
  };
  
}

#endif //__EJIN_ID_GEN_H__
