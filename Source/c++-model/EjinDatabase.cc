/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "EjinDatabase.h"
#include "UsingEjinTypes.h"

#import <TemplateResources/LocalizationManager.h>

#include <stdio.h>
#include <typeinfo>
#include <dirent.h>
#include <sys/param.h>
#include <dispatch/dispatch.h>
#include <fstream>
#include <cstdio>

#include "EjinTable.h"
#include "IdGen.h"
#include "SyncSource.h"
#include "Member.h"
#include "Picture.h"
#include "ApprovedMember.h"
#include "ChannelMember.h"
#include "Media.h"
#include "Chat.h"
#include "Product.h"
#include "Message.h"
#include "Attachment.h"
#include "ChangeLog.h"
#include "Thumb.h"
#include "MemberProfileView.h"
#include "ChannelMedia.h"
#include "ChannelComment.h"
#include "ChannelPost.h"
#include "ChannelHeader.h"
#include "ChannelFullText.h"
#include "ChannelProfileView.h"
#include "ChannelSkeletonView.h"
#include "KeySequence.h"
#include "SqliteValue.h"
#include "SqliteResultSet.h"
#include "DebugLog.h"
#include "LocalizationManager.h"

#define FILE_EXTENSION_OF( mimeType ) \
LocalizationManager::getInstance()->fileExtensionOfMimeType(mimeType)

static int callback(void *NotUsed, int argc, char **argv, char **szColName)
{
  for(int i = 0; i < argc; i++)
  {
    std::cout << szColName[i] << " = " << argv[i] << std::endl;
  }
  std::cout << "\n";
  return 0;
}

namespace ejin
{
  using sqlite::Table;
  using sqlite::ResultSet;
  
  Database::Database( const char* username ): sqlite::Database(), username_(username) {
  }
  
  void Database::initEntityManager( void )
  {
    // initialize Entity Manager
    registerPrototype(new IdGen());
    registerPrototype(new SyncSource());
    
    registerPrototype(new Member());
    registerPrototype(new Picture());
    registerPrototype(new ApprovedMember());
    registerPrototype(new ChannelMember());
    registerPrototype(new MemberProfileView());

    registerPrototype(new Media());
    registerPrototype(new MediaProfileView());
    registerPrototype(new ChannelMedia());
    
    registerPrototype(new ChannelComment());
    registerPrototype(new ChannelPost());
    registerPrototype(new ChannelHeader());
    registerPrototype(new ChannelProfileView());
    registerPrototype(new ChannelSkeletonView());
    registerPrototype(new ChannelFullText());

    registerPrototype(new Attachment());
    registerPrototype(new Thumb());

    registerPrototype(new Chat());
    registerPrototype(new Message());

    registerPrototype(new ChangeLog());

    registerPrototype(new Product());
  }
  
  void Database::registerPrototype( sqlite::BaseEntity* proto ) {
    ejin::EjinTable* source_tbl = new ejin::EjinTable( *this, proto );
    _tableToEntity.insert ( std::pair<string,ejin::EjinTable*>(proto->tableName(),source_tbl) );
  }
  
  // ==========
  // Public Interface
  // ==========
  

  const string Database::username( void ) const
  { return this->username_; }
  
  void Database::upgradeDatabase( const char* database, int version )
  {
    DEBUG_METHOD();
    if (isOpen()) {
      int ver = getUserVersion( handle(), database );
      if ( ver > version ) {
        ejin::_throw_exception(ejin::DATA_INTEGRITY_VIOLATION_EXCEPTION,
                               "ejin database schema version %d greater than expected (%d)", ver, version);
      }
      
      if ( ver < version ) {
        // begin transaction
        bool managed = isAutoCommit();
        if ( managed ) transactionBegin();
        
        switch ( ver ) {
          case 0:
          case 1:
          case 2:
          case 3:
            ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Can't upgrade database from version %d\n", ver);
            break;
          case 4:
            DEBUG_PRINT("Upgrade Ejin DB schema from 4 to 5");
            /* new outdated field in view */
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "DROP VIEW IF EXISTS %s.pending_thumb_view", database);
              exec(this->handle(), "CREATE VIEW %s.pending_thumb_view AS "
                   "SELECT "
                   "a.id             AS attachment_id,"
                   "a.media_id       AS media_id,"
                   "a.size           AS size"
                   "  FROM attach.attachment_tbl a"
                   "    LEFT OUTER JOIN attach.thumb_tbl t ON (a.id = t.attachment_id)"
                   "  WHERE t.id IS NULL", database );
            }
            setUserVersion( handle(), 5, database );
            break;
          case 5:
            DEBUG_PRINT("Upgrade Ejin DB schema from 5 to 6");
            /* new outdated field in view */
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.channel_tbl add COLUMN invalid_session_key BOOLEAN NOT NULL DEFAULT 0", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN revoke_key BOOLEAN NOT NULL DEFAULT 0", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN max_channels INTEGER", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN max_attachments INTEGER", database);
            }
            setUserVersion( handle(), 6, database );
          case 6:
            DEBUG_PRINT("Upgrade Ejin DB schema from 6 to 7");
            /* new outdated field in view */
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN max_attachments INTEGER", database);
            }
            setUserVersion( handle(), 7, database );
          case 7:
            DEBUG_PRINT("Upgrade Ejin DB schema from 7 to 8");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ) {
              /* new member field */
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN image_mime_type VARCHAR(32)", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN image_data BLOB", database);
              exec(this->handle(), "UPDATE %s.member_tbl SET image_data = image, image = NULL", database);
            }
            setUserVersion( handle(), 8, database );
          case 8:
            DEBUG_PRINT("Upgrade Ejin DB schema from 8 to 9");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ) {
              /* new outdated field in view */
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN max_channel_count INTEGER", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN max_attachment_count INTEGER", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl  add COLUMN max_total_attachment_size INTEGER", database);
            }
            setUserVersion( handle(), 9, database );
          case 9:
            DEBUG_PRINT("Upgrade Ejin DB schema from 9 to 10");
            if ( strncmp( database, ATTACHMENT_DATABASE_NAME, strlen(ATTACHMENT_DATABASE_NAME)) == 0 ||
                 strncmp( database, ATTACHMENT_ARCHIVE_DATABASE_NAME, strlen(ATTACHMENT_ARCHIVE_DATABASE_NAME)) == 0 ) {
              /* new outdated field in view */
              exec(this->handle(), "ALTER TABLE %s.thumb_tbl add COLUMN size BIGINT CHECK (size >= 0) DEFAULT 0", database);
            }
            setUserVersion( handle(), 10, database );
          case 10:
            DEBUG_PRINT("Upgrade Ejin DB schema from 10 to 11");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              /* new outdated field in view */
              exec(this->handle(), "ALTER TABLE %s.media_tbl add COLUMN size BIGINT CHECK (size >= 0) DEFAULT 0", database);
            }
            setUserVersion( handle(), 11, database );
          case 11:
            DEBUG_PRINT("Upgrade Ejin DB schema from 10 to 11");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "INSERT INTO %s.source_tbl (sync_source,state) VALUES  ('CHAT',0)", database);
              exec(this->handle(), "INSERT INTO %s.id_gen (gen_key,gen_value) VALUES ('chat_tbl',101)", database);
              exec(this->handle(), "INSERT INTO %s.id_gen (gen_key,gen_value) VALUES ('message_tbl',101)", database);
              /* create new chat tables */
              exec(this->handle(), "CREATE TABLE %s.chat_tbl ( "
                   "id                    INTEGER PRIMARY KEY, "
                   "gid                   VARCHAR(64), "
                   "message_count         INTEGER NOT NULL DEFAULT 0, "
                   "ack                   INTEGER NOT NULL DEFAULT 0, "
                   "loaded                INTEGER NOT NULL DEFAULT 0, "
                   "key                   VARCHAR(256), "
                   "partner               VARCHAR(64) NOT NULL, "
                   "partner_ack           INTEGER NOT NULL DEFAULT 0, "
                   "CONSTRAINT chat_partner UNIQUE (partner) "
                   ")", database);
              exec(this->handle(), "CREATE TABLE %s.message_tbl ( "
                   "id                    INTEGER PRIMARY KEY, "
                   "chat_id               INTEGER, "
                   "sync_ind              INTEGER NOT NULL CHECK (sync_ind == 0 OR sync_ind == 1 OR sync_ind == 3) DEFAULT 0, "
                   "no                    INTEGER NOT NULL DEFAULT 0, "
                   "sender                VARCHAR(64) NOT NULL, "
                   "content               TEXT NOT NULL, "
                   "create_time           INTEGER, "
                   "send_time             INTEGER, "
                   "ack                   BOOLEAN NOT NULL DEFAULT false, "
                   "secure                BOOLEAN NOT NULL DEFAULT false, "
                   "encrypted             BOOLEAN NOT NULL DEFAULT false, "
                   "FOREIGN KEY( chat_id ) REFERENCES chat_tbl (id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED "
                   ")", database);
              exec(this->handle(), "CREATE TRIGGER gen_chat_id "
                   "AFTER INSERT ON chat_tbl "
                   "FOR EACH ROW BEGIN "
                   "UPDATE id_gen SET gen_value = gen_value+1 WHERE gen_key = 'chat_tbl' "
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER gen_message_id "
                   "AFTER INSERT ON message_tbl "
                   "FOR EACH ROW BEGIN "
                   "UPDATE id_gen SET gen_value = gen_value+1 WHERE gen_key = 'message_tbl' "
                   "END", database);
            }
            setUserVersion( handle(), 12, database );
          case 12:
            DEBUG_PRINT("Upgrade Ejin DB schema from 12 to 13");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.chat_tbl add COLUMN pkey_finger_print CHAR(64)", database);
              exec(this->handle(), "ALTER TABLE %s.chat_tbl add COLUMN partner_pkey_finger_print CHAR(64)", database);
            }
            setUserVersion( handle(), 13, database );
          case 13:
            DEBUG_PRINT("Upgrade Ejin DB schema from 13 to 14");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "CREATE TABLE %.picture_tbl ( "
                   "id                    INTEGER PRIMARY KEY, "
                   "member_id             INTEGER NOT NULL, "
                   "mime_type             VARCHAR(32) NOT NULL, "
                   "data                  BLOB, "
                   "create_time           INTEGER, "
                   "modify_time           INTEGER, "
                   "sync_ind              INTEGER NOT NULL CHECK (sync_ind >= 0 AND sync_ind <= 4) DEFAULT 0, "
                   "master_id             INTEGER DEFAULT NULL, "
                   "modification_mask     INTEGER NOT NULL DEFAULT 0, "
                   "FOREIGN KEY( member_id ) REFERENCES member_tbl (id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED, "
                   "FOREIGN KEY( member_id ) REFERENCES picture_tbl (id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED, "
                   "UNIQUE ( member_id, master_id )"
                   ")", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl drop COLUMN image_mime_type", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl drop COLUMN image", database);
              exec(this->handle(), "CREATE TRIGGER gen_picture_id "
                   "AFTER INSERT ON picture_tbl "
                   "FOR EACH ROW BEGIN "
                   "UPDATE id_gen SET gen_value = gen_value+1 WHERE gen_key = 'picture_tbl' "
                   "END", database);
              exec(this->handle(), "DROP VIEW IF EXISTS %s.member_profile_view", database);
              exec(this->handle(), "CREATE VIEW %s.member_profile_view AS "
                   "SELECT "
                   "m.username                  AS username, "
                   "m.id                        AS member_id, "
                   "m.sync_ind                  AS member_sync_ind, "
                   "p.id                        AS picture_id, "
                   "COALESCE(p.sync_ind,0)      AS picture_sync_ind "
                   "FROM member_tbl m "
                   "LEFT OUTER JOIN picture_tbl p ON (p.member_id = m.id AND p.master_id IS NULL) "
                   "WHERE m.master_id IS NULL", database );
            }
            setUserVersion( handle(), 14, database );
          case 14:
            DEBUG_PRINT("Upgrade Ejin DB schema from 14 to 15");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
//              exec(this->handle(), "ALTER TABLE %s.channel_tbl alter COLUMN session_key TYPE VARCHAR", database);
//              exec(this->handle(), "ALTER TABLE %s.channel_member_tbl alter COLUMN session_key TYPE VARCHAR", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN pub_key_create_time INTEGER", database);
              exec(this->handle(), "ALTER TABLE %s.approved_member_tbl add COLUMN modify_time INTEGER", database);
              exec(this->handle(), "DROP VIEW IF EXISTS %s.approval_chain_view", database);
              exec(this->handle(), "CREATE VIEW %s.approval_chain_view AS "
                   "SELECT "
                   "m0.id                                     as id0, "
                   "m0.username                               as username0, "
                   "m1.id                                     as id1, "
                   "m1.username                               as username1, "
                   "(m1.pub_key_create_time > a0.modify_time) as outdated1, "
                   "m2.id                                     as id2, "
                   "m2.username                               as username2, "
                   "(m2.pub_key_create_time > a1.modify_time) as outdated2 "
                   "FROM member_tbl m0 "
                   "LEFT OUTER JOIN approved_member_tbl a0 ON (a0.member_id = m0.id) "
                   "LEFT OUTER JOIN member_tbl m1 ON (m1.username = a0.username) "
                   "LEFT OUTER JOIN approved_member_tbl a1 ON (a1.member_id = m1.id) "
                   "LEFT OUTER JOIN member_tbl m2 ON (m2.username = a1.username)", database);
            }
            
            setUserVersion( handle(), 15, database );
          case 15:
            DEBUG_PRINT("Upgrade Ejin DB schema from 15 to 16");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "CREATE TABLE %s.change_log_tbl ( "
                   "id                    INTEGER PRIMARY KEY, "
                   "sync_source           CHAR(16) NOT NULL, "
                   "sync_operation        CHAR(16) NOT NULL, "
                   "modified_by           VARCHAR(64) NOT NULL, "
                   "resource_id_1          INTEGER NOT NULL, "
                   "resource_id_2          INTEGER, "
                   "resource_id_3          INTEGER, "
                   "message               VARCHAR, "
                   "sync_anchor           VARCHAR(64) NOT NULL, "
                   "sync_time             INTEGER )", database);
              exec(this->handle(), "CREATE TRIGGER %s.change_log_tbl"
                   "AFTER INSERT ON change_log_tbl "
                   "FOR EACH ROW BEGIN "
                   "UPDATE id_gen SET gen_value = gen_value+1 WHERE gen_key = 'change_log_tbl'; "
                   "END", database);
            }
            setUserVersion( handle(), 16, database );
          case 16:
            DEBUG_PRINT("Upgrade Ejin DB schema from 16 to 17");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "CREATE VIRTUAL TABLE %s.full_text_channel_fts "
                   "USING FTS5(channel_id, entity_id, source, name, content, tokenize=porter)", database);
              exec(this->handle(), "INSERT INTO %s.full_text_channel_fts(full_text_channel_fts, rank) "
                   "VALUES('rank', 'bm25(0, 0, 0, 10, 1)')", database);
              exec(this->handle(), "CREATE TRIGGER %s.channel_content_insert AFTER INSERT ON channel_tbl "
                   "BEGIN "
                   "INSERT INTO full_text_channel_fts ( channel_id, entity_id, source, name, content ) "
                   "VALUES ( new.id, new.id, 'CHANNEL', new.name, new.content ); "
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER channel_content_insert UPDATE OF name ON channel_tbl "
                   "BEGIN "
                   "UPDATE full_text_channel_fts SET name = new.name WHERE entity_id = old.id AND source = 'CHANNEL';"
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.channel_content_insert UPDATE OF content ON channel_tbl "
                   "BEGIN "
                   "UPDATE full_text_channel_fts SET content = new.content WHERE entity_id = old.id AND source = 'CHANNEL';"
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.channel_content_delete AFTER DELETE ON channel_tbl "
                   "BEGIN "
                   "DELETE FROM full_text_channel_fts WHERE entity_id = old.id AND source = 'CHANNEL';"
                   "END", database);

              exec(this->handle(), "CREATE TRIGGER %s.post_content_insert AFTER INSERT ON post_tbl "
                   "BEGIN "
                   "INSERT INTO %s.full_text_channel_fts ( channel_id, entity_id, source, content ) "
                   "VALUES ( new.channel_id, new.id, 'CHANNEL_POST', new.content ); "
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.post_content_update_content UPDATE OF content ON post_tbl "
                   "BEGIN "
                   "UPDATE full_text_channel_fts SET content = new.content WHERE entity_id = old.id AND source = 'CHANNEL_POST';"
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.post_content_delete AFTER DELETE ON post_tbl "
                   "BEGIN "
                   "DELETE FROM full_text_channel_fts WHERE entity_id = old.id AND source = 'CHANNEL_POST';"
                   "END", database);

              exec(this->handle(), "CREATE TRIGGER %s.comment_content_insert AFTER INSERT ON comment_tbl "
                   "BEGIN "
                   "INSERT INTO %s.full_text_channel_fts ( channel_id, entity_id, source, content ) "
                   "VALUES ( new.channel_id, new.id, 'CHANNEL_COMMENT', new.content ); "
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.comment_content_update_content UPDATE OF content ON comment_tbl "
                   "BEGIN "
                   "UPDATE full_text_channel_fts SET content = new.content WHERE entity_id = old.id AND source = 'CHANNEL_COMMENT';"
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.comment_content_delete AFTER DELETE ON comment_tbl "
                   "BEGIN "
                   "DELETE FROM full_text_channel_fts WHERE entity_id = old.id AND source = 'CHANNEL_COMMENT';"
                   "END", database);

              exec(this->handle(), "CREATE TRIGGER %s.media_content_insert AFTER INSERT ON media_tbl "
                   "BEGIN "
                   "INSERT INTO %s.full_text_channel_fts ( channel_id, entity_id, source, content ) "
                   "VALUES ( new.channel_id, new.id, 'MEDIA', new.name ); "
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.media_content_update_content UPDATE OF name ON media_tbl "
                   "BEGIN "
                   "UPDATE full_text_channel_fts SET name = new.name WHERE entity_id = old.id AND source = 'MEDIA';"
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.media_content_delete AFTER DELETE ON media_tbl "
                   "BEGIN "
                   "DELETE FROM full_text_channel_fts WHERE entity_id = old.id AND source = 'MEDIA';"
                   "END", database);

              exec(this->handle(), "CREATE TRIGGER %s.channel_member_content_insert AFTER INSERT ON media_tbl "
                   "BEGIN "
                   "INSERT INTO %s.full_text_channel_fts ( channel_id, entity_id, source, content ) "
                   "VALUES ( new.channel_id, new.id, 'CHANNEL_MEMBER', new.tags ); "
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.channel_member_content_update_content UPDATE OF name ON media_tbl "
                   "BEGIN "
                   "UPDATE full_text_channel_fts SET name = new.tags WHERE entity_id = old.id AND source = 'CHANNEL_MEMBER';"
                   "END", database);
              exec(this->handle(), "CREATE TRIGGER %s.channel_member_content_delete AFTER DELETE ON media_tbl "
                   "BEGIN "
                   "DELETE FROM full_text_channel_fts WHERE entity_id = old.id AND source = 'CHANNEL_MEMBER';"
                   "END", database);
            }

            setUserVersion( handle(), 17, database );
          case 17:
            DEBUG_PRINT("Upgrade Ejin DB schema from 17 to 18");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ) {
              exec( this->handle(), "CREATE TABLE %s.product_tbl ( "
                    "id                         INTEGER PRIMARY KEY, "
                    "no                         INTEGER NOT NULL DEFAULT 0, "
                    "identifier                 VARCHAR(64) NOT NULL, "
                    "enabled                    BOOLEAN NOT NULL DEFAULT 0, "
                    "title                      VARCHAR(64), "
                    "description                VARCHAR(256), "
                    "price                      REAL DEFAULT 0.0, "
                    "price_locale               CHAR(32), "
                    "subscription_period        INT DEFAULT 0, "
                    "subscription_period_unit   INT DEFAULT 0 CHECK (subscription_period_unit IN (0,1,2,3)), "
                    "max_channel_count          INTEGER DEFAULT 0, "
                    "max_attachment_count       INTEGER DEFAULT 0, "
                    "max_total_attachment_size  INTEGER DEFAULT 0, "
                    "UNIQUE (identifier)"
                    ")", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN product VARCHAR(64)", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN subscription_expire_time INTEGER", database);
            }
            setUserVersion( handle(), 18, database );
          case 18:
            DEBUG_PRINT("Upgrade Ejin DB schema from 18 to 19");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.chat_tbl add COLUMN modify_time Integer", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN product_name VARCHAR(64)", database);
            }
            setUserVersion( handle(), 19, database );
          case 19:
            DEBUG_PRINT("Upgrade Ejin DB schema from 19 to 20");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN phone VARCHAR(64)", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN dial_in_code INTEGER", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN two_factor_authentication BOOLEAN DEFAULT FALSE", database);
            }
            setUserVersion( handle(), 20, database );
          case 20:
            DEBUG_PRINT("Upgrade Ejin DB schema from 20 to 21");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.channel_tbl add COLUMN session_key_2 VARCHAR", database);
            }
            setUserVersion( handle(), 21, database );
          case 21:
            DEBUG_PRINT("Upgrade Ejin DB schema from 21 to 22");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.source_tbl add COLUMN assertions INTEGER NOT NULL DEFAULT 0", database);

              exec(this->handle(), "CREATE INDEX %s.channel_tbl_idx_1 ON channel_tbl (master_id)", database);
              exec(this->handle(), "CREATE INDEX %s.post_tbl_idx_1 ON post_tbl (master_id)", database);
              exec(this->handle(), "CREATE INDEX %s.comment_tbl_idx_1 ON comment_tbl (master_id)", database);
              exec(this->handle(), "CREATE INDEX %s.member_tbl_idx_1 ON member_tbl (master_id)", database);
              exec(this->handle(), "CREATE INDEX %s.picture_tbl_idx_1 ON picture_tbl (master_id)", database);
              exec(this->handle(), "CREATE INDEX %s.media_tbl_idx_1 ON media_tbl (master_id)", database);
              
              exec(this->handle(), "DROP VIEW %s.channel_profile_view", database);
              exec( this->handle(), "CREATE VIEW %s.channel_profile_view AS "
                   "SELECT "
                   "c.gid                                                          AS channel_gid, "
                   "c.id                                                           AS channel_id, "
                   "c.membership                                                   AS channel_membership, "
                   "c.role                                                         AS channel_role, "
                   "c.outdated                                                     AS channel_outdated, "
                   "c.sync_anchor                                                  AS channel_sync_anchor, "
                   "c.sync_ind                                                     AS channel_sync_ind, "
                   "(select ifnull(max(m.sync_ind),0) from channel_member_tbl m WHERE m.channel_id = c.id) "
                   "AS member_sync_ind, "
                   "(select ifnull(max(e.sync_ind),0) from post_tbl e WHERE e.channel_id = c.id) "
                   "AS post_sync_ind, "
                   "(select ifnull(max(d.sync_ind),0) from comment_tbl d WHERE d.channel_id = c.id) "
                   "AS comment_sync_ind, "
                   "(select ifnull(max(f.sync_ind),0) from media_tbl f WHERE (f.channel_id = c.id AND "
                   "                           (f.sync_ind not in ( 0, 2 ) OR f.master_id IS NOT NULL))) "
                   "AS media_sync_ind "
                   "FROM channel_tbl c "
                   "WHERE c.master_id IS NULL", database);
            }
            setUserVersion( handle(), 22, database );
          case 22:
            DEBUG_PRINT("Upgrade Ejin DB schema from 22 to 23");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN apple_login BOOLEAN DEFAULT NULL", database);
            }
            setUserVersion( handle(), 23, database );
          case 23:
            DEBUG_PRINT("Upgrade Ejin DB schema from 23 to 24");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN email_hash VARCHAR(32) DEFAULT NULL", database);
            }
            setUserVersion( handle(), 24, database );
          case 24:            
            DEBUG_PRINT("Upgrade Ejin DB schema from 24 to 25");
            if ( strncmp( database, MAIN_DATABASE_NAME, strlen(MAIN_DATABASE_NAME)) == 0 ||
                strncmp( database, BACKUP_DATABASE_NAME, strlen(BACKUP_DATABASE_NAME)) == 0 ||
                strncmp( database, ARCHIVE_DATABASE_NAME, strlen(ARCHIVE_DATABASE_NAME)) == 0 ) {
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN firstname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.member_tbl add COLUMN lastname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.channel_member_tbl add COLUMN fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.channel_member_tbl add COLUMN modified_by_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.channel_member_tbl add COLUMN deleted BOOLEAN NOT NULL DEFAULT 0", database);
              exec(this->handle(), "ALTER TABLE %s.channel_tbl add COLUMN owner_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.channel_tbl add COLUMN modified_by_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.comment_tbl add COLUMN owner_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.comment_tbl add COLUMN modified_by_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.post_tbl add COLUMN owner_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.post_tbl add COLUMN modified_by_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.media_tbl add COLUMN owner_fullname VARCHAR(256)", database);
              exec(this->handle(), "ALTER TABLE %s.media_tbl add COLUMN modified_by_fullname VARCHAR(256)", database);
            }
            setUserVersion( handle(), 25, database );
          default:
            break;
        }
        
        // commit transaction
        if ( managed ) transactionCommit();
      }
    }
  }
  bool Database::exec( sqlite3* db, const char *statement, ... )
  {
    assert( db );
    assert( statement );
    
    va_list ap;
    char stmt[BUFSIZ];
    va_start(ap, statement);
    (void) vsnprintf(stmt, BUFSIZ, statement, ap);
    va_end(ap);
    
    if ( sqlite3_exec(db, stmt, callback, 0, NULL) == SQLITE_OK ) {
      return true;
    } else {
      std::cerr << "Sqlite exec error message: " << sqlite3_errmsg(db) << std::endl;
      return false;
    }
  }
  
  bool Database::checkConstency( void )
  {
    DEBUG_METHOD();
    
    if (isClean()) {
      
      unique_ptr<SyncSource> syncSource(new SyncSource(kSourceMember));
      this->refresh( *syncSource ); // throws data_access_error if not found
      if (! syncSource.get()) {
        DEBUG_PRINT("No member source entity!");
        return false;
      }
      unique_ptr<ResultSet> values(this->find(Member::TABLE_NAME, "findAllOrderBySyncAnchorDesc", NULL));
      if (values->size() > 0) {
        // compare on maximum sync anchor in member table
        
        KeySequence maxSyncAnchor(static_cast< Member& >((*values)[0]).getSyncAnchor());
        KeySequence currentSyncAnchor(syncSource->getSyncAnchor());
        if (maxSyncAnchor > currentSyncAnchor) {
          DEBUG_PRINT("Last member sync anchor in source is lower than the most recent member entity remote update!");
          return false;
        }
      }
      syncSource->setUsername(this->username_);
      this->update( *syncSource );
      
      syncSource = unique_ptr<SyncSource>(new SyncSource(kSourceMedia));
      this->refresh( *syncSource ); // throws data_access_error if not found
      if (! syncSource.get()) {
        DEBUG_PRINT("No media source entity!");
        return false;
      }
      values = this->find(Media::TABLE_NAME, "findAllOrderBySyncAnchorDesc", NULL);
      if (values->size() > 0) {
        // compare on maximum sync anchor in media table
        
        KeySequence maxSyncAnchor(static_cast< Media& >((*values)[0]).getSyncAnchor());
        KeySequence currentSyncAnchor(syncSource->getSyncAnchor());
        if (maxSyncAnchor > currentSyncAnchor) {
          DEBUG_PRINT("Last media sync anchor in source is lower than the most recent media entity remote update!");
          return false;
        }
      }
      syncSource->setUsername(this->username_);
      this->update( *syncSource );
      
      syncSource = unique_ptr<SyncSource>(new SyncSource(kSourceChannel));
      this->refresh( *syncSource ); // throws data_access_error if not found
      if (! syncSource.get()) {
        DEBUG_PRINT("No channel source entity!");
        return false;
      }
      
      const Value username(this->username_.c_str());
      unique_ptr<Value> result( this->valueOf(ChannelHeader::TABLE_NAME, "missingMembership", &username, NULL) );
      if ( result->num() > 0 ) {
        DEBUG_PRINT("expect channel_member_tbl[username=%s] entity for all available channel_tbl!", this->username_.c_str());
        return false;
      }
      
      result = this->valueOf(Member::TABLE_NAME, "missingMembers", NULL) ;
      if ( result->num() > 0 ) {
        DEBUG_PRINT("unknown entity in channel_member_tbl not found in member_tbl!");
        //return false;
      }
      /*
      values = this->find(ChannelHeader::TABLE_NAME, "findAllOrderBySyncAnchorDesc", NULL);
      if (values->size() > 0) {
        // compare on minimal sync anchor in channel table
        KeySequence minSyncAnchor(static_cast< ChannelHeader& >((*values)[(int)values->size()-1]).getSyncAnchor());
        KeySequence currentSyncAnchor(syncSource->getSyncAnchor());
        if (minSyncAnchor > currentSyncAnchor) {
          DEBUG_PRINT("Last channel sync anchor in source is lower than the most oldest channel entity remote update!");
          //return false;
        }
      }
      */
      syncSource->setUsername(this->username_);
      this->update( *syncSource );
    }
    return true;
  }
  
  bool Database::clearChannel( const string& channelGid )
  {
    bool success = false;
    DEBUG_METHOD();
    if (isOpen()) {
      DEBUG_PRINT("Clear local channel[gid=%s]", channelGid.c_str());
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      Value gid( channelGid.c_str() );
      Value hidden(false);
      Value visible(true);
      
      // search and remove
      unique_ptr<ResultSet> values =
      this->find(ChannelHeader::TABLE_NAME, "findChannelByGid", &gid, &hidden, NULL);
      vector< BaseEntity* >::iterator it = values->begin();
      shared_ptr<ChannelHeader> header;
      if (it != values->end()) {
        header = shared_ptr<ChannelHeader>( static_cast< ChannelHeader* >( *it ) );
        it = values->take(it);
        clearChannelInternal( *header );
        success = true;
      }
      
      // commit transaction
      if ( managed ) transactionCommit();
    }
    return success;
  }
  
  bool Database::backupChannel( const string& channelGid ) {
    DEBUG_METHOD();
    bool success = false;
    if (isOpen()) {
      DEBUG_PRINT("Backup local channel[gid=%s]", channelGid.c_str());
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      Value gid( channelGid.c_str() );
      Value hidden(false);
      Value visible(true);
      
      // search and remove
      unique_ptr<ResultSet> values =
      this->find(ChannelHeader::TABLE_NAME, "findChannelByGid", &gid, &hidden, NULL);
      vector< BaseEntity* >::iterator it = values->begin();
      shared_ptr<ChannelHeader> header;
      if (it != values->end()) {
        success = true;
        header = shared_ptr<ChannelHeader>( static_cast< ChannelHeader* >( *it ) );
        it = values->take(it);

        // backup channel specific data rows
        Value channelId(header->getId());
        success = true;
        
        ChannelHeader bakHeader( *header );
        if (this->contains( bakHeader, bakHeader.backupSchemaName() )) {
          this->refresh( bakHeader, bakHeader.backupSchemaName() );
          if ( (bakHeader.hasModifyTime() && header->getModifyTime() == bakHeader.getModifyTime()) ||
              header->getSyncInd() == kSyncIndUpdate ) {
            // save only the meta data
            bakHeader.copyMetaDataFrom( *header );
            bakHeader.setSyncAnchor( header->getSyncAnchor() );
            bakHeader.setSyncInd( kSyncIndSynchronous );
            // update
            this->update(bakHeader, bakHeader.backupSchemaName());
          } else {
            this->backup(ChannelHeader::TABLE_NAME, 0, &channelId, NULL);
          }
        } else {
          // remove old leftover channel with same GID
          unique_ptr<ResultSet> values =
          this->find(ChannelHeader::TABLE_NAME, "findChannelByGidBak", &gid, NULL);
          vector< BaseEntity* >::iterator it = values->begin();
          shared_ptr<ChannelHeader> header;
          if (it != values->end()) {
            this->remove( **it, BACKUP_DATABASE_NAME );
          }
          this->backup(ChannelHeader::TABLE_NAME, 0, &channelId, NULL);
        }
        bool b = this->backup(ChannelMember::TABLE_NAME, 0, &channelId, NULL);
        b = this->backup(ChannelPost::TABLE_NAME, 0, &channelId, NULL);
        b = this->backup(ChannelComment::TABLE_NAME, 0, &channelId, NULL);
        b = this->backup(Media::TABLE_NAME, 0, &channelId, NULL);
      }

      // commit transaction
      if ( managed ) transactionCommit();
    }
    return success;
  }
  bool Database::restoreChannel( const string& channelGid ) {
    DEBUG_METHOD();
    bool success = false;
    if (isOpen()) {
      DEBUG_PRINT("Restore local channel[gid=%s]", channelGid.c_str());
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      Value gid( channelGid.c_str() );
      Value hidden(false);
      Value visible(true);
      
      // search and remove
      unique_ptr<ResultSet> values =
      this->find(ChannelHeader::TABLE_NAME, "findChannelByGid", &gid, &hidden, NULL);
      vector< BaseEntity* >::iterator it = values->begin();
      shared_ptr<ChannelHeader> header;
      if (it != values->end()) {
        header = shared_ptr<ChannelHeader>( static_cast< ChannelHeader* >( *it ) );
        it = values->take(it);
        clearChannelInternal( *header );
        
        if ( contains(*header, BACKUP_DATABASE_NAME) ) {
          // restore channel specific data rows
          Value channelId(header->getId());
          success = true;
          this->restore(ChannelHeader::TABLE_NAME, 0, &channelId, NULL);
          this->restore(ChannelMember::TABLE_NAME, 0, &channelId, NULL);
          this->restore(ChannelPost::TABLE_NAME, 0, &channelId, NULL);
          this->restore(ChannelComment::TABLE_NAME, 0, &channelId, NULL);
          this->restore(Media::TABLE_NAME, 0, &channelId, NULL);
          
          // collect all media identifier for the channel
          unique_ptr<ResultSet> allMedias(this->find(Media::TABLE_NAME, "findMediaByChannelId", &channelId, NULL));
          for (vector< BaseEntity* >::iterator it2 = allMedias->begin(); it2 != allMedias->end(); it2++) {
            Media* media = static_cast< Media* >( *it2 );
            Value mediaId( media->getId() );
            
            BaseEntity* entity = this->find(Attachment::TABLE_NAME, "findMaster", &mediaId, NULL)->takeFirst();
            shared_ptr<Attachment> attachmentMaster = shared_ptr<Attachment>(static_cast< Attachment* >( entity ) );
            media->setAttachmentOutdated( attachmentMaster == NULL );
            this->update( *media );
            
            entity = this->find(Attachment::TABLE_NAME, "findSlave", &mediaId, NULL)->takeFirst();
            shared_ptr<Attachment> attachmentSlave = shared_ptr<Attachment>(static_cast< Attachment* >( entity ) );
            if ( attachmentSlave ) {
              // replace master by slave instance
              attachmentSlave->clearMasterId( );
              this->update( *attachmentSlave );
              this->remove( *attachmentMaster );
            }
          }
        }
      }
      // commit transaction
      if ( managed ) transactionCommit();
    }
    return success;
  }
  bool Database::backupMember( void ) {
    DEBUG_METHOD();
    bool success = false;
    if (isOpen()) {
      DEBUG_PRINT("Backup local members");
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      
      // backup channel specific data rows
      this->backup(Member::TABLE_NAME, 0, NULL);
      this->backup(ApprovedMember::TABLE_NAME, 0, NULL);
      this->backup(Picture::TABLE_NAME, 0, NULL);
      success = true;

      // commit transaction
      if ( managed ) transactionCommit();
    }
    return success;
  }
  bool Database::restoreMember( void ) {
    DEBUG_METHOD();
    bool success = false;
    if (isOpen()) {
      DEBUG_PRINT("Restore local members");
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      
      // clear conflicted members first
      vector< BaseEntity* >::iterator it;
      unique_ptr<ResultSet> values = this->find(Member::TABLE_NAME, "findConflictedMembers", NULL);
      for (it = values->begin(); it != values->end(); it++) {
        this->remove( **it );
      }
      
      // and restore members from backup
      success = true;
      success &= this->restore(Member::TABLE_NAME, NULL);
      
      // commit transaction
      if ( managed ) transactionCommit();
    }
    return success;
  }
  
  bool Database::archiveChannel( const integer channelId, const char* password )
  throw(data_access_error)
  {
    assert( channelId > 0 );
    DEBUG_METHOD();
    bool success = false;
    if (isOpen()) {
      bool wasArchiveOpen = isArchiveOpen();
      if ( this->attachArchiveDb( password ) == Database::DatabaseOpen ) {
        // begin transaction
        bool managed = isAutoCommit();
        if ( managed ) transactionBegin();
        
        DEBUG_PRINT("archive local channel[id=%s]", channelId);
        success = this->archiveChannelHeader( channelId );
        
        // commit transaction
        if ( managed ) transactionCommit();

        if ( !wasArchiveOpen ) {
          this->detachArchiveDb( password );
        }
      }
    }
    return success;
  }
  bool Database::unarchiveChannel( const integer channelId, const char* password )
  throw(data_access_error)
  {
    assert( channelId > 0 );
    DEBUG_METHOD();
    bool success = false;
    if (isOpen()) {
      bool wasArchiveOpen = isArchiveOpen();
      if ( this->attachArchiveDb( password ) == Database::DatabaseOpen ) {
        // begin transaction
        bool managed = isAutoCommit();
        if ( managed ) transactionBegin();
        
        DEBUG_PRINT("unarchive local channel[id=%s]", channelId);
        success = this->unarchiveChannelHeader( channelId );
        
        // commit transaction
        if ( managed ) transactionCommit();
        
        if ( !wasArchiveOpen ) {
          this->detachArchiveDb( password );
        }
      }
    }
    return success;
  }
  int Database::exportAllChannelAttachments(integer channel, string targetFolder)
  {
    DEBUG_METHOD();
    int result = 0;
    if (isOpen()) {
      DEBUG_PRINT("Export all attachments of channel[id=%s]", channelId.c_str());
      
      std::ofstream fout;
      std::ostringstream stringStream;
      // collect all channel media attachments
      Value channelId( channel );
      unique_ptr<ResultSet> allMedias(this->find(Media::TABLE_NAME, "findMediaByChannelId", &channelId, NULL));
      for (vector< BaseEntity* >::iterator it = allMedias->begin(); it != allMedias->end(); it++) {
        Media* media = static_cast< Media* >( *it );
        
        // 1. export attachment first
        Value mediaId( media->getId() );
        BaseEntity* entity = this->find(Attachment::TABLE_NAME, "findMaster", &mediaId, NULL)->takeFirst();
        shared_ptr<Attachment> attachment = shared_ptr<Attachment>(static_cast< Attachment* >( entity ) );
        if (attachment && attachment->hasData()) {
          stringStream.clear(); stringStream.str("");
          stringStream << targetFolder << "/" << media->getId() << "." << FILE_EXTENSION_OF(media->getMimeType());
          std::string filename = stringStream.str();
          if (! filename.empty() ) {
            fout.open(filename, std::ios::binary | std::ios::out);
            if (!fout.is_open()) {
              DEBUG_PRINT("Failed to open file '%s';  media[id=%d]", filename.c_str(), media->getId());
              return -1;
            }
            fout.write(attachment->getData(), attachment->getDataSize());
            fout.close();
            if (fout.fail()) {
              DEBUG_PRINT("Failed write to file '%s'; media[id=%d]", filename.c_str(), media->getId());
              std::remove( filename.c_str() );
              return -1;
            }
            result++;
            
            // 2. and the thumbnail afterwards
            Value attachmentId( attachment->getId() );
            entity = this->find("THUMB_TBL", "findByAttachmentId", &attachmentId, NULL)->takeFirst();
            shared_ptr<Thumb> thumb = shared_ptr<Thumb>(static_cast< Thumb* >( entity ) );
            if ( thumb && thumb->hasData() ) {
              stringStream.clear(); stringStream.str("");
              stringStream << targetFolder << "/" << "_" << media->getId() << ".png";
              std::string filename = stringStream.str();
              if (! filename.empty()) {
                fout.open(filename, std::ios::binary | std::ios::out);
                if (!fout.is_open()) {
                  DEBUG_PRINT("Failed write to file '%s'; thumb[id=%d]", filename.c_str(), thumb->getId());
                  return -1;
                }
                fout.write(thumb->getData(), thumb->getDataSize());
                fout.close();
                if (fout.fail()) {
                  DEBUG_PRINT("Failed write to file '%s'; thumb[id=%d]", filename.c_str(), thumb->getId());
                  std::remove( filename.c_str() );
                  return -1;
                }
              }
            }
          }
        }
      }
    }
    return result;
  }
  int Database::getAttachmentCount(integer channel) {
    DEBUG_METHOD();
    int result = 0;
    if (isOpen()) {
      Value channelId( channel );
      unique_ptr<ResultSet> allMedias(this->find(Media::TABLE_NAME, "findMediaByChannelId", &channelId, NULL));
      result = (int)allMedias->size();
    }
    return result;
  }
  string Database::getChannelName(integer channelId) {
    DEBUG_METHOD();
    string result;
    if (isOpen()) {
      shared_ptr<ChannelHeader> channelHeader(shared_ptr<ChannelHeader>( new ChannelHeader( channelId ) ));
      if ( this->contains( *channelHeader ) ) {
        this->refresh( *channelHeader );
        result = channelHeader->getName();
      }
    }
    return result;
  }
  string Database::getChannelMembers(integer channel, bool excludeMe) {
    DEBUG_METHOD();
    string result;
    if (isOpen()) {
      Value channelId( channel );
      Value hidden(false);
      std::stringstream oss;
      ChannelMember* channelMember;
      unique_ptr<ResultSet> values = this->find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL);
      for (auto it = values->begin(); it != values->end(); it++) {
        channelMember = static_cast< ChannelMember* >(*it);
        if ( !excludeMe || channelMember->getUsername() != username() ) {
          if ( oss.tellg() > 0 )
            oss << ", ";
          oss << channelMember->getUsername();
        }
      }
      result = oss.str();
    }
    return result;
  }
  bool Database::deleteChannel( const string& channelGid )
  {
    // search and remove
    Value gid( channelGid.c_str() );
    Value hidden(false);
    unique_ptr<ResultSet> values =
    this->find(ChannelHeader::TABLE_NAME, "findChannelByGid", &gid, &hidden, NULL);
    if (values->size() == 1) {
      return _deleteChannel( shared_ptr<ChannelHeader>( static_cast< ChannelHeader* >( values->takeFirst() ) ) );
    }
    return false;
  }
  bool Database::deleteChannel( integer channelId )
  {
    shared_ptr<ChannelHeader> channelHeader(shared_ptr<ChannelHeader>( new ChannelHeader( channelId ) ));
    if ( this->contains( *channelHeader ) ) {
      this->refresh( *channelHeader );
      return _deleteChannel( channelHeader );
    }
    return false;
  }
  bool Database::deleteChatMessages( integer chatId )
  {
    // search and remove
    bool result = false;
    Value chat( chatId );
    unique_ptr<ResultSet> values = this->find(Message::TABLE_NAME, "findAllMessagesInChat", &chat, NULL);
    shared_ptr<Message> message;
    vector< sqlite::BaseEntity* >::iterator it = values->begin();
    while ( it != values->end() ) {
      // initialize found member
      message = shared_ptr<Message>(static_cast< Message* >(*it));
      it = values->take(it);
      remove( *message );
      result = true;
    }
    return result;
  }

  bool Database::clearAll( void )
  {
    DEBUG_METHOD();
    if (isOpen()) {
      DEBUG_PRINT("Clear local database");
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      // 1. clear all tables
      //removeAll("PRODUCT_TBL");
      removeAll("SOURCE_TBL");
      removeAll("ID_GEN");
      removeAll("MEDIA_TBL");
      removeAll("PICTURE_TBL");
      removeAll("MEMBER_TBL");
      removeAll("COMMENT_TBL");
      removeAll("CHANNEL_MEMBER_TBL");
      removeAll("POST_TBL");
      removeAll("CHANNEL_TBL");
      removeAll("CHAT_TBL");
      removeAll("MESSAGE_TBL");
      removeAll("CHANGE_LOG_TBL");
      // 2. fill initial id_gen table
      {
        IdGen idGen("product_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("channel_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("member_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("picture_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("media_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("header_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("post_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("comment_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("channel_member_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("chat_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("message_tbl", 101L);
        insert(idGen);
      }
      {
        IdGen idGen("change_log_tbl", 101L);
        insert(idGen);
      }
      // fill initial sync_source table
      {
        SyncSource syncSource(kSourceMedia, "0");
        insert(syncSource);
      }
      {
        SyncSource syncSource(kSourceMember, "0");
        insert(syncSource);
      }
      {
        SyncSource syncSource(kSourceChannel, "0");
        insert(syncSource);
      }
      {
        SyncSource syncSource(kSourceChat, "0");
        insert(syncSource);
      }
      // commit transaction
      if ( managed ) transactionCommit();
      
      return true;
    }
    
    return false;
  }
  
  bool Database::clearAllBak( void )
  {
    DEBUG_METHOD();
    if (isOpen()) {
      DEBUG_PRINT("Clear local backup database");
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      // 2 clear all backup tables
      removeAll("SOURCE_TBL", BACKUP_DATABASE_NAME);
      removeAll("ID_GEN", BACKUP_DATABASE_NAME);
      removeAll("MEDIA_TBL", BACKUP_DATABASE_NAME);
      removeAll("MEMBER_TBL", BACKUP_DATABASE_NAME);
      removeAll("CHANNEL_TBL", BACKUP_DATABASE_NAME);
      removeAll("CHANNEL_MEMBER_TBL", BACKUP_DATABASE_NAME);
      removeAll("POST_TBL", BACKUP_DATABASE_NAME);
      removeAll("COMMENT_TBL", BACKUP_DATABASE_NAME);
      removeAll("CHAT_TBL", BACKUP_DATABASE_NAME);
      removeAll("MESSAGE_TBL", BACKUP_DATABASE_NAME);
      removeAll("CHANGE_LOG_TBL", BACKUP_DATABASE_NAME);
      // commit transaction
      if ( managed ) transactionCommit();
      
      return true;
    }
    
    return false;
  }
  
  void Database::clearChannelInternal( ChannelHeader& header )
  {
    // remove hidden channel data set if available
    Value gid( header.getGid().c_str() );
    Value channelId( header.getId() );
    Value hidden(true);
    Value visible(false);
    vector< BaseEntity* >::iterator it;
    
    unique_ptr<ResultSet> values = this->find(ChannelHeader::TABLE_NAME, "findChannelByGid", &gid, &hidden, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    
    // update header sync anchor and mark as outdated
    header.setSyncAnchor("0");
    header.setOutdated( true );
    header.clearSyncTime();
    header.clearLastSyncTime();
    header.setSyncInd( ejin::kSyncIndSynchronous );
    
    this->update( header );
    
    values = this->find(ChannelMedia::TABLE_NAME, "findChannelResources", &channelId, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      ChannelMedia* media = static_cast< ChannelMedia* >(*it);
      if (! media->hasGid()) {
        // remove local added channel medias
        this->remove( *media );
      } else {
        // unbound media resources from entities to delete below
        media->setAttachmentOutdated(true);
        media->clearPostId( );
        media->clearCommentId( );
        this->update( *media );
      }
    }
    
    // remove channel comments (normal and hidden)
    values = this->find(ChannelComment::TABLE_NAME, "findCommentsByChannel", &channelId, &hidden, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    values = this->find(ChannelComment::TABLE_NAME, "findCommentsByChannel", &channelId, &visible, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    
    // remove channel posts (normal and hidden)
    values = this->find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &hidden, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    values = this->find(ChannelPost::TABLE_NAME, "findPostsByChannel", &channelId, &visible, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    
    // remove channel member (normal and hidden)
    values = this->find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &hidden, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    values = this->find(ChannelMember::TABLE_NAME, "findChannelMembers", &channelId, &visible, NULL);
    for (it = values->begin(); it != values->end(); it++) {
      this->remove( **it );
    }
    
  }
  
  // ==========
  // Helper methods
  // ==========

  bool Database::_deleteChannel( shared_ptr<ChannelHeader> channelHeader ) throw(ejin::data_access_error)
  {
    bool success = false;
    DEBUG_METHOD();
    if (isOpen()) {
      DEBUG_PRINT("Delete local channel[id=%s]", channelHeader->getId());
      
      // begin transaction
      bool managed = isAutoCommit();
      if ( managed ) transactionBegin();
      Value hidden(false);
      Value visible(true);
      
      Value id( channelHeader->getId() );
      
      // delete all media in attached database !!!
      // all other entities gets deleted on cascading delete
      shared_ptr<Media> media;
      shared_ptr<Attachment> attachment;
      unique_ptr<ResultSet> values2 = find(Media::TABLE_NAME, "findAllMediaOfChannelId", &id, NULL);
      for (vector< BaseEntity* >::iterator it = values2->begin(); it != values2->end(); ) {
        media = shared_ptr<Media>(static_cast< Media* >(*it));
        it = values2->take(it);
        id = media->getId();
        
        unique_ptr<ResultSet> values3 = find("ATTACHMENT_TBL", "findMaster", &id, NULL);
        if ( 1 == values3->size() ) {
          attachment = shared_ptr<Attachment>( static_cast< Attachment* >( values3->takeFirst() ) );
          remove( *attachment );
        }
      }
      remove( *channelHeader );
      success = true;
      
      // commit transaction
      if ( managed ) transactionCommit();
    }
    return success;
  }

  bool Database::archiveChannelHeader( integer channelId )
  throw(data_access_error)
  {
    this->setTargetSchema( Database::TargetSchema::MainDatabase );
    ChannelHeader channelHeader( channelId);
    if ( this->contains( channelHeader ) ) {
      this->refresh( channelHeader );
      
      Value hidden(false);
      Value cId(channelHeader.getId());
      unique_ptr<ResultSet> channelMembers = this->find(ChannelMember::TABLE_NAME, "findChannelMembers", &cId, &hidden, NULL);
      unique_ptr<ResultSet> channelPosts = this->find(ChannelPost::TABLE_NAME, "findPostsByChannel", &cId, &hidden, NULL);
      unique_ptr<ResultSet> channelComments = this->find(ChannelComment::TABLE_NAME, "findCommentsByChannel", &cId, &hidden, NULL);
      unique_ptr<ResultSet> channelResources = this->find(Media::TABLE_NAME, "findMediaByHeaderId", &cId, NULL);
      
      // switch to archive schema
      this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
      
      // remove old archived channel with same global id first
      this->deleteChannel( channelHeader.getGid() );
      
      channelHeader.archive();
      this->insert( channelHeader );
      
      this->archiveChannelMembers(channelMembers, channelHeader.getId() );
      this->archiveResources(channelResources, channelHeader.getId(), -1, -1);
      this->archiveChannelPosts( channelPosts, channelHeader.getId() );
      this->archiveChannelComments( channelComments, channelHeader.getId(), -1 );
      
      return true;
    }
    return false;
  }
  void Database::archiveChannelMembers(unique_ptr<ResultSet>& channelMembers,
                                             integer channelId )
  throw(data_access_error)
  {
    ChannelMember* channelMember;
    
    // resolve full member entity of all members from main database
    this->setTargetSchema( Database::TargetSchema::MainDatabase );
    Value hidden(false);
    Value username;
    list< shared_ptr<Member> > members;
    for (auto it = channelMembers->begin(); it != channelMembers->end(); ++it) {
      channelMember = static_cast< ChannelMember* >(*it);
      switch (channelMember->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          username = Value(channelMember->getUsername());
          unique_ptr<ResultSet> values( this->find(Member::TABLE_NAME, "findMemberByUsername", &username, &hidden, NULL) );
          shared_ptr<Member> member = shared_ptr<Member>( static_cast< Member* >( values->takeFirst() ) );
          assert( member );
          members.push_back( member );
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
    
    // save to archive database
    this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
    for (auto it = channelMembers->begin(); it != channelMembers->end(); ++it) {
      channelMember = static_cast< ChannelMember* >(*it);
      switch (channelMember->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          // replace full member entity in archive
          username = Value(channelMember->getUsername());
          unique_ptr<ResultSet> values( this->find(Member::TABLE_NAME, "findMemberByUsername", &username, &hidden, NULL) );
          shared_ptr<Member> member = shared_ptr<Member>( static_cast< Member* >( values->takeFirst() ) );
          auto ii = find_if(members.begin(), members.end(), [channelMember] (const shared_ptr<Member> m) {
            return m->getUsername() == channelMember->getUsername();
          } );
          if ( member ) {
            assert( ii != members.end() );
            member->copyDataFrom( **ii );
            this->update( *member );
          } else {
            (*ii)->archive();
            this->insert( **ii );
          }
          
          channelMember->archive();
          channelMember->setChannelId( channelId );
          this->insert( *channelMember );
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  void Database::archiveChannelPosts( unique_ptr<ResultSet>& posts, integer channelId )
  throw(data_access_error)
  {
    ChannelPost* channelPost;
    Value hidden(false);
    Value postId;
    for (auto it = posts->begin(); it != posts->end(); ++it) {
      channelPost = static_cast< ChannelPost* >(*it);
      switch (channelPost->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          this->setTargetSchema( Database::TargetSchema::MainDatabase );
          postId = channelPost->getId();
          unique_ptr<ResultSet> comments = this->find(ChannelComment::TABLE_NAME, "findCommentsByPost", &postId, &hidden, NULL);
          unique_ptr<ResultSet> resources = this->find(Media::TABLE_NAME, "findMediaByPostId", &postId, NULL);
          
          this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
          channelPost->archive();
          channelPost->setChannelId( channelId );
          this->insert( *channelPost );
          
          this->archiveResources(resources, channelId, channelPost->getId(), -1 );
          this->archiveChannelComments( comments, channelId, channelPost->getId() );
          
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  void Database::archiveChannelComments( unique_ptr<ResultSet>& comments, integer channelId, integer postId )
  throw(data_access_error)
  {
    ChannelComment* channelComment;
    Value hidden(false);
    Value commentId;
    for (auto it = comments->begin(); it != comments->end(); ++it) {
      channelComment = static_cast< ChannelComment* >(*it);
      switch (channelComment->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          this->setTargetSchema( Database::TargetSchema::MainDatabase );
          commentId = channelComment->getId();
          unique_ptr<ResultSet> resources = this->find(Media::TABLE_NAME, "findMediaByCommentId", &commentId, NULL);
          
          this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
          channelComment->archive();
          channelComment->setChannelId( channelId );
          if ( postId > 0 )
            channelComment->setPostId( postId );
          this->insert( *channelComment );
          
          this->archiveResources(resources, channelId, postId, channelComment->getId() );
          
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  void Database::archiveResources(unique_ptr<ResultSet>& resources,
                                        integer channelId, integer postId, integer commentId )
  throw(data_access_error)
  {
    Resource* resource;
    for (auto it = resources->begin(); it != resources->end(); ++it) {
      resource = static_cast< Resource* >(*it);
      switch (resource->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          shared_ptr<Attachment> attachment = NULL;
          shared_ptr<Thumb> thumb = NULL;
          
          // resolve attachment and thumb data from main database
          this->setTargetSchema( Database::TargetSchema::MainDatabase );
          Value id( resource->getId() );
          unique_ptr<ResultSet> values = this->find("ATTACHMENT_TBL", "findMaster", &id, NULL);
          if ( 1 == values->size() ) {
            attachment = shared_ptr<Attachment>( static_cast< Attachment* >( values->takeFirst() ) );
            id = attachment->getId();
            attachment->archive();
            
            unique_ptr<ResultSet> values2 = this->find("THUMB_TBL", "findByAttachmentId", &id, NULL);
            if ( 1 == values2->size() ) {
              thumb = shared_ptr<Thumb>( static_cast< Thumb* >( values2->takeFirst() ) );
              thumb->archive();
            }
          }
          
          // save resource
          this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
          resource->archive();
          resource->setChannelId( channelId );
          if ( postId > 0 )
            resource->setPostId( postId );
          if ( commentId > 0 )
            resource->setCommentId( commentId );
          this->insert( *resource );
          
          // add attachment and thumb data to archive database
          if ( attachment ) {
            attachment->archive( );
            attachment->setMediaId( resource->getId() );
            this->insert( *attachment );
            if ( thumb ) {
              thumb->archive( );
              thumb->setAttachmentId(attachment->getId());
              this->insert( *thumb );
            }
          }
          
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }  
  
  bool Database::unarchiveChannelHeader( integer channelId )
  throw(data_access_error)
  {
    this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
    ChannelHeader channelHeader( channelId );
    if ( this->contains( channelHeader ) ) {
      this->refresh( channelHeader );
      
      Value hidden(false);
      Value cId(channelHeader.getId());
      unique_ptr<ResultSet> channelMembers = this->find(ChannelMember::TABLE_NAME, "findChannelMembers", &cId, &hidden, NULL);
      unique_ptr<ResultSet> channelPosts = this->find(ChannelPost::TABLE_NAME, "findPostsByChannel", &cId, &hidden, NULL);
      unique_ptr<ResultSet> channelComments = this->find(ChannelComment::TABLE_NAME, "findCommentsByChannel", &cId, &hidden, NULL);
      unique_ptr<ResultSet> channelResources = this->find(Media::TABLE_NAME, "findMediaByHeaderId", &cId, NULL);
      
      // switch to archive schema
      this->setTargetSchema( Database::TargetSchema::MainDatabase );
      
      channelHeader.unarchive();
      channelHeader.setOwner( this->username() ); // change ownership
      std::cout << channelHeader << std::endl;
      this->insert( channelHeader );
      
      this->unarchiveChannelMembers(channelMembers, channelHeader.getId() );
      this->unarchiveResources(channelResources, channelHeader.getId(), -1, -1);
      this->unarchiveChannelPosts( channelPosts, channelHeader.getId() );
      this->unarchiveChannelComments( channelComments, channelHeader.getId(), -1 );
      
      return true;
    }
    return false;
  }
  void Database::unarchiveChannelMembers(unique_ptr<ResultSet>& channelMembers,
                                               integer channelId )
  throw(data_access_error)
  {
    ChannelMember* channelMember;
    
    // resolve full member entity of all members from main database
    this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
    Value hidden(false);
    Value username;
    list< shared_ptr<Member> > members;
    for (auto it = channelMembers->begin(); it != channelMembers->end(); ++it) {
      channelMember = static_cast< ChannelMember* >(*it);
      switch (channelMember->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          username = Value(channelMember->getUsername());
          unique_ptr<ResultSet> values( this->find(Member::TABLE_NAME, "findMemberByUsername", &username, &hidden, NULL) );
          shared_ptr<Member> member = shared_ptr<Member>( static_cast< Member* >( values->takeFirst() ) );
          assert( member );
          members.push_back( member );
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
    
    // save to main database
    this->setTargetSchema( Database::TargetSchema::MainDatabase );
    for (auto it = channelMembers->begin(); it != channelMembers->end(); ++it) {
      channelMember = static_cast< ChannelMember* >(*it);
      switch (channelMember->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          // insert full member entity in archive
          username = Value(channelMember->getUsername());
          unique_ptr<ResultSet> values( this->find(Member::TABLE_NAME, "findMemberByUsername", &username, &hidden, NULL) );
          shared_ptr<Member> member = shared_ptr<Member>( static_cast< Member* >( values->takeFirst() ) );
          assert( member );
          auto ii = find_if(members.begin(), members.end(), [channelMember] (const shared_ptr<Member> m) {
            return m->getUsername() == channelMember->getUsername();
          } );
          assert( ii != members.end() );
          if ( ! member ) {
            this->insert( **ii );
          }
          
          channelMember->unarchive();
          channelMember->setChannelId( channelId );
          if ( channelMember->getUsername() == this->username() ) {
            channelMember->setMembership(kMembershipAccepted);
            channelMember->setRole(kSyncRoleAdmin);
          }
          this->insert( *channelMember );
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  void Database::unarchiveChannelPosts( unique_ptr<ResultSet>& posts, integer channelId )
  throw(data_access_error)
  {
    ChannelPost* channelPost;
    Value hidden(false);
    Value postId;
    for (auto it = posts->begin(); it != posts->end(); ++it) {
      channelPost = static_cast< ChannelPost* >(*it);
      switch (channelPost->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
          postId = channelPost->getId();
          unique_ptr<ResultSet> comments = this->find(ChannelComment::TABLE_NAME, "findCommentsByPost", &postId, &hidden, NULL);
          unique_ptr<ResultSet> resources = this->find(Media::TABLE_NAME, "findMediaByPostId", &postId, NULL);
          
          this->setTargetSchema( Database::TargetSchema::MainDatabase );
          channelPost->unarchive();
          channelPost->setChannelId( channelId );
          this->insert( *channelPost );
          
          this->unarchiveResources(resources, channelId, channelPost->getId(), -1 );
          this->unarchiveChannelComments( comments, channelId, channelPost->getId() );
          
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  void Database::unarchiveChannelComments( unique_ptr<ResultSet>& comments, integer channelId, integer postId )
  throw(data_access_error)
  {
    ChannelComment* channelComment;
    Value hidden(false);
    Value commentId;
    for (auto it = comments->begin(); it != comments->end(); ++it) {
      channelComment = static_cast< ChannelComment* >(*it);
      switch (channelComment->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
          commentId = channelComment->getId();
          unique_ptr<ResultSet> resources = this->find(Media::TABLE_NAME, "findMediaByCommentId", &commentId, NULL);
          
          this->setTargetSchema( Database::TargetSchema::MainDatabase );
          channelComment->unarchive();
          channelComment->setChannelId( channelId );
          if ( postId > 0 )
            channelComment->setPostId( postId );
          this->insert( *channelComment );
          
          this->unarchiveResources(resources, channelId, postId, channelComment->getId() );
          
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  void Database::unarchiveResources(unique_ptr<ResultSet>& resources,
                                          integer channelId, integer postId, integer commentId )
  throw(data_access_error)
  {
    Resource* resource;
    for (auto it = resources->begin(); it != resources->end(); ++it) {
      resource = static_cast< Resource* >(*it);
      switch (resource->getSyncInd()) {
        case kSyncIndSynchronous:
        case kSyncIndInsert:
        case kSyncIndUpdate:
        case kSyncIndConflict:
        {
          shared_ptr<Attachment> attachment = NULL;
          shared_ptr<Thumb> thumb = NULL;
          
          // resolve attachment and thumb data from main database
          this->setTargetSchema( Database::TargetSchema::ArchiveDatabase );
          Value id( resource->getId() );
          unique_ptr<ResultSet> values = this->find("ATTACHMENT_TBL", "findMaster", &id, NULL);
          if ( 1 == values->size() ) {
            attachment = shared_ptr<Attachment>( static_cast< Attachment* >( values->takeFirst() ) );
            id = attachment->getId();
            attachment->unarchive();
            
            unique_ptr<ResultSet> values2 = this->find("THUMB_TBL", "findByAttachmentId", &id, NULL);
            if ( 1 == values2->size() ) {
              thumb = shared_ptr<Thumb>( static_cast< Thumb* >( values2->takeFirst() ) );
              thumb->unarchive();
            }
          }
          
          // save resource
          this->setTargetSchema( Database::TargetSchema::MainDatabase );
          resource->unarchive();
          resource->setChannelId( channelId );
          if ( postId > 0 )
            resource->setPostId( postId );
          if ( commentId > 0 )
            resource->setCommentId( commentId );
          this->insert( *resource );
          
          // add attachment and thumb data to archive database
          if ( attachment ) {
            attachment->setMediaId( resource->getId() );
            attachment->unarchive( );
            this->insert( *attachment );
            if ( thumb ) {
              thumb->unarchive( );
              thumb->setAttachmentId(attachment->getId());
              this->insert( *thumb );
            }
          }
          
          break;
        }
        case kSyncIndDelete:
          break;
      }
    }
  }
  
}
