/* Copyright (C) 2012 - 2016 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHAT_ENTITY_H__
#define __EJIN_CHAT_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class ChatProfile;
  class ChatRepository;
  class ChatService;
  class Message;
  
  /**
   * Entity Class for CHAT_TBL
   */
  class Chat: public BaseEntity, public IBusinessData, public DataSchema<Chat> {
    friend class Database;
    friend class ChatProfile;
    friend class ChatRepository;
    friend class ChatService;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "CHAT_TBL";
    
    // ctors
    Chat( void );
    Chat( const integer id );
    Chat( const string& name );
    Chat( const Chat& record );
    // dtor
    ~Chat( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Chat& operator=( const Chat& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Chat(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool hasContent() const { return false; }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );
    
    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    sqlite::Value* valueByXmlNode( const char* name );

    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
  public:

    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_STRING_PROPERTY_INTF( Gid, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( MessageCount, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( Acknowledged, 3 )
    ACCESS_INTEGER_PROPERTY_INTF( Loaded, 4 )
    ACCESS_STRING_PROPERTY_INTF( Key, 5 )
    ACCESS_STRING_PROPERTY_INTF( Partner, 6 )
    ACCESS_INTEGER_PROPERTY_INTF( PartnerAcknowledged, 7 )
    ACCESS_STRING_PROPERTY_INTF( FingerPrint, 8 )
    ACCESS_STRING_PROPERTY_INTF( PartnerFingerPrint, 9 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 10 )

    const string getPartnerKey( void ) const { return this->partnerKey_.toString(); }
    void setPartnerKey( const string& key ) { this->partnerKey_.str( key ); }
    void clearPartnerKey( void ) { this->partnerKey_.setNull(); }
    bool hasPartnerKey( void ) const { return ! this->partnerKey_.isNull(); }
    
    const string getHref( void ) const { return this->href_.toString(); }
    void setHref( const string& href ) { this->href_.str( href ); }
    void clearHref( void ) { this->href_.setNull(); }
    bool hasHref( void ) const { return ! this->href_.isNull(); }
    
    list< shared_ptr<Message> >& getMessages( void ) { return this->messages_; }
    const list< shared_ptr<Message> >& getMessages( void ) const { return this->messages_; }
    void setMessages(const list< shared_ptr<Message> >& messages) { this->messages_ = messages; }

    void prepareAllStatementsOn( const char* schema );

    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Chat Variables
    
    bool isEmpty_;
    sqlite::Value partnerKey_;
    sqlite::Value href_;
    
    // List of posts of the channel.
    list< shared_ptr<Message> > messages_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif //__EJIN_CHAT_ENTITY_H__
