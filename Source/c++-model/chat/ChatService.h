/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHAT_SERVICE_H__
#define __EJIN_CHAT_SERVICE_H__

#include "ChatRepository.h"

namespace ejin
{
  using ejin::Chat;
  using ejin::Message;
  
  /**
   * Methods to resolve mchannel comment entities by state.
   */
  class ChatService: public ChatRepository {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChatService( Database& db, ChangeLogRepository& changeLogRepository, MemberProfile& memberProfile ):
      ChatRepository( db, changeLogRepository, memberProfile ) {}
    ~ChatService( void ) {}

    /** Returns the synchronization anchor of the last synchronization of members. */
    const string getLock( void ) const;
    
    /**
     * Updates the synchronization anchor of the list of visible members.
     */
    void setLock( const string& syncAnchor, const string& username, const jtime& localTime )
    throw(data_access_error);

    unique_ptr<SyncSource> exportNewMessage( const Chat& chat, list< shared_ptr<Message> >& container )
    const throw(conflict_error, data_access_error);

    bool importUpdatedChat( const Modifications<Chat>& container, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);

    integer importUpdatedMessage( Chat& chat, const list< shared_ptr<Message> >& message, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);

    bool revert( const string& path )
    throw(data_access_error);
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    /**
     * Return the overall media synchronization state.
     */
    unique_ptr<SyncSource> getSyncSource( void ) const
    throw(data_access_error);
    
    /**
     * Updates the overall media synchronization state.
     */
    void setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
    throw(data_access_error);    

    /**
     * Merge updates into the specified current message list.
     */
    list< shared_ptr<Message> > mergeMessageLists( const list< shared_ptr<Message> >& current, const list< shared_ptr<Message> >& updates, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);

  };
  
}

#endif // __EJIN_CHAT_SERVICE_H__
