/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Message.h"

#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

#include "SqlitePreparedTable.h"

namespace ejin
{
  using ser::ISerializableEntity;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const sqlite::Attribute DataSchema<Message>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("CHAT_ID", type_int, flag_not_null),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("NO", type_int, flag_not_null),
    Attribute("SENDER", type_text, flag_not_null),
    Attribute("CONTENT", type_text, flag_not_null),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("SEND_TIME", type_time, flag_none),
    Attribute("ACK", type_bool, flag_not_null, Value(false)),
    Attribute("SECURE", type_bool, flag_not_null, Value(false)),
    Attribute("ENCRYPTED", type_bool, flag_not_null, Value(false)),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<Message>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Message::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findNewMessages"] = "CHAT_ID = ? AND SEND_TIME IS NULL ORDER BY ID";
      tMap["findAllMessagesInChat"] = "CHAT_ID = ? ORDER BY NO, ID";
      tMap["findMessagesInChatSince"] = "CHAT_ID = ?1 AND (NO == 0 OR NO >= ?2) ORDER BY NO, ID";
    }
    return tMap;
  }
  const map<string,string> Message::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "SELECT ifnull(max(NO),0) "
      "FROM " << schema << ".message_tbl "
      "WHERE chat_id = ?";
      tMap["maxMessageNo"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "SELECT count(*) "
      "FROM " << schema << ".message_tbl "
      "WHERE chat_id = ?";
      tMap["messageCount"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) ) {
      std::ostringstream ss;
      ss << "UPDATE " << schema << ".message_tbl "
      "SET no = (no - ?) WHERE no != 0";
      tMap["shiftMessageNo"] = ss.str();
    }
    return tMap;
  }
  
  // ==========
  // Crypto Interface
  // ==========
  
  bool Message::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if (this->isEncrypted()) {
      if ( crypto && crypto->hasSessionKey( ) ) {
        if (this->hasContent()) {
          this->setContent( crypto->decrypt( this->getContent() ) );
        }
        this->contentSize_ = 0;
        this->setEncrypted( false );
        this->setSecure( true );
      } else {
        this->setSecure( true );
      }
      return true;
    } else {
      this->setSecure( false );
    }
    return false;
  }
  
  bool Message::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if ( this->isSecure() ) {
      if ( crypto && crypto->hasSessionKey( ) ) {
        if (this->hasContent()) {
          this->contentSize_ = this->value("CONTENT")->size();
          this->setContent( crypto->encrypt( this->getContent() ) );
        }
        this->setEncrypted( true );
        return true;
      } else {
        _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "no session key to encrypt message");
      }
    } else {
      this->contentSize_ = 0;
      this->setEncrypted( false );
    }
    return false;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Message::Message( void ): BaseEntity(TABLE_DDL, false), isEmpty_(false)
  {
    this->setSyncInd( kSyncIndInsert );
    this->clearId( );
    this->clearChatId( );
    this->setNo( 0 );
    this->setAcknowledged( 0 );
    this->setSecure( false );
    this->setEncrypted( false );
    this->setMyMessage( false );
    this->setSend( false );
    this->setDelivered( false );
  }
  Message::Message( const integer id ): Message()
  {
    setId(id);
  }
  Message::Message( const Message& record ): Message()
  {
    this->operator=(record);
  }
  Message::~Message( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Message& Message::operator=( const Message& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, ChatId);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, No);
      COPY_PROPERTY((*this), rhs, Sender);
      COPY_PROPERTY((*this), rhs, Content);
      COPY_BOOL_PROPERTY((*this), rhs, Acknowledged);
      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, SendTime);
      COPY_BOOL_PROPERTY((*this), rhs, Secure);
      COPY_BOOL_PROPERTY((*this), rhs, Encrypted);
      this->isDelivered_ = rhs.isDelivered_;
      this->isSend_ = rhs.isSend_;
      this->isMyMessage_ = rhs.isMyMessage_;
    }
    return *this;
  }
  
  // comparison
  bool Message::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Message& mrhs = dynamic_cast<const Message&> (rhs);
    if (this->hasId() && mrhs.hasId())
      return (this->getId() == mrhs.getId());
    else if (this->hasNo() && mrhs.hasNo())
      return (this->getNo() == mrhs.getNo());
    return false;
  }
  bool Message::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Message& mrhs = dynamic_cast<const Message&> (rhs); // throws std::bad_cast
    if (this->hasId() && mrhs.hasId())
      return (this->getId() < mrhs.getId());
    else if (this->hasNo() && mrhs.hasNo())
      return (this->getNo() < mrhs.getNo());
    return this->getId() ? true : false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  // Database schema
  const char* Message::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& Message::tableFields( void ) const { return TABLE_DDL; }
  
  // XML schema
  const string& Message::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& Message::xmlFields( void ) const { return XML_DDL; }
  
  bool Message::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const Message& mrhs = dynamic_cast<const Message&> (rhs);
      COPY_PROPERTY((*this), mrhs, No);
      COPY_PROPERTY((*this), mrhs, Sender);
      COPY_PROPERTY((*this), mrhs, Content);
      COPY_PROPERTY((*this), mrhs, SendTime);
      COPY_BOOL_PROPERTY((*this), mrhs, Acknowledged);
      COPY_BOOL_PROPERTY((*this), mrhs, Secure);
      COPY_BOOL_PROPERTY((*this), mrhs, Encrypted);

      return true;
    }
    return false;
  }
  bool Message::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    Message& rhs( (Message&)arg );
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }

    if ( this->getNo() != rhs.getNo() )
      return false;
    if ( this->getSender() != rhs.getSender() )
      return false;
    if ( this->getSender() != rhs.getSender() )
      return false;
    if ( this->getContent() != rhs.getContent() )
      return false;
    if ( this->getCreateTime() != rhs.getCreateTime() )
      return false;
    if ( this->getSendTime() != rhs.getSendTime() )
      return false;
    if ( this->isAcknowledged() != rhs.isAcknowledged() )
      return false;
    return true;
  }
  bool Message::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    return false;
  }
  void Message::archive( void )
  {
  }
  void Message::unarchive( void )
  {
  }
  void Message::clearData( void )
  {
    this->clearSender( );
    this->clearContent( );
    this->clearCreateTime( );
    this->clearSendTime( );
    this->setAcknowledged( false );
    this->setNo( 0 );
    this->setSyncInd( kSyncIndInsert );
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // ======= JSON Mapping
  
  bool Message::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasContent() ) {
      j["content"] = string(this->value("CONTENT")->str(), this->value("CONTENT")->size());
    }
    if (this->isEncrypted()) {
      j["encrypted"] = true;
    }
    return true;
  }
  /*
  bool Message::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if ( this->hasContent() ) {
      json_object_set_new( node, "content", json_stringn_nocheck( this->value("CONTENT")->str(),
                                                                 this->value("CONTENT")->size() ) );
    }
    if (this->isEncrypted()) {
      json_object_set_new( node, "encrypted", json_true() );
    }
    return true;
  }
  */

  bool Message::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("message") ) {
        return unmarshalJson( j["message"] );
      } else {
        if ( j.contains("content") ) {
          this->value("CONTENT")->str( j["content"] );
        }
        if ( j.contains("sender") ) {
          this->value("SENDER")->str( j["sender"] );
        }
        if ( j.contains("no")) {
          this->value("NO")->num( j["no"] );
        }
        if ( j.contains("encrypted") ) {
          this->value("ENCRYPTED")->bol( j["encrypted"] );
          this->setSecure( this->isEncrypted() );
        }
        if ( j.contains("send_time") ) {
          this->value("SEND_TIME")->str( j["send_time"] );
        }
        return true;
      }
    }
    return false;
  }
  /*
  bool Message::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* root = json_object_get(node, "message");
    if (root) {
      return unmarshalJson( root );
      
    } else {
      json_t* value;
      value = json_object_get(node, "content");
      if ( json_is_string(value) ) {
        this->value("CONTENT")->bytea( json_string_value(value), json_string_length(value) );
      }
      
      value = json_object_get(node, "sender");
      if ( json_is_string(value) ) {
        this->value("SENDER")->bytea( json_string_value(value), json_string_length(value) );
      }
      value = json_object_get(node, "no");
      if ( json_is_number(value) ) {
        this->value("NO")->num( (integer) json_integer_value(value) );
      }
      value = json_object_get(node, "encrypted");
      if ( json_is_string(value) ) {
        this->value("ENCRYPTED")->bytea( json_string_value(value), json_string_length(value) );
        this->setSecure( this->isEncrypted() );
      }
      value = json_object_get(node, "send_time");
      if ( json_is_string(value) ) {
        this->value("SEND_TIME")->bytea( json_string_value(value), json_string_length(value) );
      }
      return true;
    }
  }
  */

  // ======= XML Mapping
  
  const string Message::XML_NAME   = "message";
  const ser::XmlAttribute Message::XML_FIELDS[] =
  {
    ser::XmlAttribute("content", "CONTENT" ),
    ser::XmlAttribute("sender", "SENDER" ),
    ser::XmlAttribute("no", "NO" ),
    ser::XmlAttribute("encrypted", "ENCRYPTED" ),
    ser::XmlAttribute("send_time", "SEND_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet Message::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  ISerializableEntity* Message::entityByXmlNode( const char* name, int position )
  {
    if ( std::strcmp("message", name) == 0 ) {
      return this;
    }
    return SerializableEntity::entityByXmlNode(name, position);
  }
  void Message::endXmlNode( const char* name )
  {
    if ( strcmp("message", name) == 0 ) {
      this->setSecure( this->isEncrypted() );
    }
  }
  
  bool Message::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("message"); // start element tag
    
    // serialize properties
    if ( this->isEncrypted() ) {
      xml << attr("encrypted") << "true";
    }
    if ( this->hasContent() ) {
      xml << tag("content") << chardata() << this->getContent() << endtag();
    }

    xml << ser::endtag(); // end element tag
    return true;
  }

  const sqlite::jtime Message::getSendTime2() const
  {
    if (this->isPending()) {
      return getCreateTime();
    }
    return getSendTime();
  }
  bool Message::hasSendTime2() const
  {
    if (this->isPending()) {
      return hasCreateTime();
    }
    return hasSendTime();
  }

}
