/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MESSAGE_LIST_H
#define __EJIN_MESSAGE_LIST_H

#include "SqliteBaseEntity.h"
#include "SerializableEntity.h"

namespace ejin
{
  using ser::ISerializableEntity;
  using ser::XmlStream;
  
  class Message;
  
  /**
   * Container class wrapping of search result of the same business type.
   */ 
  class MessageList: public ISerializableEntity {
    friend ostream& operator<<( ostream& ostr, const MessageList& rhs );
    
    // ==========
    // Ctor / Dtor
    // ==========
  public:
    
    // ctors
    MessageList( void );
    MessageList( list< shared_ptr<Message> > list );
    // dtor
    ~MessageList( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    MessageList& operator=( const MessageList& rhs );
    // comparision
    bool operator==( const MessageList& rhs ) const;
    // debug
    string toString( void ) const;
    
    bool isEmpty( void ) const { return _list.empty(); };
    size_t size( void ) const { return _list.size(); };
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool unmarshalJson( json_t* node );
    //bool marshalJson( json_t* node ) const;
    
    ISerializableEntity* entityByXmlNode( const char* name, int position );
    sqlite::Value* valueByXmlNode( const char* name );
    void endXmlNode( const char* name );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const; 
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    list< shared_ptr<Message> >& getList( void ) const   { return this->_list; }
    void setList( const list< shared_ptr<Message> >& list )      { this->_list = list; }

    // ==========
    // Protected Interface
    // ==========
  private:
    
    // result list
    mutable list< shared_ptr<Message> >  _list;

  };
  
  // debugging
  ostream& operator<<( ostream& ostr, const MessageList& rhs );
  
}

#endif // __EJIN_MESSAGE_LIST_H
