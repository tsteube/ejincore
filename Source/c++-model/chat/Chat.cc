/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Chat.h"

#include "SqlitePreparedTable.h"

#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::ISerializableEntity;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const sqlite::Attribute DataSchema<Chat>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("GID", type_text, flag_not_null),
    Attribute("MESSAGE_COUNT", type_int, flag_none),
    Attribute("ACK", type_int, flag_none),
    Attribute("LOADED", type_int, flag_none),
    Attribute("KEY", type_text, flag_none),
    Attribute("PARTNER", type_text, flag_none),
    Attribute("PARTNER_ACK", type_int, flag_none),
    Attribute("PKEY_FINGER_PRINT", type_text, flag_none),
    Attribute("PARTNER_PKEY_FINGER_PRINT", type_text, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<Chat>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Chat::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findChatById"] = "ID = ?";
      tMap["findAllOpened"] = "GID IS NOT NULL";
      tMap["findChatByGid"] = "GID = ?";
      tMap["findChatByPartner"] = "PARTNER = ?";
    }
    return tMap;
  }
  const map<string,string> Chat::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    return tMap;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Chat::Chat( void ): BaseEntity(TABLE_DDL, false), isEmpty_(false), partnerKey_(), href_()
  {
    this->clearId( );
    this->clearGid( );
    this->setMessageCount((integer) 0);
    this->setAcknowledged((integer) 0);
    this->setLoaded((integer) 0);
    this->setPartnerAcknowledged((integer) 0);
    this->clearModifyTime( );
  }
  Chat::Chat( const integer id ): Chat()
  {
    setId(id);
  }
  Chat::Chat( const string& gid ): Chat()
  {
    setGid( gid );
  }
  Chat::Chat( const Chat& record ): Chat()
  {
    this->operator=(record);
  }
  Chat::~Chat( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Chat& Chat::operator=( const Chat& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Gid);
      COPY_PROPERTY((*this), rhs, MessageCount);
      COPY_PROPERTY((*this), rhs, Acknowledged);
      COPY_PROPERTY((*this), rhs, Loaded);
      COPY_PROPERTY((*this), rhs, Key);
      COPY_PROPERTY((*this), rhs, Partner);
      COPY_PROPERTY((*this), rhs, ModifyTime);
    }
    return *this;
  }
  
  // comparison
  bool Chat::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Chat& mrhs = dynamic_cast<const Chat&> (rhs);
    if (this->hasId() && mrhs.hasId())
      return (this->getId() == mrhs.getId());
    else if (this->hasGid() && mrhs.hasGid())
      return (this->getGid() == mrhs.getGid());
    return false;
  }
  bool Chat::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Chat& mrhs = dynamic_cast<const Chat&> (rhs); // throws std::bad_cast
    if (this->hasId() && mrhs.hasId())
      return (this->getId() < mrhs.getId());
    else if (this->hasGid() && mrhs.hasGid())
      return (this->getGid() < mrhs.getGid());
    return this->getId() ? true : false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  // Database schema
  const char* Chat::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& Chat::tableFields( void ) const { return TABLE_DDL; }
  
  // XML schema
  const string& Chat::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& Chat::xmlFields( void ) const { return XML_DDL; }
  
  bool Chat::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const Chat& mrhs = dynamic_cast<const Chat&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Gid);
      COPY_PROPERTY((*this), mrhs, Href);
      COPY_PROPERTY((*this), mrhs, Acknowledged);
      COPY_PROPERTY((*this), mrhs, PartnerAcknowledged);
      COPY_PROPERTY((*this), mrhs, MessageCount);
      COPY_PROPERTY((*this), mrhs, ModifyTime);

      return true;
    }
    return false;
  }
  bool Chat::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    Chat& rhs( (Chat&)arg );
    if ( this->getGid() != rhs.getGid() )
      return false;
    if ( this->getAcknowledged() != rhs.getAcknowledged() )
      return false;
    if ( this->getPartnerAcknowledged() != rhs.getPartnerAcknowledged() )
      return false;
    if ( this->getMessageCount() != rhs.getMessageCount() )
      return false;
    return true;
  }
  bool Chat::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void Chat::archive( void )
  {
  }
  void Chat::unarchive( void )
  {
  }
  void Chat::clearData( void )
  {
    this->clearGid( );
    this->clearHref( );
    this->setAcknowledged( 0 );
    this->setPartnerAcknowledged( 0 );
    this->setMessageCount( 0 );
    this->setLoaded( 0 );
    this->clearModifyTime( );
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // ======= JSON Mapping

  bool Chat::marshalJson( nlohmann::json& node ) const
  {
    json j;
    
    if ( this->hasPartner() ) {
      j["partner"] = string(this->value("PARTNER")->str(), this->value("PARTNER")->size());
    }
    if (this->hasKey()) {
      j["key"] = string(this->value("KEY")->str(), this->value("KEY")->size());
    }
    if (this->hasPartnerKey()) {
      j["partner_key"] = this->getPartnerKey();
    }
    node["chat"] = j;
    
    return true;
  }
  /*
  bool Chat::marshalJson( json_t* node ) const
  {
    assert( node );
    
    json_t *chat = json_object();
    if (this->hasPartner()) {
      json_object_set_new( chat, "partner", json_stringn_nocheck( this->value("PARTNER")->str(),
                                                                  this->value("PARTNER")->size() ) );
    }
    if (this->hasKey()) {
      json_object_set_new( chat, "key", json_stringn_nocheck( this->value("KEY")->str(),
                                                              this->value("KEY")->size() ) );
    }
    if (this->hasPartnerKey()) {
      string key = this->getPartnerKey();
      json_object_set_new( chat, "partner_key", json_stringn_nocheck( key.c_str(), key.size() ) );
    }
    
    json_object_set_new(node, "chat", chat);

    return true;
  }
  */

  bool Chat::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("id")) {
        this->value("GID")->str( j["id"] );
      }
      if ( j.contains("partner")) {
        this->value("PARTNER")->str( j["partner"] );
      }
      if ( j.contains("key")) {
        this->value("KEY")->str( j["key"] );
      }
      if ( j.contains("count")) {
        this->value("MESSAGE_COUNT")->num( j["count"] );
      }
      if ( j.contains("ack")) {
        this->value("ACK")->num( j["ack"] );
      }
      if ( j.contains("partner_ack")) {
        this->value("PARTNER_ACK")->num( j["partner_ack"] );
      }
      if ( j.contains("href")) {
        this->setHref( j["href"] );
      }
      if ( j.contains("finger_print")) {
        this->value("PKEY_FINGER_PRINT")->str( j["finger_print"] );
      }
      if ( j.contains("partner_finger_print")) {
        this->value("PARTNER_PKEY_FINGER_PRINT")->str( j["partner_finger_print"] );
      }

      return true;
    }
    
    return false;
  }
  /*
  bool Chat::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "id");
    if ( json_is_string(value) ) {
      this->value("GID")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "partner");
    if ( json_is_string(value) ) {
      this->value("PARTNER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "key");
    if ( json_is_string(value) ) {
      this->value("KEY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "count");
    if ( json_is_number(value) ) {
      this->value("MESSAGE_COUNT")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "ack");
    if ( json_is_number(value) ) {
      this->value("ACK")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "partner_ack");
    if ( json_is_number(value) ) {
      this->value("PARTNER_ACK")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "href");
    if ( value ) {
      this->setHref( json_string_value(value) );
    }
    value = json_object_get(node, "finger_print");
    if ( json_is_string(value) ) {
      this->value("PKEY_FINGER_PRINT")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "partner_finger_print");
    if ( json_is_string(value) ) {
      this->value("PARTNER_PKEY_FINGER_PRINT")->bytea( json_string_value(value), json_string_length(value) );
    }

    return true;
  }
  */

  // ======= XML Mapping
  
  const string Chat::XML_NAME   = "chat";
  const ser::XmlAttribute Chat::XML_FIELDS[] =
  {
    ser::XmlAttribute("id", "GID" ),
    ser::XmlAttribute("count", "MESSAGE_COUNT" ),
    ser::XmlAttribute("partner", "PARTNER" ),
    ser::XmlAttribute("ack", "ACK" ),
    ser::XmlAttribute("partner_ack", "PARTNER_ACK" ),
    ser::XmlAttribute("key", "KEY" ),
    ser::XmlAttribute("partner_finger_print", "PARTNER_PKEY_FINGER_PRINT" ),
    ser::XmlAttribute("finger_print", "PKEY_FINGER_PRINT" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet Chat::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  Value* Chat::valueByXmlNode( const char* name )
  {
    if (strcmp(name, "href") == 0) {
      return &this->href_;
    } else {
      return SerializableEntity::valueByXmlNode(name);
    }
  }
  
  bool Chat::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("chat"); // start element tag
    
    // serialize properties
    if (this->hasPartner()) {
      xml << attr("partner") << this->getPartner();
    }
    if (this->hasPartnerKey()) {
      xml << attr("partner_key") << this->getPartnerKey();
    }
    if (this->hasKey()) {
      xml << attr("key") << this->getKey();
    }
    
    xml << ser::endtag(); // end element tag
    return true;
  }
}
