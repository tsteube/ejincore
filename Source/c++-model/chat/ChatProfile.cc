/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatProfile.h"

#include "UsingEjinTypes.h"
#include "Utilities.h"
#include "Chat.h"
#include "Message.h"
#include "MemberProfile.h"

#include <iostream>
#include <algorithm>    // std::for_each

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<Message> > ChatProfile::findNewMessages( const Chat& chat ) const throw(data_access_error)
  {
    assert( chat.hasId() );
    
    // call entity finder
    Value chatId( chat.getId() );
    unique_ptr<ResultSet> values( this->db_.find( Message::TABLE_NAME, "findNewMessages", &chatId, NULL ) );
    
    auto it = values->begin();
    list< shared_ptr<Message> > result;
    while (it != values->end()) {
      result.push_back(shared_ptr<Message>(static_cast< Message* >(*it)));
      it = values->take( it );
    }

    return result;
  }
  
  list< shared_ptr<Message> > ChatProfile::findAllMessages( const Chat& chat ) const throw(data_access_error)
  {
    assert( chat.hasId() );
    
    // call entity finder
    Value chatId( chat.getId() );
    unique_ptr<ResultSet> values( this->db_.find( Message::TABLE_NAME, "findAllMessagesInChat", &chatId, NULL ) );

    list< shared_ptr<Message> > result;
    auto it = values->begin();
    while (it != values->end()) {
      result.push_back(shared_ptr<Message>(static_cast< Message* >(*it)));
      it = values->take( it );
    }
    
    return result;
  }
  
  list< shared_ptr<Message> > ChatProfile::findMessagesSince( const Chat& chat, integer number ) const throw(data_access_error)
  {
    assert( chat.hasId() );
    
    // call entity finder
    Value chatId( chat.getId() );
    Value no( number );
    unique_ptr<ResultSet> values( this->db_.find( Message::TABLE_NAME, "findMessagesInChatSince", &chatId, &no, NULL ) );
    
    list< shared_ptr<Message> > result;
    auto it = values->begin();
    while (it != values->end()) {
      result.push_back(shared_ptr<Message>(static_cast< Message* >(*it)));
      it = values->take( it );
    }
    
    return result;
  }
  
  shared_ptr<Chat> ChatProfile::loadChatByGid( const string& gid ) const throw(data_access_error)
  {
    assert( !gid.empty() );
    
    // call entity finder
    Value gidValue( gid );
    unique_ptr<ResultSet> values( db_.find(Chat::TABLE_NAME, "findChatByGid", &gidValue, NULL) );
    
    shared_ptr<Chat> chat;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      chat = shared_ptr<Chat>(static_cast< Chat* >(*it));
      it = values->take(it);
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate chat [gid=%s].",  gid.c_str());
    
    return chat;
  }
  
  shared_ptr<Chat> ChatProfile::loadChatByPartner( const string& username ) const throw(data_access_error)
  {
    assert( !username.empty() );
    
    // call entity finder
    Value usernameValue( username );
    unique_ptr<ResultSet> values( db_.find(Chat::TABLE_NAME, "findChatByPartner", &usernameValue, NULL) );
    
    shared_ptr<Chat> chat;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      chat = shared_ptr<Chat>(static_cast< Chat* >(*it));
      it = values->take(it);
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate chat [partner=%s].",  username.c_str());
    
    return chat;
  }
  
  shared_ptr<Chat> ChatProfile::loadChatByPartner2( const string& username ) const throw(data_access_error)
  {
    shared_ptr<Chat> chat( this->loadChatByPartner( username ) );
    if ( ! chat ) {
      shared_ptr<Member> member( this->memberProfile_.loadByUsername( username ) );
      if ( ! member ) {
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "unknown member name: %s.",  username.c_str());
      }

      // create new local chat instance
      chat = shared_ptr<Chat>(new Chat());
      chat->setPartner( username );
      this->db_.insert(*chat);
    }
    return chat;
  }
  shared_ptr<Chat> ChatProfile::loadById( integer chatId ) const throw(data_access_error)
  {
    // call entity finder
    Value id( chatId );
    unique_ptr<ResultSet> values( db_.find(Chat::TABLE_NAME, "findChatById", &id, NULL) );
    
    shared_ptr<Chat> chat;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      chat = shared_ptr<Chat>(static_cast< Chat* >(*it));
      it = values->take(it);
    }
    return chat;
  }

  int ChatProfile::lastMessageNoIn( const Chat& chat ) const throw(data_access_error)
  {
    assert( chat.hasId() );
    
    // call entity finder
    Value chatId( chat.getId() );
    unique_ptr<Value> result( db_.valueOf( Message::TABLE_NAME, "maxMessageNo", &chatId, NULL ) );
    return (int)result->num();
  }
  shared_ptr<Chat> ChatProfile::exportChat( const integer chatId ) const
  throw(data_access_error)
  {
    // load channel header first
    shared_ptr<Chat> chat = this->loadById( chatId );
    if ( ! chat )
    {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "chat[id=%ld] not found", chatId);
    }
    
    auto messages( this->findAllMessages( *chat ) );
    list< shared_ptr<Message> > visibleMessages(messages.size());
    auto it = std::copy_if( messages.begin(), messages.end(), visibleMessages.begin(),
                           [](shared_ptr<Message> message){return message->getSyncInd() != kSyncIndDelete;} );
    visibleMessages.resize(std::distance(visibleMessages.begin(),it));  // shrink container to new size

    // sort for output
    visibleMessages.sort([](const shared_ptr<Message> & rhs, const shared_ptr<Message> & lhs) {
      if ( rhs->getNo() == 0 || lhs->getNo() == 0 ) {
        // sort all unsend messages to the end
        if ( lhs->getNo() == 0 ) {
          return rhs->getId() < lhs->getId();
        } else {
          return false;
        }
      }
      return rhs->getNo() < lhs->getNo();
    });
    
    // extend message properties resolved from chat context
    std::for_each( visibleMessages.begin(), visibleMessages.end(), [chat] (shared_ptr<Message> message)
    {
      message->setPending( message->getCreateTime() > chat->getModifyTime() );
      message->setSend( message->getNo() != 0 );
      message->setDelivered( message->getNo() > 0 && chat->getPartnerAcknowledged() >= message->getNo() );
      message->setMyMessage( message->getSender() != chat->getPartner() );
    } );
    chat->setMessages( visibleMessages );
    
    return chat;
  }

}
