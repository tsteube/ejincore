/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatService.h"

#include "UsingEjinTypes.h"
#include "Modifications.h"
#include "Chat.h"
#include "Message.h"
#include "Utilities.h"
#include "SyncSource.h"
#include "ChangeLogRepository.h"

namespace ejin
{
  inline void markEmpty( shared_ptr<Chat> m) {
    m->setEmpty( true );
  }

  // ==========
  // Public Interface
  // ==========
  
  const string ChatService::getLock( void ) const
  {
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    assert (source->hasSyncAnchor());
    return source->getSyncAnchor();
  }
  
  void ChatService::setLock( const string& syncAnchor, const string& username, const jtime& localTime )
  throw(data_access_error)
  {
    assert( ! syncAnchor.empty() );
    // increment overall sync anchor
    setSyncSource( syncAnchor, username, localTime );
  }

  unique_ptr<SyncSource> ChatService::exportNewMessage( const Chat& chat, list< shared_ptr<Message> >& container )
  const throw(conflict_error, data_access_error)
  {
    assert( container.empty() );
    
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    // update in/out set
    container = this->findNewMessages( chat );

    return source;
  }
  
  bool ChatService::importUpdatedChat( const Modifications<Chat>& container, const string& syncAnchor, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( ! container.getSyncAnchor().empty() );
    
    list< shared_ptr<Chat> > chatToUpdate;
    chatToUpdate.insert(chatToUpdate.end(), container.getAdded().begin(), container.getAdded().end());
    chatToUpdate.insert(chatToUpdate.end(), container.getUpdated().begin(), container.getUpdated().end());
    
    // mark to remove
    for_each (container.getRemoved().begin(), container.getRemoved().end(), markEmpty);
    chatToUpdate.insert(chatToUpdate.end(), container.getRemoved().begin(), container.getRemoved().end());
    
    list<string> conflicts;
    
    // now update medias
    for (list< shared_ptr<Chat> >::iterator it=chatToUpdate.begin(); it != chatToUpdate.end(); it++) {
      this->update( **it, syncAnchor, syncTime );
    }
    
    return !chatToUpdate.empty();
  }

  integer ChatService::importUpdatedMessage( Chat& chat, const list< shared_ptr<Message> >& messages, const string& syncAnchor, const jtime& syncTime )
  throw(data_access_error)
  {
    integer maxNumber = 0;
    if ( !messages.empty() ) {
      integer minNumber = INT_MAX;
      // mark acknowledge flag of send messages
      for (auto it=messages.begin(); it != messages.end(); it++) {
        // find min/max number
        if ( (*it)->getNo() < minNumber ) {
          minNumber = (*it)->getNo();
        }
        if ( (*it)->getNo() > maxNumber ) {
          maxNumber = (*it)->getNo();
        }

        // fix link to chat instance
        (*it)->setSyncInd( kSyncIndSynchronous );
        (*it)->setChatId( chat.getId() );
        if ( !(*it)->isEmpty() ) {
          assert( (*it)->hasSender() );
          if ( (*it)->getSender() == this->db_.username() )
          {
            // my message
            (*it)->setAcknowledged( true );
          }
          else
          {
            // received message
            (*it)->setAcknowledged( chat.getPartnerAcknowledged() >= (*it)->getNo() );
          }
        }
      }
      
      // mark all for acknowledged
      chat.setAcknowledged( maxNumber );
      chat.setMessageCount( maxNumber );
      chat.setLoaded( maxNumber );
      this->db_.update( chat );
      
      // merge messages with current list in local database
      // get all local message since min number from input collection
      list< shared_ptr<Message> > current( this->findMessagesSince( chat.getId(), minNumber ) );
      mergeMessageLists( current, messages, syncAnchor, syncTime );
    }
    
    return maxNumber;
  }
  
  bool ChatService::revert( const string& path )
  throw(data_access_error)
  {
    std::list<string> segments;
    Utilities::split(segments, path, "/");
    std::list<string>::iterator it=segments.begin();
    if ( it != segments.end() && (*it).empty() ) it++; // skip leading empty segment
    if ( it != segments.end() && strcasecmp((*it).c_str(), "chat" ) == 0 && ++it != segments.end())
    {
      return ChatRepository::revert( *it );
    }
    return false;
  }

  // ==========
  // Helper methods
  // ==========
  
  unique_ptr<SyncSource> ChatService::getSyncSource( void ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceChat));
    this->db_.refresh( *entity ); // throws data_access_error if not found
    return entity;
  }
  
  void ChatService::setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceChat));
    this->db_.refresh( *entity );  // throws data_access_error if not found
    
    entity->setSyncAnchor(syncAnchor);
    entity->setModifyTime(localTime);
    entity->setUsername(username);
    this->db_.update( *entity );
  }

  list< shared_ptr<Message> > ChatService::mergeMessageLists( const list< shared_ptr<Message> >& current, const list< shared_ptr<Message> >& updates, const string& syncAnchor, const jtime& syncTime )
  throw(data_access_error)
  {
    list< shared_ptr<Message> > result( current );
    
    for (auto it=result.begin(); it != result.end(); ) {
      shared_ptr<Message> messageEntity(static_cast<shared_ptr<Message>>(*it));
      shared_ptr<Message> update = Utilities::findElementIn (updates, *messageEntity);
      if ( update ) {
        if ( !update->isEmpty() ) {
          messageEntity->copyDataFrom( *update );
          this->db_.update( *messageEntity );
          this->changeLogRepository_.update( **it, syncAnchor, syncTime );
        } else {
          this->db_.remove( *messageEntity );
          this->changeLogRepository_.remove( **it, syncAnchor, syncTime );
        }
        it++;
      } else {
        it = result.erase( it );
      }
    }
    
    for (auto it=updates.begin(); it != updates.end(); it++) {
      shared_ptr<Message> entity(static_cast<shared_ptr<Message>>(*it));
      if ( ! Utilities::findElementIn (current, *entity) ) {
        this->db_.insert( *entity );
        this->changeLogRepository_.add( *entity, syncAnchor, syncTime );
        result.push_back( entity );
      }
    }
    
    return result;
  }
  

}
