/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChatRepository.h"

#include <iostream>

#include "UsingEjinTypes.h"
#include "Chat.h"
#include "Member.h"
#include "Message.h"
#include "Utilities.h"
#include "MemberProfile.h"
#include "ChangeLogRepository.h"

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
  
  void ChatRepository::commitChat( Chat& chat, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( chat.hasId() );
    assert( chat.hasGid() );
    
    shared_ptr<Chat> chatEntity( shared_ptr<Chat>(new Chat()) );
    chatEntity->setId( chat.getId() );
    this->db_.refresh( *chatEntity );
    chatEntity->setGid( chat.getGid() );
    chatEntity->setKey( chat.getKey() );
    this->db_.update( *chatEntity );
    this->changeLogRepository_.outdated( *chatEntity, syncAnchor, syncTime );
  }

  shared_ptr<Chat> ChatRepository::loadChatByGid( const string& gid ) const throw(data_access_error)
  {
    shared_ptr<Chat> chat( this->ChatProfile::loadChatByGid( gid ) );
    if ( ! chat ) {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "no chat with gid: %s.",  gid.c_str());
    }
    return chat;
  }
  
  shared_ptr<Chat> ChatRepository::loadChatByPartner( const string& username ) const throw(data_access_error)
  {
    shared_ptr<Chat> chat( this->ChatProfile::loadChatByPartner( username ) );
    if ( ! chat ) {
      shared_ptr<Member> member( this->memberProfile_.loadByUsername( username ) );
      if ( ! member ) {
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "unknown member name: %s.",  username.c_str());
      }

      // create new local chat instance
      chat = shared_ptr<Chat>(new Chat());
      chat->setPartner( username );
      this->db_.insert(*chat);
    }
    return chat;
  }

  shared_ptr<Chat> ChatRepository::update( Chat& chat, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( chat.hasGid() );
    
    shared_ptr<Member> myself( memberProfile_.loadByUsername( this->db_.username() ) );
    if (myself)
      chat.setFingerPrint( myself->getFingerPrint() );

    shared_ptr<Chat> chatEntity( this->ChatProfile::loadChatByGid( chat.getGid() ) );
    if ( chatEntity )
    {
      if ( chat.isEmpty() )
      {
        this->removeChat( *chatEntity );
      }
      else
      {
        if (this->updateChat( *chatEntity, chat, syncTime ))
          this->changeLogRepository_.outdated( *chatEntity, syncAnchor, syncTime );
      }
    }
    else if ( !chat.isEmpty() && chat.hasPartner() )
    {
      chatEntity = this->ChatProfile::loadChatByPartner( chat.getPartner() );
      if ( chatEntity ) {
        if (this->removeChat( *chatEntity )) {
          chatEntity = this->addChat( chat ); // complete new data set
        } else {
          this->updateChat( *chatEntity, chat, syncTime ); // save old messages before updating with new ones
        }
      } else {
        chatEntity = this->addChat( chat );
      }
      this->changeLogRepository_.outdated( *chatEntity, syncAnchor, syncTime );
    }

    return chatEntity;
  }
  
  // -----
  // Protected
  
  shared_ptr<Chat> ChatRepository::addChat( Chat& chat )
  throw(data_access_error)
  {
    assert( chat.hasGid() );
    
    // create new entity with unique global key
    shared_ptr<Chat> chatEntity = shared_ptr<Chat>(new Chat( chat ));
    chatEntity->clearId(); // generate new local database identifier
    // persist entity
    this->db_.insert(*chatEntity);
    
    // update input data
    chat.setId( chatEntity->getId() );
    
    return chatEntity;
  }

  bool ChatRepository::updateChat( Chat& chatEntity, Chat& chat, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( chatEntity.hasId() );
    assert( chat.hasGid() );
    
    chatEntity.copyDataFrom( chat );
    chatEntity.setModifyTime( syncTime );
    if ( chat.hasKey() ) {
      chatEntity.setFingerPrint(chat.getFingerPrint());
      chatEntity.setPartnerFingerPrint(chat.getPartnerFingerPrint());
      if ( chat.getKey().empty() ) {
        // reset key
        chatEntity.clearKey( );
      } else {
        if ( chatEntity.hasKey() )
          // add new key(s)
          chatEntity.setKey( chat.getKey() + " " + chatEntity.getKey() );
        else
          chatEntity.setKey( chat.getKey() );
      }
    }

    this->db_.update( chatEntity );

    return true;
  }
  
  bool ChatRepository::removeChat( Chat& chatEntity )
    throw(data_access_error)
  {
    assert( chatEntity.hasId() );
    
    Value chatId( chatEntity.getId() );
    unique_ptr<Value> result( db_.valueOf( Message::TABLE_NAME, "messageCount", &chatId, NULL ) );
    
    if ( result->num() == 0 ) {
      // delete entity itself
      this->db_.remove(chatEntity);
      return true;
      
    } else {
      int maxMessageNo = this->lastMessageNoIn( chatEntity );
      if (maxMessageNo > 0) {
        // call entity finder
        Value offset( (integer)(maxMessageNo + 1) );
        unique_ptr<Value> result2( db_.valueOf( Message::TABLE_NAME, "shiftMessageNo", &offset, NULL ) );
      }
      chatEntity.clearGid( );
      chatEntity.setLoaded( 0 );
      chatEntity.setMessageCount( 0 );
      chatEntity.setAcknowledged( 0 );
      chatEntity.setPartnerAcknowledged( 0 );
      chatEntity.clearHref( );
      chatEntity.clearKey( );
      chatEntity.clearPartnerKey( );
      this->db_.update( chatEntity );
      return false;
    }
  }

  bool ChatRepository::revert( const string& gid )
  throw(data_access_error)
  {
    assert( ! gid.empty() );

    shared_ptr<Chat> chatEntity( this->ChatProfile::loadChatByPartner( gid ) );
    if ( chatEntity )
    {
      chatEntity->clearGid( );
      // persist entity
      this->db_.persist(*chatEntity);
      return true;
    }
    return false;
  }

}
