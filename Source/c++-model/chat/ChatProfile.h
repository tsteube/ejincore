/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHAT_PROFILE_H__
#define __EJIN_CHAT_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

namespace ejin
{
  using ejin::Chat;
  using ejin::Message;
  
  /**
   * Methods to resolve mchannel comment entities by state.
   */
  class ChatProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChatProfile( Database& db, MemberProfile& memberProfile ): db_( db ), memberProfile_(memberProfile) {}
    virtual ~ChatProfile( void ) {}

    /**
     * Load the chat with specified partner.
     */
    virtual shared_ptr<Chat> loadChatByPartner( const string& username ) const
    throw(data_access_error);
    virtual shared_ptr<Chat> loadChatByPartner2( const string& username ) const
    throw(data_access_error);
    shared_ptr<Chat> loadById( integer chatId ) const
    throw(data_access_error);

    /**
     * Load the chat by its global identifier.
     */
    virtual shared_ptr<Chat> loadChatByGid( const string& gid ) const
    throw(data_access_error);

    /**
     * Returns the last message number in the target chat.
     */
    int lastMessageNoIn( const Chat& chat )
    const throw(data_access_error);

    /**
     * Find new local message not delivered yet.
     */
    list< shared_ptr<Message> > findNewMessages( const Chat& chat ) const
    throw(data_access_error);

    /**
     * Find all message in the specified chat.
     */
    list< shared_ptr<Message> > findAllMessages( const Chat& chat ) const
    throw(data_access_error);

    /**
     * Find all message in the specified chat.
     */
    list< shared_ptr<Message> > findMessagesSince( const Chat& chat, integer number ) const
    throw(data_access_error);
    
    /**
     * Extract chat instance with all valid messages and some additional logical properties.
     */
    shared_ptr<Chat> exportChat( const integer chatId ) const
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    // --------
    // Member Variables 
    
    Database& db_;
    MemberProfile&  memberProfile_;

  private:
    
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ChatProfile );

  };
  
}

#endif // __EJIN_CHAT_PROFILE_H__
