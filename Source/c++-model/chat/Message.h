/* Copyright (C) 2012 - 2016 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MESSAGE_ENTITY_H__
#define __EJIN_MESSAGE_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class MessageProfile;
  class MessageRepository;
  class MessageService;
  
  /**
   * Entity Class for CHAT_TBL
   */
  class Message: public BaseEntity, public IBusinessData, public DataSchema<Message> {
    friend class Database;
    friend class MessageProfile;
    friend class MessageRepository;
    friend class MessageService;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "MESSAGE_TBL";

    // ctors
    Message( void );
    Message( const integer id );
    Message( const Message& record );
    // dtor
    ~Message( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Message& operator=( const Message& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Message(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    void endXmlNode( const char* name );
    
    //sqlite::Value* valueByXmlNode( const char* name );
    ISerializableEntity* entityByXmlNode( const char* name, int position );

    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);

  public:

    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( ChatId, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( No, 3 )
    ACCESS_STRING_PROPERTY_INTF( Sender, 4 )
    ACCESS_STRING_PROPERTY_INTF( Content, 5 )
    ACCESS_TIME_PROPERTY_INTF( CreateTime, 6 )
    ACCESS_TIME_PROPERTY_INTF( SendTime, 7 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Acknowledged, 8 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Secure, 9 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Encrypted, 10 )
    
    const sqlite::jtime getSendTime2() const;
    bool hasSendTime2() const;

    integer getContentSize( void ) const { return (this->contentSize_ > 0) ? this->contentSize_ : this->value("CONTENT")->size(); }
    void setContentSize( integer size ) { this->contentSize_ = size; }

    bool isDelivered() const { return this->isDelivered_; }
    void setDelivered( bool delivered ) { this->isDelivered_ = delivered; }

    bool isPending() const { return this->isPending_; }
    void setPending( bool pending ) { this->isPending_ = pending; }
    
    bool isSend() const { return this->isSend_; }
    void setSend( bool send ) { this->isSend_ = send; }

    bool isMyMessage() const { return this->isMyMessage_; }
    void setMyMessage( bool myMessage ) { this->isMyMessage_ = myMessage; }

    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Message Variables
    
    bool      isEmpty_;
    bool      isDelivered_;
    bool      isSend_;
    bool      isPending_;
    bool      isMyMessage_;
    integer   contentSize_;

    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif //__EJIN_MESSAGE_ENTITY_H__
