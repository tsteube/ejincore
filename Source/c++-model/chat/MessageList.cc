/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MessageList.h"
#include "UsingEjinTypes.h"

#include <sstream>

#include "Utilities.h"
#include "Media.h"
#include "ChannelHeader.h"
#include "Message.h"

namespace ejin
{
  // ==========
  // Ctor / Dtor
  // ==========
  
  MessageList::MessageList( list< shared_ptr<Message> > list ): _list(list) { }
  
  MessageList::MessageList( void ): _list() { }
  
  MessageList::~MessageList( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  MessageList& MessageList::operator=( const MessageList& rhs )
  {
    if (this != &rhs) {
      this->setList( rhs.getList() );
    }
    return *this;
  }
  
  bool MessageList::operator==( const MessageList& rhs ) const
  {
    if (this == &rhs) 
      return true;
    if (this->_list.size() != rhs._list.size())
      return false;
    return true;
  }
  
  string MessageList::toString( void ) const
  {
    ostringstream buf;
    buf << "MessageList" << "["
    << ",#result=" << this->_list.size()
    << "}";
    return buf.str();
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  bool MessageList::marshalJson( nlohmann::json& node ) const
  {
    json a = json::array();
    for (list< shared_ptr<Message> >::const_iterator it=this->_list.begin();
         it != this->_list.end(); it++) {
      json j2({});
      if ( (*it)->marshalJson( j2 ) ) {
        a.push_back(j2);
      }
      node["messages"] = a;
    }
    return true;
  }
  /*
  bool MessageList::marshalJson( json_t* node ) const
  {
    assert( node );
    
    json_t* array = json_array();
    json_object_set_new( node, "messages", array );
    for (list< shared_ptr<Message> >::const_iterator it=this->_list.begin();
         it != this->_list.end(); it++) {
      json_t* child = json_object();
      if ( (*it)->marshalJson( child ) ) {
        json_array_append_new( array, child );
      }
    }
    
    return true;
  }
  */

  bool MessageList::unmarshalJson( const nlohmann::json& node )
  {
    json j;
    if ( node.contains("result_set")) {
      j = node["result_set"];
      if ( j.is_array() ) {
        for (int i=0; i<j.size(); i++) {
          shared_ptr<Message> entity = Utilities::findOrAppend( this->getList(), i );
          if (! entity || ! entity->unmarshalJson( j[i] )) {
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }
  /*
  bool MessageList::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* resultSet = json_object_get(node, "result_set");
    if (json_is_array(resultSet))
    {
      size_t index;
      json_t* value;
      json_array_foreach(resultSet, index, value) {
        shared_ptr<Message> entity = Utilities::findOrAppend( this->getList(), index );
        if (!entity || ! entity->unmarshalJson( value )) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  */

  // implements a simple state engine to handle entities for add, update and remove
  ISerializableEntity* MessageList::entityByXmlNode( const char* name, int position )
  {
    if (strcmp(name, "messages") == 0)
      return this;
    
    this->_list.push_back( shared_ptr<Message>(new Message()) );
    return this->_list.back().get();
  }
  
  Value* MessageList::valueByXmlNode( const char* name )
  {
    return NULL; 
  }
  
  // update state engine
  void MessageList::endXmlNode( const char* name )
  {  
  }
  
  // generate core XML structure
  bool MessageList::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << ser::prolog("utf-8");
    xml << tag("messages"); // start root tag
    
    // print out added entities if any
    if (! this->_list.empty()) {
      for (typename list< shared_ptr<Message> >::const_iterator it=this->_list.begin(); it != this->_list.end(); it++) {
        (*it)->marshalXml( xml, aesKey, keyLength );
      }
    }
    
    xml << endtag("messages"); // end root tag
    
    return true;
  }
  
  // ==========
  // Debugging
  // ==========
  
  ostream& operator<<( ostream& ostr, const MessageList& rhs ) {
    ostr << rhs.toString();
    return ostr;
  }
}


