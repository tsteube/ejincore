/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHAT_REPOSITORY_H__
#define __EJIN_CHAT_REPOSITORY_H__

#include "ChatProfile.h"

namespace ejin
{
  using ejin::Chat;
  using ejin::Message;
  
  /**
   * Methods to resolve mchannel comment entities by state.
   */
  class ChatRepository: public ChatProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChatRepository( Database& db, ChangeLogRepository& changeLogRepository, MemberProfile& memberProfile ): ChatProfile( db, memberProfile), changeLogRepository_(changeLogRepository) {}
    ~ChatRepository( void ) {}

    /**
     * Load the chat with specified partner.
     */
    shared_ptr<Chat> loadChatByPartner( const string& username ) const throw(data_access_error);
    
    /**
     * Load the chat by its global identifier.
     */
    shared_ptr<Chat> loadChatByGid( const string& gid ) const throw(data_access_error);

    shared_ptr<Chat> update( Chat& chat, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error);

    void commitChat( Chat& chat, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error);

    bool revert( const string& gid ) throw(data_access_error);

    // ==========
    // Protected Interface
    // ==========
  protected:
    
    ChangeLogRepository& changeLogRepository_;
    
    shared_ptr<Chat> addChat( Chat& chat ) throw(data_access_error);

    bool updateChat( Chat& chatEntity, Chat& chat, const jtime& syncTime ) throw(data_access_error);

    bool removeChat( Chat& chatEntity ) throw(data_access_error);

    list< shared_ptr<Message> > mergeCollections( list< shared_ptr<Message> > current, list< shared_ptr<Message> > updates )
    throw(data_access_error);
    
    template <typename T>
    list< shared_ptr<T> > mergeLists( list< shared_ptr<T> > current, list< shared_ptr<T> > updates )
    throw(data_access_error);

  };
  
}

#endif // __EJIN_CHAT_REPOSITORY_H__
