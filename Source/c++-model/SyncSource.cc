/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SyncSource.h"
#include "UsingEjinTypes.h"

#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<SyncSource>::TABLE_FIELDS[] =
  {
    Attribute("SYNC_SOURCE", type_text, flag_primary_key),
    Attribute("USERNAME", type_text, flag_not_null),
    Attribute("SYNC_ANCHOR", type_text, flag_not_null),
    Attribute("STATE", type_int, flag_none),
    Attribute("ASSERTIONS", type_int, flag_not_null, Value((integer)0)),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute()
  };
  template <> const AttributeSet DataSchema<SyncSource>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  SyncSource::SyncSource( void ): BaseEntity(TABLE_DDL, false)
  {
    init();
  }
  SyncSource::SyncSource( const SourceEntity& id ): BaseEntity(TABLE_DDL, false)
  {
    init();
    setSource(id);
  }
  SyncSource::SyncSource( const SourceEntity& id, const string& anchor ): BaseEntity(TABLE_DDL, false)
  {
    init();
    setSource(id);
    setSyncAnchor(anchor);
  }
  SyncSource::SyncSource( const SyncSource& record ): BaseEntity(record)
  {
    this->operator=(record);
  }
  SyncSource::~SyncSource( void ) { }
  void SyncSource::init( void )
  {
    this->clearSyncSource( );
    this->clearSource( );
    this->clearUsername( );
    this->clearSyncAnchor( );
    this->setState( 0 );
    this->setAssertions( 0 );
    this->clearModifyTime( );
  }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  SyncSource& SyncSource::operator=( const SyncSource& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      if (rhs.hasSource())
        this->setSource( rhs.getSource() );
      else
        this->clearSource();
      COPY_PROPERTY((*this), rhs, SyncSource);
      COPY_PROPERTY((*this), rhs, Username);
      COPY_PROPERTY((*this), rhs, SyncAnchor);
      COPY_PROPERTY((*this), rhs, State);
      COPY_PROPERTY((*this), rhs, Assertions);
      COPY_PROPERTY((*this), rhs, ModifyTime);
    }
    return *this;
  }
  
  // comparison
  bool SyncSource::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const SyncSource& mrhs = dynamic_cast<const SyncSource&> (rhs);
    return getSource() == mrhs.getSource();
  }
  bool SyncSource::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const SyncSource& mrhs = dynamic_cast<const SyncSource&> (rhs); // throws std::bad_cast if not of type Member&
    return this->getSource() < mrhs.getSource();
  }
  
  // ==========
  // Getter / Setter
  // ==========
  
  const char* SyncSource::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& SyncSource::tableFields( void ) const { return TABLE_DDL; }
  
  const SourceEntity SyncSource::getSource( void ) const
  {
    string src = getSyncSource();
    if (src == "CHANNEL")
      return kSourceChannel;
    if (src == "MEDIA")
      return kSourceMedia;
    if (src == "MEMBER")
      return kSourceMember;
    if (src == "CHAT")
      return kSourceChat;
    return kSourceNull;
  }
  void SyncSource::setSource( const SourceEntity& source )
  {
    switch (source) {
      case kSourceChannel:
        setSyncSource("CHANNEL");
        break;
      case kSourceMedia:
        setSyncSource("MEDIA");
        break;
      case kSourceMember:
        setSyncSource("MEMBER");
        break;
      case kSourceChat:
        setSyncSource("CHAT");
        break;
      default:
        clearSyncSource();
        break;
    }
  }
  
}
