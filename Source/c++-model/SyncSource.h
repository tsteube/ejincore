/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_SYNC_SOURCE2_H__
#define __EJIN_SYNC_SOURCE2_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  
  /**
   * ejin Entity for SOURCE_TBL
   */
  class SyncSource: public sqlite::BaseEntity, DataSchema<SyncSource>  {
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "SOURCE_TBL";

    // constructors
    SyncSource( void );
    SyncSource( const SourceEntity& id );
    SyncSource( const SourceEntity& id, const string& anchor );
    SyncSource( const SyncSource& record );
    // dtor
    ~SyncSource( void );
  private:
    // initilize object
    void init( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    SyncSource& operator=( const SyncSource& rhs );
    // clone
    BaseEntity* clone( void ) const { return new SyncSource(*this); }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_STRING_PROPERTY_INTF( SyncSource, 0 )
    ACCESS_STRING_PROPERTY_INTF( Username, 1 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( State, 3 )
    ACCESS_INTEGER_PROPERTY_INTF( Assertions, 4 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 5 )
    
    const SourceEntity getSource( void ) const;
    void setSource( const SourceEntity& _source );
    void clearSource( void ) { return setNull(0); }
    bool hasSource( void ) const { return ! isNull(0); }
    
    // ==========
    // Protected Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // schema DDL
    const sqlite::AttributeSet& tableFields( void ) const;
    
    const char* tableName( void ) const;
    const char* tableMainSchema( void ) const { return MAIN_DATABASE_NAME; }
    const char* tableBackupSchema( void ) { return BACKUP_DATABASE_NAME; }
    const char* tableArchiveSchema( void ) const { return ARCHIVE_DATABASE_NAME; }
  };
  
}

#endif //__EJIN_SYNC_SOURCE_H__
