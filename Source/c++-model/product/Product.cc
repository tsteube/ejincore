/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Product.h"

#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::ISerializableEntity;
  
  // ==========
  // Schema DDL
  // ==========  
  
  template <> const sqlite::Attribute DataSchema<Product>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("NO", type_int, flag_none),
    Attribute("IDENTIFIER", type_text, flag_not_null),
    Attribute("NAME", type_text, flag_not_null),
    Attribute("ENABLED", type_bool, flag_not_null, Value(false)),
    Attribute("TITLE", type_text, flag_none),
    Attribute("INFORMATION", type_text, flag_none),
    Attribute("PRICE", type_float, flag_none),
    Attribute("PRICE_LOCALE", type_text, flag_none),
    Attribute("SUBSCRIPTION_PERIOD", type_int, flag_none),
    Attribute("SUBSCRIPTION_PERIOD_UNIT", type_int, flag_none, Value((integer)0), DurationPeriodEnumMapping),
    Attribute("MAX_CHANNEL_COUNT", type_int, flag_none),
    Attribute("MAX_ATTACHMENT_COUNT", type_int, flag_none),
    Attribute("MAX_ATTACHMENT_SIZE", type_int, flag_none),
    Attribute("MAX_TOTAL_ATTACHMENT_SIZE", type_int, flag_none),
    Attribute("MAX_TEXT_CONTENT_LENGTH", type_int, flag_none),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<Product>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Product::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findProductById"] = "ID = ?";
      tMap["findProductByIdentifier"] = "IDENTIFIER = ?";
      tMap["findProductByAll"] = "1 ORDER BY NO";
      tMap["BackupSet"] = "1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findProductByIdentifierBak"] = "IDENTIFIER = ?";
      tMap["BackupSetBak"] = "1";
    }
    return tMap;
  }

  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Product::Product( void ): BaseEntity(TABLE_DDL, false), isEmpty_(false)
  {
    setEnabled(false);
    setNo(0);
  }
  Product::Product( const integer id ): Product()
  {
    setId(id);
    setEnabled(false);
    setNo(0);
  }
  Product::Product( const string& identifier ): Product()
  {
    setIdentifier(identifier);
    setEnabled(false);
    setNo(0);
  }
  Product::Product( const Product& record ): Product()
  {
    this->operator=(record);
  }
  Product::~Product( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Product& Product::operator=( const Product& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Identifier);
      COPY_PROPERTY((*this), rhs, Name);
      COPY_BOOL_PROPERTY((*this), rhs, Enabled);
      COPY_PROPERTY((*this), rhs, Title);
      COPY_PROPERTY((*this), rhs, Information);
      COPY_PROPERTY((*this), rhs, Price);
      COPY_PROPERTY((*this), rhs, PriceLocale);
      COPY_PROPERTY((*this), rhs, SubscriptionPeriod);
      COPY_PROPERTY((*this), rhs, SubscriptionPeriodUnit);
      COPY_PROPERTY((*this), rhs, MaxChannelCount);
      COPY_PROPERTY((*this), rhs, MaxAttachmentCount);
      COPY_PROPERTY((*this), rhs, MaxAttachmentSize);
      COPY_PROPERTY((*this), rhs, MaxTotalAttachmentSize);
      COPY_PROPERTY((*this), rhs, MaxTextContentLength);
    }
    return *this;
  }
  
  // comparison
  bool Product::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Product& mrhs = dynamic_cast<const Product&> (rhs);
    if (this->hasIdentifier() && mrhs.hasIdentifier())
      return (this->getIdentifier() == mrhs.getIdentifier());
    else if (this->hasId() && mrhs.hasId())
      return (this->getId() == mrhs.getId());
    return false;
  }
  bool Product::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Product& mrhs = dynamic_cast<const Product&> (rhs); // throws std::bad_cast
    if (this->hasIdentifier() && mrhs.hasIdentifier())
      return (this->getIdentifier() < mrhs.getIdentifier());
    else if (this->hasId() && mrhs.hasId())
      return (this->getId() < mrhs.getId());
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  // Database schema
  const char* Product::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& Product::tableFields( void ) const { return TABLE_DDL; }
  
  // XML schema
  const string& Product::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& Product::xmlFields( void ) const { return XML_DDL; }
  
  bool Product::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const Product& mrhs = dynamic_cast<const Product&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Identifier);
      COPY_PROPERTY((*this), mrhs, Name);
      COPY_BOOL_PROPERTY((*this), mrhs, Enabled);
      COPY_PROPERTY((*this), mrhs, Title);
      COPY_PROPERTY((*this), mrhs, Information);
      COPY_PROPERTY((*this), mrhs, Price);
      COPY_PROPERTY((*this), mrhs, PriceLocale);
      COPY_PROPERTY((*this), mrhs, SubscriptionPeriod);
      COPY_PROPERTY((*this), mrhs, SubscriptionPeriodUnit);
      COPY_PROPERTY((*this), mrhs, MaxChannelCount);
      COPY_PROPERTY((*this), mrhs, MaxAttachmentCount);
      COPY_PROPERTY((*this), mrhs, MaxAttachmentSize);
      COPY_PROPERTY((*this), mrhs, MaxTotalAttachmentSize);
      COPY_PROPERTY((*this), mrhs, MaxTextContentLength);

      return true;
    }
    return false;
  }
  bool Product::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    Product& rhs( (Product&)arg );
    if ( this->getIdentifier() != rhs.getIdentifier() )
      return false;
    if ( this->getPrice() != rhs.getPrice() )
      return false;
    if ( this->getSubscriptionPeriod() != rhs.getSubscriptionPeriod() )
      return false;
    if ( this->getMaxChannelCount() != rhs.getMaxChannelCount() )
      return false;
    if ( this->getMaxAttachmentCount() != rhs.getMaxAttachmentCount() )
      return false;
    if ( this->getMaxAttachmentSize() != rhs.getMaxAttachmentSize() )
      return false;
    if ( this->getMaxTotalAttachmentSize() != rhs.getMaxTotalAttachmentSize() )
      return false;
    if ( this->getMaxTextContentLength() != rhs.getMaxTextContentLength() )
      return false;
    return true;
  }
  bool Product::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void Product::archive( void )
  {
  }
  void Product::unarchive( void )
  {
  }
  void Product::clearData( void )
  {
    this->clearIdentifier( );
    this->clearName( );
    this->clearEnabled( );
    this->clearTitle( );
    this->clearInformation( );
    this->clearPrice( );
    this->clearPriceLocale( );
    this->clearSubscriptionPeriod( );
    this->clearSubscriptionPeriodUnit( );
    this->clearMaxChannelCount( );
    this->clearMaxAttachmentCount( );
    this->clearMaxAttachmentSize( );
    this->clearMaxTotalAttachmentSize( );
    this->clearMaxTextContentLength( );
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // ======= JSON Mapping
  
  bool Product::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("identifier") ) {
        this->value("IDENTIFIER")->str( j["identifier"] );
      }
      if ( j.contains("name") ) {
        this->value("NAME")->str( j["name"] );
      }
      if ( j.contains("enabled") ) {
        this->value("ENABLED")->bol( j["enabled"] );
      }
      if ( j.contains("subscription_period")) {
        this->value("SUBSCRIPTION_PERIOD")->num( j["subscription_period"] );
      }
      if ( j.contains("subscription_period_unit")) {
        this->value("SUBSCRIPTION_PERIOD_UNIT")->str( j["subscription_period_unit"] );
      }
      if ( j.contains("max_channel_count")) {
        this->value("MAX_CHANNEL_COUNT")->num( j["max_channel_count"] );
      }
      if ( j.contains("max_attachment_count")) {
        this->value("MAX_ATTACHMENT_COUNT")->num( j["max_attachment_count"] );
      }
      if ( j.contains("max_total_attachment_size")) {
        this->value("MAX_TOTAL_ATTACHMENT_SIZE")->num( j["max_total_attachment_size"] );
      }
      if ( j.contains("max_attachment_size")) {
        this->value("MAX_ATTACHMENT_SIZE")->num( j["max_attachment_size"] );
      }
      if ( j.contains("max_text_content_length")) {
        this->value("MAX_TEXT_CONTENT_LENGTH")->num( j["max_text_content_length"] );
      }

      return true;
    }
    return false;
  }
  /*
  bool Product::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "identifier");
    if ( json_is_string(value) ) {
      this->value("IDENTIFIER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "name");
    if ( json_is_string(value) ) {
      this->value("NAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "enabled");
    this->value("ENABLED")->bol( json_is_true(value) );
    value = json_object_get(node, "subscription_period");
    if ( json_is_number(value) ) {
      this->value("SUBSCRIPTION_PERIOD")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "subscription_period_unit");
    if ( json_is_string(value) ) {
      this->value("SUBSCRIPTION_PERIOD_UNIT")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "max_channel_count");
    if ( json_is_number(value) ) {
      this->value("MAX_CHANNEL_COUNT")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_attachment_count");
    if ( json_is_number(value) ) {
      this->value("MAX_ATTACHMENT_COUNT")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_total_attachment_size");
    if ( json_is_number(value) ) {
      this->value("MAX_TOTAL_ATTACHMENT_SIZE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_attachment_size");
    if ( json_is_number(value) ) {
      this->value("MAX_ATTACHMENT_SIZE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_text_content_length");
    if ( json_is_number(value) ) {
      this->value("MAX_TEXT_CONTENT_LENGTH")->num( (integer) json_integer_value(value) );
    }

    return true;
  }
  */
  // ======= XML Mapping
  
  const string Product::XML_NAME   = "product";
  const ser::XmlAttribute Product::XML_FIELDS[] =
  {
    ser::XmlAttribute("identifier", "IDENTIFIER" ),
    ser::XmlAttribute("enabled", "ENABLED" ),
    ser::XmlAttribute("name", "NAME" ),
    ser::XmlAttribute("subscription_period", "SUBSCRIPTION_PERIOD" ),
    ser::XmlAttribute("subscription_period_unit", "SUBSCRIPTION_PERIOD_UNIT" ),
    ser::XmlAttribute("max_channel_count", "MAX_CHANNEL_COUNT" ),
    ser::XmlAttribute("max_attachment_count", "MAX_ATTACHMENT_COUNT" ),
    ser::XmlAttribute("max_attachment_size", "MAX_ATTACHMENT_SIZE" ),
    ser::XmlAttribute("max_total_attachment_size", "MAX_TOTAL_ATTACHMENT_SIZE" ),
    ser::XmlAttribute("max_text_content_length", "MAX_TEXT_CONTENT_LENGTH" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet Product::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
}
