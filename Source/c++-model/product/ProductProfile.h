/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PRODUCT_PROFILE_H__
#define __EJIN_PRODUCT_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

//class ChannelHeaderRevertTest;
namespace ejin
{
  class Product;
  /**
   * Methods to resolve member entities by state.
   */
  class ProductProfile {
//    friend class ::ChannelHeaderRevertTest;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    ProductProfile( Database& db ):  db_( db ) { }
    // dtor
    virtual ~ProductProfile( void ) {}
    
    /**
     * Find all conflicted member data sets. If the member instance was edited locally 
     * and on the server both data sets
     * are kept to resolve the conflict by user interaction.
     */
    list< shared_ptr<Product> > findAllProducts( void ) const
    throw(data_access_error);    

    /**
     * Load a single member entity
     */
    shared_ptr<Product> loadProductByIdentifier( const string& name ) const
    throw(data_access_error);
    
    // --------
    // Product Variables 

  protected:
    shared_ptr<Product> loadProductById( const integer id ) const
    throw(data_access_error);    

    Database& db_;

  private:
  
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ProductProfile );
    
  };
  
}

#endif // __EJIN_PRODUCT_PROFILE_H__
