/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductService.h"
#include "UsingEjinTypes.h"

#include "Product.h"
#include "Utilities.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool ProductService::updateAllProducts( const list< shared_ptr<Product> >& products )
  throw(data_access_error)
  {
    bool result = false;
    list< shared_ptr<Product> > oldProductList( findAllProducts( ) );
    int index = 0;
    for (list< shared_ptr<Product> >::const_iterator it=products.begin(); it != products.end(); ++it) {
      shared_ptr<Product> prod = updateProduct( **it );
      prod->setNo(index++);
      oldProductList.remove_if([prod](shared_ptr<Product> p){ return *prod == *p; });
      result = true;
    }
    for (list< shared_ptr<Product> >::iterator it=oldProductList.begin(); it != oldProductList.end(); ++it) {
      this->db_.remove( **it );
      result = true;
    }
    return result;
  }

}
