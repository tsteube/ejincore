/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PRODUCT_SERVICE_H__
#define __EJIN_PRODUCT_SERVICE_H__

#include "ProductRepository.h"

namespace ejin
{
  /**
   * Update operations on the member entity. There are a create, update and delete operation to manages members bound to a
   * channel instance.
   */
  class ProductService: public ProductRepository {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    ProductService( Database& db ): ProductRepository( db ) {}
    // dtor
    ~ProductService( void ) {};
    
    bool updateAllProducts( const list< shared_ptr<Product> >& product )
    throw(data_access_error);

    // ==========
    // Private Interface
    // ==========

  private:

  };
  
}

#endif // __EJIN_PRODUCT_SERVICE_H__
