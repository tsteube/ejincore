/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductRepository.h"
#include "UsingEjinTypes.h"

#include "Product.h"
#include "Utilities.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  shared_ptr<Product> ProductRepository::updateProduct( Product& product )
  throw(data_access_error)
  {
    assert( product.hasIdentifier() );

    // call entity finder
    Value name(product.getIdentifier());
    unique_ptr<ResultSet> values( db_.find(Product::TABLE_NAME, "findProductByIdentifier", &name,  NULL) );
    shared_ptr<Product> productEntity = loadProductByIdentifier( product.getIdentifier() );
    if ( productEntity ) {
      productEntity->copyDataFrom( product );
      this->db_.update( *productEntity );
    } else {
      productEntity = shared_ptr<Product>(new Product(product));
      productEntity->clearId();
      this->db_.insert( *productEntity );
    }

    return productEntity;
  }

}
