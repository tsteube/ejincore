/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ProductProfile.h"
#include "UsingEjinTypes.h"

#include "Product.h"
#include "Utilities.h"

namespace ejin
{
  inline list< shared_ptr<Product> > convertToProductSet( unique_ptr<ResultSet> values )
  {
    list< shared_ptr<Product> > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
      result.push_back( shared_ptr<Product>( static_cast< Product* >( *it ) ) );
      it = values->take( it );
    }
    return result;
  }  
  
  // ==========
  // Public Interface
  // ==========
  
  shared_ptr<Product> ProductProfile::loadProductByIdentifier( const string& productName ) const throw(data_access_error)
  {
    // call entity finder
    
    Value name(productName);
    unique_ptr<ResultSet> values( db_.find(Product::TABLE_NAME, "findProductByIdentifier", &name, NULL) );
    shared_ptr<Product> productEntity;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      productEntity = shared_ptr<Product>(static_cast< Product* >( *it ) );
      it = values->take( it );
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate product with identifier '%s'.",  productName.c_str());
    
    return productEntity;
  }
  
  list< shared_ptr<Product> > ProductProfile::findAllProducts() const throw(data_access_error)
  {
    // call entity finder
    Value updatedVal((integer)kSyncIndUpdate);
    unique_ptr<ResultSet> values( this->db_.find(Product::TABLE_NAME, "findProductByAll", NULL) );
    list< shared_ptr<Product> > result;
    shared_ptr<Product> productEntity;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
      productEntity = shared_ptr<Product>(static_cast< Product* >( *it ) );
      it = values->take( it );
      result.push_back( productEntity );
    }
    return result;
  }

  // ==========
  // Protected Interface
  // ==========
  
  shared_ptr<Product> ProductProfile::loadProductById( const integer id ) const throw(data_access_error)
  {
    // call entity finder
    
    Value identifier(id);
    unique_ptr<ResultSet> values( db_.find(Product::TABLE_NAME, "findProductById", &identifier, NULL) );
    shared_ptr<Product> productEntity;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      productEntity = shared_ptr<Product>(static_cast< Product* >( *it ) );
      it = values->take( it );
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate member with id '%d'.",  id);
    
    return productEntity;
  }

}
