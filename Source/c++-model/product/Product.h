/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PRODUCT_ENTITY_H__
#define __EJIN_PRODUCT_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class ProductProfile;
  class ApprovedProduct;
  
  /**
   * Entity Class for PRODUCT_TBL
   */
  class Product: public BaseEntity, public IBusinessData, public DataSchema<Product> {
    friend class Database;
    friend class ProductProfile;
    friend class ProductRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "PRODUCT_TBL";
    
    // ctors
    Product( void );
    Product( const integer id );
    Product( const string& identifier );
    Product( const Product& record );
    // dtor
    ~Product( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Product& operator=( const Product& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Product(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool hasContent() const { return true; }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool unmarshalJson( const nlohmann::json& node );
    //bool unmarshalJson( json_t* node );
    
    // SQL select statements
    const map<string,string> finderTemplates( const char* schema ) const;

    // ==========
    // Crypto Interface
    // ==========
  public:
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( No, 1 )
    ACCESS_STRING_PROPERTY_INTF( Identifier, 2 )
    ACCESS_STRING_PROPERTY_INTF( Name, 3 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Enabled, 4 )
    ACCESS_STRING_PROPERTY_INTF( Title, 5 )
    ACCESS_STRING_PROPERTY_INTF( Information, 6 )
    ACCESS_DOUBLE_PROPERTY_INTF( Price, 7 )
    ACCESS_STRING_PROPERTY_INTF( PriceLocale, 8 )
    ACCESS_INTEGER_PROPERTY_INTF( SubscriptionPeriod, 9 )
    ACCESS_INTEGER_PROPERTY_INTF( SubscriptionPeriodUnit, 10 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxChannelCount, 11 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxAttachmentCount, 12 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxAttachmentSize, 13 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxTotalAttachmentSize, 14 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxTextContentLength, 15 )
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Product Variables
    
    bool isEmpty_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
}

#endif //__EJIN_PRODUCT_ENTITY_H__
