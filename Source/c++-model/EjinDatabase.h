/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_DATABASE_H__
#define __EJIN_DATABASE_H__

#include "Declarations.h"

#include "SqliteDatabase.h"
#include "SqliteUtil.h"

using sqlite::jtime;
using sqlite::ResultSet;

namespace ejin
{
  class ChannelHeader;
  
  /**
   * meta data modification class.
   */
  struct EntityModification
  {
  public:
    const integer mask;
    const jtime   time;
    const string  by;
    const bool    isOutdated;

    EntityModification(integer _mask, jtime _modifyTime, string _modifiedBy, bool _isOutdated):
    mask(_mask), time(_modifyTime), by(_modifiedBy), isOutdated(_isOutdated) {}
  };
  
  /**
   * Concreate database sub class for ejin
   */
  class Database: public sqlite::Database {
    friend class DatabaseLock;
    
  public:

    // ctor
    Database( const char* username );

    // ==========
    // Public Interface
    // ==========
    
    /**
     * Returns the database user name
     */
    const string username( void ) const;
    
    /**
     * Upgrade user database if necessary
     */
    void upgradeDatabase( const char* database, int version );
    
    /**
     * Runs some internal database consistency checks,
     * Return true if no problem has been detected.
     */
    bool checkConstency( void );
    
    /**
     * Clear local ejin channel data and reset to defaults
     */
    bool clearChannel( const string& channelGid );

    /**
     * Backup local channel data
     */
    bool backupChannel( const string& channelGid );

    /**
     * Restore local channel data from last backup
     */
    bool restoreChannel( const string& channelGid );
    
    /**
     * Backup local member data
     */
    bool backupMember( void );

    /**
     * Restore local member data from last backup
     */
    bool restoreMember( void );
    
    /**
     * Archive specified channel instance
     */
    bool archiveChannel( const integer channelId, const char* password )
    throw(data_access_error);
    
    /**
     * Unarchive specified channel instance
     */
    bool unarchiveChannel( const integer channelId, const char* password )
    throw(data_access_error);
    
    /**
     * Export all channel media to the file system.
     * The filename are resolved in callback functions.
     * Returns the number of exported media files.
     */
    int exportAllChannelAttachments(integer channelId, string targetFolder);
    int getAttachmentCount(integer channelId);
    string getChannelName(integer channelId);
    string getChannelMembers(integer channelId, bool excludeMe);

    /**
     * Delete channel with all attachments
     */
    bool deleteChannel( integer channelId );
    bool deleteChannel( const string& channelGid );

    /*+
     * Delete all messages in the target chat
     */
    bool deleteChatMessages( integer chatId );

    /**
     * Clear local ejin database and reset to defaults
     */
    bool clearAll( void );
    
    /**
     * Clear local ejin backup database and reset to defaults
     */
    bool clearAllBak( void );
    
    // ==========
    // Abstrace Interface
    // ==========
  protected:
    
    void initEntityManager( void );
    static bool exec( sqlite3* db, const char *statement, ... );

    // ==========
    // Private Interface
    // ==========
  private:
    
    // Unique global identifier of the channel.
    std::recursive_mutex mutex_;
    string username_;

    void registerPrototype( sqlite::BaseEntity* proto );
    void clearChannelInternal( ChannelHeader& header );
    
    bool _deleteChannel( shared_ptr<ChannelHeader> channelHeader )
    throw(data_access_error);

    /**
     * archive
     */
    bool archiveChannelHeader( integer channelId )
    throw(data_access_error);
    void archiveChannelMembers( unique_ptr<ResultSet>& channelMembers, integer channelId )
    throw(data_access_error);
    void archiveChannelPosts( unique_ptr<ResultSet>& posts, integer channelId )
    throw(data_access_error);
    void archiveChannelComments( unique_ptr<ResultSet>& comments, integer channelId, integer postId )
    throw(data_access_error);
    void archiveResources( unique_ptr<ResultSet>& resource, integer channelId, integer postId, integer commentId )
    throw(data_access_error);
    
    /**
     * unarchive entities
     */
    bool unarchiveChannelHeader( integer channelId )
    throw(data_access_error);
    void unarchiveChannelMembers( unique_ptr<ResultSet>& channelMembers, integer channelId )
    throw(data_access_error);
    void unarchiveChannelPosts( unique_ptr<ResultSet>& posts, integer channelId )
    throw(data_access_error);
    void unarchiveChannelComments( unique_ptr<ResultSet>& comments, integer channelId, integer postId )
    throw(data_access_error);
    void unarchiveResources( unique_ptr<ResultSet>& resource, integer channelId, integer postId, integer commentId )
    throw(data_access_error);
    
  };
  
  // A class with implements RAII on the database
  class DatabaseLock {
  private:
    Database* database_;
  public:
    DatabaseLock(Database* database) : database_(database) {
      assert ( database );
      if ( !database_->isClean() ) {
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "database is dirty" );
      }
      if ( Database::isExclusive( database_->handle() ) ) {
        _throw_exception(DATA_ACCESS_EXCEPTION, "database is already locked");
      }
      database_->mutex_.lock();
      Database::setExclusive( database_->handle(), true );
    }
    ~DatabaseLock() {
      Database::setExclusive( database_->handle(), false );
      database_->mutex_.unlock();
    }
  };
  
}

#endif // __EJIN_DATABASE_H__
