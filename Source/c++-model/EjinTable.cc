/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "EjinTable.h"

#include "SqliteResultSet.h"
#include "SqliteAttributeSet.h"
#include "SqliteUtil.h"
#include "DebugLog.h"
#include "IBusinessData.h"

using sqlite::jtime;
using sqlite::ResultSet;
using sqlite::Table;
using sqlite::BaseEntity;
using sqlite::Database;
using sqlite::AttributeSet;
using sqlite::Attribute;
using sqlite::Value;
using sqlite::field_type;

namespace ejin
{

  bool EjinTable::backup( va_list conditions ) {
    DEBUG_METHOD();

    bool success = true;
    list<shared_ptr<BaseEntity>> entitiesToUpdate;
    list<shared_ptr<BaseEntity>> entitiesToDelete;
    
    // entities already in the backup
    {
      va_list cond;
      va_copy(cond,conditions);
      unique_ptr<ResultSet> values(this->find( BACKUP_DATABASE_NAME, "BackupSetBak", cond ));
      for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
        entitiesToDelete.push_back(shared_ptr<BaseEntity>( *it ));
        it = values->take( it );
      }
    }

    // backup unchanged entites
    {
      va_list cond;
      va_copy(cond,conditions);
      unique_ptr<ResultSet> values(this->find( MAIN_DATABASE_NAME, "BackupSet", cond ));
      for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
        shared_ptr<BaseEntity> entity((shared_ptr<BaseEntity>( *it )));
        it = values->take( it );
        auto foundInBackup = std::find_if(entitiesToDelete.begin(), entitiesToDelete.end(), [entity](shared_ptr<BaseEntity> e) {
          return *entity == *e;
        });
        if ( foundInBackup != entitiesToDelete.end() ) {
          // to not delete this one
          entitiesToDelete.erase(foundInBackup);
        }
        
        switch (entity->getSyncInd()) {
          case ejin::kSyncIndUpdate:
          case ejin::kSyncIndInsert:
          case ejin::kSyncIndDelete:
            continue;
          case ejin::kSyncIndSynchronous:
          case ejin::kSyncIndConflict:
            break;
        }
        
        const AttributeSet fs(fields());
        for (int index = 0; index < fs.count(); index++)
        {
          if (const Attribute* field = fs.byIndex(index))
          {
            if (field->isTransient()) {
              switch (field->type()) {
                case sqlite::type_int:
                  entity->asInteger(index, 0);
                  break;
                case sqlite::type_bool:
                  entity->asBool(index, false);
                  break;
                default:
                  entity->setNull(index);
                  break;
              }
            }
          }
        }
        
        success = true;
        // remember to persist later
        entitiesToUpdate.push_back(entity);
      }
    }
    
    // remove entities
    for (auto it=entitiesToDelete.begin(); it!=entitiesToDelete.end(); ++it) {
      remove(**it, BACKUP_DATABASE_NAME);
    }
    // persist entities
    for (auto it=entitiesToUpdate.begin(); it!=entitiesToUpdate.end(); ++it) {
      persist(**it, BACKUP_DATABASE_NAME);
      success = true;
    }

    return success;
  }

  bool EjinTable::restore( va_list conditions ) {
    DEBUG_METHOD();
    
    // convert variable arguments into a list
    vector< pair<field_type,const Value*> > values( this->toBindVariables( conditions ) );
    {
      sqlite3_stmt* stmt = this->preparedRestoreStatement1_;
      assert( stmt );
      
      // clear statement first
      EXEC_SQLITE_STATEMENT (sqlite3_reset (stmt), db_.handle());
      EXEC_SQLITE_STATEMENT (sqlite3_clear_bindings (stmt), db_.handle());
      
      // bind entities values
      bindValuesToStmt(stmt, 1, values);
      
      DEBUG_PRINT("%s: bind prepared restore statement %s", this->name(), sqlite3_sql(this->preparedRestoreStatement1_));
      switch (sqlite3_step (stmt)) {
        case SQLITE_DONE:
          break;
        case SQLITE_BUSY:
        case SQLITE_LOCKED:
          return false;
        default:
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"restore entity failed: %s", sqlite3_errmsg(handle()));
      }
    }
    {
      sqlite3_stmt* stmt = this->preparedRestoreStatement2_;
      assert( stmt );
      
      // clear statement first
      EXEC_SQLITE_STATEMENT (sqlite3_reset (stmt), db_.handle());
      EXEC_SQLITE_STATEMENT (sqlite3_clear_bindings (stmt), db_.handle());
      
      // bind entities values
      bindValuesToStmt(stmt, 1, values);
      
      DEBUG_PRINT("%s: bind prepared restore statement %s", this->name(), sqlite3_sql(this->preparedRestoreStatement2_));
      switch (sqlite3_step (stmt)) {
        case SQLITE_DONE:
          return true;
        case SQLITE_BUSY:
        case SQLITE_LOCKED:
          return false;
        default:
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"restore entity failed: %s", sqlite3_errmsg(handle()));
      }
    }
    return false;
  }
  
}
