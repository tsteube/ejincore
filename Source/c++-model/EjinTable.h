/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_TABLE_H__
#define __EJIN_TABLE_H__

#include "Declarations.h"

#include "SqliteTable.h"
#include "SqliteUtil.h"

namespace ejin
{
  /**
   * Concreate database sub class for ejin
   */
  class EjinTable: public sqlite::Table {
    
  public:
    
    // ctor
    EjinTable( const sqlite::Database& db, sqlite::BaseEntity* proto ): sqlite::Table(db, proto) { };
    // copy ctor
    EjinTable( const EjinTable& table ): sqlite::Table(table) { };
    // dtor
    ~EjinTable() { };
    
    // ==========
    // Public Interface
    // ==========
    
    bool backup( va_list conditions );
    bool restore( va_list conditions );
    
  };
}

#endif // __EJIN_TABLE_H__

