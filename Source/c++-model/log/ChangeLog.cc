/* Copyright (C) 2012 - 2017 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChangeLog.h"

#include "UsingEjinTypes.h"
#include "Utilities.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "Media.h"
#include "Member.h"
#include "ApprovedMember.h"
#include "ChannelMember.h"
#include "Picture.h"
#include "Chat.h"
#include "Message.h"

#define PREFIX_64( string ) string.substr(0, 64)

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========  
  
  template <> const sqlite::Attribute DataSchema<ChangeLog>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("SYNC_SOURCE", type_text, flag_not_null),
    Attribute("SYNC_OPERATION", type_text, flag_not_null),
    Attribute("MODIFIED_BY", type_text, flag_not_null),
    Attribute("RESOURCE_ID_1", type_int, flag_not_null),
    Attribute("RESOURCE_ID_2", type_int, flag_none),
    Attribute("RESOURCE_ID_3", type_int, flag_none),
    Attribute("MESSAGE", type_text, flag_none),
    Attribute("SYNC_ANCHOR", type_text, flag_not_null),
    Attribute("SYNC_TIME", type_time, flag_not_null),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<ChangeLog>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ChangeLog::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) ) {
      tMap["findBySyncAnchor"] = "SYNC_ANCHOR = ? ORDER BY RESOURCE_ID_1,RESOURCE_ID_2,RESOURCE_ID_3,SYNC_SOURCE,SYNC_OPERATION";
      tMap["findBySyncTime"] = "SYNC_TIME = ? ORDER BY RESOURCE_ID_1,RESOURCE_ID_2,RESOURCE_ID_3,SYNC_SOURCE,SYNC_OPERATION";
    }
    return tMap;
  }

  const map<string,string> ChangeLog::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    return tMap;
  }
  
  ChangeLog* ChangeLog::createChangeLog( const BaseEntity* entity, Database& db ) {
    {
      const ChannelHeader* entity2 = dynamic_cast<const ChannelHeader*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const ChannelPost* entity2 = dynamic_cast<const ChannelPost*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const ChannelComment* entity2 = dynamic_cast<const ChannelComment*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const Media* entity2 = dynamic_cast<const Media*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const Member* entity2 = dynamic_cast<const Member*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const ChannelMember* entity2 = dynamic_cast<const ChannelMember*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const ApprovedMember* entity2 = dynamic_cast<const ApprovedMember*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const Picture* entity2 = dynamic_cast<const Picture*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const Chat* entity2 = dynamic_cast<const Chat*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    {
      const Message* entity2 = dynamic_cast<const Message*> (entity);
      if (entity2) return new ChangeLog( *entity2, db );
    }
    return NULL;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  ChangeLog::ChangeLog( void ): BaseEntity(TABLE_DDL, false)
  {
  }
  ChangeLog::ChangeLog( const ChannelHeader& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceChannel );
    COPY_PROPERTY2((*this), entity, ResourceId1, Id);
    clearResourceId2( );
    clearResourceId3( );
    COPY_PROPERTY((*this), entity, ModifiedBy);
    setMessage( PREFIX_64( entity.getContent() ) );
  }
  ChangeLog::ChangeLog( const ChannelPost& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceChannelPost );
    COPY_PROPERTY2((*this), entity, ResourceId1, ChannelId);
    COPY_PROPERTY2((*this), entity, ResourceId2, Id);
    clearResourceId3( );
    COPY_PROPERTY((*this), entity, ModifiedBy);
    setMessage( PREFIX_64( entity.getContent() ) );
  }
  ChangeLog::ChangeLog( const ChannelComment& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceChannelComment );
    COPY_PROPERTY2((*this), entity, ResourceId1, ChannelId);
    COPY_PROPERTY2((*this), entity, ResourceId2, PostId);
    COPY_PROPERTY2((*this), entity, ResourceId3, Id);
    COPY_PROPERTY((*this), entity, ModifiedBy);
    setMessage( PREFIX_64( entity.getContent() ) );
  }
  ChangeLog::ChangeLog( const Media& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceMedia );
    COPY_PROPERTY2((*this), entity, ResourceId1, ChannelId);
    COPY_PROPERTY2((*this), entity, ResourceId2, PostId);
    COPY_PROPERTY2((*this), entity, ResourceId3, Id);
    COPY_PROPERTY((*this), entity, ModifiedBy);
    setMessage( PREFIX_64( entity.getName() ) );
  }
  ChangeLog::ChangeLog( const Member& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceMember );
    COPY_PROPERTY2((*this), entity, ResourceId1, Id);
    clearResourceId2( );
    clearResourceId3( );
    COPY_PROPERTY2((*this), entity, ModifiedBy, Username);
    clearMessage( );
  }
  ChangeLog::ChangeLog( const ChannelMember& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceChannelMember );
    COPY_PROPERTY2((*this), entity, ResourceId1, ChannelId);
    COPY_PROPERTY2((*this), entity, ResourceId2, Id);
    setResourceId2( entity.getId() );
    clearResourceId3( );
    COPY_PROPERTY((*this), entity, ModifiedBy);
    setMessage( PREFIX_64( entity.getTags() ) );
  }
  ChangeLog::ChangeLog( const ApprovedMember& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceMemberApproval );
    Value username( entity.getUsername() );
    unique_ptr<ResultSet> values( db.find(Member::TABLE_NAME, "findMemberByUsername", &username, NULL) );
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      this->setResourceId1( static_cast< Member* >(*it)->getId() );
    } else {
      this->setResourceId1( -1 );
    }
    COPY_PROPERTY2((*this), entity, ResourceId2, MemberId);
    this->clearResourceId3();
    COPY_PROPERTY2((*this), entity, ModifiedBy, Username);
    clearMessage( );
  }
  ChangeLog::ChangeLog( const Picture& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceMemberPicture );
    COPY_PROPERTY2((*this), entity, ResourceId1, MemberId);
    COPY_PROPERTY2((*this), entity, ResourceId2, Id);
    clearResourceId3( );
    COPY_PROPERTY((*this), entity, ModifiedBy);
    clearMessage( );
  }
  ChangeLog::ChangeLog( const Chat& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceChat );
    COPY_PROPERTY2((*this), entity, ResourceId1, Id);
    clearResourceId2( );
    clearResourceId3( );
    COPY_PROPERTY2((*this), entity, ModifiedBy, Partner);
    clearMessage( );
  }
  ChangeLog::ChangeLog( const Message& entity, Database& db ): ChangeLog( )
  {
    setSource( kChangeLogSourceChatMessage );
    if ( entity.hasChatId() )
      setResourceId1( entity.getChatId() );
    setResourceId2( entity.getId() );
    clearResourceId3( );
    COPY_PROPERTY2((*this), entity, ModifiedBy, Sender);
    setMessage( PREFIX_64( entity.getContent() ) );
  }
  ChangeLog::ChangeLog( const ChangeLog& record ): ChangeLog()
  {
    this->operator=( record );
  }
  ChangeLog::~ChangeLog( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  ChangeLog& ChangeLog::operator=( const ChangeLog& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Source);
      COPY_PROPERTY((*this), rhs, Operation);
      COPY_PROPERTY((*this), rhs, ModifiedBy);
      COPY_PROPERTY((*this), rhs, ResourceId1);
      COPY_PROPERTY((*this), rhs, ResourceId2);
      COPY_PROPERTY((*this), rhs, ResourceId3);
      COPY_PROPERTY((*this), rhs, Message);
      COPY_PROPERTY((*this), rhs, SyncAnchor);
      COPY_PROPERTY((*this), rhs, SyncTime);
    }
    return *this;
  }
  
  // comparison
  bool ChangeLog::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const ChangeLog& mrhs = dynamic_cast<const ChangeLog&> (rhs);
    if (this->hasId() && mrhs.hasId())
      return (this->getId() == mrhs.getId());
    if (this->hasSource() && mrhs.hasSource() && this->getSource() != mrhs.getSource())
      return false;
    if (this->hasOperation() && mrhs.hasOperation() && this->getOperation() != mrhs.getOperation())
      return false;
    if (this->hasResourceId1() && mrhs.hasResourceId1() && this->getResourceId1() != mrhs.getResourceId1())
      return false;
    if (this->hasResourceId2() && mrhs.hasResourceId2() && this->getResourceId2() != mrhs.getResourceId2())
      return false;
    if (this->hasResourceId3() && mrhs.hasResourceId3() && this->getResourceId3() != mrhs.getResourceId3())
      return false;
    return false;
  }
  bool ChangeLog::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ChangeLog& mrhs = dynamic_cast<const ChangeLog&> (rhs); // throws std::bad_cast
    if (this->hasSource() && mrhs.hasSource())
      return (this->getSource() < mrhs.getSource());
    if (this->hasOperation() && mrhs.hasOperation())
      return (this->getOperation() < mrhs.getOperation());
    if (this->hasResourceId1() && mrhs.hasResourceId1())
      return (this->getResourceId1() < mrhs.getResourceId1());
    if (this->hasResourceId2() && mrhs.hasResourceId2())
      return (this->getResourceId2() < mrhs.getResourceId2());
    if (this->hasResourceId3() && mrhs.hasResourceId3())
      return (this->getResourceId3() < mrhs.getResourceId3());
    return false;
  }
  
  // Database schema
  const char* ChangeLog::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& ChangeLog::tableFields( void ) const { return TABLE_DDL; }

  bool ChangeLog::isCoveredBy( const ChangeLog& rhs ) const
  {
    if ( rhs.getOperation() == kChangeLogOperationAdd || rhs.getOperation() == kChangeLogOperationRemove ) {
      switch ( getSource() ) {
        case kChangeLogSourceChannel:
        case kChangeLogSourceMedia:
        case kChangeLogSourceMember:
        case kChangeLogSourceChat:
          return false;
        case kChangeLogSourceChannelMember:
        case kChangeLogSourceChannelPost:
          switch ( rhs.getSource() ) {
            case kChangeLogSourceChannel:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              return false;
          }
        case kChangeLogSourceChannelComment:
          switch ( rhs.getSource() ) {
            case kChangeLogSourceChannel:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            case kChangeLogSourceChannelPost:
              return PROPERTY_EQUALS( ResourceId1, rhs ) && PROPERTY_EQUALS( ResourceId2, rhs );
            default:
              return false;
          }
        case kChangeLogSourceMemberPicture:
          switch ( rhs.getSource() ) {
            case kChangeLogSourceMember:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              return false;
          }
        case kChangeLogSourceChatMessage:
          switch ( rhs.getSource() ) {
            case kChangeLogSourceChat:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              return false;
          }
        case kChangeLogSourceMemberApproval:
          break;
      }
    }
    return false;
  }
  
  bool ChangeLog::replaceLog( const ChangeLog& rhs ) const
  {
    if ( rhs.getOperation() == kChangeLogOperationOutdated ) {
      switch ( rhs.getSource() ) {
        case kChangeLogSourceChannel:
          switch ( getSource() ) {
            case kChangeLogSourceChannel:
            case kChangeLogSourceChannelComment:
            case kChangeLogSourceChannelPost:
            case kChangeLogSourceChannelMember:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              break;
          }
          break;
        case kChangeLogSourceMedia:
          switch ( getSource() ) {
            case kChangeLogSourceMedia:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              break;
          }
          break;
        case kChangeLogSourceMember:
          switch ( getSource() ) {
            case kChangeLogSourceMember:
            case kChangeLogSourceMemberPicture:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              break;
          }
          break;
        case kChangeLogSourceChat:
          switch ( getSource() ) {
            case kChangeLogSourceChat:
            case kChangeLogSourceChatMessage:
              return PROPERTY_EQUALS( ResourceId1, rhs );
            default:
              break;
          }
          break;
      }
    }
    return rhs.isCoveredBy( *this );
  }
}
