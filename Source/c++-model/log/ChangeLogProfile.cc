/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChangeLogProfile.h"

#include "UsingEjinTypes.h"

#include "ChangeLog.h"
#include "Utilities.h"

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
 
  list< shared_ptr<ChangeLog> > ChangeLogProfile::findBySyncAnchor( const string& syncAnchor ) const throw(data_access_error)
  {
    // call entity finder
    Value anchor( syncAnchor );
    unique_ptr<ResultSet> values( db_.find(ChangeLog::TABLE_NAME, "findBySyncAnchor", &anchor, NULL) );
    list< shared_ptr<ChangeLog> > result;
    auto it = values->begin();
    while (it != values->end()) {
      result.push_back(shared_ptr<ChangeLog>(static_cast< ChangeLog* >(*it)));
      it = values->take( it );
    }
    return result;
  }
  
  list< shared_ptr<ChangeLog> > ChangeLogProfile::findBySyncTime( const sqlite::jtime& syncTime ) const throw(data_access_error)
  {
    // call entity finder
    Value time( syncTime );
    unique_ptr<ResultSet> values( db_.find(ChangeLog::TABLE_NAME, "findBySyncTime", &time, NULL) );
    list< shared_ptr<ChangeLog> > result;
    auto it = values->begin();
    while (it != values->end()) {
      result.push_back(shared_ptr<ChangeLog>(static_cast< ChangeLog* >(*it)));
      it = values->take( it );
    }
    return result;
  }

}
