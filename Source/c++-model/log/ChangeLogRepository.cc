/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChangeLogRepository.h"
#include "UsingEjinTypes.h"

#include "ChangeLog.h"
#include "Member.h"
#include "Utilities.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  void ChangeLogRepository::begin( ) throw(data_access_error)
  {
    this->syncChangeLog_.clear();
  }
  bool ChangeLogRepository::outdated( const BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime )
  {
    shared_ptr<ChangeLog> changeLog( this->createLog( entity, syncAnchor, syncTime ) );
    assert( changeLog );
    changeLog->setOperation( kChangeLogOperationOutdated );
    return this->registerLog( changeLog, syncAnchor );
  }
  bool ChangeLogRepository::add( const BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime )
  {
    shared_ptr<ChangeLog> changeLog( this->createLog( entity, syncAnchor, syncTime ) );
    assert( changeLog );
    changeLog->setOperation( kChangeLogOperationAdd );
    return this->registerLog( changeLog, syncAnchor );
  }
  bool ChangeLogRepository::update( const BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime )
  {
    shared_ptr<ChangeLog> changeLog( this->createLog( entity, syncAnchor, syncTime ) );
    assert( changeLog );
    changeLog->setOperation( kChangeLogOperationUpdate );
    return this->registerLog( changeLog, syncAnchor );
  }
  bool ChangeLogRepository::remove( const BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime )
  {
    shared_ptr<ChangeLog> changeLog( this->createLog( entity, syncAnchor, syncTime ) );
    assert( changeLog );
    changeLog->setOperation( kChangeLogOperationRemove );
    return this->registerLog( changeLog, syncAnchor );
  }
  bool ChangeLogRepository::end( ) throw(data_access_error)
  {
    if ( ! this->syncChangeLog_.empty() ) {
      for (const auto& logs : this->syncChangeLog_) {
        for (const auto& log : logs.second) {
          if (log->hasId())
            this->db_.update( *log );
          else
            this->db_.insert( *log );
        }
      }
      this->syncChangeLog_.clear();
      return true;
    }
    return false;
  }

  // ==========
  // Private Interface
  // ==========
  
  bool ChangeLogRepository::registerLog( shared_ptr<ChangeLog> changelog, const string& syncAnchor )
  {
    std::map<string,list< shared_ptr<ChangeLog> >>::iterator it = this->syncChangeLog_.find( syncAnchor );
    if ( it == this->syncChangeLog_.end() ) {
      this->syncChangeLog_[ syncAnchor ] = list< shared_ptr<ChangeLog> >( this->findBySyncAnchor( syncAnchor ) );
    }
    list< shared_ptr<ChangeLog> >& changeLogs = this->syncChangeLog_[ syncAnchor ];

    if ( std::find_if(changeLogs.begin(), changeLogs.end(),
                      [changelog](shared_ptr<ChangeLog> elem){ return changelog->isCoveredBy( *elem ); }) == changeLogs.end() )
    {
      changeLogs.remove_if([changelog](shared_ptr<ChangeLog> elem){ return changelog->replaceLog( *elem ); });
      changeLogs.push_back( changelog );
      return true;
    }
    return false;
  }
  
  shared_ptr<ChangeLog> ChangeLogRepository::createLog( const sqlite::BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime )
  {
    shared_ptr<ChangeLog> changeLog = shared_ptr<ChangeLog>( ChangeLog::createChangeLog( &entity, this->db_ ) );
    if (changeLog) {
      changeLog->setSyncAnchor( syncAnchor );
      changeLog->setSyncTime( syncTime );
    }
    return changeLog;
  }

}
