/* Copyright (C) 2012 - 2017 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANGE_LOG_ENTITY_H__
#define __EJIN_CHANGE_LOG_ENTITY_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  
  /**
   * Entity Class for CHANGE_LOG_TBL
   */
  class ChangeLog: public sqlite::BaseEntity, public DataSchema<ChangeLog> {
    friend class Database;
    friend class ChangeLogProfile;
    friend class ChangeLogRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "CHANGE_LOG_TBL";
    
    static ChangeLog* createChangeLog( const BaseEntity* entity, Database& db );
    
    // ctors
    ChangeLog( void );
    ChangeLog( const ChangeLog& record );
    // dtor
    ~ChangeLog( void );
    
  private:
    
    ChangeLog( const ChannelHeader& entity, Database& db );
    ChangeLog( const ChannelPost& entity, Database& db );
    ChangeLog( const ChannelComment& entity, Database& db );
    ChangeLog( const ChannelMember& entity, Database& db );
    ChangeLog( const Media& entity, Database& db );
    ChangeLog( const Member& entity, Database& db );
    ChangeLog( const ApprovedMember& entity, Database& db );
    ChangeLog( const Picture& entity, Database& db );
    ChangeLog( const Chat& entity, Database& db );
    ChangeLog( const Message& entity, Database& db );

    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ChangeLog& operator=( const ChangeLog& rhs );
    // clone
    ChangeLog* clone( void ) const { return new ChangeLog(*this); }
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( Source, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( Operation, 2 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedBy, 3 )
    ACCESS_INTEGER_PROPERTY_INTF( ResourceId1, 4 )
    ACCESS_INTEGER_PROPERTY_INTF( ResourceId2, 5 )
    ACCESS_INTEGER_PROPERTY_INTF( ResourceId3, 6 )
    ACCESS_STRING_PROPERTY_INTF( Message, 7 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 8 )
    ACCESS_TIME_PROPERTY_INTF( SyncTime, 9 )

    bool isCoveredBy( const ChangeLog& rhs ) const;
    bool replaceLog( const ChangeLog& rhs ) const;
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
  };
}

#endif //__EJIN_CHANGE_LOG_ENTITY_H__
