/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANGE_LOG_REPOSITORY_H__
#define __EJIN_CHANGE_LOG_REPOSITORY_H__

#include "ChangeLogProfile.h"
#include "SqliteBaseEntity.h"

namespace ejin
{
  class MemberRepository;
  
  /**
   * Update operations on the picture entity. There are a create, update and delete operation to manages pictures bound to a
   * channel instance.
   */
  class ChangeLogRepository: public ChangeLogProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    ChangeLogRepository( Database& db ): ChangeLogProfile( db ), syncChangeLog_() { }
    // dtor
    ~ChangeLogRepository( void ) {};

    /*
     * Start registration change events in the context of a synchronization operation.
     */
    void begin( ) throw(data_access_error);

    /*
     * Register outdated entity event.
     */
    bool outdated( const sqlite::BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime );

    /*
     * Register add entity event.
     */
    bool add( const sqlite::BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime );
    
    /*
     * Register update entity event.
     */
    bool update( const sqlite::BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime );

    /*
     * Register remove entity event.
     */
    bool remove( const sqlite::BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime );
    
    /*
     * Commit all registered changes into the database.
     */
    bool end( ) throw(data_access_error);
   
    // ==========
    // Private Interface
    // ==========
    
  private:
    
    bool registerLog( shared_ptr<ChangeLog> changelog, const string& syncAnchor );

    shared_ptr<ChangeLog> createLog( const sqlite::BaseEntity& entity, const string& syncAnchor, const sqlite::jtime& syncTime );

    // --------
    // Member Variables

    std::map< string, list< shared_ptr<ChangeLog> > > syncChangeLog_;
  };
  
}

#endif // __EJIN_CHANGE_LOG_REPOSITORY_H__
