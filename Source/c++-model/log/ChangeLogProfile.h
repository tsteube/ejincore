/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANGE_LOG_PROFILE_H__
#define __EJIN_CHANGE_LOG_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

namespace ejin
{
  class ChangeLog;
  
  /**
   * Methods to resolve member entities by state.
   */
  class ChangeLogProfile {

    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    ChangeLogProfile( Database& db ): db_( db ) { }
    // dtor
    virtual ~ChangeLogProfile( void ) {}
    
    /*
     * Load all changes registered on the specified synchronization anchor.
     */
    list< shared_ptr<ChangeLog> > findBySyncAnchor( const string& syncAnchor ) const
    throw(data_access_error);

    /*
     * Load all changes registered on the specified synchronization date.
     */
    list< shared_ptr<ChangeLog> > findBySyncTime( const sqlite::jtime& syncTime ) const
    throw(data_access_error);    
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    // --------
    // ChangeLog Variables 
    
    Database& db_;

  private:
  
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ChangeLogProfile );
    
  };
  
}

#endif // __EJIN_MEMBER_PROFILE_H__
