/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_USER_REPOSITORY_H__
#define __EJIN_USER_REPOSITORY_H__

#include "UserProfile.h"

namespace ejin
{
  /**
   * Update operations on the member entity. There are a create, update and delete operation to manages members bound to a
   * channel instance.
   */
  class UserRepository: public UserProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    UserRepository( Database& db, MemberRepository& memberRepository, PictureRepository& pictureRepository ):
      UserProfile( db, memberRepository, pictureRepository ) {}
    // dtor
    ~UserRepository( void ) {};

    /**
     * Update the member data.
     */
    User& update( User& member, bool force, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */
    void commit( User& member, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Revert local changes to synchronize the media entity with the server reference.
     */ 
    bool clearConflicts( const list<string>& members )
    throw(data_access_error);
    
    /**
     * Restore original member instance
     * and mark local changes as conflicted data set.
     */
    bool revert( const string& gid, bool force = false )
    throw(data_access_error);
    
    /**
     * Return true if the public key of the given user should be revoked.
     */
    bool isRSAKeyRevoked( const string& username ) const
    throw(security_integrity_error,data_access_error);

    /**
     * Return the public key of the given user from the database
     */
    shared_ptr<RSACrypto> getPublicRSAKey( const string& username ) const
    throw(security_integrity_error,data_access_error);

    /**
     * Generate RSA key pair for the given user.
     * The public key is saved in the database while the private key gets returned.
     */
    shared_ptr<RSACrypto> generateRSAKey( const string& username, const void* seed = NULL , int num = 0, bool update = true )
    throw(security_integrity_error,data_access_error);

  };
  
}

#endif // __EJIN_USER_REPOSITORY_H__
