/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_USER_H__
#define __EJIN_USER_H__

#include "Member.h"
#include "Picture.h"
#include "Declarations.h"
#include "SerializableEntity.h"
#include "SqliteValue.h"
#include "SqliteUtil.h"

namespace ejin
{  
  using std::list;
  using sqlite::Value;
  using ser::XmlStream;
  using ser::ISerializableEntity;
  
  // forward declaration
  class Database;  
  class UserHeader;
  class UserPost;
  class UserComment;
  class Resource;
  class RSACrypto;
  
  /**
   * This wrapper class contains the top level channel entity together with posts and comments on the channel. This class
   * is used to provide a channel profile with posts and comments.
   */
  class User: public ISerializableEntity, public ICryptoEntity {
    friend std::ostream& operator<<( std::ostream& ostr, const User& rhs );
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctors
    User( void );
    User( const string& username );
    User( const string& username, const string& syncAnchor );
    User( const string& username, const shared_ptr<Member> member, const shared_ptr<Picture> picture );
    User( const User& channel );
    // dtor
    ~User( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    User& operator=( const User& rhs );
    // comparision
    bool operator==( const User& rhs ) const;    
    bool operator<( const User& rhs ) const;
    // debug
    string toString( void ) const;
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    void clearBusinessData( void );
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }

    // ==========
    // Crypto Interface
    // ==========
  public:
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );

    ISerializableEntity* entityByXmlNode( const char* name, int position );
    Value* valueByXmlNode( const char* name );
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const; 
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    const string getUsername( void ) const { return this->username_.toString(); }
    void setUsername( const string& gid ) { this->username_.str(gid); }
    void clearUsername( void ) { this->username_.setNull(); }
    bool hasUsername( void ) const { return ! this->username_.isNull(); }
    
    const string getSyncAnchor( void ) const { return this->syncAnchor_.toString(); }
    void setSyncAnchor( const string& syncAnchor ) { this->syncAnchor_.str(syncAnchor); }
    void clearSyncAnchor( void ) { this->syncAnchor_.setNull(); }
    bool hasSyncAnchor( void ) const { return ! this->syncAnchor_.isNull(); }
    
    shared_ptr<Member> getMember( void ) const { return this->member_; }
    void setMember( shared_ptr<Member> member );
    void clearMember( void ) { this->member_.reset(); }
    bool hasMember( void ) const { return (bool)this->member_; }

    shared_ptr<Picture> getPicture( void ) const { return this->picture_; }
    void setPicture( shared_ptr<Picture> picture ) { this->picture_ = picture; }
    void clearPicture( void ) { this->picture_.reset(); }
    bool hasPicture( void ) const { return (bool)this->picture_; }
    
    bool hasConflict( void );

    // ==========
    // Private Interface
    // ==========
  private:
    
    // Unique global identifier of the channel.
    Value username_;

    // Last synchronization anchor
    Value syncAnchor_;
    
    // Top level core member object
    shared_ptr<Member> member_;
    
    // member picture
    shared_ptr<Picture> picture_;
    
    bool isEmpty_;

  };
  
  // debugging
  std::ostream& operator<<( std::ostream& ostr, const User& rhs );  
  
}

#endif // __EJIN_USER_H__
