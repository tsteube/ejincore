/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMemberRepository.h"
#include "UsingEjinTypes.h"

#include "Member.h"
#include "ChannelHeader.h"
#include "ChannelMember.h"
#include "Utilities.h"
#include "RSACrypto.h"
#include "ChangeLogRepository.h"
#include "ChannelFullText.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  ChannelMember& ChannelMemberRepository::add( ChannelMember& channelMember, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( channelMember.hasChannelId() );
    assert( channelMember.hasUsername() );
    
    // persist entity
    channelMember.clearId();
    channelMember.setSyncInd( kSyncIndSynchronous );
    channelMember.setModificationMask( kChannelMemberRecentRemoteModification );
    this->db_.insert( channelMember );
    this->changeLogRepository_.add( channelMember, syncAnchor, syncTime );
    
    clearDeleteOwnerInd( channelMember );
    
    return channelMember;
  }
  
  bool ChannelMemberRepository::update( ChannelMember& channelMemberEntity, ChannelMember& channelMember, const string& syncAnchor, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelMemberEntity.hasId() );
    assert( channelMemberEntity.isHidden() == false );
    assert( channelMember.hasChannelId() );
    assert( channelMember.hasUsername() );

    bool updated = false;
    SyncInd syncInd = (SyncInd) channelMemberEntity.getSyncInd();
    switch ( syncInd )
    {
      case kSyncIndSynchronous:
        if ( channelMember.getModifyTime() > channelMemberEntity.getModifyTime() ) {
          // entity was updated on the server
          if ( ! channelMemberEntity.hasEqualContent( channelMember ) ) {
            channelMemberEntity.setModificationMask(kChannelMemberRecentRemoteModification);
          }
          channelMemberEntity.copyDataFrom( channelMember );
          this->db_.update( channelMemberEntity );
          this->changeLogRepository_.update( channelMember, syncAnchor, syncTime );
          updated = true;
        }
        break;
      case kSyncIndInsert:
      case kSyncIndUpdate:
        if ( channelMember.getModifyTime() > channelMemberEntity.getModifyTime() ) {
          if ( channelMemberEntity.hasEqualContent( channelMember ) )
          {
            // update only some dates and state data
            channelMemberEntity.copyDataFrom( channelMember );
            channelMemberEntity.setSyncInd( kSyncIndSynchronous );
            channelMemberEntity.setModificationMask( kNoneModification );
            this->db_.update( channelMemberEntity );
            updated = true;
          }
          else
          {
            // conflict detected; save local entity for reference
            updated = cloneChannelMember( channelMemberEntity, &channelMember );
          }
        }
        if ( syncInd == kSyncIndInsert ) {
          this->changeLogRepository_.add( channelMemberEntity, syncAnchor, syncTime );
        } else {
          this->changeLogRepository_.update( channelMemberEntity, syncAnchor, syncTime );
        }
        break;
      case kSyncIndDelete:
        if ( channelMemberEntity.hasEqualContent( channelMember ) )
        {
          // no change on server side but removed on client
        }
        else
        {
          // conflict detected; save local entity for reference
          updated = cloneChannelMember( channelMemberEntity, &channelMember );
        }
        this->changeLogRepository_.remove( channelMemberEntity, syncAnchor, syncTime );
        break;
      case kSyncIndConflict:
        if ( ! channelMemberEntity.hasModifyTime() ||
            channelMember.getModifyTime() > channelMemberEntity.getModifyTime() ) {
          shared_ptr<ChannelMember> conflict( this->loadChannelMemberInternal( Value(channelMemberEntity.getChannelId()),
                                                                              Value(channelMemberEntity.getUsername().c_str()),
                                                                              Value(true) ) );
          if ( ! conflict )
            ejin::_throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d,username=%s]",
                                   channelMemberEntity.getChannelId(), channelMemberEntity.getUsername().c_str());
          if ( conflict->hasEqualContent( channelMember ) )
          {
            // resolve conflict
            switch ( conflict->getSyncInd() ) {
              case kSyncIndSynchronous:
              case kSyncIndConflict:
                ejin::_throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid local hidden data set [channelId=%d,username=%s]",
                                       channelMemberEntity.getChannelId(), channelMemberEntity.getUsername().c_str());
              case kSyncIndInsert:
              case kSyncIndUpdate:
                // clear conflict data set because the server has the same data content
                this->db_.remove( *conflict );
                // client and server are in sync now again
                // update only some dates and state data
                channelMemberEntity.copyDataFrom( channelMember );
                channelMemberEntity.setSyncInd( kSyncIndSynchronous );
                channelMemberEntity.setModificationMask( kNoneModification );
                this->db_.update( channelMemberEntity );
                this->changeLogRepository_.update( channelMemberEntity, syncAnchor, syncTime );
                updated = true;
                break;
              case kSyncIndDelete:
                // clear conflict data set because the server has the same data content
                this->db_.remove( *conflict );
                // remember that the member was already deleted on the client
                channelMemberEntity.setSyncInd( kSyncIndDelete );
                this->changeLogRepository_.remove( channelMemberEntity, syncAnchor, syncTime );
                this->db_.update( channelMemberEntity );
                updated = true;
                break;
            }
          }
          else
          {
            // keep original local conflict data
            // and update master with data from the server
            channelMemberEntity.copyDataFrom( channelMember );
            channelMemberEntity.setSyncInd( kSyncIndConflict );
            this->db_.update( channelMemberEntity );
            updated = true;
          }
        }
        break;
    }
    // update unique id
    channelMember.setId( channelMemberEntity.getId() );
    clearDeleteOwnerInd( channelMemberEntity );
    return updated;
  }
  
  bool ChannelMemberRepository::remove( ChannelMember& channelMemberEntity, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert ( channelMemberEntity.hasId() );
    
    switch ( channelMemberEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // normal case: added locally so the server doesn't know about the membership yet
        break;
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        this->db_.remove( channelMemberEntity );
        this->changeLogRepository_.remove( channelMemberEntity, syncAnchor, syncTime );
        break;
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        cloneChannelMember( channelMemberEntity, NULL );
        this->changeLogRepository_.update( channelMemberEntity, syncAnchor, syncTime );
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelMember> conflict( this->loadChannelMemberInternal( Value(channelMemberEntity.getChannelId()),
                                                                            Value(channelMemberEntity.getUsername().c_str()),
                                                                            Value(true) ) );
        if ( ! conflict ) {
          ejin::_throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d,username=%s]",
                                 channelMemberEntity.getChannelId(), channelMemberEntity.getUsername().c_str());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndConflict:
            ejin::_throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid local hidden data set [channelId=%d,username=%s]",
                                   channelMemberEntity.getChannelId(), channelMemberEntity.getUsername().c_str());
          case kSyncIndDelete:
            // entity was removed on client and server
            this->db_.remove( *conflict );
            this->db_.remove( channelMemberEntity );
            break;
          case kSyncIndInsert:
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            // and mark entity as removed from server
            channelMemberEntity.clearData();
            channelMemberEntity.setSyncInd( kSyncIndConflict );
            this->db_.update( channelMemberEntity );
            break;
        }
        this->changeLogRepository_.update( channelMemberEntity, syncAnchor, syncTime );
        break;
    }
    
    clearDeleteOwnerInd( channelMemberEntity );
    return true;
  }
  
  void ChannelMemberRepository::commit( ChannelMember& channelMemberEntity, const ChannelMember* channelMember, const string& username, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert ( channelMemberEntity.getId());
    
    channelMemberEntity.clearSessionKey( );
    channelMemberEntity.setModifiedBy( username );
    SyncInd syncInd = (SyncInd) channelMemberEntity.getSyncInd();
    switch ( syncInd )
    {
      case kSyncIndConflict:
        channelMemberEntity.setModificationMask( kChannelMemberConflictedModification );
      case kSyncIndInsert:
        channelMemberEntity.setCreateTime( syncTime );
        channelMemberEntity.setSecure( channelMember && channelMember->isSecure() );
      case kSyncIndUpdate:
      {
        shared_ptr<RSACrypto> crypto( getPublicRSAKey( channelMemberEntity.getUsername() ));
        if (crypto.get() != NULL) {
          channelMemberEntity.setFingerPrint(crypto->getPublicKeyFingerPrint());
        }
        channelMemberEntity.setSyncInd( kSyncIndSynchronous );
        channelMemberEntity.setModifyTime( syncTime );
        channelMemberEntity.setModificationMask( kNoneModification );
        if (channelMember) {
          channelMemberEntity.setIV( channelMember->getIV() );
        }
      }
      case kSyncIndSynchronous:
        this->db_.update( channelMemberEntity );
        if ( syncInd == kSyncIndInsert ) {
          this->changeLogRepository_.add( channelMemberEntity, syncAnchor, syncTime );
        } else if ( syncInd == kSyncIndUpdate ) {
          this->changeLogRepository_.update( channelMemberEntity, syncAnchor, syncTime );
        }
        break;
      case kSyncIndDelete:
        this->db_.remove( channelMemberEntity );
        this->changeLogRepository_.remove( channelMemberEntity, syncAnchor, syncTime );
        break;
    }
  }
  
  bool ChannelMemberRepository::clearConflict( ChannelMember& channelMemberEntity ) throw(data_access_error)
  {
    assert ( channelMemberEntity.getId() );
    shared_ptr<ChannelMember> conflict( this->loadChannelMemberInternal( Value(channelMemberEntity.getChannelId()),
                                                                        Value(channelMemberEntity.getUsername().c_str()),
                                                                        Value(true) ) );
    if ( conflict ) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      shared_ptr<ChannelMember> channelMember( this->loadChannelMemberInternal( Value(channelMemberEntity.getChannelId()),
                                                                               Value(channelMemberEntity.getUsername().c_str()),
                                                                               Value(false) ) );
      if ( channelMember ) {
        if ( channelMember->isEmpty() ) {
          this->db_.remove( *channelMember );
        } else {
          channelMember->setModificationMask( kNoneModification );
          channelMember->setSyncInd( kSyncIndSynchronous );
          this->db_.update( *channelMember );
        }
      }
      
      return true;
    }
    return false;
  }
  
  bool ChannelMemberRepository::cloneChannelMember( ChannelMember& channelMemberEntity, const ChannelMember* channelMember ) throw(data_access_error)
  {
    if ( channelMember == NULL || ! channelMemberEntity.hasEqualContent( *channelMember ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<ChannelMember> clone = unique_ptr<ChannelMember>(new ChannelMember( channelMemberEntity ));
      clone->clearId( );
      clone->setMasterId( channelMemberEntity.getId() );
      clone->setModificationMask( clone->getModificationMask()|kChannelMemberConflictedModification );
      
      if (channelMember) {
        clone->setMembership( channelMember->getMembership() );
      }
      if ( channelMember ) {
        channelMemberEntity.copyDataFrom( *channelMember );
      } else {
        channelMemberEntity.clearData();
      }
      // mark entity for conflict
      channelMemberEntity.setSyncInd( kSyncIndConflict );
      channelMemberEntity.setModificationMask( kChannelMemberConflictedModification|kChannelMemberRecentRemoteModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( channelMemberEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      
      // ------------------
      // Special handling on synchronization if the member data that results in a conflict
      // was removed before in the same operation.
      Value gid(channelMemberEntity.getUsername().c_str());
      Value force(false);
      shared_ptr<Member> memberEntity( this->loadMemberInternal( gid, force ) );
      if (memberEntity == NULL) {
        // So revert the delete member operation!
        MemberRepository::revert( channelMemberEntity.getUsername(), true );
      }
      //--------------------
      
      return true;
    }
    return false;
  }
  bool ChannelMemberRepository::revert( integer channelId, const string& username, bool force )
  throw(data_access_error)
  {
    assert( ! username.empty() );
    
    bool reverted = false;
    Value gid(username.c_str());
    shared_ptr<ChannelMember> channelMemberEntity( this->loadChannelMemberInternal(Value(channelId), gid, Value(false) ) );
    if ( channelMemberEntity != NULL ) {
      switch ( channelMemberEntity->getSyncInd() ) {
        case kSyncIndInsert:
        {
          if (force) {
            db_.remove( *channelMemberEntity );
            reverted = true;
          } else {
            cloneChannelMember(*channelMemberEntity, NULL);
            reverted = true;
          }
          break;
        }
        case kSyncIndSynchronous:
        case kSyncIndUpdate:
        case kSyncIndDelete:
        {
          // load entity from backup
          shared_ptr<ChannelMember> channelMemberEntityBak( shared_ptr<ChannelMember>(new ChannelMember()) );
          channelMemberEntityBak->setId(channelMemberEntity->getId());
          db_.refresh(*channelMemberEntityBak, channelMemberEntityBak->backupSchemaName() );
          
          if ( force ) {
            // force update entity with data from backup
            channelMemberEntity->copyDataFrom( *channelMemberEntityBak );
            channelMemberEntity->setSyncInd( kSyncIndSynchronous );
            channelMemberEntity->setModificationMask( kNoneModification );
            this->db_.update( *channelMemberEntity );
            reverted = true;
          } else {
            if (this->cloneChannelMember( *channelMemberEntity, channelMemberEntityBak.get() )) {
              reverted = true;
            }
          }
          break;
        }
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "member[id=%d] in conflict state", channelMemberEntity->getId());
      }
      
    } else {
      // restore old member if found in backup
      unique_ptr<ResultSet> values( db_.find(Member::TABLE_NAME, "findMemberByUsernameBak", &gid, NULL) );
      vector< BaseEntity* >::iterator it = values->begin();
      if (it != values->end()) {
        this->db_.insert( **it );
        reverted = true;
      }
      
    }
    return reverted;
  }
  
  bool ChannelMemberRepository::resolveSessionKey( integer channelId, const string& username, RSACrypto* crypto ) const
  throw(security_integrity_error,data_access_error)
  {
    if ( channelId <= 0 || crypto == NULL || username.empty() ) {
      return false;
    }
    
    Value gid(username.c_str());
    Value hidden(false);
    shared_ptr<ChannelMember> channelMemberEntity( this->loadChannelMemberInternal(Value(channelId), gid, hidden ) );
    if ( ! channelMemberEntity || ! channelMemberEntity->hasSessionKey() ) {
      return false;
    }
    if ( channelMemberEntity->hasSessionKey() && crypto->hasPrivateKeyASN1() ) {
      try {
        crypto->setEncryptedSessionKey( channelMemberEntity->getSessionKey() );
        return true;
      } catch (runtime_error e) {
        return false;
      }
    }
    return false;
  }
  string ChannelMemberRepository::exportSessionKey( integer channelId, const string& username  ) const
  throw(data_access_error)
  {
    if ( channelId <= 0 || username.empty() ) {
      return "";
    }
    
    Value gid(username.c_str());
    Value hidden(false);
    shared_ptr<ChannelMember> channelMemberEntity( this->loadChannelMemberInternal(Value(channelId), gid, hidden ) );
    if ( channelMemberEntity && channelMemberEntity->hasSessionKey() ) {
      return channelMemberEntity->getSessionKey();
    }
    return "";
  }
  list<string> ChannelMemberRepository::detectLostKey( integer channelId, const string& username )
  throw(security_integrity_error,data_access_error)
  {
    list<string> result;
    if ( channelId <= 0 || username.empty() ) {
      return result;
    }
    
    Value gid(username.c_str());
    Value hidden(false);
    shared_ptr<ChannelMember> channelMemberEntity( this->loadChannelMemberInternal( Value(channelId), gid, hidden ) );
    if ( channelMemberEntity &&
        channelMemberEntity->getSyncInd() != kSyncRemove &&
        channelMemberEntity->getSyncInd() != kSyncInsert ) {
      shared_ptr<Member> memberEntity( loadMemberInternal( gid, hidden ) );
      if ( memberEntity && memberEntity->hasPubKey() ) {
        unique_ptr<RSACrypto> crypto( RSACrypto::fromPublicKeyASN1( memberEntity->getPubKey() ) );
        if ( crypto.get() == NULL ) {
          throw runtime_error("found invalid public key");
        }
        if ( crypto->getPublicKeyFingerPrint() != channelMemberEntity->getFingerPrint() ) {
          // different public key fingerprints of new Member and old ChannelMember
          result.push_back(memberEntity->getFullname());
        }
      }
    }
    return result;
  }

  bool ChannelMemberRepository::setAESKey( integer channelId, const string& username, const string& aesKey, bool force )
  throw(security_integrity_error,data_access_error)
  {
    if ( channelId <= 0 || username.empty() || aesKey.empty() ) {
      return false;
    }
    
    Value gid(username.c_str());
    Value hidden(false);
    shared_ptr<ChannelMember> channelMemberEntity( this->loadChannelMemberInternal(Value(channelId), gid, hidden ) );
    if ( channelMemberEntity && channelMemberEntity->getSyncInd() != kSyncRemove ) {
      
      shared_ptr<Member> memberEntity( loadMemberInternal( gid, hidden ) );
      if ( memberEntity && memberEntity->hasPubKey() ) {
        try {
          RSACrypto* rsaKey = RSACrypto::fromPublicKeyASN1( memberEntity->getPubKey() );
          if (! rsaKey) {
            throw runtime_error("found invalid public key");
          }
          shared_ptr<RSACrypto> crypto( rsaKey );
          crypto->setSessionKey( aesKey );
          switch (channelMemberEntity->getSyncInd()) {
            case kSyncIndInsert:
            case kSyncIndUpdate:
              // always refresh session key
              channelMemberEntity->setSessionKey( crypto->getEncryptedSessionKey( ) );
              channelMemberEntity->setFingerPrint( crypto->getPublicKeyFingerPrint( ) );
              this->db_.update( *channelMemberEntity );
              return true;
              break;
            case kSyncIndSynchronous:
              if ( force || crypto->getPublicKeyFingerPrint() != channelMemberEntity->getFingerPrint() ) {
                channelMemberEntity->setSessionKey( crypto->getEncryptedSessionKey( ) );
                channelMemberEntity->setFingerPrint( crypto->getPublicKeyFingerPrint( ) );
                // mark for update
                channelMemberEntity->setSyncInd( kSyncIndUpdate );
                this->db_.update( *channelMemberEntity );
                return true;
              }
            case kSyncIndDelete:
            case kSyncIndConflict:
              return false;
          }
        } catch (runtime_error e) {
          _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "Encrypt session key for member[name=%s]",
                           username.c_str());
        }
      }
    }
    return false;
  }
  
  void ChannelMemberRepository::clearDeleteOwnerInd( ChannelMember& channelMemberEntity )
  throw(data_access_error)
  {
    // reset delete marker of owner entites
    const Value channelId(channelMemberEntity.getChannelId());
    this->db_.valueOf(ChannelHeader::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
  }
  
  list<integer> ChannelMemberRepository::searchFullText( const string& pattern )
  throw(data_access_error)
  {
    // call entity finder
    Value regex( pattern );
    Value source( "CHANNEL_MEMBER" );
    unique_ptr<ResultSet> values( this->db_.find(ChannelFullText::TABLE_NAME, "searchEntityFullText", &source, &regex, NULL) );
    list< integer > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      ChannelFullText* channelFullText = static_cast< ChannelFullText* >( *it );
      result.push_back(channelFullText->getEntityId());
    }
    return result;
  }
}
