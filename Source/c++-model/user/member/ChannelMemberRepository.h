/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_MEMBER_REPOSITORY_H__
#define __EJIN_CHANNEL_MEMBER_REPOSITORY_H__

#include "MemberRepository.h"

namespace ejin
{
  /**
   * Update operations on channel member entity. There are a create, update and delete operation used by the channel
   * repository to update membership.
   */
  class ChannelMemberRepository: public MemberRepository {
    friend ChannelHeaderProfile;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ChannelMemberRepository( Database& db, ChangeLogRepository& changeLogRepository, ApprovedMemberRepository& approvedMemberRepository ):
      MemberRepository( db, changeLogRepository, approvedMemberRepository ) {}
    ~ChannelMemberRepository( void ) {};
    
    /** Adds a new channel member reference. Note that the concrete member data must be updated separately.*/
    ChannelMember& add( ChannelMember& channelMember, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /** Update the given channel member entity with the reference data. */          
    bool update( ChannelMember& channelMemberEntity, ChannelMember& channelMember, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /** Removes a channel member reference. */ 
    bool remove( ChannelMember& channelMemberEntity, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Commit the previous data update operation, i.e. cleanup temporary data.
     */ 
    void commit( ChannelMember& channelMemberEntity, const ChannelMember* channelMember, const string& username, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Force clear conflicts on the specified channel member.
     */ 
    bool clearConflict( ChannelMember& channelMemberEntity )
    throw(data_access_error);
    
    /**
     * Revert channel member from backup
     */
    bool revert( integer channelId, const string& username, bool force )
    throw(data_access_error);
    
    /**
     * Decrypt symmetric session key for channel for the specified user.
     */
    bool resolveSessionKey( integer channelId, const string& username, RSACrypto* crypto ) const
    throw(security_integrity_error,data_access_error);

    /**
     * Verifies the public key fingerprints of the members with their current member entity state
     */
    list<string> detectLostKey( integer channelId, const string& username )
      throw(security_integrity_error,data_access_error);

    /**
     * Set encrypted channel session key only for new member.
     */
    bool setAESKey( integer channelId, const string& username, const string& aesKey, bool force )
    throw(security_integrity_error,data_access_error);

    /**
     * Full Text Search for member tags.
     */
    list<integer> searchFullText( const string& pattern )
    throw(data_access_error);
    
    /**
     * Read current session key
     */
    string exportSessionKey( integer channelId, const string& username  ) const
    throw(data_access_error);
    
    // ==========
    // Private Interface
    // ==========    
  private:

    /** Clone channel member data set in case of conflict */          
    bool cloneChannelMember( ChannelMember& channelMemberEntity, const ChannelMember* channelMember )
    throw(data_access_error);

    /*
     * Reset delete marker on owner entites
     */
    void clearDeleteOwnerInd( ChannelMember& channelMemberEntity )
    throw(data_access_error);

  };
  
}

#endif // __EJIN_CHANNEL_MEMBER_REPOSITORY_H__
