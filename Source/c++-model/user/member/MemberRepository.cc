/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberRepository.h"
#include "UsingEjinTypes.h"

#include "Member.h"
#include "ApprovedMemberRepository.h"
#include "ChangeLogRepository.h"
#include "ApprovedMember.h"
#include "ChannelMember.h"
#include "Utilities.h"
#include "KeySequence.h"
#include "RSACrypto.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool MemberRepository::detectConflict( const string& username ) const throw(data_access_error)
  {
    assert ( ! username.empty() );
    
    shared_ptr<Member> member = this->loadMemberInternal( Value(username.c_str()), Value(true) );
    return (bool)member;
  }
  
  bool MemberRepository::clearConflict( const string& username ) throw(data_access_error)
  {
    assert ( ! username.empty() );
    
    shared_ptr<Member> memberEntity = this->loadMemberInternal( Value(username.c_str()), Value(false) );
    shared_ptr<Member> conflict = this->loadMemberInternal( Value(username.c_str()), Value(true) );
    
    bool cleared = false;
    if ( conflict ) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      if ( memberEntity ) {
        if ( memberEntity->hasContent() ) {
          memberEntity->setSyncInd( kSyncIndSynchronous );
          memberEntity->setModificationMask(kNoneModification);
          this->db_.update( *memberEntity );
        } else {
          this->db_.remove( *memberEntity );
        }
      }
      cleared = true;
    }
    return cleared;
  }
  
  shared_ptr<Member> MemberRepository::update( Member& member, bool force, const jtime& syncTime ) throw(data_access_error)
  {
    assert( member.hasUsername() );
    
    shared_ptr<Member> memberEntity = this->loadMemberInternal( Value(member.getUsername().c_str()), Value(false) );
    
    // update algorithm
    unique_ptr<Member> clone;
    if ( memberEntity ) {
      
      // setup sync anchor keys
      KeySequence entitySyncAnchor(memberEntity->getSyncAnchor());
      KeySequence nextSyncAnchor = entitySyncAnchor;
      KeySequence memberSyncAnchor;
      if ( member.hasSyncAnchor() ) {
        memberSyncAnchor = KeySequence(member.getSyncAnchor());
        if (memberSyncAnchor > entitySyncAnchor) {
          nextSyncAnchor = memberSyncAnchor;
        }
      }
      
      if ( force || nextSyncAnchor >= entitySyncAnchor ) {
        if ( member.isEmpty() )  {
          memberEntity->setSyncAnchor( member.getSyncAnchor() );
          removeMember( *memberEntity, nextSyncAnchor );
          this->changeLogRepository_.remove( *memberEntity, nextSyncAnchor, syncTime );
        } else {
          if ( updateMember( *memberEntity, member, nextSyncAnchor, syncTime ) ) {
            this->changeLogRepository_.update( *memberEntity, nextSyncAnchor, syncTime );
          }
        }
        
      } else {
        memberEntity.reset();
        
      }
      
    } else {
      
      if (! member.isEmpty()) {
        // add member entity
        memberEntity = addMember( member, syncTime );
        if (memberEntity) {
          this->changeLogRepository_.add( *memberEntity, member.getSyncAnchor(), syncTime );
        }
      } else {
        memberEntity.reset();
      }
    }
    
    return memberEntity;
  }
  
  void MemberRepository::commit( Member& member, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( member.hasId() );
    assert( member.hasUsername() );
    assert( ! syncAnchor.empty() );
    
    shared_ptr<Member> memberEntity = shared_ptr<Member>(new Member(member.getId()));
    this->db_.refresh( *memberEntity ); // throws DataRetrievalFailureException if not found
    
    memberEntity->setSyncAnchor( syncAnchor );
    memberEntity->setModifyTime( syncTime );
    switch (memberEntity->getSyncInd()) {
      case kSyncIndConflict:
        memberEntity->setModificationMask(kMemberConflictedModification);
        this->db_.update( *memberEntity );
        this->changeLogRepository_.update( *memberEntity, syncAnchor, syncTime );
        break;
      case kSyncIndUpdate:
        // update attachment meta data
        memberEntity->setModificationMask( kNoneModification );
        memberEntity->setSyncInd( kSyncIndSynchronous );
        this->changeLogRepository_.update( *memberEntity, syncAnchor, syncTime );
      case kSyncIndSynchronous:
        this->db_.update( *memberEntity );
        break;
      default:
        assert(false); // invalid state
    }
    
    // commit approvals
    Value memberId(memberEntity->getId());
    unique_ptr<ResultSet> allApprovedMemberEntities =
    db_.find(ApprovedMember::TABLE_NAME, "findApprovedMembersByMemberId", &memberId, NULL);
    for ( vector< BaseEntity* >::iterator it = allApprovedMemberEntities->begin();
         it != allApprovedMemberEntities->end();
         it++ ) {
      ApprovedMember* approvedMemberEntity( static_cast<ApprovedMember*>(*it) );
      approvedMemberRepository_.commit(*approvedMemberEntity, syncAnchor, syncTime );
    }
  }
  
  bool MemberRepository::revert( const string& username, bool force )
  throw(data_access_error)
  {
    assert( ! username.empty() );
    
    bool reverted = false;
    Value gid(Value(username.c_str()));
    shared_ptr<Member> memberEntity( this->loadMemberInternal( gid, Value(false) ) );
    if ( memberEntity != NULL )
    {
      // ensure that there a no pending conflicts
      if ( clearConflict( username ) ) {
        // reload after conflict has been cleared
        memberEntity = this->loadMemberInternal( gid, Value(false) );
      }
      
      switch ( memberEntity->getSyncInd() ) {
        case kSyncIndInsert:
        {
          if (force) {
            db_.remove( *memberEntity );
            reverted = true;
          } else {
            cloneMember( *memberEntity, NULL, NULL);
            reverted = true;
          }
          break;
        }
        case kSyncIndSynchronous:
        case kSyncIndUpdate:
        case kSyncIndDelete:
        {
          // load entity from backup
          shared_ptr<Member> memberEntityBak( shared_ptr<Member>(new Member()) );
          memberEntityBak->setId(memberEntity->getId());
          db_.refresh(*memberEntityBak, memberEntityBak->backupSchemaName() );
          if ( force ) {
            memberEntity->copyDataFrom( *memberEntityBak );
            memberEntity->setSyncInd( kSyncIndSynchronous );
            memberEntity->setModificationMask( kNoneModification );
            this->db_.update( *memberEntity );
            reverted = true;
          } else {
            KeySequence syncAnchor( memberEntity->getSyncAnchor() );
            reverted = cloneMember( *memberEntity, memberEntityBak.get(), &syncAnchor );
          }
          break;
        }
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "member[id=%d] in conflict state", memberEntity->getId());
      }
      
      approvedMemberRepository_.revertApprovals( memberEntity->getId() );
      
    } else {
      // restore old member if found in backup
      unique_ptr<ResultSet> values( db_.find(Member::TABLE_NAME, "findMemberByUsernameBak", &gid, NULL) );
      vector< BaseEntity* >::iterator it = values->begin();
      if (it != values->end()) {
        Member* member = static_cast< Member* >( *it );
        this->db_.insert( *member );
        it = values->take( it );
        reverted = true;
      }
      
    }
    
    return reverted;
  }
  
  bool MemberRepository::isRSAKeyRevoked( const string& username ) const
  throw(security_integrity_error,data_access_error)
  {
    assert( ! username.empty() );
    
    Value gid(Value(username.c_str()));
    shared_ptr<Member> memberEntity( this->loadMemberInternal( gid, Value(false) ) );
    if ( ! memberEntity ) {
      //ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "local member[name=%s] not found",  username.c_str());
      return false;
    }
    return memberEntity->isRevokeKey();
  }
  
  shared_ptr<RSACrypto> MemberRepository::getPublicRSAKey( const string& username ) const
  throw(security_integrity_error,data_access_error)
  {
    assert( ! username.empty() );
    
    Value gid(Value(username.c_str()));
    shared_ptr<Member> memberEntity( this->loadMemberInternal( gid, Value(false) ) );
    if ( ! memberEntity ) {
      //ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "local member[name=%s] not found",  username.c_str());
      return shared_ptr<RSACrypto>( );
    }
    if (memberEntity->hasPubKey() && ! memberEntity->isRevokeKey()) {
      return shared_ptr<RSACrypto>(RSACrypto::fromPublicKeyASN1( memberEntity->getPubKey() ));
    }
    return shared_ptr<RSACrypto>( );
  }
  
  shared_ptr<RSACrypto> MemberRepository::generateRSAKey( const string& username, const void *seed, int num, bool update )
  throw(security_integrity_error,data_access_error)
  {
    assert( ! username.empty() );
    
    Value gid(Value(username.c_str()));
    shared_ptr<Member> memberEntity( this->loadMemberInternal( gid, Value(false) ) );
    if ( ! memberEntity ) {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "local member[name=%s] not found",  username.c_str());
    }
    
    try {
      RSACrypto::initSeed( seed, num );
      shared_ptr<RSACrypto> crypto = shared_ptr<RSACrypto>(RSACrypto::generateRSAKeyPair( ));
      if (update) {
        memberEntity->setPubKey( crypto->getPublicKeyASN1() );
        memberEntity->setFingerPrint( crypto->getPublicKeyFingerPrint() );
        memberEntity->clearCreateTime();
        memberEntity->setSyncInd( kSyncIndUpdate );
        this->db_.update( *memberEntity );
      }
      return crypto;
    } catch (runtime_error e) {
      _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "RSA key generation failed");
    }
    return shared_ptr<RSACrypto>( );
  }
  
  // ==========
  // Private Interface
  // ==========
  
  shared_ptr<Member> MemberRepository::addMember( Member& member, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( member.hasUsername() );
    
    // create new entity with unique global key
    shared_ptr<Member> memberEntity = shared_ptr<Member>(new Member( member ));
    memberEntity->clearId(); // generate new local database identifier
    memberEntity->setSyncInd( kSyncIndSynchronous );
    memberEntity->setModificationMask( kMemberRecentRemoteModification );
    
    // persist entity
    this->db_.insert( *memberEntity );
    
    // set id
    member.setId( memberEntity->getId() );
    member.setSyncInd( memberEntity->getSyncInd() );
    
    updateApprovedMembers( member, NULL, syncTime );
    
    return memberEntity;
  }
  
  bool MemberRepository::updateMember( Member& memberEntity, Member& member, const KeySequence& syncAnchor, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( memberEntity.hasId() );
    assert( ! memberEntity.isHidden() );
    assert( member.hasUsername() );
    
    bool result = false;
    switch ( memberEntity.getSyncInd() ) {
      case kSyncIndSynchronous:
        // entity was updated on the server
        memberEntity.setSyncAnchor( syncAnchor );
        if ( ! memberEntity.hasEqualContent( member ) ) {
          memberEntity.setModificationMask(kMemberRecentRemoteModification);
          result = true;
        }
        memberEntity.copyDataFrom( member );
        this->db_.update( memberEntity );
        break;
      case kSyncIndUpdate:
        if ( memberEntity.hasEqualContent( member ) )
        {
          // update only some dates and state data
          memberEntity.copyDataFrom( member );
          memberEntity.setSyncInd( kSyncIndSynchronous );
          memberEntity.setSyncAnchor( syncAnchor );
          memberEntity.setModificationMask( kNoneModification );
          this->db_.update( memberEntity );
          result = true;
        }
        else
        {
          // conflict detected; save local entity for reference
          cloneMember( memberEntity, &member, &syncAnchor );
          result = true;
        }
        break;
      case kSyncIndConflict:
        shared_ptr<Member> conflict( this->loadMemberInternal( Value(memberEntity.getUsername().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", memberEntity.getId());
        }
        if ( conflict->hasEqualContent( member ) )
        {
          // resolve conflict
          switch ( conflict->getSyncInd() ) {
            case kSyncIndSynchronous:
            case kSyncIndConflict:
              _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", memberEntity.getId());
            case kSyncIndUpdate:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // client and server are in sync now again
              // update only some dates and state data
              memberEntity.setSyncInd( kSyncIndSynchronous );
              memberEntity.setSyncAnchor( syncAnchor );
              memberEntity.setModificationMask( kNoneModification );
              memberEntity.copyDataFrom( member );
              this->db_.update( memberEntity );
              result = true;
              break;
          }
        } else {
          // keep original local conflict data
          // update only some dates and state data
          memberEntity.setSyncInd( kSyncIndConflict );
          memberEntity.setSyncAnchor( syncAnchor );
          memberEntity.copyDataFrom( member );
          this->db_.update( memberEntity );
          result = true;
        }
        break;
    }
    
    // update id
    member.setId( memberEntity.getId() );
    
    // update approvals
    Value memberId( member.getId() );
    unique_ptr<ResultSet> allApprovedMemberEntities = db_.find(ApprovedMember::TABLE_NAME, "findApprovedMembersByMemberId", &memberId, NULL);
    updateApprovedMembers( member, allApprovedMemberEntities.get(), syncTime );
    
    return result;
  }
  
  bool MemberRepository::removeMember( Member& memberEntity, const KeySequence& syncAnchor )
  throw(data_access_error)
  {
    assert( memberEntity.hasId() );
    assert( ! memberEntity.isHidden() );
    
    switch ( memberEntity.getSyncInd() ) {
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        cloneMember( memberEntity, NULL, &syncAnchor );
        break;
      case kSyncIndSynchronous:
      {
        Value username( memberEntity.getUsername() );
        unique_ptr<Value> required( db_.valueOf(ChannelMember::TABLE_NAME, "requiredMember", &username, NULL) );
        if (! required->bol()) {
          // local unchanged entity was removed on the server
          deleteMember( memberEntity );
        }
        break;
      }
      case kSyncIndConflict:
        shared_ptr<Member> conflict( this->loadMemberInternal( Value(memberEntity.getUsername().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", memberEntity.getId());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndConflict:
            _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", memberEntity.getId());
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            // and mark entity as removed from server
            memberEntity.clearData();
            memberEntity.setSyncInd( kSyncIndConflict );
            memberEntity.setSyncAnchor( syncAnchor );
            this->db_.update( memberEntity );
            break;
        }
        break;
    }
    return true;
  }
  
  void MemberRepository::deleteMember( Member& memberEntity )
  throw(data_access_error)
  {
    assert( memberEntity.hasId() );
    
    // delete entity itself
    this->db_.remove(memberEntity);
  }
  
  bool MemberRepository::cloneMember( Member& memberEntity, const Member* member, const KeySequence* syncAnchor )
  throw(data_access_error)
  {
    if ( member == NULL || ! memberEntity.hasEqualContent( *member ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<Member> clone = unique_ptr<Member>(new Member( memberEntity ));
      clone->clearId( );
      clone->setMasterId( memberEntity.getId() );
      clone->setModificationMask( clone->getModificationMask()|kMemberConflictedModification );
      
      // update only some dates and states
      if ( member )
      {
        // update dates and states
        memberEntity.copyDataFrom( *member );
      }
      else
      {
        // mark entity as removed from server
        memberEntity.clearData();
      }
      memberEntity.setSyncInd( kSyncIndConflict );
      if (syncAnchor)
        memberEntity.setSyncAnchor( *syncAnchor );
      memberEntity.setModificationMask( kMemberConflictedModification|kMemberRecentRemoteModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( memberEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      
      return true;
    }
    return false;
  }
  
  bool MemberRepository::updateApprovedMembers( Member& member, ResultSet* allApprovedMemberEntities, const jtime& syncTime )
  throw(data_access_error)
  {
    bool updated = false;
    // ensure correct relationship
    for (list< shared_ptr<ApprovedMember> >::iterator it = member.getApprovedMembers().begin(); it != member.getApprovedMembers().end(); it++ ) {
      (*it)->setMemberId( member.getId() );
    }
    list< shared_ptr<ApprovedMember> > approvedMembers(const_cast<Member&>(member).getApprovedMembers());
    if (allApprovedMemberEntities) {
      for (vector< BaseEntity* >::iterator it = allApprovedMemberEntities->begin(); it != allApprovedMemberEntities->end(); it++ ) {
        ApprovedMember* approvedMemberEntity(static_cast<ApprovedMember*>(*it));
        
        shared_ptr<ApprovedMember> approvedMember = Utilities::findElementIn (approvedMembers, *approvedMemberEntity);
        if (! approvedMember ) {
          if ( approvedMemberEntity->getMemberId() == member.getId() ) {
            // delete resource
            if ( this->approvedMemberRepository_.remove( *approvedMemberEntity, member.getSyncAnchor(), syncTime ) )
            {
              updated = true;
            }
          }
          
        } else {
          approvedMembers.remove(approvedMember);
          // update membership
          this->approvedMemberRepository_.update( *approvedMemberEntity, *approvedMember, member.getSyncAnchor(), syncTime );
          updated = true;
        }
      }
    }
    
    for (list< shared_ptr<ApprovedMember> >::iterator it = approvedMembers.begin(); it != approvedMembers.end(); it++ ) {
      // add membership
      this->approvedMemberRepository_.add( **it, member.getSyncAnchor(), syncTime );
      updated = true;
    }
    
    return updated;
  }
  
}
