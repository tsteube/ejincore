/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEMBER_PROFILE_H__
#define __EJIN_MEMBER_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"
#include "ApprovedMemberRepository.h"

class ChannelHeaderRevertTest;
class ChannelMemberRepositoryImportTest;
namespace ejin
{
  class Member;
  /**
   * Methods to resolve member entities by state.
   */
  class MemberProfile {
    friend class ChannelProfile;
    friend class UserRepository;
    friend class ::ChannelHeaderRevertTest;
    friend class ::ChannelMemberRepositoryImportTest;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    MemberProfile( Database& db, ApprovedMemberRepository& approvedMemberRepository ):
      db_( db ), approvedMemberRepository_(approvedMemberRepository) { }
    // dtor
    virtual ~MemberProfile( void ) {}
    
    /** Find all modified members. */
    list< shared_ptr<Member> > findUpdated( void ) const
    throw(data_access_error);
    
    /** 
     * Find all conflicted member data sets. If the member instance was edited locally 
     * and on the server both data sets
     * are kept to resolve the conflict by user interaction.
     */
    list< shared_ptr<Member> > findConflicted( void ) const
    throw(data_access_error);    

    /**
     * Load a single member entity
     */
    shared_ptr<Member> loadByUsername( const string& name ) const
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    /**
     * Load entity instance from persistence store.
     */ 
    shared_ptr<Member> loadMemberInternal( const sqlite::Value& username, const sqlite::Value& hidden ) const
    throw(data_access_error);

    /**
     * Load entity instance from persistence store.
     */ 
    shared_ptr<ChannelMember> loadChannelMemberInternal( const sqlite::Value& channelId, const sqlite::Value& username, const sqlite::Value& hidden ) const
    throw(data_access_error);

    // --------
    // Member Variables 
    
    ApprovedMemberRepository&  approvedMemberRepository_;
    Database& db_;

  private:
  
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( MemberProfile );
    
  };
  
}

#endif // __EJIN_MEMBER_PROFILE_H__
