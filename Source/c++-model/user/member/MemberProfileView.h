/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEMBER_PROFILE_VIEW_H__
#define __EJIN_MEMBER_PROFILE_VIEW_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"

namespace ejin
{
  /**
   * Entity Class for MEMBER_PROFILE_VIEW
   */
  class MemberProfileView: public sqlite::BaseEntity, DataSchema<MemberProfileView> {
    friend class Database;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "MEMBER_PROFILE_VIEW";

    // ctor
    MemberProfileView( void );
    MemberProfileView( const MemberProfileView& record );
    // dtor
    ~MemberProfileView( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    MemberProfileView& operator=( const MemberProfileView& rhs );
    // clone
    BaseEntity* clone( void ) const { return new MemberProfileView(*this); }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_STRING_PROPERTY_INTF( Username, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( MemberId, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( MemberSyncInd, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( PictureId, 3 )
    ACCESS_INTEGER_PROPERTY_INTF( PictureSyncInd, 4 )

    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
  };
  
}

#endif // __EJIN_MEMBER_PROFILE_VIEW_H__
