/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEMBER_ENTITY_H__
#define __EJIN_MEMBER_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class MemberProfile;
  class ApprovedMember;
  
  /**
   * Entity Class for MEMBER_TBL
   */
  class Member: public BaseEntity, public IBusinessData, public DataSchema<Member> {
    friend class Database;
    friend class MemberProfile;
    friend class MemberRepository;
    friend class ChannelMemberRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "MEMBER_TBL";
    
    // ctors
    Member( void );
    Member( const integer id );
    Member( const string& name );
    Member( const Member& record );
    // dtor
    ~Member( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Member& operator=( const Member& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Member(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool hasContent() const { return true; }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool unmarshalJson( json_t* node );
    //bool marshalJson( json_t* node ) const;
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    ISerializableEntity* entityByXmlNode( const char* name, int position );
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_STRING_PROPERTY_INTF( Username, 1 )
    ACCESS_STRING_PROPERTY_INTF( Email, 2 )
    ACCESS_STRING_PROPERTY_INTF( FirstName, 3 )
    ACCESS_STRING_PROPERTY_INTF( LastName, 4 )
    //ACCESS_STRING_PROPERTY_INTF( PubKey, 5 )
    const std::string getPubKey() const { return asString(5); }
    void setPubKey(const std::string& i);
    bool hasPubKey() const { return ! isNull(5); }
    void clearPubKey();
    ACCESS_TIME_PROPERTY_INTF( PubKeyCreateTime, 6 )
    ACCESS_STRING_PROPERTY_INTF( FingerPrint, 7 )
    //const std::string getFingerPrint() const { return asString(7); }
    //bool hasFingerPrint() const { return ! isNull(7); }
    ACCESS_BOOLEAN_PROPERTY_INTF( RevokeKey, 8 )
    ACCESS_TIME_PROPERTY_INTF( CreateTime, 9 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 10 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 11 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 12 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 13 )
    ACCESS_INTEGER_PROPERTY_INTF( ModificationMask, 14 )
    ACCESS_STRING_PROPERTY_INTF( ProductIdentifier, 15 )
    ACCESS_STRING_PROPERTY_INTF( ProductName, 16 )
    ACCESS_TIME_PROPERTY_INTF( SubscriptionExpireTime, 17 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxChannelCount, 18 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxAttachmentCount, 19 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxAttachmentSize, 20 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxTotalAttachmentSize, 21 )
    ACCESS_INTEGER_PROPERTY_INTF( MaxTextContentLength, 22 )
    ACCESS_STRING_PROPERTY_INTF( Phone, 23 )
    ACCESS_INTEGER_PROPERTY_INTF( DialInCode, 24 )
    ACCESS_BOOLEAN_PROPERTY_INTF( TwoFactorAuthentication, 25 )
    ACCESS_BOOLEAN_PROPERTY_INTF( AppleLogin, 26 )
    ACCESS_STRING_PROPERTY_INTF( EmailHash, 27 )
    bool isHidden() { return this->getMasterId() > 0; }
    
    const std::string getModifiedBy( ) const { return getUsername(); }
    void setModifiedBy( const string& modifiedBy ) {  }
    void clearModifiedBy( void ) {  }
    bool hasModifiedBy( void ) const { return this->hasUsername(); }

    const std::string getFullname( ) const;

    // ==========
    // Metadata
    // ==========
    integer getHierarchyModificationMask( void ) const { return this->getModificationMask( ); }
    void setHierarchyModificationMask( integer mask ) { this->setModificationMask( mask ); }
    
    // ==========
    // Relations
    // ==========
    list< shared_ptr<ApprovedMember> >& getApprovedMembers( void ) { return this->approvedMembers_; }
    void setApprovedMembers( const list< shared_ptr<ApprovedMember> >& approvals ) { this->approvedMembers_ = approvals; }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Member Variables
    
    bool isEmpty_;
    
    list< shared_ptr<ApprovedMember> > approvedMembers_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
}

#endif //__EJIN_MEMBER_ENTITY_H__
