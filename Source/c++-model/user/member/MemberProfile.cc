/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberProfile.h"
#include "UsingEjinTypes.h"

#include "Member.h"
#include "ApprovedMember.h"
#include "ChannelMember.h"
#include "Utilities.h"

namespace ejin
{
  inline list< shared_ptr<Member> > convertToMemberSet( unique_ptr<ResultSet>& values )
  {
    list< shared_ptr<Member> > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
      result.push_back( shared_ptr<Member>( static_cast< Member* >( *it ) ) );
      it = values->take( it );
    }
    return result;
  }  
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<Member> > MemberProfile::findUpdated( void ) const throw(data_access_error) 
  {
    // call entity finder
    Value updatedVal((integer)kSyncIndUpdate);
    unique_ptr<ResultSet> values( this->db_.find(Member::TABLE_NAME, "findMembersByState", &updatedVal, NULL) );
    list< shared_ptr<Member> > result;
    Value hidden(false);
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      Member* member = static_cast< Member* >( *it );
      result.push_back( this->loadMemberInternal( Value(member->getUsername().c_str()), hidden ) );
    }
    return result;
  }
  
  list< shared_ptr<Member> > MemberProfile::findConflicted( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(Member::TABLE_NAME, "findConflictedMembers", NULL) );
    list< shared_ptr<Member> > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
      result.push_back( shared_ptr<Member>( static_cast< Member* >( *it ) ) );
      it = values->take( it );
    }
    return convertToMemberSet( values );
  }
  
  shared_ptr<Member> MemberProfile::loadByUsername( const string& name ) const
  throw(data_access_error)
  {
    assert( ! name.empty() );
    
    Value hidden(false);
    shared_ptr<Member> member(this->loadMemberInternal( Value(name.c_str()), hidden ));
    return member ? member : shared_ptr<Member>();
  }
  
  // ==========
  // Protected Interface
  // ==========
  
  shared_ptr<Member> MemberProfile::loadMemberInternal( const Value& username, const Value& hidden ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(Member::TABLE_NAME, "findMemberByUsername", &username, &hidden, NULL) );
    
    shared_ptr<Member> member;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      member = shared_ptr<Member>(static_cast< Member* >( *it ) );
      it = values->take( it );

      // resolve all approved members
      Value memberId(member->getId());
      ApprovedMember *approvedMember;
      unique_ptr<ResultSet> approvedMembers( db_.find(ApprovedMember::TABLE_NAME, "findApprovedMembersByMemberId", &memberId, NULL) );
      for (vector< BaseEntity* >::iterator it2 = approvedMembers->begin(); it2 != approvedMembers->end(); ) {
        approvedMember = static_cast< ApprovedMember* >(*it2);
        switch (approvedMember->getSyncInd()) {
          case kSyncIndSynchronous:
          case kSyncIndInsert:
            member->getApprovedMembers().push_back(shared_ptr<ApprovedMember>( approvedMember ));
            it2 = approvedMembers->take( it2 );
            break;
          case kSyncIndDelete:
            it2++;
            break;
          case kSyncIndUpdate:
          case kSyncIndConflict:
            assert( false );
            break;
        }
      }
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate member with username '%s'.",  username.str());
    
    return member;
  }
  
  shared_ptr<ChannelMember> MemberProfile::loadChannelMemberInternal( const Value& channelId, const Value& username, const Value& hidden ) const  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(ChannelMember::TABLE_NAME, "findByChannelAndMember", &channelId, &username, &hidden, NULL) );
    
    shared_ptr<ChannelMember> member;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      member = shared_ptr<ChannelMember>(static_cast< ChannelMember* >( *it ) );
      it = values->take( it );
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate channel member with username '%s' in channel %d.",  username.str(), channelId.num() );
    
    return member;
  }
  
}
