/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Member.h"

#include "UsingEjinTypes.h"

#include "ApprovedMember.h"
#include "Utilities.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::ISerializableEntity;
  
  // ==========
  // Schema DDL
  // ==========  
  
  template <> const sqlite::Attribute DataSchema<Member>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("USERNAME", type_text, flag_not_null),
    Attribute("EMAIL", type_text, flag_none),
    Attribute("FIRSTNAME", type_text, flag_not_null),
    Attribute("LASTNAME", type_text, flag_not_null),
    Attribute("PUB_KEY", type_text, flag_none),
    Attribute("PUB_KEY_CREATE_TIME", type_time, flag_none),
    Attribute("FINGER_PRINT", type_text, flag_none),
    Attribute("REVOKE_KEY", type_bool, flag_not_null, Value(false)),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_ANCHOR", type_text, flag_not_null),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute("PRODUCT_IDENTIFIER", type_text, flag_not_null),
    Attribute("PRODUCT_NAME", type_text, flag_not_null),
    Attribute("SUBSCRIPTION_EXPIRE_TIME", type_time, flag_none),
    Attribute("MAX_CHANNEL_COUNT", type_int, flag_none),
    Attribute("MAX_ATTACHMENT_COUNT", type_int, flag_none),
    Attribute("MAX_ATTACHMENT_SIZE", type_int, flag_none),
    Attribute("MAX_TOTAL_ATTACHMENT_SIZE", type_int, flag_none),
    Attribute("MAX_TEXT_CONTENT_LENGTH", type_int, flag_none),    
    Attribute("PHONE", type_text, flag_not_null),
    Attribute("DIAL_IN_CODE", type_int, flag_none),
    Attribute("TWO_FACTOR_AUTHENTICATION", type_bool, flag_not_null, Value(false)),
    Attribute("APPLE_LOGIN", type_bool, flag_not_null, Value(false)),
    Attribute("EMAIL_HASH", type_text, flag_none),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<Member>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Member::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findMemberById"] = "ID = ?";
      tMap["findMemberByUsername"] = "USERNAME = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["findMembersByState"] = "MASTER_ID IS NULL AND SYNC_IND = ?";
      tMap["findMembersOfChannel"] = "SYNC_IND = ? AND MASTER_ID IS NULL";
      tMap["findConflictedMembers"] = "MASTER_ID IS NOT NULL";
      tMap["findAllOrderBySyncAnchorDesc"] = "MASTER_ID IS NULL ORDER BY SYNC_ANCHOR DESC";
      tMap["BackupSet"] = "MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findMemberByUsernameBak"] = "USERNAME = ?";
      tMap["BackupSetBak"] = "MASTER_ID IS NULL";
    }
    return tMap;
  }
  const map<string,string> Member::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "SELECT "
      "count(*) FROM " << schema << ".channel_member_tbl cm LEFT OUTER JOIN member_tbl m "
      "ON (cm.username = m.username) WHERE m.username IS NULL";
      tMap["missingMembers"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT IFNULL(MIN(COALESCE( "
            "CASE outdated1 WHEN 1 THEN -1 END, "
            "CASE username1 WHEN ?2 THEN 1 END, "
            "CASE outdated2 WHEN 1 THEN -2 END, "
            "CASE username2 WHEN ?2 THEN 2 END "
            ")),0) FROM " << schema << ".approval_chain_view "
            "WHERE username0 = ?1 and ?2 in ( username1, username2 )";
      tMap["trustLevel"] = ss.str();

      ss.str("");ss.clear();
      ss << "SELECT CASE "
            "WHEN firstname is not null and lastname is not null THEN firstname || ' ' || lastname "
            "WHEN lastname is not null THEN lastname "
            "WHEN firstname is not null THEN firstname "
            "WHEN apple_login THEN '<apple login>' "
            "ELSE username "
            "END AS name "
            "FROM " << schema << ".member_tbl "
            "WHERE username = ?1";
      tMap["fullname"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["cleanupBak"] = "DELETE FROM mainBak.member_tbl WHERE id NOT IN ( SELECT id FROM main.member_tbl )";
    }
    return tMap;
  }
  const string Member::restoreSet( void ) const {
    return "ID = ?1";
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Member::Member( void ): BaseEntity(TABLE_DDL, false), isEmpty_(false), approvedMembers_()
  {
    this->setSyncInd( kSyncIndInsert );
    this->clearMasterId( );
    this->setRevokeKey( false );
    this->setModificationMask((integer) 0);
  }
  Member::Member( const integer id ): Member()
  {
    setId(id);
  }
  Member::Member( const string& name ): Member()
  {
    setUsername(name);
  }
  Member::Member( const Member& record ): Member()
  {
    this->operator=(record);
  }
  Member::~Member( void ) { }
  
  const std::string Member::getFullname( ) const
  {
    std::ostringstream ss;
    if ( this->hasFirstName() && this->hasLastName() ) {
      ss << this->getFirstName() << " " << this->getLastName() <<
        " (" << this->getUsername() << ")";
    } else if ( this->hasFirstName() ) {
      ss << this->getFirstName() <<
        " (" << this->getUsername() << ")";
    } else if ( this->hasLastName() ) {
      ss << this->getLastName() <<
        " (" << this->getUsername() << ")";
    } else {
      return this->getUsername();
    }
    return ss.str();
  }

  void Member::setPubKey(const std::string& key)
  {
    if (! key.empty()) {
      asString( 5, key );
      asString( 7, RSACrypto::getFingerPrint( key ) );
      asBool( 8, false );
    } else {
      setNull( 5 );
      setNull( 7 );
      asBool( 8, false );
    }
  }
  void Member::clearPubKey()
  {
    setNull( 5 );
    setNull( 7 );
    asBool( 8, false );
  }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Member& Member::operator=( const Member& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Username);
      COPY_PROPERTY((*this), rhs, SyncAnchor);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, MasterId);
      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      COPY_PROPERTY((*this), rhs, PubKey);
      COPY_PROPERTY((*this), rhs, PubKeyCreateTime);
      COPY_PROPERTY((*this), rhs, Email);
      COPY_PROPERTY((*this), rhs, EmailHash);

      COPY_PROPERTY((*this), rhs, ModificationMask);
      COPY_PROPERTY((*this), rhs, ProductIdentifier);
      COPY_PROPERTY((*this), rhs, ProductName);
      COPY_PROPERTY((*this), rhs, SubscriptionExpireTime);
      COPY_PROPERTY((*this), rhs, MaxChannelCount);
      COPY_PROPERTY((*this), rhs, MaxAttachmentCount);
      COPY_PROPERTY((*this), rhs, MaxAttachmentSize);
      COPY_PROPERTY((*this), rhs, MaxTotalAttachmentSize);
      COPY_PROPERTY((*this), rhs, MaxTextContentLength);
      COPY_PROPERTY((*this), rhs, Phone);
      COPY_PROPERTY((*this), rhs, DialInCode);
      COPY_BOOL_PROPERTY((*this), rhs, TwoFactorAuthentication);
      COPY_BOOL_PROPERTY((*this), rhs, AppleLogin);
    }
    return *this;
  }
  
  // comparison
  bool Member::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Member& mrhs = dynamic_cast<const Member&> (rhs);
    if (this->hasUsername() && mrhs.hasUsername())
      return (this->getUsername() == mrhs.getUsername());
    else if (this->hasId() && mrhs.hasId())
      return (this->getId() == mrhs.getId());
    return false;
  }
  bool Member::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Member& mrhs = dynamic_cast<const Member&> (rhs); // throws std::bad_cast
    if (this->hasUsername() && mrhs.hasUsername())
      return (this->getUsername() < mrhs.getUsername());
    else if (this->hasId() && mrhs.hasId())
      return (this->getId() < mrhs.getId());
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  // Database schema
  const char* Member::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& Member::tableFields( void ) const { return TABLE_DDL; }
  
  // XML schema
  const string& Member::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& Member::xmlFields( void ) const { return XML_DDL; }
  
  bool Member::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const Member& mrhs = dynamic_cast<const Member&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, PubKey);
      COPY_PROPERTY((*this), mrhs, PubKeyCreateTime);
      COPY_PROPERTY((*this), mrhs, Email);
      COPY_PROPERTY((*this), mrhs, EmailHash);
      COPY_PROPERTY((*this), mrhs, FirstName);
      COPY_PROPERTY((*this), mrhs, LastName);
      COPY_PROPERTY((*this), mrhs, ModifyTime);
      COPY_PROPERTY((*this), mrhs, ProductIdentifier);
      COPY_PROPERTY((*this), mrhs, ProductName);
      COPY_PROPERTY((*this), mrhs, SubscriptionExpireTime);
      COPY_PROPERTY((*this), mrhs, MaxChannelCount);
      COPY_PROPERTY((*this), mrhs, MaxAttachmentCount);
      COPY_PROPERTY((*this), mrhs, MaxAttachmentSize);
      COPY_PROPERTY((*this), mrhs, MaxTotalAttachmentSize);
      COPY_PROPERTY((*this), mrhs, MaxTextContentLength);
      COPY_PROPERTY((*this), mrhs, Phone);
      COPY_PROPERTY((*this), mrhs, DialInCode);
      COPY_BOOL_PROPERTY((*this), mrhs, TwoFactorAuthentication);
      COPY_BOOL_PROPERTY((*this), mrhs, AppleLogin);

      return true;
    }
    return false;
  }
  bool Member::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    Member& rhs( (Member&)arg );
    
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        return false;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( this->getUsername() != rhs.getUsername() )
      return false;
    if ( this->getFingerPrint() != rhs.getFingerPrint() )
      return false;
    if ( this->isRevokeKey() != rhs.isRevokeKey() )
      return false;
    if ( this->getEmail() != rhs.getEmail() )
      return false;
    if ( this->getEmailHash() != rhs.getEmailHash() )
      return false;
    if ( this->getFirstName() != rhs.getFirstName() )
      return false;
    if ( this->getLastName() != rhs.getLastName() )
      return false;
    if ( this->getProductIdentifier() != rhs.getProductIdentifier() )
      return false;
    if ( this->getMaxChannelCount() != rhs.getMaxChannelCount() )
      return false;
    if ( this->getMaxAttachmentCount() != rhs.getMaxAttachmentCount() )
      return false;
    if ( this->getMaxAttachmentSize() != rhs.getMaxAttachmentSize() )
      return false;
    if ( this->getMaxTotalAttachmentSize() != rhs.getMaxTotalAttachmentSize() )
      return false;
    if ( this->getMaxTextContentLength() != rhs.getMaxTextContentLength() )
      return false;
    if ( this->getPhone() != rhs.getPhone() )
      return false;
    if ( this->getDialInCode() != rhs.getDialInCode() )
      return false;
    if ( this->isTwoFactorAuthentication() != rhs.isTwoFactorAuthentication() )
      return false;
    if ( this->isAppleLogin() != rhs.isAppleLogin() )
      return false;
    return true;
  }
  bool Member::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void Member::archive( void )
  {
  }
  void Member::unarchive( void )
  {
  }
  void Member::clearData( void )
  {
    this->clearPubKey( );
    this->clearPubKeyCreateTime( );
    this->clearEmail( );
    this->clearEmailHash( );
    this->clearModifyTime( );
    this->clearCreateTime( );
    this->clearFirstName( );
    this->clearLastName( );
    this->clearModifyTime( );
    this->clearProductIdentifier( );
    this->clearProductName( );
    this->clearSubscriptionExpireTime( );
    this->clearMaxChannelCount( );
    this->clearMaxAttachmentCount( );
    this->clearMaxAttachmentSize( );
    this->clearMaxTotalAttachmentSize( );
    this->clearMaxTextContentLength( );
    this->clearPhone( );
    this->clearDialInCode( );
    this->clearTwoFactorAuthentication( );
    this->clearAppleLogin( );
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // ======= JSON Mapping
  
  bool Member::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasFirstName() ) {
      j["firstname"] = string( this->value("FIRSTNAME")->str(), this->value("FIRSTNAME")->size() );
    }
    if ( this->hasLastName() ) {
      j["lastname"] = string( this->value("LASTNAME")->str(), this->value("LASTNAME")->size() );
    }
    if ( this->hasEmail() ) {
      j["email"] = string( this->value("EMAIL")->str(), this->value("EMAIL")->size() );
    }
    if (this->hasPubKey() && ! this->isRevokeKey()) {
      j["key"] = string( this->value("PUB_KEY")->str(), this->value("PUB_KEY")->size() );
    }
    if ( this->hasPhone() ) {
      j["phone"] = string( this->value("PHONE")->str(), this->value("PHONE")->size() );
    }
    if ( this->hasDialInCode() ) {
      j["dial_in_code"] = this->value("DIAL_IN_CODE")->num();
    }
    if ( this->isTwoFactorAuthentication() ) {
      j["two_factor_authentication"] = true;
    }

    if ( ! this->approvedMembers_.empty() ) {
      json a = json::array();
      for (list< shared_ptr<ApprovedMember> >::const_iterator it=this->approvedMembers_.begin();
           it != this->approvedMembers_.end(); it++) {
        json j2({});
        if ( (*it)->marshalJson( j2 ) ) {
          a.push_back(j2);
        }
      }
      j["approved"] = a;
    }
    
    return true;
  }
  /*
  bool Member::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if (this->hasFirstName()) {
      json_object_set_new( node, "firstname", json_stringn_nocheck( this->value("FIRSTNAME")->str(),
                                                                    this->value("FIRSTNAME")->size() ) );
    }
    if (this->hasLastName()) {
      json_object_set_new( node, "lastname", json_stringn_nocheck( this->value("LASTNAME")->str(),
                                                                   this->value("LASTNAME")->size() ) );
    }
    if (this->hasEmail()) {
      json_object_set_new( node, "email", json_stringn_nocheck( this->value("EMAIL")->str(),
                                                               this->value("EMAIL")->size() ) );
    }
    if (this->hasPubKey() && ! this->isRevokeKey()) {
      json_object_set_new( node, "key", json_stringn_nocheck( this->value("PUB_KEY")->str(),
                                                             this->value("PUB_KEY")->size() ) );
    }
    if (this->hasPhone()) {
      json_object_set_new( node, "phone", json_stringn_nocheck( this->value("PHONE")->str(),
                                                              this->value("PHONE")->size() ) );
    }
    if (this->hasDialInCode()) {
      integer code = this->value("DIAL_IN_CODE")->num();
      json_object_set_new( node, "dial_in_code", json_integer( code ) );
    }
    if (this->isTwoFactorAuthentication()) {
      json_object_set_new( node, "two_factor_authentication", json_true( ) );
    }

    if ( ! this->approvedMembers_.empty() ) {
      json_t* array = json_array();
      json_object_set_new( node, "approved", array );
      for (list< shared_ptr<ApprovedMember> >::const_iterator it=this->approvedMembers_.begin();
           it != this->approvedMembers_.end(); it++) {
        json_t* child = json_object();
        if ( (*it)->marshalJson( child ) ) {
          json_array_append_new( array, child );
        }
      }
    }
    return true;
  }
  */

  bool Member::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }
      if ( j.contains("key")) {
        this->value("PUB_KEY")->str( j["key"] );
      }
      if ( j.contains("pub_key_create_time")) {
        this->value("PUB_KEY_CREATE_TIME")->str( j["pub_key_create_time"] );
      }
      if ( j.contains("email")) {
        this->value("EMAIL")->str( j["email"] );
      }
      if ( j.contains("email_hash")) {
        this->value("EMAIL_HASH")->str( j["email_hash"] );
      }
      if ( j.contains("product_identifier")) {
        this->value("PRODUCT_IDENTIFIER")->str( j["product_identifier"] );
      }
      if ( j.contains("product_name")) {
        this->value("PRODUCT_NAME")->str( j["product_name"] );
      }
      if ( j.contains("subscription_expire_time")) {
        this->value("SUBSCRIPTION_EXPIRE_TIME")->str( j["subscription_expire_time"] );
      }
      if ( j.contains("firstname")) {
        this->value("FIRSTNAME")->str( j["firstname"] );
      }
      if ( j.contains("lastname")) {
        this->value("LASTNAME")->str( j["lastname"] );
      }
      if ( j.contains("max_channel_count")) {
        this->value("MAX_CHANNEL_COUNT")->num( j["max_channel_count"] );
      }
      if ( j.contains("max_attachment_count")) {
        this->value("MAX_ATTACHMENT_COUNT")->num( j["max_attachment_count"] );
      }
      if ( j.contains("max_attachment_size")) {
        this->value("MAX_ATTACHMENT_SIZE")->num( j["max_attachment_size"] );
      }
      if ( j.contains("max_total_attachment_size")) {
        this->value("MAX_TOTAL_ATTACHMENT_SIZE")->num( j["max_total_attachment_size"] );
      }
      if ( j.contains("max_text_content_length")) {
        this->value("MAX_TEXT_CONTENT_LENGTH")->num( j["max_text_content_length"] );
      }
      if ( j.contains("phone")) {
        this->value("PHONE")->str( j["phone"] );
      }
      if ( j.contains("dial_in_code")) {
        this->value("DIAL_IN_CODE")->num( j["dial_in_code"] );
      }
      if ( j.contains("two_factor_authentication")) {
        this->value("TWO_FACTOR_AUTHENTICATION")->bol( j["two_factor_authentication"] );
      }
      if ( j.contains("apple_login")) {
        this->value("APPLE_LOGIN")->bol( j["apple_login"] );
      }

      // unmarshal approved members
      if ( j.contains("approved")) {
        json j2 = j["approved"];
        if ( j2.is_array() ) {
          for (int i=0; i<j2.size(); i++) {
            shared_ptr<ApprovedMember> entity = Utilities::findOrAppend( this->approvedMembers_, i );
            if (! entity || ! entity->unmarshalJson( j2[i] )) {
              return false;
            }
          }
        }
      }

      return true;
    }
    
    return false;
  }
  /*
  bool Member::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "key");
    if ( json_is_string(value) ) {
      this->value("PUB_KEY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "pub_key_create_time");
    if ( json_is_string(value) ) {
      this->value("PUB_KEY_CREATE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "email");
    if ( json_is_string(value) ) {
      this->value("EMAIL")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "email_hash");
    if ( json_is_string(value) ) {
      this->value("EMAIL_HASH")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "product_identifier");
    if ( json_is_string(value) ) {
      this->value("PRODUCT_IDENTIFIER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "product_name");
    if ( json_is_string(value) ) {
      this->value("PRODUCT_NAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "subscription_expire_time");
    if ( json_is_string(value) ) {
      this->value("SUBSCRIPTION_EXPIRE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "firstname");
    if ( json_is_string(value) ) {
      this->value("FIRSTNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "lastname");
    if ( json_is_string(value) ) {
      this->value("LASTNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "max_channel_count");
    if ( json_is_number(value) ) {
      this->value("MAX_CHANNEL_COUNT")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_attachment_count");
    if ( json_is_number(value) ) {
      this->value("MAX_ATTACHMENT_COUNT")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_attachment_size");
    if ( json_is_number(value) ) {
      this->value("MAX_ATTACHMENT_SIZE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_total_attachment_size");
    if ( json_is_number(value) ) {
      this->value("MAX_TOTAL_ATTACHMENT_SIZE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "max_text_content_length");
    if ( json_is_number(value) ) {
      this->value("MAX_TEXT_CONTENT_LENGTH")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "phone");
    if ( json_is_string(value) ) {
      this->value("PHONE")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "dial_in_code");
    if ( json_is_number(value) ) {
      this->value("DIAL_IN_CODE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "two_factor_authentication");
    if ( json_is_string(value) ) {
      this->value("TWO_FACTOR_AUTHENTICATION")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "apple_login");
    if ( json_is_string(value) ) {
      this->value("APPLE_LOGIN")->bytea( json_string_value(value), json_string_length(value) );
    }

    // unmarshal approved members
    value = json_object_get(node, "approved");
    if (json_is_array(value)) {
      size_t index;
      json_t* child;
      json_array_foreach(value, index, child) {
        shared_ptr<ApprovedMember> entity = Utilities::findOrAppend( this->approvedMembers_, index );
        if (! entity || ! entity->unmarshalJson( child )) {
          return false;
        }
      }
    }
    
    return true;
  }
  */

  // ======= XML Mapping
  
  const string Member::XML_NAME   = "member";
  const ser::XmlAttribute Member::XML_FIELDS[] =
  {
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute("key", "PUB_KEY" ),
    ser::XmlAttribute("pub_key_create_time", "PUB_KEY_CREATE_TIME" ),
    ser::XmlAttribute("email", "EMAIL" ),
    ser::XmlAttribute("email_hash", "EMAIL_HASH" ),
    ser::XmlAttribute("product_identifier", "PRODUCT_IDENTIFIER" ),
    ser::XmlAttribute("product_name", "PRODUCT_NAME" ),
    ser::XmlAttribute("subscription_expire_time", "SUBSCRIPTION_EXPIRE_TIME" ),
    ser::XmlAttribute("firstname", "FIRSTNAME" ),
    ser::XmlAttribute("lastname", "LASTNAME" ),
    ser::XmlAttribute("max_channel_count", "MAX_CHANNEL_COUNT" ),
    ser::XmlAttribute("max_attachment_count", "MAX_ATTACHMENT_COUNT" ),
    ser::XmlAttribute("max_attachment_size", "MAX_TOTAL_ATTACHMENT_SIZE" ),
    ser::XmlAttribute("max_total_attachment_size", "MAX_TOTAL_ATTACHMENT_SIZE" ),
    ser::XmlAttribute("max_text_content_length", "MAX_TEXT_CONTENT_LENGTH" ),
    ser::XmlAttribute("phone", "PHONE" ),
    ser::XmlAttribute("dial_in_code", "DIAL_IN_CODE" ),
    ser::XmlAttribute("two_factor_authentication", "TWO_FACTOR_AUTHENTICATION" ),
    ser::XmlAttribute("apple_login", "APPLE_LOGIN" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet Member::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  ISerializableEntity* Member::entityByXmlNode( const char* name, int position )
  {
    ISerializableEntity* result;
    if ( strcmp("approved", name) == 0 ) {
      shared_ptr<ApprovedMember> entity = Utilities::findOrAppend( this->getApprovedMembers(), position );
      return entity.get();
    } else {
      result = SerializableEntity::entityByXmlNode(name, position);
      
    }
    return result;
  }
  
  bool Member::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("member"); // start element tag
    
    // serialize properties
    if (this->hasEmail()) {
      xml << tag("email") << chardata() << this->getEmail() << endtag();
    }
    if (this->hasFirstName()) {
      xml << tag("firstname") << chardata() << this->getFirstName() << endtag();
    }
    if (this->hasLastName()) {
      xml << tag("lastname") << chardata() << this->getLastName() << endtag();
    }
    if (this->hasPubKey() && ! this->isRevokeKey()) {
      xml << tag("key") << chardata() << this->getPubKey() << endtag();
    }
    if (this->hasPhone()) {
      xml << attr("phone") << chardata() << this->getLastName() << endtag();
    }
    if (this->hasDialInCode()) {
      xml << attr("dial_in_code") << this->getDialInCode();
    }
    if (this->isTwoFactorAuthentication()) {
      xml << attr("two_factor_authentication") << "true";
    }

    // print out approved if any
    for (list< shared_ptr<ApprovedMember> >::const_iterator it=this->approvedMembers_.begin(); it != this->approvedMembers_.end(); it++) {
      (*it)->marshalXml( xml, aesKey, keyLength );
    }
    
    xml << ser::endtag(); // end element tag
    return true;
  }
  
}
