/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MemberProfileView.h"
#include "UsingEjinTypes.h"

#include "SqliteValue.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  
  template <> const Attribute DataSchema<MemberProfileView>::TABLE_FIELDS[] =
  {
    Attribute("USERNAME", type_text, flag_primary_key),
    Attribute("MEMBER_ID", type_text, flag_not_null),
    Attribute("MEMBER_SYNC_IND", type_int, flag_not_null),
    Attribute("PICTURE_ID", type_text, flag_not_null),
    Attribute("PICTURE_SYNC_IND", type_int, flag_not_null),
    Attribute()
  };
  template <> const AttributeSet DataSchema<MemberProfileView>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> MemberProfileView::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["modifiedMember"] = "username = ? AND MAX(member_sync_ind,picture_sync_ind) IN (1,2,3)";
      tMap["modifiedMembers"] = "MAX(member_sync_ind, picture_sync_ind) IN (1,2,3)";
      tMap["conflictedMembers"] = "MAX(member_sync_ind, picture_sync_ind) = 4";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  MemberProfileView::MemberProfileView( void ): BaseEntity(TABLE_DDL, true)
  { }
  MemberProfileView::MemberProfileView( const MemberProfileView& record ): BaseEntity(record)
  { this->operator=(record); }
  MemberProfileView::~MemberProfileView( void )
  { }
  
  // ==========
  // Object Interface
  // ==========
  
  MemberProfileView& MemberProfileView::operator=( const MemberProfileView& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      COPY_PROPERTY((*this), rhs, Username);
      COPY_PROPERTY((*this), rhs, MemberId);
      COPY_PROPERTY((*this), rhs, MemberSyncInd);
      COPY_PROPERTY((*this), rhs, PictureId);
      COPY_PROPERTY((*this), rhs, PictureSyncInd);
    }
    return *this;
  }
  
  // comparison
  bool MemberProfileView::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const MemberProfileView& mrhs = dynamic_cast<const MemberProfileView&> (rhs);
    return getUsername() == mrhs.getUsername();
  }
  bool MemberProfileView::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const MemberProfileView& mrhs = dynamic_cast<const MemberProfileView&> (rhs); // throws std::bad_cast if not of type Member&
    return this->getUsername() < mrhs.getUsername();
  }
  
  const char* MemberProfileView::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& MemberProfileView::tableFields( void ) const { return TABLE_DDL; }
  
}
