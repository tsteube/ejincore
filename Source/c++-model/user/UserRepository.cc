/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UserRepository.h"
#include "UsingEjinTypes.h"

#include "User.h"
#include "MemberRepository.h"
#include "PictureRepository.h"
#include "Utilities.h"
#include "KeySequence.h"
#include "RSACrypto.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool UserRepository::clearConflicts( const list<string>& members )
  throw(data_access_error)
  {
    bool success = true;
    for (list<string>::const_iterator it=members.begin(); it != members.end(); it++) {
      if (! memberRepository_.clearConflict( *it )) {
        success = false;
      }
      if (! pictureRepository_.clearConflict( *it )) {
        success = false;
      }
    }
    return success;
  }
  
  User& UserRepository::update( User& user, bool force, const jtime& syncTime ) throw(data_access_error)
  {
    assert( user.hasUsername() );
    
    if ( user.isEmpty() ) {
      // delete member
      Member member( user.getUsername() );
      member.setSyncAnchor( user.getSyncAnchor() );
      member.setModifiedBy( user.getUsername() );
      member.setEmpty( true );
      user.setMember( memberRepository_.update( member, force, syncTime ) );
    } else {
      if ( user.hasMember() ) {
        Member& member( *user.getMember() );
        member.setUsername( user.getUsername() );
        member.setModifiedBy( user.getUsername() );
        member.setSyncAnchor( user.getSyncAnchor() );
        user.setMember( memberRepository_.update( member, force, syncTime ) );
      }
      
      shared_ptr<Member> member = user.getMember();
      if ( ! member ) {
        member = memberRepository_.loadMemberInternal( Value(user.getUsername().c_str()), Value(false) );
      }
      if (user.hasPicture()) {
        Picture& picture( *user.getPicture() );
        picture.setModifiedBy( user.getUsername() );
        picture.setMemberId( member->getId() );
        picture.setEmpty( !picture.hasContent() );
        user.setPicture( pictureRepository_.update( picture, user.getSyncAnchor(), syncTime ) );
      }
    }
    
    return user;
  }
  
  void UserRepository::commit( User& user, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( user.hasUsername() );
    assert( ! syncAnchor.empty() );

    if ( user.hasMember() ) {
      Member& member( *user.getMember() );
      member.setModifiedBy( user.getUsername() );
      memberRepository_.commit( member, syncAnchor, syncTime );
    }
    if ( user.hasPicture() ) {
      Picture& picture( *user.getPicture() );
      picture.setModifiedBy( user.getUsername() );
      pictureRepository_.commit( picture, syncAnchor, syncTime );
    }
  }
  
  bool UserRepository::revert( const string& path, bool force )
  throw(data_access_error)
  {
    std::list<string> segments;
    Utilities::split(segments, path, "/");
    std::list<string>::iterator it=segments.begin();
    while ( it != segments.end() && (*it).empty() ) it++; // skip leading empty segment
    
    if ( it != segments.end() && strcasecmp((*it).c_str(), "member" ) == 0 && ++it != segments.end())
    {
      return memberRepository_.revert( *it++, force );
    }
    if ( it != segments.end() && strcasecmp((*it).c_str(), "picture" ) == 0 && ++it != segments.end())
    {
      return pictureRepository_.revert( *it++, force );
    }
    
    return false;
  }
  
  bool UserRepository::isRSAKeyRevoked( const string& username ) const
  throw(security_integrity_error,data_access_error)
  {
    return memberRepository_.isRSAKeyRevoked( username );
  }
  
  shared_ptr<RSACrypto> UserRepository::getPublicRSAKey( const string& username ) const
  throw(security_integrity_error,data_access_error)
  {
    return memberRepository_.getPublicRSAKey( username );
  }
  
  shared_ptr<RSACrypto> UserRepository::generateRSAKey( const string& username, const void *seed, int num, bool update )
  throw(security_integrity_error,data_access_error)
  {
    return memberRepository_.generateRSAKey( username, seed, num, update );
  }
  
}
