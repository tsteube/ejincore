/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "User.h"

#include "UsingEjinTypes.h"

#include <iostream>
#include <sstream>

#include "Utilities.h"
#include "SqliteBaseEntity.h"
#include "IBusinessData.h"
#include "Member.h"
#include "Picture.h"
#include "Resource.h"
#include "SqliteValue.h"

namespace ejin
{
  using ser::tag;
  using ser::attr;
  using ser::chardata;
  using ser::endtag;
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  User::User( void ):
  username_(type_text), syncAnchor_(type_text), member_(), picture_(), isEmpty_(false)
  {
  }
  User::User( const string& username ): User()
  {
    this->username_.str(username.c_str());
  }
  User::User( const string& username, const string& syncAnchor ): User()
  {
    this->username_.str(username.c_str());
    this->syncAnchor_.str( syncAnchor );
  }
  User::User( const string& username, const shared_ptr<Member> member = NULL, const shared_ptr<Picture> picture = NULL ) : User( )
  {
    this->username_.str(username.c_str());
    this->member_ = member;
    this->picture_ = picture;
  }
  User::User( const User& user ): User()
  {
    this->username_ = user.username_;
    this->picture_ = user.picture_;
    this->member_ = user.member_;
    this->syncAnchor_ = user.syncAnchor_;
  }
  User::~User( void ) { }  
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  User& User::operator=( const User& rhs ) {    
    if (this != &rhs) {
      this->username_ = rhs.username_;
      this->picture_ = rhs.picture_;
      this->member_ = rhs.member_;
      this->syncAnchor_ = rhs.syncAnchor_;
    }
    return *this;
  }
  
  // comparison
  bool User::operator==( const User& rhs ) const {
    if (this == &rhs) 
      return true;
    
    return (this->username_ == rhs.username_);
  }  
  bool User::operator<( const User& rhs ) const {
    if (this == &rhs) 
      return false;
    
    if (this->username_.num() < rhs.username_.num())
      return true;
    return false;
  }
  // debug
  string User::toString( void ) const {
    std::ostringstream buf;
    buf << "User[" <<
    "username=" << this->username_.toString() <<
    ",syncAnchor=" << this->syncAnchor_.toString() <<
    ",member=" << ((bool)this->member_) <<
    ",picture=" << ((bool)this->picture_) <<
    "]";
    return buf.str();
  }

  // ==========
  // IBusinessData Interface
  // ==========
  
  void User::clearBusinessData( void )
  {
    if (this->member_)
      this->member_.reset();
    if (this->picture_)
      this->picture_.reset();
  }

  // ==========
  // Getter / Setter
  // ==========

  void User::setMember( shared_ptr<Member> member )
  {
    this->member_ = member;
    if (this->member_) {
      if (this->member_->hasUsername()) {
        this->username_ = this->member_->getUsername();
      } else if (this->hasUsername()) {
        this->member_->setUsername( getUsername() );
      }
    }
  }

  bool User::hasConflict( void )
  {
    if (this->member_)
      if (this->member_->getSyncInd() == kSyncIndConflict)
        return true;
    if (this->picture_)
      if (this->picture_->getSyncInd() == kSyncIndConflict)
        return true;
        return false;
  }

  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON mapping
  
  bool User::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasUsername() ) {
      j["name"] = string(username_.str(), username_.size());
    }
    if ( this->member_ ) {
      json j2({});
      if ( this->member_->marshalJson( j2 ) ) {
        j["member"] = j2;
      }
    }
    if ( this->picture_ ) {
      json j2({});
      if ( this->picture_->marshalJson( j2 ) ) {
        j["picture"] = j2;
      }
    }
    
    return true;
  }
  /*
  bool User::marshalJson( json_t* node ) const
  {
    assert( node );

    json_t* user = node;
    //json_t* user = json_object();
    //json_object_set_new( node, "user", user );
    
    if ( this->hasUsername() ) {
      json_object_set_new( user, "name", json_stringn_nocheck( username_.str(), username_.size() ) );
    }

    if ( this->member_ ) {
      json_t* member = json_object();
      if ( this->member_->marshalJson( member ) ) {
        json_object_set_new( user, "member", member );
      }
    }
    
    if ( this->picture_ ) {
      json_t* picture = json_object();
      if ( this->picture_->marshalJson( picture ) ) {
        json_object_set_new( user, "picture", picture );
      }
    }
    
    return true;
  }
  */

  bool User::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.contains("name") ) {
      this->username_.str( j["name"] );
    }
    if ( j.contains("anchor") ) {
      this->syncAnchor_.str( j["anchor"] );
    }

    if ( j.contains("member") ) {
      json j2 = j["member"];
      if ( j2.is_object() ) {
        if (! this->member_ )
          this->member_ = shared_ptr<Member>(new Member());
        if (! this->member_->unmarshalJson( j2 )) {
          return false;
        }
      }
    }
    if ( j.contains("picture") ) {
      json j2 = j["picture"];
      if ( j2.is_object() ) {
        if (! this->picture_ )
          this->picture_ = shared_ptr<Picture>(new Picture());
        if (! this->picture_->unmarshalJson( j2 )) {
          return false;
        }
      }
    }
      
    return true;
  }
  /*
  bool User::unmarshalJson( json_t* node )
  {
    assert( node );
    {
      json_t* value;
      value = json_object_get(node, "name");
      if ( json_is_string(value) ) {
        this->username_.bytea( json_string_value(value), json_string_length(value) );
      }
      value = json_object_get(node, "anchor");
      if ( json_is_string(value) ) {
        this->syncAnchor_.bytea( json_string_value(value), json_string_length(value) );
      }
      
      value = json_object_get(node, "member");
      if ( value ) {
        if (! this->member_ )
          this->member_ = shared_ptr<Member>(new Member());
        member_->unmarshalJson( value );
      }
      value = json_object_get(node, "picture");
      if ( value ) {
        if (! this->picture_ )
          this->picture_ = shared_ptr<Picture>(new Picture());
        picture_->unmarshalJson( value );
      }
      
      return true;
    }
    return false;
  }
  */

  // XML Mapping
  
  // implements a simple state engine to handle entities for add, update and remove
  ISerializableEntity* User::entityByXmlNode( const char* name, int position ) 
  {
    if (strcmp(name, "member") == 0) {
      if (! this->member_) {
        this->member_ = shared_ptr<Member>(new Member());
      }
      return this->member_.get();
      
    } else if (strcmp(name, "picture") == 0) {
      if (! this->picture_) {
        this->picture_ = shared_ptr<Picture>(new Picture());
      }
      return this->picture_.get();
      
    }
    return this;
  }
  
  Value* User::valueByXmlNode( const char* name ) 
  {
    if (strcmp(name, "name") == 0) {
      return &this->username_;
    }
    if (strcmp(name, "anchor") == 0) { 
      return &this->syncAnchor_;
    }
    return NULL;
  }
  
  // generate core XML structure
  bool User::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const 
  {
    xml << ser::prolog("utf-8");
    xml << tag("user"); // start root tag
    
    if ( this->hasUsername() ) {
      xml << attr("name") << this->getUsername(); // 1. level attribute
    }
    
    // print out member entitiy
    if ( this->member_ ) {
      this->member_->marshalXml( xml, aesKey, keyLength );
    }

    // print out picture entitiy
    if ( this->picture_ ) {
      this->picture_->marshalXml( xml, aesKey, keyLength );
    }

    xml << endtag(); // end root tag
    
    return true;
  }
  
  // ==========
  // Debugging
  // ==========
  
  ostream& operator<<( ostream& ostr, const User& rhs ) {
    return ostr << rhs.toString();
  }

}
