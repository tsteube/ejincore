/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_USER_PROFILE_H__
#define __EJIN_USER_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"
#include "MemberRepository.h"
#include "PictureRepository.h"

namespace ejin
{
  class User;
  
  /**
   * Methods to resolve member entities by state.
   */
  class UserProfile {

    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    UserProfile( Database& db, MemberRepository& memberRepository, PictureRepository& pictureRepository ):
      db_( db ), memberRepository_(memberRepository), pictureRepository_(pictureRepository) { }
    // dtor
    virtual ~UserProfile( void ) { }
    
    /** Find all modified members. */
    list< shared_ptr<User> > findUpdated( void ) const
    throw(data_access_error);
    
    /** 
     * Find all conflicted member data sets. If the member instance was edited locally 
     * and on the server both data sets
     * are kept to resolve the conflict by user interaction.
     */
    list< string > findConflicts( void ) const
    throw(data_access_error);    

    /**
     * Load a single member entity
     */
    shared_ptr<User> loadByUsername( const string& name ) const
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    // --------
    // User Variables 
    
    MemberRepository&  memberRepository_;
    PictureRepository&  pictureRepository_;
    Database& db_;

  private:
  
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( UserProfile );
    
  };
  
}

#endif // __EJIN_USER_PROFILE_H__
