/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UserService.h"
#include "UsingEjinTypes.h"

#include "User.h"
#include "SyncSource.h"
#include "Modifications.h"
#include "Utilities.h"

namespace ejin
{
  
  inline void markEmpty( shared_ptr<User> m) {
    m->setEmpty( true );
  }
  
  // ==========
  // Public Interface
  // ==========
  
  const string UserService::getLock( void ) const
  {
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    assert (source->hasSyncAnchor());
    return source->getSyncAnchor();
  }
  
  void UserService::setLock( const string& syncAnchor, const string& username, const jtime& localTime )
  throw(data_access_error)
  {
    assert( ! syncAnchor.empty() );
    // increment overall sync anchor
    setSyncSource( syncAnchor, username, localTime );
  }
  
  unique_ptr<SyncSource> UserService::exportModifiedUser( list< shared_ptr<User> >& container )
  const throw(conflict_error, data_access_error) 
  {    
    assert( container.empty() );
    
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    // update in/out set
    container = this->findUpdated();
    
    return source;
  }
  
  list<string> UserService::importUpdatedUser( const Modifications<User>& container, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( ! container.getSyncAnchor().empty() );
    
    list< shared_ptr<User> > memberToUpdate;
    memberToUpdate.insert(memberToUpdate.begin(), container.getAdded().begin(), container.getAdded().end());
    memberToUpdate.insert(memberToUpdate.begin(), container.getUpdated().begin(), container.getUpdated().end());
    
    // mark to remove and add to container
    for_each (container.getRemoved().begin(), container.getRemoved().end(), markEmpty); 
    memberToUpdate.insert(memberToUpdate.begin(), container.getRemoved().begin(), container.getRemoved().end());
    
    list<string> conflicts;
    
    // now update members
    for (list< shared_ptr<User> >::iterator it=memberToUpdate.begin(); it != memberToUpdate.end(); it++) {
      User& m(**it);
      m.setSyncAnchor( container.getSyncAnchor() );
      this->update( m, false, syncTime );
      
      if ( m.hasConflict() )
      {
        conflicts.push_back( m.getUsername() );
      }
    }
    
    return conflicts;
  }
  
  void UserService::commitUser( const list< shared_ptr<User> >& members, const string& syncAnchor, const jtime& syncTime )
  throw(data_access_error) 
  {
    assert( ! syncAnchor.empty() );
    
    for (list< shared_ptr<User> >::const_iterator it=members.begin(); it != members.end(); it++) {
      this->commit( **it, syncAnchor, syncTime );
    }
    
  }
  
  // ==========
  // Helper methods
  // ==========
  
  unique_ptr<SyncSource> UserService::getSyncSource( void ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceMember));
    this->db_.refresh( *entity ); // throws data_access_error if not found
    return entity;
  }
  
  void UserService::setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceMember));
    this->db_.refresh( *entity );  // throws data_access_error if not found
    
    entity->setSyncAnchor(syncAnchor);
    entity->setModifyTime(localTime);
    entity->setUsername(username);
    this->db_.update( *entity );
  }
  
}
