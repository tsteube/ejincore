/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_USER_SERVICE_H__
#define __EJIN_USER_SERVICE_H__

#include "Declarations.h"
#include "UserRepository.h"

namespace ejin
{
  /**
   * The member service provides serialized views on the member repository. These methods will be used to load and update
   * the private list of member entities.
   */
  class UserService: public UserRepository {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    UserService( Database& db, MemberRepository& memberRepository, PictureRepository& pictureRepository ):
      UserRepository( db, memberRepository, pictureRepository ) {}
    // dtor
    ~UserService( void ) {};
    
    /** Returns the synchronization anchor of the last synchronization of members. */
    const string getLock( void ) const;
    
    /**
     * Updates the synchronization anchor of the list of visible members.
     */
    void setLock( const string& syncAnchor, const string& username, const jtime& localTime )
    throw(data_access_error);
    
    /**
     * Search for all recent changes of members since the last synchronization. The last remembered synchronization
     * anchor will be returned by this method.<br>
     * Note that there can't be any added or removed member here. Only modified members are valid. The add and remove
     * operations are not supported on the server.
     */
    unique_ptr<SyncSource> exportModifiedUser( list< shared_ptr<User> >& container ) const
    throw(conflict_error, data_access_error);
    
    /**
     * Commit all changes on the given member instances into the persistence store. If conflicts are detected this
     * method returns false.
     */
    list<string> importUpdatedUser( const Modifications<User>& container, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Commit all changes on the given member instances into the persistence store. This method will update the
     * synchronization anchor and global identifiers.
     */
    void commitUser( const list< shared_ptr<User> >& container, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    // ==========
    // Private Interface
    // ==========    
  private:
    
    /**
     * Return the overall media synchronization state.
     */
    unique_ptr<SyncSource> getSyncSource( void ) const
    throw(data_access_error);
    
    /**
     * Updates the overall media synchronization state.
     */ 
    void setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
    throw(data_access_error);
    
  };
  
}

#endif // __EJIN_USER_SERVICE_H__
