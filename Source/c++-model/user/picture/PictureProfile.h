/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PICTURE_PROFILE_H__
#define __EJIN_PICTURE_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

namespace ejin
{
  class Picture;
  
  /**
   * Methods to resolve member entities by state.
   */
  class PictureProfile {

    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    PictureProfile( Database& db ): db_( db ) { }
    // dtor
    virtual ~PictureProfile( void ) {}
    
    /**
     * Load a single picture entity
     */
    shared_ptr<Picture> loadPicture( const sqlite::Value& username, const sqlite::Value& hidden ) const
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    // --------
    // Picture Variables 
    
    Database& db_;

  private:
  
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( PictureProfile );
    
  };
  
}

#endif // __EJIN_MEMBER_PROFILE_H__
