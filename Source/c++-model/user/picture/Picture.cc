/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Picture.h"

#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  using ser::ISerializableEntity;
  
  // ==========
  // Schema DDL
  // ==========  
  
  template <> const sqlite::Attribute DataSchema<Picture>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("MEMBER_ID", type_int, flag_not_null),
    Attribute("MIME_TYPE", type_text, flag_none),
    Attribute("DATA", type_bytea, flag_none),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<Picture>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Picture::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findPictureById"] = "ID = ?";
      tMap["findPictureByUsername"] = "MEMBER_ID = ? AND (MASTER_ID IS NOT NULL) = ?";
      tMap["findPicturesByState"] = "MASTER_ID IS NULL AND SYNC_IND = ?";
      tMap["findConflictedPictures"] = "MASTER_ID IS NOT NULL";
      tMap["BackupSet"] = "MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findPictureByUsernameBak"] = "MEMBER_ID = ?";
      tMap["BackupSetBak"] = "MASTER_ID IS NULL";
    }
    return tMap;
  }
  const map<string,string> Picture::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["cleanupBak"] = "DELETE FROM mainBak.picture_tbl WHERE id NOT IN ( SELECT id FROM main.picture_tbl )";
    }
    return tMap;
  }
  const string Picture::restoreSet( void ) const {
    return "ID = ?1";
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Picture::Picture( void ): BaseEntity(TABLE_DDL, false), isEmpty_(false), imageBase64Cipher_()
  {
    this->setSyncInd( kSyncIndInsert );
    this->clearMasterId( );
    this->setModificationMask((integer) 0);
  }
  Picture::Picture( const integer id ): Picture()
  {
    setId( id );
  }
  Picture::Picture( const Picture& record ): Picture()
  {
    this->operator=(record);
  }
  Picture::~Picture( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Picture& Picture::operator=( const Picture& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, MemberId);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, MasterId);
      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      COPY_PROPERTY((*this), rhs, MimeType);
      COPY_PROPERTY((*this), rhs, Data);
      COPY_PROPERTY((*this), rhs, ModifiedBy);

      COPY_PROPERTY((*this), rhs, ModificationMask);
    }
    return *this;
  }
  
  // comparison
  bool Picture::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Picture& mrhs = dynamic_cast<const Picture&> (rhs);
    if (this->hasMemberId() && mrhs.hasMemberId())
      return (this->getMemberId() == mrhs.getMemberId());
    else if (this->hasId() && mrhs.hasId())
      return (this->getId() == mrhs.getId());
    return false;
  }
  bool Picture::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Picture& mrhs = dynamic_cast<const Picture&> (rhs); // throws std::bad_cast
    if (this->hasMemberId() && mrhs.hasMemberId())
      return (this->getMemberId() < mrhs.getMemberId());
    else if (this->hasId() && mrhs.hasId())
      return (this->getId() < mrhs.getId());
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  // Database schema
  const char* Picture::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& Picture::tableFields( void ) const { return TABLE_DDL; }
  
  // XML schema
  const string& Picture::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& Picture::xmlFields( void ) const { return XML_DDL; }
  
  bool Picture::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const Picture& mrhs = dynamic_cast<const Picture&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Data);
      COPY_PROPERTY((*this), mrhs, MimeType);
      COPY_PROPERTY((*this), mrhs, ModifyTime);
      COPY_PROPERTY((*this), mrhs, ModifiedBy);
      
      return true;
    }
    return false;
  }
  bool Picture::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    Picture& rhs( (Picture&)arg );
    
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        return false;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( this->getMemberId() != rhs.getMemberId() )
      return false;
    if ( this->getData() != rhs.getData() )
      return false;
    if ( this->getMimeType() != rhs.getMimeType() )
      return false;
    return true;
  }
  bool Picture::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void Picture::archive( void )
  {
  }
  void Picture::unarchive( void )
  {
  }
  void Picture::clearData( void )
  {
    setNull( 3 ); // data member variable
    this->clearMimeType( );
    this->clearModifyTime( );
    this->clearCreateTime( );
  }

  // ==========
  // Serialisation Interface
  // ==========
  
  // ======= JSON Mapping
  
  bool Picture::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasMimeType() ) {
      j["mime_type"] = string( this->value("MIME_TYPE")->str(), this->value("MIME_TYPE")->size() );
    }
    if (this->hasData()) {
      // encode base64 encode image
      vector<char> cipher ( Utilities::base64_encode( this->getData() ) );
      // temporary store in instance variable
      imageBase64Cipher_.assign( cipher.begin(), cipher.end() );
      j["data"] = string(imageBase64Cipher_.data(), imageBase64Cipher_.size());
    }
    
    return true;
  }
  /*
  bool Picture::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if (this->hasMimeType()) {
      json_object_set_new( node, "mime_type", json_stringn_nocheck( this->value("MIME_TYPE")->str(),
                                                                         this->value("MIME_TYPE")->size() ) );
    }
    if (this->hasData()) {
      // encode base64 encode image
      vector<char> cipher ( Utilities::base64_encode( this->getData() ) );
      // temporary store in instance variable
      imageBase64Cipher_.assign( cipher.begin(), cipher.end() );
      json_object_set_new( node, "data", json_stringn_nocheck( imageBase64Cipher_.data(), imageBase64Cipher_.size() ) );
    }
    
    return true;
  }
  */

  bool Picture::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }
      if ( j.contains("mime_type")) {
        this->value("MIME_TYPE")->str( j["mime_type"] );
      }
      if ( j.contains("data")) {
        json dat = j["data"];
        vector<char> data( Utilities::base64_decode( dat.get<string>() ) );
        this->setData( string(data.begin(), data.end()) );
      }
      return true;
    }
    return false;
  }
  /*
  bool Picture::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "mime_type");
    if ( json_is_string(value) ) {
      this->value("MIME_TYPE")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "data");
    if ( json_is_string(value) ) {
      // decode base64 encode image
      vector<char> data( Utilities::base64_decode( json_string_value(value) ) );
      this->setData( string(data.begin(), data.end()) );
    }
    
    return true;
  }
  */

  // ======= XML Mapping
  
  const string Picture::XML_NAME   = "picture";
  const ser::XmlAttribute Picture::XML_FIELDS[] =
  {
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute("mime_type", "MIME_TYPE" ),
    ser::XmlAttribute("data", "DATA" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet Picture::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  void Picture::endXmlNode( const char* name )
  {
    if ( strcmp( name, "picture" ) == 0 ) {
      if (this->hasData()) {
        // decode base64 encode image
        vector<char> data( Utilities::base64_decode( this->getData() ) );
        this->setData( string(data.begin(), data.end()) );
      }
    }
  }
  
  bool Picture::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("picture"); // start element tag
    
    // serialize properties
    if (this->hasMimeType()) {
      xml << tag("mime_type") << chardata() << this->getMimeType() << endtag();
    }
    if (this->hasData()) {
      // encode base64 encode image
      vector<char> cipher ( Utilities::base64_encode( this->getData() ) );
      xml << tag("data") << chardata() << cipher << endtag();
    }
    
    xml << ser::endtag(); // end element tag
    return true;
  }
  
}
