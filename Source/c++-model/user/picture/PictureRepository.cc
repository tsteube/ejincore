/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "PictureRepository.h"
#include "UsingEjinTypes.h"

#include "Member.h"
#include "Picture.h"
#include "MemberRepository.h"
#include "ChangeLogRepository.h"
#include "Utilities.h"
#include "KeySequence.h"
#include "RSACrypto.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  bool PictureRepository::detectConflict( const integer memberId ) const throw(data_access_error)
  {
    shared_ptr<Picture> picture = this->loadPicture( Value(memberId), Value(true) );
    return (bool)picture;
  }

  bool PictureRepository::clearConflict( const string& username ) throw(data_access_error)
  {
    assert ( ! username.empty() );
    
    shared_ptr<Member> member = memberRepository_.loadMemberInternal( Value(username.c_str()), Value(false) );
    return member ? clearConflict( member->getId() ) : false;
  }
  
  shared_ptr<Picture> PictureRepository::update( Picture& picture, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( picture.hasMemberId() );
    
    shared_ptr<Picture> pictureEntity = this->loadPicture( Value(picture.getMemberId()), Value(false) );
    
    // update algorithm
    unique_ptr<Picture> clone;
    if ( pictureEntity ) {
      if ( picture.isEmpty() )  {
        pictureEntity->setModifiedBy( picture.getModifiedBy() );
        bool result = removePicture( *pictureEntity );
        this->changeLogRepository_.remove( *pictureEntity, syncAnchor, syncTime );
        if ( result )
          pictureEntity.reset();
      } else {
        if ( updatePicture( *pictureEntity, picture ) ) {
          this->changeLogRepository_.update( *pictureEntity, syncAnchor, syncTime );
        }
      }
    } else {
      if (picture.isEmpty()) {
        pictureEntity.reset();
      } else {
        // add picture entity
        pictureEntity = addPicture( picture );
        if ( pictureEntity ) {
          this->changeLogRepository_.add( *pictureEntity, syncAnchor, syncTime );
        }
      }
    }
    
    return pictureEntity;
  }
  
  void PictureRepository::commit( Picture& picture, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( picture.hasId() );
    assert( ! syncAnchor.empty() );
    
    shared_ptr<Picture> pictureEntity = shared_ptr<Picture>(new Picture(picture.getId()));
    this->db_.refresh( *pictureEntity ); // throws DataRetrievalFailureException if not found
    
    pictureEntity->setModifyTime( syncTime );
    pictureEntity->setModifiedBy( picture.getModifiedBy() );
    
    SyncInd syncInd = (SyncInd) pictureEntity->getSyncInd();
    switch ( syncInd ) {
      case kSyncIndConflict:
        pictureEntity->setModificationMask(kMemberConflictedModification);
        this->db_.update( *pictureEntity );
        this->changeLogRepository_.update( *pictureEntity, syncAnchor, syncTime );
        break;
      case kSyncIndInsert:
        // entity has just been created
        pictureEntity->setCreateTime( syncTime );
      case kSyncIndUpdate:
        pictureEntity->setModificationMask( kNoneModification );
        pictureEntity->setSyncInd( kSyncIndSynchronous );
      case kSyncIndSynchronous:
        this->db_.update( *pictureEntity );
        if ( syncInd == kSyncIndInsert ) {
          this->changeLogRepository_.add( *pictureEntity, syncAnchor, syncTime );
        } else {
          this->changeLogRepository_.update( *pictureEntity, syncAnchor, syncTime );
        }
        break;
      case kSyncIndDelete:
        this->db_.remove( *pictureEntity );
        this->changeLogRepository_.remove( *pictureEntity, syncAnchor, syncTime );
        break;
      default:
        assert(false); // invalid state
    }
  }
  
  bool PictureRepository::revert( const string& username, bool force )
  throw(data_access_error)
  {
    assert( ! username.empty() );
    
    bool reverted = false;
    
    shared_ptr<Member> member = memberRepository_.loadMemberInternal( Value(username.c_str()), Value(false) );
    if (member) {
      shared_ptr<Picture> pictureEntity( this->loadPicture( Value(member->getId()), Value(false) ) );
      if ( pictureEntity != NULL )
      {
        // ensure that there a no pending conflicts
        if ( clearConflict( member->getId() ) ) {
          // reload after conflict has been cleared
          pictureEntity = this->loadPicture( Value(member->getId()), Value(false) );
        }
        
        switch ( pictureEntity->getSyncInd() ) {
          case kSyncIndInsert:
          {
            if (force) {
              db_.remove( *pictureEntity );
              reverted = true;
            } else {
              clonePicture( *pictureEntity, NULL );
              reverted = true;
            }
            break;
          }
          case kSyncIndSynchronous:
            break;
          case kSyncIndUpdate:
          case kSyncIndDelete:
          {
            // load entity from backup
            shared_ptr<Picture> pictureEntityBak( shared_ptr<Picture>(new Picture()) );
            pictureEntityBak->setId(pictureEntity->getId());
            db_.refresh(*pictureEntityBak, pictureEntityBak->backupSchemaName() );
            if ( force ) {
              pictureEntity->copyDataFrom( *pictureEntityBak );
              pictureEntity->setSyncInd( kSyncIndSynchronous );
              pictureEntity->setModificationMask( kNoneModification );
              this->db_.update( *pictureEntity );
              reverted = true;
            } else {
              reverted = clonePicture( *pictureEntity, pictureEntityBak.get() );
            }
            break;
          }
          case kSyncIndConflict:
            assert(false);
        }
      } else {
        // restore old picture if found in backup
        Value id(member->getId());
        unique_ptr<ResultSet> values( db_.find(Picture::TABLE_NAME, "findPictureByUsernameBak", &id, NULL) );
        vector< BaseEntity* >::iterator it = values->begin();
        if (it != values->end()) {
          Picture* picture = static_cast< Picture* >( *it );
          this->db_.insert( *picture );
          it = values->take( it );
          reverted = true;
        }
        
      }
      return reverted;
    }
    return false;
  }
  
  // ==========
  // Private Interface
  // ==========
  
  shared_ptr<Picture> PictureRepository::addPicture( Picture& picture )
  throw(data_access_error)
  {
    if ( picture.isEmpty() ) {
      return shared_ptr<Picture>();
    }
    
    assert( picture.hasMemberId() );
    // create new entity with unique global key
    shared_ptr<Picture> pictureEntity = shared_ptr<Picture>(new Picture( picture ));
    pictureEntity->clearId(); // generate new local database identifier
    pictureEntity->setSyncInd( kSyncIndSynchronous );
    pictureEntity->setModificationMask( kMemberRecentRemoteModification );
    
    // persist entity
    this->db_.insert( *pictureEntity );
    
    // set id
    picture.setId( pictureEntity->getId() );
    picture.setSyncInd( pictureEntity->getSyncInd() );
    
    return pictureEntity;
  }
  
  bool PictureRepository::updatePicture( Picture& pictureEntity, Picture& picture )
  throw(data_access_error)
  {
    assert( pictureEntity.hasId() );
    assert( ! pictureEntity.isHidden() );
    assert( picture.hasMemberId() );
    
    bool result = true;
    switch ( pictureEntity.getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( ! pictureEntity.hasEqualContent( picture ) ) {
          pictureEntity.setModificationMask(kMemberRecentRemoteModification);
          result = true;
        }
        pictureEntity.copyDataFrom( picture );
        this->db_.update( pictureEntity );
        break;
      case kSyncIndUpdate:
        if ( pictureEntity.hasEqualContent( picture ) )
        {
          // update only some dates and state data
          pictureEntity.copyDataFrom( picture );
          pictureEntity.setSyncInd( kSyncIndSynchronous );
          pictureEntity.setModificationMask( kNoneModification );
          this->db_.update( pictureEntity );
          result = true;
        }
        else
        {
          // conflict detected; save local entity for reference
          clonePicture( pictureEntity, &picture );
          result = true;
        }
        break;
      case kSyncIndConflict:
        shared_ptr<Picture> conflict( this->loadPicture( Value(pictureEntity.getMemberId()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", pictureEntity.getId());
        }
        if ( conflict->hasEqualContent( picture ) )
        {
          // resolve conflict
          switch ( conflict->getSyncInd() ) {
            case kSyncIndSynchronous:
            case kSyncIndConflict:
              _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", pictureEntity.getId());
            case kSyncIndUpdate:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // client and server are in sync now again
              // update only some dates and state data
              pictureEntity.setSyncInd( kSyncIndSynchronous );
              pictureEntity.setModificationMask( kNoneModification );
              pictureEntity.copyDataFrom( picture );
              this->db_.update( pictureEntity );
              result = true;
              break;
          }
        } else {
          // keep original local conflict data
          // update only some dates and state data
          pictureEntity.setSyncInd( kSyncIndConflict );
          pictureEntity.copyDataFrom( picture );
          this->db_.update( pictureEntity );
          result = true;
        }
        break;
    }
    
    // update id
    picture.setId( pictureEntity.getId() );
    
    return result;
  }
  
  bool PictureRepository::removePicture( Picture& pictureEntity )
  throw(data_access_error)
  {
    assert( pictureEntity.hasId() );
    assert( ! pictureEntity.isHidden() );
    
    bool result = false;
    switch ( pictureEntity.getSyncInd() ) {
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        clonePicture( pictureEntity, NULL );
        break;
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
        deletePicture( pictureEntity );
        result = true;
        break;
      case kSyncIndConflict:
        shared_ptr<Picture> conflict( this->loadPicture( Value(pictureEntity.getMemberId()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", pictureEntity.getId());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndConflict:
            _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", pictureEntity.getId());
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            // and mark entity as removed from server
            pictureEntity.clearData();
            pictureEntity.setSyncInd( kSyncIndConflict );
            this->db_.update( pictureEntity );
            result = true;
            break;
        }
        break;
    }
    return result;
  }
  
  void PictureRepository::deletePicture( Picture& pictureEntity )
  throw(data_access_error)
  {
    assert( pictureEntity.hasId() );
    
    // delete entity itself
    this->db_.remove(pictureEntity);
  }
  
  bool PictureRepository::clonePicture( Picture& pictureEntity, const Picture* picture )
  throw(data_access_error)
  {
    if ( picture == NULL || ! pictureEntity.hasEqualContent( *picture ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<Picture> clone = unique_ptr<Picture>(new Picture( pictureEntity ));
      clone->clearId( );
      clone->setMasterId( pictureEntity.getId() );
      clone->setModificationMask( clone->getModificationMask()|kMemberConflictedModification );
      
      // update only some dates and states
      if ( picture )
      {
        // update dates and states
        pictureEntity.copyDataFrom( *picture );
      }
      else
      {
        // mark entity as removed from server
        pictureEntity.clearData();
      }
      pictureEntity.setSyncInd( kSyncIndConflict );
      pictureEntity.setModificationMask( kMemberConflictedModification|kMemberRecentRemoteModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( pictureEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      
      return true;
    }
    return false;
  }
  
  bool PictureRepository::clearConflict( const integer memberId ) throw(data_access_error)
  {
    shared_ptr<Picture> pictureEntity = this->loadPicture( Value(memberId), Value(false) );
    shared_ptr<Picture> conflict = this->loadPicture( Value(memberId), Value(true) );
    
    bool cleared = false;
    if ( conflict ) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      if ( pictureEntity ) {
        if ( pictureEntity->hasContent() ) {
          pictureEntity->setSyncInd( kSyncIndSynchronous );
          pictureEntity->setModificationMask(kNoneModification);
          this->db_.update( *pictureEntity );
        } else {
          this->db_.remove( *pictureEntity );
        }
      }
      cleared = true;
    }
    return cleared;
  }

}
