/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PICTURE_ENTITY_H__
#define __EJIN_PICTURE_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class PictureProfile;
  
  /**
   * Entity Class for MEMBER_TBL
   */
  class Picture: public BaseEntity, public IBusinessData, public DataSchema<Picture> {
    friend class Database;
    friend class PictureProfile;
    friend class PictureRepository;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "PICTURE_TBL";
    
    // ctors
    Picture( void );
    Picture( const integer id );
    Picture( const Picture& record );
    // dtor
    ~Picture( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Picture& operator=( const Picture& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Picture(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void archive( void );
    void unarchive( void );
    void clearData( void );
    bool hasContent() const { return this->hasData(); }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );

    //bool unmarshalJson( json_t* node );
    //bool marshalJson( json_t* node ) const;
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    void endXmlNode( const char* name );
        
    // ==========
    // Crypto Interface
    // ==========
  public:
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( MemberId, 1 )
    ACCESS_STRING_PROPERTY_INTF( MimeType, 2 )
    const std::string getData() const { return asString(3); }
    void setData(const std::string& i) { asString(3, i); };
    bool hasData() const { return ! isNull(3); }

    ACCESS_TIME_PROPERTY_INTF( CreateTime, 4 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 5 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 6 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 7 )
    ACCESS_INTEGER_PROPERTY_INTF( ModificationMask, 8 )
    bool isHidden() { return this->getMasterId() > 0; }
    
    const string getModifiedBy( void ) const { return this->modifiedBy_.toString(); }
    void setModifiedBy( const string& modifiedBy ) { this->modifiedBy_.str(modifiedBy); }
    void clearModifiedBy( void ) { this->modifiedBy_.setNull(); }
    bool hasModifiedBy( void ) const { return ! this->modifiedBy_.isNull(); }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // Picture Variables
    
    bool isEmpty_;
    // transient property.
    sqlite::Value modifiedBy_;

    mutable vector<char> imageBase64Cipher_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
}

#endif //__EJIN_PICTURE_ENTITY_H__
