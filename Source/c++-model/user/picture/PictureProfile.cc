/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "PictureProfile.h"

#include "UsingEjinTypes.h"

#include "Picture.h"
#include "Utilities.h"

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
 
  shared_ptr<Picture> PictureProfile::loadPicture( const Value& memberId, const Value& hidden ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(Picture::TABLE_NAME, "findPictureByUsername", &memberId, &hidden, NULL) );
    
    shared_ptr<Picture> picture;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      // initialize found member
      picture = shared_ptr<Picture>(static_cast< Picture* >(*it));
      it = values->take(it);
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate picture [gid=%d].",  memberId.num());
    
    return picture;
  }
  
}
