/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_PICTURE_REPOSITORY_H__
#define __EJIN_PICTURE_REPOSITORY_H__

#include "PictureProfile.h"

namespace ejin
{
  class MemberRepository;
  
  /**
   * Update operations on the picture entity. There are a create, update and delete operation to manages pictures bound to a
   * channel instance.
   */
  class PictureRepository: public PictureProfile {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctor
    PictureRepository( Database& db, ChangeLogRepository& changeLogRepository, MemberRepository& memberRepository ):
    PictureProfile( db ), memberRepository_(memberRepository), changeLogRepository_(changeLogRepository) { }
    // dtor
    ~PictureRepository( void ) {};
    
    /**
     * Returns true if the specified picture instance has been synchronized with the server reference entity although the
     * entity contains unsynchronized local changes.
     */ 
    bool detectConflict( const integer memberId ) const
    throw(data_access_error);
    
    /**
     * Revert local changes to synchronize the media entity with the server reference.
     */ 
    bool clearConflict( const string& username )
    throw(data_access_error);
    
    /**
     * Update the picture data.
     */ 
    shared_ptr<Picture> update( Picture& picture, const string& syncAnchor, const jtime& syncTime ) 
    throw(data_access_error);
    
    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */ 
    void commit( Picture& picture, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Restore original picture instance
     * and mark local changes as conflicted data set.
     */
    bool revert( const string& gid, bool force = false )
    throw(data_access_error);
    
    /**
     * Return true if the public key of the given user should be revoked.
     */
    bool isRSAKeyRevoked( const string& username ) const
    throw(security_integrity_error,data_access_error);

    /**
     * Return the public key of the given user from the database
     */
    shared_ptr<RSACrypto> getPublicRSAKey( const string& username ) const
    throw(security_integrity_error,data_access_error);

    /**
     * Generate RSA key pair for the given user.
     * The public key is saved in the database while the private key gets returned.
     */
    shared_ptr<RSACrypto> generateRSAKey( const string& username, const void* seed = NULL , int num = 0 )
    throw(security_integrity_error,data_access_error);

    // ==========
    // Private Interface
    // ==========    
  private:
    
    ChangeLogRepository& changeLogRepository_;

    /**
     * Add a new picture entity into the persistent store
     */ 
    shared_ptr<Picture> addPicture( Picture& picture ) 
    throw(data_access_error);

    /**
     * Updates a picture entity in the persistent store
     */
    bool updatePicture( Picture& pictureEntity, Picture& picture )
    throw(data_access_error);    
    
    /*
     * Removes a picture entity from the persistent store
     */
    bool removePicture( Picture& pictureEntity )
    throw(data_access_error);
    
    /*
     * Delete picture with all dependent entities
     */
    void deletePicture( Picture& pictureEntity )
    throw(data_access_error);
        
    /*
     * Clone picture data to save local data on conflict
     */
    bool clonePicture( Picture& pictureEntity, const Picture* picture )
    throw(data_access_error);

    /*
     * Clear picture conflict.
     */
    bool clearConflict( const integer memberId )
    throw(data_access_error);

    // --------
    // Member Variables

    MemberRepository&  memberRepository_;

  };
  
}

#endif // __EJIN_PICTURE_REPOSITORY_H__
