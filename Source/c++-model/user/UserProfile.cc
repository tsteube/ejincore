/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "UserProfile.h"

#include "UsingEjinTypes.h"

#include "User.h"
#include "MemberProfileView.h"
#include "Member.h"
#include "Picture.h"
#include "Utilities.h"

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<User> > UserProfile::findUpdated( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(MemberProfileView::TABLE_NAME, "modifiedMembers", NULL) );
    list< shared_ptr<User> > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      MemberProfileView* memberProfile = static_cast< MemberProfileView* >( *it );
      shared_ptr<User> user( new User( memberProfile->getUsername() ));
      
      // member changes
      switch ( memberProfile->getMemberSyncInd() ) {
        case kSyncIndSynchronous:
          break;
        case kSyncIndUpdate:
        {
          // load member from database
          shared_ptr<Member> member( new Member(memberProfile->getMemberId()) );
          db_.refresh(*member); // throws DataRetrievalFailureException if not found
          user->setMember( member );
          break;
        }
        case kSyncIndInsert:
        case kSyncIndDelete:
        case kSyncIndConflict:
          assert( false ); // "invalid sync state"
          break;
      }

      // picture changes
      switch ( memberProfile->getPictureSyncInd() ) {
        case kSyncIndSynchronous:
          break;
        case kSyncIndInsert:
        case kSyncIndUpdate:
        {
          // load member from database
          shared_ptr<Picture> picture( new Picture( memberProfile->getPictureId()) );
          db_.refresh(*picture); // throws DataRetrievalFailureException if not found
          assert( memberProfile->getPictureSyncInd() );
          user->setPicture( picture );
          break;
        }
        case kSyncIndDelete:
        {
          shared_ptr<Picture> picture( new Picture( memberProfile->getPictureId()) );
          picture->setEmpty(true);
          user->setPicture( picture );
          break;
        }
        case kSyncIndConflict:
          assert( false ); // "invalid sync state"
          break;
      }

      result.push_back( user );
    }

    return result;
  }
  
  list< string > UserProfile::findConflicts( void ) const
  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values( this->db_.find(MemberProfileView::TABLE_NAME, "conflictedMembers", NULL) );
    list< string > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      MemberProfileView* memberProfile = static_cast< MemberProfileView* >( *it );
      result.push_back(memberProfile->getUsername());
    }
    return result;
  }
  
  shared_ptr<User> UserProfile::loadByUsername( const string& name ) const throw(data_access_error)
  {
    assert( ! name.empty() );
    
    Value gid( name.c_str() );
    Value hidden(false);
    unique_ptr<ResultSet> values( db_.find(Member::TABLE_NAME, "findMemberByUsername", &gid, &hidden, NULL) );
    shared_ptr<Member> member( shared_ptr<Member>(static_cast< Member* >( values->takeFirst() ) ) );
    if ( member ) {
      values = db_.find(Picture::TABLE_NAME, "findPictureByUsername", &gid, &hidden, NULL);
      shared_ptr<Picture> picture( shared_ptr<Picture>( static_cast< Picture* >( values->takeFirst() ) ) );
      return shared_ptr<User>(new User( name, member, picture ) );
    }
    return shared_ptr<User>();
  }

}
