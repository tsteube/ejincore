/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_APPROVED_MEMBER_ENTITY_H__
#define __EJIN_APPROVED_MEMBER_ENTITY_H__

#include "Declarations.h"
#include "IBusinessData.h"
#include "SqliteBaseEntity.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  
  /**
   * Entity Class for MEMBER_TBL
   */
  class ApprovedMember: public BaseEntity, public IBusinessData, public DataSchema<ApprovedMember> {
    friend class Database;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "APPROVED_MEMBER_TBL";
    
    // ctors
    ApprovedMember( void );
    ApprovedMember( const string& name );
    ApprovedMember( const integer memberId, const string& name );
    ApprovedMember( const ApprovedMember& record );
    // dtor
    ~ApprovedMember( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    ApprovedMember& operator=( const ApprovedMember& rhs );
    // clone
    BaseEntity* clone( void ) const { return new ApprovedMember(*this); }
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void clearData( void );
    void archive( void );
    void unarchive( void );
    bool hasContent() const { return false; }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool marshalJson( nlohmann::json& node ) const;
    bool unmarshalJson( const nlohmann::json& node );
    
    //bool marshalJson( json_t* node ) const;
    //bool unmarshalJson( json_t* node );
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    const integer getChannelId( void ) const { return 0; }
    bool hasChannelId( void ) const { return false; }
    
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( MemberId, 0 )
    ACCESS_STRING_PROPERTY_INTF( Username, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 2 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 3 )

    const std::string getModifiedBy( ) const { return getUsername(); }
    void setModifiedBy( const string& modifiedBy ) {  }
    void clearModifiedBy( void ) {  }
    bool hasModifiedBy( void ) const { return this->hasUsername(); }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
    // XML schema
    const string& xmlName( void ) const;
    const ser::XmlAttributeSet& xmlFields( void ) const;
    
    // ------
    // ApprovedMember Variables
    
    bool isEmpty_;

    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
  };
  
}

#endif //__EJIN_APPROVED_MEMBER_ENTITY_H__
