/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ApprovedMemberRepository.h"

#include "UsingEjinTypes.h"
#include "ChangeLogRepository.h"
#include "ApprovedMember.h"
#include "Utilities.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  ApprovedMember ApprovedMemberRepository::add( ApprovedMember& approvedMember, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert( approvedMember.hasMemberId() );
    assert( approvedMember.hasUsername() );
    
    // persist entity
    approvedMember.setSyncInd( kSyncIndSynchronous );
    this->db_.insert( approvedMember );
    this->changeLogRepository_.add( approvedMember, syncAnchor, syncTime );

    return approvedMember;
  }
  
  ApprovedMember& ApprovedMemberRepository::update( ApprovedMember& approvedMemberEntity, ApprovedMember& approvedMember, const string& syncAnchor, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( approvedMember.hasMemberId() );
    assert( approvedMember.hasUsername() );
    
    switch ( approvedMemberEntity.getSyncInd() ) {
      case kSyncIndInsert:
        approvedMemberEntity.setSyncInd( kSyncIndSynchronous );
        this->changeLogRepository_.update( approvedMemberEntity, syncAnchor, syncTime );
      case kSyncIndSynchronous:
        approvedMemberEntity.copyDataFrom( approvedMember );
        this->db_.update( approvedMemberEntity );
        break;
      case kSyncIndDelete:
        // no change on server side but removed on client
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        assert( false ); // "invalid sync state"
        break;
    }
    return approvedMemberEntity;
  }
  
  bool ApprovedMemberRepository::remove( ApprovedMember& approvedMemberEntity, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert ( approvedMemberEntity.hasMemberId() );
    assert ( approvedMemberEntity.hasUsername() );
    
    switch ( approvedMemberEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // normal case: added locally so the server doesn't know about the membership yet
        break;
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        this->db_.remove( approvedMemberEntity );
        this->changeLogRepository_.remove( approvedMemberEntity, syncAnchor, syncTime );
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        assert( false ); // "invalid sync state"
        break;
    }

    return true;
  }
  
  void ApprovedMemberRepository::commit( ApprovedMember& approvedMemberEntity, const string& syncAnchor, const jtime& syncTime ) throw(data_access_error)
  {
    assert ( approvedMemberEntity.hasMemberId() );
    assert ( approvedMemberEntity.hasUsername() );

    switch ( approvedMemberEntity.getSyncInd() )
    {
      case kSyncIndInsert:
        approvedMemberEntity.setSyncInd( kSyncIndSynchronous );
        this->changeLogRepository_.add( approvedMemberEntity, syncAnchor, syncTime );
      case kSyncIndSynchronous:
        this->db_.update( approvedMemberEntity );
        break;
      case kSyncIndDelete:
        // cleanup
        this->db_.remove( approvedMemberEntity );
        this->changeLogRepository_.remove( approvedMemberEntity, syncAnchor, syncTime );
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        assert( false ); // "invalid sync state"
        break;
    }
  }

  bool ApprovedMemberRepository::revertApprovals( integer memberId )
  throw(data_access_error)
  {
/*
 // restore old member if found in backup
 unique_ptr<ResultSet> values( db_.find(ChannelPost::TABLE_NAME, "findPostByGidBak", &gidValue, NULL) );
 vector< BaseEntity* >::iterator it = values->begin();
 if (it != values->end()) {
 this->db_.insert( **it );
 reverted = true;
 }

    // restore old member if found in backup
    shared_ptr<ApprovedMember> approvedMemberEntityBak = this->loadApprovedMemberInternal( pk1, pk2, true );
    if ( approvedMemberEntityBak ) {
      this->db_.insert( *approvedMemberEntityBak );
      reverted = true;
    }

    this.db.approvedMemberTable.deleteMember( memberId );
    this.db.approvedMemberTable.restoreMember( memberId );
 */
    return true;
  }

  bool ApprovedMemberRepository::revert( integer memberId, const string& username, bool force )
  throw(data_access_error)
  {
    assert( ! username.empty() );
    
    bool reverted = false;
    
    // call entity finder
    Value pk1( memberId );
    Value pk2( username.c_str() );
    shared_ptr<ApprovedMember> approvedMemberEntity = this->loadApprovedMemberInternal( pk1, pk2, false );
    if ( approvedMemberEntity ) {
      switch ( approvedMemberEntity->getSyncInd() ) {
        case kSyncIndInsert:
          if (force) {
            db_.remove( *approvedMemberEntity );
            reverted = true;
          }
          break;
        case kSyncIndSynchronous:
        case kSyncIndDelete:
        {
          shared_ptr<ApprovedMember> approvedMemberEntityBak = this->loadApprovedMemberInternal( pk1, pk2, true );
          if ( approvedMemberEntityBak && force ) {
            // force update entity with data from backup
            approvedMemberEntity->copyDataFrom( *approvedMemberEntityBak );
            approvedMemberEntity->setSyncInd( kSyncIndSynchronous );
            this->db_.update( *approvedMemberEntity );
            reverted = true;
          }
          break;
        }
        case kSyncIndUpdate:
        case kSyncIndConflict:
          assert( false ); // "invalid sync state"
          break;
      }
    } else {
      // restore old member if found in backup
      shared_ptr<ApprovedMember> approvedMemberEntityBak = this->loadApprovedMemberInternal( pk1, pk2, true );
      if ( approvedMemberEntityBak ) {
        this->db_.insert( *approvedMemberEntityBak );
        reverted = true;
      }
    }

    return reverted;
  }
  
  // private
  
  shared_ptr<ApprovedMember> ApprovedMemberRepository::loadApprovedMemberInternal( const Value& memberId,
                                                                                  const Value& username,
                                                                                  bool backup ) const
  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values;
    if ( backup )
      values = db_.find(ApprovedMember::TABLE_NAME, "findApprovedMemberByIdBak", &memberId, &username, NULL);
    else
      values = db_.find(ApprovedMember::TABLE_NAME, "findApprovedMemberById", &memberId, &username, NULL);
    
    shared_ptr<ApprovedMember> approvedMember;
    vector< BaseEntity* >::iterator it = values->begin();
    if (it != values->end()) {
      approvedMember = shared_ptr<ApprovedMember>(static_cast< ApprovedMember* >( *it ) );
      it = values->take( it );
    }
    
    if (values->size() > 0)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "duplicate approved member with username '%s' memberId %d.",  username.str(), memberId.num() );
    
    return approvedMember;
  }
  
}
