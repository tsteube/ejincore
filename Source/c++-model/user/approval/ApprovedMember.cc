/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ApprovedMember.h"

#include "UsingEjinTypes.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace ejin
{
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const sqlite::Attribute DataSchema<ApprovedMember>::TABLE_FIELDS[] =
  {
    Attribute("MEMBER_ID", type_int, flag_primary_key),
    Attribute("USERNAME", type_text, flag_primary_key),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute()
  };
  template <> const sqlite::AttributeSet DataSchema<ApprovedMember>::TABLE_DDL = sqlite::AttributeSet(const_cast<sqlite::Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> ApprovedMember::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findApprovedMemberById"] = "MEMBER_ID = ? AND USERNAME = ?";
      tMap["findApprovedMembersByMemberId"] = "MEMBER_ID = ?";
      tMap["findApprovedMembersByUsername"] = "USERNAME = ?";
      tMap["BackupSet"] = "";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findApprovedMemberByIdBak"] = "MEMBER_ID = ? AND USERNAME = ?";
      tMap["BackupSetBak"] = "";
    }
    return tMap;
  }
  const map<string,string> ApprovedMember::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["cleanupBak"] = "DELETE FROM mainBak.approved_member_tbl";
    }
    return tMap;
  }

  // ==========
  // Ctor / Dtor
  // ==========
  
  ApprovedMember::ApprovedMember( void ): BaseEntity(TABLE_DDL, false), isEmpty_(false)
  {
    this->setSyncInd( kSyncIndInsert );
  }
  ApprovedMember::ApprovedMember( const string& name ): ApprovedMember()
  {
    this->setUsername(name);
    this->setSyncInd( kSyncIndInsert );
  }
  ApprovedMember::ApprovedMember( const integer memberId, const string& name ): ApprovedMember()
  {
    this->setMemberId(memberId);
    this->setUsername(name);
    this->setSyncInd( kSyncIndInsert );
  }
  ApprovedMember::ApprovedMember( const ApprovedMember& record ): ApprovedMember()
  {
    this->operator=(record);
  }
  ApprovedMember::~ApprovedMember( void ) { }
  
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  ApprovedMember& ApprovedMember::operator=( const ApprovedMember& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, MemberId);
      COPY_PROPERTY((*this), rhs, Username);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      COPY_PROPERTY((*this), rhs, ModifiedBy);
    }
    return *this;
  }
  
  // comparison
  bool ApprovedMember::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    bool success = true;
    const ApprovedMember& mrhs = dynamic_cast<const ApprovedMember&> (rhs);
    if ( this->hasMemberId() && mrhs.hasMemberId() )
      success = success && (this->getMemberId() == mrhs.getMemberId());
    if (this->hasUsername() && mrhs.hasUsername())
      success = success && (this->getUsername() == mrhs.getUsername());
    return success;
  }
  bool ApprovedMember::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const ApprovedMember& mrhs = dynamic_cast<const ApprovedMember&> (rhs); // throws std::bad_cast
    if (this->hasMemberId() && mrhs.hasMemberId()) {
      if ( this->getMemberId() != mrhs.getMemberId() ) {
        return (this->getMemberId() < mrhs.getMemberId());
      }
    }
    if (this->hasUsername() && mrhs.hasUsername()) {
      if (this->getUsername() != mrhs.getUsername()) {
        return (this->getUsername() < mrhs.getUsername());
      }
    }
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  // Database schema
  const char* ApprovedMember::tableName( void ) const { return TABLE_NAME; }
  const sqlite::AttributeSet& ApprovedMember::tableFields( void ) const { return TABLE_DDL; }
  
  // XML schema
  const string& ApprovedMember::xmlName( void ) const { return XML_NAME; }
  const ser::XmlAttributeSet& ApprovedMember::xmlFields( void ) const { return XML_DDL; }
  
  bool ApprovedMember::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const ApprovedMember& mrhs = dynamic_cast<const ApprovedMember&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Username);
      COPY_PROPERTY((*this), mrhs, ModifyTime);
      COPY_PROPERTY((*this), mrhs, ModifiedBy);
      
      return true;
    }
    return false;
  }
  bool ApprovedMember::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    ApprovedMember& rhs( (ApprovedMember&)arg );
    if ( this->getUsername() != rhs.getUsername() )
      return false;
    return true;
  }
  bool ApprovedMember::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void ApprovedMember::archive( void )
  {
  }
  void ApprovedMember::unarchive( void )
  {
  }
  void ApprovedMember::clearData( void )
  {
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // ======= JSON Mapping
  
  bool ApprovedMember::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasUsername() ) {
      j["username"] = string( this->value("USERNAME")->str(), this->value("USERNAME")->size() );
    }
    return true;
  }
  /*
  bool ApprovedMember::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if (this->hasUsername()) {
      json_object_set_new( node, "username", json_stringn_nocheck( this->value("USERNAME")->str(),
                                                                   this->value("USERNAME")->size() ) );
    }
    return true;
  }
  */
  bool ApprovedMember::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }
      if ( j.contains("username")) {
        this->value("USERNAME")->str( j["username"] );
      }
      return true;
    }
    return false;
  }
  /*
  bool ApprovedMember::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value = json_object_get(node, "username");
    if ( json_is_string(value) ) {
      this->value("USERNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    return true;
  }
  */

  // ======= XML Mapping
  
  const string ApprovedMember::XML_NAME   = "approved";
  const ser::XmlAttribute ApprovedMember::XML_FIELDS[] =
  {
    ser::XmlAttribute("username", "USERNAME" ),
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet ApprovedMember::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  bool ApprovedMember::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("approved"); // start element tag
    xml << attr("username") << this->getUsername();
    xml << ser::endtag(); // end element tag
    return true;
  }
  
}
