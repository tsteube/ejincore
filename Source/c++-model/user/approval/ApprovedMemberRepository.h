/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_APPROVED_MEMBER_REPOSITORY_H__
#define __EJIN_APPROVED_MEMBER_REPOSITORY_H__

#include "Declarations.h"
#include "EjinDatabase.h"

namespace ejin
{
  class ApprovedMember;
  
  /**
   * Update operations on channel member entity. There are a create, update and delete operation used by the channel
   * repository to update membership.
   */
  class ApprovedMemberRepository {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    ApprovedMemberRepository( Database& db, ChangeLogRepository& changeLogRepository ):
      db_( db ), changeLogRepository_(changeLogRepository) { }
    ~ApprovedMemberRepository( void ) {};
    
    /** Adds a new channel member reference. Note that the concrete member data must be updated separately.*/
    ApprovedMember add( ApprovedMember& channelMember, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /** Update the given channel member entity with the reference data. */          
    ApprovedMember& update( ApprovedMember& channelMemberEntity, ApprovedMember& channelMember, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /** Removes a channel member reference. */ 
    bool remove( ApprovedMember& channelMemberEntity, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Commit the previous data update operation, i.e. cleanup temporary data.
     */ 
    void commit( ApprovedMember& channelMemberEntity, const string& syncAnchor, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Revert channel member approvals from backup
     */
    bool revertApprovals( integer memberId )
    throw(data_access_error);
    
    /**
     * Revert channel member from backup
     */
    bool revert( integer memberId, const string& username, bool force )
    throw(data_access_error);
    
  private:
    
    shared_ptr<ApprovedMember> loadApprovedMemberInternal( const sqlite::Value& memberId,
                                                          const sqlite::Value& username,
                                                          bool backup ) const
    throw(data_access_error);
    
    // --------
    // Member Variables
    
    Database& db_;
    
    ChangeLogRepository& changeLogRepository_;
    
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( ApprovedMemberRepository );
    

  };
  
}

#endif // __EJIN_APPROVED_MEMBER_REPOSITORY_H__
