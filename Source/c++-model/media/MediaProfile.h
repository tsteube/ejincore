/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEDIA_PROFILE_H__
#define __EJIN_MEDIA_PROFILE_H__

#include "Declarations.h"
#include "EjinDatabase.h"

class MediaRevertTest;

namespace ejin
{
  /**
   * Methods to resolve media entities by state.
   */
  class MediaProfile {
    friend MediaRevertTest;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    MediaProfile( Database& db );
    virtual ~MediaProfile( void ) { }
    
    /** Find all modified medias. */
    list< shared_ptr<Media> > findUpdated( void ) const 
    throw(data_access_error);
    
    /** 
     * Finds all conflicted media data sets. If the media instance was edited locally and on the server both data sets
     * are kept to resolve the conflict by user interaction.
     */
    list< shared_ptr<Media> > findConflicted( void ) const 
    throw(data_access_error);    
    
    /**
     * Finds all conflicted media data sets. If the media instance was edited locally and on the server both data sets
     * are kept to resolve the conflict by user interaction.
     */
    list< string > findConflicts( void ) const
    throw(data_access_error);
    
    /**
     * Find all attachment in the local repository that are marked as outdated;
     * i.e. the remote side was updated so ejin must download it again.
     */
    list< shared_ptr<Attachment> > findOutdatedAttachments( void ) const
    throw(data_access_error);
    
    /**
     * Find all attachment in the local repository that are marked as updated;
     * i.e. the local data has been changed and must be uploaded to the remote side.
     */
    list< shared_ptr<Attachment> > findUpdatedAttachments( void ) const
    throw(data_access_error);
    
    /**
     * Load binary attachment from the repository.
     * This method will also calculate the HMAC-SHA1 hash key.
     */
    bool loadAttachment( Attachment& attachment ) const
    throw(data_access_error);
    
    /**
     * Load a single media entity
     */
    shared_ptr<Media> loadByGid( const string& gid ) const
    throw(data_access_error);    
    
    /**
     * Returns the overall access role of the specified entity instance
     */
    MemberRole getAccessRole( integer mediaId, const char* username )
    const throw(data_access_error);

    /**
     * Full Text Search for media names.
     */
    list<integer> searchFullText( const string& pattern )
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    /**
     * Reload the list of entities from the database. <br>
     * Note: The profile selection with stored procedures just include the unique identifier of the database entity.
     * Therefore the complete entity data must be forced reloaded.
     */
    list< shared_ptr<Media> > refreshEntity( const list<integer>& list ) const 
    throw(data_access_error);
    
    /**
     * Load entity instance from persistence store.
     */ 
    shared_ptr<Media> loadMediaInternal( const sqlite::Value& gid, const sqlite::Value& hidden ) const;

    /**
     * Load entity instance from persistence store.
     */ 
    shared_ptr<ChannelMedia> loadChannelMediaInternal( const sqlite::Value& gid, const sqlite::Value& hidden ) const;
    
    /**
     * Load entity instance from persistence store.
     */
    shared_ptr<Attachment> loadAttachmentInternal( const sqlite::Value& mediaId, bool master ) const;

    // --------
    // Member Variables 
    
    Database& db_;
    
  private:
    
    DISALLOW_COPY_AND_ASSIGN_AND_COMPARE( MediaProfile );
    
  };
  
}

#endif // __EJIN_MEDIA_PROFILE_H__
