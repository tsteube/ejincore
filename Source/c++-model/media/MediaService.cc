/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaService.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "Media.h"
#include "SyncSource.h"
#include "Attachment.h"
#include "Modifications.h"
#include "SqliteValue.h"
#include "ChannelHeaderRepository.h"
#include "ChannelHeader.h"
#include "RSACrypto.h"

namespace ejin
{
  
  // ==========
  // Public Interface
  // ==========
  
  const string MediaService::getLock( void ) const
  {
    // resolve the current sync source
    unique_ptr<SyncSource> source( this->getSyncSource() );
    assert (source->hasSyncAnchor());
    return source->getSyncAnchor();
  }
  
  void MediaService::setLock( const string& syncAnchor, const string& username, const jtime& localTime )
  throw(data_access_error)
  {
    assert( ! syncAnchor.empty() );

    // increment overall sync anchor
    setSyncSource( syncAnchor, username, localTime );
  }
  
  unique_ptr<SyncSource> MediaService::exportModifiedMedia( list< shared_ptr<Media> >& container )
  const throw(conflict_error, data_access_error) 
  {    
    assert( container.empty() );
    
    unique_ptr<SyncSource> source( this->getSyncSource() );
    // update in/out set
    container = this->findUpdated();
    
    return source;
  }
  
  list<string> MediaService::importUpdatedMedia( const Modifications<Media>& container, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( ! container.getSyncAnchor().empty() );
    
    list< shared_ptr<Media> > mediaToUpdate;
    mediaToUpdate.insert(mediaToUpdate.end(), container.getAdded().begin(), container.getAdded().end());
    mediaToUpdate.insert(mediaToUpdate.end(), container.getUpdated().begin(), container.getUpdated().end());
    
    // mark to remove
    for_each (container.getRemoved().begin(), container.getRemoved().end(),
              []( shared_ptr<Media> m) { m->setEmpty( true ); });
    mediaToUpdate.insert(mediaToUpdate.end(), container.getRemoved().begin(), container.getRemoved().end());
    
    list<string> conflicts;
    
    // now update medias
    shared_ptr<Media> m;
    for (list< shared_ptr<Media> >::iterator it=mediaToUpdate.begin(); it != mediaToUpdate.end(); it++) {
      (*it)->setSyncAnchor( container.getSyncAnchor() );
      m = this->update( **it, syncTime );
      
      if ( m && m->getSyncInd() == kSyncIndConflict )
      {
        conflicts.push_back( m->getGid() );
      }
    }
    
    return conflicts;
  }
  
  void MediaService::commitMedia( const list< shared_ptr<Media> >& medias, const string& syncAnchor, const string& username, const jtime& syncTime )
  throw(data_access_error) 
  {
    assert( ! syncAnchor.empty() );
    
    for (list< shared_ptr<Media> >::const_iterator it=medias.begin(); it != medias.end(); it++) {
      this->commit( **it, syncAnchor, username, syncTime );
    }    
  }
  
  bool MediaService::clearConflicts( const list<string>& medias )
  throw(data_access_error)
  {
    bool success = true;
    for (list<string>::const_iterator it=medias.begin(); it != medias.end(); it++) {
      if (! this->clearConflict( *it )) {
        success = false;
      }
    }
    return success;
  }
  
  void MediaService::saveAttachments( list< shared_ptr<Attachment> >& attachments )
  throw(data_access_error)
  {
    for (list< shared_ptr<Attachment> >::iterator it=attachments.begin(); it != attachments.end(); it++) {
      this->saveAttachment( **it );
    }
  }
  
  bool MediaService::revert( const string& path, bool force )
  throw(data_access_error)
  {
    std::list<string> segments;
    Utilities::split(segments, path, "/");
    std::list<string>::iterator it=segments.begin();
    if ( it != segments.end() && (*it).empty() ) it++; // skip leading empty segment
    if ( it != segments.end() && strcasecmp((*it).c_str(), "media" ) == 0 && ++it != segments.end())
    {
      return this->MediaRepository::revert( *it, force );
    }
    return false;
  }

  string MediaService::getChannelGidOfMedia( const string& mediaGid ) const
  throw(data_access_error)
  {
    shared_ptr<Media> mediaEntity( this->loadByGid( mediaGid ));
    if (mediaEntity) {
      shared_ptr<ChannelHeader> header = this->channelHeaderRepository_.loadById( mediaEntity->getChannelId(), kNoResource );
      if (header) {
        return header->getChannelGid();
      }
    }
    return "";
  }
  
  bool MediaService::resolveSessionKeys(const list< shared_ptr<Attachment> >& attachments,
                                        const string& username,
                                        RSACrypto* crypto,
                                        bool required) const
  throw(data_access_error)
  {
    if ( attachments.empty() || username.empty() || ! crypto ) {
      return false;
    }
    
    bool result = true;
    for (list< shared_ptr<Attachment> >::const_iterator it = attachments.begin(); it != attachments.end(); it++) {
      assert ((*it)->getMedia());
      try {
        shared_ptr<RSACrypto> copy( shared_ptr<RSACrypto>( new RSACrypto(*crypto) ) );
        if ( this->channelHeaderRepository_.resolveSessionKey((*it)->getMedia()->getChannelId(), username, copy.get(), required) ) {
          (*it)->setCrypto( copy );
        } else {
          result = false;
        }
      } catch (ejin::security_integrity_error& e) {
        // mandatory AES key is missing - postphone download
      }
    }
    return result;
  }

  // ==========
  // Helper methods
  // ==========
  
  unique_ptr<SyncSource> MediaService::getSyncSource( void ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceMedia));
    this->db_.refresh( *entity ); // throws data_access_error if not found
    return entity;
  }
  
  void MediaService::setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
  throw(data_access_error)
  {
    unique_ptr<SyncSource> entity(new SyncSource(kSourceMedia));
    this->db_.refresh( *entity ); // throws data_access_error if not found
    entity->setSyncAnchor(syncAnchor);
    entity->setModifyTime(localTime);
    entity->setUsername(username);
    this->db_.update( *entity );
  }

  list< shared_ptr<Attachment> > MediaService::findAttachments( const Value& state ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet>  values( this->db_.find(Media::TABLE_NAME, "findMediaByAttachState", &state, NULL) );
    
    // convert set of unique identifiers to member entities
    list< shared_ptr<Attachment> > attachments;
    for (vector<BaseEntity*>::iterator it=values->begin(); it != values->end(); ) {
      shared_ptr<Media> media( shared_ptr<Media>( static_cast<Media*>(*it)) );
      attachments.push_back(shared_ptr<Attachment>(new Attachment(media)));
      it = values->take( it );
    }
    return attachments;
  }
  
}
