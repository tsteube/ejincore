/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_RESOURCE_ENTITY_H__
#define __EJIN_RESOURCE_ENTITY_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "IBusinessData.h"
#include "SqliteUtil.h"

namespace ejin
{
  // forward declaration
  class Database;
  class MediaProfile;
  class MediaService;
  class MediaProfileView;
  class RSACrypto;
  
  /**
   * Entity Class for MEDIA_TBL
   */
  class Resource: public BaseEntity, public IBusinessData {
    friend class Database;
    friend class MediaProfile;
    friend class MediaRepository;
    friend class MediaService;
    friend class MediaProfileView;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctors
    Resource( const sqlite::AttributeSet& field, bool readonly );
    Resource( const sqlite::AttributeSet& field, const string& gid );
    Resource( const Resource& record );
    // dtor
    ~Resource( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    virtual Resource& operator=( const Resource& rhs );
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    virtual bool hasEqualContent( const BaseEntity& rhs ) const;
    virtual bool copyDataFrom( const BaseEntity& rhs );
    virtual bool isDataEqualTo( const BaseEntity& rhs ) const;
    virtual void archive( void );
    virtual void unarchive( void );
    virtual void clearData( void );
    virtual bool hasContent() const { return false; }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    // ==========
    // Crypto Interface
    // ==========
  public:

    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_STRING_PROPERTY_INTF( Gid, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( No, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( ChannelId, 3 )
    ACCESS_INTEGER_PROPERTY_INTF( PostId, 4 )
    ACCESS_INTEGER_PROPERTY_INTF( CommentId, 5 )
    ACCESS_STRING_PROPERTY_INTF( Owner, 6 )
    ACCESS_STRING_PROPERTY_INTF( OwnerFullname, 7 )
    ACCESS_TIME_PROPERTY_INTF( CreateTime, 8 )
    ACCESS_TIME_PROPERTY_INTF( ModifyTime, 9 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 10 )
    ACCESS_INTEGER_PROPERTY_INTF( SyncInd, 11 )
    ACCESS_BOOLEAN_PROPERTY_INTF( AttachmentOutdated, 12 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 13 )
    ACCESS_INTEGER_PROPERTY_INTF( ModificationMask, 14 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Selected, 15 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedBy, 16 )
    ACCESS_STRING_PROPERTY_INTF( ModifiedByFullname, 17 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Secure, 18 )
    ACCESS_BOOLEAN_PROPERTY_INTF( Encrypted, 19 )
    ACCESS_STRING_PROPERTY_INTF( IV, 20 )
    bool isHidden() { return this->getMasterId() > 0; }

    integer getHierarchyModificationMask( void ) const { return this->getModificationMask( ); }
    void setHierarchyModificationMask( integer mask ) { this->setModificationMask( mask ); }

    // ==========
    // Private Interface
    // ==========
  protected:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // ------
    // Member Variables
    
    bool isEmpty_;
  };
  
}

#endif //__EJIN_RESOURCE_ENTITY_H__
