/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ChannelMediaRepository.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "Media.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"

namespace ejin
{
  // ==========
  // Public Interface
  // ==========
  
  ChannelMedia& ChannelMediaRepository::add( ChannelMedia& channelMedia ) 
  throw(data_access_error)
  {
    // create new entity with unique global key
    shared_ptr<ChannelMedia> channelMediaEntity = shared_ptr<ChannelMedia>(new ChannelMedia( channelMedia ));
    channelMediaEntity->clearId( ); // generate new local database identifier
    channelMediaEntity->setAttachmentOutdated( true );
    channelMediaEntity->setModificationMask( kNoneModification );

    // persist entity
    this->db_.insert( *channelMediaEntity );

    // update input data
    channelMedia.setSyncInd( channelMediaEntity->getSyncInd() );
    channelMedia.setAttachmentOutdated( channelMediaEntity->isAttachmentOutdated() );
    channelMedia.setId( channelMediaEntity->getId() );

    // cleanup
    clearDeleteOwnerInd( *channelMediaEntity );

    return channelMedia;
  }
  
  ChannelMedia& ChannelMediaRepository::update( ChannelMedia& channelMediaEntity, ChannelMedia& channelMedia ) 
  throw(data_access_error)
  {
    assert( channelMedia.hasGid() );
    assert( channelMediaEntity.hasId() );
    assert( ! channelMediaEntity.isHidden() );

    // ensure correct relationship to channel entities
    // Note: media entities synced via media list update only have the channelId reference
    channelMediaEntity.setChannelId( channelMedia.getChannelId() );
    if (channelMedia.hasPostId())
      channelMediaEntity.setPostId( channelMedia.getPostId() );
    else
      channelMediaEntity.clearPostId( );
    if (channelMedia.hasCommentId())
      channelMediaEntity.setCommentId( channelMedia.getCommentId() );
    else
      channelMediaEntity.clearCommentId( );
    switch ( channelMediaEntity.getSyncInd() )
    {
      case kSyncIndInsert:
        // must be unknown on the server
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of ChannelMedia[id=%d]", channelMediaEntity.getId());
      case kSyncIndSynchronous:
      case kSyncIndUpdate:
        // update only some dates and state data
        if ( ! channelMediaEntity.hasEqualContent( channelMedia ) ) {
          channelMediaEntity.setModificationMask( kMediaRecentRemoteModification );
        }
        channelMediaEntity.copyDataFrom( channelMedia );
        //channelMediaEntity.setModificationMask( kMediaConflictedModification );
        this->db_.update( channelMediaEntity );
        break;
      case kSyncIndDelete:
        if ( channelMediaEntity.hasEqualContent( channelMedia ) )
        {
          // noop
        }
        else
        {
          // conflict detected; save local entity for reference and update entity with remote changes
          cloneChannelMedia( channelMediaEntity, &channelMedia );
        }
        break;
      case kSyncIndConflict:
        shared_ptr<ChannelMedia> conflict( this->loadChannelMediaInternal( Value(channelMediaEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelMediaEntity.getId());
        }
        // keep original local conflict data
        // and update master with data from the server
        channelMediaEntity.copyDataFrom( channelMedia );
        channelMediaEntity.setSyncInd( kSyncIndConflict );
        this->db_.update( channelMediaEntity );
        break;
    }
    
    // update input data
    channelMedia.setId( channelMediaEntity.getId() );

    // cleanup
    clearDeleteOwnerInd( channelMediaEntity );

    return channelMediaEntity;
  }
  
  bool ChannelMediaRepository::remove( ChannelMedia& channelMediaEntity )
  throw(data_access_error)
  {
    assert( channelMediaEntity.hasId() );
    assert( ! channelMediaEntity.isHidden() );

    // update algorithm
    switch ( channelMediaEntity.getSyncInd() )
    {
      case kSyncIndInsert:
        return false; // should be added to the server in the next sync
      case kSyncIndUpdate:
        // conflict detected; save local entity for reference
        cloneChannelMedia( channelMediaEntity, NULL );
        break;
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        this->db_.remove( channelMediaEntity );
        break;        
      case kSyncIndConflict:
        shared_ptr<ChannelMedia> conflict( this->loadChannelMediaInternal( Value(channelMediaEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[channelId=%d]", channelMediaEntity.getId());
        }
        // keep original local conflict data
        // and update master with data from the server
        channelMediaEntity.clearData( );
        channelMediaEntity.setSyncInd( kSyncIndConflict );
        this->db_.update( channelMediaEntity );
        break;
    }
    
    // cleanup
    clearDeleteOwnerInd( channelMediaEntity );
    
    return true;
  }
  
  void ChannelMediaRepository::commit( ChannelMedia& channelMediaEntity, const ChannelMedia* channelMedia, const string& syncAnchor, const string& username, const jtime& syncTime )
  throw(data_access_error)
  {
    assert( channelMediaEntity.hasId() );
    assert( ! syncAnchor.empty() );
    
    // update algorithm
    switch ( channelMediaEntity.getSyncInd() )
    {
      case kSyncIndSynchronous:
      case kSyncIndConflict:
        break;
      case kSyncIndInsert:
        // update global identifier
        assert( channelMedia->hasGid() );
        channelMediaEntity.setSecure( channelMedia->isSecure() );
        channelMediaEntity.setGid( channelMedia->getGid() );
        //channelMediaEntity.setModificationMask( kNoneModification );
        channelMediaEntity.setModifiedBy(username);
        this->db_.update( channelMediaEntity );
        break;
      case kSyncIndUpdate:
        //channelMediaEntity.setModificationMask( kNoneModification );
        channelMediaEntity.setModifiedBy(username);
        this->db_.update( channelMediaEntity );
        break;
      case kSyncIndDelete:
        assert( channelMediaEntity.hasGid() );
        shared_ptr<Media> mediaEntity( loadByGid( channelMediaEntity.getGid() ) );
        assert( mediaEntity );
        deleteMedia( *mediaEntity );
        break;
    }
  } 
  
  // ==========
  // Protected Interface
  // ==========    
  
  void ChannelMediaRepository::cloneChannelMedia( ChannelMedia& channelMediaEntity, const ChannelMedia* channelMedia )
  throw(data_access_error)
  {
    // update only some dates and states
    if ( channelMedia )
    {
      // update dates and states
      channelMediaEntity.copyDataFrom( *channelMedia );
    }
    else
    {
      // mark entity as removed from server
      channelMediaEntity.clearData();
    }
    channelMediaEntity.setSyncInd( kSyncIndConflict );
    channelMediaEntity.setModificationMask( kMediaConflictedModification|kMediaRecentRemoteModification );

    // derive clone from original data set
    shared_ptr<Media> mediaEntity( this->loadByGid( channelMediaEntity.getGid() ));
    assert( mediaEntity );
    unique_ptr<Media> clone = unique_ptr<Media>(new Media( *mediaEntity ) );
    clone->clearId( );
    clone->setMasterId( channelMediaEntity.getId() );
    clone->setModificationMask( clone->getModificationMask()|kMediaConflictedModification );

    // first we need to change the local state of the master entity
    this->db_.update( channelMediaEntity );    
    // before we can insert the clone to avoid a unique constraint violation
    this->db_.insert( *clone );
    // move attachment to cloned entity
    this->copyAttachment( channelMediaEntity.getId(), clone->getId() );
  }
  
  void ChannelMediaRepository::clearDeleteOwnerInd( ChannelMedia& channelMediaEntity )
  throw(data_access_error)
  {
    // reset delete marker of owner entites
    const Value channelId(channelMediaEntity.getChannelId());
    this->db_.valueOf(ChannelHeader::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
    if ( channelMediaEntity.hasPostId() ) {
      const Value postId(channelMediaEntity.getPostId());
      this->db_.valueOf(ChannelPost::TABLE_NAME, "clearDeleteSyncInd", &postId, NULL);
    }
    if ( channelMediaEntity.hasCommentId() ) {
      const Value commentId(channelMediaEntity.getCommentId());
      this->db_.valueOf(ChannelComment::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
    }
  }

}
