/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEDIA_REPOSITORY_H__
#define __EJIN_MEDIA_REPOSITORY_H__

#include "MediaProfile.h"

namespace ejin
{

  /**
   * Update operation on the media entity. There are a create, update and delete method to manages resources bound to a
   * channel, post or comment instance.
   */
  class MediaRepository: public MediaProfile {

    // ==========
    // Public Interface
    // ==========
  public:

    // ctor
    MediaRepository( Database& db, ChangeLogRepository& changeLogRepository ) :
      MediaProfile( db ), changeLogRepository_(changeLogRepository) { }
    // dtor
    ~MediaRepository( void ) { };

    /**
     * Returns true if the specified media instance has been synchronized with the server reference entity although the
     * entity contains unsynchronized local changes.
     */
    bool detectConflict( const string& mediaGid ) const
    throw(data_access_error);

    /**
     * Revert local changes to synchronize the media entity with the server reference.
     */
    bool clearConflict( const string& mediaGid )
    throw(data_access_error);

    /**
     * This method will update the media data itself.
     */
    shared_ptr<Media> update( Media& media, const jtime& syncTime ) throw(data_access_error);

    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */
    shared_ptr<Media> commit( Media& media, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);

    /**
     * Save binary attachment in the repository.
     * This method will also calculate the HMAC-SHA1 hash key of the given data
     * and compares the hash value with the information of the attachment {@link Media} instance.
     */
    Attachment& saveAttachment( Attachment& attachment )
    throw(data_access_error);

    /**
     * Remove binary attachment in the repository.
     */
    bool deleteAttachment( const Media& media )
    throw(data_access_error);

    /**
     * Move attachment from Media from to attachment of media to if available.
     */
    bool moveAttachment( integer from, integer to )
    throw(data_access_error);

    /**
     * Copy attachment from Media from to attachment of media to if available.
     */
    bool copyAttachment( integer from, integer to )
    throw(data_access_error);
    
    /**
     * Restore original media instance
     * and mark local changes as conflicted data set.
     */
    bool revert( const string& gid, bool force = false )
    throw(data_access_error);
    
    /**
     * Revert all new local media instances of the given channel identifier.
     */
    bool revertNewMedias( integer channelId, integer postId, integer commentId, bool force )
    throw(data_access_error);

    // ==========
    // Private Interface
    // ==========
  protected:

    ChangeLogRepository& changeLogRepository_;

    /*
     * Revert media attachment from backup
     */
    bool revertAttachment( const Media& media )
    throw(data_access_error);
    
    /*
     * Add a new media entity to the persistent store
     */
    shared_ptr<Media> addMedia( Media& media )
    throw(data_access_error);

    /*
     * Updates a media entity in the persistent store
     */
    bool updateMedia( Media& mediaEntity, Media& media, const KeySequence& syncAnchor )
    throw(data_access_error);

    /*
     * Removes a media entity from the persistent store
     */
    bool removeMedia( Media& mediaEntity, const KeySequence& syncAnchor )
    throw(data_access_error);

    /*
     * Delete media with all dependent entities
     */
    void deleteMedia( Media& mediaEntity )
    throw(data_access_error);

    /**
     * Delete all dependent files of attachment.
     * All files started with the media attachment prefix.
     */
    bool clearMediaCache( const Media& media )
    throw(data_access_error);

    /*
     * Clone media data to save local data on conflict
     */
    bool cloneMedia( Media& mediaEntity, const Media* media, const KeySequence* syncAnchor )
    throw(data_access_error);

    /*
     * Reset delete marker on owner entites
     */
    void clearDeleteOwnerInd( Media& mediaEntity )
    throw(data_access_error);
    
    // --------
    // Member Variables 
       
  };

}

#endif // __EJIN_MEMBER_REPOSITORY_H__
