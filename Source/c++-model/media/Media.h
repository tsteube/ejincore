/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEDIA_ENTITY_H__
#define __EJIN_MEDIA_ENTITY_H__

#include "Declarations.h"
#include "SqliteBaseEntity.h"
#include "SqliteValue.h"
#include "IBusinessData.h"
#include "Resource.h"

namespace ejin
{
  /**
   * Entity Class for MEDIA_TBL
   */
  class AbstractMedia: public Resource {
    friend class Database;
    friend class MediaProfile;
    friend class MediaRepository;
    friend class MediaService;
    friend class MediaProfileView;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctors
    AbstractMedia( const sqlite::AttributeSet& field, bool readonly );
    AbstractMedia( const sqlite::AttributeSet& field, const string& gid );
    AbstractMedia( const AbstractMedia& record );
    // dtor
    ~AbstractMedia( void );
    
    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    AbstractMedia& operator=( const AbstractMedia& rhs );
    
    // ==========
    // IBusinessData Interface
    // ==========
  public:
    
    bool hasEqualContent( const BaseEntity& rhs ) const;
    bool copyDataFrom( const BaseEntity& rhs );
    bool isDataEqualTo( const BaseEntity& rhs ) const;
    void clearData( void );
    void archive( void );
    void unarchive( void );
    bool hasContent() const { return false; }
    bool isEmpty( void ) const { return this->isEmpty_; };
    void setEmpty( bool empty ) { this->isEmpty_ = empty; }
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    
    bool unmarshalJson( const nlohmann::json& node );
    bool marshalJson( nlohmann::json& j ) const;

    //bool unmarshalJson( json_t* node );
    //bool marshalJson( json_t* node ) const;
    
    bool marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const;
    sqlite::Value* valueByXmlNode( const char* name );
    void endXmlNode( const char* name );
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_INTEGER_PROPERTY_INTF( Role, 21 )
    ACCESS_STRING_PROPERTY_INTF( MimeType, 22 )
    ACCESS_INTEGER_PROPERTY_INTF( Size, 23 )
    ACCESS_STRING_PROPERTY_INTF( TransferKey, 24 )
    ACCESS_STRING_PROPERTY_INTF( Hmac, 25 )
    ACCESS_STRING_PROPERTY_INTF( Name, 26 )
    ACCESS_BOOLEAN_PROPERTY_INTF( AttachmentUpdate, 27 )

    // ==========
    // Metadata
    // ==========
    
    const string getChannelGid( void ) const { return this->channelGid_.toString(); }
    void setChannelGid( const string& gid ) { this->channelGid_.str( gid ); }
    void clearChannelGid( void ) { this->channelGid_.setNull(); }
    bool hasChannelGid( void ) const { return ! this->channelGid_.isNull(); }
    
    MemberRole getAccessRole( void ) const { return this->accessRole_; }
    void setAccessRole( MemberRole role ) { this->accessRole_ = role; }
    
    // ==========
    // Private Interface
    // ==========
  protected:
    
    // comparision
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // SQL select statements
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    const string restoreSet( void ) const;
    
    // XML schema
    const string& xmlName( void ) const { return XML_NAME; }
    const ser::XmlAttributeSet& xmlFields( void ) const { return XML_DDL; }
    
    // ------
    // Member Variables
    
    sqlite::Value channelGid_;
    MemberRole accessRole_;
    
    // ------
    // Static Variables
    
    // XML schema
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
  };
  
  class Media: public AbstractMedia, DataSchema<Media> {
    friend class Database;
    friend class MediaProfile;
    friend class MediaRepository;
    friend class MediaService;
    friend class ChannelCommentProfile;
    friend class ChannelPostProfile;
    friend class ChannelHeaderProfile;
    friend class ChannelRepository;
    friend class ChannelService;
    friend class ChannelProfile;

    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "MEDIA_TBL";

    // ctors
    Media( void );
    Media( const string& gid );
    Media( const Media& record );
    Media( const MediaProfileView& record );
    
    // clone
    BaseEntity* clone( void ) const { return new Media(*this); }
    
    bool copyDataFrom( const BaseEntity& rhs );

  private:
    // Database schema
    const char* tableName( void ) const;
    const sqlite::AttributeSet& tableFields( void ) const;
    
  };
  
  class MediaProfileView: public AbstractMedia, DataSchema<MediaProfileView> {
    friend class Database;
    friend class MediaProfile;
    friend class MediaRepository;
    friend class MediaService;
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "MEDIA_PROFILE_VIEW";

    // ctors
    MediaProfileView( void );
    MediaProfileView( const string& gid );
    MediaProfileView( const MediaProfileView& record );
    
    // clone
    BaseEntity* clone( void ) const { return new MediaProfileView(*this); }
    
  public:
    
    // ==========
    // Getter / Setter
    // ==========
    ACCESS_STRING_PROPERTY_INTF( Gid, 26 )
    ACCESS_INTEGER_PROPERTY_INTF( Role, 27 )
    ACCESS_INTEGER_PROPERTY_INTF( Membership, 28 )
    ACCESS_STRING_PROPERTY_INTF( SyncAnchor, 29 )
    
  private:
    // Database schema
    const char* tableName( void ) const;
    const char* tableMainSchema( void ) const { return MAIN_DATABASE_NAME; }
    const char* tableBackupSchema( void ) { return BACKUP_DATABASE_NAME; }
    const char* tableArchiveSchema( void ) const { return ARCHIVE_DATABASE_NAME; }
    const sqlite::AttributeSet& tableFields( void ) const;
    
    const map<string,string> finderTemplates( const char* schema ) const;
    
  };
  
}

#endif //__EJIN_MEDIA_ENTITY_H__
