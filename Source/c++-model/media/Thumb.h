/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_THUMB_H__
#define __EJIN_THUMB_H__

#include "Attachment.h"
#include "ICryptoEntity.h"
#include "SqliteBaseEntity.h"

using sqlite::BaseEntity;

namespace ejin
{
  /**
   * ejin file attachment entity
   */
  class Thumb: public sqlite::BaseEntity, public DataSchema<Thumb> {
    
    friend ostream& operator<<(ostream& ostr, const Thumb& rhs);
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "THUMB_TBL";

    // ctor
    Thumb( void );
    Thumb( const char* data, size_t length );
    Thumb( const Thumb& record );
    // dtor
    ~Thumb( void );
    
    // ==========
    // Finder Interface
    // ==========
    const map<string,string> finderTemplates( const char* schema ) const;
    
    void archive( void );
    void unarchive( void );

    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Thumb& operator=( const Thumb& rhs );
    bool operator<( const Thumb& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Thumb(*this); }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( AttachmentId, 1 )
    ACCESS_BYTEA_PROPERTY_INTF( Data, 2, 3 )
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // schema DDL
    const char* tableName( void ) const;
    const char* mainSchemaName( void ) const { return ATTACHMENT_DATABASE_NAME; }
    const char* backupSchemaName( void ) const { return NULL; }
    const char* archiveSchemaName( void ) const { return ATTACHMENT_ARCHIVE_DATABASE_NAME; }
    const sqlite::AttributeSet& tableFields( void ) const;
    bool useSequenceTable() const { return false; }    
  };
  
  // ----------
  // global methods
  
  // comparision
  inline bool operator!=(const Thumb& lhs, const Thumb& rhs) { return !operator==(lhs,rhs); }
  inline bool operator> (const Thumb& lhs, const Thumb& rhs) { return  operator< (rhs,lhs); }
  inline bool operator<=(const Thumb& lhs, const Thumb& rhs) { return !operator> (lhs,rhs); }
  inline bool operator>=(const Thumb& lhs, const Thumb& rhs) { return !operator< (lhs,rhs); }
  
  // debugging
  ostream& operator<<( ostream& ostr, const Thumb& rhs );
  
}

#endif // __EJIN_THUMB_H__
