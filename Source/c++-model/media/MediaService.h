/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_MEDIA_SERVICE_H__
#define __EJIN_MEDIA_SERVICE_H__

#include "MediaRepository.h"

namespace ejin
{
  
  /**
   * The resource service provides serialized views on the resource repository. These methods will be used to read and
   * update the private list of resource entities.
   */
  class MediaService: public MediaRepository {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
    MediaService( Database& db, ChangeLogRepository& changeLogRepository, const ChannelHeaderRepository& channelHeaderRepository ):
      MediaRepository(db, changeLogRepository), channelHeaderRepository_(channelHeaderRepository) { }
    ~MediaService(void) {};
    
    /** Returns the synchronization anchor of the last synchronization of medias. */
    const string getLock( void ) const;
    
    /**
     * Updates the synchronization anchor of the list of visible medias.
     */
    void setLock( const string& syncAnchor, const string& username, const jtime& localTime )
    throw(data_access_error);
    
    /**
     * Search for all recent changes of medias since the last synchronization. The last remediaed synchronization
     * anchor will be returned by this method.
     */
    unique_ptr<SyncSource> exportModifiedMedia( list< shared_ptr<Media> >& container ) const
    throw(conflict_error, data_access_error);
    
    /**
     * Updates all changes on the given media instances into the persistence store. If conflicts are detected this
     * method returns false.
     */
    list<string> importUpdatedMedia( const Modifications<Media>& container, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Revert local changes to synchronize the media entity with the server reference.
     */
    bool clearConflicts( const list<string>& medias )
    throw(data_access_error);
    
    /**
     * Commit all changes on the given media instances into the persistence store. This method will update the
     * synchronization anchor and global identifiers.
     */
    void commitMedia( const list< shared_ptr<Media> >& container, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);
    
    /**
     * Save the given attachment as file in the local repostory.
     */
    void saveAttachments( list< shared_ptr<Attachment> >& attachments )
    throw(data_access_error);
    
    /**
     * Revert entity according to the specified target channel entity path; separated by '/'
     * @param path Media entity path Media/{gid}
     * @param force if true all local changes gets reverted completely otherwise the data set is marked as conflicted
     */
    bool revert( const string& path, bool force = false )
    throw(data_access_error);
    
    /**
     * Resolve session key of all given attachments
     */
    bool resolveSessionKeys(const list< shared_ptr<Attachment> >& attachments,
                            const string& username,
                            RSACrypto* crypto,
                            bool required = true) const throw(data_access_error);

    /**
     * Maps the specified media to the channel global identifier.
     */
    string getChannelGidOfMedia( const string& mediaGid ) const
    throw(data_access_error);

    // ==========
    // Private Interface
    // ==========    
  private:
    
    const ChannelHeaderRepository& channelHeaderRepository_;

    /*
     * Return the overall media synchronization state.
     */
    unique_ptr<SyncSource> getSyncSource( void ) const
    throw(data_access_error);
    
    /*
     * Updates the overall media synchronization state.
     */ 
    void setSyncSource( const string& syncAnchor, const string& username, const jtime& localTime ) const
    throw(data_access_error);    
    
    /*
     * Filter on medias to find attachments
     */
    list< shared_ptr<Attachment> > findAttachments( const sqlite::Value& state ) const 
    throw(data_access_error);
    
  };
  
}

#endif // __EJIN_MEDIA_SERVICE_H__
