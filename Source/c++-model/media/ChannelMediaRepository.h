/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CHANNEL_MEDIA_REPOSITORY_H__
#define __EJIN_CHANNEL_MEDIA_REPOSITORY_H__

#include "MediaRepository.h"

class AbstractChannelTestFixture;
class AbstractMediaTestFixture;

namespace ejin
{
  
  /**
   * Update operations on channel media entity. There are a create, update and delete operation used by the channel
   * repository to update mediaship.
   */
  class ChannelMediaRepository: public MediaRepository {
    friend AbstractChannelTestFixture;
    friend AbstractMediaTestFixture;
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // constructors
   ChannelMediaRepository( Database& db, ChangeLogRepository& changeLogRepository ):
      MediaRepository( db, changeLogRepository ) {}
    ~ChannelMediaRepository( void ) { };
    
    /** Adds a new channel media reference. Note that the concrete media data must be updated separately.*/
    ChannelMedia& add( ChannelMedia& media )
    throw(data_access_error);
    
    /** Update the given channel media entity with the reference data. */          
    ChannelMedia& update( ChannelMedia& entity, ChannelMedia& media )
    throw(data_access_error);
    
    /** Removes a channel media reference. */ 
    bool remove( ChannelMedia& entity )
    throw(data_access_error);
    
    /**
     * Commit the previous data update operation by setting the global identifier generated on the server side and the
     * current synchronization anchor provided by the server.
     */      
    void commit( ChannelMedia& mediaEntity, const ChannelMedia* media, const string& syncAnchor, const string& username, const jtime& syncTime )
    throw(data_access_error);
    
    // ==========
    // Protected Interface
    // ==========    
  protected:
    
    /**
     * Execute update operation of media reference entities. This method also handles the conflict handling.
     */ 
    shared_ptr<ChannelMedia> updateChannelMediaInternal( SyncOperation op, ChannelMedia* entity, ChannelMedia* media )
    throw(data_access_error);
    
    /*
     * Clone channel media data to save local data on conflict
     */
    void cloneChannelMedia( ChannelMedia& channelMediaEntity, const ChannelMedia* channelMedia )
    throw(data_access_error);
    
    /*
     * Reset delete marker on owner entites
     */
    void clearDeleteOwnerInd( ChannelMedia& channelMediaEntity )
    throw(data_access_error);

  };
  
}

#endif // __EJIN_CHANNEL_MEDIA_REPOSITORY_H__
