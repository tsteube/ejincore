/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Resource.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "SqliteValue.h"

using ser::tag;
using ser::attr;
using ser::chardata;
using ser::endtag;


namespace ejin
{
  // ==========
  // Ctor / Dtor
  // ==========
  
  Resource::Resource( const sqlite::AttributeSet& field, bool readonly ): BaseEntity(field, readonly), isEmpty_(false)
  {
    this->setSyncInd( kSyncIndInsert );
    this->setAttachmentOutdated( false );
    this->clearMasterId( );
    this->setSelected( false );
    this->setSecure( false );
    this->setEncrypted( false );
    this->setModificationMask((integer) 0);
  }
  Resource::Resource( const AttributeSet& field, const string& gid ): Resource(field, false)
  {
    setGid(gid);
  }
  Resource::Resource( const Resource& record ): BaseEntity(record), isEmpty_(false)
  {
    this->operator=(record);
  }
  Resource::~Resource( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Resource& Resource::operator=( const Resource& rhs ) {
    if (this != &rhs) {
      BaseEntity::operator=(rhs);
      
      COPY_PROPERTY((*this), rhs, Id);
      COPY_PROPERTY((*this), rhs, Gid);
      COPY_PROPERTY((*this), rhs, No);
      
      COPY_PROPERTY((*this), rhs, ChannelId);
      COPY_PROPERTY((*this), rhs, PostId);
      COPY_PROPERTY((*this), rhs, CommentId);
      
      COPY_PROPERTY((*this), rhs, SyncAnchor);
      COPY_PROPERTY((*this), rhs, SyncInd);
      COPY_PROPERTY((*this), rhs, MasterId);
      COPY_BOOL_PROPERTY((*this), rhs, AttachmentOutdated);
      COPY_PROPERTY((*this), rhs, Owner);
      COPY_PROPERTY((*this), rhs, OwnerFullname);

      COPY_PROPERTY((*this), rhs, CreateTime);
      COPY_PROPERTY((*this), rhs, ModifyTime);
      
      COPY_PROPERTY((*this), rhs, ModificationMask);
      COPY_BOOL_PROPERTY((*this), rhs, Selected);
      COPY_PROPERTY((*this), rhs, ModifiedBy);
      COPY_PROPERTY((*this), rhs, ModifiedByFullname);

      COPY_BOOL_PROPERTY((*this), rhs, Secure);
      COPY_BOOL_PROPERTY((*this), rhs, Encrypted);
      COPY_PROPERTY((*this), rhs, IV);
    }
    return *this;
  }
  
  // comparison
  bool Resource::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Resource& mrhs = dynamic_cast<const Resource&> (rhs);
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() == mrhs.getGid();
    else if (this->hasId() && mrhs.hasId())
      return this->getId() == mrhs.getId();
    return false;
  }
  bool Resource::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Resource& mrhs = dynamic_cast<const Resource&> (rhs); // throws std::bad_cast if not of type Media&
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() < mrhs.getGid();
    else if (this->hasId() && mrhs.hasId())
      return this->getId() < mrhs.getId();
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  bool Resource::copyDataFrom( const BaseEntity& rhs )
  {
    if (! this->isDataEqualTo(rhs)) {
      const Resource& mrhs = dynamic_cast<const Resource&> (rhs);

      COPY_PROPERTY((*this), mrhs, Owner);
      COPY_PROPERTY((*this), mrhs, ModifyTime);
      COPY_BOOL_PROPERTY((*this), mrhs, Selected);

      return true;
    }
    return false;
  }
  bool Resource::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    
    return true;
  }
  bool Resource::isDataEqualTo( const BaseEntity& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return false;
  }
  void Resource::archive( void )
  {
    this->clearId( );
    this->setSyncInd( kSyncIndSynchronous );
    this->setModificationMask( kNoneModification );
  }
  void Resource::unarchive( void )
  {
    this->clearId( );
    this->clearGid( ); // new channel
    this->clearSyncAnchor( );
    this->setSyncInd( kSyncIndInsert );
    this->setModificationMask( kMediaPendingLocalModification );
    this->clearCreateTime( );
    this->clearModifyTime( );
    this->setAttachmentOutdated( false );
    this->clearMasterId( );
    this->setSecure( true );
    this->setEncrypted( false );
    this->clearModifiedByFullname( );
    this->clearModifiedBy( );
    this->clearOwnerFullname( );
    this->clearIV( );
  }
  void Resource::clearData( void )
  {
    this->setSecure( false );
    this->setEncrypted( false );
    this->clearIV( );
  }
  
}
