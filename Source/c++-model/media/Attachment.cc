/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Attachment.h"
#include "UsingEjinTypes.h"

#include <iostream>
#include <sstream>

#include "Utilities.h"
#include "SqliteValue.h"
#include "RSACrypto.h"

namespace ejin
{
  using std::ostream;
  using std::ostringstream;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<Attachment>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_text, flag_primary_key),
    Attribute("MEDIA_ID", type_int, flag_not_null),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("SIZE", type_int, flag_not_null),
    Attribute("DATA", type_bytea, flag_not_null),
    Attribute()
  };
  template <> const AttributeSet DataSchema<Attachment>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Attachment::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findMaster"] = "MEDIA_ID = ? and MASTER_ID is null";
      tMap["findSlave"] = "MEDIA_ID = ? and MASTER_ID is not null";
      tMap["findByMediaId"] = "MEDIA_ID = ?";
      tMap["findAll"] = " 1 ";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  const map<string,string> Attachment::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "delete "
      "from " << schema << ".ATTACHMENT_TBL "
      "where MEDIA_ID = ? and MASTER_ID IS NULL";
      tMap["deleteMaster"] = ss.str();

      ss.str("");ss.clear();
      ss << "delete "
      "from " << schema << ".ATTACHMENT_TBL "
      "where MEDIA_ID = ? and MASTER_ID IS NOT NULL";
      tMap["deleteSlave"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  Attachment::Attachment( void ): BaseEntity(TABLE_DDL, false), _media(), _crypto()
  {
  }
  Attachment::Attachment( shared_ptr<Media> media ): Attachment()
  {
    this->setMedia(media);
  }
  Attachment::Attachment( shared_ptr<Media> media, const char* data, size_t size ): Attachment()
  {
    this->setMedia(media);
    setData(data, size);
    setSize(size);
  }
  Attachment::Attachment( const Attachment& attachment ): BaseEntity(attachment), _media(attachment._media),  _crypto()
  {
    COPY_BYTEA_PROPERTY((*this), attachment, Data);
  }
  Attachment::~Attachment( void ) {
    this->clearData();
  }
  
  void Attachment::archive( void ) {
    this->clearId();
  }
  void Attachment::unarchive( void ) {
    this->clearId();
  }

  // ==========
  // Crypto Interface
  // ==========
  
  bool Attachment::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    shared_ptr<Media> media = this->getMedia();
    if (media->isSecure()) {
      if (crypto && crypto->hasSessionKey( )) {
        if (this->hasData()) {
          string cipher = crypto->decrypt( string(this->getData(), this->getDataSize()) );
          this->setData(cipher.c_str(), cipher.length());
          this->setSize(cipher.length());
        }
        if ( media->isEncrypted() ) {
          if (media->hasName()) {
            media->setName( crypto->decrypt( media->getName() ) );
          }
          media->setEncrypted( false );
          COPY_PROPERTY((*media), (*crypto), IV);
        }
        return true;
      }
    }
    return false;
  }
  
  bool Attachment::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    shared_ptr<Media> media = this->getMedia();
    if (media->isSecure()) {
      if (crypto && crypto->hasSessionKey()) {
        if (this->hasData()) {
          string cipher = crypto->encrypt( string(this->getData(), this->getDataSize()) );
          this->setData(cipher.c_str(), cipher.length());
          this->setSize(cipher.length());
          // generating the HMAC-SH1 key from the attachment data
          media->setHmac( Utilities::sign( this->getData(), this->getDataSize(), media->getOwner().c_str() ) );
        }
        if ( ! media->isEncrypted() ) {
          if (media->hasName()) {
            media->setName( crypto->encrypt( media->getName() ) );
          }
          media->setEncrypted( true );
          COPY_PROPERTY((*media), (*crypto), IV);
        }
        return true;
      } else {
        _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "no session key to decrypt message");
      }
    }
    return false;
  }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Attachment& Attachment::operator=( const Attachment& rhs ) {
    if (this != &rhs) {
      this->_media = rhs._media;
      this->_crypto = rhs._crypto;
      COPY_PROPERTY((*this), rhs, MasterId);
      COPY_BYTEA_PROPERTY((*this), rhs, Data);
    }
    return *this;
  }
  bool Attachment::operator<( const Attachment& rhs ) {
    if (this != &rhs) {
      return this->_media < rhs._media;
    }
    return false;
  }
  // comparison
  bool Attachment::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Attachment& mrhs = dynamic_cast<const Attachment&> (rhs);
    return getId() == mrhs.getId();
  }
  bool Attachment::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Attachment& mrhs = dynamic_cast<const Attachment&> (rhs); // throws std::bad_cast if not of type Member&
    return getId() < mrhs.getId();
  }
  
  const char* Attachment::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& Attachment::tableFields( void ) const { return TABLE_DDL; }
  
  // ==========
  // Debugging
  // ==========
  
  ostream& operator<<( ostream& ostr, const Attachment& rhs ) {
    ostringstream buf;
    buf.flags(ostr.flags());
    buf.fill(ostr.fill());
    buf << "Attachment[id=" << rhs.getId()
    << ", mediaId=" << rhs.getMediaId()
    << ", isMaster=" << ! rhs.hasMasterId()
    << ", hasData=" << rhs.hasData()
    << ", size=" << rhs.getDataSize() << " ]";
    ostr << buf.str();
    return ostr;
  }
  
}

