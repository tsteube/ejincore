/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaRepository.h"
#include "UsingEjinTypes.h"

extern "C" {
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>   // open
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <string.h>
#include <sys/param.h>
}

#include "Utilities.h"
#include "Media.h"
#include "Attachment.h"
#include "KeySequence.h"
#include "Media.h"
#include "ChannelHeader.h"
#include "ChannelPost.h"
#include "ChannelComment.h"
#include "ChangeLogRepository.h"
#include "SqliteValue.h"


namespace ejin
{
  // ==========
  // Ctor
  // ==========
  
  
  // ==========
  // Public Interface
  // ==========
  
  bool MediaRepository::detectConflict( const string& mediaGid ) const throw(data_access_error)
  {
    assert ( ! mediaGid.empty() );
    
    shared_ptr<Media> media = this->loadMediaInternal( Value(mediaGid.c_str()), Value(true) );
    return (bool)media;
  }
  
  bool MediaRepository::clearConflict( const string& mediaGid ) throw(data_access_error)
  {
    assert ( ! mediaGid.empty() );
    
    shared_ptr<Media> mediaEntity = this->loadMediaInternal( Value(mediaGid.c_str()), Value(false) );
    shared_ptr<Media> conflict = this->loadMediaInternal( Value(mediaGid.c_str()), Value(true) );
    
    bool cleared = false;
    if (conflict) {
      // remove conflicted entity
      this->db_.remove(*conflict);
      
      // update state of original entity
      if ( mediaEntity ) {
        if ( mediaEntity->isEmpty() ) {
          this->db_.remove( *mediaEntity );
        } else {
          mediaEntity->setSyncInd( kSyncIndSynchronous );
          mediaEntity->setModificationMask(kNoneModification);
          this->db_.update( *mediaEntity );
        }
      }
      cleared = true;
    }
    return cleared;
  }
  
  shared_ptr<Media> MediaRepository::update( Media& media, const jtime& syncTime ) throw(data_access_error)
  {
    assert( media.hasGid() );
    
    bool updated = false;
    shared_ptr<Media> mediaEntity = this->loadMediaInternal( Value(media.getGid().c_str()), Value(false) );
    
    // update algorithm
    if ( mediaEntity ) {
      if ( ! media.hasModifiedBy() ) {
        // keep modifier from channel update
        media.setModifiedBy( mediaEntity->getModifiedBy() );
      }
      
      // setup sync anchor keys
      KeySequence entitySyncAnchor(mediaEntity->getSyncAnchor());
      KeySequence nextSyncAnchor = entitySyncAnchor;
      KeySequence mediaSyncAnchor;
      if ( media.hasSyncAnchor() ) {
        mediaSyncAnchor = KeySequence(media.getSyncAnchor());
        if (mediaSyncAnchor > entitySyncAnchor) {
          nextSyncAnchor = mediaSyncAnchor;
        }
      }
      
      if ( nextSyncAnchor >= entitySyncAnchor ) {
        if ( media.isEmpty() )  {
          mediaEntity->setModifiedBy( media.getModifiedBy( ) );
          if ( removeMedia( *mediaEntity, nextSyncAnchor ) ) {
            updated = true;
            this->changeLogRepository_.remove( *mediaEntity, nextSyncAnchor, syncTime );
          }
        } else {
          if ( updateMedia( *mediaEntity, media, nextSyncAnchor ) ) {
            updated = true;
            this->changeLogRepository_.update( *mediaEntity, nextSyncAnchor, syncTime );
          }
        }
        
      } else {
        mediaEntity.reset();
        
      }
      
    } else {
      
      if (! media.isEmpty()) {
        // add channel post entity
        mediaEntity = addMedia( media );
        if ( mediaEntity ) {
          updated = true;
          this->changeLogRepository_.add( *mediaEntity, media.getSyncAnchor(), syncTime );
        }
      } else {
        mediaEntity.reset();
      }
    }
    
    if (updated) {
      clearDeleteOwnerInd( *mediaEntity );
    }
    
    return mediaEntity;
  }
  
  shared_ptr<Media> MediaRepository::commit( Media& media, const string& syncAnchor, const string& username, const jtime& syncTime ) throw(data_access_error)
  {
    assert( media.hasId() );
    assert( media.hasGid() );
    assert( ! syncAnchor.empty() );
    
    shared_ptr<Media> mediaEntity = shared_ptr<Media>(new Media());
    mediaEntity->setId(media.getId());
    this->db_.refresh( *mediaEntity ); // throws DataRetrievalFailureException if not found
    
    mediaEntity->setSyncAnchor( syncAnchor );
    mediaEntity->setModifyTime( syncTime );
    mediaEntity->setAttachmentOutdated( false );
    mediaEntity->setModifiedBy( username );
    SyncInd syncInd = (SyncInd) mediaEntity->getSyncInd();
    switch ( syncInd ) {
      case kSyncIndSynchronous:
        this->db_.update( *mediaEntity );
        break;
      case kSyncIndConflict:
        mediaEntity->setTransferKey( media.getTransferKey() );
        mediaEntity->setHmac( media.getHmac() );
        mediaEntity->setModificationMask( kMediaConflictedModification );
        this->db_.update( *mediaEntity );
        this->changeLogRepository_.update( *mediaEntity, syncAnchor, syncTime );
        this->clearMediaCache( *mediaEntity );
        break;
      case kSyncIndInsert:
        // update global identifier
        mediaEntity->setGid( media.getGid() );
        mediaEntity->setCreateTime( syncTime );
      case kSyncIndUpdate:
        // update attachment meta data
        mediaEntity->setTransferKey( media.getTransferKey() );
        mediaEntity->setHmac( media.getHmac() );
        mediaEntity->setSyncInd( kSyncIndSynchronous );
        mediaEntity->setModificationMask( kNoneModification );
        mediaEntity->setAttachmentUpdate( false );
        mediaEntity->setIV( media.getIV() );
        this->db_.update( *mediaEntity );
        if ( syncInd == kSyncIndInsert ) {
          this->changeLogRepository_.add( *mediaEntity, syncAnchor, syncTime );
        } else {
          this->changeLogRepository_.update( *mediaEntity, syncAnchor, syncTime );
        }
        // delete attachment backup if available
        this->clearMediaCache( *mediaEntity );
        break;
      case kSyncIndDelete:
        this->removeMedia( *mediaEntity, KeySequence(syncAnchor) );
        this->changeLogRepository_.remove( *mediaEntity, syncAnchor, syncTime );
        break;
    }
    
    return mediaEntity;
  }
  
  Attachment& MediaRepository::saveAttachment( Attachment& attachment )
  throw(data_access_error)
  {
    shared_ptr<Media> media = attachment.getMedia();
    assert( media->hasId() );
    assert( media->hasGid() );
    assert( media->hasOwner() );
    
    shared_ptr<Media> entity = shared_ptr<Media>(new Media());
    entity->setId(media->getId());
    this->db_.refresh( *entity ); // throws DataRetrievalFailureException if not found
    
    Value mediaId(media->getId());
    shared_ptr<Attachment> attachmentEntity( this->loadAttachmentInternal( mediaId, false ) );
    if ( attachmentEntity ) {
      if ( media->hasTransferKey() && attachment.hasData() ) {
        // update attachment
        assert( attachment.hasHmac() );
        assert( attachment.hasData() );
        attachmentEntity->setData( attachment.getData(), attachment.getDataSize());
        this->db_.update( *attachmentEntity );
        this->clearMediaCache( *media );
        
        // delete old backup if available
        //this->db_.valueOf(Attachment::TABLE_NAME, "deleteSlave", &mediaId, NULL);
      } else {
        // clear attachment
        entity->setModificationMask( kNoneModification );
        this->db_.remove( *attachmentEntity );
      }
    } else {
      // new attachment
      attachment.setMediaId( media->getId() );
      this->db_.insert( attachment );
    }
    
    // update local repository state
    entity->setName( media->getName() );
    entity->setEncrypted( media->isEncrypted() );
    entity->setIV( media->getIV() );
    entity->setSize( attachment.getSize() );
    entity->setAttachmentOutdated( !attachment.hasData() );
    entity->setAttachmentUpdate( false );
    //entity->setModificationMask( kNoneModification );
    
    // persist entity
    this->db_.update( *entity );
    
    attachment.setMedia( entity );
    return attachment;
  }
  
  bool MediaRepository::deleteAttachment( const Media& media )
  throw(data_access_error)
  {
    assert( media.hasId() );
    assert( media.hasOwner() );
    
    Value mediaId(media.getId());
    shared_ptr<Attachment> attachmentEntity( this->loadAttachmentInternal( mediaId, false ) );
    if ( attachmentEntity ) {
      this->db_.remove( *attachmentEntity );
      return true;
    }
    return false;
  }
  
  bool MediaRepository::moveAttachment( integer from, integer to )
  throw(data_access_error)
  {
    Value fromMediaId(from);
    Value toMediaId(to);
    shared_ptr<Attachment> attachmentMaster( this->loadAttachmentInternal( fromMediaId, false ) );
    if ( attachmentMaster ) {
      // delete old backup if available
      this->db_.valueOf(Attachment::TABLE_NAME, "deleteSlave", &toMediaId, NULL);
      // set new target attachment
      attachmentMaster->setMediaId( to );
      this->db_.update( *attachmentMaster );
      
      shared_ptr<Attachment> attachmentSlave( this->loadAttachmentInternal( fromMediaId, true ) );
      if ( attachmentSlave ) {
        // set new target attachment
        attachmentSlave->setMediaId( to );
        this->db_.update( *attachmentSlave );
      }
      return true;
    }
    return false;
  }
  
  bool MediaRepository::copyAttachment( integer from, integer to )
  throw(data_access_error)
  {
    Value fromMediaId(from);
    Value toMediaId(to);
    shared_ptr<Attachment> attachmentMaster( this->loadAttachmentInternal( fromMediaId, false ) );
    if ( attachmentMaster ) {
      // delete old backup if available
      this->db_.valueOf(Attachment::TABLE_NAME, "deleteSlave", &toMediaId, NULL);
      // set new target attachment
      attachmentMaster->clearId();
      attachmentMaster->setMediaId( to );
      this->db_.insert( *attachmentMaster );
      
      shared_ptr<Attachment> attachmentSlave( this->loadAttachmentInternal( fromMediaId, true ) );
      if ( attachmentSlave ) {
        // set new target attachment
        attachmentSlave->clearId();
        attachmentSlave->setMasterId( attachmentMaster->getId() );
        attachmentSlave->setMediaId( to );
        this->db_.insert( *attachmentSlave );
      }
      return true;
    }
    return false;
  }
  
  bool MediaRepository::revert( const string& gid, bool force )
  throw(data_access_error)
  {
    assert( ! gid.empty() );
    
    bool reverted = false;
    Value gidValue(gid.c_str());
    shared_ptr<Media> mediaEntity( this->loadMediaInternal( gidValue, Value(false) ) );
    if ( mediaEntity != NULL )
    {
      // ensure that there a no pending conflicts
      if ( clearConflict( gid ) ) {
        // reload after conflict has been cleared
        mediaEntity = this->loadMediaInternal( gidValue, Value(false) );
      }
      
      switch ( mediaEntity->getSyncInd() ) {
        case kSyncIndInsert:
        {
          if (force) {
            db_.remove( *mediaEntity );
            reverted = true;
          } else {
            cloneMedia(*mediaEntity, NULL, NULL);
            reverted = true;
          }
          break;
        }
        case kSyncIndSynchronous:
        case kSyncIndUpdate:
        case kSyncIndDelete:
        {
          // load entity from backup
          shared_ptr<Media> mediaEntityBak( shared_ptr<Media>(new Media()) );
          mediaEntityBak->setId(mediaEntity->getId());
          db_.refresh(*mediaEntityBak, mediaEntityBak->backupSchemaName() );
          if ( force ) {
            // update dates and states
            mediaEntity->copyDataFrom( *mediaEntityBak );
            mediaEntity->setSyncInd( kSyncIndSynchronous );
            mediaEntity->setModificationMask( kNoneModification );
            mediaEntity->setAttachmentOutdated( false );
            mediaEntity->setAttachmentUpdate( false );
            this->db_.update( *mediaEntity );
            revertAttachment( *mediaEntity );
            reverted = true;
          } else {
            KeySequence syncAnchor( mediaEntity->getSyncAnchor() );
            if ( cloneMedia( *mediaEntity, mediaEntityBak.get(), &syncAnchor ) ) {
              revertAttachment( *mediaEntity );
              reverted = true;
            }
          }
          break;
        }
        case kSyncIndConflict:
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "media[id=%d] in conflict state", mediaEntity->getId());
      }
      
    } else {
      // restore old member if found in backup
      unique_ptr<ResultSet> values( db_.find(Media::TABLE_NAME, "findMediaByGidBak", &gidValue, NULL) );
      vector< BaseEntity* >::iterator it = values->begin();
      if (it != values->end()) {
        this->db_.insert( **it );
        reverted = true;
      }
      
    }
    return reverted;
  }
  
  bool MediaRepository::revertNewMedias( integer channelId, integer postId, integer commentId, bool force )
  throw(data_access_error)
  {
    unique_ptr<ResultSet> values;
    if ( commentId > 0 ) {
      Value id(commentId);
      values = db_.find(Media::TABLE_NAME, "findChannelCommentMedias", &id, NULL);
    } else if ( postId > 0 ) {
      Value id(postId);
      values = db_.find(Media::TABLE_NAME, "findChannelPostMedias", &id, NULL);
    } else {
      Value id(channelId);
      values = db_.find(Media::TABLE_NAME, "findChannelHeaderMedias", &id, NULL);
    }
    
    bool reverted = false;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); it++) {
      Media* mediaEntity(static_cast< Media* >(*it));
      if ( mediaEntity->getSyncInd() == kSyncIndInsert ) {
        if (force) {
          deleteMedia( *mediaEntity );
          reverted = true;
        } else {
          cloneMedia(*mediaEntity, NULL, NULL);
          reverted = true;
        }
      }
    }
    return reverted;
  }
  
  // ==========
  // Private Interface
  // ==========
  
  bool MediaRepository::revertAttachment( const Media& media )
  throw(data_access_error)
  {
    assert( media.getId() > 0 );
    
    Value mediaId(media.getId());
    shared_ptr<Attachment> attachmentMaster( this->loadAttachmentInternal( mediaId, false ) );
    shared_ptr<Attachment> attachmentSlave( this->loadAttachmentInternal( mediaId, true ) );
    if ( attachmentSlave ) {
      // set new target attachment
      attachmentSlave->clearMasterId( );
      this->db_.update( *attachmentSlave );
      this->db_.remove( *attachmentMaster );
      return true;
    }
    return false;
  }
  
  shared_ptr<Media> MediaRepository::addMedia( Media& media )
  throw(data_access_error)
  {
    assert( media.hasGid() );
    
    // Map the new media entity to an existing channel by the global channel id.
    // Otherwise we would have a media not bound to a channel.
    Value channelGid(media.getChannelGid().c_str());
    Value hidden(false);
    unique_ptr<ResultSet> values( db_.find(ChannelHeader::TABLE_NAME, "findChannelByGid", &channelGid, &hidden, NULL) );
    if (values->size() == 0) {
      ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Can't resolve channel[gid=%s] for media [gid=%s] to add.",
                             media.getChannelGid().c_str(), media.getGid().c_str());
    }
    ChannelHeader* channelHeader( static_cast< ChannelHeader* >(*values->begin()) );
    
    // create new entity with unique global key
    shared_ptr<Media> mediaEntity = shared_ptr<Media>(new Media( media ));
    mediaEntity->clearId(); // generate new local database identifier
    mediaEntity->setChannelId( channelHeader->getId() );
    mediaEntity->setNo( -1 ); // not bound to channel entity yet
    mediaEntity->setModifiedBy( media.getOwner() );
    mediaEntity->setSyncAnchor( media.getSyncAnchor() );
    mediaEntity->setSyncInd( kSyncIndSynchronous );
    mediaEntity->setAttachmentOutdated( true );
    mediaEntity->setAttachmentUpdate( false );
    mediaEntity->setModificationMask( kMediaRecentRemoteModification );
    // persist entity
    this->db_.insert(*mediaEntity);
    
    // update input data
    media.setId( mediaEntity->getId() );
    media.setSyncInd( mediaEntity->getSyncInd() );
    
    return mediaEntity;
  }
  
  bool MediaRepository::updateMedia( Media& mediaEntity, Media& media, const KeySequence& syncAnchor )
  throw(data_access_error)
  {
    assert( mediaEntity.hasId() );
    assert( ! mediaEntity.isHidden() );
    assert( media.hasGid() );
    
    bool result = false;
    switch ( mediaEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // Note in this case the new media has already been added to the server in the syncChannel step
        // but the data itself has not been uploaded yet.
        // Therefore skip this update request and go on with the upload step
        break;
      case kSyncIndSynchronous:
        // entity was updated on the server
        // update attachment state
        if ( mediaEntity.hasHmac() && media.hasHmac() &&
            mediaEntity.getHmac() != media.getHmac()) {
          mediaEntity.setAttachmentOutdated( true );
        }
        mediaEntity.setAttachmentUpdate( false );
        mediaEntity.setSyncAnchor( syncAnchor );
        if ( ! mediaEntity.hasEqualContent( media ) ) {
          mediaEntity.setModificationMask( kMediaRecentRemoteModification );
          result = true;
        }
        mediaEntity.copyDataFrom( media );
        std::cout << "BBBBBBB " << mediaEntity << std::endl;
        this->db_.update( mediaEntity );
        break;
      case kSyncIndUpdate:
        if ( mediaEntity.hasEqualContent( media ) )
        {
          mediaEntity.setAttachmentOutdated( false );
          mediaEntity.setAttachmentUpdate( false );
          // update only some dates and state data
          mediaEntity.copyDataFrom( media );
          mediaEntity.setSyncInd( kSyncIndSynchronous );
          mediaEntity.setSyncAnchor( syncAnchor );
          mediaEntity.setModificationMask( kNoneModification );
          std::cout << "BBBBBBB 2 " << mediaEntity << std::endl;

          this->db_.update( mediaEntity );
          result = true;
        }
        else
        {
          // conflict detected; save local entity for reference
          cloneMedia( mediaEntity, &media, &syncAnchor );
          result = true;
        }
        break;
      case kSyncIndDelete:
        if ( mediaEntity.hasEqualContent( media ) )
        {
          // no change on server side but removed on client
          mediaEntity.setSyncAnchor( syncAnchor );
          this->db_.update( mediaEntity );
          result = true;
        }
        else
        {
          // conflict detected; save local entity for reference
          cloneMedia( mediaEntity, &media, &syncAnchor );
          result = true;
        }
        break;
      case kSyncIndConflict:
        shared_ptr<Media> conflict( this->loadMediaInternal( Value(mediaEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", mediaEntity.getId());
        }
        if ( conflict->hasEqualContent( media ) )
        {
          // resolve conflict
          switch ( conflict->getSyncInd() ) {
            case kSyncIndSynchronous:
            case kSyncIndConflict:
              _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", mediaEntity.getId());
            case kSyncIndInsert:
            case kSyncIndUpdate:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // client and server are in sync now again
              // update only some dates and state data
              mediaEntity.copyDataFrom( media );
              mediaEntity.setSyncInd( kSyncIndSynchronous );
              mediaEntity.setAttachmentOutdated( false );
              mediaEntity.setAttachmentUpdate( false );
              mediaEntity.setSyncAnchor( syncAnchor );
              mediaEntity.setModificationMask( kNoneModification );
              this->db_.update( mediaEntity );
              result = true;
              break;
            case kSyncIndDelete:
              // clear conflict data set because the server has the same data content
              this->db_.remove( *conflict );
              // remember that the member was already deleted on the client
              mediaEntity.setSyncInd( kSyncIndDelete );
              this->db_.update( mediaEntity );
              result = true;
              break;
          }
        } else {
          // keep original local conflict data
          // update only some dates and state data
          if ( mediaEntity.hasHmac() && media.hasHmac() &&
              mediaEntity.getHmac() == media.getHmac()) {
            mediaEntity.setAttachmentOutdated( false );
          } else {
            mediaEntity.setAttachmentOutdated( true );
          }
          mediaEntity.setAttachmentUpdate( false );
          mediaEntity.setSyncInd( kSyncIndConflict );
          mediaEntity.setSyncAnchor( syncAnchor );
          mediaEntity.copyDataFrom( media );
          this->db_.update( mediaEntity );
          result = true;
        }
        break;
    }
    
    // update id
    media.setId( mediaEntity.getId() );
    
    return result;
  }
  
  bool MediaRepository::removeMedia( Media& mediaEntity, const KeySequence& syncAnchor )
  throw(data_access_error)
  {
    assert( mediaEntity.hasId() );
    assert( ! mediaEntity.isHidden() );
    
    switch ( mediaEntity.getSyncInd() ) {
      case kSyncIndInsert:
        // must be unknown on the server
        _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "Invalid sync state of Media[id=%d]", mediaEntity.getId());
      case kSyncIndUpdate:
      {
        // conflict detected; save local entity for reference
        cloneMedia( mediaEntity, NULL, &syncAnchor );
        this->deleteAttachment( mediaEntity );
        break;
      }
      case kSyncIndSynchronous:
        // local unchanged entity was removed on the server
      case kSyncIndDelete:
        // entity was removed on client and server
        deleteMedia( mediaEntity );
        break;
      case kSyncIndConflict:
        shared_ptr<Media> conflict( this->loadMediaInternal( Value(mediaEntity.getGid().c_str()), Value(true) ) );
        if ( ! conflict ) {
          _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", mediaEntity.getId());
        }
        switch ( conflict->getSyncInd() ) {
          case kSyncIndSynchronous:
          case kSyncIndInsert:
          case kSyncIndConflict:
            _throw_exception(DATA_INTEGRITY_VIOLATION_EXCEPTION, "No local hidden data set found for conflict[id=%d]", mediaEntity.getId());
          case kSyncIndDelete:
            // entity was removed on client and server
            this->db_.remove( *conflict );
            deleteMedia( mediaEntity );
            break;
          case kSyncIndUpdate:
            // normal case: keep conflict with local changes
            this->deleteAttachment( mediaEntity );
            break;
        }
        break;
    }
    return true;
  }
  
  void MediaRepository::deleteMedia( Media& mediaEntity )
  throw(data_access_error)
  {
    assert( mediaEntity.hasId() );
    
    // delete attachments
    this->deleteAttachment( mediaEntity );
    // delete entity itself
    this->db_.remove(mediaEntity);
  }
  
  bool MediaRepository::clearMediaCache( const Media& media )
  throw(data_access_error)
  {
    assert( media.hasId() );
    assert( media.hasGid() );
    assert( media.hasOwner() );
    
    Value mediaId(media.getId());
    shared_ptr<Attachment> attachmentEntity( this->loadAttachmentInternal( mediaId, true ) );
    if ( attachmentEntity ) {
      this->db_.remove( *attachmentEntity );
      return true;
    }
    return false;
  }
  
  bool MediaRepository::cloneMedia( Media& mediaEntity, const Media* media, const KeySequence* syncAnchor )
  throw(data_access_error)
  {
    if ( media == NULL || ! mediaEntity.hasEqualContent( *media ) ) {
      // copy current state of entity into cloned entity
      unique_ptr<Media> clone = unique_ptr<Media>(new Media( mediaEntity ));
      clone->clearId( );
      clone->setMasterId( mediaEntity.getId() );
      clone->setModificationMask( clone->getModificationMask()|kMediaConflictedModification );
      
      // update only some dates and states
      if ( media )
      {
        // reload attachment data if data hash is not identical
        if ( mediaEntity.hasHmac() && media->hasHmac() &&
            mediaEntity.getHmac() == media->getHmac()) {
          mediaEntity.setAttachmentOutdated( false );
        } else {
          mediaEntity.setAttachmentOutdated( true );
        }
        // update dates and states
        mediaEntity.copyDataFrom( *media );
      }
      else
      {
        // mark entity as removed from server
        mediaEntity.clearData();
        mediaEntity.setAttachmentOutdated( false );
      }
      mediaEntity.setSyncInd( kSyncIndConflict );
      if (syncAnchor)
        mediaEntity.setSyncAnchor( *syncAnchor );
      mediaEntity.setModificationMask( kMediaConflictedModification );
      
      // first we need to change the local state of the master entity
      this->db_.update( mediaEntity );
      // before we can insert the clone to avoid a unique constraint violation
      this->db_.insert( *clone );
      // copy attachment to cloned entity
      this->copyAttachment( mediaEntity.getId(), clone->getId() );
      
      return true;
    }
    return false;
  }
  
  void MediaRepository::clearDeleteOwnerInd( Media& mediaEntity )
  throw(data_access_error)
  {
    // reset delete marker of owner entites
    const Value channelId(mediaEntity.getChannelId());
    this->db_.valueOf(ChannelHeader::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
    if ( mediaEntity.hasPostId() ) {
      const Value postId(mediaEntity.getPostId());
      this->db_.valueOf(ChannelPost::TABLE_NAME, "clearDeleteSyncInd", &postId, NULL);
    }
    if ( mediaEntity.hasCommentId() ) {
      const Value commentId(mediaEntity.getCommentId());
      this->db_.valueOf(ChannelComment::TABLE_NAME, "clearDeleteSyncInd", &channelId, NULL);
    }
  }
  
}
