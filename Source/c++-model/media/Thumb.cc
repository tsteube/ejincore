/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Thumb.h"
#include "UsingEjinTypes.h"

#include <iostream>
#include <sstream>

#include "Utilities.h"
#include "SqliteValue.h"

namespace ejin
{
  using std::ostream;
  using std::ostringstream;
  
  // ==========
  // Schema DDL
  // ==========
  
  template <> const Attribute DataSchema<Thumb>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_text, flag_primary_key),
    Attribute("ATTACHMENT_ID", type_int, flag_not_null),
    Attribute("SIZE", type_int, flag_not_null),
    Attribute("DATA", type_bytea, flag_not_null),
    Attribute()
  };
  template <> const AttributeSet DataSchema<Thumb>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> Thumb::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findByAttachmentId"] = "ATTACHMENT_ID = ?";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  // ==========
  // Ctor / Dtor
  // ==========
  
  Thumb::Thumb( void ): BaseEntity(TABLE_DDL, false)
  {
  }
  Thumb::Thumb( const char* data, size_t size ): Thumb()
  {
    setData(data, size);
  }
  Thumb::Thumb( const Thumb& attachment ): BaseEntity(attachment)
  {
    COPY_BYTEA_PROPERTY((*this), attachment, Data);
  }
  Thumb::~Thumb( void ) {
    this->clearData();
  }

  void Thumb::archive( void ) {
    this->clearId();
  }
  void Thumb::unarchive( void ) {
    this->clearId();
  }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  Thumb& Thumb::operator=( const Thumb& rhs ) {
    if (this != &rhs) {
      COPY_PROPERTY((*this), rhs, AttachmentId);
      COPY_BYTEA_PROPERTY((*this), rhs, Data);
    }
    return *this;
  }
  bool Thumb::operator<( const Thumb& rhs ) {
    if (this != &rhs) {
      return this->getAttachmentId() < rhs.getAttachmentId();
    }
    return false;
  }
  // comparison
  bool Thumb::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    
    const Thumb& mrhs = dynamic_cast<const Thumb&> (rhs);
    return getId() == mrhs.getId();
  }
  bool Thumb::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    
    const Thumb& mrhs = dynamic_cast<const Thumb&> (rhs); // throws std::bad_cast if not of type Member&
    return getId() < mrhs.getId();
  }
  
  const char* Thumb::tableName( void ) const { return TABLE_NAME; }
  const AttributeSet& Thumb::tableFields( void ) const { return TABLE_DDL; }
  
  // ==========
  // Debugging
  // ==========
  
  ostream& operator<<( ostream& ostr, const Thumb& rhs ) {
    ostringstream buf;
    buf.flags(ostr.flags());
    buf.fill(ostr.fill());
    buf << "Thumb[id=" << rhs.getId()
    << ", attachmentId=" << rhs.getAttachmentId()
    << ", hasData=" << rhs.hasData()
    << ", size=" << rhs.getDataSize() << " ]";
    ostr << buf.str();
    return ostr;
  }
  
}

