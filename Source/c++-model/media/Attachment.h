/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_ATTACHMENT_H__
#define __EJIN_ATTACHMENT_H__

#include "Media.h"
#include "ICryptoEntity.h"
#include "SqliteBaseEntity.h"

using sqlite::BaseEntity;

namespace ejin
{
  class RSACrypto;
  
  /**
   * ejin file attachment entity
   */
  class Attachment: public sqlite::BaseEntity, public DataSchema<Attachment>, public ICryptoEntity {
    
    friend ostream& operator<<(ostream& ostr, const Attachment& rhs);
    
    // ==========
    // Public Interface
    // ==========
  public:
    constexpr static const char* const TABLE_NAME = "ATTACHMENT_TBL";

    // ctor
    Attachment( void );
    Attachment( shared_ptr<Media> media );
    Attachment( shared_ptr<Media> media, const char* data, size_t length );
    Attachment( const Attachment& record );
    // dtor
    ~Attachment( void );
    
    void archive( void );
    void unarchive( void );

    // ==========
    // Object Interface
    // ==========
  public:
    
    // assignment
    Attachment& operator=( const Attachment& rhs );
    bool operator<( const Attachment& rhs );
    // clone
    BaseEntity* clone( void ) const { return new Attachment(*this); }
    
    // ==========
    // Crypto Interface
    // ==========
  public:
    
    bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error);
    const string getChannelGid( void ) const { return string(); }
    bool hasChannelGid( void ) const { return false; }
    
    // ==========
    // Getter / Setter
    // ==========
  public:
    
    ACCESS_INTEGER_PROPERTY_INTF( Id, 0 )
    ACCESS_INTEGER_PROPERTY_INTF( MediaId, 1 )
    ACCESS_INTEGER_PROPERTY_INTF( MasterId, 2 )
    ACCESS_INTEGER_PROPERTY_INTF( Size, 3 )
    ACCESS_BYTEA_PROPERTY_INTF( Data, 3, 4 )
    
    const string getTransferKey( void ) const { return this->_media->getTransferKey(); }
    bool hasTransferKey( void ) const { return this->_media->hasTransferKey(); }
    void setTransferKey( const string& key ) { return this->_media->setTransferKey(key); }
    void clearTransferKey( void ) { return this->_media->clearTransferKey(); }
    
    const shared_ptr<Media> getMedia( void ) const { return this->_media; }
    const void setMedia( shared_ptr<Media> media ) { this->_media = media; }
    
    const string getGid( void ) const { return this->_media->getGid(); }
    bool hasGid( void ) const { return this->_media->hasGid(); }
    
    const integer getChannelId( void ) const { return this->_media->getChannelId(); }
    bool hasChannelId( void ) const { return this->_media->hasChannelId(); }
    
    const string getOwner( void ) const { return this->_media->getOwner(); }
    bool hasOwner( void ) const { return this->_media->hasOwner(); }
    
    const string getHmac( void ) const { return this->_media->getHmac(); }
    bool hasHmac( void ) const { return this->_media->hasHmac(); }
    
    bool isAttachmentOutdated( void ) const { return this->_media->isAttachmentOutdated(); }
    bool hasAttachmentOutdated( void ) const { return this->_media->hasAttachmentOutdated(); }
    
    bool isAttachmentUpdate( void ) const { return this->_media->isAttachmentUpdate(); }
    bool hasAttachmentUpdate( void ) const { return this->_media->hasAttachmentUpdate(); }
    
    const string getMimeType( void ) const { return this->_media->getMimeType(); }
    bool hasMimeType( void ) const { return this->_media->hasMimeType(); }
    
    shared_ptr<RSACrypto> getCrypto() const { return this->_crypto; }
    void setCrypto( shared_ptr<RSACrypto> crypto ) { this->_crypto = crypto; }
    void clearCrypto( void ) { this->_crypto.reset(); }
    bool hasCrypto( void ) const { return (bool)this->_crypto; }
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    const map<string,string> sqlAliases( const char* schema ) const;
    const map<string,string> finderTemplates( const char* schema ) const;
    
    bool equals( const BaseEntity& rhs ) const;
    bool lessThan( const BaseEntity& rhs ) const;
    
    // schema DDL
    const char* tableName( void ) const;
    const char* mainSchemaName( void ) const { return ATTACHMENT_DATABASE_NAME; }
    const char* backupSchemaName( void ) const { return NULL; }
    const char* archiveSchemaName( void ) const { return ATTACHMENT_ARCHIVE_DATABASE_NAME; }
    const sqlite::AttributeSet& tableFields( void ) const;
    bool useSequenceTable() const { return false; }
    
    // crypto reference
    shared_ptr<RSACrypto> _crypto;
    shared_ptr<Media>     _media;
  };
  
  // ----------
  // global methods
  
  // comparision
  inline bool operator!=(const Attachment& lhs, const Attachment& rhs) { return !operator==(lhs,rhs); }
  inline bool operator> (const Attachment& lhs, const Attachment& rhs) { return  operator< (rhs,lhs); }
  inline bool operator<=(const Attachment& lhs, const Attachment& rhs) { return !operator> (lhs,rhs); }
  inline bool operator>=(const Attachment& lhs, const Attachment& rhs) { return !operator< (lhs,rhs); }
  
  // debugging
  ostream& operator<<( ostream& ostr, const Attachment& rhs );
  
}

#endif // __EJIN_ATTACHMENT_H__
