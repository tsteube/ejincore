/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "MediaProfile.h"
#include "UsingEjinTypes.h"

extern "C" {
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>   // open
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <string.h>
#include <sys/param.h>
}

#include "Media.h"
#include "Attachment.h"
#include "ChannelMedia.h"
#include "Utilities.h"
#include "SqliteValue.h"
#include "ChannelFullText.h"

typedef struct stat Stat;

namespace ejin
{
  
  static list< shared_ptr<Media> > convertToMediaSet( unique_ptr<ResultSet>& values );
  list< shared_ptr<Media> > convertToMediaSet( unique_ptr<ResultSet>& values )
  {
    list< shared_ptr<Media> > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ) {
      result.push_back( shared_ptr<Media>(static_cast< Media* >( *it ) ) );
      it = values->take( it );
    }
    return result;
  }
  
  MediaProfile::MediaProfile( Database& db ): db_(db)
  { }
  
  // ==========
  // Public Interface
  // ==========
  
  list< shared_ptr<Media> > MediaProfile::findUpdated( void ) const throw(data_access_error) 
  {
    // call entity finder
    Value insertedVal((integer)kSyncIndInsert);
    Value updatedVal((integer)kSyncIndUpdate);
    unique_ptr<ResultSet> insertValues(this->db_.find(MediaProfileView::TABLE_NAME, "findMediasByState", &insertedVal, NULL));
    unique_ptr<ResultSet> updatesValues(this->db_.find(MediaProfileView::TABLE_NAME, "findMediasByState", &updatedVal, NULL));

    list< shared_ptr<Media> > inserted( convertToMediaSet( insertValues ) );
    list< shared_ptr<Media> > updated( convertToMediaSet( updatesValues ) );
    
    list< shared_ptr<Media> > result;
    result.insert(result.end(), inserted.begin(), inserted.end());
    result.insert(result.end(), updated.begin(), updated.end());
    return result;
  }
  
  list< shared_ptr<Media> > MediaProfile::findConflicted( void ) const throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet> values(this->db_.find(MediaProfileView::TABLE_NAME, "findConflictedMedias", NULL));
    return convertToMediaSet( values );
  }
  
  list< string > MediaProfile::findConflicts( void ) const
  throw(data_access_error)
  {
    list< string > conflicts;
    list< shared_ptr<Media> > rawConflicts( this->findConflicted( ) );
    for (list< shared_ptr<Media> >::iterator it=rawConflicts.begin() ; it != rawConflicts.end(); it++ ) {
      conflicts.push_back( (*it)->getGid() );
    }
    return conflicts;
  }
  
  list< shared_ptr<Attachment> > MediaProfile::findOutdatedAttachments( void ) const
  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet>  values( this->db_.find(Media::TABLE_NAME, "findOutdatedAttachments", NULL) );
    // convert set of unique identifiers to member entities
    list< shared_ptr<Attachment> > attachments;
    for (vector<BaseEntity*>::iterator it=values->begin(); it != values->end(); ) {
      shared_ptr<Media> media( shared_ptr<Media>( static_cast<Media*>(*it)) );
      attachments.push_back(shared_ptr<Attachment>(new Attachment(media)));
      it = values->take( it );
    }
    return attachments;
  }
  
  list< shared_ptr<Attachment> > MediaProfile::findUpdatedAttachments( void ) const
  throw(data_access_error)
  {
    // call entity finder
    unique_ptr<ResultSet>  values( this->db_.find(MediaProfileView::TABLE_NAME, "findUpdatedAttachments", NULL) );
    
    // convert set of unique identifiers to member entities
    list< shared_ptr<Attachment> > attachments;
    for (vector<BaseEntity*>::iterator it=values->begin(); it != values->end(); it++) {
      MediaProfileView* mediaProfile = static_cast<MediaProfileView*>( *it );
      shared_ptr<Attachment> attachment( shared_ptr<Attachment>(new Attachment(shared_ptr<Media>(new Media(*mediaProfile)))) );
      if (this->loadAttachment( *attachment )) {
        attachments.push_back(attachment);
      }
    }
    return attachments;
  }
  
  bool MediaProfile::loadAttachment( Attachment& attachment ) const
  throw(data_access_error)
  {
    const shared_ptr<Media> media(attachment.getMedia());
    assert( media->hasId() );
    assert( media->hasOwner() );

    Value mediaId(media->getId());
    shared_ptr<Attachment> attachmentEntity( this->loadAttachmentInternal( mediaId, false ) );
    if ( attachmentEntity ) {
      attachment.setId( attachmentEntity->getId() );
      size_t size = attachmentEntity->getDataSize();
      attachment.setData( attachmentEntity->getData(), size );
      // generating the HMAC-SH1 key from the attachment data
      media->setHmac( Utilities::sign( attachment.getData(), size, media->getOwner().c_str() ) );
      return true;
    }
    return false;
  }
  
  shared_ptr<Media> MediaProfile::loadByGid( const string& gid ) const
  throw(data_access_error)
  {
    assert( ! gid.empty() );
    
    Value hidden(false);
    shared_ptr<Media> media(this->loadMediaInternal( Value(gid.c_str()), hidden )); 
    return media ? media : shared_ptr<Media>();
  }

  MemberRole MediaProfile::getAccessRole( integer mediaId, const char* username ) const throw(data_access_error)
  {
    const Value _mediaId(mediaId);
    const Value _username(username);
    unique_ptr<Value> result( db_.valueOf(Media::TABLE_NAME, "accessRole", &_mediaId, &_username, NULL) );
    return (MemberRole)result->num();
  }
  
  list<integer> MediaProfile::searchFullText( const string& pattern )
  throw(data_access_error)
  {
    // call entity finder
    Value regex( pattern );
    Value source( "MEDIA" );
    unique_ptr<ResultSet> values( this->db_.find(ChannelFullText::TABLE_NAME, "searchEntityFullText", &source, &regex, NULL) );
    list< integer > result;
    for (vector< BaseEntity* >::iterator it = values->begin(); it != values->end(); ++it) {
      ChannelFullText* channelFullText = static_cast< ChannelFullText* >( *it );
      result.push_back(channelFullText->getEntityId());
    }
    return result;
  }
  
  // ==========
  // Protected Interface
  // ==========
  
  shared_ptr<Media> MediaProfile::loadMediaInternal( const Value& gid, const Value& hidden ) const
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(Media::TABLE_NAME, "findMediaByGid", &gid, &hidden, NULL) );
    return shared_ptr<Media>(static_cast< Media* >( values->takeFirst() ) );
  }
  
  shared_ptr<ChannelMedia> MediaProfile::loadChannelMediaInternal( const Value& gid, const Value& hidden ) const
  {
    // call entity finder
    unique_ptr<ResultSet> values( db_.find(ChannelMedia::TABLE_NAME, "findChannelMediaByGid", &gid, &hidden, NULL) );
    return shared_ptr<ChannelMedia>(static_cast< ChannelMedia* >( values->takeFirst() ) );
  }
  
  shared_ptr<Attachment> MediaProfile::loadAttachmentInternal( const Value& mediaId, bool hidden ) const
  {
    // call entity finder
    unique_ptr<ResultSet> values = hidden ?
      db_.find(Attachment::TABLE_NAME, "findSlave", &mediaId, NULL) : db_.find(Attachment::TABLE_NAME, "findMaster", &mediaId, NULL);
    return shared_ptr<Attachment>(static_cast< Attachment* >( values->takeFirst() ) );
  }

}
