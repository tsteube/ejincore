/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "Media.h"
#include "UsingEjinTypes.h"

#include "Utilities.h"
#include "SqliteValue.h"
#include "RSACrypto.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

using ser::tag;
using ser::attr;
using ser::chardata;
using ser::endtag;


namespace ejin
{
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> AbstractMedia::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findMediaById"] = "ID = ? ORDER BY NO";
      tMap["findMediaByGid"] = "GID = ? AND (MASTER_ID IS NOT NULL) = ? ORDER BY NO";
      tMap["findAllMediaOfChannelId"] = "CHANNEL_ID = ? ORDER BY ID";
      tMap["findMediaByChannelId"] = "CHANNEL_ID = ? AND MASTER_ID IS NULL ORDER BY ID";
      tMap["findConflictedByChannelId"] = "CHANNEL_ID = ? AND SYNC_IND = 4 ORDER BY ID";
      tMap["findMediaByHeaderId"] = "CHANNEL_ID = ? AND POST_ID IS NULL AND COMMENT_ID IS NULL AND MASTER_ID IS NULL ORDER BY ID";
      tMap["findMediaByPostId"] = "POST_ID = ? AND COMMENT_ID IS NULL AND MASTER_ID IS NULL ORDER BY ID";
      tMap["findMediaByCommentId"] = "COMMENT_ID = ? AND MASTER_ID IS NULL ORDER BY ID";
      tMap["findChannelHeaderMedias"] = "CHANNEL_ID = ? AND POST_ID IS NULL AND COMMENT_ID IS NULL AND MASTER_ID IS NULL AND NO >= 0 ORDER BY NO";
      tMap["findAllChannelPostMedias"] = "CHANNEL_ID = ? AND POST_ID IS NOT NULL AND COMMENT_ID IS NULL AND MASTER_ID IS NULL ORDER BY NO";
      tMap["findAllChannelCommentMedias"] = "CHANNEL_ID = ? AND COMMENT_ID IS NOT NULL AND MASTER_ID IS NULL ORDER BY NO";
      tMap["findChannelPostMedias"] = "POST_ID = ? AND MASTER_ID IS NULL ORDER BY NO";
      tMap["findChannelCommentMedias"] = "COMMENT_ID = ? AND MASTER_ID IS NULL ORDER BY NO";
      tMap["findOutdatedAttachments"] = "MASTER_ID IS NULL and SYNC_IND not in ( 1, 2 ) AND ATTACH_OUTDATED = 1 ORDER BY ID";
      tMap["findAllOrderBySyncAnchorDesc"] = "MASTER_ID IS NULL ORDER BY sync_anchor DESC";
      tMap["BackupSet"] = "CHANNEL_ID = ?1 AND GID IS NOT NULL AND MASTER_ID IS NULL AND SYNC_IND != 1";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
      tMap["findMediaByGidBak"] = "GID = ?";
      tMap["BackupSetBak"] = "CHANNEL_ID = ?1 AND GID IS NOT NULL AND MASTER_ID IS NULL";
    }
    return tMap;
  }
  const map<string,string> AbstractMedia::sqlAliases( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      std::ostringstream ss;
      ss << "SELECT MAX(COALESCE(modified_by, owner)) "
      "FROM " << schema << ".media_tbl "
      "WHERE channel_id = ? AND modify_time = ? AND master_id IS NULL";
      tMap["lastModifiedBy"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT "
      "IFNULL((SELECT 3 from " << schema << ".media_hierarchy_view where media_id = ?1 AND "
      "?2 IN (channel_owner,post_owner,comment_owner,media_owner)), c.role) "
      "FROM " << schema << ".media_tbl m JOIN " << schema << ".channel_tbl c on (m.channel_id = c.id) where m.id = ?1";
      tMap["accessRole"] = ss.str();
      
      ss.str("");ss.clear();
      ss << "SELECT IFNULL(SUM(size), 0) "
      "FROM " << schema << ".media_tbl WHERE channel_id = ?1 AND attach_outdated = 1";
      tMap["outdatedChannelMediaSize"] = ss.str();
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  const string AbstractMedia::restoreSet( void ) const {
    return "CHANNEL_ID = ?1";
  }
  
  // ==========
  // Ctor / Dtor
  // ==========
  
  AbstractMedia::AbstractMedia( const AttributeSet& field, bool readonly ):
  Resource(field, readonly), channelGid_(type_int)
  {
    this->setRole( kSyncRoleRead );
    this->setSyncInd( kSyncIndInsert );
    this->setAttachmentUpdate( true );
    this->setAccessRole( kSyncRoleRead );
    this->setModificationMask((integer) 0);
  }
  AbstractMedia::AbstractMedia( const AttributeSet& field, const string& gid ): AbstractMedia(field, false)
  {
    setGid(gid);
  }
  AbstractMedia::AbstractMedia( const AbstractMedia& record ): Resource(record), channelGid_(type_int)
  {
    this->operator=(record);
  }
  AbstractMedia::~AbstractMedia( void ) { }
  
  // ==========
  // Object Interface
  // ==========
  
  // assignment
  AbstractMedia& AbstractMedia::operator=( const AbstractMedia& rhs ) {
    if (this != &rhs) {
      Resource::operator=(rhs);
      
      this->channelGid_ = rhs.channelGid_;
      COPY_PROPERTY((*this), rhs, Role);
      COPY_PROPERTY((*this), rhs, TransferKey);
      COPY_PROPERTY((*this), rhs, Hmac);
      COPY_PROPERTY((*this), rhs, Name);
      COPY_PROPERTY((*this), rhs, MimeType);
      COPY_PROPERTY((*this), rhs, Size);
      COPY_BOOL_PROPERTY((*this), rhs, AttachmentUpdate);
      
      this->accessRole_ = rhs.accessRole_;
    }
    return *this;
  }
  
  // comparison
  bool AbstractMedia::equals( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return true;
    if (! Resource::equals(rhs))
      return false;
    
    const AbstractMedia& mrhs = dynamic_cast<const AbstractMedia&> (rhs);
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() == mrhs.getGid();
    else if (this->hasId() && mrhs.hasId())
      return this->getId() == mrhs.getId();
    return false;
  }
  bool AbstractMedia::lessThan( const BaseEntity& rhs ) const {
    if (this == &rhs)
      return false;
    if (! Resource::lessThan(rhs))
      return false;
    
    const AbstractMedia& mrhs = dynamic_cast<const AbstractMedia&> (rhs); // throws std::bad_cast if not of type Media&
    if (this->hasGid() && mrhs.hasGid())
      return this->getGid() < mrhs.getGid();
    else if (this->hasId() && mrhs.hasId())
      return this->getId() < mrhs.getId();
    return false;
  }
  
  // ==========
  // IBusinessData Interface
  // ==========
  
  bool AbstractMedia::copyDataFrom( const BaseEntity& rhs )
  {
    if ( Resource::copyDataFrom(rhs) ) {
      const AbstractMedia& mrhs = dynamic_cast<const AbstractMedia&> (rhs);
      
      COPY_PROPERTY((*this), mrhs, Role);
      COPY_PROPERTY((*this), mrhs, TransferKey);
      COPY_PROPERTY((*this), mrhs, Hmac);
      COPY_PROPERTY((*this), mrhs, MimeType);
      COPY_PROPERTY((*this), mrhs, Size);
      COPY_PROPERTY((*this), mrhs, Name);
      COPY_PROPERTY((*this), mrhs, OwnerFullname);
      COPY_PROPERTY((*this), mrhs, ModifiedBy);
      COPY_PROPERTY((*this), mrhs, ModifiedByFullname);
      COPY_PROPERTY((*this), mrhs, ModifyTime);
      if (! this->hasCreateTime()) {
        COPY_PROPERTY((*this), mrhs, CreateTime);
      }
      COPY_BOOL_PROPERTY((*this), mrhs, Secure);
      COPY_BOOL_PROPERTY((*this), mrhs, Encrypted);
      COPY_PROPERTY((*this), mrhs, IV);
      
      return true;
    }
    return false;
  }
  bool AbstractMedia::hasEqualContent( const BaseEntity& arg ) const
  {
    if (this == &arg)
      return true;
    if (! Resource::hasEqualContent( arg ))
      return false;
    
    AbstractMedia& rhs( (AbstractMedia&)arg );
    
    switch ( this->getSyncInd() ) {
      case kSyncIndSynchronous:
        if ( rhs.getSyncInd() == kSyncIndInsert || rhs.getSyncInd() == kSyncIndDelete )
          return false;
        break;
      case kSyncIndDelete:
        if ( rhs.getSyncInd() != kSyncIndDelete )
          return false;
        break;
      case kSyncIndInsert:
        if ( rhs.getSyncInd() != kSyncIndInsert )
          return false;
        break;
      case kSyncIndUpdate:
      case kSyncIndConflict:
        break;
    }
    
    if ( this->getName() != rhs.getName() )
      return false;
    if ( this->getMimeType() != rhs.getMimeType() )
      return false;
    if ( this->getHmac() != rhs.getHmac() )
      return false;
    if ( this->getTransferKey() != rhs.getTransferKey() )
      return false;
    if ( this->getRole() != rhs.getRole() )
      return false;
    return true;
  }
  bool AbstractMedia::isDataEqualTo( const BaseEntity& rhs ) const
  {
    return Resource::isDataEqualTo( rhs );
  }
  void AbstractMedia::archive( void )
  {
    Resource::archive( );
  }
  void AbstractMedia::unarchive( void )
  {
    Resource::unarchive( );
    this->setAttachmentUpdate( true );
    this->clearTransferKey( );
  }
  void AbstractMedia::clearData( void )
  {
    Resource::clearData( );
    this->setRole( kSyncRoleRead );
    this->clearTransferKey( );
    this->clearHmac( );
    this->clearName( );
    this->clearMimeType( );
    this->clearSize( );
    this->clearCreateTime();
    this->clearModifyTime();
  }
  
  // ==========
  // Crypto Interface
  // ==========
  
  bool AbstractMedia::decrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if (this->isSecure()) {
      if ( crypto && crypto->hasSessionKey( ) ) {
        if (this->isEncrypted()) {
          if (this->hasName()) {
            this->setName( crypto->decrypt( this->getName() ) );
          }
          this->setSecure( true );
          this->setEncrypted( false );
          COPY_PROPERTY((*this), (*crypto), IV);
          return true;
        }
      } else {
        this->clearIV( );
      }
    } else {
      this->clearIV( );
    }
    return false;
  }
  
  bool AbstractMedia::encrypt( const RSACrypto* crypto ) throw(security_integrity_error)
  {
    if ( this->isSecure() ) {
      if ( crypto && crypto->hasSessionKey( ) ) {
        switch( this->getSyncInd() ) {
          case kSyncIndSynchronous:
            COPY_PROPERTY((*crypto), (*this), IV); // use old IV
            break;
          case kSyncIndInsert:
          case kSyncIndUpdate:
            crypto->createIV( ); // generate new IV
            break;
          case kSyncIndDelete:
          case kSyncIndConflict:
            assert( false ); // "invalid sync state"
            break;
        }
        if (this->hasName()) {
          this->setName( crypto->encrypt( this->getName() ) );
        }
        this->setEncrypted( true );
        COPY_PROPERTY((*this), (*crypto), IV);
        return true;
      } else {
        // do not publish unencrypted updates to the server in a secure channel
        _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, "no session key to encrypt message");
      }
    } else {
      this->setEncrypted( false );
      this->clearIV( );
    }
    return false;
  }
  
  // ==========
  // Serialisation Interface
  // ==========
  
  // JSON Mapping
  
  bool AbstractMedia::marshalJson( nlohmann::json& j ) const
  {
    if ( this->hasGid() ) {
      j["id"] = string(this->value("GID")->str(), this->value("GID")->size());
    }
    if ( this->hasName() ) {
      j["owner"] = string( this->value("OWNER")->str(), this->value("OWNER")->size() );
    }
    if ( this->hasMimeType() ) {
      j["mime_type"] = string( this->value("MIME_TYPE")->str(), this->value("MIME_TYPE")->size() );
    }
    if (this->hasTransferKey()) {
      j["key"] = string( this->value("TRANSFER_KEY")->str(), this->value("TRANSFER_KEY")->size() );
    }
    if (this->hasSize()) {
      integer size = this->value("SIZE")->num();
      if (this->isSecure()) {
        // protect encryption cipher by changing the public length of the raw text
        // will be overwritten on decryption later
        if (size > 10000) {
          j["size"] = (size / 1000 ) * 1000;
        } else if (size > 100) {
          j["size"] = (size / 10 ) * 10;
        } else {
          j["size"] = size + rand() % 10;
        }
      } else {
        j["size"] = size;
      }
    }
    if ( this->hasName() ) {
      j["name"] = string( this->value("NAME")->str(), this->value("NAME")->size() );
    }
    if (this->isEncrypted()) {
      j["encrypted"] = true;
    }
    
    return true;
  }
  /*
  bool AbstractMedia::marshalJson( json_t* node ) const
  {
    assert( node );
    
    if (this->hasGid()) {
      json_object_set_new( node, "id", json_stringn_nocheck( this->value("GID")->str(),
                                                             this->value("GID")->size() ) );
    }
    if (this->hasOwner()) {
      json_object_set_new( node, "owner", json_stringn_nocheck( this->value("OWNER")->str(),
                                                               this->value("OWNER")->size() ) );
    }
    if (this->hasMimeType()) {
      json_object_set_new( node, "mime_type", json_stringn_nocheck( this->value("MIME_TYPE")->str(),
                                                                   this->value("MIME_TYPE")->size() ) );
    }
    if (this->hasTransferKey()) {
      json_object_set_new( node, "key", json_stringn_nocheck( this->value("TRANSFER_KEY")->str(),
                                                             this->value("TRANSFER_KEY")->size() ) );
    }
    if (this->hasSize()) {
      integer size = this->value("SIZE")->num();
      json_object_set_new( node, "size", json_integer( (size / 1000 ) * 1000 ) );
    }
    if (this->hasName()) {
      json_object_set_new( node, "name", json_stringn_nocheck( this->value("NAME")->str(),
                                                              this->value("NAME")->size() ) );
    }
    if (this->isEncrypted()) {
      json_object_set_new( node, "encrypted", json_true( ) );
    }
    return true;
  }
  */

  bool AbstractMedia::unmarshalJson( const nlohmann::json& j )
  {
    if ( j.is_object() ) {
      if ( j.contains("id")) {
        this->value("GID")->str( j["id"] );
      }
      if ( j.contains("role")) {
        this->value("ROLE")->str( j["role"] );
      }
      if ( j.contains("channel_id")) {
        this->channelGid_.str( j["channel_id"] );
      }
      if ( j.contains("anchor")) {
        this->value("ANCHOR")->str( j["anchor"] );
      }
      if ( j.contains("owner")) {
        this->value("OWNER")->str( j["owner"] );
      }
      if ( j.contains("owner_fullname")) {
        this->value("OWNER_FULLNAME")->str( j["owner_fullname"] );
      }
      if ( j.contains("modified_by")) {
        this->value("MODIFIED_BY")->str( j["modified_by"] );
      }
      if ( j.contains("modified_by_fullname")) {
        this->value("MODIFIED_BY_FULLNAME")->str( j["modified_by_fullname"] );
      }
      if ( j.contains("key")) {
        this->value("TRANSFER_KEY")->str( j["key"] );
      }
      if ( j.contains("hmac")) {
        this->value("HMAC")->str( j["hmac"] );
      }
      if ( j.contains("name")) {
        this->value("NAME")->str( j["name"] );
      }
      if ( j.contains("encrypted")) {
        this->value("ENCRYPTED")->bol( j["encrypted"] );
        this->setSecure( this->isEncrypted() );
      }
      if ( j.contains("mime_type")) {
        this->value("MIME_TYPE")->str( j["mime_type"] );
      }
      if ( j.contains("size")) {
        this->value("SIZE")->num( j["size"] );
      }
      if ( j.contains("create_time")) {
        this->value("CREATE_TIME")->str( j["create_time"] );
      }
      if ( j.contains("modify_time")) {
        this->value("MODIFY_TIME")->str( j["modify_time"] );
      }

      return true;
    }
    
    return false;
  }
  /*
  bool AbstractMedia::unmarshalJson( json_t* node )
  {
    assert( node );
    
    json_t* value;
    value = json_object_get(node, "id");
    if ( json_is_string(value) ) {
      this->value("GID")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "role");
    if ( json_is_string(value) ) {
      this->value("ROLE")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "channel_id");
    if ( json_is_string(value) ) {
      channelGid_.bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "anchor");
    if ( json_is_string(value) ) {
      this->value("ANCHOR")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner");
    if ( json_is_string(value) ) {
      this->value("OWNER")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "owner_fullname");
    if ( json_is_string(value) ) {
      this->value("OWNER_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modified_by_fullname");
    if ( json_is_string(value) ) {
      this->value("MODIFIED_BY_FULLNAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "key");
    if ( json_is_string(value) ) {
      this->value("TRANSFER_KEY")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "hmac");
    if ( json_is_string(value) ) {
      this->value("HMAC")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "name");
    if ( json_is_string(value) ) {
      this->value("NAME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "encrypted");
    if ( json_is_string(value) ) {
      this->value("ENCRYPTED")->bytea( json_string_value(value), json_string_length(value) );
      this->setSecure( this->isEncrypted() );
    }
    value = json_object_get(node, "mime_type");
    if ( json_is_string(value) ) {
      this->value("MIME_TYPE")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "size");
    if ( json_is_number(value) ) {
      this->value("SIZE")->num( (integer) json_integer_value(value) );
    }
    value = json_object_get(node, "create_time");
    if ( json_is_string(value) ) {
      this->value("CREATE_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    value = json_object_get(node, "modify_time");
    if ( json_is_string(value) ) {
      this->value("MODIFY_TIME")->bytea( json_string_value(value), json_string_length(value) );
    }
    
    return true;
  }
  */

  // XML Mapping
  
  const string AbstractMedia::XML_NAME   = "media";
  const ser::XmlAttribute AbstractMedia::XML_FIELDS[] =
  {
    ser::XmlAttribute("id", "GID" ),
    ser::XmlAttribute("role", "ROLE" ),
    ser::XmlAttribute("anchor", "SYNC_ANCHOR" ),
    ser::XmlAttribute("owner", "OWNER" ),
    ser::XmlAttribute("owner_fullname", "OWNER_FULLNAME" ),
    ser::XmlAttribute("modified_by", "MODIFIED_BY" ),
    ser::XmlAttribute("modified_by_fullname", "MODIFIED_BY_FULLNAME" ),
    ser::XmlAttribute("key", "TRANSFER_KEY" ),
    ser::XmlAttribute("hmac", "HMAC" ),
    ser::XmlAttribute("name", "NAME" ),
    ser::XmlAttribute("mime_type", "MIME_TYPE" ),
    ser::XmlAttribute("size", "SIZE" ),
    ser::XmlAttribute("encrypted", "ENCRYPTED" ),
    ser::XmlAttribute("create_time", "CREATE_TIME" ),
    ser::XmlAttribute("modify_time", "MODIFY_TIME" ),
    ser::XmlAttribute()
  };
  const ser::XmlAttributeSet AbstractMedia::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  Value* AbstractMedia::valueByXmlNode( const char* name )
  {
    if ( strcmp("channel_id", name) == 0 ) {
      return &channelGid_;
    } else {
      return SerializableEntity::valueByXmlNode(name);
    }
  }
  void AbstractMedia::endXmlNode( const char* name )
  {
    if ( strcmp("media", name) == 0 ) {
      this->setSecure( this->isEncrypted() );
    }
  }
  
  bool AbstractMedia::marshalXml( XmlStream& xml, const char* const aesKey, size_t keyLength ) const
  {
    xml << tag("media"); // start element tag
    // serialize properties
    if (this->hasGid()) {
      xml << attr("id") << this->getGid();
    }
    if (this->isEncrypted()) {
      xml << attr("encrypted") << "true";
    }
    if (this->hasOwner()) {
      xml << attr("owner") << this->getOwner();
    }
    if (this->hasMimeType()) {
      xml << attr("mime_type") << this->getMimeType();
    }
    if (this->hasSize()) {
      xml << attr("size") << (this->getSize() / 1000 ) * 1000; /* rounded */
    }
    if (this->hasTransferKey()) {
      xml << tag("key") << chardata() << this->getTransferKey() << endtag();
    }
    if (this->hasName()) {
      xml << tag("name") << chardata() << this->getName() << endtag();
    }
    xml << ser::endtag(); // end element tag
    return true;
  }
  
  // ============================
  // Media Subclass
  //
  
  // ==========
  // Schema DDL
  // ==========
  // IMPORT NOTE: There are 2 mappings on MEDIA_TBL therefore we use different table names (lower/upper letter)!!!!
  template <> const Attribute DataSchema<Media>::TABLE_FIELDS[] =
  {
    Attribute("ID", type_int, flag_primary_key),
    Attribute("GID", type_text, flag_not_null),
    Attribute("NO", type_int, flag_not_null),
    Attribute("CHANNEL_ID", type_int, flag_none),
    Attribute("POST_ID", type_int, flag_none),
    Attribute("COMMENT_ID", type_int, flag_none),
    Attribute("OWNER", type_text, flag_not_null),
    Attribute("OWNER_FULLNAME", type_text, flag_none),
    Attribute("CREATE_TIME", type_time, flag_none),
    Attribute("MODIFY_TIME", type_time, flag_none),
    Attribute("SYNC_ANCHOR", type_text, flag_not_null, Value((integer)0)),
    Attribute("SYNC_IND", type_int, flag_not_null|flag_transient, Value((integer)0)),
    Attribute("ATTACH_OUTDATED", type_bool, flag_not_null),
    Attribute("MASTER_ID", type_int, flag_none),
    Attribute("MODIFICATION_MASK", type_int, flag_not_null|flag_transient),
    Attribute("SELECTED", type_bool, flag_not_null),
    Attribute("MODIFIED_BY", type_text, flag_none),
    Attribute("MODIFIED_BY_FULLNAME", type_text, flag_none),
    Attribute("SECURE", type_bool, flag_not_null, Value(false)),
    Attribute("ENCRYPTED", type_bool, flag_not_null, Value(false)),
    Attribute("IV", type_text, flag_none),
    Attribute("ROLE", type_int, flag_not_null, Value((integer)0), RoleEnumMapping),
    Attribute("MIME_TYPE", type_text, flag_none),
    Attribute("SIZE", type_int, flag_none),
    Attribute("TRANSFER_KEY", type_text, flag_none),
    Attribute("HMAC", type_text, flag_none),
    Attribute("NAME", type_text, flag_none),
    Attribute("ATTACH_UPDATE", type_bool, flag_not_null|flag_transient, Value((integer)1)),
    Attribute()
  };
  
  template <> const AttributeSet DataSchema<Media>::TABLE_DDL = AttributeSet(const_cast<Attribute*>(TABLE_FIELDS));
  
  Media::Media( void ) : AbstractMedia(TABLE_DDL, false) {};
  Media::Media( const string& gid ) : AbstractMedia( TABLE_DDL, gid ) {};
  Media::Media( const Media& record ) : AbstractMedia( record ) {};
  Media::Media( const MediaProfileView& record ) : AbstractMedia( record ) { };
  
  const char* Media::tableName( void ) const
  { return TABLE_NAME; }
  const AttributeSet& Media::tableFields( void ) const
  { return DataSchema<Media>::TABLE_DDL; }
  
  bool Media::copyDataFrom( const BaseEntity& rhs )
  {
    if (! AbstractMedia::copyDataFrom(rhs)) {
      const Resource& mrhs = dynamic_cast<const Resource&> (rhs);
      COPY_PROPERTY((*this), mrhs, CreateTime);
      return true;
    }
    return false;
  }
  
  // ============================
  // MediaProfileView Subclass
  //
  
  // ==========
  // Schema DDL
  // ==========
  template <> const Attribute DataSchema<MediaProfileView>::TABLE_FIELDS[] =
  {
    //Attribute("CHANNEL_ID", type_int, flag_not_null),
    Attribute("CHANNEL_GID", type_text, flag_not_null),
    Attribute("CHANNEL_ROLE", type_int, flag_not_null, Value((integer)0), RoleEnumMapping),
    Attribute("CHANNEL_MEMBERSHIP", type_int, flag_not_null, Value((integer)0), MembershipEnumMapping),
    Attribute("CHANNEL_SYNC_ANCHOR", type_text, flag_not_null),
    Attribute(),
  };
  template <> const AttributeSet DataSchema<MediaProfileView>::TABLE_DDL =
  AttributeSet(const_cast<Attribute*>(DataSchema<Media>::TABLE_FIELDS),
               const_cast<Attribute*>(DataSchema<MediaProfileView>::TABLE_FIELDS));
  
  // ==========
  // Finder Interface
  // ==========
  
  const map<string,string> MediaProfileView::finderTemplates( const char* schema ) const {
    map<string,string> tMap;
    if ( COMPARE_STRINGS( schema, mainSchemaName() ) || COMPARE_STRINGS( schema, archiveSchemaName() ) ) {
      tMap["findUpdatedAttachments"] = "GID NOT NULL AND CHANNEL_GID NOT NULL AND MASTER_ID IS NULL AND SYNC_IND IN ( 1, 2 ) and ATTACH_UPDATE = 1";
      tMap["findMediasByState"] = "MASTER_ID IS NULL AND GID IS NOT NULL AND SYNC_IND = ?";
      tMap["findConflictedMedias"] = "MASTER_ID IS NOT NULL";
    }
    if ( COMPARE_STRINGS( schema, backupSchemaName() ) ) {
    }
    return tMap;
  }
  
  MediaProfileView::MediaProfileView( void ) : AbstractMedia(TABLE_DDL, true) {};
  MediaProfileView::MediaProfileView( const string& gid ) : AbstractMedia( TABLE_DDL, gid ) {};
  MediaProfileView::MediaProfileView( const MediaProfileView& record ) : AbstractMedia( record ) {};
  
  const char* MediaProfileView::tableName( void ) const
  { return TABLE_NAME; }
  const AttributeSet& MediaProfileView::tableFields( void ) const
  { return DataSchema<MediaProfileView>::TABLE_DDL; }
  
}
