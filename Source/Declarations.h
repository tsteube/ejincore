/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_COMMON_H__
#define __EJIN_COMMON_H__

#include "Environment.h"

#include "SqliteAttribute.h"
#include "SqliteAttributeSet.h"

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
//#include <semaphore.h>

#include <stdexcept>
#include <assert.h>
#include <memory>

using std::shared_ptr;
using std::unique_ptr;
using std::string;
using std::map;
using std::pair;
using std::set;
using std::list;
using std::locale;
using std::string;
using std::vector;
using std::ostream;
using std::stringstream;
using std::ostringstream;

//#define MAKE_ITUNES_SCREENSHOTS

//#include <tr1/unordered_map>

#define DISALLOW_COPY_AND_ASSIGN_AND_COMPARE(TypeName) \
TypeName(const TypeName&); \
void operator=(const TypeName&); \
void operator==(const TypeName&) const;

#if defined(WIN32) || defined(_WIN32)
#define PATH_SEPARATOR "\\"
#elseif defined(__APPLE__)
#define PATH_SEPARATOR "/"
#else
#define PATH_SEPARATOR "/"
#endif

#define COMPARE_STRINGS( a, b ) (a != NULL && b != NULL && strcmp(a,b) == 0)

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {log_error(M, ##__VA_ARGS__); assert(A); }

/**
 * Common enumeration values
 */
namespace ejin {
  
  template<class T>
  struct DataSchema
  {
    // Database schema
    const static char* const               TABLE_NAME;
    const static sqlite::Attribute         TABLE_FIELDS[];
    const static sqlite::AttributeSet      TABLE_DDL;
    
    // XML schema
    //const static std::string               XML_NAME;
    //const static ser::XmlAttribute            XML_FIELDS[];
    //const static ser::XmlAttributeSet         XML_DDL;
  };
  
  /**
   * Different indicator for specific sub resource classes
   */
  typedef enum
  {
    kNoResource,
    kResourceTypeMedia,
    kResourceTypeChannelMedia
  } ResourceType;
  
  /**
   * Unique key names of all known synchronization source entities.
   */
  typedef enum
  {
    kSourceChannel, 
    kSourceMedia, 
    kSourceMember,
    kSourceChat,
    kSourceNull
  } SourceEntity;
  
  /**
   * Predefined synchronization state of local entities
   */ 
  typedef enum 
  {
    // The local and server reference entities are in sync.
    kSyncIndSynchronous = 0,
    // The entity has been added locally but is not synchronized yet.
    kSyncIndInsert,
    // The entity has been modified locally but is not synchronized yet.
    kSyncIndUpdate,
    // The entity has been deleted locally but is not synchronized yet.
    kSyncIndDelete,
    // The entity has been modified or deleted locally while the server reference entity was changed also in the
    // meantime. Now the server changes has been synchronized into the local store but the local changes conflicts with
    // the reread server changes. This conflict must be resolved by user interaction.
    kSyncIndConflict
  } SyncInd;

  /**
   * Predefined membership state of the local channel
   */ 
  typedef enum 
  {
    // The local and server membership are in sync.
    kMembershipIndInSync = 0,
    // Current user will accept channel membership on next sync
    kAcceptMemberIndShip,
    // Current user will reject channel membership on next sync.
    kRejetMemberIndShip,
  } MembershipInd;

  /**
   * Predefined synchronization operation codes.
   */
  typedef enum
  {
    kSyncNone = 0,
    // The entity must be added into the local persistence store.
    kSyncInsert,
    // The entity must be updated in the local persistence store.
    kSyncUpdate,
    // The entity must be deleted from the local persistence store.
    kSyncRemove
  } SyncOperation;

  /**
   * Predefined Membership constants.
   */
  typedef enum
  {
    // user has been invited
    kMembershipInvited = 0,
    // user has accepted channel membership
    kMembershipAccepted,
    // user has rejected channel membership
    kMembershipRejected,
  } Membership;

  /**
   * Predefined Membership constants.
   */
  typedef enum
  {
    // read access on channel
    kSyncRoleRead = 0,
    // write access on channel
    kSyncRoleWrite,
    // admin access on channel
    kSyncRoleAdmin,
    // owner
    kSyncRoleOwner,
  } MemberRole;  
  
  /**
   * Predefined trust level states
   */
  typedef enum
  {
    kTrustLevelUnapproved = 0,
    kTrustLevelIndirectlyApproved = 2,
    kTrustLevelApproved = 1,
    kTrustLevelIndirectlyApprovedOutdated = -2,
    kTrustLevelApprovedOutdated = -1,
  } TrustLevel;
  
  /**
   * Predefined duration period constants.
   */
  typedef enum
  {
    day = 0,
    week,
    month,
    year
  } DurationPeriod;
  
  /*
   * Predefined modification mask of entity
   */
  typedef enum {
    kNoneModification                          =   0,
    
    kEntityPendingLocalModification            =    1,
    kEntityRecentRemoteModification            =    2,
    kEntityConflictedModification              =    4,
    
    kChannelHeaderPendingLocalModification     = (  1 << 3 ),
    kChannelHeaderRecentRemoteModification     = (  2 << 3 ),
    kChannelHeaderConflictedModification       = (  4 << 3 ),
    
    kChannelMemberPendingLocalModification     = (  1 << 6),
    kChannelMemberRecentRemoteModification     = (  2 << 6 ),
    kChannelMemberConflictedModification       = (  4 << 6 ),
    
    kChannelPostPendingLocalModification       = (  1 << 9 ),
    kChannelPostRecentRemoteModification       = (  2 << 9 ),
    kChannelPostConflictedModification         = (  4 << 9 ),
    
    kChannelCommentPendingLocalModification    = (  1 << 12 ),
    kChannelCommentRecentRemoteModification    = (  2 << 12 ),
    kChannelCommentConflictedModification      = (  4 << 12 ),
    
    kMediaPendingLocalModification             = (  1 << 15 ),
    kMediaRecentRemoteModification             = (  2 << 15 ),
    kMediaConflictedModification               = (  4 << 15 ),
    
    kMemberPendingLocalModification            = (  1 << 18 ),
    kMemberRecentRemoteModification            = (  2 << 18 ),
    kMemberConflictedModification              = (  4 << 18 ),
    
    kPicturePendingLocalModification           = (  1 << 21 ),
    kPictureRecentRemoteModification           = (  2 << 21 ),
    kPictureConflictedModification             = (  4 << 21 ),
    
  } ModificationMask;

  /**
   * Unique key names of all known synchronization source entities.
   */
  typedef enum
  {
    kChangeLogSourceChannel,
    kChangeLogSourceChannelPost,
    kChangeLogSourceChannelComment,
    kChangeLogSourceMedia,
    kChangeLogSourceChannelMember,
    kChangeLogSourceMemberPicture,
    kChangeLogSourceMember,
    kChangeLogSourceMemberApproval,
    kChangeLogSourceChat,
    kChangeLogSourceChatMessage
  } ChangeLogSource;
  
  /**
   * Unique key names of all known synchronization operation entities.
   */
  typedef enum
  {
    kChangeLogOperationAdd,
    kChangeLogOperationUpdate,
    kChangeLogOperationOutdated,
    kChangeLogOperationRemove
  } ChangeLogOperation;
  
  // forward declarations

  class Utilities;
  class Database;
  class KeySequence;
  class SyncSource;
  class AESCrypto;
  class RSACrypto;
  class XmlMarshaller;
  class JsonMarshaller;
  class StringTemplate;
  class StringTemplate2;

  class Channel;
  class ChannelMedia;
  class ChannelMember;
  class ChannelProfile;
  class ChannelRepository;
  class ChannelService;

  class ChannelHeader;
  class ChannelHeaderProfile;
  class ChannelHeaderRepository;
  
  class ChannelComment;
  class ChannelCommentProfile;
  class ChannelCommentRepository;

  class ChannelPost;
  class ChannelPostProfile;
  class ChannelPostRepository;

  class Resource;
  class Media;
  class Attachment;
  class MediaProfile;
  class MediaRepository;
  class ChannelMediaRepository;
  class MediaService;
  
  class Member;
  class ApprovedMember;
  class ChannelMember;
  class MemberProfile;
  class MemberRepository;
  class ChannelMemberRepository;
  class ApprovedMemberRepository;
  
  class Picture;
  class PictureProfile;
  class PictureRepository;
  
  class User;
  class UserProfile;
  class UserRepository;
  class UserService;

  class Chat;
  class Message;
  class ChatProfile;
  class ChatRepository;
  class ChatService;

  class ChangeLog;
  class ChangeLogProfile;
  class ChangeLogRepository;

  // forward declaration
  template <typename T> class Modifications;
  template <typename T> class ResultList;
}

// forward declaration
namespace ser {
  class XmlAttribute;
  class XmlAttributeSet;
  class ISerializableEntity;
  class SerializableEntity;
}

#include "Exceptions.h"

#define NONE_MODIFICATION( mask ) (mask==kNoneModification)

#define CHANNEL_HEADER_MODIFICATION( mask ) \
 ((mask&(kChannelHeaderPendingLocalModification|kChannelHeaderRecentRemoteModification))!=kNoneModification)
#define CHANNEL_MEMBER_MODIFICATION( mask ) \
 ((mask&(kChannelMemberPendingLocalModification|kChannelMemberRecentRemoteModification))!=kNoneModification)
#define MEDIA_MODIFICATION( mask ) \
 ((mask&(kMediaPendingLocalModification|kMediaRecentRemoteModification))!=kNoneModification)
#define CHANNEL_POST_MODIFICATION( mask ) \
 ((mask&(kChannelPostPendingLocalModification|kChannelPostRecentRemoteModification))!=kNoneModification)
#define CHANNEL_COMMENT_MODIFICATION( mask ) \
 ((mask&(kChannelCommentPendingLocalModification|kChannelCommentRecentRemoteModification))!=kNoneModification)
#define MEMBER_MODIFICATION( mask ) \
 ((mask&(kMemberPendingLocalModification|kMemberRecentRemoteModification))!=kNoneModification)
#define MODIFICATION( mask ) \
 (CHANNEL_HEADER_MODIFICATION( mask ) || \
  CHANNEL_MEMBER_MODIFICATION( mask ) || \
           MEDIA_MODIFICATION( mask ) || \
    CHANNEL_POST_MODIFICATION( mask ) || \
 CHANNEL_COMMENT_MODIFICATION( mask ) || \
          MEMBER_MODIFICATION( mask ))

#define CHANNEL_HEADER_RECENT_REMOTE_MODIFICATION( mask ) \
 ((mask&kChannelHeaderRecentRemoteModification)==kChannelHeaderRecentRemoteModification)
#define CHANNEL_MEMBER_RECENT_REMOTE_MODIFICATION( mask ) \
 ((mask&kChannelMemberRecentRemoteModification)==kChannelMemberRecentRemoteModification)
#define MEDIA_RECENT_REMOTE_MODIFICATION( mask ) \
 ((mask&kMediaRecentRemoteModification)==kMediaRecentRemoteModification)
#define CHANNEL_POST_RECENT_REMOTE_MODIFICATION( mask ) \
 ((mask&kChannelPostRecentRemoteModification)==kChannelPostRecentRemoteModification)
#define CHANNEL_COMMENT_RECENT_REMOTE_MODIFICATION( mask ) \
 ((mask&kChannelCommentRecentRemoteModification)==kChannelCommentRecentRemoteModification)
#define MEMBER_RECENT_REMOTE_MODIFICATION( mask ) \
 ((mask&kMemberRecentRemoteModification)==kMemberRecentRemoteModification)
#define RECENT_REMOTE_MODIFICATION( mask ) \
 (CHANNEL_HEADER_RECENT_REMOTE_MODIFICATION( mask ) || \
  CHANNEL_MEMBER_RECENT_REMOTE_MODIFICATION( mask ) || \
           MEDIA_RECENT_REMOTE_MODIFICATION( mask ) || \
    CHANNEL_POST_RECENT_REMOTE_MODIFICATION( mask ) || \
 CHANNEL_COMMENT_RECENT_REMOTE_MODIFICATION( mask ) || \
          MEMBER_RECENT_REMOTE_MODIFICATION( mask ))
#define RECENT_REMOTE_CREATION( entity ) \
 (RECENT_REMOTE_MODIFICATION( entity.getModificationMask() ) && \
  entity.getCreateTime() == entity.getModifyTime())

#define CHANNEL_HEADER_PENDING_LOCAL_MODIFICATION( mask ) \
 ((mask&kChannelHeaderPendingLocalModification)==kChannelHeaderPendingLocalModification)
#define CHANNEL_MEMBER_PENDING_LOCAL_MODIFICATION( mask ) \
 ((mask&kChannelMemberPendingLocalModification)==kChannelMemberPendingLocalModification)
#define MEDIA_PENDING_LOCAL_MODIFICATION( mask ) \
 ((mask&kMediaPendingLocalModification)==kMediaPendingLocalModification)
#define CHANNEL_POST_PENDING_LOCAL_MODIFICATION( mask ) \
 ((mask&kChannelPostPendingLocalModification)==kChannelPostPendingLocalModification)
#define CHANNEL_COMMENT_PENDING_LOCAL_MODIFICATION( mask ) \
 ((mask&kChannelCommentPendingLocalModification)==kChannelCommentPendingLocalModification)
#define MEMBER_PENDING_LOCAL_MODIFICATION( mask ) \
 ((mask&kMemberPendingLocalModification)==kMemberPendingLocalModification)
#define PENDING_LOCAL_MODIFICATION( mask ) \
 (CHANNEL_HEADER_PENDING_LOCAL_MODIFICATION( mask ) || \
  CHANNEL_MEMBER_PENDING_LOCAL_MODIFICATION( mask ) || \
           MEDIA_PENDING_LOCAL_MODIFICATION( mask ) || \
    CHANNEL_POST_PENDING_LOCAL_MODIFICATION( mask ) || \
 CHANNEL_COMMENT_PENDING_LOCAL_MODIFICATION( mask ) || \
          MEMBER_PENDING_LOCAL_MODIFICATION( mask ))

#define CHANNEL_HEADER_CONFLICTED_MODIFICATION( mask ) \
 ((mask&kChannelHeaderConflictedModification)==kChannelHeaderConflictedModification)
#define CHANNEL_MEMBER_CONFLICTED_MODIFICATION( mask ) \
 ((mask&kChannelMemberConflictedModification)==kChannelMemberConflictedModification)
#define MEDIA_CONFLICTED_MODIFICATION( mask ) \
 ((mask&kMediaConflictedModification)==kMediaConflictedModification)
#define CHANNEL_POST_CONFLICTED_MODIFICATION( mask ) \
 ((mask&kChannelPostConflictedModification)==kChannelPostConflictedModification)
#define CHANNEL_COMMENT_CONFLICTED_MODIFICATION( mask ) \
 ((mask&kChannelCommentConflictedModification)==kChannelCommentConflictedModification)
#define MEMBER_CONFLICTED_MODIFICATION( mask ) \
 ((mask&kMemberConflictedModification)==kMemberConflictedModification)
#define CONFLICTED_MODIFICATION( mask ) \
 (CHANNEL_HEADER_CONFLICTED_MODIFICATION( mask ) || \
  CHANNEL_MEMBER_CONFLICTED_MODIFICATION( mask ) || \
           MEDIA_CONFLICTED_MODIFICATION( mask ) || \
    CHANNEL_POST_CONFLICTED_MODIFICATION( mask ) || \
 CHANNEL_COMMENT_CONFLICTED_MODIFICATION( mask ) || \
          MEMBER_CONFLICTED_MODIFICATION( mask ))

#define CHANNEL_HEADER_CONCURRENT_MODIFICATION( mask ) \
 (CHANNEL_HEADER_RECENT_REMOTE_MODIFICATION(mask) && CHANNEL_HEADER_PENDING_LOCAL_MODIFICATION(mask))
#define CHANNEL_MEMBER_CONCURRENT_MODIFICATION( mask ) \
 (CHANNEL_MEMBER_RECENT_REMOTE_MODIFICATION(mask) && CHANNEL_MEMBER_PENDING_LOCAL_MODIFICATION(mask))
#define MEDIA_CONCURRENT_MODIFICATION( mask ) \
 (MEDIA_RECENT_REMOTE_MODIFICATION(mask) && MEDIA_PENDING_LOCAL_MODIFICATION(mask))
#define CHANNEL_POST_CONCURRENT_MODIFICATION( mask ) \
 (CHANNEL_POST_RECENT_REMOTE_MODIFICATION(mask) && CHANNEL_POST_PENDING_LOCAL_MODIFICATION(mask))
#define CHANNEL_COMMENT_CONCURRENT_MODIFICATION( mask ) \
 (CHANNEL_COMMENT_RECENT_REMOTE_MODIFICATION(mask) && CHANNEL_COMMENT_PENDING_LOCAL_MODIFICATION(mask))
#define MEMBER_CONCURRENT_MODIFICATION( mask ) \
 (MEMBER_RECENT_REMOTE_MODIFICATION(mask) && MEMBER_PENDING_LOCAL_MODIFICATION(mask))
#define CONCURRENT_MODIFICATION( mask ) \
 (RECENT_REMOTE_MODIFICATION(mask) && PENDING_LOCAL_MODIFICATION(mask))

#endif //__EJIN_COMMON_H__
