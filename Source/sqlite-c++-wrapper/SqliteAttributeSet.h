/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_ATTRIBUTE_SET_H__
#define __SQLITE_ATTRIBUTE_SET_H__

#include "SqliteCommon.h"

namespace sqlite
{

  class AttributeSet
  {
    
    // ==========
    // CTOR
    // ==========
  public:
    
    AttributeSet( Attribute* definitions );
    AttributeSet( Attribute* definitions1, Attribute* definitions2 );
    AttributeSet( const vector<Attribute>& definition );
    AttributeSet( const AttributeSet& source );
    
    bool operator==(const AttributeSet& rhs) const;
    bool operator!=(const AttributeSet& rhs) const {
      return ! (*this == rhs);
    }
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    const string toString(void) const;
    int count(void) const
    { return (int) this->vec_.size(); }    
    const Attribute* byName(string name) const
    { return this->map_[name]; }
    const Attribute* byIndex(int index) const
    {
      if ((index >= 0) && (index < count())) {
        const Attribute* a = &this->vec_[index];
        return a;
      }
      return NULL;
    }    
    
    // ==========
    // Private Interface
    // ==========    
  private:
    
    vector<Attribute> vec_;
    mutable map<string, Attribute*> map_;

    void init( Attribute* definitions );
    void copy( const vector<Attribute>& definition );
    
  };

}
#endif //__SQLITE_ATTRIBUTE_SET_H__