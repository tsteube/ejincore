/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteAttribute.h"
#include "SqliteValue.h"

namespace sqlite
{
  // ==========
  // CTOR
  // ==========
  Attribute::Attribute():
  name_(), use_(FIELD_DEFAULT), type_(type_undefined), index_(0), flags_(0), mapping_(NULL), defValue_()
  { }
  Attribute::Attribute(string nam, field_type typ, int flags, const Value& defValue, const char* map[]):
  name_(), use_(FIELD_DEFAULT), type_(type_undefined), index_(0), flags_(0), mapping_(map), defValue_(defValue)
  {
    this->name_ = nam;
    this->type_ = typ;
    this->index_ = -1;
    this->flags_ = flags;
    if (this->isPrimaryKey())
      this->use_ = FIELD_KEY;
    if (! this->isNullable())
      this->use_ = FIELD_DEFAULT;
  }
  Attribute::Attribute(const Attribute& value):
  name_(value.name_), use_(value.use_), type_(value.type_), index_(value.index_), flags_(value.flags_), mapping_(value.mapping_), defValue_(value.defValue_)
  { }
  Attribute& Attribute::operator=(const Attribute& rhs)
  {
    if (this != &rhs) {
      this->name_ = rhs.name_;
      this->use_ = rhs.use_;
      this->type_ = rhs.type_;
      this->index_ = rhs.index_;
      this->flags_ = rhs.flags_;
      this->mapping_ = rhs.mapping_;
    }
    return *this;
  }

  bool Attribute::operator==(const Attribute& rhs) const 
  {
    if (this == &rhs)
      return true;

    if (this->name_ != rhs.name_)
      return false;
    if (this->use_ != rhs.use_)
      return false;
    if (this->type_ != rhs.type_)
      return false;
    if (this->index_ != rhs.index_)
      return false;
    if (this->flags_ != rhs.flags_)
      return false;

    return true;
  }  
  
}
