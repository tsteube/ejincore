/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqlitePreparedTable.h"

#include "SqliteDatabase.h"

#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <assert.h>

#include "SqliteTable.h"
#include "SqliteValue.h"
#include "DebugLog.h"

const int PRIMARY_KEY_SQL_PARAMETER_OFFSET = 99;

// static variables
namespace sqlite
{
  using std::stringstream;
  
  // ==========
  // CTOR
  // ==========
  
  PreparedTable::PreparedTable( const Database& db, BaseEntity* proto ):
  db_(db), prototype_(proto), numberOfPreparedStatements(0), preparedRestoreStatement1_(NULL), preparedRestoreStatement2_(NULL)
  {
  }
  PreparedTable::~PreparedTable( void )
  {
    DEBUG_METHOD();
    // free release prepared statements
    vector<string> schemas;
    for(auto iter = preparedSelectStatement_.begin(); iter != preparedSelectStatement_.end(); ++iter) {
      schemas.push_back(iter->first);
    }
    for(auto iter = schemas.begin(); iter != schemas.end(); ++iter) {
      clearAllStatementsOn( iter->c_str() );
    }
    assert( numberOfPreparedStatements == 0 );
  }
  void PreparedTable::prepareAllStatementsOn( const char* schema )
  {
    if ( schema ) {
      this->clearAllStatementsOn( schema );
      this->preparedSelectStatement_[schema] = prepareSelectStatement( schema, "" );
      this->preparedCountStatement_[schema] = prepareCountStatement( schema );
      if (! prototype_->isReadOnly()) {
        if ( prototype_->useSequenceTable() ) {
          this->preparedSequenceStatement_[schema] = prepareSequenceStatement( schema );
        }
        this->preparedInsertStatement_[schema] = prepareInsertStatement( schema );
        this->preparedUpdateStatement_[schema] = prepareUpdateStatement( schema );
        this->preparedDeleteStatement_[schema] = prepareDeleteStatement( schema );
        this->preparedDeleteAllStatement_[schema] = prepareDeleteAllStatement( schema );
        if ( backupSchemaName() != NULL && strcmp(schema, backupSchemaName()) == 0 ) {
          this->preparedRestoreStatement1_ = prepareRestoreStatement1( prototype_->restoreSet() );
          this->preparedRestoreStatement2_ = prepareRestoreStatement2( prototype_->restoreSet(), prototype_->restoreSet() );
        }
      }
      
      // register finder statement
      map<string,string> finderMap = prototype_->finderTemplates(schema);
      this->preparedFinderStatement_[schema] = map<string,sqlite3_stmt*>();
      for( map<string,string>::iterator ii=finderMap.begin(); ii!=finderMap.end(); ++ii)
      {
        this->preparedFinderStatement_[schema][(*ii).first] = prepareSelectStatement( schema, (*ii).second );
      }
      
      // register alias statements
      map<string,string> sqlAliasMap = prototype_->sqlAliases(schema);
      this->preparedSqlAliasStatement_[schema] = map<string,sqlite3_stmt*>();
      for( map<string,string>::iterator ii=sqlAliasMap.begin(); ii!=sqlAliasMap.end(); ++ii)
      {
        this->preparedSqlAliasStatement_[schema][(*ii).first] = prepareStatement( (*ii).second );
      }
    }
  }
  void PreparedTable::clearAllStatementsOn( const char* schema )
  {
    eraseStatementOn( this->preparedSelectStatement_, schema );
    eraseStatementOn( this->preparedCountStatement_, schema );
    eraseStatementOn( this->preparedSequenceStatement_, schema );
    eraseStatementOn( this->preparedInsertStatement_, schema );
    eraseStatementOn( this->preparedUpdateStatement_, schema );
    eraseStatementOn( this->preparedDeleteStatement_, schema );
    eraseStatementOn( this->preparedDeleteAllStatement_, schema );
    eraseStatementOn( this->preparedFinderStatement_, schema );
    eraseStatementOn( this->preparedSqlAliasStatement_, schema );
    
    if ( schema == NULL || (backupSchemaName() != NULL && strcmp(schema, backupSchemaName()) == 0) ) {
      clearStmt( this->preparedRestoreStatement1_ );
      this->preparedRestoreStatement1_ = NULL;
      clearStmt( this->preparedRestoreStatement2_ );
      this->preparedRestoreStatement2_ = NULL;
    }
  }
  void PreparedTable::eraseStatementOn( map<string,sqlite3_stmt*>& statements, const char* schema ) {
    if ( schema ) {
      auto it = statements.find( schema );
      if ( it != statements.end() ) {
        sqlite3_finalize( it->second ); numberOfPreparedStatements--;
        statements.erase( it );
      }
    } else {
      for( auto it=statements.begin(); it!=statements.end(); ++it )
      {
        sqlite3_finalize( it->second ); numberOfPreparedStatements--;
      }
      statements.clear( );
    }
  }
  void PreparedTable::eraseStatementOn( map<string,map<string,sqlite3_stmt*>>& statements, const char* schema ) {
    if ( schema ) {
      auto it = statements.find( schema );
      if ( it != statements.end() ) {
        for( auto ii=it->second.begin(); ii!=it->second.end(); ++ii )
        {
          assert((*ii).second);
          sqlite3_finalize((*ii).second); numberOfPreparedStatements--;
        }
        it->second.clear();
        statements.erase( it );
      }
    } else {
      for( auto it=statements.begin(); it!=statements.end(); ++it )
      {
        for( auto ii=it->second.begin(); ii!=it->second.end(); ++ii )
        {
          assert((*ii).second);
          sqlite3_finalize((*ii).second); numberOfPreparedStatements--;
        }
        it->second.clear();
      }
      statements.clear( );
    }
  }
  void PreparedTable::clearStmt( sqlite3_stmt* stmt ) {
    if ( stmt ) {
      sqlite3_finalize( stmt ); numberOfPreparedStatements--;
      stmt = NULL;
    }
  }
  string PreparedTable::toString() const
  {
    std::stringstream buf;
    buf << this->name() << "["
    << "open=" << this->db_.isOpen();
    if (this->prototype_->isReadOnly())
      buf << ",ro]";
    else
      buf << ",rw]";
    return buf.str();
  }
  
  // ==========
  // Private Methods: 1. level - Mapping
  // ==========
  
  integer PreparedTable::nextSequence( const char* schema ) const
  {
    DEBUG_METHOD();
    assert( schema );
    // select entity record
    sqlite3_stmt* stmt = preparedSequenceStatement_.at( schema );
    assert( stmt );
    
    DEBUG_PRINT("%s: executing sql %s",
                this->name(),
                sqlite3_sql( stmt ));
    EXEC_SQLITE_STATEMENT (sqlite3_reset (stmt), db_.handle());
    integer result = 0;
    bool found = false;
    int resultCode;
    while ((resultCode=sqlite3_step (stmt)) == SQLITE_ROW) { // read all from db counter
      if (! found) {
        switch ( sqlite3_column_type( stmt, 0) ) {
          case SQLITE_INTEGER:
            result = (integer) sqlite3_column_int64( stmt, 0 );
            break;
          default:
            ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid sequence data type");
        }
        found = true;
      } else {
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid number of result rows");
      }
    }
    switch (resultCode) {
      case SQLITE_DONE:
        return result;
      default:
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION,"Next Sequence in database: %s", sqlite3_errmsg(handle()));
        return result;
    }
  }
  
  // read all entity fields from the sqlite statement
  BaseEntity& PreparedTable::readEntityValues(sqlite3_stmt* stmt,
                                              BaseEntity& entity )
  {
    vector< pair<field_type,Value*> > values;
    for (int index = 0; index < entity.fields().count(); index++)
    {
      if (const Attribute* field = entity.fields().byIndex(index))
      {
        values.push_back( pair<field_type,Value*>(field->type(),entity.value(field->name())) );
      }
    }
    // read entities values
    readValuesFromStmt(stmt, 0, values);
    return entity;
  }
  
  int PreparedTable::readValuesFromStmt(sqlite3_stmt* stmt,
                                        int offset,
                                        vector< pair<field_type,Value*> > values )
  {
    DEBUG_METHOD();
    
    int paramIndex = offset;
    vector< pair<field_type,Value*> >::iterator it;
    for (it=values.begin(); it!=values.end(); it++) {
      pair<field_type,Value*>  value = *it;
      
      switch ( sqlite3_column_type( stmt, paramIndex) ) {
        case SQLITE_FLOAT:
        case SQLITE_INTEGER:
        case SQLITE_TEXT:
        case SQLITE_BLOB:
          switch (value.first) {
            case  type_int:
              value.second->num( (integer) sqlite3_column_int64(stmt, paramIndex) );
              DEBUG_PRINT("set %d. prop = %d",
                          paramIndex,
                          value.second->num());
              break;
            case  type_time:
#if defined(__i386__) || defined(__ppc__) || defined(__arm__)
              value.second->num( (integer) sqlite3_column_int64(stmt, paramIndex) ); // convert from seconds to millis from epoch
              DEBUG_PRINT("set %d. prop = %ld", paramIndex, value.second->num());
#else
#if defined(__x86_64__) || defined(__ppc64__) || defined(__arm64__)
              value.second->num( (integer) sqlite3_column_int64(stmt, paramIndex) * 1000 ); // convert from seconds to millis from epoch
              DEBUG_PRINT("xset %d. prop = %ld", paramIndex, value.second->num());
#else
#error UNKNOWN ARCHITECTURE
#endif
#endif
              break;
            case  type_bool:
              value.second->bol(sqlite3_column_int(stmt, paramIndex) != 0);
              DEBUG_PRINT("set %d. prop = %d",
                          paramIndex,
                          value.second->bol());
              break;
            case  type_float:
              value.second->dbl( sqlite3_column_double(stmt, paramIndex) );
              DEBUG_PRINT("set %d. prop = %lf",
                          paramIndex,
                          value.second->dbl());
              break;
            case  type_text:
              value.second->bytea( reinterpret_cast<const char*>(sqlite3_column_text(stmt, paramIndex)),
                                  sqlite3_column_bytes(stmt, paramIndex) / sizeof(char) );
              DEBUG_PRINT("set %d. prop = %s(%d)",
                          paramIndex,
                          value.second->str(),
                          value.second->size());
              break;
            case  type_bytea:
              value.second->bytea( reinterpret_cast<const char*>(sqlite3_column_blob(stmt, paramIndex)),
                                  sqlite3_column_bytes(stmt, paramIndex) / sizeof(char) );
              DEBUG_PRINT("set %d. prop = bytes(%d)",
                          paramIndex,
                          value.second->size());
              break;
            default:
              ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid output data type: %d", value.first);
          }
          break;
        case SQLITE_NULL:
          value.second->setNull();
          DEBUG_PRINT("set %d. prop = NULL", paramIndex);
          break;
      }
      paramIndex++;
    }
    return paramIndex - offset;
  }
  
  // clears the statement first
  // bind all entity fields to the sqlite statement
  sqlite3_stmt* PreparedTable::bindEntityValues(sqlite3_stmt* stmt,
                                                const char* schema,
                                                BaseEntity& entity,
                                                vector<Value*> k,
                                                bool keys,
                                                bool props)
  {
    DEBUG_METHOD();
    EXEC_SQLITE_STATEMENT (sqlite3_reset (stmt), db_.handle());
    EXEC_SQLITE_STATEMENT (sqlite3_clear_bindings (stmt), db_.handle());
    
    vector<Value*>::iterator keysIt = k.begin();
    vector< pair<field_type,const Value*> > allKeys;
    vector< pair<field_type,const Value*> > allValues;
    for (int index = 0; index < fields().count(); index++)
    {
      if (const Attribute* field = fields().byIndex(index))
      {
        if (field->isPrimaryKey()) {
          if (keysIt != k.end()) {
            Value* v(*keysIt++);
            if (v->isNull() && entity.useSequenceTable())
              *v = nextSequence( schema ); // primary key sequence
            allKeys.push_back( pair<field_type,const Value*>(field->type(),v) );
          } else {
            allKeys.push_back( pair<field_type,const Value*>(field->type(),NULL) );
          }
        } else {
          allValues.push_back( pair<field_type,const Value*>(field->type(),entity.value(index)) );
        }
      }
    }
    
    DEBUG_PRINT("%s: bind prepared statement %s", this->name(), sqlite3_sql(stmt));
    if (keys) {
      // bind entity unique identifier
      bindValuesToStmt(stmt, PRIMARY_KEY_SQL_PARAMETER_OFFSET + 1, allKeys);
    }
    if (props) {
      // bind entities values
      bindValuesToStmt(stmt, 1, allValues);
    }
    return stmt;
  }
  int PreparedTable::bindValuesToStmt(sqlite3_stmt* stmt,
                                      int offset,
                                      vector< pair<field_type,const Value*> > values )
  {
    DEBUG_METHOD();
    int paramIndex = offset;
    vector< pair<field_type,const Value*> >::iterator it;
    for (it=values.begin(); it!=values.end(); it++) {
      pair<field_type,const Value*>  value = *it;
      if (! value.second->isNull()) {
        switch (value.first) {
          case  type_int:
            EXEC_SQLITE_STATEMENT(sqlite3_bind_int64(stmt, paramIndex, value.second->num()), db_.handle());
            DEBUG_PRINT("set var ?%d: %d", paramIndex, value.second->num());
            break;
          case  type_text:
            EXEC_SQLITE_STATEMENT(sqlite3_bind_text (stmt, paramIndex, value.second->str(),
                                                     int (sizeof(char) * value.second->size()), NULL), db_.handle());
            DEBUG_PRINT("set var ?%d: %s", paramIndex, value.second->str());
            break;
          case  type_bytea:
            EXEC_SQLITE_STATEMENT(sqlite3_bind_blob (stmt, paramIndex, value.second->str(),
                                                     int (sizeof(char) * value.second->size()), NULL), db_.handle());
            DEBUG_PRINT("set var ?%d: #%d", paramIndex, value.second->size());
            break;
          case  type_float:
            EXEC_SQLITE_STATEMENT(sqlite3_bind_double(stmt, paramIndex, value.second->dbl()), db_.handle());
            DEBUG_PRINT("set var ?%d: %lf", paramIndex, value.second->dbl());
            break;
          case  type_bool:
            EXEC_SQLITE_STATEMENT(sqlite3_bind_int(stmt, paramIndex, value.second->bol() ? 1 : 0 ), db_.handle());
            DEBUG_PRINT("set var ?%d: %d", paramIndex, value.second->bol());
            break;
          case  type_time:
#if defined(__i386__) || defined(__ppc__) || defined(__arm__)
            EXEC_SQLITE_STATEMENT( sqlite3_bind_int64( stmt, paramIndex, value.second->num() ), db_.handle() ); // save seconds since epoch
            DEBUG_PRINT("set var ?%d: %ld", paramIndex, value.second->num());
#else
#if defined(__x86_64__) || defined(__ppc64__) || defined(__arm64__)
            EXEC_SQLITE_STATEMENT( sqlite3_bind_int64( stmt, paramIndex, value.second->num() / 1000. ), db_.handle() ); // save seconds since epoch
            DEBUG_PRINT("xset var ?%d: %ld", paramIndex, value.second->num());
#else
#error UNKNOWN ARCHITECTURE
#endif
#endif
            break;
          default:
            ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid input data type: %d", value.first);
        }

      } else {
        EXEC_SQLITE_STATEMENT(sqlite3_bind_null (stmt, paramIndex), db_.handle());
        DEBUG_PRINT("set var ?%d: NULL", paramIndex);
      }
      paramIndex++;
    }
    return paramIndex - offset;
  }
  
  // ==========
  // Prepare SQLITE statments
  // ==========
  
  sqlite3_stmt* PreparedTable::prepareSequenceStatement( const string& schema ) const
  {
    DEBUG_METHOD();
    std::stringstream sql;
    std::string key( name() );
    std::transform(key.begin(), key.end(), key.begin(),
                   [](unsigned char c) -> unsigned char { return std::tolower(c); });
    sql << "SELECT gen_value FROM " << schema << ".id_gen WHERE gen_key = '" << key << "'";
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareInsertStatement( const string& schema ) const
  {
    DEBUG_METHOD();
    int pkNo = 1;
    int fieldParam = 1;
    stringstream fieldNames;
    stringstream fieldValues;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (index > 0) {
          fieldNames << ", ";
          fieldValues << ", ";
        }
        fieldNames << field->name();
        if (field->isPrimaryKey())
          fieldValues << "?" << (PRIMARY_KEY_SQL_PARAMETER_OFFSET + pkNo++);
        else
          fieldValues << "?" << fieldParam++;
      }
    }
    
    stringstream sql;
    sql << "INSERT INTO " << schema << "." << name() << " (" << fieldNames.str() << ") VALUES (" << fieldValues.str() + ")";
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareUpdateStatement( const string& schema ) const
  {
    DEBUG_METHOD();
    stringstream sql;
    sql << "UPDATE " << schema << "." << name() << " SET ";
    
    int pkNo = 1;
    int fieldParam = 1;
    stringstream whereCondition;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (field->isKeyIdField()) {
          if (pkNo > 1)
            whereCondition << "AND";
          whereCondition << " " << field->name() << " = ?" << (PRIMARY_KEY_SQL_PARAMETER_OFFSET + pkNo++);
          continue;
        }
        
        if (fieldParam > 1)
          sql << ", ";
        sql << field->name() << " = ?" << fieldParam++;
      }
    }
    sql << " WHERE " << whereCondition.str();
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement( sql.str() );
  }
  sqlite3_stmt* PreparedTable::prepareSelectStatement( const string& schema, const string& condition ) const
  {
    DEBUG_METHOD();
    bool createPkCondition = condition.empty();
    
    stringstream sql;
    sql << "SELECT ";
    
    int pkNo = 1;
    stringstream whereCondition;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        string n = field->name();
        if (createPkCondition && field->isKeyIdField()) {
          if (pkNo > 1)
            whereCondition << " AND ";
          whereCondition << " " << n << " = ?" << (PRIMARY_KEY_SQL_PARAMETER_OFFSET + pkNo++);
        }
        
        if (index > 0)
          sql << ", ";
        sql << n;
      }
    }
    sql << " FROM " << schema << "." << name();
    if (createPkCondition) {
      sql << " WHERE " << whereCondition.str();
    } else {
      sql << " WHERE " << condition;
    }
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareDeleteStatement( const string& schema ) const
  {
    DEBUG_METHOD();
    stringstream sql;
    sql << "DELETE FROM " << schema << "." <<name() << " WHERE ";
    
    int pkNo = 1;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (field->isKeyIdField()) {
          if (pkNo > 1)
            sql << "AND";
          sql << " " << field->name() << " = ?" << (PRIMARY_KEY_SQL_PARAMETER_OFFSET + pkNo++);
        }
      }
    }
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement( sql.str() );
  }
  sqlite3_stmt* PreparedTable::prepareDeleteAllStatement( const string& schema ) const
  {
    DEBUG_METHOD();
    stringstream sql;
    sql << "DELETE FROM " << schema << "." <<name();
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement( sql.str().c_str() );
  }
  sqlite3_stmt* PreparedTable::prepareCountStatement( const string& schema ) const
  {
    DEBUG_METHOD();
    stringstream sql;
    sql << "SELECT count(*) FROM " << schema << "." <<name();
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement( sql.str().c_str() );
  }
  sqlite3_stmt* PreparedTable::prepareBackupStatement1( const string& condition ) const {
    DEBUG_METHOD();
    
    stringstream columns;
    const AttributeSet fs(fields());
    bool list = false;
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (! field->isTransient()) {
          if (list) {
            columns << ", ";
          }
          list = true;
          columns << field->name();
        }
      }
    }
    
    stringstream sql;
    sql << "INSERT OR REPLACE INTO " << this->backupSchemaName() << "." << name() << " ( " <<
    columns.str() << " ) SELECT " << columns.str() << " FROM main." << name();
    if (! condition.empty()) {
      sql << " WHERE " << condition;
    }
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareBackupStatement2( const string& condition1, const string& condition2 ) const {
    DEBUG_METHOD();
    
    string pkey;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (field->isPrimaryKey()) {
          pkey = field->name();
          break;
        }
      }
    }

    stringstream sql;
    sql << "DELETE FROM " << this->backupSchemaName() << "." << name() << " WHERE ";
    if (! condition2.empty()) {
      sql << condition2 << " AND ";
    }
    sql << pkey << " NOT IN " <<
    "(SELECT " << pkey << " FROM main." << name();
    if (! condition1.empty()) {
      sql << " WHERE " << condition1;
    }
    sql << ")";
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::preparedCutBackupStatement( ) const {
    DEBUG_METHOD();
    stringstream sql;
    
    string pkey;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (field->isPrimaryKey()) {
          if ( pkey.empty() ) {
            pkey = field->name();
          } else {
            // more than one key -> skip
            return NULL;
          }
        }
      }
    }
    
    sql << "DELETE FROM " << this->backupSchemaName() << "."  << name() << " WHERE (" << pkey << ") NOT IN "
    "(SELECT " << pkey << " FROM " << this->mainSchemaName() << "." << name() << ")";
    
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareRestoreStatement1( const string& condition ) const {
    DEBUG_METHOD();
    stringstream sql;
    sql << "INSERT OR REPLACE INTO main." << name() << " SELECT * " << " FROM " << this->backupSchemaName() << "." << name();
    if (! condition.empty()) {
      sql << " WHERE " << condition;
    }
    
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareRestoreStatement2( const string& condition1, const string& condition2 ) const {
    DEBUG_METHOD();
    
    string pkey;
    const AttributeSet fs(fields());
    for (int index = 0; index < fs.count(); index++)
    {
      if (const Attribute* field = fs.byIndex(index))
      {
        if (field->isPrimaryKey()) {
          pkey = field->name();
          break;
        }
      }
    }
    
    stringstream sql;
    sql << "DELETE FROM main." << name() << " WHERE ";
    if (! condition1.empty()) {
      sql << condition1 << " AND ";
    }
    sql << "(" << pkey << ") NOT IN " <<
    "(SELECT " << pkey << " FROM " << this->backupSchemaName() << "." << name();
    if (! condition2.empty()) {
      sql << " WHERE " << condition2;
    }
    sql << ")";
    DEBUG_PRINT("%s: prepare statement '%s'", name(), sql.str().c_str());
    return prepareStatement(sql.str());
  }
  sqlite3_stmt* PreparedTable::prepareStatement( const string& sql ) const {
    sqlite3_stmt *stmt;
    char* c_str = Value::strdup(sql);
    int result = sqlite3_prepare_v2(handle(), c_str, -1, &stmt, NULL);
    free(c_str);
    EXEC_SQLITE_STATEMENT( result, db_.handle() );
    numberOfPreparedStatements++;
    return stmt;
  }
  
  // ----------
  // debugging
  
  ostream& operator<<(ostream& ostr, const PreparedTable& rhs)
  { return ostr << " " << rhs.toString() << " "; }
  
}
