/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteTable.h"

#include <sstream>
#include <map>
#include <assert.h>

#include "SqliteDatabase.h"
#include "SqliteValue.h"
#include "SqliteResultSet.h"
#include "DebugLog.h"

#define PRIMARY_KEY_SQL_PARAMETER "?99"

//const int PRIMARY_KEY_SQL_PARAMETER_OFFSET = 99;

// static variables
namespace sqlite
{
  
  // ==========
  // CRUD methods
  // ==========
  
  bool Table::persist( BaseEntity& entity, const char* schema )
  {
    if ( contains( entity, schema ) ) {
      return update( entity, schema );
    } else {
      return insert( entity, schema );
    }
  }
  bool Table::insert( BaseEntity& entity, const char* schema )
  {
    assert( schema );
    // add entity record
    sqlite3_stmt* stmt = preparedInsertStatement_.at( schema );
    assert( stmt );
    bindEntityValues(stmt, schema, entity, entity.keyValue(), true, true);
    int c = sqlite3_step (stmt);
    switch (c) {
//    switch (sqlite3_step (stmt)) {
      case SQLITE_DONE:
        if (sqlite3_changes(handle()) != 1)
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Add entity to database failed!");
        // set unique identifier if we have only one key
        if (entity.keyValue().size() == 1)
          (*entity.keyValue().begin())->num((integer)sqlite3_last_insert_rowid(handle()));
        return true;
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return false;
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Add entity to database: %s", sqlite3_errmsg(handle()));
    }
    return false;
  }
  bool Table::update( BaseEntity& entity, const char* schema )
  {
    assert( schema );
    // update entity record
    sqlite3_stmt* stmt = preparedUpdateStatement_.at( schema );
    assert( stmt );
    bindEntityValues(stmt, schema, entity, entity.keyValue(), true, true);
    switch (sqlite3_step (stmt)) {
      case SQLITE_DONE:
        if (sqlite3_changes(handle()) != 1)
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Update entity in database failed!");
        return true;
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return false;
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Update entity in database: %s", sqlite3_errmsg(handle()));
        return -1;
    }
  }
  BaseEntity& Table::refresh( BaseEntity& entity, const char* schema )
  {
    assert( schema );
    bool found=false;
    int resultCode;
    vector<Value*> values;
    // select entity record
    sqlite3_stmt* stmt = preparedSelectStatement_.at( schema );
    assert( stmt );
    bindEntityValues(stmt, schema, entity, entity.keyValue(), true, false);
    while ((resultCode=sqlite3_step (stmt)) == SQLITE_ROW) { // read all from db counter
      if (! found) {
        readEntityValues(stmt, entity);
        found = true;
      } else {
        // TOOD: this validation fails
        //if (sqlite3_changes(handle()) != 1)
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Refresh entity in database failed!");
      }
    }
    switch (resultCode) {
      case SQLITE_DONE:
        if (! found)
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Refresh entity in database failed!");
        break;
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return entity;
      default:
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION,"Refresh entity in database: %s", sqlite3_errmsg(handle()));
    }
    return entity;
  }
  bool Table::contains( BaseEntity& entity, const char* schema )
  {
    assert( schema );
    bool found=false;
    int resultCode;
    vector<Value*> values;
    // select entity record
    // select entity record
    sqlite3_stmt* stmt = preparedSelectStatement_.at( schema );
    assert( stmt );
    bindEntityValues(stmt, schema, entity, entity.keyValue(), true, false);
    while ((resultCode=sqlite3_step (stmt)) == SQLITE_ROW) { // read all from db counter
      if (! found) {
        found = true;
      } else {
        // TOOD: this validation fails
        //if (sqlite3_changes(handle()) != 1)
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Refresh entity in database failed!");
      }
    }
    switch (resultCode) {
      case SQLITE_DONE:
        return found;
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return false;
      default:
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION,"Refresh entity in database: %s", sqlite3_errmsg(handle()));
        return false;
    }
  }
  bool Table::remove( BaseEntity& entity, const char* schema )
  {
    assert( schema );
    entity.finalize( );
    // select entity record
    sqlite3_stmt* stmt = preparedDeleteStatement_.at( schema );
    assert( stmt );
    bindEntityValues(stmt, schema, entity, entity.keyValue(), true, false);
    switch (sqlite3_step (stmt)) {
      case SQLITE_DONE:
        if (sqlite3_changes(handle()) != 1)
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Remove entity from database failed!");
        return true;
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return false;
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Remove entity from database: %s", sqlite3_errmsg(handle()));
        return false;
    }
  }
  long Table::removeAll( const char* schema ) {
    assert( schema );
    // select entity record
    sqlite3_stmt* stmt = preparedDeleteAllStatement_.at( schema );
    assert( stmt );
    switch (sqlite3_step ( stmt )) {
      case SQLITE_DONE:
        return sqlite3_changes(db_.handle());
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return 0;
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Remove entity in database: %s", sqlite3_errmsg(handle()));
        return -1;
    }
  }
  long Table::count( const char* schema ) const
  {
    assert( schema );
    DEBUG_METHOD();
    sqlite3_stmt* stmt = preparedCountStatement_.at( schema );
    assert( stmt );
    DEBUG_PRINT("%s: executing sql %s",
                this->name(),
                sqlite3_sql(stmt));
    EXEC_SQLITE_STATEMENT (sqlite3_reset ( stmt ), db_.handle());
    long result = 0;
    bool found = false;
    int resultCode;
    while ((resultCode=sqlite3_step ( stmt )) == SQLITE_ROW) { // read all from db counter
      if (! found) {
        switch ( sqlite3_column_type( stmt, 0) ) {
          case SQLITE_INTEGER:
            result = (long)sqlite3_column_int64( stmt, 0 );
            break;
          default:
            ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid sequence data type");
        }
        found = true;
      } else {
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid number of result rows");
      }
    }
    switch (resultCode) {
      case SQLITE_DONE:
        return result;
      case SQLITE_BUSY:
      case SQLITE_LOCKED:
        return 0;
      default:
        ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION,"Count in database: %s", sqlite3_errmsg(handle()));
        return result;
    }
  }
  unique_ptr<Value> Table::valueOf( const char* schema, const char* sqlAlias, va_list conditions ) {
    DEBUG_METHOD();
    
    assert( schema );
    map<string,sqlite3_stmt*> statements = preparedSqlAliasStatement_.at( schema );
    assertf(statements.find( sqlAlias ) != statements.end(), "Missing finder definition in %s: %s", schema, sqlAlias);
    sqlite3_stmt* stmt = statements.at( sqlAlias );
    assert( stmt );
    
    // convert variable arguments into a list
    vector< pair<field_type,const Value*> > values( this->toBindVariables( conditions ) );
    
    // clear statement first
    EXEC_SQLITE_STATEMENT (sqlite3_reset (stmt), db_.handle());
    EXEC_SQLITE_STATEMENT (sqlite3_clear_bindings (stmt), db_.handle());
    
    DEBUG_PRINT("%s: bind prepared sql alias %s", this->name(), sqlite3_sql(stmt));
    
    // bind entities values
    bindValuesToStmt(stmt, 1, values);
    
    unique_ptr<Value> result(new Value());
    bool loop = true;
    for (;loop;) {
      switch (sqlite3_step (stmt)) {
        case SQLITE_ROW:
          // read entities values
          if ( sqlite3_column_count( stmt ) != 1 ) {
            ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "raw sql aggregation statment must return a single value");
          }
          switch ( sqlite3_column_type( stmt, 0) ) {
            case SQLITE_NULL:
              break;
            case SQLITE_INTEGER:
              result = unique_ptr<Value>(new Value((integer) sqlite3_column_int64( stmt, 0 )));
              break;
            case SQLITE_FLOAT:
              result = unique_ptr<Value>(new Value((real) sqlite3_column_double( stmt, 0 )));
              break;
            case SQLITE3_TEXT:
              result = unique_ptr<Value>(new Value(reinterpret_cast<const char*>(sqlite3_column_text( stmt, 0 ))));
              break;
            case SQLITE_BLOB:
              result = unique_ptr<Value>(new Value(reinterpret_cast<const char*>(sqlite3_column_blob( stmt, 0 )),
                                                 (sqlite3_column_bytes(stmt, 0) / sizeof(char))));
              break;
            default:
              ejin::_throw_exception(ejin::RETRIEVAL_EXCEPTION, "Invalid sequence data type");
          }
          break;
        case SQLITE_DONE:
        case SQLITE_BUSY:
        case SQLITE_LOCKED:
          loop = false;
          break;
        default:
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Select entity in database: %s", sqlite3_errmsg(handle()));
      }
    }
    return result;
  }
  unique_ptr<ResultSet> Table::find( const char* schema, const char* finder, va_list conditions ) {
    DEBUG_METHOD();
    
    assert( schema );
    
    map<string,sqlite3_stmt*> statements = preparedFinderStatement_.at( schema );
    assertf(statements.find( finder ) != statements.end(), "Missing finder definition in %s: %s", schema, finder);
    sqlite3_stmt* stmt = statements.at( finder );
    assert( stmt );
    
    // convert variable arguments into a list
    vector< pair<field_type,const Value*> > values( this->toBindVariables( conditions ) );
    
    // clear statement first
    EXEC_SQLITE_STATEMENT (sqlite3_reset (stmt), db_.handle());
    EXEC_SQLITE_STATEMENT (sqlite3_clear_bindings (stmt), db_.handle());
    
    DEBUG_PRINT("%s: bind prepared finder statement %s", this->name(), sqlite3_sql(stmt));
    
    // bind entities values
    bindValuesToStmt(stmt, 1, values);
    // loop through result set
    BaseEntity* entity;
    
    unique_ptr<ResultSet> result(unique_ptr<ResultSet>(new ResultSet()));
    string msg;
    bool loop = true;
    for (;loop;) {
      switch (sqlite3_step (stmt)) {
        case SQLITE_ROW:
          entity = prototype_->clone();
          readEntityValues(stmt, *entity);
          result->put(entity);
          break;
        case SQLITE_DONE:
        case SQLITE_BUSY:
        case SQLITE_LOCKED:
          loop = false;
          break;
        default:
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Select entity in database: %s", sqlite3_errmsg(handle()));
      }
    }
    return result;
  }
  
  vector< pair<field_type,const Value*> > Table::toBindVariables( va_list conditions ) {
    // convert variable arguments into a list
    vector< pair<field_type,const Value*> > values;
    const Value* condition = va_arg(conditions, const Value *);
    while (condition) {
      values.push_back( pair<field_type,const Value*>(condition->type(),condition) );
      condition = va_arg(conditions, const Value *);
    }
    return values;
  }
  
}
