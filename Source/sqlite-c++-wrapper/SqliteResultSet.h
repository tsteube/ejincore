/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_RESULT_SET_H__
#define __SQLITE_RESULT_SET_H__

#include "SqliteCommon.h"

namespace sqlite
{
  
  /**
   * Container of all result sets of a db search.
   * The destructor will free all temporay created BaseEntity 
   * except the ones which were taken from the caller.
   */ 
  class ResultSet {
    friend class Table;
    friend ostream& operator<<(ostream& ostr, const ResultSet& rhs);

    // ==========
    // Object Interface
    // ==========
  public:
    ~ResultSet( void );
  private:
    // ctor
    ResultSet( void );
    // disable copy ctor
    ResultSet( const ResultSet& recordSet );
    // disable assignment / comparision
    ResultSet& operator=(const ResultSet& rhs);
    
    // ==========
    // Public interface
    // ==========
  public:
    
    vector< BaseEntity* >::iterator begin ( void ) { return this->resultSet_.begin(); }
    vector< BaseEntity* >::const_iterator begin ( void ) const { return this->resultSet_.begin(); }
    
    vector< BaseEntity* >::iterator end ( void ) { return this->resultSet_.end(); }
    vector< BaseEntity* >::const_iterator end ( void ) const { return this->resultSet_.end(); }

    size_t size( void ) const { return this->resultSet_.size(); }
    BaseEntity& operator[] (int nIndex) const { return *this->resultSet_[nIndex]; }

    BaseEntity* takeFirst( );
    vector< BaseEntity* >::iterator take( vector< BaseEntity* >::iterator position );
    
    // ==========
    // Private Interface
    // ==========
  private:
    
    vector< BaseEntity* > resultSet_;
    int taken_;
    
    void put( BaseEntity* entity) { this->resultSet_.push_back(entity); }    
    
  };
  
  // debugging
  ostream& operator<<(ostream ostr, const ResultSet& rhs);  
  
}

#endif // __SQLITE_RESULT_SET_H__