/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteDatabase.h"

#include <assert.h>
#include <time.h>
#include <utime.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <copyfile.h>
#include <openssl/sha.h>
#include <openssl/md5.h>

#include "RSACrypto.h"
#include "SqliteResultSet.h"
#include "SqliteTable.h"
#include "SqliteValue.h"
#include "DebugLog.h"

static int drop_trigger_callback(void* data, int argc, char** argv, char** azColName)
{
  if ( argc == 2 ) {
    assert( data );
    sqlite3* db = static_cast<sqlite3*>(data);
    char cmd[256];
    snprintf(cmd,sizeof(cmd),"DROP TRIGGER %s.%s", argv[0], argv[1]);
    EXEC_SQLITE_STATEMENT (sqlite3_exec(db, cmd ,NULL, NULL, NULL), db );
  }
  return 0;
}

static bool hasEnding(const std::string& fullString, const std::string& ending)
{
  if (fullString.length() > ending.length()) {
    return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
  } else {
    return false;
  }
}

#ifdef SQLITE_HAS_CODEC
static char* replace(const char *str, const char *oldp, const char *newp)
{
  char *ret, *r;
  const char *p, *q;
  size_t oldlen = strlen(oldp);
  size_t count, retlen, newlen = strlen(newp);
  
  if (oldlen != newlen) {
    for (count = 0, p = str; (q = strstr(p, oldp)) != NULL; p = q + oldlen)
      count++;
    /* this is undefined if p - str > PTRDIFF_MAX */
    retlen = p - str + strlen(p) + count * (newlen - oldlen);
  } else
    retlen = strlen(str);
  
  ret = (char*)malloc(retlen + 1);
  
  for (r = ret, p = str; (q = strstr(p, oldp)) != NULL; p = q + oldlen) {
    /* this is undefined if q - p > PTRDIFF_MAX */
    ptrdiff_t l = q - p;
    memcpy(r, p, l);
    r += l;
    memcpy(r, newp, newlen);
    r += newlen;
  }
  strcpy(r, p);
  
  return ret;
}
#endif
static int fileExistsAndNotEmpty( const char* filename )
{
  struct stat dbFileAttr;
  return ( stat( filename, &dbFileAttr ) == 0 &&
          S_ISREG(dbFileAttr.st_mode) && (dbFileAttr.st_mode & S_IWUSR) &&
          dbFileAttr.st_size > 0 );
}

static void sha1_func(sqlite3_context *context, int argc, sqlite3_value **argv)
{
  const char *value = (char *)sqlite3_value_text(argv[0]);
  int length = sqlite3_value_bytes(argv[0]);
  
  unsigned char hash[SHA_DIGEST_LENGTH];
  SHA_CTX sha1;
  SHA1_Init(&sha1);
  SHA1_Update(&sha1, value, length);
  SHA1_Final(hash, &sha1);

  char buffer[SHA_DIGEST_LENGTH * 2];
  for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
    sprintf(buffer + i*2, "%02x", (int) hash[i]);
  }
  
  sqlite3_result_text(context, buffer, SHA256_DIGEST_LENGTH * 2, SQLITE_TRANSIENT);
}
static void sha256_func(sqlite3_context *context, int argc, sqlite3_value **argv)
{
  const char *value = (char *)sqlite3_value_text(argv[0]);
  int length = sqlite3_value_bytes(argv[0]);
  
  unsigned char hash[SHA256_DIGEST_LENGTH];
  SHA256_CTX sha256;
  SHA256_Init(&sha256);
  SHA256_Update(&sha256, value, length);
  SHA256_Final(hash, &sha256);

  char buffer[SHA256_DIGEST_LENGTH * 2];
  for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
    sprintf(buffer + i*2, "%02x", (int) hash[i]);
  }
  
  sqlite3_result_text(context, buffer, SHA256_DIGEST_LENGTH * 2, SQLITE_TRANSIENT);
}
static void md5_func(sqlite3_context *context, int argc, sqlite3_value **argv)
{
  const char *value = (char *)sqlite3_value_text(argv[0]);
  int length = sqlite3_value_bytes(argv[0]);
  
  unsigned char hash[MD5_DIGEST_LENGTH];
  MD5_CTX md5;
  MD5_Init(&md5);
  MD5_Update(&md5, value, length);
  MD5_Final(hash, &md5);

  char buffer[MD5_DIGEST_LENGTH * 2];
  for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
    sprintf(buffer + i*2, "%02x", (int) hash[i]);
  }

  sqlite3_result_text(context, buffer, MD5_DIGEST_LENGTH * 2, SQLITE_TRANSIENT);
}
static void escapeSearchPattern_func(sqlite3_context *context, int argc, sqlite3_value **argv)
{
  const char *value = (char *)sqlite3_value_text(argv[0]);
  int length = sqlite3_value_bytes(argv[0]);
  
  int j=0,i=0;
  char buffer[length+2];
  buffer[j++] = '%';
  for (;i<length; i++) {
    switch (value[i]) {
      case '*':
        buffer[j++]='%';
        break;
      case '?':
        buffer[j++]='_';
        break;
      case '%':
        break;
      case '\0':
        break;
      default:
        buffer[j++]=tolower(value[i]);
    }
  }
  buffer[j++] = '%';
  buffer[j] = '\0';

  sqlite3_result_text(context, buffer, MD5_DIGEST_LENGTH * 2, SQLITE_TRANSIENT);
}
static void max_cell_step(sqlite3_context *context, int argc, sqlite3_value** argv)
{
  sqlite3_int64 *buffer = (sqlite3_int64 *)sqlite3_aggregate_context(context, sizeof(sqlite3_int64));
  for (int i=0; i<argc; i++) {
    if (argv[i]) {
      sqlite3_int64 x = sqlite3_value_int64(argv[i]);
      if (*buffer < x)
        *buffer = x;
    }
  }
}
static void max_cell_or_final(sqlite3_context *context)
{
  sqlite3_int64 *buffer = (sqlite3_int64 *)sqlite3_aggregate_context(context, sizeof(sqlite3_int64));
  sqlite3_result_int64(context, *buffer);
}

static void bitwise_or_step(sqlite3_context *context, int argc, sqlite3_value** argv)
{
  int *buffer = (int *)sqlite3_aggregate_context(context, sizeof(int));
  for (int i=0; i<argc; i++) {
    *buffer |= sqlite3_value_int(argv[i]);
  }
}
static void bitwise_or_final(sqlite3_context *context)
{
  int *buffer = (int *)sqlite3_aggregate_context(context, sizeof(int));
  sqlite3_result_int(context, *buffer);
}

namespace sqlite
{
  using ejin::AESCrypto;
  using std::ios;
  using std::stringstream;
  using std::ifstream;
  using std::ofstream;
  using std::ostreambuf_iterator;
  using std::istreambuf_iterator;
  
  // ==========
  // Ctors
  // ==========
  
  Database::Database( void ):
  _tableToEntity(), db_(NULL), result_open_(SQLITE_ERROR), targetDb_(MainDatabase), version_(0),
  backup_attached_(false), archive_attached_(false),
  preparedTxBeginStatement_(NULL), preparedTxRollbackStatement_(NULL), preparedTxCommitStatement_(NULL)
  {
    tzset();
  }
  Database::~Database( void )
  {
    close( );
  }
  
  string Database::toString( void ) const
  {
    stringstream buf;
    buf << typeid(this).name() << "["
    << "open=" << this->isOpen()
    << ",size=" << this->_tableToEntity.size()
    << ",version=" << this->version_
    << "]";
    return buf.str();
  }
  
  // ==========
  // Public Methods
  // ==========
  
  Database::Status Database::open(int version, const char* mainProto, const char* mainDatabase,
                                  const char* attachmentProto, const char* attachmentDatabase,
                                  const char* keyFileProto, const char* keyFile,
                                  const char* password, bool force )
  {
    assert( mainProto );
    assert( mainDatabase );
    
    if ( createDbFileIfNotExists( mainDatabase, password, mainProto, force ) ) {
      // delete backup file name if available
      char backupFilename[PATH_MAX];
      strcpy( backupFilename, mainDatabase );
      strcat( backupFilename, "~" );
      unlink( backupFilename );
    }
    createDbFileIfNotExists( attachmentDatabase, password, attachmentProto, force );
    useKeyFileIfExists( keyFile, password, keyFileProto, force);
    
    this->version_ = version;
    this->mainPrototype = mainProto;
    this->attachmentPrototype = attachmentProto;
    return this->openDb( mainDatabase, attachmentDatabase, password );
  }
  bool Database::createDbFileIfNotExists( const char* database, const char* password, const char* prototype, bool force )
  {
    bool result = false;
    if ( force || ! fileExistsAndNotEmpty( database ) ) {
#ifdef SQLITE_HAS_CODEC
      // export database from prototype into new database with given password
      result = Database::createDbFrom( prototype, database, password );
#else
      // just copy prototype database to target
      copyfile_state_t s = copyfile_state_alloc();
      result = copyfile( mainPrototype, mainDatabase, s, COPYFILE_DATA | COPYFILE_XATTR) == 0;
      copyfile_state_free( s );
#endif
    }
    return result;
  }
  bool Database::useKeyFileIfExists( const char* keyFile, const char* password, const char* keyFileProto, bool force )
  {
    bool result = false;
    if (fileExistsAndNotEmpty( keyFileProto )) {
      if ( force || ! fileExistsAndNotEmpty( keyFile ) ) {
        // just copy prototype database to target
        copyfile_state_t s = copyfile_state_alloc();
        result = copyfile( keyFileProto, keyFile, s, COPYFILE_DATA | COPYFILE_XATTR) == 0;
        copyfile_state_free( s );
      }
    }
    return result;
  }
  Database::Status Database::reopen( const char* password )
  {
    assert(this->isOpen());
    string mainDb = this->getMainFilename();
    string attachDb = this->getAttachmentFilename();
    this->close();
    return this->openDb( mainDb.c_str(), attachDb.c_str(), password);
  }
  Database::Status Database::vacuumDb( const char* database, const char* password )
  {
    DEBUG_METHOD();
    if (! fileExistsAndNotEmpty( database ) ) {
      return DatabaseNotFound;
    }
    
    sqlite3* db;
#ifdef SQLITE_ENABLE_SHAREDCACHE
    this->result_open_ = sqlite3_open_v2(database, &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_SHAREDCACHE , NULL );
#else
    this->result_open_ = sqlite3_open_v2(database, &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_PRIVATECACHE , NULL );
#endif
    switch (this->result_open_) {
      case SQLITE_OK:
        try {
          // set password for all databases
          Database::setKey( db, NULL, password );
          Database::vacuum( db );
          sqlite3_close(db);
        } catch(const ejin::access_denied_error& e) {
          sqlite3_close(db);
          return DatabaseEncrypted;
        } catch(const ejin::data_access_error& e) {
          sqlite3_close(db);
          return DatabaseCorrupted;
        }
    }
    return DatabaseClose;
  }
  Database::Status Database::openDb( const char* mainDatabase, const char* attachmentDatabase,  const char* password )
  {
    DEBUG_METHOD();
    close();
    
    if (! fileExistsAndNotEmpty( mainDatabase ) ||
        ! fileExistsAndNotEmpty( attachmentDatabase ) ) {
      DEBUG_PRINT("database files are not available");
      return DatabaseNotFound;
    }
    
    //Database::vacuumAll( mainDatabase, password );
    //Database::vacuumAll( attachmentDatabase, password );

#ifdef SQLITE_ENABLE_SHAREDCACHE
    this->result_open_ = sqlite3_open_v2(mainDatabase, &db_, SQLITE_OPEN_READWRITE|SQLITE_OPEN_SHAREDCACHE, NULL );
#else
    this->result_open_ = sqlite3_open_v2(mainDatabase, &db_, SQLITE_OPEN_READWRITE|SQLITE_OPEN_PRIVATECACHE, NULL );
#endif
    switch (this->result_open_) {
      case SQLITE_OK:
        try {
          // set password for all databases
          Database::setKey( db_, MAIN_DATABASE_NAME, password );

          // Another possibility is to multiply the free pages by the page_size PRAGMA and trigger when an absolute size is reached.
          
          Database::preparedTxBeginStatement_ = prepareTxBeginStatement(db_);
          Database::preparedTxRollbackStatement_ = prepareTxRollbackStatement(db_);
          Database::preparedTxCommitStatement_ = prepareTxCommitStatement(db_);

          Database::setEncoding(db_, "UTF-8");
          Database::setForeignKeySupport( db_ );
          Database::setLockingMode(db_, kSQLITE3LockingModeNormal);
          Database::addCustomFunctions(db_);
          
          backup_attached_ = false;
          archive_attached_ = false;
          
          Database::attachDatabase( db_, attachmentDatabase, ATTACHMENT_DATABASE_NAME, password );
          upgradeDatabase( MAIN_DATABASE_NAME, version_ );
          upgradeDatabase( ATTACHMENT_DATABASE_NAME, version_ );
          
          if ( needsVacuum( db_, MAIN_DATABASE_NAME ) ) {
            vacuumDb( this->getMainFilename(), password );
          }
          if ( needsVacuum( db_, ATTACHMENT_DATABASE_NAME ) ) {
            vacuumDb( this->getAttachmentFilename(), password );
          }
          
          //Database::setAutoVacuum(db_, kSQLITE3AutoVacuumIncrementalVacuum, MAIN_DATABASE_NAME);
          //Database::setAutoVacuum(db_, kSQLITE3AutoVacuumIncrementalVacuum, ATTACHMENT_DATABASE_NAME);
          //Database::incrementalVacuum(db_, 5, MAIN_DATABASE_NAME);
          //Database::incrementalVacuum(db_, 5, ATTACHMENT_DATABASE_NAME);
          
          initEntityManager();
          for ( auto it=_tableToEntity.begin() ; it != _tableToEntity.end(); it++ ) {
            it->second->prepareAllStatementsOn( it->second->mainSchemaName() );
          }

          return DatabaseOpen;
        } catch(const ejin::access_denied_error& e) {
          sqlite3_close(db_);
          this->result_open_ = SQLITE_ERROR;
          DEBUG_PRINT("can't decrypt database file: %s", mainDatabase);
          return DatabaseEncrypted;
        } catch(const ejin::data_access_error& e) {
          sqlite3_close(db_);
          this->result_open_ = SQLITE_ERROR;
          DEBUG_PRINT("can't open database file '%s': %s", mainDatabase, e.what());
          return DatabaseCorrupted;
        }
        
      default:
        // Even though the open failed, call close to properly clean up resources.
        sqlite3_close(db_);
        return DatabaseNotFound;
    }
  }
  Database::Status Database::attachBackupDb( const char* password )
  {
    DEBUG_METHOD();
    if ( isOpen() ) {
      if ( ! isBackupOpen() ) {
        const char* mainDatabase = getMainFilename( );
        char backupFilename[PATH_MAX];
        strcpy( backupFilename, mainDatabase );
        strcat( backupFilename, "~" );
        if ( ! createBackupDbIfNecessary( mainDatabase, backupFilename ) ) {
          DEBUG_PRINT("backup database file '%s' is not available", backupFilename);
          return DatabaseNotFound;
        }
        try {
          Database::attachDatabase( db_, backupFilename, BACKUP_DATABASE_NAME, password );
          upgradeDatabase( BACKUP_DATABASE_NAME, version_ );
          if ( needsVacuum( db_, BACKUP_DATABASE_NAME ) ) {
            vacuumDb( this->getBackupFilename(), password );
          }
          //Database::dropFTS5Support( db_, BACKUP_DATABASE_NAME );
          //Database::dropGenIDSupport( db_, BACKUP_DATABASE_NAME );
          Database::dropAllTrigger( db_, BACKUP_DATABASE_NAME );
          //Database::setAutoVacuum(db_, kSQLITE3AutoVacuumIncrementalVacuum, BACKUP_DATABASE_NAME);
          //Database::incrementalVacuum(db_, 5, BACKUP_DATABASE_NAME);

          for ( auto it=_tableToEntity.begin() ; it != _tableToEntity.end(); it++ ) {
            it->second->prepareAllStatementsOn( it->second->backupSchemaName() );
          }
          backup_attached_ = true;
          return DatabaseOpen;
        } catch(const ejin::access_denied_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't decrypt database file: %s", backupFilename);
          return DatabaseEncrypted;
        } catch(const ejin::data_access_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't open database file '%s': %s", backupFilename, e.what());
          return DatabaseCorrupted;
        }
      } else {
        return DatabaseOpen; // already attached
      }
    }
    return DatabaseNotFound;
  }
  Database::Status Database::detachBackupDb( )
  {
    DEBUG_METHOD();
    if ( isOpen() ) {
      if ( isBackupOpen() ) {
        try {
          Database::detachDatabase( db_, BACKUP_DATABASE_NAME );
          for ( auto it=_tableToEntity.begin() ; it != _tableToEntity.end(); it++ ) {
            it->second->clearAllStatementsOn( it->second->backupSchemaName() );
          }
          backup_attached_ = false;
          if ( getTargetSchema() == TargetSchema::BackupDatabase ) {
            setTargetSchema( TargetSchema::MainDatabase );
          }
          return DatabaseOpen; // already attached
        } catch(const ejin::access_denied_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't decrypt database file: %s", getBackupFilename());
          return DatabaseEncrypted;
        } catch(const ejin::data_access_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't open database file '%s': %s", getBackupFilename(), e.what());
          return DatabaseCorrupted;
        }
      } else {
        return DatabaseOpen; // already attached
      }
    }
    return DatabaseNotFound;
  }
  Database::Status Database::attachArchiveDb( const char* password, bool force )
  {
    DEBUG_METHOD();
    if ( isOpen() ) {
      if ( ! isArchiveOpen() ) {
        const char* mainDatabase = getMainFilename( );
        const char* attachmentDatabase = getAttachmentFilename( );
        
        char mainArchiveFilename[PATH_MAX];
        strcpy( mainArchiveFilename, mainDatabase );
        strcat( mainArchiveFilename, "a" );
        
        char attachmentArchiveFilename[PATH_MAX];
        strcpy( attachmentArchiveFilename, attachmentDatabase );
        strcat( attachmentArchiveFilename, "a" );
        
        createDbFileIfNotExists( mainArchiveFilename, password, this->mainPrototype.c_str(), force );
        createDbFileIfNotExists( attachmentArchiveFilename, password, this->attachmentPrototype.c_str(), force );
        
        try {
          
          Database::attachDatabase( db_, mainArchiveFilename, ARCHIVE_DATABASE_NAME, password );
          Database::attachDatabase( db_, attachmentArchiveFilename, ATTACHMENT_ARCHIVE_DATABASE_NAME, password );
          
          upgradeDatabase( ARCHIVE_DATABASE_NAME, version_ );
          upgradeDatabase( ATTACHMENT_ARCHIVE_DATABASE_NAME, version_ );
          
          if ( needsVacuum( db_, ARCHIVE_DATABASE_NAME ) ) {
            vacuumDb( this->getArchiveFilename(), password );
          }
          if ( needsVacuum( db_, ATTACHMENT_ARCHIVE_DATABASE_NAME ) ) {
            vacuumDb( this->getAttachmentArchiveFilename(), password );
          }

          //Database::setAutoVacuum(db_, kSQLITE3AutoVacuumIncrementalVacuum, ARCHIVE_DATABASE_NAME);
          //Database::setAutoVacuum(db_, kSQLITE3AutoVacuumIncrementalVacuum, ATTACHMENT_ARCHIVE_DATABASE_NAME);
          //Database::incrementalVacuum(db_, 5, ARCHIVE_DATABASE_NAME);
          //Database::incrementalVacuum(db_, 5, ATTACHMENT_ARCHIVE_DATABASE_NAME);

          for ( auto it=_tableToEntity.begin() ; it != _tableToEntity.end(); it++ ) {
            it->second->prepareAllStatementsOn( it->second->archiveSchemaName() );
          }
          
          archive_attached_ = true;
          return DatabaseOpen;
        } catch(const ejin::access_denied_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't decrypt database file: %s", mainArchiveFilename);
          return DatabaseEncrypted;
        } catch(const ejin::data_access_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't open database file '%s': %s", mainArchiveFilename, e.what());
          return DatabaseCorrupted;
        }
      } else {
        return DatabaseOpen; // already attached
      }
    }
    return DatabaseNotFound;
  }
  Database::Status Database::vacuumDb( const char* password )
  {
    assert( password );
    DEBUG_METHOD();
    if ( isOpen() ) {
      try {
        
        if ( needsVacuum( db_, MAIN_DATABASE_NAME ) ) {
          vacuumDb( this->getMainFilename(), password );
        }
        if ( needsVacuum( db_, ATTACHMENT_DATABASE_NAME ) ) {
          vacuumDb( this->getAttachmentFilename(), password );
        }
        if ( backup_attached_ ) {
          if ( needsVacuum( db_, BACKUP_DATABASE_NAME ) ) {
            vacuumDb( this->getBackupFilename(), password );
          }
        }
        if ( archive_attached_ ) {
          if ( needsVacuum( db_, ARCHIVE_DATABASE_NAME ) ) {
            vacuumDb( this->getArchiveFilename(), password );
          }
          if ( needsVacuum( db_, ATTACHMENT_ARCHIVE_DATABASE_NAME ) ) {
            vacuumDb( this->getAttachmentArchiveFilename(), password );
          }
        }
        return DatabaseOpen;
      } catch(const ejin::access_denied_error& e) {
        sqlite3_close(db_);
        DEBUG_PRINT("error access database to vacuum");
        return DatabaseEncrypted;
      } catch(const ejin::data_access_error& e) {
        sqlite3_close(db_);
        DEBUG_PRINT("error access database to vacuum");
        return DatabaseCorrupted;
      }
    }
    return DatabaseClose;
  }
  bool Database::needsVacuumDb( const char* password )
  {
    assert( password );
    DEBUG_METHOD();
    if ( isOpen() ) {
      if ( needsVacuum( db_, MAIN_DATABASE_NAME ) ) {
        return true;
      }
      if ( needsVacuum( db_, ATTACHMENT_DATABASE_NAME ) ) {
        return true;
      }
      if ( backup_attached_ ) {
        if ( needsVacuum( db_, BACKUP_DATABASE_NAME ) ) {
          return true;
        }
      }
      if ( archive_attached_ ) {
        if ( needsVacuum( db_, ARCHIVE_DATABASE_NAME ) ) {
          return true;
        }
        if ( needsVacuum( db_, ATTACHMENT_ARCHIVE_DATABASE_NAME ) ) {
          return true;
        }
      }
    }
    return false;
  }
  Database::Status Database::detachArchiveDb( const char* password )
  {
    DEBUG_METHOD();
    if ( isOpen() ) {
      if ( isArchiveOpen() ) {
        try {
          if ( password ) {
            if ( needsVacuum( db_, ARCHIVE_DATABASE_NAME ) ) {
              vacuumDb( this->getArchiveFilename(), password );
            }
            if ( needsVacuum( db_, ATTACHMENT_ARCHIVE_DATABASE_NAME ) ) {
              vacuumDb( this->getAttachmentArchiveFilename(), password );
            }
          }
          
          Database::detachDatabase( db_, ARCHIVE_DATABASE_NAME );
          Database::detachDatabase( db_, ATTACHMENT_ARCHIVE_DATABASE_NAME );
          
          for ( auto it=_tableToEntity.begin() ; it != _tableToEntity.end(); it++ ) {
            it->second->clearAllStatementsOn( it->second->archiveSchemaName() );
          }
          archive_attached_ = false;
          if ( getTargetSchema() == TargetSchema::ArchiveDatabase ) {
            setTargetSchema( TargetSchema::MainDatabase );
          }

          return DatabaseOpen; // already attached
        } catch(const ejin::access_denied_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't decrypt database file: %s", getArchiveFilename());
          return DatabaseEncrypted;
        } catch(const ejin::data_access_error& e) {
          sqlite3_close(db_);
          DEBUG_PRINT("can't open database file '%s': %s", getArchiveFilename(), e.what());
          return DatabaseCorrupted;
        }
      } else {
        return DatabaseOpen; // already attached
      }
    }
    return DatabaseNotFound;
  }
  void Database::close( void )
  {
    DEBUG_METHOD();
    if (db_)
    {
      // detach databases
      if ( backup_attached_ ) {
        this->detachDatabase( db_, BACKUP_DATABASE_NAME );
      }
      if ( archive_attached_ ) {
        this->detachDatabase( db_, ATTACHMENT_ARCHIVE_DATABASE_NAME );
        this->detachDatabase( db_, ARCHIVE_DATABASE_NAME );
      }
      
      // clear entity manager
      map<string,Table*>::iterator it;
      for ( it=_tableToEntity.begin() ; it != _tableToEntity.end(); it++ ) {
        it->second->clearAllStatementsOn( it->second->mainSchemaName() );
        it->second->clearAllStatementsOn( it->second->backupSchemaName() );
        it->second->clearAllStatementsOn( it->second->archiveSchemaName() );
        delete (*it).second;
      }
      _tableToEntity.clear();
      
      // free release prepared statements
      if (this->preparedTxBeginStatement_) {
        sqlite3_finalize(this->preparedTxBeginStatement_);
        this->preparedTxBeginStatement_ = NULL;
      }
      if (this->preparedTxRollbackStatement_) {
        sqlite3_finalize(this->preparedTxRollbackStatement_);
        this->preparedTxRollbackStatement_ = NULL;
      }
      if (this->preparedTxCommitStatement_) {
        sqlite3_finalize(this->preparedTxCommitStatement_);
        this->preparedTxCommitStatement_ = NULL;
      }
      
      sqlite3_close(db_);
      this->db_ = NULL;
      this->result_open_ = SQLITE_ERROR;
    }
  }
  bool Database::isClean( void ) const
  {
    if (isOpen()) {
      switch (sqlite3_errcode(db_)) {
        case SQLITE_OK:
        case SQLITE_ROW:
        case SQLITE_DONE:
          return true;
        default:
          return false;
      }
    }
    return false;
  }
  bool Database::needsVacuum( sqlite3* db, const char* database ) const
  {
    assert(this->isOpen());
    int pageSize = Database::getPageSize( db_, database );
    //int pageCount = Database::getPageCount( db_, database );
    int freeListCount = Database::getFreeListCount( db_, database );
    return pageSize * freeListCount > 1024 * 1000;
  }
  void Database::setTargetSchema( TargetSchema targetDb ) {
    switch ( targetDb ) {
      case MainDatabase:
        this->targetDb_ = targetDb;
        break;
      case ArchiveDatabase:
        assert( isArchiveOpen() );
        this->targetDb_ = targetDb;
        break;
      case BackupDatabase:
        this->targetDb_ = targetDb;
        break;
    }
  }
  bool Database::isAutoCommit( void )
  {
    assert(this->isOpen());
    return sqlite3_get_autocommit( db_ ) != 0;
  }
  void Database::transactionBegin( void )
  {
    assert(this->isOpen());
    EXEC_SQLITE_STATEMENT (sqlite3_reset (this->preparedTxBeginStatement_), db_);
    if (sqlite3_step (this->preparedTxBeginStatement_) != SQLITE_DONE)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Transaction Begin: %s\n", sqlite3_errmsg(db_));
  }
  void Database::transactionCommit( void )
  {
    assert(this->isOpen());
    EXEC_SQLITE_STATEMENT (sqlite3_reset (this->preparedTxCommitStatement_), db_);
    if (sqlite3_step (this->preparedTxCommitStatement_) != SQLITE_DONE)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Transaction Commit: %s\n", sqlite3_errmsg(db_));
  }
  void Database::transactionRollback( void )
  {
    assert(this->isOpen());
    EXEC_SQLITE_STATEMENT (sqlite3_reset (this->preparedTxRollbackStatement_), db_);
    if (sqlite3_step (this->preparedTxRollbackStatement_) != SQLITE_DONE)
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Transaction Rollback: %s\n", sqlite3_errmsg(db_));
  }
  bool Database::resetPassword( const char* oldPassword, const char* newPassword )
  {
    assert(this->isOpen());

#ifdef SQLITE_HAS_CODEC
/*
    {
      auto start = std::chrono::system_clock::now();
      auto end = std::chrono::system_clock::now();
      std::chrono::duration<double>  elapsed_seconds = end-start;
      std::cout << "XXXXXXXX 1 " << elapsed_seconds.count() << std::endl;
    }
 */
    bool success = this->rekey( getMainFilename(), newPassword, MAIN_DATABASE_NAME ) &&
      this->rekey( getBackupFilename(), newPassword, BACKUP_DATABASE_NAME ) &&
      this->rekey( getAttachmentFilename(), newPassword, ATTACHMENT_DATABASE_NAME );
    if (success) {
      if ( this->attachArchiveDb( oldPassword ) == DatabaseOpen ) {
        success = this->rekey( getArchiveFilename(), newPassword, ARCHIVE_DATABASE_NAME ) &&
          this->rekey( getAttachmentArchiveFilename(), newPassword, ATTACHMENT_ARCHIVE_DATABASE_NAME );
        this->detachArchiveDb( newPassword );
      }
    }
    return success;
#else
    return false;
#endif
  }
  bool Database::rekey( const char* filename, const char* newPassword, const char* database ) {
    struct stat statbuf;
    stat(filename, &statbuf);
    bool result = Database::rekey( db_, database, newPassword );
    struct utimbuf times;
    times.actime = statbuf.st_atime; /* keep atime unchanged */
    times.modtime = statbuf.st_mtime; /* keep mtime unchanged */
    //utime(filename, &times);
    return result;
  }

  // ==========
  // Public EM Interface
  // ==========
  
  const BaseEntity& Database::prototype( const char* tableName )
  {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    return table->prototype();
  }
  bool Database::persist( BaseEntity& entity, const char* schema )
  {
    Table* table = this->_tableToEntity[entity.tableName()];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->persist( entity, database );
  }
  bool Database::insert( BaseEntity& entity, const char* schema )
  {
    Table* table = this->_tableToEntity[entity.tableName()];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->insert( entity, database );
  }
  bool Database::update( BaseEntity& entity, const char* schema )
  {
    Table* table = this->_tableToEntity[entity.tableName()];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->update(entity, database);
  }
  BaseEntity& Database::refresh( BaseEntity& entity, const char* schema )
  {
    Table* table = this->_tableToEntity[entity.tableName()];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    table->refresh(entity, database);
    return entity;
  }
  bool Database::contains( BaseEntity& entity, const char* schema )
  {
    Table* table = this->_tableToEntity[entity.tableName()];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->contains(entity, database);
  }
  bool Database::remove( BaseEntity& entity, const char* schema )
  {
    Table* table = this->_tableToEntity[entity.tableName()];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->remove(entity, database);
  }
  long Database::removeAll( const char* tableName, const char* schema )
  {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->removeAll( database );
  }
  long Database::count( const char* tableName, const char* schema )
  {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    const char* database = schema;
    if ( schema == NULL ) {
      database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    }
    return table->count( database );
  }
  unique_ptr<ResultSet> Database::find( const char* tableName, const char* finder, ... ) {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    const char* database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    if ( hasEnding(finder, "Bak") ) {
      database = table->backupSchemaName();
    }
    va_list argptr;
    va_start(argptr,finder);
    unique_ptr<ResultSet> result = table->find(database, finder, argptr);
    va_end(argptr);
    return result;
  }
  unique_ptr<Value> Database::valueOf( const char* tableName, const char* sqlAlias, ... ) {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    const char* database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    if ( hasEnding(sqlAlias, "Bak") ) {
      database = table->backupSchemaName();
    }
    va_list argptr;
    va_start(argptr,sqlAlias);
    unique_ptr<Value> result = table->valueOf(database, sqlAlias, argptr);
    va_end(argptr);
    return result;
  }
  unique_ptr<Value> Database::valueOfWithDefault( const char* tableName, const char* sqlAlias, const Value* defValue, ... ) {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    const char* database = targetDb_ == MainDatabase ? table->mainSchemaName() : table->archiveSchemaName();
    if ( hasEnding(sqlAlias, "Bak") ) {
      database = table->backupSchemaName();
    }
    va_list argptr;
    va_start(argptr, defValue);
    unique_ptr<Value> result = table->valueOf(database, sqlAlias, argptr);
    va_end(argptr);
    if (result->isNull() && defValue!=NULL) {
      result = unique_ptr<Value>(new Value(*defValue));
    }
    return result;
  }
  bool Database::cutBackup( const char* tableName ) {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    bool result = table->cutBackup();
    return result;
  }
  bool Database::backup( const char* tableName, int dummy, ... ) {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    va_list argptr;
    va_start(argptr, dummy);
    bool result = table->backup(argptr);
    va_end(argptr);
    return result;
  }
  bool Database::restore( const char* tableName, int dummy, ... ) {
    Table* table = this->_tableToEntity[tableName];
    assert(table != NULL);
    va_list argptr;
    va_start(argptr, dummy);
    bool result = table->restore(argptr);
    va_end(argptr);
    return result;
  }
  
  // ==========
  // Protected static Method
  // ==========
  
  bool Database::createDbFrom( const char* fromFilename, const char* toFilename, const char* password )
  {
    assert( fromFilename );
    assert( toFilename );
    
    DEBUG_METHOD();
    if ( fileExistsAndNotEmpty( fromFilename ) ) {
      sqlite3* db;
#ifdef SQLITE_ENABLE_SHAREDCACHE
      if ( sqlite3_open_v2(fromFilename, &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_SHAREDCACHE, NULL ) == SQLITE_OK ) {
#else
      if ( sqlite3_open_v2(fromFilename, &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_PRIVATECACHE, NULL ) == SQLITE_OK ) {
#endif
        // ensure target file exists and is empty
        FILE* fd = fopen( toFilename, "w");
        if ( ! fd ) {
          DEBUG_PRINT("Can't create/write export database file '%s'", toFilename);
          return false;
        }
        fclose( fd );
        
        // delete backup file name if available
        char backupFilename[PATH_MAX];
        strcpy( backupFilename, toFilename );
        strcat( backupFilename, "~" );
        unlink( backupFilename );
        
        const char* attachedDbName = "target";
        // attach new database to the prototype one and export data to the new database
        Database::attachDatabase( db, toFilename, attachedDbName, password );
        Database::cipherExport( db, attachedDbName );
        //Database::rememberHMAC( db, password, attachedDbName );
        Database::setUserVersion( db, getUserVersion( db ), attachedDbName );
        Database::detachDatabase( db, attachedDbName );
        
        // open new database
        sqlite3_close(db);
        return true;
      }
    } else {
      DEBUG_PRINT("database file '%s' is not available", fromFilename);
    }
    return false;
  }
  
  void Database::cipherExport( sqlite3* db, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"SELECT sqlcipher_export('%s')", database);
    EXEC_SQLITE_STATEMENT (sqlite3_exec(db,cmd , NULL, NULL, NULL), db );
  }
  
  void Database::dropFTS5Support( sqlite3* db, const char* database )
  {
    assert( db );
    char cmd[256];
    
    // select name from sqlite_master where type = 'trigger' and name like 'gen_%_id';
    snprintf(cmd,sizeof(cmd),"select '%s', name from %s.sqlite_master where type = 'trigger' and name like '%%_content_%%'", database, database);
    EXEC_SQLITE_STATEMENT (sqlite3_exec(db, cmd, drop_trigger_callback, db, NULL), db);
  }

  void Database::dropGenIDSupport( sqlite3* db, const char* database )
  {
    assert( db );
    char cmd[256];
    
    snprintf(cmd,sizeof(cmd),"select '%s', name from %s.sqlite_master where type = 'trigger' and name like 'gen_%%_id'", database, database);
    EXEC_SQLITE_STATEMENT (sqlite3_exec(db, cmd, drop_trigger_callback, db, NULL), db);
  }
  
  void Database::dropAllTrigger( sqlite3* db, const char* database )
  {
    assert( db );
    char cmd[256];
    
    // select name from sqlite_master where type = 'trigger' and name like 'gen_%_id';
    snprintf(cmd,sizeof(cmd),"select '%s', name from %s.sqlite_master where type = 'trigger'", database, database);
    EXEC_SQLITE_STATEMENT (sqlite3_exec(db, cmd, drop_trigger_callback, db, NULL), db);
  }

  void Database::addCustomFunctions( sqlite3* db )
  {
    assert( db );
    EXEC_SQLITE_STATEMENT (sqlite3_create_function_v2(db, "BITWISE_OR", -1, SQLITE_ANY, NULL,
                                                      NULL, bitwise_or_step, bitwise_or_final, NULL), db);
    EXEC_SQLITE_STATEMENT (sqlite3_create_function_v2(db, "MAX_CELL", -1, SQLITE_ANY, NULL,
                                                      NULL, max_cell_step, max_cell_or_final, NULL), db);
    EXEC_SQLITE_STATEMENT (sqlite3_create_function_v2(db, "MD5", 1, SQLITE_UTF8, NULL,
                                                      md5_func, NULL, NULL, NULL), db);
    EXEC_SQLITE_STATEMENT (sqlite3_create_function_v2(db, "SHA1", 1, SQLITE_UTF8, NULL,
                                                      sha1_func, NULL, NULL, NULL), db);
    EXEC_SQLITE_STATEMENT (sqlite3_create_function_v2(db, "SHA256", 1, SQLITE_UTF8, NULL,
                                                      sha256_func, NULL, NULL, NULL), db);
    EXEC_SQLITE_STATEMENT (sqlite3_create_function_v2(db, "searchPattern", 1, SQLITE_UTF8, NULL,
                                                      escapeSearchPattern_func, NULL, NULL, NULL), db);
  }
  bool Database::createBackupDbIfNecessary( const char* _dbFilename, const char* _bakFilename )
  {
    DEBUG_METHOD();
    struct stat dbFileAttr;
    struct stat bakFileAttr;
    
    if (stat(_bakFilename, &bakFileAttr) == 0) {
      if ( ! S_ISREG(bakFileAttr.st_mode) && ! (bakFileAttr.st_mode & S_IWUSR) ) {
        DEBUG_PRINT("backup database file '%s' is not a file and writable", _bakFilename);
        return false;
      }
      
#ifdef _DARWIN_FEATURE_64_BIT_INODE
      // data get lost somehow ??
      stat(_bakFilename, &bakFileAttr);
      stat(_dbFilename, &dbFileAttr);
      
      if (dbFileAttr.st_birthtimespec.tv_sec < bakFileAttr.st_birthtimespec.tv_sec) {
        return true;
      } else if (dbFileAttr.st_birthtimespec.tv_sec == bakFileAttr.st_birthtimespec.tv_sec) {
        if (dbFileAttr.st_birthtimespec.tv_nsec <= bakFileAttr.st_birthtimespec.tv_nsec) {
          return true;
        }
      }
#endif
      
    }
    
    // copy original database to backup database
    copyfile_state_t s = copyfile_state_alloc();
    bool result = copyfile( _dbFilename, _bakFilename, s, COPYFILE_DATA | COPYFILE_XATTR) == 0;
    copyfile_state_free( s );
    return result;
  }
  
  void Database::attachDatabase( sqlite3* db, const char* filename, const char* database, const char* password )
  {
    assert( db );
    
    char cmd[BUFSIZ];
#ifdef SQLITE_HAS_CODEC
    if (password) {
      // escape delimiter character
      char* pwd = replace(password, "'", "''");
      snprintf(cmd,sizeof(cmd),"ATTACH DATABASE '%s' AS %s KEY '%s'", filename, database, pwd);
      free(pwd);
    } else
#endif
      snprintf(cmd,sizeof(cmd),"ATTACH DATABASE '%s' AS %s", filename, database);
    
    int result = sqlite3_exec(db, cmd , NULL, NULL, NULL);
    switch ( result ) {
      case SQLITE_OK:
        return;
      case SQLITE_NOTADB:
        ejin::_throw_exception(ejin::ACCESS_DENIED_EXCEPTION, "database is encrypted");
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Sqlite error code: %s", sqlite3_errmsg(db));
    }
    assert( countTables( db, database ) > 0 );
  }
  void Database::detachDatabase( sqlite3* db, const char* database )
  {
    assert( db );
    char cmd[BUFSIZ];
    snprintf(cmd,sizeof(cmd),"DETACH DATABASE '%s'", database);

    int result = sqlite3_exec(db, cmd , NULL, NULL, NULL);
    switch ( result ) {
      case SQLITE_OK:
        return;
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Sqlite error code: %s", sqlite3_errmsg(db));
    }
  }
  
  bool Database::setKey( sqlite3* db, const char* database, const char* password )
  {
    assert( db );
#ifdef SQLITE_HAS_CODEC
    if ( password ) {
      int result = sqlite3_key_v2(db, NULL, password, (int)strlen(password) );
      switch ( result ) {
        case SQLITE_OK:
          assert( countTables( db, database ) > 0 );
          return true;
        case SQLITE_NOTADB:
          ejin::_throw_exception(ejin::ACCESS_DENIED_EXCEPTION, "database is encrypted");
        default:
          ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Sqlite error code: %s", sqlite3_errmsg(db));
      }
    }
#endif
    return false;
  }
  bool Database::rekey( sqlite3* db, const char* database, const char* password )
  {
    assert( db );
#ifdef SQLITE_HAS_CODEC
    if ( password ) {
      EXEC_SQLITE_STATEMENT( sqlite3_rekey_v2(db, database, password, (int)strlen(password) ), db );
      return true;
    }
#endif
    return false;
  }
  
  void Database::setForeignKeySupport( sqlite3* db, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"PRAGMA %s.foreign_keys = ON", database);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd, NULL, NULL, NULL), db );
  }
  void Database::setEncoding( sqlite3* db, const char* encoding, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"PRAGMA %s.encoding=\"%s\"", database, encoding);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db,cmd , NULL, NULL, NULL), db );
  }
  void Database::setCacheSize( sqlite3* db, int size, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"PRAGMA %s.cache_size=%d", database, size);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd , NULL, NULL, NULL), db );
  }
  void Database::setLockingMode( sqlite3* db, SQLITE3LockingMode mode, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"PRAGMA %s.locking_mode=%d", database, mode);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd , NULL, NULL, NULL), db);
  }
  void Database::setAutoVacuum( sqlite3* db, SQLITE3AutoVacuum mode, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"PRAGMA %s.auto_vacuum=%d", database, mode);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd , NULL, NULL, NULL), db);
  }
  void Database::incrementalVacuum( sqlite3* db, int freelist, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"PRAGMA %s.incremental_vacuum( %d )", database, freelist);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd , NULL, NULL, NULL), db);
  }
  void Database::vacuum( sqlite3* db )
  {
    assert( db );
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db,"VACUUM", NULL, NULL, NULL), db);
  }
  int Database::countTables( sqlite3* db, const char* database )
  {
    assert( db );
    // get current database version of schema
    static sqlite3_stmt *stmt;
    int count = 0;
    
    char cmd[128];
    snprintf(cmd,sizeof(cmd),"select count(name) from %s.sqlite_master where type='table'", database);
    switch ( sqlite3_prepare_v2(db, cmd, -1, &stmt, NULL) ) {
      case SQLITE_OK:
        if (sqlite3_step(stmt) == SQLITE_ROW) {
          count = sqlite3_column_int(stmt, 0);
        }
        break;
      case SQLITE_NOTADB:
        ejin::_throw_exception(ejin::ACCESS_DENIED_EXCEPTION, "database is encrypted");
      default:
        ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Sqlite error code: %s", sqlite3_errmsg(db));
    }
    sqlite3_finalize(stmt);
    
    return count;
  }
  bool Database::rememberHMAC( sqlite3* db, const char* password, const char* database )
  {
    assert( db );
    if ( password ) {
      // remember new hashed key with current timestamp
      time_t current_time;
      current_time = time(NULL); // current time as seconds elapsed since the Epoch.
      assert(current_time != ((time_t)-1));
      
      AESCrypto crypto;
      crypto.setHmacSessionKeyFrom( password );
      
      char cmd[256];
      snprintf(cmd,sizeof(cmd),"INSERT INTO %s.key_tbl (hmac_key,create_time) VALUES ('%s',%ld)",
               database, crypto.getSessionKey().c_str(), current_time);
      EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd, NULL, NULL, NULL), db);
      return true;
    }
    return false;
  }
  
  int Database::getPageSize( sqlite3* db, const char* database )
  {
    assert( db );
    // get current database version of schema
    static sqlite3_stmt *stmt_version;
    int result = -1;
    
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.page_size", database );
    if (sqlite3_prepare_v2(db, cmd, -1, &stmt_version, NULL) == SQLITE_OK) {
      if (sqlite3_step(stmt_version) == SQLITE_ROW) {
        result = sqlite3_column_int(stmt_version, 0);
      }
    } else {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Read page_size: %s\n", sqlite3_errmsg(db));
    }
    sqlite3_finalize(stmt_version);
    return result;
  }
  int Database::getPageCount( sqlite3* db, const char* database )
  {
    assert( db );
    // get current database version of schema
    static sqlite3_stmt *stmt_version;
    int result = -1;
    
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.page_count", database );
    if (sqlite3_prepare_v2(db, cmd, -1, &stmt_version, NULL) == SQLITE_OK) {
      if (sqlite3_step(stmt_version) == SQLITE_ROW) {
        result = sqlite3_column_int(stmt_version, 0);
      }
    } else {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Read page_count: %s\n", sqlite3_errmsg(db));
    }
    sqlite3_finalize(stmt_version);
    return result;
  }
  int Database::getFreeListCount( sqlite3* db, const char* database )
  {
    assert( db );
    // get current database version of schema
    static sqlite3_stmt *stmt_version;
    int result = -1;
    
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.freelist_count", database );
    if (sqlite3_prepare_v2(db, cmd, -1, &stmt_version, NULL) == SQLITE_OK) {
      if (sqlite3_step(stmt_version) == SQLITE_ROW) {
        result = sqlite3_column_int(stmt_version, 0);
      }
    } else {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Read freelist_count: %s\n", sqlite3_errmsg(db));
    }
    sqlite3_finalize(stmt_version);
    return result;
  }
  int Database::getUserVersion( sqlite3* db, const char* database )
  {
    assert( db );
    // get current database version of schema
    static sqlite3_stmt *stmt_version;
    int databaseVersion = 0;
    
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.user_version", database );
    if (sqlite3_prepare_v2(db, cmd, -1, &stmt_version, NULL) == SQLITE_OK) {
      if (sqlite3_step(stmt_version) == SQLITE_ROW) {
        databaseVersion = sqlite3_column_int(stmt_version, 0);
      }
    } else {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Read user version: %s\n", sqlite3_errmsg(db));
    }
    sqlite3_finalize(stmt_version);
    return databaseVersion;
  }
  void Database::setUserVersion( sqlite3* db, int version, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.user_version=%d", database, version);
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd , NULL, NULL, NULL), db);
  }
  
  bool Database::isExclusive( sqlite3* db, const char* database )
  {
    assert( db );
    // get current database version of schema
    static sqlite3_stmt *stmt_version;
    int isExclusive = 0;
    
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.locking_mode", database );
    if (sqlite3_prepare_v2(db, cmd, -1, &stmt_version, NULL) == SQLITE_OK) {
      if (sqlite3_step(stmt_version) == SQLITE_ROW) {
        isExclusive = sqlite3_column_int(stmt_version, 0);
      }
    } else {
      ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION,"Read locking mode: %s\n", sqlite3_errmsg(db));
    }
    sqlite3_finalize(stmt_version);
    return isExclusive != 0;
  }
  void Database::setExclusive( sqlite3* db, bool exclusive, const char* database )
  {
    assert( db );
    char cmd[128];
    snprintf(cmd, sizeof(cmd), "PRAGMA %s.locking_mode=%s", database, exclusive ? "EXCLUSIVE" : "NORMAL");
    EXEC_SQLITE_STATEMENT( sqlite3_exec(db, cmd , NULL, NULL, NULL), db);
    // https://www.sqlite.org/pragma.html#pragma_locking_mode
    // The first time the database is read in EXCLUSIVE mode, a shared lock is obtained and held.
    // The first time the database is written, an exclusive lock is obtained and held.
    // Simply setting the locking-mode to NORMAL is not enough - locks are not released until the next time the database file is accessed.
    snprintf(cmd,sizeof(cmd),"SELECT name FROM %s.sqlite_master WHERE type='table'", database);
    EXEC_SQLITE_STATEMENT (sqlite3_exec(db,cmd , NULL, NULL, NULL), db );
  }

  sqlite3_stmt* Database::prepareTxBeginStatement( sqlite3* db )
  {
    assert( db );
    string sql = "BEGIN TRANSACTION";
    sqlite3_stmt *stmt;
    EXEC_SQLITE_STATEMENT( sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL), db );
    return stmt;
  }
  sqlite3_stmt* Database::prepareTxCommitStatement( sqlite3* db )
  {
    assert( db );
    string sql = "COMMIT TRANSACTION";
    sqlite3_stmt *stmt;
    EXEC_SQLITE_STATEMENT( sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL), db );
    return stmt;
  }
  sqlite3_stmt* Database::prepareTxRollbackStatement( sqlite3* db )
  {
    assert( db );
    string sql = "ROLLBACK TRANSACTION";
    sqlite3_stmt *stmt;
    EXEC_SQLITE_STATEMENT( sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL), db );
    return stmt;
  }
  
  // ----------
  // debugging
  
  ostream& operator<<(ostream& ostr, const Database& rhs)
  { return ostr << " " << rhs.toString() << " "; }
  
}
