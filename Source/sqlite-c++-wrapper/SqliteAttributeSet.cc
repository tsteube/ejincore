/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteAttributeSet.h"
#include "SqliteAttribute.h"

namespace sqlite
{
  // ==========
  // CTOR
  // ==========
  
  AttributeSet::AttributeSet( Attribute* definitions ): vec_(), map_()
  {
    init( definitions );
  }
  AttributeSet::AttributeSet( Attribute* definitions1, Attribute* definitions2 ): vec_(), map_()
  {
    init( definitions1 );
    init( definitions2 );
  }
  AttributeSet::AttributeSet( const vector<Attribute>& definition ): vec_(), map_()
  {
    copy(definition);
  }  
  AttributeSet::AttributeSet( const AttributeSet& source ): vec_(), map_()
  {
    copy(source.vec_);
  }  
  bool AttributeSet::operator==(const AttributeSet& rhs) const 
  {
    if (this == &rhs)
      return true;
    return this->vec_ == rhs.vec_;
  }  
  void AttributeSet::init( Attribute* definitions )
  {
    if (definitions) {
      size_t offset = this->vec_.size();
      for (int index=0; definitions[index]; index++) {
        definitions[index].index_ = (int)offset + index;
        this->vec_.push_back( definitions[index] );
        this->map_[definitions[index].name_] = &definitions[index];
      }
    }
  }

  // ==========
  // Public Interface
  // ==========
  
  const string AttributeSet::toString() const
  {
    string s;
    
    for (int index = 0; index < count(); index++)
    {
      if (const Attribute* f = byIndex(index))
      {
        s += f->name();
        if (index < (count() - 1))
          s += ", ";
      }
    }
    
    return s;
  }

  // ==========
  // Private Interface
  // ==========    
  
  void AttributeSet::copy(const vector<Attribute>& definition)
  {
    this->vec_ = definition;
    
    //make fields map from vector
    for (int index = 0; index < (int)vec_.size(); index++)
    {
      Attribute& field = this->vec_[index];
      this->map_[field.name()] = &field;
    }
  }
  
}

