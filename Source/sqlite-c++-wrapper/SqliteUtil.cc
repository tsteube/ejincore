/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteUtil.h"
#include "SqliteCommon.h"

#include <iostream>
#include <sstream>

extern "C" {
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
}

const char* const ISO_8601_FORMAT = "%Y-%m-%dT%H:%M:%S%z";
//const char* const ISO_8601_FORMAT2 = "%FT%T%z";

namespace sqlite
{

  using std::string;
  
  // ==========
  // CTOR
  // ==========
  
  jtime::jtime(): value_(), milliSeconds_(0)
  {
    timeval curTime;
    if (::gettimeofday(&curTime, NULL) == -1) {
      throw "Failed to resolve current local time";
    }
    this->milliSeconds_ = curTime.tv_usec % 1000;
    time_t t = curTime.tv_sec;
    memset(&this->value_, 0, sizeof (struct tm));
    ::localtime_r(&t, &this->value_);
  }
  jtime::jtime(const double julian): value_(), milliSeconds_(0)
  {
    // conversion from julian day to millis since epoch
    time_t t = (time_t) (( julian - 2440587.5 ) * 86400000.0);
    memset(&this->value_, 0, sizeof (struct tm));
    ::localtime_r(&t, &this->value_);
  }
  jtime::jtime(const jtime& value): value_(value.value_), milliSeconds_(value.milliSeconds_)
  {  }
  jtime::jtime(const time_t value): value_(), milliSeconds_(0)
  {
    memset(&this->value_, 0, sizeof (struct tm));
    ::localtime_r(&value, &this->value_);
    this->milliSeconds_ = 0;
  }
#if defined(__i386__) || defined(__ppc__) || defined(__arm__)
  jtime::jtime(const int32_t value): value_(), milliSeconds_(0)
  {
    time_t t = (time_t) (value);
    memset(&this->value_, 0, sizeof (struct tm));
    ::localtime_r(&t, &this->value_);
    this->milliSeconds_ = 0;
  }
#else
#if defined(__x86_64__) || defined(__ppc64__) || defined(__arm64__)
  jtime::jtime(const int64_t value): value_(), milliSeconds_(0)
  {
    time_t t = (time_t) (value / 1000);
    memset(&this->value_, 0, sizeof (struct tm));
    ::localtime_r(&t, &this->value_);
    this->milliSeconds_ = value % 1000;
  }
#else
#error UNKNOWN ARCHITECTURE
#endif
#endif
  jtime::jtime(const struct tm& value): value_(), milliSeconds_(0)
  {
    this->value_ = value;
  }  
  jtime::jtime(const char* value): value_(), milliSeconds_(0)
  {
    if (! parseISO8601(value) ) 
      throw "Failed to parse ISO 8601 time string";
  }
  jtime::jtime(const char* val, const char* pattern): value_(), milliSeconds_(0)
  {
    if (! parse(val, pattern) ) 
      throw "Failed to parse time string";
  }  
  jtime::~jtime()
  {
  }
  jtime& jtime::operator=(const jtime& value)
  {
    if (this != &value)
    {
      this->value_ = value.value_;
      this->milliSeconds_ = value.milliSeconds_;
    }
    return *this;
  }
  bool jtime::equals( const jtime& rhs ) const
  {
    if (this == &rhs)
      return false;
    
    return memcmp(&this->value_, &rhs.value_, sizeof (struct tm)) == 0 &&
      this->milliSeconds_ == rhs.milliSeconds_;
  }
  bool jtime::lessThan( const jtime& rhs ) const
  {
    if (this == &rhs)
      return false;
    
    double seconds = difftime( const_cast<jtime *>(this)->time(),const_cast<jtime &>(rhs).time() );
    if (seconds == 0) {
      return this->milliSeconds_ < rhs.milliSeconds_;
    }
    return seconds < 0;
  }

  // ==========
  // Public Interface
  // ==========
  
  double jtime::julianDay() const
  {
    // conversion from millis since epoch to julian day
    return ( this->time() / 86400000.0 ) + 2440587.5;
  }
#if defined(__i386__) || defined(__ppc__) || defined(__arm__)
  integer jtime::integer() const
  {
    time_t t = mktime(const_cast<struct tm *>(&this->value_));
    return t;
  }
#else
#if defined(__x86_64__) || defined(__ppc64__) || defined(__arm64__)
  integer jtime::integer() const
  {
    time_t t = mktime(const_cast<struct tm *>(&this->value_));
    return t*1000 + this->milliSeconds_;
  }
#else
#error UNKNOWN ARCHITECTURE
#endif
#endif
  time_t jtime::time() const
  { 
    return mktime(const_cast<struct tm *>(&this->value_));
  }
  const struct tm* jtime::tm() const
  { 
    return &this->value_; 
  }
  
  jtime jtime::truncToDay() const
  {
    jtime clone(*this);
    clone.milliSeconds_ = 0;
    clone.value_.tm_sec = 0;
    clone.value_.tm_min = 0;
    clone.value_.tm_hour = 0;
    return clone;
  }
  bool jtime::isToday() const
  {
    timeval curTime;
    if (::gettimeofday(&curTime, NULL) == -1) {
      throw "Failed to resolve current local time";
    }
    time_t t = curTime.tv_sec;
    struct tm current;
    ::localtime_r(&t, &current);
    
    return this->value_.tm_mday == current.tm_mday;
  }
  void jtime::addMinutes(int count)
  { 
    this->value_.tm_min += count; 
  }
  void jtime::addHours(int count)
  { 
    this->value_.tm_hour += count; 
  }
  void jtime::addDays(int count)
  { 
    this->value_.tm_mday += count; 
  }
  
  // ISO 8601:2004 section 4.2.5.1 - pattern with timezone
  string jtime::asISO8601() const
  {
    char buffer[32];
    return asISO8601(buffer, 32);
  }  
  char* jtime::asISO8601(char* buffer, size_t maxLength) const
  {
    format(buffer, ISO_8601_FORMAT, maxLength); 
    size_t len = strlen(buffer);
    // insert ':' in timezone offset
    if (len + 1 < maxLength) {
      buffer[len+1] = '\0';
      buffer[len] = buffer[len-1];
      buffer[len-1] = buffer[len-2];
      buffer[len-2] = ':';
    }
    return buffer;
  }
  bool jtime::parseISO8601(const char* value)
  {
    assert (value);
    
    char isoDateTime[32];
    memset(isoDateTime, 0, sizeof (isoDateTime));

    // prepare input 
    
    const char* ptr = value;
    while (::isspace(*ptr)) ptr++; // trim leading white spaces
    strncpy(isoDateTime, ptr, 19);  

    ptr = value + strlen(value) - 1;
    while (::isspace(*ptr)) ptr--; // trim white spaces at the end
    if (*ptr == 'Z') { // special sign for UTC
      strcat(isoDateTime, "+0000");
    } else if ( *(ptr-2) == ':' ) { // special sign in timezone offset
      strncat(isoDateTime, ptr-5, 3);
      strncat(isoDateTime, ptr-1, 2);
    } else { // expect timezone offset as [-+][0-9]{4}
      strcat(isoDateTime, ptr-5); 
    } 

    if (this->parse(isoDateTime, ISO_8601_FORMAT)) {
      char buffer[32];
      char buffer2[32];
      
      asISO8601(buffer, 32);
      jtime(jtime::integer()).asISO8601(buffer2, 32);
      
      //http://en.cppreference.com/w/cpp/io/manip/get_time
      
      return true;
    }
    return false;
  }
  
  // -------
  // HELPER
  // -------
  
  char* jtime::format(char* buffer, const char* fm, size_t maxLength) const
  {
    if (strftime(buffer, maxLength, fm, &this->value_) > 0) {
      return buffer;
    }
    return NULL;
  }
  
  bool jtime::parse(const char* value, const char* pattern)
  {
    assert (pattern);
    
    memset(&this->value_, 0, sizeof (struct tm));
    if (! strptime(value, pattern, &this->value_))
      return false;
    return true;
  }  
  
  void jtime::init(int* year, int* month, int* mday, int* hours, int* minutes, int* seconds, long* tzOffset)
  {
    time_t t;
    if (::time(&t) == -1) {
      throw "Failed to resolve current local time";
    }
    memset(&this->value_, 0, sizeof (struct tm));
    ::localtime_r(&t, &this->value_);
    if (year)
      this->value_.tm_year = *year;
    if (month)
      this->value_.tm_mon = *month;
    if (mday)
      this->value_.tm_mday = *mday;
    if (hours)
      this->value_.tm_hour = *hours;
    if (minutes)
      this->value_.tm_min = *minutes;
    if (seconds)
      this->value_.tm_sec = *seconds;
    if (tzOffset)
      this->value_.tm_gmtoff = *tzOffset;
  }    

  std::ostream& operator<<( std::ostream& ostr, const sqlite::jtime& rhs )
  {
    return ostr << rhs.asISO8601();
  }
}

