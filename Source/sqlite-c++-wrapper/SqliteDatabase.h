/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_DATABASE_H__
#define __SQLITE_DATABASE_H__

#include "SqliteCommon.h"

namespace sqlite
{
  /*
   * SQLite Database handle
   */
  class Database
  {
    friend class Table;
    friend class PreparedTable;

    // ==========
    // Public EM Interface
    // ==========
  public:
    enum TargetSchema { MainDatabase, ArchiveDatabase, BackupDatabase };
    enum Status { DatabaseOpen, DatabaseNotFound, DatabaseEncrypted, DatabaseCorrupted, DatabaseClose };
    
    Status open(int version,
                const char* mainPrototype, const char* mainDatabase,
                const char* attachmentPrototype, const char* attachmentDatabase,
                const char* keyProto, const char* key,
                const char* password = NULL, bool force = false );
    Status reopen( const char* password );
    void close( void );
    
    bool needsVacuumDb( const char* password );
    Status vacuumDb( const char* password );

    TargetSchema getTargetSchema() { return this->targetDb_; };
    void setTargetSchema( TargetSchema targetDb );

    Status attachBackupDb( const char* password );
    Status detachBackupDb( );
    bool isBackupOpen( void ) const { return this->isOpen() && archive_attached_; }
    
    Status attachArchiveDb( const char* password, bool force = false );
    Status detachArchiveDb( const char* password = NULL );
    bool isArchiveOpen( void ) const { return this->isOpen() && archive_attached_; }
    
    string toString( void ) const;
    
    const char* getMainFilename( void ) const { return db_ ? sqlite3_db_filename(db_, MAIN_DATABASE_NAME) : NULL; }
    const char* getBackupFilename( void ) const { return db_ ? sqlite3_db_filename(db_, BACKUP_DATABASE_NAME) : NULL; }
    const char* getArchiveFilename( void ) const { return db_ ? sqlite3_db_filename(db_, ARCHIVE_DATABASE_NAME) : NULL; }
    const char* getAttachmentFilename( void ) const{ return db_ ? sqlite3_db_filename(db_, ATTACHMENT_DATABASE_NAME) : NULL; }
    const char* getAttachmentArchiveFilename( void ) const{ return db_ ? sqlite3_db_filename(db_, ATTACHMENT_ARCHIVE_DATABASE_NAME) : NULL; }
    
    bool isOpen( void ) const           { return (result_open_ == SQLITE_OK); }
    bool isClean( void ) const;
    void vacuum( void )                 {  this->vacuum(db_); }
    const char* error( void ) const     { return isClean() ? NULL : sqlite3_errmsg(db_); }
    sqlite3* handle( void ) const       { return this->db_; }
    
    bool isAutoCommit( void );
    void transactionBegin( void );
    void transactionCommit( void );
    void transactionRollback( void );
    
    bool resetPassword( const char* oldPassword, const char* newPassword );
    
    const BaseEntity& prototype( const char* tableName );
    bool persist( BaseEntity& entity, const char* schema = NULL );
    bool insert( BaseEntity& entity, const char* schema = NULL );
    bool update( BaseEntity& entity, const char* schema = NULL );
    BaseEntity& refresh( BaseEntity& entity, const char* schema = NULL );
    bool contains( BaseEntity& entity, const char* schema = NULL );
    bool remove( BaseEntity& entity, const char* schema = NULL );
    long removeAll( const char* tableName, const char* schema = NULL );
    long count( const char* tableName, const char* schema = NULL );
    std::unique_ptr<ResultSet> find( const char* tableName, const char* finder, ... );
    std::unique_ptr<Value> valueOf( const char* tableName, const char* sql, ... );
    std::unique_ptr<Value> valueOfWithDefault( const char* tableName, const char* sql, const Value* defValue, ... );

    bool cutBackup( const char* tableName );
    bool backup( const char* tableName, int dummy, ... );
    bool restore( const char* tableName, int dummy, ... );
    
    const void operator[] ( const int nIndex ) const;
    
    // ==========
    // Abstract Method
    // ==========
  protected:
    
    map<string,Table*> _tableToEntity;
    
    virtual void upgradeDatabase( const char* database, int version ) = 0;
    virtual void initEntityManager( void ) = 0;
    
    Database( void );
    virtual ~Database( void );
    
    bool createDbFileIfNotExists( const char* database, const char* password, const char* prototype, bool force );
    bool useKeyFileIfExists( const char* keyFile, const char* password, const char* keyFileProto, bool force );
    Status openDb( const char* mainDatabse, const char* attachmentDatabase, const char* password = NULL );
    Status vacuumDb( const char* database,  const char* password );
    bool needsVacuum( sqlite3* db, const char* database = MAIN_DATABASE_NAME ) const;
    bool rekey( const char* filename, const char* newPassword, const char* database = MAIN_DATABASE_NAME  );

    // ==========
    // Protected static Method
    // ==========
    
    static bool createDbFrom( const char* fromFilename, const char* toFilename, const char* password = NULL );
    
    static void cipherExport( sqlite3* db, const char* database );
    
    static void dropFTS5Support( sqlite3* db, const char* database );
    static void dropGenIDSupport( sqlite3* db, const char* database );
    static void dropAllTrigger( sqlite3* db, const char* database );

    static void addCustomFunctions( sqlite3* db );
    
    static bool createBackupDbIfNecessary( const char* dbFilename, const char* bakFileName );
    
    static void attachDatabase( sqlite3* db, const char* filename, const char* database, const char* password );
    static void detachDatabase( sqlite3* db, const char* database );
    
    static bool setKey( sqlite3* db, const char* password, const char* database = MAIN_DATABASE_NAME );
    static bool rekey( sqlite3* db, const char* password, const char* database = MAIN_DATABASE_NAME );
    
    static void setForeignKeySupport( sqlite3* db, const char* database = MAIN_DATABASE_NAME );
    static void setEncoding( sqlite3* db, const char* encoding, const char* database = MAIN_DATABASE_NAME );
    static void setCacheSize( sqlite3* db, int size, const char* database = MAIN_DATABASE_NAME );
    static void setLockingMode( sqlite3* db, SQLITE3LockingMode mode, const char* database = MAIN_DATABASE_NAME );
    static void setAutoVacuum( sqlite3* db, SQLITE3AutoVacuum mode, const char* database = MAIN_DATABASE_NAME );
    static void vacuum( sqlite3* db );
    static void incrementalVacuum( sqlite3* db, int freelist, const char* database = MAIN_DATABASE_NAME );

    static bool rememberHMAC( sqlite3* db, const char* password, const char* database = MAIN_DATABASE_NAME );
    
    static int getUserVersion( sqlite3* db, const char* database = MAIN_DATABASE_NAME );
    static void setUserVersion( sqlite3* db, int version, const char* database = MAIN_DATABASE_NAME );
    
    static int getPageSize( sqlite3* db, const char* database = MAIN_DATABASE_NAME );
    static int getPageCount( sqlite3* db, const char* database = MAIN_DATABASE_NAME );
    static int getFreeListCount( sqlite3* db, const char* database = MAIN_DATABASE_NAME );

    static int countTables( sqlite3* db, const char* database = MAIN_DATABASE_NAME );
    
    static bool isExclusive( sqlite3* db, const char* database = MAIN_DATABASE_NAME );
    static void setExclusive( sqlite3* db, bool exclusive, const char* database = MAIN_DATABASE_NAME );

    static sqlite3_stmt* prepareTxBeginStatement( sqlite3* db );
    static sqlite3_stmt* prepareTxRollbackStatement( sqlite3* db );
    static sqlite3_stmt* prepareTxCommitStatement( sqlite3* db );
    
    // ==========
    // Private Variables
    // ==========
  private:
    
    sqlite3* db_;
    int version_;
    TargetSchema targetDb_;
    int result_open_;
    bool backup_attached_;
    bool archive_attached_;
    string mainPrototype;
    string attachmentPrototype;    
    
    // ------------
    // prepared statments
    
    sqlite3_stmt*  preparedTxBeginStatement_;
    sqlite3_stmt*  preparedTxRollbackStatement_;
    sqlite3_stmt*  preparedTxCommitStatement_;
    
    // undefined copy ctor
    Database( const Database& rhs );
    // undefined assignment
    Database& operator=( const Database& rhs );
    
  };
  
  // debugging
  ostream& operator<<(ostream& ostr, const Database& rhs);
  
}
#endif // __SQLITE_DATABASE_H__
