/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_TABLE_H__
#define __SQLITE_TABLE_H__

#include "SqlitePreparedTable.h"

namespace sqlite
{
  // forward declaration
  class ResultSet;
  
  // ==============
  // Table: Base table implementation to support entity manager
  // ==============
  
  class Table: public PreparedTable {
    // friend classes
    friend class Database;
    
    // ==========
    // Constructor
    // ==========
  public:
    
    // ctor
    Table( const Database& db, BaseEntity* proto ): PreparedTable(db, proto) { };
    // copy ctor
    Table( const Table& table ): PreparedTable(table) {};
    // dtor
    ~Table() {};

    // ==========
    // CRUD methods
    // ==========
  public:
    
    BaseEntity& refresh( BaseEntity& entity, const char* schema );
    bool contains( BaseEntity& entity, const char* schema );
    bool persist( BaseEntity& entity, const char* schema );
    bool insert( BaseEntity& entity, const char* schema );
    bool update( BaseEntity& entity, const char* schema );
    bool remove( BaseEntity& entity, const char* schema );
    long count( const char* schema ) const;
    long removeAll( const char* schema  );
    // Note the result entities must be delete by caller
    unique_ptr<ResultSet> find( const char* schema, const char* finder, va_list conditions );
    unique_ptr<Value> valueOf( const char* schema, const char* sqlAlias, va_list conditions );
    virtual bool cutBackup( ) { return false; }
    virtual bool backup( va_list conditions ) { return false; }
    virtual bool restore( va_list conditions ) { return false; }
    vector< pair<field_type,const Value*> > toBindVariables( va_list conditions );
    
  };
}

#endif // __SQLITE_TABLE_H__
