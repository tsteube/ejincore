/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_ATTRIBUTE_H__
#define __SQLITE_ATTRIBUTE_H__

#include "SqliteValue.h"

/**
 * SQL attribute class for SQLite.
 */
namespace sqlite
{
  class Attribute
  {
  private:
    friend class AttributeSet;
    
    // ==========
    // CTOR
    // ==========
  public:

    Attribute( void );
    Attribute( string name, field_type type, int flags = flag_none, const Value& defValue = Value(), const char* mapping[] = NULL );
    Attribute( const Attribute& value );
    
    Attribute& operator=(const Attribute& rhs);
    bool operator==(const Attribute& rhs) const;
    bool operator!=(const Attribute& rhs) const
    { return ! (*this == rhs); }
    operator bool() const
    { return ! this->name_.empty(); }

    // ==========
    // Public Interface
    // ==========
  public:        

    bool isKeyIdField( void ) const
    { return (this->use_ == FIELD_KEY); }
    
    field_type type( void ) const
    { return this->type_; }
    
    const char** mapping( void ) const
    { return this->mapping_; }
    
    int index( void ) const
    { return this->index_; }
    
    const string name( void ) const
    { return this->name_; }
    
    bool isPrimaryKey( void ) const
    { return ((this->flags_ & flag_primary_key) == flag_primary_key); }
    
    bool isNullable( void ) const
    { return ((this->flags_ & flag_not_null) != flag_not_null); }

    bool isTransient( void ) const
    { return ((this->flags_ & flag_transient) == flag_transient); }

    // ==========
    // Private Interface
    // ==========    
  private:
    
    string name_;
    field_use use_;
    field_type type_;
    int index_;
    int flags_;
    const char** mapping_;
    const Value defValue_;
    
  };
  
}
#endif //__SQLITE_ATTRIBUTE_H__
