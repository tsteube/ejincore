/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_VALUE_H__
#define __SQLITE_VALUE_H__

#include "SqliteCommon.h"

namespace sqlite
{
  /**
   * Generic value class for SQLite attributes.
   */
  class Value
  {
    
    // ==========
    // Public Interface
    // ==========
  public:
    
    // ctors  
    Value(void);
    ~Value(void);
    
    Value(field_type type, const char* mapping[] = NULL);
    /*explicit*/ Value(integer value);
    /*explicit*/ Value(real value);
    /*explicit*/ Value(bool value);
    /*explicit*/ Value(const char* value);
    /*explicit*/ Value(const char* value, size_t size);
    /*explicit*/ Value(const string& value);
    /*explicit*/ Value(const jtime& value);
    
    // copy ctor
    Value(const Value& rhs);
    
    // assignment / comparision
    Value& operator=(const Value& rhs);
    bool operator==(const Value& rhs) const;
    bool operator!=(const Value& rhs) const;

    // ==========
    // Public Interface
    // ==========
  public:
    
    field_type type(void) const { return this->type_; }
    const string toString(void) const;

    bool isNull(void) const { return this->isNull_; }
    const char* str(void) const;
    const char* bytea(void) const;
    const size_t size(void) const;
    const integer num(void) const;
    const real dbl(void) const;
    const bool bol(void) const;
    const jtime date(void) const;
    
    void setNull();
    void str(const string& value);
    void bytea(const char* value, size_t size);
    void append(const char* value, size_t size);
    void num(integer value);
    void dbl(real value);
    void bol(bool value);
    void date(jtime value);
    
    // ==========
    // Conversion Interface
    // ==========
  public:
    
    static char* strdup(const string& string);
    static char* strdup(const char* string);
    static char* strndup(const char* string, size_t size);
    static wchar_t* mbstowcs_alloc(const char *string);
    static char* wcstombs_alloc(const wchar_t* string);
    static wchar_t* wcsdup(const wchar_t* string);
    
    // ==========
    // Private Member
    // ==========
  private:
    
    field_type type_;
    const char** mapping_;
    bool isNull_;
    // use native value to store value
    mutable char* sValue_;
    mutable size_t sSize_;
    mutable union {
      // type_bytea and type_text
      struct {
        char* v;
        size_t l;
      } s;
      // type_int
      integer d;
      // type_float
      // type_time
      real f;
      // type_bool
      bool b;
    } val_;  
  };
  
} //class Value

#endif // __SQLITE_VALUE_H__
