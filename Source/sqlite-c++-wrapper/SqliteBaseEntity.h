/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQL_BASE_ENTITY_H__
#define __SQL_BASE_ENTITY_H__

#include "SerializableEntity.h"
#include "SqliteCommon.h"

using ser::XmlStream;

#define ACCESS_INTEGER_PROPERTY_INTF(name,index) \
const integer get## name () const { return asInteger(index); }  \
void set## name (const integer i) { return asInteger(index, i); } \
bool has## name () const { return ! isNull(index); } \
void clear## name () { return setNull(index); }

#define ACCESS_DOUBLE_PROPERTY_INTF(name,index) \
const double get## name () const { return asDouble(index); }  \
void set## name (const double i) { return asDouble(index, i); } \
bool has## name () const { return ! isNull(index); } \
void clear## name () { return setNull(index); }

/*
 #define ACCESS_BYTEA_PROPERTY_INTF(name,index) \
 const char* get## name ( void ) const { return asBytea( index ); } \
 bool has## name ( void ) const { return ! isNull( index ); } \
 void clear## name ( void ) { setNull( index ); } \
 size_t get## name ##Size( void ) const { return size( index ); } \
 void set## name ( const char* data, size_t length ) { asBytea(index, data, length); }
 */

#define ACCESS_BYTEA_PROPERTY_INTF(name, sizeIndex, dataIndex) \
const char* get## name ( void ) const { return asBytea( dataIndex ); } \
bool has## name ( void ) const { return asInteger( sizeIndex ) > 0; } \
void clear## name ( void ) { setNull( dataIndex ); setNull( sizeIndex ); } \
size_t get## name ##Size( void ) const { return asInteger( sizeIndex ); } \
void set## name ( const char* data, size_t length ) { asBytea(dataIndex, data, length); asInteger(sizeIndex, length); }

#define ACCESS_STRING_PROPERTY_INTF(name,index) \
const std::string get## name () const { return asString(index); }  \
void set## name (const std::string& i) { return asString(index, i); } \
bool has## name () const { return ! isNull(index); } \
void clear## name () { return setNull(index); }

#define ACCESS_TIME_PROPERTY_INTF(name,index) \
const sqlite::jtime get## name () const { return asTime(index); }  \
void set## name (const sqlite::jtime& t) { return asTime(index, t); } \
bool has## name () const { return ! isNull(index); } \
void clear## name () { return setNull(index); }

#define ACCESS_BOOLEAN_PROPERTY_INTF(name,index) \
const bool is## name () const { return asBool(index); }  \
void set## name (const bool b) { return asBool(index, b); } \
bool has## name () const { return ! isNull(index); } \
void clear## name () { return setNull(index); }

#define COPY_PROPERTY(lhs, rhs, name) COPY_PROPERTY2( lhs, rhs, name, name )

#define COPY_PROPERTY2(lhs, rhs, lname, rname) \
if (rhs.has ## rname ()) \
lhs.set## lname ( rhs.get## rname () ); \
else \
lhs.clear## lname ();

#define COPY_BYTEA_PROPERTY(lhs, rhs, name) \
lhs.set## name ( rhs.get## name (), rhs.get## name ##Size() );

#define COPY_BOOL_PROPERTY(lhs, rhs, name) \
if (rhs.has ## name ()) \
lhs.set## name ( rhs.is## name () ); \
else \
lhs.clear## name ();

#define CLONE_SET_PROPERTY(type, lhs, rhs, name) \
for(auto it = rhs.name.begin(); it != rhs.name.end(); it++) { \
lhs.name.insert(shared_ptr< type >(new type (**it))); \
}
#define CLONE_LIST_PROPERTY(type, lhs, rhs, name) \
for(auto it = rhs.name.begin(); it != rhs.name.end(); it++) { \
lhs.name.push_back(shared_ptr< type >(new type (**it))); \
}

#define PROPERTY_EQUALS( name, rhs ) \
(this->has## name () && rhs.has## name () && this->get## name () == rhs.get## name ())

// forward declaration
class DBUnit;

namespace sqlite
{
  
  // forward declaration
  class Table;
  class Value;
  class ResultSet;
  
  /**
   * Base Class for all entity classes map to a relation row.
   */
  class BaseEntity: public ser::SerializableEntity {
    const static string                  XML_NAME;
    const static ser::XmlAttribute       XML_FIELDS[];
    const static ser::XmlAttributeSet    XML_DDL;
    
    // friend classes
    friend class Table;
    friend class DBUnit;
    
    friend bool operator==(const BaseEntity& lhs, const BaseEntity& rhs);
    friend bool operator< (const BaseEntity& lhs, const BaseEntity& rhs);
    friend ostream& operator<<(ostream& ostr, const BaseEntity& rhs);
    
    // ==========
    // Public Interface
    // ==========
  public:
    enum TargetSchema { MainDatabase, BackupDatabase, ArchiveDatabase };
    
    // ==========
    // CTORs
    // ==========
    
    // default ctor
    BaseEntity(const AttributeSet& fields, bool readOnly);
    // copy ctor
    BaseEntity(const BaseEntity& record);
    // dtor
    virtual ~BaseEntity(void);
    // assignment
    BaseEntity& operator=(const BaseEntity& rhs);
    
    // ==========
    // Abstract Interface
    // ==========
  public:
    
    // debug
    virtual string toString( void ) const;
    // clone this instance
    virtual BaseEntity* clone( void ) const = 0;
    /** concrete DB relation name */
    virtual const char* tableName( void ) const = 0;
    /** concrete DB relation name */
    virtual const char* mainSchemaName( void ) const { return MAIN_DATABASE_NAME; }
    virtual const char* backupSchemaName( void ) const { return BACKUP_DATABASE_NAME; }
    virtual const char* archiveSchemaName( void ) const { return ARCHIVE_DATABASE_NAME; }
    virtual const AttributeSet& tableFields( void ) const = 0;
    /** map of finder name to SQL WHERE condition on this tablename */
    virtual const map<string,string> finderTemplates( const char* schema ) const { return map<string,string>(); };
    /** map of sql statments that must return a single value */
    virtual const map<string,string> sqlAliases( const char* schema ) const { return map<string,string>(); };
    /** map of finder name to SQL WHERE condition on this tablename */
    virtual const string restoreSet( void ) const { return ""; }
    virtual bool useSequenceTable() const { return true; }
    virtual void finalize() const { }
    
    // ==========
    // Serialisation Interface
    // ==========
  public:
    ISerializableEntity* entity(string property, int index = 0) const { return NULL; }
    int size(string property) const { return 0; }
    
  protected:
    const string& xmlName() const { return XML_NAME; }
    const ser::XmlAttributeSet& xmlFields() const { return XML_DDL; }
    
    // ==========
    // Protected Interface
    // ==========
  protected:
    
    // comparision
    virtual bool equals( const BaseEntity& rhs ) const = 0;
    virtual bool lessThan( const BaseEntity& rhs ) const = 0;
    
    // ==========
    // Protected Methods
    // ==========
  public:

    // common business properties
    integer getId();
    void setId( integer id );
    ejin::SyncInd getSyncInd();
    void setSyncInd( ejin::SyncInd ind );
    
    // mapping field name to field index
    //int fieldByName(const string& fieldName) const;
    vector<Value*> keyValue() const;
    Value* value(string fieldName) const;
    Value* value(size_t index) const;
    const Attribute* byName(string fieldName) const;
    const Attribute* byIndex(int index) const;
    
    // ddl
    const AttributeSet& fields() const { return fields_; }
    size_t columnCount() const  { return values_.size(); }
    
    bool isReadOnly() const { return readOnly_; }
    
    // read field value by index and convert into different base types
    const bool isNull(int field) const;
    const char* asBytea(int field) const;
    const size_t size(int field) const;
    const string asString(int field) const;
    const integer asInteger(int field) const;
    const double asDouble(int field) const;
    const bool asBool(int field) const;
    const jtime asTime(int field) const;
    
    // write field by index and convert from different base types
    void setNull(int field);
    void asString(int field, const string& value);
    void asString(int index, const char* val, size_t length);
    void asBytea(int index, const char* val, size_t length);
    void asInteger(int field, const integer value);
    void asDouble(int field, const double value);
    void asBool(int field, const bool value);
    void asTime(int field, const jtime& value);
    
    // ==========
    // Private Methods
    // ==========
  private:
    
    const AttributeSet& fields_;
    bool readOnly_;
    vector<Value> values_;
    
  };
  
  // ----------
  // global methods
  
  // comparision
  inline bool operator==(const BaseEntity& lhs, const BaseEntity& rhs) { return lhs.equals(rhs); }
  inline bool operator!=(const BaseEntity& lhs, const BaseEntity& rhs) { return !operator==(lhs,rhs); }
  inline bool operator< (const BaseEntity& lhs, const BaseEntity& rhs) { return lhs.lessThan(rhs); }
  inline bool operator> (const BaseEntity& lhs, const BaseEntity& rhs) { return  operator< (rhs,lhs); }
  inline bool operator<=(const BaseEntity& lhs, const BaseEntity& rhs) { return !operator> (lhs,rhs); }
  inline bool operator>=(const BaseEntity& lhs, const BaseEntity& rhs) { return !operator< (lhs,rhs); }
  
  // debugging
  inline ostream& operator<<(ostream& ostr, const BaseEntity& rhs)  {return ostr << " " << rhs.toString() << " "; }
  
} //class BaseEntity

// used in std::set for sorting
namespace std
{
  template <typename T> struct less< T* >
  {
    bool operator() (const T* lhs, const T* rhs)
    {
      return *lhs < *rhs;
    }
  };
}

#endif // __SQL_BASE_ENTITY_H__
