/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteValue.h"

#include "SqliteUtil.h"
#include "Exceptions.h"

#include <stdlib.h>
#include <string.h>
#include <iostream>

#include <stdlib.h>
#include <xlocale.h>

namespace sqlite
{
  
  // ==========
  // CTOR
  // ==========
  
  Value::Value(): type_(type_text), mapping_(NULL), isNull_(true), sValue_(NULL), sSize_(0), val_() { }
	Value::Value(field_type t, const char* mapping[]): type_(t), mapping_(mapping), isNull_(true), sValue_(NULL), sSize_(0), val_() {
  }  
	Value::Value(integer value): type_(type_int), mapping_(NULL), isNull_(false), sValue_(NULL), sSize_(0), val_() {
    this->val_.d = value;
  }
	Value::Value(real value): type_(type_float), mapping_(NULL), isNull_(false), sValue_(NULL), sSize_(0), val_() {
    this->val_.f = value;
  }
	Value::Value(bool value): type_(type_bool), mapping_(NULL), isNull_(false), sValue_(NULL), sSize_(0), val_() {
    this->val_.b = value;
  }
	Value::Value(const char* value): type_(type_text), mapping_(NULL), isNull_(true), sValue_(NULL), sSize_(0), val_() {
    bytea(value, strlen(value));
  }
	Value::Value(const string& value): type_(type_text), mapping_(NULL), isNull_(true), sValue_(NULL), sSize_(0), val_() {
    str(value);
  }
	Value::Value(const char* value, size_t size): type_(type_bytea), mapping_(NULL), isNull_(true), sValue_(NULL), sSize_(0), val_() {
    bytea(value, size);
  }
	Value::Value(const jtime& value): type_(type_time), mapping_(NULL), isNull_(true), sValue_(NULL), sSize_(0), val_() {
    date(value);
  }
  Value::~Value() {
    setNull();
  }  
	Value::Value(const Value& rhs): type_(rhs.type_), mapping_(rhs.mapping_), isNull_(rhs.isNull_), sValue_(NULL), sSize_(0), val_() {
    if (!isNull_) {
      switch (type_) {
        case type_int:
        case type_time:
          this->val_.d = rhs.val_.d;
          break;
        case type_float:
          this->val_.f = rhs.val_.f;
          break;
        case type_text:
        case type_bytea:
          this->val_.s.l = rhs.val_.s.l;
          this->val_.s.v = Value::strndup(rhs.val_.s.v, rhs.val_.s.l);
          if (! val_.s.v)
            ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");            
          break;
        case type_bool:
          this->val_.b = rhs.val_.b;
          break;
        default:
          ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);            
      }
    }
    
    // string representation
    if ( rhs.sValue_ ) {
      this->sSize_ = rhs.sSize_;
      this->sValue_ = Value::strndup(rhs.sValue_, rhs.sSize_);
    }
  }
  
  Value& Value::operator=(const Value& rhs) {
    if (this != &rhs) {
      setNull();
      
      this->isNull_ = rhs.isNull_;
      this->type_ = rhs.type_;
      this->mapping_ = rhs.mapping_;
      if (!isNull_) {
        switch (type_) {
          case type_int:
          case type_time:
            this->val_.d = rhs.val_.d;
            break;
          case type_float:
            this->val_.f = rhs.val_.f;
            break;
          case type_text:
          case type_bytea:
            this->val_.s.l = rhs.val_.s.l;
            this->val_.s.v = Value::strndup(rhs.val_.s.v, rhs.val_.s.l);
            if (! val_.s.v)
              ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
            break;
          case type_bool:
            this->val_.b = rhs.val_.b;
            break;
          default:
            ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);            
        }
      }
      
      // string representation
      if ( rhs.sValue_ ) {
        this->sSize_ = rhs.sSize_;
        this->sValue_ = Value::strndup(rhs.sValue_, rhs.sSize_);
      }
    }
    return *this;
  }  
  bool Value::operator==(const Value& rhs) const {
    if (this->type_ == rhs.type_) {
      if (this->isNull_) {
        return rhs.isNull_;
      } else {
        if (rhs.isNull_)
          return false;
        
        switch (this->type_) {
          case type_int:
          case type_time:
            return this->val_.d == rhs.val_.d;
          case type_float:
            return this->val_.f == rhs.val_.f;
          case type_text:
          case type_bytea:
            return (val_.s.l == rhs.val_.s.l) && strncmp(val_.s.v, rhs.val_.s.v, val_.s.l) == 0;
          case type_bool:
            return this->val_.b == rhs.val_.b;
          default:
            ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);    
            return false;
        }
      }
    } else {
      return false;
    }
  }  
  bool Value::operator!=(const Value& rhs) const {
    return ! (*this == rhs);
  }  
  
  // ==========
  // Public Interface: Read methods
  // ==========
  
  const integer Value::num() const
  {
    if (this->isNull_) {
      return 0;
    }
    
    switch (this->type_) {
      case type_int:
      case type_time:
        return this->val_.d;
      case type_float:
        return (integer)this->val_.f;
      case type_text:
      case type_bytea:
        if ( this->val_.s.l > 0 && this->val_.s.l < 127) {
          // add terminated '\0' to parse as number
          char buffer[128];
          memcpy(buffer, this->val_.s.v, this->val_.s.l);
          buffer[this->val_.s.l] = '\0';
          char* endPtr;
          double v = strtod(buffer, &endPtr);
          if ((size_t)(endPtr - buffer) == this->val_.s.l) {
            return (integer)v;
          }
        }
        return 0;
      case type_bool:
        return this->val_.b ? 0 : 1;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
        return 0;
    }
  }
  const real Value::dbl() const
  {
    if (this->isNull_)
      return 0.0;
    
    switch (type_) {
      case type_int:
      case type_time:
        return (real) this->val_.d;
      case type_float:
        return this->val_.f;;
      case type_text:
      case type_bytea:
        if ( this->val_.s.l > 0 && this->val_.s.l < 127) {
          // add terminated '\0' to parse as number
          char buffer[128];
          memcpy(buffer, this->val_.s.v, this->val_.s.l);
          buffer[this->val_.s.l] = '\0';
          char* endPtr;
          double v = strtod(buffer, &endPtr);
          if ((size_t)(endPtr - buffer) == this->val_.s.l) {
            return v;
          }
        }
        return 0.0;
      case type_bool:
        return this->val_.b ? 1. : 0.;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
        return 0.0;
    }
  }
  const bool Value::bol() const
  {
    if (isNull_)
      return 0;
    
    switch (type_) {
      case type_int:
      case type_time:
        return this->val_.d != 0;
      case type_float:
        return this->val_.f != 0.0;
      case type_text:
      case type_bytea:
        if ( this->val_.s.l > 0 ) {
          if (this->val_.s.l==4 && strncasecmp("TRUE", this->val_.s.v, 4)==0 )
            return true;
          if (this->val_.s.l==1 && strncasecmp("T", this->val_.s.v, 1)==0 )
            return true;
          if (this->val_.s.l==3 && strncasecmp("YES", this->val_.s.v, 3)==0 )
            return true;
          if (this->val_.s.l==1 && strncasecmp("Y", this->val_.s.v, 1)==0 )
            return true;
          if (this->val_.s.l==2 && strncasecmp("ON", this->val_.s.v, 2)==0 )
            return true;
          
          if ( this->val_.s.l > 0 && this->val_.s.l < 127) {
            // add terminated '\0' to parse as number
            char buffer[128];
            memcpy(buffer, this->val_.s.v, this->val_.s.l);
            buffer[this->val_.s.l] = '\0';
            char* endPtr;
            double v = strtod(buffer, &endPtr);
            if ((size_t)(endPtr - buffer) == this->val_.s.l) {
              return v != 0.0;
            }
          }
        }
        return false;
      case type_bool:
        return this->val_.b;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
        return false;
    }
  }
  const jtime Value::date() const
  {
    if (this->isNull_)
      return (time_t)0;
    switch (this->type_) {
      case type_int:
      case type_time:
        return jtime( (integer) this->val_.d );
      case type_float:
        // conversion from julian day to millis since epoch
        return jtime( this->val_.f );
      case type_text:
      case type_bytea:
        if ( this->val_.s.l > 0 ) {
          char* endPtr;
          long v = strtol(this->val_.s.v, &endPtr, 10);
          if ((size_t)(endPtr - this->val_.s.v) == this->val_.s.l)
            return jtime( (time_t) v );
          return v;
        }
        return jtime( );
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
        return (time_t)0;
    }
  }
  const size_t Value::size(void) const
  {
    if (! this->isNull_) {
      switch (this->type_) {
        case type_text:
        case type_bytea:
          return this->val_.s.l;
        default:
          return 0;
      }
    }
    return 0;
  }
  const char* Value::bytea(void) const
  {
    if (! this->isNull_) {
      switch (this->type_) {
        case type_text:
        case type_bytea:
          return this->val_.s.v;
        default:
          return NULL;
      }
    }
    return NULL;
  }
  const char* Value::str() const
  {
    if ( this->isNull_ )
      return NULL;
    if ( this->sValue_ )
      return this->sValue_;
    
    // conversion into character string
    char buffer[128];
    switch (this->type_) {
      case type_int:
        snprintf(buffer, 128, "%ld", (long) this->val_.d);
        this->sSize_ = strlen( buffer );
        this->sValue_ = Value::strndup( buffer, this->sSize_ );
        break;
      case type_float:
        snprintf(buffer, 128, "%0.3f", this->val_.f);
        this->sSize_ = strlen( buffer );
        this->sValue_ = Value::strndup( buffer, this->sSize_ );
        break;
      case type_time:
        // conversion from julian day to millis since epoch
        jtime(this->val_.d).asISO8601( buffer, 128 );
        this->sSize_ = strlen( buffer );
        this->sValue_ = Value::strndup( buffer, this->sSize_ );
        break;
      case type_bool:
        if ( this->val_.b ) {
          this->sSize_ = 4;
          this->sValue_ = Value::strndup( "true", this->sSize_ );
        } else {
          this->sSize_ = 5;
          this->sValue_ = Value::strndup( "false", this->sSize_ );
        }
        break;
      case type_bytea:
      case type_text:
        this->sSize_ = this->val_.s.l;
        this->sValue_ = Value::strndup( this->val_.s.v, this->sSize_ );
        break;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
    }
    return this->sValue_;
  }
  const string Value::toString(void) const
  {
    this->str(); // initialize
    if ( this->sValue_ ) {
      return string(this->sValue_, this->sSize_);
    }
    return string();
  }

  // ==========
  // Public Interface: Write methods
  // ==========
  
  void Value::setNull() {
    if (! this->isNull_ && (this->type_ == type_text || this->type_ == type_bytea)) {
      // free char pointer
      if (this->val_.s.v)
        free(this->val_.s.v);
      this->val_.s.v = NULL;
      this->val_.s.l = 0;
    }
    // free string representation
    if ( this->sValue_ )
      free( this->sValue_ );
    this->sValue_ = NULL;
    this->sSize_ = 0;
    this->isNull_ = true;
  }
  void Value::num(integer value) {
    setNull();
    
    char buffer[128];
    switch (this->type_) {
      case type_int:
      case type_time:
        this->val_.d = value;
        break;
      case type_float:
        this->val_.f = value;
        break;
      case type_text:
      case type_bytea:
        sprintf(buffer,"%ld",(long int)value);
        this->val_.s.l = strlen(buffer);
        this->val_.s.v = Value::strndup(buffer, this->val_.s.l);
        if (! this->val_.s.v)
          ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
        break;
      case type_bool:
        this->val_.b = (value == 1);
        break;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
    }
    this->isNull_ = false;
  }
  void Value::dbl(real value) {
    setNull();
    
    char buffer[128];
    switch (this->type_) {
      case type_int:
      case type_time:
        this->val_.d = (integer) value;
        break;
      case type_float:
        this->val_.f = value;
        break;
      case type_text:
      case type_bytea:
        sprintf(buffer,"%f",value);
        this->val_.s.l = strlen(buffer);
        this->val_.s.v = Value::strndup(buffer, this->val_.s.l);
        if (! val_.s.v)
          ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
        break;
      case type_bool:
        this->val_.b = (value == 1.0);
        break;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
    }
    this->isNull_ = false;
  }
  void Value::bol(bool value) {
    setNull();
    
    switch (this->type_) {
      case type_int:
        this->val_.d = value ? 1 : 0;
        break;
      case type_float:
        this->val_.f = value ? 1.0 : 0.0;
        break;
      case type_text:
      case type_bytea:
        if (value) {
          this->val_.s.l = 4;
          this->val_.s.v = Value::strndup("true", this->val_.s.l);
        } else {
          this->val_.s.l = 5;
          this->val_.s.v = Value::strndup("false", this->val_.s.l);
        }
        if (! this->val_.s.v)
          ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
        break;
      case type_bool:
        this->val_.b = value;
        break;
      default:
        ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
    }
    this->isNull_ = false;
  }
  void Value::date(jtime value) {
    num(value.integer());
  }
  void Value::str(const string& value) {
    bytea( value.c_str(), value.size() );
  }
  void Value::bytea(const char* value, size_t size) {
    setNull();
    if (value) {
      // map input value if mapping available.
      if ( this->mapping_ ) {
        int i;
        for (i=0; this->mapping_[i]; i+=2) {
          if ( strncmp(this->mapping_[i], value, size) == 0 ) {
            value = this->mapping_[i+1];
          }
        }
      }
      
      char *pEnd;
      switch (this->type_) {
        case type_int:
          this->val_.d = strtol(value,&pEnd,10);
          if (*pEnd != '\0')
            ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid integer value: %s", value);
          break;
        case type_float:
          this->val_.f = strtod(value,&pEnd);
          if (*pEnd != '\0')
            ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid decimal value: %s", value);
          break;
        case type_time:
        {
          // Note the internal representation is unix time since epoch
          jtime t;
          
          // 1. try to convert as millis since epoch
          long l = strtol(value,&pEnd,10);
          if (*pEnd == '\0') {
            // conversion from millis since epoch to julian day
            t = jtime((time_t) l);
          } else {
            // 2. interpret as julian day in db
            double d = strtod(value,&pEnd);
            if (*pEnd == '\0') {
              t = jtime( d );
            } else {
              // 3. interpret as ISO date string
              t = jtime( value );
            }
          }
          
          // conversion from millis since epoch to julian day
          this->val_.d = t.integer();
          break;
        }
        case type_text:
        case type_bytea:
          this->val_.s.v = (char*) malloc (sizeof(char) * size);
          if (! this->val_.s.v) {
            ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
          } else {
            memcpy (this->val_.s.v, value, sizeof(char) * size);
            this->val_.s.l = size;
          }
          break;
        case type_bool:
          if (strncmp("1", value, size) == 0) {
            this->val_.b = true;
          } else if (strncasecmp("Y", value, size) == 0) {
            this->val_.b = true;
          } else if (strncasecmp("YES", value, size) == 0) {
            this->val_.b = true;
          } else if (strncasecmp("T", value, size) == 0) {
            this->val_.b = true;
          } else if (strncasecmp("true", value, size) == 0) {
            this->val_.b = true;
          } else if (strncasecmp("ON", value, size) == 0) {
            this->val_.b = true;
          } else {
            this->val_.b = false;
          }
          break;
        default:
          ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
      }
      this->isNull_ = false;
    }
  }
  void Value::append( const char* value, size_t size ) {
    vector<char> v;
    if (value && *value) {
      if ( isNull() ) {
        bytea(value, size);
      } else {
        switch (this->type_) {
          case type_bytea:
          case type_text:
            this->val_.s.v = (char *)realloc( this->val_.s.v, this->val_.s.l + size );
            if (! this->val_.s.v)
              ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
            memcpy( this->val_.s.v + this->val_.s.l, value, sizeof(char) * size );
            this->val_.s.l += size;
            break;
          default:
            ejin::_throw_exception(ejin::CONVERSION_EXCEPTION, "Invalid database type: %d", this->type_);
        }
      }
    }
  }
  
  // ---
  // Conversion
  // 
  
  char* Value::strdup(const string& string)
  {
    char* s = strndup(string.c_str(), string.size()+1);
    s[string.size()] = '\0'; // string end character
    return s;
  }
  char* Value::strdup(const char* string)
  {
    char* v = strndup(string, strlen(string)+1);
    v[strlen(string)] = '\0';
    return v;
  }
  char* Value::strndup(const char* string, size_t size)
  {
    char* s = (char*) malloc (sizeof(char) * size);
    if (!s)
      ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
    else
      memcpy (s, string, sizeof(char) * size);
    return s;
  }
  
  wchar_t* Value::wcsdup(const wchar_t* string)
  {
    size_t size = wcslen(string) + 1;
    wchar_t* s = (wchar_t*) malloc (sizeof(wchar_t) * size);
    if (!s)
      ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
    else
      memcpy (s, string, sizeof(wchar_t) * size);
    return s;
  }
  
#ifndef __clang_analyzer__

  wchar_t* Value::mbstowcs_alloc(const char *string)
  {
    size_t size = strlen (string) + 1;
    wchar_t *buf = (wchar_t*) malloc (size * sizeof (wchar_t));
    if (! buf)
      ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");            
    
    size = mbstowcs (buf, string, size);
    if (size == (size_t) -1) {
      free(buf);
      return NULL;
    }
    buf = (wchar_t*) realloc (buf, (size + 1) * sizeof (wchar_t));
    if (! buf)
      ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");            
    return buf;
  }
  
  char* Value::wcstombs_alloc(const wchar_t* string) {
    size_t size = (wcslen (string) * MB_LEN_MAX) + 1;
    char *buf = (char*) malloc (size * sizeof (char));
    if (! buf)
      ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");            
    
    size = wcstombs (buf, string, size);
    if (size == (size_t) -1) {
      free(buf);
      return NULL;
    }
    buf = (char*) realloc (buf, (size + 1) * sizeof (char));
    if (! buf)
      ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");            
    return buf;
  }

#endif

  //sql eof
}
