/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_UTIL_H__
#define __SQLITE_UTIL_H__

#include "SqliteCommon.h"

namespace sqlite
{

  /**
   * Simple SQL time class.
   */
  class jtime
  {
    friend bool operator==(const jtime& lhs, const jtime& rhs);
    friend bool operator< (const jtime& lhs, const jtime& rhs);
    // ==========
    // Static
    // ==========
  public:

    static jtime now(void) { return jtime(); }
      
    // ==========
    // Ctors
    // ==========
  public:
    
    jtime(void);
    jtime(const double julianDay);
    jtime(const time_t value);
    jtime(const integer value);
    jtime(const jtime& value);
    jtime(const struct tm& value);
    jtime(const char* value);
    jtime(const char* value, const char* pattern);
    ~jtime();
    jtime& operator=(const jtime& value);
    //bool operator==(const jtime& value) const;

    // ==========
    // Public Interface
    // ==========
  public:

    double julianDay(void) const;
    integer integer(void) const;
    time_t time(void) const;
    const struct tm* tm(void) const;
    
    jtime truncToDay() const;
    bool isToday() const;
    void addMinutes(int count);
    void addHours(int count);
    void addDays(int count);

    char* asISO8601( char* buffer, size_t maxLength ) const;
    string asISO8601(void) const;
    char* format(char* buffer, const char* fm, size_t maxLength) const;

    // ==========
    // Private Interface
    // ==========
  private:

    struct tm value_;
    int milliSeconds_;
    
    bool equals( const jtime& rhs ) const;
    bool lessThan( const jtime& rhs ) const;

    bool parseISO8601(const char* value);
    bool parse(const char* value, const char* pattern);
    void init(int* year, int* month, int* mday, int* hours, int* minutes, int* seconds, long* tzOffset);

  };
  // comparision
  inline bool operator==( const jtime& lhs, const jtime& rhs ){return lhs.equals(rhs);}
  inline bool operator!=( const jtime& lhs, const jtime& rhs ){return !operator==(lhs,rhs);}
  inline bool operator< ( const jtime& lhs, const jtime& rhs ){return lhs.lessThan(rhs);}
  inline bool operator> ( const jtime& lhs, const jtime& rhs ){return  operator< (rhs,lhs);}
  inline bool operator<=( const jtime& lhs, const jtime& rhs ){return !operator> (lhs,rhs);}
  inline bool operator>=( const jtime& lhs, const jtime& rhs ){return !operator< (lhs,rhs);}

  // debugging
  std::ostream& operator<<( std::ostream& ostr, const sqlite::jtime& rhs );
}
#endif //__SQLITE_UTIL_H__
