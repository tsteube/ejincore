/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteResultSet.h"

#include <sstream>

#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include "SqliteValue.h"
#include "SqliteBaseEntity.h"

template< typename T >
struct delete_ptr : public std::unary_function<bool,T>
{
  bool operator()(T*pT) const { delete pT; return true; }
};

namespace sqlite {

  using std::ostringstream;
  
  // ==========
  // Ctor
  // ==========
  
  ResultSet::ResultSet( void ): resultSet_(), taken_(0) { }
  ResultSet::~ResultSet() 
  {
    // erase all remaining records
    for_each( this->resultSet_.begin(), this->resultSet_.end(), delete_ptr<BaseEntity>() );  
    this->resultSet_.clear();
  } 
  
  BaseEntity* ResultSet::takeFirst( )
  {
    BaseEntity* result = NULL;
    if (this->resultSet_.begin() != this->resultSet_.end()) {
      result = *this->resultSet_.begin();
      this->taken_++;
      this->resultSet_.erase(this->resultSet_.begin());
    }
    return result;
  }
  vector< BaseEntity* >::iterator ResultSet::take( vector< BaseEntity* >::iterator position )
  {
    this->taken_++;
    return this->resultSet_.erase(position);
  }
  
  // ----------
  // debugging
  
  ostream& operator<<( ostream& ostr, const ResultSet& rhs ) {
    ostringstream buf;
    buf.flags(ostr.flags());
    buf.fill(ostr.fill());
    buf << "ResultSet[size=" << rhs.size() << ",taken=" << rhs.taken_ << "]";    
    ostr << buf.str();
    return ostr;
  }

}