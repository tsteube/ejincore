/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SqliteBaseEntity.h"

#include <sstream>
#include <assert.h>

#include "SqliteValue.h"
#include "SqliteUtil.h"
#include "XMLAttribute.h"
#include "XMLAttributeSet.h"

namespace sqlite
{
  
  // ==========
  // XML Mapping
  // ==========
  
  const string BaseEntity::XML_NAME = "";
  const ser::XmlAttribute BaseEntity::XML_FIELDS[] = { ser::XmlAttribute() };
  const ser::XmlAttributeSet BaseEntity::XML_DDL = ser::XmlAttributeSet(const_cast<ser::XmlAttribute*>(XML_FIELDS));
  
  // ==========
  // CTORs
  // ==========

  BaseEntity::BaseEntity(const AttributeSet& field, bool readOnly): 
  fields_(field), readOnly_(readOnly), values_()
  {
    const Attribute* attr;
    for (int index = 0; index < fields_.count(); index++)
    {
      attr = fields_.byIndex(index);
      values_.push_back(Value(attr->type(), attr->mapping()));
    }
  }
  BaseEntity::BaseEntity(const BaseEntity& record): 
  fields_(record.fields_), readOnly_(record.readOnly_), values_()
  {
    values_.resize(fields_.count());
    values_ = record.values_;
  }
  BaseEntity& BaseEntity::operator=(const BaseEntity& rhs)
  {
    assert(this->fields_ == rhs.fields());
    if (this != &rhs) {
      this->values_ = rhs.values_;
      this->readOnly_ = rhs.readOnly_;
    }
    return *this;
  }
  BaseEntity::~BaseEntity(void) 
  {
    this->values_.clear();
  }

  // ==========
  // Public Methods
  // ==========

  integer BaseEntity::getId() {
    Value* v = value("ID");
    return v ? v->num() : 0;
  }
  void BaseEntity::setId( integer id ) {
    Value* v = value("ID");
    if ( v )
      v->num( id );
  }
  ejin::SyncInd BaseEntity::getSyncInd() {
    Value* v = value("SYNC_IND");
    return v ? (ejin::SyncInd) v->num() : ejin::kSyncIndSynchronous;
  }
  void BaseEntity::setSyncInd( ejin::SyncInd ind ) {
    Value* v = value("SYNC_IND");
    if ( v )
      v->num((integer)ind);
  }

  string BaseEntity::toString() const
  {
    std::stringstream buf;
    buf << tableName() << "[";
    for (size_t column = 0; column < columnCount(); column++) {
      if (Value* val = value(column))
      {
        buf << val->toString();
        if (column < (columnCount() - 1))
          buf << "|";
      }    
    }
    buf << "]";

    return buf.str();
  }
  
  // ==========
  // Protected Methods
  // ==========
  
  vector<Value*> BaseEntity::keyValue() const
  {
    vector<Value*> keys;
    for (int index = 0; index < fields_.count(); index++)
    {
      if (const Attribute* field = fields_.byIndex(index))
      {
        if (field->isKeyIdField())
          keys.push_back(value(field->index()));
      }
    }    
    return keys;
  }
  Value* BaseEntity::value(string fieldName) const
  {
    if (const Attribute* field = fields_.byName(fieldName))
      return value(field->index());    
    return NULL;
  }
  Value* BaseEntity::value(size_t column_index) const
  {
    Value* v = NULL;
    if (column_index < values_.size()) {
      v = const_cast<Value*>(&values_[column_index]);
    }
    return v;
  }  
  const Attribute* BaseEntity::byName(string fieldName) const
  {
    return fields().byName(fieldName);
  }
  const Attribute* BaseEntity::byIndex(int index) const
  {
    return fields().byIndex(index);
  }
  
  // ----------
  // get methods
  
  const bool BaseEntity::isNull(int index) const 
  { 
    return value(index)->isNull();
  }
  const string BaseEntity::asString(int index) const
  { 
    Value* val = value(index);
    if (val->isNull()) {
      return "";
    } else {
      return val->toString();
    }
  }
  const char* BaseEntity::asBytea(int index) const
  {
    return value(index)->bytea();
  }
  const size_t BaseEntity::size(int index) const
  {
    return value(index)->size();
  }
  const integer BaseEntity::asInteger(int index) const
  { 
    Value* val = value(index);
    if (val->isNull()) {
      return 0;
    } else {
      return val->num();
    }
  }
  const double BaseEntity::asDouble(int index) const 
  { 
    Value* val = value(index);
    if (val->isNull()) {
      return 0;
    } else {
      return val->dbl();
    }
  }
  const bool BaseEntity::asBool(int index) const 
  { 
    Value* val = value(index);
    if (val->isNull()) {
      return 0;
    } else {
      return val->bol();
    }
  }
  const jtime BaseEntity::asTime(int index) const 
  { 
    Value* val = value(index);
    if (val->isNull()) {
      return (integer)0;
    } else {
      return val->date();
    }
  }
  
  // ----------
  // set methods

  void BaseEntity::setNull(int index) 
  {
    value(index)->setNull();
  }
  void BaseEntity::asString(int index, const string& val) 
  {
    value(index)->str(val);
  }
  void BaseEntity::asString(int index, const char* val, size_t length)
  {
    value(index)->bytea(val, length);
  }
  void BaseEntity::asBytea(int index, const char* val, size_t length)
  {
    value(index)->bytea(val, length);
  }
  void BaseEntity::asBool(int index, const bool val)
  {
    value(index)->bol(val);
  }
  void BaseEntity::asInteger(int index, const integer val)
  {
    value(index)->num(val);
  }
  void BaseEntity::asDouble(int index, const double val) 
  {
    value(index)->dbl(val);
  }
  void BaseEntity::asTime(int index, const jtime& val) 
  {
    value(index)->date(val);
  }

}
