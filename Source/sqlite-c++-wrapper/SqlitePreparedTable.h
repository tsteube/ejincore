/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQL_PREPARED_TABLE_H__
#define __SQL_PREPARED_TABLE_H__

#include "SqliteBaseEntity.h"
#include "SqliteDatabase.h"

namespace sqlite
{
  
  // ==============
  // Table: Base table implementation to support entity manager
  // ==============
  
  class PreparedTable {
    
    // ==========
    // Constructor
    // ==========
  public:
    
    // ctor
    PreparedTable( const Database& db, BaseEntity* prototype );
    // copy ctor
    PreparedTable( const PreparedTable& table );
    // dtor
    virtual ~PreparedTable( void );
    
    // ==========
    // Public methods
    // ==========
  public:
    
    string toString( void ) const;
    const char* name( void ) const
    { return this->prototype_->tableName(); }
    const char* mainSchemaName( void ) const
    { return this->prototype_->mainSchemaName(); }
    const char* backupSchemaName( void ) const
    { return this->prototype_->backupSchemaName(); }
    const char* archiveSchemaName( void ) const
    { return this->prototype_->archiveSchemaName(); }
    const AttributeSet& fields( void ) const
    { return this->prototype_->tableFields(); }
    sqlite3* handle( void ) const
    { return this->db_.handle(); }
    const BaseEntity& prototype( void ) const
    { return *this->prototype_; }
    
    // ==========
    // Protected Variables
    // ==========
  protected:
    
    const Database& db_;
    /** The prototype is used to create new hollow entity */
    const unique_ptr<BaseEntity> prototype_;
    
    // ------------
    // prepared statments
    map<string,sqlite3_stmt*>             preparedSelectStatement_;
    map<string,sqlite3_stmt*>             preparedInsertStatement_;
    map<string,sqlite3_stmt*>             preparedUpdateStatement_;
    map<string,sqlite3_stmt*>             preparedDeleteStatement_;
    map<string,sqlite3_stmt*>             preparedDeleteAllStatement_;
    map<string,sqlite3_stmt*>             preparedCountStatement_;
    map<string,sqlite3_stmt*>             preparedSequenceStatement_;
    sqlite3_stmt*                         preparedRestoreStatement1_;
    sqlite3_stmt*                         preparedRestoreStatement2_;
    map<string,map<string,sqlite3_stmt*>> preparedFinderStatement_;
    map<string,map<string,sqlite3_stmt*>> preparedSqlAliasStatement_;
    
    // ==========
    // Protected Methods
    // ==========
  protected:
    mutable int numberOfPreparedStatements;

    // disable assignment
    PreparedTable& operator=( const PreparedTable& rhs );
    
    void prepareAllStatementsOn( const char* schema );
    void clearAllStatementsOn( const char* schema );
    
    // -----------
    // Private Methods: 1. level - Mapping
    
    integer nextSequence( const char* schema ) const;
    sqlite3_stmt* bindEntityValues(sqlite3_stmt* stmt,
                                   const char* schema,
                                   BaseEntity& entity,
                                   vector<Value*> k,
                                   bool keys,
                                   bool props);
    BaseEntity& readEntityValues(sqlite3_stmt* stmt,
                                 BaseEntity& entity);
    int readValuesFromStmt(sqlite3_stmt* stmt,
                           int offset,
                           vector< pair<field_type,Value*> >values );
    int bindValuesToStmt(sqlite3_stmt* stmt,
                         int offset,
                         vector< pair<field_type,const Value*> > values);
    
    // -----------
    // Private Methods: 2. level - SQLITE statments
    
    sqlite3_stmt* prepareSequenceStatement( const string& schema ) const;
    sqlite3_stmt* prepareInsertStatement( const string& schema ) const;
    sqlite3_stmt* prepareUpdateStatement( const string& schema ) const;
    sqlite3_stmt* prepareSelectStatement( const string& schema, const string& condition ) const;
    sqlite3_stmt* prepareDeleteStatement( const string& schema ) const;
    sqlite3_stmt* prepareDeleteAllStatement( const string& schema ) const;
    sqlite3_stmt* prepareCountStatement( const string& schema ) const;
    sqlite3_stmt* preparedCutBackupStatement( ) const;
    sqlite3_stmt* prepareBackupStatement1( const string& condition ) const;
    sqlite3_stmt* prepareBackupStatement2( const string& condition1, const string& condition2 ) const;
    sqlite3_stmt* prepareRestoreStatement1( const string& condition ) const;
    sqlite3_stmt* prepareRestoreStatement2( const string& condition1, const string& condition2 ) const;
    sqlite3_stmt* prepareStatement( const string& sql ) const;
    
  private:
    void clearStmt( sqlite3_stmt* stmt );
    void eraseStatementOn( map<string,sqlite3_stmt*>& statements, const char* schema );
    void eraseStatementOn( map<string,map<string,sqlite3_stmt*>>& statements, const char* schema );
  };
  
  // debugging
  ostream& operator<<( ostream& ostr, const PreparedTable& rhs );
  
}

#endif // __SQL_PREPARED_TABLE_H__
