/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SQLITE_COMMON_H__
#define __SQLITE_COMMON_H__

#include "Environment.h"

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <stdexcept>

using std::unique_ptr;
using std::string;
using std::map;
using std::pair;
using std::set;
using std::list;
using std::string;
using std::vector;
using std::ostream;

#include <ThirdParty/sqlite3.h>

#define EXEC_SQLITE_STATEMENT(result, db) { \
switch (result) { \
case SQLITE_OK: \
break; \
case SQLITE_NOMEM: \
ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Sqlite error code: %s", sqlite3_errmsg(db)); \
default: \
ejin::_throw_exception(ejin::DATA_ACCESS_EXCEPTION, "Sqlite error code: %s", sqlite3_errmsg(db)); \
} \
}

namespace sqlite
{
  // forward class declarations
  class jtime;
  class Value;
  class Attribute;
  class AttributeSet;
  class BaseEntity;
  class ResultSet;
  class PreparedTable;
  class Table;
  class Database;

  // ==========
  // Enums
  // ==========
  
  typedef enum SQLITE3AutoVacuum
  {
    kSQLITE3AutoVacuumNoAutoVacuum = 0,
    kSQLITE3AutoVacuumFullVacuum,
    kSQLITE3AutoVacuumIncrementalVacuum
    
  } SQLITE3AutoVacuum;
  typedef enum SQLITE3LockingMode
  {
    kSQLITE3LockingModeNormal = 0,
    kSQLITE3LockingModeExclusive
  } SQLITE3LockingMode;
  
  enum field_use
  {
    FIELD_DEFAULT,
    FIELD_KEY
  };
  
  enum field_type
  {
    type_undefined,
    type_int,
    type_text,
    type_bytea,
    type_float,
    type_bool,
    type_time
  };
  
  enum field_flags
  {
    flag_none = 0,
    flag_not_null = 1,
    flag_primary_key = 2,
    flag_transient = 4,
  };
}

#endif // __SQLITE_COMMON_H__
