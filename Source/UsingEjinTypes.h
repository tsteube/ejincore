/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __USING_EJIN_CORE_H__
#define __USING_EJIN_CORE_H__

#include "Declarations.h"

using ejin::Modifications;
using ejin::Utilities;
using ejin::ResultList;
using ejin::KeySequence;
using ejin::SyncSource;
using ejin::AESCrypto;
using ejin::RSACrypto;
using ejin::XmlMarshaller;
using ejin::JsonMarshaller;
using ejin::StringTemplate;
using ejin::StringTemplate2;

using ejin::Chat;
using ejin::Message;

using ejin::Channel;
using ejin::ChannelMedia;
using ejin::ChannelMember;
using ejin::ChannelProfile;
using ejin::ChannelRepository;
using ejin::ChannelService;

using ejin::ChannelHeader;
using ejin::ChannelHeaderProfile;
using ejin::ChannelHeaderRepository;

using ejin::ChannelComment;
using ejin::ChannelCommentProfile;
using ejin::ChannelCommentRepository;

using ejin::ChannelPost;
using ejin::ChannelPostProfile;
using ejin::ChannelPostRepository;

using ejin::Resource;
using ejin::Media;
using ejin::Attachment;
using ejin::MediaProfile;
using ejin::MediaRepository;
using ejin::ChannelMediaRepository;
using ejin::MediaService;

using ejin::Member;
using ejin::ApprovedMember;
using ejin::ChannelMember;
using ejin::MemberProfile;
using ejin::MemberRepository;
using ejin::ChannelMemberRepository;
using ejin::ApprovedMemberRepository;

using ejin::Picture;
using ejin::PictureProfile;
using ejin::PictureRepository;

using ejin::User;
using ejin::UserProfile;
using ejin::UserRepository;
using ejin::UserService;

using ejin::ChangeLog;
using ejin::ChangeLogProfile;
using ejin::ChangeLogRepository;

using ejin::kChangeLogSourceChannel;
using ejin::kChangeLogSourceChannelPost;
using ejin::kChangeLogSourceChannelComment;
using ejin::kChangeLogSourceMedia;
using ejin::kChangeLogSourceChannelMember;
using ejin::kChangeLogSourceMemberPicture;
using ejin::kChangeLogSourceMember;
using ejin::kChangeLogSourceMemberApproval;
using ejin::kChangeLogSourceChat;
using ejin::kChangeLogSourceChatMessage;

using ejin::kChangeLogOperationAdd;
using ejin::kChangeLogOperationUpdate;
using ejin::kChangeLogOperationOutdated;
using ejin::kChangeLogOperationRemove;

using ejin::SyncInd;
using ejin::kSyncIndSynchronous;
using ejin::kSyncIndInsert;
using ejin::kSyncIndUpdate;
using ejin::kSyncIndDelete;
using ejin::kSyncIndConflict;

using ejin::MembershipInd;
using ejin::kMembershipIndInSync;
using ejin::kAcceptMemberIndShip;
using ejin::kRejetMemberIndShip;

using ejin::SyncOperation;
using ejin::kSyncNone;
using ejin::kSyncInsert;
using ejin::kSyncUpdate;
using ejin::kSyncRemove;

using ejin::Membership;
using ejin::kMembershipInvited;
using ejin::kMembershipAccepted;
using ejin::kMembershipRejected;

using ejin::MemberRole;
using ejin::kSyncRoleRead;
using ejin::kSyncRoleWrite;
using ejin::kSyncRoleAdmin;
using ejin::kSyncRoleOwner;

using ejin::kNoneModification;
using ejin::kChannelHeaderPendingLocalModification;
using ejin::kChannelHeaderRecentRemoteModification;
using ejin::kChannelHeaderConflictedModification;
using ejin::kChannelMemberPendingLocalModification;
using ejin::kChannelMemberRecentRemoteModification;
using ejin::kChannelMemberConflictedModification;
using ejin::kChannelPostPendingLocalModification;
using ejin::kChannelPostRecentRemoteModification;
using ejin::kChannelPostConflictedModification;
using ejin::kChannelCommentPendingLocalModification;
using ejin::kChannelCommentRecentRemoteModification;
using ejin::kChannelCommentConflictedModification;
using ejin::kMediaPendingLocalModification;
using ejin::kMediaRecentRemoteModification;
using ejin::kMediaConflictedModification;
using ejin::kMemberPendingLocalModification;
using ejin::kMemberRecentRemoteModification;
using ejin::kMemberConflictedModification;

using ejin::memory_error;
using ejin::mapping_error;
using ejin::data_access_error;
using ejin::security_integrity_error;
using ejin::conversion_error;
using ejin::data_retrieval_error;
using ejin::conflict_error;
using ejin::data_integrity_error;


using ser::ISerializableEntity;
using ser::SerializableEntity;
//using ser::XmlStream;

using sqlite::SQLITE3LockingMode;
using sqlite::SQLITE3AutoVacuum;

using sqlite::FIELD_DEFAULT;
using sqlite::FIELD_KEY;

using sqlite::type_undefined;
using sqlite::type_int;
using sqlite::type_text;
using sqlite::type_bytea;
using sqlite::type_float;
using sqlite::type_bool;
using sqlite::type_time;

using sqlite::flag_none;
using sqlite::flag_not_null;
using sqlite::flag_primary_key;
using sqlite::flag_transient;

using sqlite::Table;
using sqlite::PreparedTable;
using sqlite::Attribute;
using sqlite::AttributeSet;
using sqlite::ResultSet;
using sqlite::BaseEntity;
using sqlite::Value;
using sqlite::jtime;

#endif // __USING_EJIN_CORE_H__
