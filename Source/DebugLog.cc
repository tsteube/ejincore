//
// License
// This article, along with any associated source code and files, is licensed under 
// The Code Project Open License (CPOL (http://www.codeproject.com/info/cpol10.aspx)
//
// Article http://www.codeproject.com/KB/cpp/SimpleDebugLog.aspx
// Author: Fredrik Bornander
//
#include "DebugLog.h"

#ifdef DEBUG

#include <exception>

#include <stdarg.h>

#define DEBUG_MAX_LINE_LEN 512
void log_printf(const char* msg, ...)
{
  if (msg)
  {
    char buf[DEBUG_MAX_LINE_LEN + 1];
    va_list argp;
    va_start(argp, msg);
    vsnprintf(buf, DEBUG_MAX_LINE_LEN, msg, argp);
    va_end(argp);
    }
}

namespace bornander
{
	namespace debug
	{
		int log::indentation = 0;
		std::ostream* log::stream = &std::cout;

		log::log(const std::string& ctx)
			: context(ctx)
#ifdef DEBUG_TIMING
			, start_time(clock())
#endif
		{
			write_indentation();
			*stream << "--> " << context << std::endl;
			++indentation;
			stream->flush();
		}

		log::~log()
		{
			--indentation;
			write_indentation(std::uncaught_exception() ? '*' : ' ');
			*stream << "<-- " << context;
#ifdef DEBUG_TIMING
			*stream << " in " << ((double)(clock() - start_time) / CLOCKS_PER_SEC) << "s";
#endif
			*stream << std::endl;
			stream->flush();
		}

		void log::set_stream(std::ostream& str)
		{
			log::stream = &str;
		}


		void log::write_indentation()
		{
			write_indentation(' ');
		}

		void log::write_indentation(const char prefix)
		{
			*stream << prefix;
			for(int i = 0; i < indentation * 2; ++i)
			{
				*stream << " ";
			}
		}

		void log::message(const std::string& msg)
		{
			write_indentation();
			*stream << msg << std::endl;
			stream->flush();
		}

    void log::printf(const char* msg, ...)
    {
      if (msg)
      {
        char buf[DEBUG_MAX_LINE_LEN + 1];
        va_list argp;
        va_start(argp, msg);
        vsnprintf(buf, DEBUG_MAX_LINE_LEN, msg, argp);
        va_end(argp);
        this->message(buf);
      }
    }
    
	}
}
#endif