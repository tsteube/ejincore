/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CRYPTO_ENTITY_H
#define __EJIN_CRYPTO_ENTITY_H

#include "Exceptions.h"

namespace ejin
{
  // forward declaration
  class RSACrypto;

  /**
   *  Helper interface to compare entities on business rules and copy business data.
   *  If isDataEqualTo returns false (true) copyDataFrom must also return false (true).
   */
  class ICryptoEntity {
    
    // ==========
    // Public Interface
    // ==========
  public:
    virtual ~ICryptoEntity( void ) { }
    
    virtual const integer getChannelId( void ) const = 0;
    virtual bool hasChannelId( void ) const = 0;

    virtual const string getChannelGid( void ) const = 0;
    virtual bool hasChannelGid( void ) const = 0;

    virtual bool decrypt( const RSACrypto* crypto ) throw(security_integrity_error) { return false; }
    virtual bool encrypt( const RSACrypto* crypto ) throw(security_integrity_error) { return false; }
    
  };
  
}

#endif
