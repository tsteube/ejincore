/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "RSACrypto.h"
#include "UsingEjinTypes.h"

#include <assert.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/ec.h>
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <openssl/ripemd.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/bn.h>
#include <base58.h>

// TODO replace RSA api calls: https://gist.github.com/grejdi/9361828
// http://dmiyakawa.blogspot.com/2013/03/pemwritebiorsapubkey-vs.html

// Callback to disable console passphrase input
static int disable_passphrase_prompt(
                              char *buf,
                              int size,
                              int rwflag,
                              void *u) 
{ 
  return 0; 
}

namespace ejin
{
  using std::runtime_error;
  using std::stringstream;
  using std::runtime_error;
  using std::function;
  using std::underflow_error;
  using std::overflow_error;
  
//RSA *EVP_PKEY_get0_RSA(const EVP_PKEY *pkey);
  
//#define HAS_PUBLIC_KEY( rsaKey )   ( rsaKey && rsaKey->e /* public exponent */ )
//#define HAS_PRIVATE_KEY( rsaKey )  ( rsaKey && rsaKey->d /* private exponent */ )
#define HAS_PUBLIC_KEY   ( _pkey != NULL )
#define HAS_PRIVATE_KEY  ( _pkey && _hasPrivateKey )
// EC_KEY_get0_private_key
  
  __unused static uchar_array charset()
  {
    uchar_array all(256);
    for (int i=0; i<256; i++)
      all[i] = (uchar) i;
    return all;
    /*
     return char_array(
     {'0','1','2','3','4',
     '5','6','7','8','9',
     'A','B','C','D','E','F',
     'G','H','I','J','K',
     'L','M','N','O','P',
     'Q','R','S','T','U',
     'V','W','X','Y','Z',
     'a','b','c','d','e','f',
     'g','h','i','j','k',
     'l','m','n','o','p',
     'q','r','s','t','u',
     'v','w','x','y','z'
     });
     */
  };
  
  RSACrypto::RSACrypto(  ) :
  AESCrypto(), _pkey(NULL), _rsaCtx(), _hasPrivateKey(false)
  {
    _rsaCtx = EVP_CIPHER_CTX_new();
    assertf(_rsaCtx, "no evelope")
    EVP_CIPHER_CTX_reset(_rsaCtx);
  }
  RSACrypto::RSACrypto( EVP_PKEY* pkey, bool hasPrivateKey ) :
  AESCrypto(), _pkey(pkey), _rsaCtx(), _hasPrivateKey(hasPrivateKey)
  {
    _rsaCtx = EVP_CIPHER_CTX_new();
    assertf(_rsaCtx, "no evelope")
    EVP_CIPHER_CTX_reset(_rsaCtx);
  }
  RSACrypto::RSACrypto( RSA* rsa, bool hasPrivateKey ):
  AESCrypto(), _pkey(), _rsaCtx(), _hasPrivateKey(hasPrivateKey)
  {
    _rsaCtx = EVP_CIPHER_CTX_new();
    assertf(_rsaCtx, "no evelope")
    EVP_CIPHER_CTX_reset(_rsaCtx);
    _pkey = EVP_PKEY_new();
    assertf(_pkey, "no evelope")
    EVP_PKEY_assign_RSA(_pkey, rsa); // take ownership
  }
  RSACrypto::RSACrypto( const RSACrypto& crypto ):
  AESCrypto(), _pkey(), _rsaCtx(), _hasPrivateKey(false)
  {
    this->operator=( crypto );
  }
  RSACrypto& RSACrypto::operator=( const RSACrypto& crypto )
  {
    if (this != &crypto) {
      AESCrypto::operator=( crypto );
      if ( _pkey ) {
        EVP_PKEY_free(_pkey);
        _pkey = NULL;
      }
      if ( crypto._pkey != NULL && EVP_PKEY_base_id(crypto._pkey) == EVP_PKEY_RSA ) {
        RSA *rsaKey = EVP_PKEY_get0_RSA(crypto._pkey);
        if (rsaKey) {
          if ( crypto._hasPrivateKey ) {
            rsaKey = RSAPrivateKey_dup(rsaKey);
          } else {
            rsaKey = RSAPublicKey_dup(rsaKey);
          }
          assertf(rsaKey, "no evelope")
          _hasPrivateKey = crypto._hasPrivateKey;
          _pkey = EVP_PKEY_new();
          assertf(_pkey, "no evelope")
          EVP_PKEY_assign_RSA(_pkey, rsaKey); // take ownership
        }
      }
    }
    return *this;
  }
  bool RSACrypto::operator==( const RSACrypto& crypto ) const {
    if (this == &crypto)
      return true;
    // check only for equal public key
    return EVP_PKEY_cmp_parameters(_pkey, crypto._pkey) && EVP_PKEY_cmp(_pkey, crypto._pkey);
  }

  RSACrypto::~RSACrypto( void )
  {
    EVP_CIPHER_CTX_free(_rsaCtx);
    if ( _pkey ) {
      EVP_PKEY_free(_pkey);
    }
  }
  RSACrypto* RSACrypto::generateRSAKeyPair( const char* folder, const char* password )
  {
    // https://wiki.openssl.org/index.php/EVP_Key_and_Parameter_Generation
    EVP_PKEY* pkey = NULL;
    EVP_PKEY_CTX* kctx = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, NULL);
    assertf( kctx, "no evelope" )
    assertOpenSSL( EVP_PKEY_keygen_init(kctx) );
    assertOpenSSL( EVP_PKEY_CTX_set_rsa_keygen_bits(kctx, RSA_KEY_LENGTH) );
    assertOpenSSL( EVP_PKEY_keygen(kctx, &pkey) );
    RSACrypto* crypto = new RSACrypto( pkey, true );
    assertf( crypto, "invalid context" );
    EVP_PKEY_CTX_free(kctx);

    if (folder) {
      crypto->saveKeys( folder, password );
    }
    return crypto;
  }
  bool RSACrypto::clearRSAKeyPair( const char* folder )
  {
    return erasePEM( folder );
  }
  RSACrypto* RSACrypto::fromPublicKeyASN1( const string& publicKey )
  {
    RSA* rsa = NULL;
    return loadKeyASN1(&rsa, publicKey, false) ? new RSACrypto(rsa, false) : NULL;
  }
  RSACrypto* RSACrypto::fromPrivateKeyASN1( const string& privateKey )
  {
    RSA* rsa = NULL;
    return loadKeyASN1(&rsa, privateKey, true) ? new RSACrypto(rsa, true) : NULL;
  }
  RSACrypto* RSACrypto::loadRSAKeyPair( const char* folder, const char* password )
  {
    EVP_PKEY* pkey = NULL;
    if ( importPEM_from_folder(&pkey, true, folder, password) ) {
      return new RSACrypto(pkey, true);
    }
    if ( importPEM_from_folder(&pkey, false, folder, password) ) {
      return new RSACrypto(pkey, false);
    }
    return NULL;
  }
  RSACrypto* RSACrypto::fromPublicKeyPEM( const string& publicKeyPEM, const char* password )
  {
    EVP_PKEY* pkey = NULL;
    if ( importKey(&pkey, publicKeyPEM, false, password) ) {
      return new RSACrypto(pkey, false);
    }
    return NULL;
  }
  RSACrypto* RSACrypto::fromPrivateKeyPEM( const string& privateKeyPEM, const char* password )
  {
    EVP_PKEY* pkey = NULL;
    if ( importKey(&pkey, privateKeyPEM, true, password) ) {
      return new RSACrypto(pkey, true);
    }
    return NULL;
  }
  
  bool RSACrypto::saveKeys( const char* folder, const char* password ) const
  {
    assertf(folder, "no folder")
    if (_pkey) {
      if ( _hasPrivateKey ) {
        exportPEM_to_folder( _pkey, true, folder, password );
      } else {
        exportPEM_to_folder( _pkey, false, folder, password );
      }
      return true;
    }
    return false;
  }
  
  bool RSACrypto::hasPublicKeyASN1( void ) const
  {
    return HAS_PUBLIC_KEY;
  }
  string RSACrypto::getPublicKeyASN1( void ) const
  {
    assertf( HAS_PUBLIC_KEY, "no public key" )
    Cipher result;
    
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    unsigned char public_key[RSA_KEY_LENGTH] = { 0 };
    BIO* bio = BIO_new( BIO_s_mem() );
    i2d_RSAPublicKey_bio( bio, rsaKey );
    int public_key_len = BIO_read( bio, public_key, sizeof(public_key) );
    BIO_free( bio );
    if (! encodeBase64(Cipher(public_key, public_key_len), result))
      throwOpenSSLLastError("getPublicKey");
    
    return result.str();
  }
  string RSACrypto::getPublicKeyFingerPrint( void ) const
  {
    assertf(HAS_PUBLIC_KEY, "no public key")
    
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    uchar public_key[RSA_KEY_LENGTH] = { 0 };
    BIO* bio = BIO_new( BIO_s_mem() );
    i2d_RSAPublicKey_bio( bio, rsaKey );
    int public_key_len = BIO_read( bio, public_key, sizeof(public_key) );
    BIO_free( bio );
    return RSACrypto::getFingerPrint( Cipher(public_key, public_key_len) );
  }
  string RSACrypto::getFingerPrint( const string& key )
  {
    Cipher data;
    if ( ! (decodeBase64( key, data ) ) ) {
      throwOpenSSLLastError("set AES key");
    }
    return RSACrypto::getFingerPrint( data );
  }
  string RSACrypto::getFingerPrint( const Cipher& cipher )
  {
    assertf(! cipher.empty(), "no cipher text")
    
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, cipher.data(), cipher.size());
    SHA256_Final(hash, &sha256);
    
    unsigned char digest[RIPEMD160_DIGEST_LENGTH];
    RIPEMD160_CTX ctx;
    RIPEMD160_Init(&ctx);
    RIPEMD160_Update(&ctx, hash, SHA256_DIGEST_LENGTH);
    RIPEMD160_Final(digest, &ctx);
    
    string s = EncodeBase58(digest, digest + RIPEMD160_DIGEST_LENGTH);
/*
    std::vector<unsigned char> data(digest, digest + RIPEMD160_DIGEST_LENGTH);
    unsigned char result[RIPEMD160_DIGEST_LENGTH * 2];
    string s = string((char*)::encode_base58(result, RIPEMD160_DIGEST_LENGTH * 2, digest, RIPEMD160_DIGEST_LENGTH));
 */
    return s;
  }
  bool RSACrypto::hasPrivateKeyASN1( void ) const
  {
    return HAS_PRIVATE_KEY;
  }
  string RSACrypto::getPrivateKeyASN1( void ) const
  {
    assertf(HAS_PRIVATE_KEY, "no private key")
    Cipher result;    
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    unsigned char private_key[RSA_KEY_LENGTH] = { 0 };
    BIO* bio = BIO_new( BIO_s_mem() );
    i2d_RSAPrivateKey_bio( bio, rsaKey );
    int private_key_len = BIO_read( bio, private_key, sizeof(private_key) );
    BIO_free( bio );
    if (! encodeBase64(Cipher(private_key, private_key_len), result))
      throwOpenSSLLastError("getPrivateKey");
    
    return result.str();
  }
  bool RSACrypto::hasPublicKeyPEM( void ) const
  {
    return HAS_PUBLIC_KEY;
  }
  string RSACrypto::getPublicKeyPEM( void ) const
  {
    assertf(HAS_PUBLIC_KEY, "no public key")
    unsigned char public_key[RSA_KEY_LENGTH] = { 0 };
    BIO* bio = BIO_new( BIO_s_mem() );
    assertOpenSSL( PEM_write_bio_PUBKEY( bio, _pkey ));
    size_t public_key_len = BIO_read( bio, public_key, sizeof(public_key) );
    string key((const char*) public_key, public_key_len);
    BIO_free( bio );
    return key;
  }
  bool RSACrypto::hasPrivateKeyPEM( void ) const
  {
    return HAS_PRIVATE_KEY;
  }
  string RSACrypto::getPrivateKeyPEM( const char* password ) const
  {
    assertf(HAS_PRIVATE_KEY, "no private key")
    unsigned char private_key[RSA_KEY_LENGTH] = { 0 };
    BIO* bio = BIO_new( BIO_s_mem() );
    if (password) {
      assertOpenSSL(PEM_write_bio_PKCS8PrivateKey( bio, _pkey, EVP_des_ede3_cbc(), NULL, 0, NULL, (char*)password ) );
    } else {
      assertOpenSSL(PEM_write_bio_PKCS8PrivateKey( bio, _pkey, NULL, NULL, 0, NULL, NULL ) );
    }
    int private_key_len = BIO_read( bio, private_key, sizeof(private_key) );
    string key = private_key_len > 0 ? string((const char*) private_key, private_key_len) : "";
    BIO_free( bio );
    return key;
  }
  const string RSACrypto::getEncryptedSessionKey( ) const throw(security_integrity_error)
  {
    assertf(! _aesKeys.empty( ), "empty aes key")
    assertf(HAS_PUBLIC_KEY, "no public key")
    
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    ostringstream s;
    for (vector<Cipher>::const_iterator it = _aesKeys.begin() ; it != _aesKeys.end(); ) {
      Cipher data;
      Cipher result;
      if (! (encryptRSACipher( *it, rsaKey, data ) && encodeBase64( data, result )) )
        throwOpenSSLSecurityException("encrypt message");
      s << result.str();

      if (++it != _aesKeys.end())
        s << " ";
    }
    return s.str();
  }
  const string RSACrypto::setEncryptedSessionKey( const string& key ) throw(security_integrity_error)
  {
    assertf(! key.empty( ), "empty key")
    assertf(HAS_PRIVATE_KEY, "no private key")
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    const vector<string> keys = split(key, ' ');
    _aesKeys.clear();
    for (vector<string>::const_iterator it = keys.begin() ; it != keys.end(); ++it) {
      Cipher data;
      Cipher result;
      if ( decodeBase64( *it, data ) && decryptRSACipher( data, rsaKey, result ) ) {
        _aesKeys.push_back(result);
      }
    }
    if (_aesKeys.empty()) {
      throwOpenSSLSecurityException("decrypt key");
    }
    return this->getSessionKey();
  }
  string RSACrypto::signMessage( const string& message ) const
  {
    assertf(! message.empty( ), "empty message")
    assertf(HAS_PRIVATE_KEY, "no private key")
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    Cipher data;
    Cipher result;
    if ( ! (signCipher( message, rsaKey, data ) && encodeBase64(data, result)) ) {
      throwOpenSSLLastError("sign message");
    }
    return result.str();
  }
  bool RSACrypto::verifyMessage( const string& message, const string& signatur ) const
  {
    assertf(! message.empty( ), "empty message")
    assertf(! signatur.empty( ), "empty signatur")
    assertf(HAS_PUBLIC_KEY, "no public key")
    RSA* rsaKey = EVP_PKEY_get0_RSA(_pkey);
    Cipher data;
    return decodeBase64( signatur, data ) && verifyCipher( message, rsaKey, data );
  }
  
  // https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
  bool RSACrypto::encryptRSACipher( const Cipher& plaintext, RSA *rsaKey, Cipher& result  )
  {
    assertf ( rsaKey, "empty envelop" );
    assertf ( ! plaintext.empty() , "empty plain text" );
    result.clear();
    result.resize( RSA_size(rsaKey) );
    uchar* pbuf = const_cast<uchar*>(result.data());
    // RSA_PKCS1_OAEP_PADDING
    int	size = RSA_public_encrypt((unsigned int)plaintext.size(), plaintext.data(), pbuf, rsaKey, RSA_PKCS1_PADDING);
    if (size != (int)result.size()) {
      assertOpenSSL( false );
      result.clear();
      return false;
    }
    return true;
  }
  bool RSACrypto::decryptRSACipher( const Cipher& ciphertext, RSA *rsaKey, Cipher& result ) const
  {
    assertf ( rsaKey, "empty envelop" );
    assertf ( ! ciphertext.empty() , "empty cipher text" );
    result.clear();
    result.resize( RSA_size(rsaKey) );
    uchar* pbuf = const_cast<uchar*>(result.data());
    // RSA_PKCS1_OAEP_PADDING
    int	size = RSA_private_decrypt((unsigned int)ciphertext.size(), ciphertext.data(), pbuf, rsaKey, RSA_PKCS1_PADDING);
    if (size < 0) {
      assertOpenSSL( false );
      result.clear();
      return false;
    }
    result.resize(size);
    return true;
  }
  
  bool RSACrypto::signCipher( const Cipher& plaintext, RSA *rsaKey, Cipher& result ) const
  {
    assertf ( rsaKey, "empty envelop" );
    assertf ( ! plaintext.empty() , "empty plain text" );
    unsigned int iSignlen = RSA_size( rsaKey );
    result.clear();
    result.resize(iSignlen);
    
    uchar* pbuf = const_cast<uchar*>(result.data());
    if ( ! RSA_sign(NID_md5, plaintext.data(), (unsigned int)plaintext.size(),
                    pbuf, &iSignlen, rsaKey) ) {
      result.clear();
      return false;
    }
    return true;
  }
  bool RSACrypto::verifyCipher( const Cipher& plaintext, RSA *rsaKey, const Cipher& signatur ) const
  {
    assertf ( rsaKey, "empty envelop" );
    assertf ( ! plaintext.empty() , "empty plain text" );
    assertf ( ! signatur.empty() , "empty signature" );
    if ( RSA_verify(NID_md5, plaintext.data(), (unsigned int)plaintext.size(),
                    signatur.data(), (unsigned int)signatur.size(), rsaKey) ) {
      return true;
    }
    return false;
  }
  
  // When seeding your generators, you should use at least 256 bits (32 bytes) of material.
  void RSACrypto::initSeed( const void *buf, int num )
  {
    // https://wiki.openssl.org/index.php/Random_Numbers
    RAND_seed(buf, num);
  }

  bool RSACrypto::exportPEM_to_folder( EVP_PKEY* rsa, bool hasPrivateKey, const char* folder, const char* password )
  {
    assertf ( rsa, "empty envelop" );
    assertf ( folder, "empty folder argument" );

    mkdir( folder );
    
    char path[1024];
    char* d = ::stpcpy(path, folder?folder:".");
    if ( hasPrivateKey ) {
      strcpy(d, "/" PRIVATE_KEY_FILENAME);
      if ( writePEM( rsa, hasPrivateKey, path, password ) != 1 ) {
        return false;
      }
    } else {
      strcpy(d, "/" PUBLIC_KEY_FILENAME);
      if ( writePEM( rsa, hasPrivateKey, path, password ) != 1 ) {
        return false;
      }
    }
    return true;
  }
  EVP_PKEY* RSACrypto::importPEM_from_folder( EVP_PKEY** rsa, bool hasPrivateKey, const char* folder, const char* password )
  {
    assertf ( rsa, "empty envelop" );
    assertf ( folder, "empty folder argument" );
    char path[1024];
    char* d = ::stpcpy(path, folder?folder:".");
    if (hasPrivateKey) {
      strcpy(d, "/" PRIVATE_KEY_FILENAME);
      if ( readPEM( rsa, true, path, password ) != NULL ) {
        return *rsa;
      }
    }
    if (!hasPrivateKey) {
      strcpy(d, "/" PUBLIC_KEY_FILENAME);
      if ( readPEM( rsa, false, path, password ) != NULL ) {
        return *rsa;
      }
    }
    return NULL;
  }
  bool RSACrypto::erasePEM( const char* folder )
  {
    assertf ( folder, "empty folder argument" );
    char path[1024];
    char* d = ::stpcpy(path, folder?folder:".");
    bool removed = false;
    strcpy(d, "/" PUBLIC_KEY_FILENAME);
    if ( remove(path) == 0 )
      removed = true;
    strcpy(d, "/" PRIVATE_KEY_FILENAME);
    remove(path);
    if ( remove(path) == 0 )
      removed = true;
    return removed;
  }
  RSA* RSACrypto::loadKeyASN1( RSA** rsa, const string& key, bool privateKey )
  {
    assertf ( rsa, "empty envelop" );
    assertf ( ! key.empty(), "empty key" );
    BIO* bio = NULL;
    BIO* base64_filter = NULL;
    bio = BIO_new_mem_buf( (void*)key.c_str(), (int) key.length() );
    base64_filter = BIO_new( BIO_f_base64() );
    BIO_set_flags( base64_filter, BIO_FLAGS_BASE64_NO_NL );
    bio = BIO_push( base64_filter, bio );
    RSA* ret = NULL;
    if (privateKey) {
      ret = d2i_RSAPrivateKey_bio(bio, rsa);
    } else {
      ret = d2i_RSAPublicKey_bio(bio, rsa);
    }
    BIO_free_all( bio );
    assertOpenSSL( ret );
    return ret;
  }
  EVP_PKEY* RSACrypto::importKey( EVP_PKEY** rsa, const string& key, bool privateKey, const char* password )
  {
    assertf ( rsa, "empty envelop"  );
    assertf ( ! key.empty(), "empty key" );
    EVP_PKEY* ret = NULL;
    BIO* bio = BIO_new_mem_buf( (void*)key.c_str(), (int) key.length() );
    if (privateKey) {
      if (password) {
        ret = PEM_read_bio_PrivateKey( bio, rsa, NULL, (char*)password );
      } else {
        ret = PEM_read_bio_PrivateKey( bio, rsa, NULL, NULL );
      }
    } else {
      if (password) {
        ret = PEM_read_bio_PUBKEY( bio, rsa, NULL, (char*)password );
      } else {
        ret = PEM_read_bio_PUBKEY( bio, rsa, disable_passphrase_prompt, NULL );
      }
    }
    BIO_free_all( bio );
    assertOpenSSL( ret );
    return ret;
  }
  EVP_PKEY* RSACrypto::readPEM( EVP_PKEY** rsa, bool privateKey, const char* filename, const char* password )
  {
    assertf ( rsa, "empty envelop"  );
    assertf ( filename, "empty filename" );
    struct stat fileInfo;
    if (stat(filename, &fileInfo)) {
      // no such file
      return NULL;
    }
    
    BIO* bio = BIO_new_file( filename, "rb" );
    if (! assertOpenSSL(bio)) {
      return 0;
    }
    //  return a file BIO or NULL if an error occurred.
    EVP_PKEY* ret = NULL;
    if (privateKey) {
      if ( password ) {
        ret = PEM_read_bio_PrivateKey( bio, rsa, NULL, (void*)password );
      } else {
        ret = PEM_read_bio_PrivateKey( bio, rsa, disable_passphrase_prompt, NULL );
      }
    } else {
      ret = PEM_read_bio_PUBKEY( bio, rsa, NULL, NULL );
    }
    BIO_free_all( bio );
    assertOpenSSL( ret );
    return ret;
  }
  int RSACrypto::writePEM( EVP_PKEY* rsa, bool privateKey, const char* filename, const char* password )
  {
    assertf ( rsa, "empty envelop"  );
    assertf ( filename, "empty filename" );
    BIO* bio = BIO_new_file( filename, "w" );
    if (! assertOpenSSL(bio))
      return 0;
    
    int ret = 0;
    if (privateKey) {
      if ( password ) {
        ret = PEM_write_bio_PrivateKey( bio, rsa, EVP_des_ede3_cbc(), NULL, NULL, NULL, (void*) password );
      } else {
        ret = PEM_write_bio_PrivateKey( bio, rsa, NULL, NULL, NULL, disable_passphrase_prompt, NULL );
      }
    } else {
      ret = PEM_write_bio_PUBKEY( bio, rsa );
    }
    BIO_free_all( bio );
    assertOpenSSL( ret );
    return ret;
  }
  
}
