/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_AES_CRYPTO_H__
#define __EJIN_AES_CRYPTO_H__

#include "Declarations.h"

#include <openssl/evp.h>

#define AES_KEY_LENGTH   256
#define CBC_IV_LENGTH    128
#define GCM_IV_LENGTH    96
#define GCM_TAG_LENGTH   128
#define PKCS5_ITERATIONS 25600
#define PKCS5_SALT       { 0x53, 0x43, 0x2a, 0xa3, 0x16, 0xde, 0x7b, 0xc8 }

typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned char uchar;
typedef std::vector<uchar> uchar_array;

using std::string;
using std::vector;
using std::unique_ptr;

// forward declaration
class AESCryptoTest;
namespace ejin
{
  extern bool assertOpenSSL( bool condition );
  extern void throwOpenSSLLastError(const char *msg);
  extern void throwOpenSSLSecurityException(const char *msg);

  extern vector<string> split(const string &s, char delim);
  extern void initCryptoLib( );
  extern void setupCryptoLib( );
  extern void teardownCryptoLib( );
  
  class AESCrypto
  {
    friend class ::AESCryptoTest;

  public:
    
    enum Mode {ENCRYPT=1, DECRYPT=2, NO_ENCRYPTION=0};
    
    AESCrypto( void );
    AESCrypto( const string& key );
    AESCrypto( const AESCrypto& crypto ); // copy
    AESCrypto& operator=( const AESCrypto& crypto ); // assignment
    bool operator==( const AESCrypto& rhs ) const;
    ~AESCrypto();
  
    void convertToAESKey( const string& password );
    void setRandomSessionKey( const char* password = NULL );
    void setHmacSessionKeyFrom( const char* password );
    void clearSessionKey( );
    void setSessionKey( const string& key );
    const string getSessionKey( ) const;
    bool hasSessionKey( void ) const;

    const string getIV( void ) const;
    bool hasIV( void ) const;
    void setIV( const string& iv ) const;
    void clearIV( void ) const;
    void createIV( const char* password = NULL ) const;
    
    string encrypt( const string& message ) const throw(security_integrity_error);
    string decrypt( const string& message ) const throw(security_integrity_error);

    bool encryptFile( const char* sourceFile, const char* targetFile, bool base64 = false )
      const throw(security_integrity_error);
    bool decryptFile( const char* sourceFile, const char* targetFile, bool base64 = false )
      const throw(security_integrity_error);

    bool encryptToFile( const char* data, int length, const char* targetFile, bool base64 = false )
      const throw(security_integrity_error);
    bool decryptFromFile( char** data, size_t* length, const char* sourceFile, bool base64 = false )
      const throw(security_integrity_error);

  protected:
    
    // Data class wrapping a binary vector
    struct Cipher {
      Cipher( void );
      Cipher( const uchar_array& v );
      Cipher( const string& v );
      Cipher( size_t size );
      Cipher( const uchar* v, size_t size );
      Cipher( const Cipher& cipher );
      ~Cipher( void );
      Cipher& operator=( const Cipher& rhs );
      bool operator==( const Cipher& rhs ) const;
      void reset( void ) const;
      size_t offset( void ) const;
      size_t size( void ) const;
	    bool empty( void ) const;
      void clear( void );
      void resize( size_t size );
      void shift( size_t len ) const;
      const uchar_array array( void ) const;
      const uchar* data( void ) const;
      const string str( void ) const;
      const string hex( void ) const;
      Cipher& operator+=( const Cipher& cipher );
      Cipher& operator+=( const uchar_array& cipher );
      Cipher& operator+=( const string& str );
      Cipher& operator+=( const uchar* str );
    private:
      uchar_array _data;
      mutable size_t _offset;
    };
    friend AESCrypto::Cipher decodeKey(const string& key);

    bool encryptAESCipher( const Cipher& plaintext, const Cipher& aesKey, Cipher& result ) const;
    bool encryptAES_CBCCipher( const Cipher& plaintext, const Cipher& aesKey, Cipher& result ) const;
    bool encryptAES_GCMCipher( const Cipher& plaintext, const Cipher& aesKey, Cipher& result ) const;
    bool decryptAESCipher( const Cipher& ciphertext, const Cipher& aesKey, Cipher& result ) const;
    bool decryptAES_GCMCipher( const Cipher& ciphertext, const Cipher& aesKey, Cipher& result ) const;
    bool decryptAES_CBCCipher( const Cipher& ciphertext, const Cipher& aesKey, Cipher& result ) const;

    bool signCipher( const Cipher& ciphertext, Cipher& result ) const;
    bool verifyCipher( const Cipher& plaintext, const Cipher& signatur ) const;

    static const Cipher randomBits( int length, const char* salt = NULL );

    static bool encodeBase64( const Cipher& ciphertext, Cipher& result );
    static bool decodeBase64( const string &str, Cipher& result );
    static bool decodeBase64FromFile( const char* file, Cipher& result );
    static bool encodeBase64ToFile( const char* outfile, const Cipher& ciphertext );
    static Cipher decodeKey(const string& key);
    static string encodeKey(const Cipher& key);

    static void mkdir(const char *dir);

    static bool readFile( const char* filename, Cipher& data );
    static bool writeFile( const char* filename, const Cipher& data );

    vector<Cipher>          _aesKeys;
    mutable Cipher          _ivKey;
    mutable EVP_CIPHER_CTX* _aesCtx;
  };
}

#endif // __EJIN_AES_CRYPTO_H__
