/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// https://wiki.openssl.org/index.php/EVP_Authenticated_Encryption_and_Decryption
// https://stackoverflow.com/questions/12153009/openssl-c-example-of-aes-gcm-using-evp-interfaces

#include "AESCrypto.h"
#include "UsingEjinTypes.h"

#include <assert.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/aes.h>
#include <openssl/engine.h>
#include <random>

#ifdef __APPLE__
#include <Security/Security.h>
#endif

template<typename Out>
void split(const std::string &s, char delim, Out result) {
  std::stringstream ss;
  ss.str(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    *(result++) = item;
  }
}

namespace ejin
{
  using std::runtime_error;
  using std::stringstream;
  using std::runtime_error;
  using std::function;
  using std::underflow_error;
  using std::overflow_error;
  using std::default_random_engine;
  using std::default_random_engine;
  using std::uniform_int_distribution;
  
  bool assertOpenSSL( bool condition )
  {
#ifdef DEBUG
    assertf(condition, "openssl error: %s", ERR_error_string(ERR_get_error(), NULL) );
#endif
    return condition;
  }
  
  void throwOpenSSLLastError(const char *msg)
  {
#ifdef DEBUG
    //convert the stream buffer into a string
    char * err = new char[128];
    //ERR_load_crypto_strings();
    ERR_error_string(ERR_get_error(), err);
    
    stringstream ss;
    ss << msg << " error: " << err;
    delete [] err;
    throw runtime_error(ss.str());
#else
    throw runtime_error(msg);
#endif
  }
  
  void throwOpenSSLSecurityException(const char *msg)
  {
#ifdef DEBUG
    //convert the stream buffer into a string
    static char * err = new char[128];
    //ERR_load_crypto_strings();
    ERR_error_string(ERR_get_error(), err);
    _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, err);
#else
    _throw_exception(SECURITY_INTEGRITY_VIOLATION_EXCEPTION, msg);
#endif
  }

  std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    ::split(s, delim, std::back_inserter(elems));
    return elems;
  }
  void initCryptoLib( )
  {
    // https://wiki.openssl.org/index.php/Library_Initialization
    // If you are using 1.1.0 or above then you don't need to take any further steps.
//    SSL_load_error_strings();
//    SSL_library_init();
  }
  void setupCryptoLib( )
  {
    // https://wiki.openssl.org/index.php/Library_Initialization
    // If you are using 1.1.0 or above then you don't need to take any further steps.
//    OpenSSL_add_all_ciphers();
//    OpenSSL_add_all_digests();
//    ERR_load_crypto_strings();
  }
  void teardownCryptoLib( )
  {
//    ERR_free_strings();
  }
  
#pragma GCC diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
  static uchar_array charset()
  {
    uchar_array all(256);
    for (int i=0; i<256; i++)
      all[i] = (uchar) i;
    return all;
    /*
     return char_array(
     {'0','1','2','3','4',
     '5','6','7','8','9',
     'A','B','C','D','E','F',
     'G','H','I','J','K',
     'L','M','N','O','P',
     'Q','R','S','T','U',
     'V','W','X','Y','Z',
     'a','b','c','d','e','f',
     'g','h','i','j','k',
     'l','m','n','o','p',
     'q','r','s','t','u',
     'v','w','x','y','z'
     });
     */
  };  
#pragma GCC diagnostic pop
  AESCrypto::AESCrypto( void ): _aesKeys(), _ivKey()
  {
    _aesCtx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_reset(_aesCtx);
  }
  AESCrypto::AESCrypto( const string& key ): _aesKeys(), _ivKey()
  {
    _aesCtx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_reset(_aesCtx);
    this->setSessionKey( key );
  }
  AESCrypto::AESCrypto( const AESCrypto& crypto ): _aesCtx(), _aesKeys(), _ivKey()
  {
    this->operator=( crypto );
  }
  AESCrypto& AESCrypto::operator=( const AESCrypto& crypto )
  {
    if (this != &crypto) {
      // init cipher context
      EVP_CIPHER_CTX_reset(_aesCtx);
      
      this->_aesKeys = crypto._aesKeys;
      this->_ivKey = crypto._ivKey;
    }
    return *this;
  }
  bool AESCrypto::operator==( const AESCrypto& crypto ) const {
    if (this == &crypto)
      return true;
    return _aesKeys == crypto._aesKeys && _ivKey == crypto._ivKey;
  }

  AESCrypto::~AESCrypto( void )
  {
    EVP_CIPHER_CTX_free(_aesCtx);
  }

  void AESCrypto::convertToAESKey( const string& password )
  {
    // https://stackoverflow.com/questions/9488919/password-to-key-function-compatible-with-openssl-commands
    unsigned char key[EVP_MAX_KEY_LENGTH], iv[EVP_MAX_IV_LENGTH];
    const unsigned char *salt = NULL;
    const EVP_CIPHER* cipher = EVP_aes_256_cbc();
    int key_len = EVP_BytesToKey(cipher, EVP_md5(), salt,
                                 (unsigned char *) password.c_str(), (int)password.size(), 1, key, iv);
    assertOpenSSL( key_len > 0 );
    _aesKeys.clear();
    _aesKeys.push_back(Cipher(key, key_len));
  }
  void AESCrypto::setRandomSessionKey( const char* password )
  {
    _aesKeys.insert( _aesKeys.begin(), randomBits( AES_KEY_LENGTH, password ) );
  }
  void AESCrypto::setHmacSessionKeyFrom( const char* password )
  {
    //key = EVP_PKEY_new_mac_key(EVP_PKEY_HMAC, NULL, "password", strlen("password"));

    assertf( password, "password is mandatory" );
    Cipher result;
    // derive from password and salt with hash function
    unsigned char salt[] = PKCS5_SALT;
    Cipher key( AES_KEY_LENGTH / 8 );
    PKCS5_PBKDF2_HMAC_SHA1(password, (int)strlen(password),
                           salt, sizeof(salt),
                           PKCS5_ITERATIONS,
                           (int)key.size(), (uchar*)key.data());
    _aesKeys.insert( _aesKeys.begin(), key );
  }
  void AESCrypto::setSessionKey( const string& key )
  {
    vector<string> keys = split(key, ' ');
    _aesKeys.clear();
    _aesKeys.resize( keys.size() );
    std::transform (keys.begin(), keys.end(), _aesKeys.begin(), decodeKey);
  }
  void AESCrypto::clearSessionKey( void )
  {
    return _aesKeys.clear();
  }
  bool AESCrypto::hasSessionKey( void ) const
  {
    return ! _aesKeys.empty( );
  }
  const string AESCrypto::getSessionKey( void ) const
  {
    assertf(! _aesKeys.empty(), "aes key is empty");
    
    vector<string> keys(_aesKeys.size());
    stringstream res;
    std::transform (_aesKeys.begin(), _aesKeys.end(), keys.begin(), encodeKey);
    copy(keys.begin(), keys.end(), std::ostream_iterator<string>(res, " ") );
    string result( res.str() );
    result.pop_back();
    return result;
  }
  
  string AESCrypto::encrypt( const string& message ) const throw(security_integrity_error)
  {
    assertf(! _aesKeys.empty(), "aes key is empty");
    if ( message.empty() )
      return "";
    
    Cipher data;
    Cipher result;
    if ( ! (encryptAESCipher( message, *_aesKeys.begin(), data ) && encodeBase64( data, result )) ) {
      throwOpenSSLSecurityException("encrypt message");
    }
    return result.str();
  }
  string AESCrypto::decrypt( const string& message ) const throw(security_integrity_error)
  {
    assertf(! _aesKeys.empty(), "aes key is empty");
    if ( message.empty() )
      return "";

    Cipher data;
    if ( ! decodeBase64( message, data ) ) {
      throwOpenSSLSecurityException("decrypt message");
    }

    for ( vector<Cipher>::const_iterator it = _aesKeys.begin() ; it != _aesKeys.end(); ++it ) {
      Cipher result;
      data.reset();
      if ( decryptAESCipher( data, *it, result ) ) {
        return result.str();
      }
    }
    throwOpenSSLSecurityException("decrypt message");
    return string();
  }
  
  bool AESCrypto::encryptFile( const char* sourceFile, const char* targetFile, bool base64 ) const throw(security_integrity_error)
  {
    assertf( sourceFile, "source file is mandatory" );
    assertf( targetFile, "target file is mandatory" );
    assertf( ! _aesKeys.empty(), "aes key is empty");

    Cipher source;
    Cipher target;
    
    bool success = true;
    success = success && readFile( sourceFile, source );
    success = success && encryptAESCipher( source, *_aesKeys.begin(), target );
    if ( base64 ) {
      success = success && encodeBase64ToFile( targetFile, target );
    } else {
      success = success && writeFile( targetFile, target );
    }
    return success;
  }
  bool AESCrypto::decryptFile( const char* sourceFile, const char* targetFile, bool base64 ) const throw(security_integrity_error)
  {
    assertf( sourceFile, "source file is mandatory" );
    assertf( targetFile, "target file is mandatory" );
    assertf( ! _aesKeys.empty(), "aes key is empty");

    Cipher source;
    Cipher target;
    
    bool success = true;
    if ( base64 ) {
      success = success && decodeBase64FromFile( sourceFile, source );
    } else {
      success = success && readFile( sourceFile, source );
    }
    if (success) {
      for ( vector<Cipher>::const_iterator it = _aesKeys.begin() ; it != _aesKeys.end(); ) {
        if ((success = decryptAESCipher( source, *it, target )))
          break;
      }
    }
    success = success && writeFile( targetFile, target );
    return success;
  }
  bool AESCrypto::encryptToFile( const char* data, int length, const char* targetFile, bool base64 )
    const throw(security_integrity_error)
  {
    assertf( data, "data is mandatory" );
    assertf( length > 0, "data is mandatory" );
    assertf( targetFile, "target file is mandatory" );
    assertf( ! _aesKeys.empty(), "aes key is empty");

    Cipher source( (const uchar*)data, length );
    Cipher target;

    bool success = true;
    success = success && encryptAESCipher( source, *_aesKeys.begin(), target );
    if ( base64 ) {
      success = success && encodeBase64ToFile( targetFile, target );
    } else {
      success = success && writeFile( targetFile, target );
    }
    return success;
  }
  bool AESCrypto::decryptFromFile( char** data, size_t* length, const char* sourceFile, bool base64 )
    const throw(security_integrity_error)
  {
    assertf( data && length, "data is mandatory" );
    assertf( sourceFile, "source file is mandatory" );
    assertf( ! _aesKeys.empty(), "aes key is empty");
    
    Cipher source;
    Cipher target;
    
    bool success = true;
    if ( base64 ) {
      success = success && decodeBase64FromFile( sourceFile, source );
    } else {
      success = success && readFile( sourceFile, source );
    }
    if (success) {
      for ( vector<Cipher>::const_iterator it = _aesKeys.begin() ; it != _aesKeys.end(); ) {
        if ((success = decryptAESCipher( source, *it, target )))
          break;
      }
    }
    if (success && ! target.empty()) {
      *length = target.size();
      *data = new char[target.size()];
      if (! *data)
        ejin::_throw_exception(ejin::MEMORY_EXCEPTION, "Out of memory");
      bcopy(target.data(), (void*)*data, target.size());
      return true;
    }
    return false;
  }

  void AESCrypto::createIV( const char* password ) const
  {
    _ivKey = randomBits( GCM_IV_LENGTH, password );
  }
  const string AESCrypto::getIV( void ) const
  {
    Cipher result;
    if (! encodeBase64(_ivKey, result))
      throwOpenSSLLastError("getIV");
    return result.str();
  }
  void AESCrypto::setIV( const string& iv ) const
  {
    if ( !iv.empty() ) {
      if (! decodeBase64(iv, _ivKey))
        throwOpenSSLLastError("set IV key");
      assertf( _ivKey.size() * 8 == GCM_IV_LENGTH, "invalid iv length" );
    }
  }
  void AESCrypto::clearIV( void ) const
  {
    _ivKey.clear();
  }
  bool AESCrypto::hasIV( void ) const
  {
    return ! _ivKey.empty();
  }
  
  bool AESCrypto::encryptAESCipher( const Cipher& plaintext, const Cipher& aesKey, Cipher& result ) const
  {
    result.clear();
    result += "\x01"; // AES-GCM marker
    return encryptAES_GCMCipher( plaintext, aesKey, result );
  }
  bool AESCrypto::encryptAES_GCMCipher( const Cipher& plaintext, const Cipher& aesKey, Cipher& result ) const
  {
    assertf( ! _aesKeys.empty(), "aes key is empty");
    // ensure IV key is set
    if ( _ivKey.empty() )
      this->createIV();
    
    size_t offset = result.offset();
    
    int ciphertext_len=0;
    int ciphertext_padlen=0;
    
    result.resize( offset + _ivKey.size() + plaintext.size() + AES_BLOCK_SIZE + 20 + 12 );
    
    // clean context first
    EVP_CIPHER_CTX_reset(_aesCtx);
    
    // encode the useing iv key as prefix in the cipher itself
    result += _ivKey;
    if (! EVP_EncryptInit_ex(_aesCtx, EVP_aes_256_gcm(), NULL, aesKey.data(), _ivKey.data()) ) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    EVP_CIPHER_CTX_ctrl(_aesCtx, EVP_CTRL_GCM_SET_IVLEN, GCM_IV_LENGTH/8, NULL);
    EVP_EncryptInit_ex(_aesCtx, NULL, NULL, aesKey.data(), _ivKey.data());
    
    uchar* pbuf = const_cast<uchar*>(result.data());
    if (! EVP_EncryptUpdate(_aesCtx,
                                           pbuf,
                                           &ciphertext_len,
                                           plaintext.data(),
                                           (int)plaintext.size())) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    // pad at the end
    if (! EVP_EncryptFinal_ex(_aesCtx,pbuf+=ciphertext_len,&ciphertext_padlen)) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    // add tag
    if (! EVP_CIPHER_CTX_ctrl(_aesCtx, EVP_CTRL_GCM_GET_TAG, GCM_TAG_LENGTH/8, pbuf)) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    result.reset();
    result.resize( offset + _ivKey.size() + ciphertext_len + ciphertext_padlen + GCM_TAG_LENGTH/8 );
    
    EVP_CIPHER_CTX_reset(_aesCtx);
    return true;
  }  
  bool AESCrypto::encryptAES_CBCCipher( const Cipher& plaintext, const Cipher& aesKey, Cipher& result ) const
  {
    assertf( ! _aesKeys.empty(), "aes key is empty");
    // ensure IV key is set
    if ( _ivKey.empty() )
      this->createIV();
    
    size_t offset = result.offset();
    
    int ciphertext_len=0;
    int ciphertext_padlen=0;
    
    result.resize( offset + _ivKey.size() + plaintext.size() + AES_BLOCK_SIZE + 20 );
    
    // clean context first
    EVP_CIPHER_CTX_reset(_aesCtx);
    
    // encode the useing iv key as prefix in the cipher itself
    result += _ivKey;
    if (! EVP_EncryptInit_ex(_aesCtx, EVP_aes_256_cbc(), NULL, aesKey.data(), _ivKey.data()) ) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    EVP_CIPHER_CTX_set_key_length(_aesCtx, AES_KEY_LENGTH);
    
    uchar* pbuf = const_cast<uchar*>(result.data());
    if (! EVP_EncryptUpdate(_aesCtx,
                                           pbuf,
                                           &ciphertext_len,
                                           plaintext.data(),
                                           (int)plaintext.size())) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    // pad at the end
    if (! EVP_EncryptFinal_ex(_aesCtx,pbuf+=ciphertext_len,&ciphertext_padlen)) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    result.reset();
    result.resize( offset + _ivKey.size() + ciphertext_len + ciphertext_padlen);
    
    EVP_CIPHER_CTX_reset(_aesCtx);
    return true;
  }

  bool AESCrypto::decryptAESCipher( const Cipher& ciphertext, const Cipher& aesKey, Cipher& result ) const
  {
    int mode = ciphertext.data()[0];
    switch ( mode ) {
      case 1:
        ciphertext.shift(1);
        return decryptAES_GCMCipher( ciphertext, aesKey, result );
      default:
        return decryptAES_CBCCipher( ciphertext, aesKey, result );
    }
  }
  bool AESCrypto::decryptAES_GCMCipher( const Cipher& ciphertext, const Cipher& aesKey, Cipher& result ) const
  {
    assertf( ! _aesKeys.empty(), "aes key is empty");
    
    int ivLength = GCM_IV_LENGTH / 8;
    int result_len = 0;
    result.clear();
    result.resize(ciphertext.size()+20);
    
    // read mandatory IV key at the beginning
    Cipher ivKey(ciphertext.data(), ivLength);
    ciphertext.shift( ivLength );
    
    // clean context first
    EVP_CIPHER_CTX_reset(_aesCtx);
    
    //    if (! assert_openssl(EVP_DecryptInit_ex(&_aesCtx, EVP_aes_256_cbc(), NULL, aesKey.data(), ivKey.data()))) {
    if (! EVP_DecryptInit_ex(_aesCtx, EVP_aes_256_gcm(), NULL, aesKey.data(), ivKey.data())) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    EVP_CIPHER_CTX_ctrl(_aesCtx, EVP_CTRL_GCM_SET_IVLEN, GCM_IV_LENGTH/8, NULL);
    EVP_DecryptInit_ex(_aesCtx, NULL, NULL, aesKey.data(), ivKey.data());
    //EVP_CIPHER_CTX_set_key_length(_aesCtx, AES_KEY_LENGTH);
    
    uchar* pbuf = const_cast<uchar*>(result.data());
    if (! EVP_DecryptUpdate(_aesCtx,
                                           pbuf,
                                           &result_len,
                                           ciphertext.data(),
                                           (int) ciphertext.size() - GCM_TAG_LENGTH/8)) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    // interpret last bytes as tag
    EVP_CIPHER_CTX_ctrl(_aesCtx, EVP_CTRL_GCM_SET_TAG, GCM_TAG_LENGTH/8,
                        (uchar*)(ciphertext.data() + ciphertext.size() - GCM_TAG_LENGTH/8));
    
    int result_padlen=0;
    if ( EVP_DecryptFinal_ex(_aesCtx,pbuf+1+result_len,&result_padlen) <= 0 ) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    result.reset();
    result.resize(result_len + result_padlen);
    this->_ivKey = ivKey;
    EVP_CIPHER_CTX_reset(_aesCtx);
    return true;
  }
  bool AESCrypto::decryptAES_CBCCipher( const Cipher& ciphertext, const Cipher& aesKey, Cipher& result ) const
  {
    assertf( ! _aesKeys.empty(), "aes key is empty");

    int ivLength = CBC_IV_LENGTH / 8;
    int result_len = 0;
    result.clear();
    result.resize(ciphertext.size()+20);
    
    // read mandatory IV key at the beginning
    Cipher ivKey(ciphertext.data(), ivLength);
    ciphertext.shift( ivLength );
    
    // clean context first
    EVP_CIPHER_CTX_reset(_aesCtx);
    
    if (! EVP_DecryptInit_ex(_aesCtx, EVP_aes_256_cbc(), NULL, aesKey.data(), ivKey.data())) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    EVP_CIPHER_CTX_set_key_length(_aesCtx, AES_KEY_LENGTH);
    
    uchar* pbuf = const_cast<uchar*>(result.data());
    if (! EVP_DecryptUpdate(_aesCtx,
                                           pbuf,
                                           &result_len,
                                           ciphertext.data(),
                                           (int) ciphertext.size())) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    int result_padlen=0;
    if (! EVP_DecryptFinal_ex(_aesCtx,pbuf+result_len,&result_padlen)) {
      EVP_CIPHER_CTX_reset(_aesCtx);
      result.clear();
      return false;
    }
    
    result.reset();
    result.resize(result_len + result_padlen);
    this->_ivKey = ivKey;
    EVP_CIPHER_CTX_reset(_aesCtx);
    return true;
  }

  const AESCrypto::Cipher AESCrypto::randomBits( int length, const char* salt )
  {
    assert ( length > 0 );

#ifdef __APPLE__
    uchar_array randomBytes(length/8, 0);
    int rc = SecRandomCopyBytes(kSecRandomDefault, randomBytes.size(), &(randomBytes[0]));
    if (rc != 0) {
        std::cout << "Failed: " << rc << std::endl;
        return 1;
    }
    return randomBytes;
#else
     
    // create the character set.
    const auto ch_set = charset();

    default_random_engine rng(std::random_device{}());
    if (salt)
      rng = default_random_engine((unsigned int)std::hash<const char*>()(salt));
    uniform_int_distribution<> dist(0, (int)ch_set.size()-1);

    auto randchar = [ ch_set,&dist,&rng ](){ return ch_set[ dist(rng) ];};

    uchar_array randomBytes(length/8,0);
    generate_n( randomBytes.begin(), length/8, randchar );
    return randomBytes;
#endif
  }
  
  bool AESCrypto::encodeBase64( const Cipher& ciphertext, Cipher& result )
  {
    assert ( ciphertext.size() > 0 );
    
    bool failure = false;
    
    BIO* bm  = BIO_new(BIO_s_mem());
    BIO_set_flags( bm, BIO_FLAGS_BASE64_NO_NL );
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO_set_flags( b64, BIO_FLAGS_BASE64_NO_NL );
    b64 = BIO_push(b64, bm);
    if ( BIO_write(b64,ciphertext.data(), (int)ciphertext.size()) < 0 ) {
      failure = true;
    }
    if (BIO_flush(b64) < 1) {
      failure = true;
    }
    if (! failure) {
      BUF_MEM *bptr=0;
      BIO_get_mem_ptr(b64, &bptr);
      result.clear();
      result += uchar_array(bptr->data, bptr->data+bptr->length);
      result.reset();
    }
    BIO_free_all(b64);
    
    assertOpenSSL( !failure );
    return !failure;
  }
  bool AESCrypto::encodeBase64ToFile( const char* outfile, const Cipher& ciphertext )
  {
    assert ( ciphertext.size() > 0 );
    assert ( outfile );
    
    BIO *bo, *b64;
    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags( b64, BIO_FLAGS_BASE64_NO_NL );
    bo = BIO_new( BIO_s_file() );
    BIO_set_flags( bo, BIO_FLAGS_BASE64_NO_NL );
    bo = BIO_push( b64, bo );

    bool failure = false;
    if( failure || BIO_write_filename(b64, (char *)outfile) < 1) {
      failure = true;
    }
    if ( BIO_write(b64, ciphertext.data(), (int)ciphertext.size() ) < 0 ) {
      failure = true;
    }
    if (BIO_flush(b64) < 1) {
      failure = true;
    }
    BIO_free_all(b64);
    
    assertOpenSSL( !failure );
    return !failure;
  }
  
  bool AESCrypto::decodeBase64( const string &str, Cipher& result )
  {
    assert ( ! str.empty() );
    result.clear();
    
    BIO* bm = BIO_new_mem_buf( (void*)str.c_str(), (int) str.length() );
    BIO_set_flags( bm, BIO_FLAGS_BASE64_NO_NL );
    BIO* b64 = BIO_new( BIO_f_base64() );
    BIO_set_flags( b64, BIO_FLAGS_BASE64_NO_NL );
    b64 = BIO_push(b64, bm);
    
    BIO* bio_out = BIO_new( BIO_s_mem() );
    uchar inbuf[512];
    int inlen;
    while( (inlen = BIO_read(b64, inbuf, 512)) > 0 ){
      BIO_write( bio_out, inbuf, inlen );
    }
    BIO_flush( bio_out );
    
    uchar* new_data = NULL;
    long bytes_written = BIO_get_mem_data( bio_out, &new_data );
    result += uchar_array(new_data, new_data+bytes_written);
    result.reset();
    
    BIO_free_all( b64 );
    BIO_free_all( bio_out );
    
    return bytes_written > 0;
  }
  bool AESCrypto::decodeBase64FromFile( const char* file, Cipher& result )
  {
    assert ( file );
    result.clear();
    
    BIO* bm = BIO_new_file( file, "r" );
    if ( ! bm )
      return false;
	  
    bool failure = false;
    BIO_set_flags( bm, BIO_FLAGS_BASE64_NO_NL );
    BIO* b64 = BIO_new( BIO_f_base64() );
    BIO_set_flags( b64, BIO_FLAGS_BASE64_NO_NL );
    b64 = BIO_push(b64, bm);
    
    BIO* bio_out = BIO_new( BIO_s_mem() );
    uchar inbuf[512];
    int inlen;
    while( (inlen = BIO_read(b64, inbuf, 512)) > 0 ){
      BIO_write( bio_out, inbuf, inlen );
    }
    BIO_flush( bio_out );
    
    uchar* new_data = NULL;
    long bytes_written = BIO_get_mem_data( bio_out, &new_data );
    result += uchar_array(new_data, new_data+bytes_written);
    result.reset();
    
    BIO_free_all( b64 );
    BIO_free_all( bio_out );
    
    return !failure;
  }
  AESCrypto::Cipher AESCrypto::decodeKey(const string& key)
  {
    AESCrypto::Cipher cipher;
    if (! decodeBase64(key, cipher))
      throwOpenSSLLastError("set AES key");
    assertf( cipher.size() * 8 == AES_KEY_LENGTH, "invalid iv length" );
    return cipher;
  }
  string AESCrypto::encodeKey(const Cipher& key)
  {
    AESCrypto::Cipher result;
    if (! encodeBase64(key, result))
      throwOpenSSLLastError("encodeKey");
    return result.str();
  }

  void AESCrypto::mkdir( const char *dir )
  {
    char tmp[1024];
    char *p = NULL;
    size_t len;
    
    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);
    if(tmp[len - 1] == '/')
      tmp[len - 1] = 0;
    for(p = tmp + 1; *p; p++)
      if(*p == '/') {
        *p = 0;
        ::mkdir(tmp, S_IRWXU);
        *p = '/';
      }
    ::mkdir(tmp, S_IRWXU);
  }

  bool AESCrypto::readFile( const char* filename, Cipher& data )
  {
    assertf( filename, "filename is mandatory" );
    data.clear();
    std::ifstream ifs( filename );
    if (!ifs) {
      return false;
    }
    data += uchar_array(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
    data.reset();
    return true;
  }
  bool AESCrypto::writeFile( const char* filename, const Cipher& data )
  {
    assertf( filename, "filename is mandatory" );
    std::ofstream ofs(filename, std::ios::binary);
    if (!ofs) {
      return false;
    }
    ofs.write(reinterpret_cast<const char*>(data.data()), data.size());
    ofs.close();
    return true;
  }

  // ============================
  // Inner Helper Class
  // ============================
  
  AESCrypto::Cipher::Cipher( void ) : _data(), _offset(0) { }
  AESCrypto::Cipher::Cipher( const uchar_array& v ) : _data(v), _offset(0) { }
  AESCrypto::Cipher::Cipher( const string& v ) : _data(v.begin(), v.end()), _offset(0) { }
  AESCrypto::Cipher::Cipher( size_t size ) : _data( size, 0 ), _offset(0) { }
  AESCrypto::Cipher::Cipher( const uchar* v, size_t size ) : _data( size ), _offset(0) { copy( v, v+size, _data.begin() ); }
  AESCrypto::Cipher::Cipher( const Cipher& d ) : _data(d._data), _offset(d._offset) { }
  AESCrypto::Cipher::~Cipher( void ) { }
  bool AESCrypto::Cipher::operator==( const AESCrypto::Cipher& rhs ) const
  {
    if (this == &rhs)
      return true;
    
    return this->_data == rhs._data;
  }
  size_t AESCrypto::Cipher::size( void ) const { return _data.size() - _offset; }
  size_t AESCrypto::Cipher::offset( void ) const { return _offset; }
  void AESCrypto::Cipher::clear( void ) { _data.clear(); _offset = 0; }
  bool AESCrypto::Cipher::empty( void ) const { return _data.size() == 0; }
  void AESCrypto::Cipher::reset( void ) const { _offset = 0; }
  void AESCrypto::Cipher::shift( size_t o ) const { _offset += o; }
  void AESCrypto::Cipher::resize( size_t size ) { _data.resize(_offset+size, 0); }
  AESCrypto::Cipher& AESCrypto::Cipher::operator=( const AESCrypto::Cipher& rhs )
  {
    if (this != &rhs) {
      this->_offset = rhs._offset;
      this->_data = rhs._data;
    }
    return *this;
  }
  AESCrypto::Cipher& AESCrypto::Cipher::operator+=( const uchar_array& str )
  {
    size_t remaining = _data.size() - _offset;
    if ( remaining == 0 ) {
      copy( str.begin(), str.end(), back_inserter(_data) );
    } else {
      if ( remaining < str.size() ) {
        _data.resize(str.size()-remaining, 0);
      }
      copy ( str.begin(), str.end(), _data.begin() + _offset );
    }
    _offset += str.size();
    return *this;
  };
  AESCrypto::Cipher& AESCrypto::Cipher::operator+=( const AESCrypto::Cipher& cipher )
  {
    return this->operator+=(cipher.array());
  }
  AESCrypto::Cipher& AESCrypto::Cipher::operator+=( const string& str )
  {
    uchar_array data;
    copy( str.begin(), str.end(), back_inserter(data));
    return this->operator+=(data);
  }
  AESCrypto::Cipher& AESCrypto::Cipher::operator+=( const uchar* str )
  {
    uchar_array data;
    while (str && *str) {
      data.push_back(*str++);
    }
    return this->operator+=(data);
  }
  const uchar_array AESCrypto::Cipher::array( void ) const
  {
    return uchar_array(_data.begin() + _offset, _data.end());
  }
  const uchar* AESCrypto::Cipher::data( void ) const { return &_data[_offset]; }
  const string AESCrypto::Cipher::str( void ) const { return string(_data.begin()+_offset, _data.end()); }
  const string AESCrypto::Cipher::hex( void ) const {
    std::stringstream ss;
    size_t size = _data.end() - _data.begin()+_offset;
    for(size_t i=_offset; i<_offset+size; ++i)
      ss << std::hex << (int)_data[i];
    return ss.str();
  }
}
