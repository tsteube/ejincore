
 Use the below command to generate RSA keys with length of 2048.
 
 openssl genrsa -out private.pem 2048
 Extract public key from private.pem with the following command.
 
 openssl rsa -in private.pem -outform PEM -pubout -out public.pem
 public.pem is RSA public key in PEM format.
 private.pem is RSA private key in PEM format.

##################

Suppose you encrypt two messages with the same key, and the two messages begin with the same 16 bytes of plaintext. (16 bytes is the block size for AES, regardless of the key size.) Will the first block of ciphertext be the same? If it is, you're already leaking some information to the attacker. Whether this information is sensitive or not depends on your application, but it's already a bad sign. If the encryption leaks more than the sign of the messages, it's not doing its job.

The basic idea of an IV is to prepend a bit of random content to each message, in a principled way. How this works precisely depends on the mode. (The core AES operation only works on 16-byte blocks. A mode is a way to extend this to longer messages.) For example, with CBC, the encryption of each block is computed from the key, the plaintext block and the ciphertext of the previous block; for the very first block, the IV is used instead of the ciphertext of the non-existent previous block. The IV is normally sent in cleartext alongside the ciphertext, usually it is sent a the first 16 bytes of the encrypted message.

CTR mode technically uses a counter and not an IV, but operationally they work very similarly: a 16-byte random value is generated at random by the sender and sent at the beginning of the encrypted message. With CTR mode, reusing that value for another message is catastrophic, because CTR works by XORing the plaintext with a pseudorandom stream deduced from the key and counter. If you have two encrypted messages that use the same counter value, their XOR is the XOR of the two plaintexts.

There are more attacks against improperly-chosen IVs than I've listed here. Generate a random IV for each message (using a cryptographic-quality random generator, the same you'd use to generate a key), and you'll be fine.

There is one exception: if you generate a fresh key for each message, you can pick a predictable IV (all-bits 0 or whatever). You still need to use a mode with an IV (ECB is not fine, for example it exposes repetitions in the plaintext since two identical input blocks will have the same encryption). That's a rare case though (it arises for storage, not for communication).

Note that encryption a priori only ensures the confidentiality of the data, not its integrity. Depending on what you do with the data, this may be a problem. In particular, if an attacker can submit tentative ciphertexts to be decrypted, or can provide additional plaintexts to be encrypted, this can expose some information. Some modes such as EAX and GCM provide authenticated encryption: a ciphertext will only be decrypted if it's genuine. Use one of these if possible.

Note also that AES-128 is just as secure in practice as AES-256.

##################

Usage of the openssl enc command-line option is described there. Below, I will answer your question, but don't forget to have a look at the last part of my text, where I take a look at what happens under the hood. It is... instructive.

OpenSSL uses a salted key derivation algorithm. The salt is a piece of random bytes which are generated when encrypting, and stored in the file header; upon decryption, the salt is retrieved from the header, and the key and IV are recomputed from the provided password and the salt value.

On the command-line, you can use the -P option (uppercase P) to print out the salt, key and IV, and then exit. You can also use the -p (lowercase P) to print out the salt, key and IV, and then proceed with the encryption. First try this out:

openssl enc -aes-256-cbc -pass pass:MYPASSWORD -P
If you run this command several times, you will notice that each invocation returns different values ! That's because, in the absence of the -d flag, openssl enc does encryption and generates a random salt each time. Since the salt varies, so do the key and IV. Thus, the -P flag is not very useful when encrypting; the -p flag, however, can be used. Let's try again; this time, we have the file foo_clear which we want to encrypt into foo_enc. Let's run this:

openssl enc -aes-256-cbc -pass pass:MYPASSWORD -p -in foo_clear -out foo_enc
This command will encrypt the file (thus creating foo_enc) and print out something like this:

salt=A68D6E406A087F05
key=E7C8836AD32C688444E3928F69F046715F8B33AF2E52A6E67A626B586DE8024E
iv =B9F128D827203729BE52A834CC0890B7
These values are the salt, key and IV which were actually used to encrypt the file.

If I want to get them back afterwards, I can use the -P flag in conjunction with the -d flag:

openssl enc -aes-256-cbc -pass pass:MYPASSWORD -d -P -in foo_enc
which will print out the same salt, key and IV than above, every time. How so ? That's because this time we are decrypting, so the header of foo_enc is read, and the salt retrieved. For a given salt value, derivation of the password into key and IV is deterministic.

Moreover, this key-and-IV retrieval is fast, even if the file is very long, because the -P flag prevents actual decryption; it reads the header, but stops there.

Alternatively, you can specify the salt value with the -S flag, or deactivate the salt altogether with -nosalt. Unsalted encryption is not recommended at all because it may allow speeding up password cracking with precomputed tables (the same password always yields the same key and IV). If you provide the salt value, then you become responsible for generating proper salts, i.e. trying to make them as unique as possible (in practice, you have to produce them randomly). It is preferable to let openssl handle that, since there is ample room for silent failures ("silent" meaning "weak and crackable, but the code still works so you do not detect the problem during your tests").

The encryption format used by OpenSSL is non-standard: it is "what OpenSSL does", and if all versions of OpenSSL tend to agree with each other, there is still no reference document which describes this format except OpenSSL source code. The header format is rather simple:

magic value (8 bytes): the bytes 53 61 6c 74 65 64 5f 5f
salt value (8 bytes)
Hence a fixed 16-byte header, beginning with the ASCII encoding of the string "Salted__", followed by the salt itself. That's all ! No indication of the encryption algorithm; you are supposed to keep track of that yourself.

The process by which the password and salt are turned into the key and IV is not documented, but a look at the source code shows that it calls the OpenSSL-specific EVP_BytesToKey() function, which uses a custom key derivation function with some repeated hashing. This is a non-standard and not-well vetted construct (!) which relies on the MD5 hash function of dubious reputation (!!); that function can be changed on the command-line with the undocumented -md flag (!!!); the "iteration count" is set by the enc command to 1 and cannot be changed (!!!!). This means that the first 16 bytes of the key will be equal to MD5(password||salt), and that's it.

This is quite weak ! Anybody who knows how to write code on a PC can try to crack such a scheme and will be able to "try" several dozens of millions of potential passwords per second (hundreds of millions will be achievable with a GPU). If you use "openssl enc", make sure your password has very high entropy ! (i.e. higher than usually recommended; aim for 80 bits, at least). Or, preferably, don't use it at all; instead, go for something more robust (GnuPG, when doing symmetric encryption for a password, uses a stronger KDF with many iterations of the underlying hash function).

  // ================================================================
  // init()
  // ================================================================
  void Cipher::init(const string& pass)
  {
    DBG_FCT("init");
    // Use a default passphrase if the user didn't specify one.
    m_pass = pass;
    if (m_pass.empty() ) {
      // Default: ' deFau1t pASsw0rD'
      // Obfuscate so that a simple strings will not find it.
      char a[] = {' ','d','e','F','a','u','1','t',' ',
        'p','A','S','s','w','0','r','D',0};
      m_pass = a;
    }
    
    // Create the key and IV values from the passkey.
    bzero(m_key,sizeof(m_key));
    bzero(m_iv,sizeof(m_iv));
    OpenSSL_add_all_algorithms();
    const EVP_CIPHER* cipher = EVP_get_cipherbyname(m_cipher.c_str());
    const EVP_MD*     digest = EVP_get_digestbyname(m_digest.c_str());
    if (!cipher) {
      string msg = "init(): cipher does not exist "+m_cipher;
      throw runtime_error(msg);
    }
    if (!digest) {
      string msg = "init(): digest does not exist "+m_digest;
      throw runtime_error(msg);
    }
    
    int ks = EVP_BytesToKey(cipher,    // cipher type
                            digest,    // message digest
                            m_salt,    // 8 bytes
                            (uchar*)m_pass.c_str(), // pass value
                            (int)m_pass.length(),
                            m_count,   // number of rounds
                            m_key,
                            m_iv);
    if (ks!=32) {
      throw runtime_error("init() failed: "
                          "EVP_BytesToKey did not return a 32 byte key");
    }
    
    DBG_PKV(m_pass);
    DBG_PKV(m_cipher);
    DBG_PKV(m_digest);
    DBG_TDUMP(m_salt);
    DBG_TDUMP(m_key);
    DBG_TDUMP(m_iv);
    DBG_PKV(m_count);
  }

