/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_RSA_CRYPTO_H__
#define __EJIN_RSA_CRYPTO_H__

#include "AESCrypto.h"

#define PUBLIC_KEY_FILENAME  "public.pem"
#define PRIVATE_KEY_FILENAME "private.key"

#define RSA_KEY_LENGTH   3072
//#define RSA_KEY_LENGTH   2048

// forward declaration
class RSACryptoTest;
namespace ejin
{
  
  class RSACrypto: public AESCrypto
  {
    friend class ::RSACryptoTest;
    
  public:
    
    RSACrypto( void );
    RSACrypto( const RSACrypto& crypto ); // copy
    RSACrypto& operator=( const RSACrypto& crypto ); // assignment
    bool operator==( const RSACrypto& rhs ) const;
    ~RSACrypto();
  
    static void initSeed( const void *buf, int num );
    static RSACrypto* generateRSAKeyPair( const char* folder = NULL, const char* password = NULL );
    static bool clearRSAKeyPair( const char* folder );

    static RSACrypto* loadRSAKeyPair( const char* folder, const char* password = NULL );
    static RSACrypto* fromPublicKeyASN1( const string& key );
    static RSACrypto* fromPrivateKeyASN1( const string& key );
    static RSACrypto* fromPublicKeyPEM( const string& keyPEM, const char* password = NULL );
    static RSACrypto* fromPrivateKeyPEM( const string& keyPEM, const char* password = NULL );
    static string getFingerPrint( const string& key );
    static string getFingerPrint( const char public_key[], size_t public_key_len );

    bool saveKeys( const char* folder, const char* password = NULL ) const;
    bool hasPublicKeyASN1( void ) const;
    string getPublicKeyASN1( void ) const;
    string getPublicKeyFingerPrint( void ) const;
    bool hasPrivateKeyASN1( void ) const;
    string getPrivateKeyASN1( void ) const;
    bool hasPublicKeyPEM( void ) const;
    string getPublicKeyPEM( void ) const;
    bool hasPrivateKeyPEM( void ) const;
    string getPrivateKeyPEM( const char* password = NULL ) const;

    const string getEncryptedSessionKey( ) const throw(security_integrity_error);
    const string setEncryptedSessionKey( const string& key )throw(security_integrity_error);

    string signMessage( const string& message ) const;
    bool verifyMessage( const string& message, const string& signatur ) const;
    
  private:

    RSACrypto( EVP_PKEY* pkey, bool hasPrivateKey = false );
    RSACrypto( RSA* rsa, bool hasPrivateKey = false );
    
    int passwordCallback(char *buf, int size, int rwflag, void *u);

    bool signCipher( const Cipher& ciphertext, RSA *rsaKey, Cipher& result ) const;
    bool verifyCipher( const Cipher& plaintext, RSA *rsaKey, const Cipher& signatur ) const;
    
    static bool encryptRSACipher( const Cipher& plaintext, RSA* rsaKey, Cipher& result );
    bool decryptRSACipher( const Cipher& ciphertext, RSA* rsaKey, Cipher& result ) const;
    
    static bool exportPEM_to_folder( EVP_PKEY* rsa, bool hasPrivateKey, const char* folder, const char* password );
    static EVP_PKEY* importPEM_from_folder( EVP_PKEY** rsa, bool hasPrivateKey, const char* folder, const char* password );
    static bool erasePEM( const char* folder );
    static RSA* loadKeyASN1( RSA** rsa, const string& key, bool privateKey );
    static EVP_PKEY* importKey( EVP_PKEY** rsa, const string& key, bool privateKey, const char* password = NULL );

    static EVP_PKEY* readPEM( EVP_PKEY** rsa, bool privateKey, const char* filename, const char* password );
    static int writePEM( EVP_PKEY*  rsa, bool privateKey, const char* filename, const char* password );

    static string getFingerPrint( const Cipher& cipher );
    
    EVP_CIPHER_CTX* _rsaCtx;
    EVP_PKEY*       _pkey;
    bool            _hasPrivateKey;
  };
}

#endif // __EJIN_RSA_CRYPTO_H__
