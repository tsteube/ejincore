/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __ENVIRONMENT_H__
#define __ENVIRONMENT_H__

#define MAIN_DATABASE_NAME                   "main"
#define BACKUP_DATABASE_NAME                 "mainBak"
#define ARCHIVE_DATABASE_NAME                "mainArv"
#define ATTACHMENT_DATABASE_NAME             "attach"
#define ATTACHMENT_ARCHIVE_DATABASE_NAME     "attachArv"

#include <inttypes.h>

// see http://developer.apple.com/library/mac/#documentation/Darwin/Conceptual/64bitPorting/MakingCode64-BitClean/MakingCode64-BitClean.html#//apple_ref/doc/uid/TP40001064-CH226-SW2
#if defined(__i386__) || defined(__ppc__) || defined(__arm__)
// 32-bit PowerPC code
//typedef int    integer;
//typedef double  real;
typedef int32_t    integer;
typedef double     real;
#else
#if defined(__x86_64__) || defined(__ppc64__) || defined(__arm64__)
// 64-bit PowerPC code
//typedef long       integer;
//typedef /*long*/ double real;
typedef int64_t         integer;
typedef /*long*/ double real;
#else
#error UNKNOWN ARCHITECTURE
#endif
#endif

#endif
