/* Copyright (C) 2012 - 2014 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EJIN_CORE_H__
#define __EJIN_CORE_H__

#include <ejinCore/Declarations.h>
#include <ejinCore/ChannelService.h>
#include <ejinCore/MediaService.h>
#include <ejinCore/UserService.h>
#include <ejinCore/MemberRepository.h>
#include <ejinCore/PictureRepository.h>
#include <ejinCore/ChatService.h>
#include <ejinCore/ChannelMemberRepository.h>
#include <ejinCore/ChannelMediaRepository.h>
#include <ejinCore/ApprovedMemberRepository.h>
#include <ejinCore/Utilities.h>
#include <ejinCore/ResultList.h>
#include <ejinCore/RSACrypto.h>
#include <ejinCore/User.h>
#include <ejinCore/SyncSource.h>
#include <ejinCore/Channel.h>
#include <ejinCore/ChannelHeader.h>
#include <ejinCore/ChannelPost.h>
#include <ejinCore/ChannelComment.h>
#include <ejinCore/Media.h>
#include <ejinCore/Attachment.h>
#include <ejinCore/Chat.h>
#include <ejinCore/Product.h>
#include <ejinCore/ProductService.h>
#include <ejinCore/Message.h>
#include <ejinCore/MessageList.h>
#include <ejinCore/ChangeLog.h>
#include <ejinCore/ChangeLogRepository.h>
#include <ejinCore/Modifications.h>
#include <ejinCore/SerializableEntity.h>
#include <ejinCore/JsonMarshaller.h>
#include <ejinCore/xmlwriter.h>
#include <ejinCore/StringTemplate.h>

#endif
