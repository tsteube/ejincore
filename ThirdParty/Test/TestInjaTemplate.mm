//
//  TestInjaTemplate.m
//  TestInjaTemplate
//
//  Created by Thorsten on 29.01.20.
//  Copyright © 2020 Thorsten Steube. All rights reserved.
//
#import <XCTest/XCTest.h>

#import "inja.hpp"

#import <iostream>
#import <fstream>
#import <iomanip>
#import <string>

using namespace inja;
using namespace std;
using json = nlohmann::json;

#define NSSTRING_TO_STRING(variable) [variable cStringUsingEncoding:NSUTF8StringEncoding]

@interface TestInjaTemplate : XCTestCase

@end

@implementation TestInjaTemplate
{
  NSString* filePath;
  NSString* outfilePath;
}

- (void)setUp {
  filePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"test.tpl"];
  outfilePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"test.txt"];
}
- (void)tearDown
{
  NSFileManager *manager = [NSFileManager defaultManager];
  [manager removeItemAtPath:outfilePath error:nil];
}

- (void)testTemplate
{
  json data;
  data["name"] = "world";

  render("Hello {{ name }}!", data);
  //render_to(cout, "Hello {{ name }}!", data); // Prints "Hello world!"
  
  string result = render("Hello {{ name }}!", data);
  XCTAssert( result == string("Hello world!") );
}

- (void)testTemplateEnvironment
{
  Environment env;

  json data;
  data["name"] = "world";

  string result = env.render("Hello {{ name }}!", data);
  XCTAssert( result == string("Hello world!") );
}

- (void)testTemplateFile
{
  Environment env;

  json data;
  data["name"] = "world";

  Template temp = env.parse_template(NSSTRING_TO_STRING(filePath));
  string result = env.render(temp, data);
  XCTAssert( result == string("Hello world!\n") );
  
  // Or write a rendered template file
  env.write(temp, data, NSSTRING_TO_STRING(outfilePath));
}

- (void)testRenderFile
{
  Environment env;

  json data;
  data["name"] = "world";

  string result = env.render_file(NSSTRING_TO_STRING(filePath), data);
  XCTAssert( result == string("Hello world!\n") );
}

@end
