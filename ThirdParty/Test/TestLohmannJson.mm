//
//  TestLohmannJson.m
//  TestLohmannJson
//
//  Created by Thorsten on 29.01.20.
//  Copyright © 2020 Thorsten Steube. All rights reserved.
//
#import <XCTest/XCTest.h>

#import "json.hpp"

#import <iostream>
#import <fstream>
#import <iomanip>
#import <string>

using namespace nlohmann;
using namespace std;

#define NSSTRING_TO_STRING(variable) [variable cStringUsingEncoding:NSUTF8StringEncoding]

@interface TestLohmannJson : XCTestCase

@end

@implementation TestLohmannJson
{
  NSString* filePath;
  NSString* outfilePath;
}

- (void)setUp {
  filePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"test.json"];
  outfilePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"pretty.json"];
}
- (void)tearDown
{
  NSFileManager *manager = [NSFileManager defaultManager];
  [manager removeItemAtPath:outfilePath error:nil];
}

- (void)testCreateFile1
{
  json j;

  // add a number that is stored as double (note the implicit conversion of j to an object)
  j["pi"] = 3.141;
  // add a Boolean that is stored as bool
  j["happy"] = true;
  // add a string that is stored as string
  j["name"] = "Niels";
  // add another null object by passing nullptr
  j["nothing"] = nullptr;
  // add an object inside the object
  j["answer"]["everything"] = 42;
  // add an array that is stored as vector (using an initializer list)
  j["list"] = { 1, 0, 2 };
  // add another object (using an initializer list of pairs)
  j["object"] = { {"currency", "USD"}, {"value", 42.99} };
  //j["object"] = json::array({ {"currency", "USD"}, {"value", 42.99} });

  //cout << setw(2) << j3 << endl;
  
  // write prettified JSON to the same file
  ofstream o(NSSTRING_TO_STRING(outfilePath));
  o << setw(4) << j << endl;
}

- (void)testCreateFile2
{
  json j = {
    {"pi", 3.141},
    {"happy", true},
    {"name", "Niels"},
    {"nothing", nullptr},
    {"answer", {
      {"everything", 42}
    }},
    {"list", {1, 0, 2}},
    {"object", {
      {"currency", "USD"},
      {"value", 42.99}
    }}
  };

  //cout << setw(2) << j3 << endl;
  
  // write prettified JSON to the same file
  ofstream o(NSSTRING_TO_STRING(outfilePath));
  o << setw(4) << j << endl;
}

- (void)testCreateFile3
{
  auto j = R"(
    {
      "happy": true,
      "pi": 3.141
    }
  )"_json;

  //cout << setw(2) << j3 << endl;
  
  // write prettified JSON to the same file
  ofstream o(NSSTRING_TO_STRING(outfilePath));
  o << setw(4) << j << endl;
}

- (void)testReadFile
{
  ifstream i(NSSTRING_TO_STRING(filePath));
  json j;
  i >> j;

  // write prettified JSON to the same file
  ofstream o(NSSTRING_TO_STRING(outfilePath));
  o << setw(4) << j << endl;
}

- (void)testExample2
{
  // store a string in a JSON value
  json j_string = "this is a string";

  // retrieve the string value
  auto cpp_string = j_string.get<string>();
  // retrieve the string value (alternative when an variable already exists)
  string cpp_string2;
  j_string.get_to(cpp_string2);

  // retrieve the serialized value (explicit JSON serialization)
  string serialized_string = j_string.dump();

  // output of original string
  XCTAssert( cpp_string == string("this is a string") );
  XCTAssert( cpp_string2 == string("this is a string") );
  XCTAssert( j_string.get<string>() == string("this is a string") );
  // output of serialized value
  XCTAssert( serialized_string == string("\"this is a string\"") );
}

@end
