//
//  TestSqlcipher.m
//  TestSqlcipher
//
//  Created by Thorsten on 29.01.20.
//  Copyright © 2020 Thorsten Steube. All rights reserved.
//
#import <XCTest/XCTest.h>

#import "sqlite3.h"

using namespace std;

#define NSSTRING_TO_STRING(variable) [variable cStringUsingEncoding:NSUTF8StringEncoding]

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   /*
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   */
   return 0;
}

@interface TestSqlcipher : XCTestCase

@end

@implementation TestSqlcipher
{
  NSString* dbPath;
}

- (void)setUp {
  dbPath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"test.db"];
}

- (void)tearDown
{
  NSFileManager *manager = [NSFileManager defaultManager];
  [manager removeItemAtPath:dbPath error:nil];
}

// https://www.tutorialspoint.com/sqlite/sqlite_c_cpp.htm
- (void)testEncryptedDatabase
{
  sqlite3 *db;
  char *zErrMsg = 0;
  int rc;
  const char *sql;
  const char* data = "Callback function called";

  // create DB
  rc = sqlite3_open(NSSTRING_TO_STRING(dbPath), &db);
  XCTAssert( rc == SQLITE_OK );

  // encrypt DB
  const char* key = "secret";
  rc = sqlite3_key(db, key, (int)strlen(key));
  XCTAssert( rc == SQLITE_OK );

  // verify that the encrypted database can be read
  sql = "SELECT count(*) FROM sqlite_master";
  rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
  XCTAssert( rc == SQLITE_OK );

  // create relation
  sql = "CREATE TABLE COMPANY("  \
     "ID INT PRIMARY KEY     NOT NULL," \
     "NAME           TEXT    NOT NULL," \
     "AGE            INT     NOT NULL," \
     "ADDRESS        CHAR(50)," \
     "SALARY         REAL );";
  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
  XCTAssert( rc == SQLITE_OK );

  // insert rows
  sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
         "VALUES (1, 'Paul', 32, 'California', 20000.00 ); " \
         "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
         "VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); "     \
         "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
         "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );" \
         "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
         "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";
  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
  XCTAssert( rc == SQLITE_OK );

  // select rows
  sql = "SELECT * from COMPANY";
  rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
  XCTAssert( rc == SQLITE_OK );

  // update row
  sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1; " \
        "SELECT * from COMPANY";
  rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
  XCTAssert( rc == SQLITE_OK );

  // delete row
  sql = "DELETE from COMPANY where ID=2; " \
        "SELECT * from COMPANY";
  rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
  XCTAssert( rc == SQLITE_OK );

  // close db
  sqlite3_close(db);
}
@end
