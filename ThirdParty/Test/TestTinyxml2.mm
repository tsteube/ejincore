//
//  TestTinyxml2.m
//  TestTinyxml2
//
//  Created by Thorsten on 29.01.20.
//  Copyright © 2020 Thorsten Steube. All rights reserved.
//
#import <XCTest/XCTest.h>

#import "tinyxml2.h"
#import <string>

using namespace tinyxml2;
using namespace std;

#define NSSTRING_TO_STRING(variable) [variable cStringUsingEncoding:NSUTF8StringEncoding]

@interface TestTinyxml2 : XCTestCase

@end

@implementation TestTinyxml2
{
  NSString* filePath;
}

- (void)setUp {
  filePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"test.xml"];
}

- (void)testExample1
{
  XMLDocument doc;
  doc.LoadFile( NSSTRING_TO_STRING(filePath) );
  XCTAssert( doc.ErrorID() == XML_SUCCESS );
}
- (void)testExample2
{
  static const char* xml = "<element/>";
  XMLDocument doc;
  doc.Parse( xml );
  XCTAssert( doc.ErrorID() == XML_SUCCESS );
}
- (void)testExample3
{
  static const char* xml =
    "<?xml version=\"1.0\"?>"
    "<!DOCTYPE PLAY SYSTEM \"play.dtd\">"
    "<PLAY>"
    "<TITLE>A Midsummer Night's Dream</TITLE>"
    "</PLAY>";

  XMLDocument doc;
  doc.Parse( xml );

  XMLElement* titleElement = doc.FirstChildElement( "PLAY" )->FirstChildElement( "TITLE" );
  const char* title = titleElement->GetText();
  XCTAssert( string(title) == string("A Midsummer Night's Dream") );

  XMLText* textNode = titleElement->FirstChild()->ToText();
  title = textNode->Value();
  XCTAssert( string(title) == string("A Midsummer Night's Dream") );

  XCTAssert( doc.ErrorID() == XML_SUCCESS );
}
- (void)testExample4
{
  static const char* xml =
    "<information>"
    "  <attributeApproach v='2' />"
    "  <textApproach>"
    "    <v>2</v>"
    "  </textApproach>"
    "</information>";

  XMLDocument doc;
  doc.Parse( xml );

  int v0 = 0;
  int v1 = 0;

  XMLElement* attributeApproachElement = doc.FirstChildElement()->FirstChildElement( "attributeApproach" );
  attributeApproachElement->QueryIntAttribute( "v", &v0 );

  XMLElement* textApproachElement = doc.FirstChildElement()->FirstChildElement( "textApproach" );
  textApproachElement->FirstChildElement( "v" )->QueryIntText( &v1 );

  XCTAssert( v0 == v1 );
  XCTAssert( doc.ErrorID() == XML_SUCCESS );
}

@end
