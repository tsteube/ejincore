//
//  EjinThirdParty.h
//  EjinThirdParty
//
//  Created by Thorsten on 29.01.20.
//  Copyright © 2020 Thorsten Steube. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for EjinThirdParty.
FOUNDATION_EXPORT double EjinThirdPartyVersionNumber;

//! Project version string for EjinThirdParty.
FOUNDATION_EXPORT const unsigned char EjinThirdPartyVersionString[];

// In this header, you should import all the public headers of your framework using statements like
#import <ThirdParty/sqlite3.h>
#import <ThirdParty/civetweb.h>
#ifdef __cplusplus
#  import <ThirdParty/tinyxml2.h>
#  import <ThirdParty/json.hpp>
#  import <ThirdParty/inja.hpp>
#endif
