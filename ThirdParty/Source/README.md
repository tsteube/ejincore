
# [Tinyxml2](https://github.com/leethomason/tinyxml2)

TinyXML-2 is a simple, small, efficient, C++ XML parser that can be easily integrated into other programs.

# [Json Parser](https://github.com/nlohmann/json)

Json Parser by nlohmann

# [Inja](https://github.com/pantor/inja)

Inja is a template engine for modern C++, loosely inspired by jinja for python.
It has an easy and yet powerful template syntax with all variables, loops, conditions, includes, callbacks, comments you need,
nested and combined as you like. Inja uses the wonderful json library by nlohmann for data input and handling.

## Note:

Replace include statements in source files from
> #include <nlohmann/json.hpp>
to
> #include "json.hpp"

# [sqlcipher](https://www.zetetic.net/sqlcipher/)

SQLCipher is widely used, protecting data for thousands of apps on hundreds of millions of devices

See [instructions for source integration](https://www.zetetic.net/sqlcipher/ios-tutorial/). 
> git clone https://github.com/sqlcipher/sqlcipher.git
> cd sqlcipher
> ./configure --with-crypto-lib=none
> make sqlite3.c

Remember to set the other C Flags in the compiler settings.
> -DSQLITE_HAS_CODEC -DSQLITE_TEMP_STORE=3 -DSQLCIPHER_CRYPTO_CC -DNDEBUG

# [CivetWeb](https://github.com/civetweb/civetweb)

Project mission is to provide easy to use, powerful, C (C/C++) embeddable web server with optional CGI, SSL and Lua support. CivetWeb has a MIT license so you can innovate without restrictions.

# [OpenSSL](https://github.com/openssl/openssl)

See [OpenSSL-for-iPhone](https://github.com/x2on/OpenSSL-for-iPhone.git) github repository to cross compile openssl for ios.
This is a script for using self-compiled builds of the OpenSSL-library on the iPhone. You can build apps with Xcode and the official SDK from Apple with this. I also made a small example-app for using the libraries with Xcode and the iPhone/iPhone-Simulator.

$ ./build-libssl.sh --targets="ios-sim-cross-x86_64 ios64-cross-arm64 ios64-cross-arm64e mac-catalyst-x86_64"
$ ./create-openssl-framework.sh

# [qrencode](https://fukuchi.org/works/qrencode/index.html.en)

Libqrencode is a fast and compact library for encoding data in a QR Code symbol, a 2D symbology that can be scanned by handy terminals such as a mobile phone with CCD. The capacity of QR Code is up to 7000 digits or 4000 characters and has high robustness.


https://www.markdownguide.org/basic-syntax
