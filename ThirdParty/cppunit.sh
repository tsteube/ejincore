#!/bin/sh
#
# These are implemented as functions, and just called by the
# short MAIN section below
## Determine the appropriate expat source path to use
## Introduced by michaeltyson, 
## Adapted by Thorsten Steube

#set
#set -x

##
## environment variables
##
LIBRARY_NAME=cppunit

# read from environment
#PLATFORM_DEVELOPER_USR_DIR=/Developer/Platforms/iPhoneOS.platform/Developer/usr
#SRCROOT=/Users/tsteube/Development/ejin-alashan/libs
#PROJECT_TEMP_DIR=/Users/tsteube/Development/tmp/XcodeBuild/expat.build
#TARGET_BUILD_DIR=/Users/tsteube/Development/tmp/XcodeBuild/Debug
#SDKROOT=/Developer/SDKs/MacOSX10.8.sdk
#SDKROOT=/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS4.3.sdk/
#ARCHS="armv7 i386 x86_64"

# figure out the right set of build architectures for this run
BUILDARCHS=${ARCHS:=$ARCHS_UNIVERSAL_IPHONE_OS}
BUILDARCHS="${ARCHS}"
case "$SDKROOT" in
  *iPhoneSimulator*)
    echo "***** no unit test on simulator *****"
    exit 1
  ;;
  *iPhoneOS*)
    echo "***** no unit test on device *****"
    exit 1
  ;;
  *MacOSX*)
    BUILDARCHS="x86_64"
  ;;
  *)
    echo "***** unknown SDK $SDKROOT *****"
    exit 1
  ;;
esac

# - - - - - - - - - - - - - - -
buildAction () {
  
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${LIBRARY_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  if [ -d "${FRAMEWORK_LOCN}" ]; then
    echo "${LIBRARY_NAME} Framework in ${TARGET_BUILD_DIR} has already been build"
    return;
  fi
  echo "Building ${LIBRARY_NAME} ..."

	# locate src archive file if present
	SRC_ARCHIVE=( ${SRCROOT}/*${LIBRARY_NAME}*.tar.gz )
	if [ "${SRC_ARCHIVE}" == "" ] 
	then
 		echo "***** \"${LIBRARY_NAME}\" source package not found" >&2
 		exit 1;
	fi
  
	TARGET_SRC="${PROJECT_DERIVED_FILE_DIR}"
  if [ ! -d "${TARGET_SRC}" ] ||  [ -z "$(ls ${TARGET_SRC}/)" ]; then
    echo "***** extracting $SRC_ARCHIVE to $TARGET_SRC ..."
	  mkdir -p "$TARGET_SRC"
	  tar -C "$TARGET_SRC" --strip-components=1 -zxf "$SRC_ARCHIVE" || exit 1
  fi

	for BUILDARCH in $BUILDARCHS
 	do
    LIBRARY_DESCRIPTION="${CONFIGURATION} ${LIBRARY_NAME} ${BUILDARCH} Library for ${SDKROOT}"
  	echo "Building ${LIBRARY_DESCRIPTION} ..."

 		cd "$TARGET_SRC"
    make distclean clean

    case "$SDKROOT" in
      *MacOSX*)
        CONFIGUREFLAGS="--enable-shared --disable-static"
		    export CC='gcc -arch x86_64'
		    export CXX='g++ -arch x86_64'
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -no-cpp-precomp -D_DARWIN_C_SOURCE"
        ;;
			*)
  			echo "***** unknown SDK $SDKROOT *****"
   			exit 1
				;;
    esac

    export LDFLAGS="${COMMONFLAGS} -O2"
    export CFLAGS="${COMMONFLAGS}"
    export CPPFLAGS="${COMMONFLAGS}"
    export CXXFLAGS="${COMMONFLAGS}"

    #############################
    # run configure + make
    #
    # TODO: I can't find the reason why I have to clear the environment to configure and make the package ? !!!!
    #
    /usr/bin/env -i ./configure ${CONFIGUREFLAGS} # Note the configure will fail at the end; just ignore it
    /usr/bin/env -i make
#    ./configure ${CONFIGUREFLAGS} && make
    if [ "$?" == "0" ]; then

      [ -d "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" ] || mkdir -p "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
		  rm -rf "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}"*
		  if [ -f ./src/cppunit/.libs/lib${LIBRARY_NAME}.a ]; then
		    cp -RPLf ./src/cppunit/.libs/lib${LIBRARY_NAME}.a "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" 
	      TARGET_STATIC_LIBS="$TARGET_STATIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.a"
		  fi
		  if [ -f ./src/cppunit/.libs/lib${LIBRARY_NAME}.dylib ]; then
		    cp -RPf ./src/cppunit/.libs/lib${LIBRARY_NAME}*.dylib "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
	      TARGET_DYNAMIC_LIBS="$TARGET_DYNAMIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.dylib"
      fi

	  	echo "Build ${LIBRARY_DESCRIPTION} in ${TARGET_BUILD_DIR}/${BUILDARCH}/lib."
		else 
		  echo "***** Failed to build ${LIBRARY_DESCRIPTION}  *****" 
		  exit 1 
		fi 
	done

  # Create the path to the real Headers dir
  /bin/rm -rf "${FRAMEWORK_LOCN}"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Resources/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Documentation/"
  # Create the required symlinks
  /bin/ln -sfh A "${FRAMEWORK_LOCN}/Versions/Current"
  /bin/ln -sfh Versions/Current/Headers "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/${LIBRARY_NAME}" "${FRAMEWORK_LOCN}/${LIBRARY_NAME}"
  # Copy the public headers into the framework
  /bin/cp -r "$TARGET_SRC/include/${LIBRARY_NAME}/" "${FRAMEWORK_LOCN}/Versions/A/Headers/"

	# build universal library
  if [[ "$SDKROOT" =~ "MacOSX" ]] && [ -n "$TARGET_DYNAMIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_DYNAMIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
    /usr/bin/install_name_tool -id "@rpath/${LIBRARY_NAME}.framework/Versions/A/${LIBRARY_NAME}" \
       "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
	elif [ -n "$TARGET_STATIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_STATIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
  fi

  /bin/ln -sfh "Versions/Current/Headers" "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/Resources" "${FRAMEWORK_LOCN}/Resources"
  /bin/ln -sfh "Versions/Current/Documentation" "${FRAMEWORK_LOCN}/Documentation"
  [ -d `dirname ${DEBUG_FRAMEWORK_LOCN}` ] || /bin/mkdir `dirname ${DEBUG_FRAMEWORK_LOCN}`
  /bin/rm -rf ${DEBUG_FRAMEWORK_LOCN}
  /bin/ln -sfh ${FRAMEWORK_LOCN} ${DEBUG_FRAMEWORK_LOCN}

	case "$SDKROOT" in
	  *iPhoneSimulator*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
  	<array>
	    <string>iPhoneOS</string>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *iPhoneOS*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>iPhoneOS</string>
	  </array>"
	    ;;
	  *MacOSX*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *)
	    ;;
	esac

	cat > ${FRAMEWORK_LOCN}/Resources/Info.plist <<EOF
		<?xml version="1.0" encoding="UTF-8"?>
		<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
		<plist version="1.0">
		<dict>
		  <key>CFBundleDevelopmentRegion</key>
		  <string>English</string>
		  <key>CFBundleIdentifier</key>
		  <string>cppunit</string>
			<key>CFBundleExecutable</key>
			<string>cppunit</string>	
		  <key>CFBundleInfoDictionaryVersion</key>
		  <string>6.0</string>
		  <key>CFBundlePackageType</key>
		  <string>FMWK</string>
		  <key>CFBundleSignature</key>
		  <string>????</string>
		  <key>CFBundleVersion</key>
		  <string>${VERSION}</string>
				$PLATFORM_KEY
			</dict>
			</plist>
EOF
  
  echo "Create ${LIBRARY_NAME}.framework for ${BUILDARCHS} in ${FRAMEWORK_LOCN}."
  if [ "x$SIGNATURE" != "x" ] ; then
		/usr/bin/codesign -i "${LIBRARY_NAME}" --force --deep --sign "${SIGNATURE}" --preserve-metadata=identifier,entitlements --timestamp=none "${FRAMEWORK_LOCN}/Versions/A" && echo "signed with ${SIGNATURE}"
	else 
  	# sign binary with empty signatur
    /usr/bin/codesign --force --sign - "${FRAMEWORK_LOCN}/Versions/A"
  fi 

	exit 0
}

# - - - - - - - - - - - - - - -
cleanAction () {	
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${LIBRARY_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  echo "Cleaning ${FRAMEWORK_LOCN} ..."
  rm -rf "${FRAMEWORK_LOCN}"
  rm -f  "${DEBUG_FRAMEWORK_LOCN}"
	echo "Cleaning ${LIBRARY_NAME} sources ..."
  rm -rf "${PROJECT_DERIVED_FILE_DIR}" 
  exit 0;
}

###############################
# MAIN

case $ACTION in
  # NOTE: for some reason, it gets set to "" rather than "build" when
  # doing a build.
  "")
    buildAction
    ;;

  "clean")
    cleanAction
    ;;
esac

exit 0
