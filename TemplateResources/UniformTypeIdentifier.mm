/*
 * Copyright (c) 2015 Thorsten Steube
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the project's author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#import "UniformTypeIdentifier.h"

//#import "EJCommon.h"

#ifdef __UTTYPE__
#endif

#if (TARGET_OS_IPHONE)
#import <MobileCoreServices/MobileCoreServices.h>
#else
#import <CoreServices/CoreServices.h>
#endif

static NSArray* AllSupportedTypes = nil;

// Defaults
NSString *const DefaultMimeType        = @"application/octet-stream";
NSString *const DefaultUTIType         = @"public.data";
NSString *const DefaultFileExtension   = @"dat";

// https://developer.apple.com/library/archive/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html#//apple_ref/doc/uid/TP40009259
// https://pki-tutorial.readthedocs.io/en/latest/mime.html

@interface UniformTypeIdentifier()
@property (nonatomic,strong)  NSString*    mimeType;
@property (nonatomic,strong)  NSString*    fileExtension;
@end

@implementation UniformTypeIdentifier
{
  NSString* _UTI;
}
+ (UniformTypeIdentifier *)GenericUTI {
  static UniformTypeIdentifier* kGenericMimeType;
  if (kGenericMimeType == nil) {
    kGenericMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeData];
  }
  return kGenericMimeType;
}
+ (UniformTypeIdentifier *)WindowsBitmapFormatUTI {
  static UniformTypeIdentifier* kWindowsBitmapFormatMimeType;
  if (kWindowsBitmapFormatMimeType == nil) {
    kWindowsBitmapFormatMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeBMP];
  }
  return kWindowsBitmapFormatMimeType;
}
+ (UniformTypeIdentifier *)TaggedImageFileFormatUTI {
  static UniformTypeIdentifier* kTaggedImageFileFormatMimeType;
  if (kTaggedImageFileFormatMimeType == nil) {
    kTaggedImageFileFormatMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeTIFF];
  }
  return kTaggedImageFileFormatMimeType;
}
+ (UniformTypeIdentifier *)JointPhotographicExpertsGroupUTI {
  static UniformTypeIdentifier* kJointPhotographicExpertsGroupMimeType;
  if (kJointPhotographicExpertsGroupMimeType == nil) {
    kJointPhotographicExpertsGroupMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeJPEG];
  }
  return kJointPhotographicExpertsGroupMimeType;
}
+ (UniformTypeIdentifier *)GraphicInterchangeFormatUTI {
  static UniformTypeIdentifier* kGraphicInterchangeFormatMimeType;
  if (kGraphicInterchangeFormatMimeType == nil) {
    kGraphicInterchangeFormatMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeGIF];
  }
  return kGraphicInterchangeFormatMimeType;
}
+ (UniformTypeIdentifier *)PortableNetworkGraphicUTI {
  static UniformTypeIdentifier* kPortableNetworkGraphicMimeType;
  if (kPortableNetworkGraphicMimeType == nil) {
    kPortableNetworkGraphicMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypePNG];
  }
  return kPortableNetworkGraphicMimeType;
}
+ (UniformTypeIdentifier *)PortableDocumentFormatUTI {
  static UniformTypeIdentifier* kPDFMimeType;
  if (kPDFMimeType == nil) {
    kPDFMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypePDF];
  }
  return kPDFMimeType;
}
+ (UniformTypeIdentifier *)ZipArchiveUTI {
  static UniformTypeIdentifier* kZipMimeType;
  if (kZipMimeType == nil) {
    kZipMimeType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeZipArchive];
  }
  return kZipMimeType;
}
+ (UniformTypeIdentifier *)CertificateUTI {
  static UniformTypeIdentifier* kPEMType;
  if (kPEMType == nil) {
    kPEMType = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypeX509Certificate];
  }
  return kPEMType;
}
+ (UniformTypeIdentifier *)PKCS12UTI {
  static UniformTypeIdentifier* kPKCS12Type;
  if (kPKCS12Type == nil) {
    kPKCS12Type = [UniformTypeIdentifier UniformTypeIdentifierWithUTType:kUTTypePKCS12];
  }
  return kPKCS12Type;
}
+ (UniformTypeIdentifier *)PemUTI {
  static UniformTypeIdentifier* kPEMType;
  if (kPEMType == nil) {
    kPEMType = [[UniformTypeIdentifier alloc] initWithUTType:@"com.rsa.pem"];
  }
  return kPEMType;
}
+ (UniformTypeIdentifier *)EjinArchiveUTI {
  static UniformTypeIdentifier* kEjinAType;
  if (kEjinAType == nil) {
    kEjinAType = [UniformTypeIdentifier UniformTypeIdentifierWithFileExtension:@"ejina"];
  }
  return kEjinAType;
}
+ (NSString *)ExtensionOfMimeType:(NSString *)mimeType {
  UniformTypeIdentifier* uti = [UniformTypeIdentifier UniformTypeIdentifierWithMimeType:mimeType];
  return uti.ext ? uti.ext : DefaultFileExtension;
}

+ (UniformTypeIdentifier *)UniformTypeIdentifierWithMimeType
{
  return [[UniformTypeIdentifier alloc] initWithMimeType:DefaultMimeType];
}
+ (instancetype)UniformTypeIdentifierWithUTType:(CFStringRef)type;
{
  return [[UniformTypeIdentifier alloc] initWithUTType:(__bridge NSString *)type];
}
+ (UniformTypeIdentifier *)UniformTypeIdentifierWithMimeType:(NSString *)mimeType
{
  return [[UniformTypeIdentifier alloc] initWithMimeType:mimeType];
}
+ (UniformTypeIdentifier *)UniformTypeIdentifierWithFileExtension:(NSString *)extension
{
  return [[UniformTypeIdentifier alloc] initWithFileExtension:extension];
}
- (id) initWithUTType:(NSString *)type {
  if (! type || ! UTTypeIsDeclared( (__bridge CFStringRef) type ))
    return nil;

  self = [super init];
  if (self != nil) {
    _UTI = type;
  }
  return self;
}
- (id) initWithFileExtension:(NSString *)extension {
  NSString* uti = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)extension, NULL);
  return [self initWithUTType: uti];
}
- (id) initWithMimeType:(NSString *)mimeType {
  NSString* uti = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, (__bridge CFStringRef)mimeType, NULL);
  return [self initWithUTType: uti];
}
- (NSString *)UTType
{
  return _UTI;
}
- (BOOL)isImage
{
  return UTTypeConformsTo((__bridge CFStringRef _Nonnull)_UTI, kUTTypeImage);
}
- (BOOL)isData
{
  return UTTypeConformsTo((__bridge CFStringRef _Nonnull)_UTI, CFSTR("public.data")) ||
    UTTypeConformsTo((__bridge CFStringRef _Nonnull)_UTI, kUTTypeURL);
}
- (BOOL)canOpenInWebView
{
  CFStringRef uti = (__bridge CFStringRef _Nonnull)_UTI;
  return
  UTTypeConformsTo(uti, CFSTR("public.image")) ||
  UTTypeConformsTo(uti, CFSTR("com.adobe.pdf")) ||
  UTTypeConformsTo(uti, CFSTR("com.microsoft.word.doc")) ||
  UTTypeConformsTo(uti, CFSTR("com.microsoft.word.wordml")) ||
  UTTypeConformsTo(uti, CFSTR("com.microsoft.excel.xls")) ||
  UTTypeConformsTo(uti, CFSTR("com.microsoft.powerpoint.ppt")) ||
  UTTypeConformsTo(uti, CFSTR("org.openxmlformats.openxml")) ||
  UTTypeConformsTo(uti, CFSTR("org.oasis-open.opendocument")) ||
  UTTypeConformsTo(uti, CFSTR("public.audio")) ||
  UTTypeConformsTo(uti, CFSTR("public.xml")) ||
  UTTypeConformsTo(uti, CFSTR("public.html")) ||
  UTTypeConformsTo(uti, CFSTR("public.rtf")) ||
  UTTypeConformsTo(uti, CFSTR("public.text")) ||
  UTTypeConformsTo(uti, CFSTR("public.movie"))
  ;
}
- (const NSString *)mimeType
{
  if (_mimeType == nil) {
    NSString* mimeType;
    if ( _UTI != nil ) {
      mimeType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)_UTI, kUTTagClassMIMEType);
    }
    _mimeType = mimeType ? mimeType : DefaultMimeType;
  }
  return _mimeType;
}
- (const NSString *)ext
{
  if ( _fileExtension == nil ) {
    NSString* extension;
    if ( _UTI != nil ) {
      CFStringRef uti = (__bridge CFStringRef _Nonnull)_UTI;
      extension = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass(uti, kUTTagClassFilenameExtension);
    }
    _fileExtension = extension ? extension : @"dat";
  }
  return _fileExtension;
}
- (BOOL)isEqual:(id)other {
  if (other == self)
    return YES;
  if (!other || ![other isKindOfClass:[self class]])
    return NO;
  
  UniformTypeIdentifier* o = (UniformTypeIdentifier*)other;
  if (_UTI && o->_UTI) {
    return [_UTI isEqualToString:o->_UTI];
  }
  if ( _UTI == nil && o->_UTI == nil ) {
    return true;
  }
  return false;
}
- (BOOL)conformsTo:(UniformTypeIdentifier *)o
{
  if ( self.isImage && o.isImage ) {
    return true;
  } else {
    return UTTypeConformsTo((__bridge CFStringRef _Nonnull)(o->_UTI), (__bridge CFStringRef _Nonnull)_UTI);
  }
  return false;
}
- (NSUInteger)hash
{
  return _UTI.hash;
}
- (NSString *)description
{
  return [NSString stringWithFormat:@"[mimeType=%@,fileExt=%@]", self.mimeType, self.ext];
}
@end
/*
#if (TARGET_OS_IPHONE)
UniformTypeIdentifier* PreferredUniformTypeIdentifierFromPasteboard() {
  UIPasteboard* pClipboard = [UIPasteboard generalPasteboard];
  for (NSString* uti in pClipboard.pasteboardTypes) {
    if ( UTTypeConformsTo((__bridge CFStringRef _Nonnull)uti, kUTTypeData) ) {
      return [UniformTypeIdentifier UniformTypeIdentifierWithUTType:uti];
    }
  }
  return nil;
}
NSData* DataFromPasteboard( UniformTypeIdentifier* uti ) {
  UIPasteboard* pClipboard = [UIPasteboard generalPasteboard];
  return [pClipboard dataForPasteboardType:uti.UTType];
}
void ClearDataFromPasteboard( UniformTypeIdentifier* uti ) {
  UIPasteboard* pClipboard = [UIPasteboard generalPasteboard];
  [pClipboard setValue:@"" forPasteboardType:uti.UTType];
}
#endif
*/
