/*
 * Copyright (c) 2015 Thorsten Steube
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the project's author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#import <Foundation/Foundation.h>

#ifdef __UTTYPE__
#endif

// UTI (Uniform Type Identifier)
// see
// http://uti.schwa.io/
// https://developer.apple.com/library/ios/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html

// Defaults
extern NSString *const DefaultMimeType;
extern NSString *const DefaultUTIType;
extern NSString *const DefaultFileExtension;

@interface UniformTypeIdentifier : NSObject
+ (UniformTypeIdentifier *)GenericUTI;
+ (UniformTypeIdentifier *)WindowsBitmapFormatUTI;
+ (UniformTypeIdentifier *)TaggedImageFileFormatUTI;
+ (UniformTypeIdentifier *)JointPhotographicExpertsGroupUTI;
+ (UniformTypeIdentifier *)GraphicInterchangeFormatUTI;
+ (UniformTypeIdentifier *)PortableNetworkGraphicUTI;
+ (UniformTypeIdentifier *)PortableDocumentFormatUTI;
+ (UniformTypeIdentifier *)CertificateUTI;
+ (UniformTypeIdentifier *)PKCS12UTI;
//+ (UniformTypeIdentifier *)PemUTI;
+ (UniformTypeIdentifier *)ZipArchiveUTI;
+ (UniformTypeIdentifier *)EjinArchiveUTI;
+ (NSString *)ExtensionOfMimeType:(NSString *)mimeType;
+ (UniformTypeIdentifier *)UniformTypeIdentifierWithMimeType;
+ (instancetype)UniformTypeIdentifierWithUTType:(CFStringRef)type;
+ (instancetype)UniformTypeIdentifierWithFileExtension:(NSString *)extension;
+ (instancetype)UniformTypeIdentifierWithMimeType:(NSString *)mimeType;
- (instancetype)init NS_UNAVAILABLE;
- (id)initWithUTType:(NSString *)type NS_DESIGNATED_INITIALIZER;
- (BOOL)conformsTo:(UniformTypeIdentifier *)o;
- (NSString *)UTType;
- (NSString *)mimeType;
- (NSString *)ext;
- (BOOL)isImage;
- (BOOL)isData;
- (BOOL)canOpenInWebView;
@end
