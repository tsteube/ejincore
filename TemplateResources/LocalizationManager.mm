/* Copyright (C) 2018 Thorsten Steube
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// https://developer.apple.com/library/content/documentation/MacOSX/Conceptual/BPInternational/StringsdictFileFormat/StringsdictFileFormat.html
// https://tech.smartling.com/localizing-your-ios-os-x-apps-the-right-way-stringsdict-to-the-rescue-1b90082c00ae

#import <Foundation/Foundation.h>

#include "LocalizationManager.h"
#import "UniformTypeIdentifier.h"

#include <iostream>
#include <sstream>
#include <iostream>

static NSBundle* bundle = nil;
static NSDateFormatter* RFC2822DateFormatter = nil;
static NSISO8601DateFormatter* ISO8601DateFormatter = nil;

#define STRING_TO_NSSTRING(variable) \
[NSString stringWithCString: (variable).c_str() encoding:NSUTF8StringEncoding]
#define NSSTRING_TO_STRING(variable) \
[variable cStringUsingEncoding:NSUTF8StringEncoding]

LocalizationManager* LocalizationManager::lm = nullptr;
LocalizationManager* LocalizationManager::getInstance() {
  if (!lm) {
    lm = new LocalizationManager();
  }
  return lm;
}

LocalizationManager::LocalizationManager() {
  bundle = [NSBundle bundleWithIdentifier:@"net.gobi.ejin.TemplateResources"];

  ISO8601DateFormatter = [[NSISO8601DateFormatter alloc] init];
//  ISO8601DateFormatter = [[NSDateFormatter alloc] init];
//  [ISO8601DateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
//  [ISO8601DateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
  [dateFormatter setLocale:enUSPOSIXLocale];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];

  RFC2822DateFormatter = [[NSDateFormatter alloc] init];
  // Note: We have to force the locale to "en_US" to avoid unexpected issues formatting data
  NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
  [RFC2822DateFormatter setLocale: usLocale];
  [RFC2822DateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
  // set the format based on an RFC2822 date format
  [RFC2822DateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
}
LocalizationManager ::~LocalizationManager(){
  if (lm) {
    delete lm;
  }
}

string LocalizationManager::formatNumber(double number)
{
  NSString* localizedNumber = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:number]
                                                               numberStyle:NSNumberFormatterDecimalStyle];
  return NSSTRING_TO_STRING(localizedNumber);
}
string LocalizationManager::formatDateTime(double secondsSinceEpoch)
{
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:secondsSinceEpoch];
  NSString* localizedDateTime = [NSDateFormatter localizedStringFromDate:date
                                                               dateStyle:NSDateFormatterShortStyle
                                                               timeStyle:NSDateFormatterShortStyle];
  return NSSTRING_TO_STRING(localizedDateTime);
}
string LocalizationManager::formatDate(double secondsSinceEpoch)
{
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:secondsSinceEpoch];
  NSString* localizedDateTime = [NSDateFormatter localizedStringFromDate:date
                                                               dateStyle:NSDateFormatterShortStyle
                                                               timeStyle:NSDateFormatterNoStyle];
  return NSSTRING_TO_STRING(localizedDateTime);
}
string LocalizationManager::formatTime(double secondsSinceEpoch)
{
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:secondsSinceEpoch];
  NSString* localizedDateTime = [NSDateFormatter localizedStringFromDate:date
                                                               dateStyle:NSDateFormatterNoStyle
                                                               timeStyle:NSDateFormatterShortStyle];
  return NSSTRING_TO_STRING(localizedDateTime);
}
string LocalizationManager::formatISO8601Time(double secondsSinceEpoch)
{
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:secondsSinceEpoch];
  NSString* formatted = [ISO8601DateFormatter stringFromDate:date];
  return NSSTRING_TO_STRING(formatted);
}
string LocalizationManager::formatRFC2822Time(double secondsSinceEpoch)
{
  NSDate* date = [NSDate dateWithTimeIntervalSince1970:secondsSinceEpoch];
  NSString* formatted = [RFC2822DateFormatter stringFromDate:date];
  return NSSTRING_TO_STRING(formatted);
}

string LocalizationManager::formatString(const string& key)
{
  NSString* value = [bundle localizedStringForKey:STRING_TO_NSSTRING(key) value:@"" table:nil];
  return NSSTRING_TO_STRING(value);
}
string LocalizationManager::formatString(const string& key, int arg)
{
  NSString* value = [bundle localizedStringForKey:STRING_TO_NSSTRING(key) value:@"" table:nil];
  return NSSTRING_TO_STRING(([NSString localizedStringWithFormat:value, arg]));
}

string LocalizationManager::formatString(const string& key, const string& arg)
{
  NSString* value = [bundle localizedStringForKey:STRING_TO_NSSTRING(key) value:@"" table:nil];
  NSString* formatted = [NSString stringWithFormat:value, STRING_TO_NSSTRING(arg)];
  return NSSTRING_TO_STRING(formatted);
}
string LocalizationManager::formatString(const string& key, const string& arg0, const string& arg1)
{
  NSString* value = [bundle localizedStringForKey:STRING_TO_NSSTRING(key) value:@"" table:nil];
  NSString* formatted = [NSString stringWithFormat:value, STRING_TO_NSSTRING(arg0), STRING_TO_NSSTRING(arg1)];
  return NSSTRING_TO_STRING(formatted);
}
string LocalizationManager::formatEnumeration(const string& key, const vector<string>& arg)
{
  NSString* value = [bundle localizedStringForKey:STRING_TO_NSSTRING(key) value:@"" table:nil];
  NSString* formatted = [NSString stringWithFormat:value, STRING_TO_NSSTRING(joinEnumberation(arg, ", "))];
  return NSSTRING_TO_STRING(formatted);
}
string LocalizationManager::fileExtensionOfMimeType(const string& mimeType) {
  NSString* key = [NSString stringWithCString: mimeType.c_str() encoding:NSUTF8StringEncoding];
  NSString* ext = [UniformTypeIdentifier ExtensionOfMimeType:key];
  return [ext cStringUsingEncoding:NSUTF8StringEncoding];
}

string LocalizationManager::joinEnumberation( const vector<string> elements, const char* const separator)
{
  switch (elements.size())
  {
    case 0:
      return "";
    case 1:
      return elements[0];
    default:
      std::ostringstream os;
      std::copy(elements.begin(), elements.end()-1, std::ostream_iterator<std::string>(os, separator));
      os << *elements.rbegin();
      return os.str();
  }
}
