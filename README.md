# Ejin Synchronization Protocol Implementation #

** [Ejin  Cloud](https://itunes.apple.com/de/app/ejin/id768267980?mt=8&uo=4) is an end-to-end encrypted cloud solution for iPhone and iPad**

The core Ejin engine requires a complex synchronization protocol that has been  
developed to exchange small chunks of updated data. The content gets encrypted  
on the client before transmitting to the server. On the other hand downloads  
from the cloud gets also decrypted on the fly.  
  
A set of metadata is necessary to manage the uploaded information on the server.  
The content is distributed for a group of members registered for the same channel.  
Each member has a defined access role for each channel. Both the membership and  
the roles can change over time. 

The synchronization protocol is described in detail on the [Ejin Homepage](http://logboard.de/) mentioned above.  
Because the server part is not published yet the testing here  
is done with mock objects and sample test data. 

### Client side reference implementation ###

This code base here is the client side implementation of the Ejin  
synchronization protocol developed for the iOS mobile application.  

It is roughly identically to the Java reference implementation  
of this protocol also available online. Note that this core C++  
library doesn't do any network communication. The communication  
layer is part of the next higher level Ejin module not published yet.  
Also in stead of using standard [Sqlite](http://www.sqlite.org/) this library requires [sqlcipher](https://www.zetetic.net/sqlcipher/).  

### Build instructions ###

* You need to compile the external libraries mentioned below first. See the readme instructions in the extlibs subfolder and use the Makefile to build the binaries for different architectures.  
* You need XCode 6.3 to compile the library and run the ejinCore unit test suite.  

### Changelog ###

* Version 2.0.0 (initial version of this repository)

### License ###

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Contact ###

Thorsten Steube [info@logboard.de](info@logboard.de)

### List of External Libraries (the packages not part of this repository) ###

* [cppunit](http://sourceforge.net/projects/cppunit/)    1.10.3
* [ctemplate](http://code.google.com/p/google-ctemplate/)  2.3
* [expat]( http://sourceforge.net/projects/expat/)      2.1.0
* [jansson](http://www.digip.org/jansson/)    2.7
* [openssl](https://www.openssl.org/source/)    1.0.2a
* [sqlcipher](http://sqlcipher.net/ios-tutorial)  3.3.0