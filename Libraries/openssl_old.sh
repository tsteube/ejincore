#!/bin/sh
#
# These are implemented as functions, and just called by the
# short MAIN section below
## Determine the appropriate expat source path to use
## Introduced by michaeltyson, 
## Adapted by Thorsten Steube

#set
#set -x

##
## environment variables
##
LIBRARY_NAME=openssl
LIBRARY_SSL=ssl
LIBRARY_CRYPTO=crypto

# read from environment
#PLATFORM_DEVELOPER_USR_DIR=/Developer/Platforms/iPhoneOS.platform/Developer/usr
#SRCROOT=/Users/tsteube/Development/ejin-alashan/libs
#PROJECT_TEMP_DIR=/Users/tsteube/Development/tmp/XcodeBuild/expat.build
#TARGET_BUILD_DIR=/Users/tsteube/Development/tmp/XcodeBuild/Debug
#SDKROOT=/Developer/SDKs/MacOSX10.6.sdk
#SDKROOT=/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS4.3.sdk/
#ARCHS="armv7 i386 x86_64"

# derive DEVELOPER environment from SDKROOT
PLATFORM_DEVELOPER_SDK=${SDKROOT}/..
PLATFORM_DEVELOPER_DIR=${PLATFORM_DEVELOPER_SDK}/..
PLATFORM_DEVELOPER_USR_DIR=${PLATFORM_DEVELOPER_DIR}/usr/
PLATFORM_DEVELOPER_BIN_DIR=${PLATFORM_DEVELOPER_USR_DIR}/bin

# figure out the right set of build architectures for this run
BUILDARCHS="${ARCHS}"
case "$SDKROOT" in
  *iPhoneSimulator*)
	  BUILDARCHS="i386 x86_64"
    ;;
  *iPhoneOS*)
	  BUILDARCHS="armv7 armv7s arm64"
    ;;
  *MacOSX*)
    BUILDARCHS="i386 x86_64"
    ;;
  *)
    echo "***** unknown SDK $SDKROOT *****"
    exit 1
  ;;
esac

# - - - - - - - - - - - - - - -
buildAction () {

	# locate src archive file if present
	SRC_ARCHIVE=( ${SRCROOT}/*${LIBRARY_NAME}*.tar.gz )
	if [ "${SRC_ARCHIVE}" == "" ] 
	then
 		echo "***** \"${LIBRARY_NAME}\" source package not found" >&2
 		exit 1;
	fi
  
	TARGET_SRC="${PROJECT_DERIVED_FILE_DIR}"
  if [ ! -d "${TARGET_SRC}" ] ||  [ -z "$(ls ${TARGET_SRC}/)" ]; then
  	echo "***** extracting $SRC_ARCHIVE..."
	  mkdir "$TARGET_SRC"
	  tar -C "$TARGET_SRC" --strip-components=1 -zxf "$SRC_ARCHIVE" || exit 1
  fi

	for BUILDARCH in $BUILDARCHS
 	do
    LIBRARY_DESCRIPTION="${LIBRARY_NAME} ${BUILDARCH} Library for ${SDKROOT}"

  	if [ -f "$TARGET_BUILD_DIR/lib${LIBRARY_SSL}.a" ]; then 
    	echo "${LIBRARY_DESCRIPTION} is already build" 
  	  continue;
	  fi
  	echo "Building ${LIBRARY_DESCRIPTION} ..."

 		# check whether library already exists - we'll only build if it does not
 		#if [ -f  ${TARGET_BUILD_DIR}/${FULL_PRODUCT_NAME} ]; then
 		#  echo "***** Using previously-built libary ${TARGET_BUILD_DIR}//${FULL_PRODUCT_NAME} - skipping build *****"
 		#  echo "***** To force a rebuild clean project and clean dependencies *****"
 		#  break
 		#fi

 		cd "$TARGET_SRC"
    make dclean clean

    case "$SDKROOT" in
      *iPhoneSimulator*)
        case "$BUILDARCH" in
	        i386)
	          CONFIGUREFLAGS="no-shared no-asm no-krb5 no-gost no-hw darwin-i386-cc"
          ;;
          x86_64)
            CONFIGUREFLAGS="no-shared no-asm no-krb5 no-gost no-hw darwin64-x86_64-cc"
	        ;;
	      esac
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -mios-simulator-version-min=6.0 -no-cpp-precomp -D_DARWIN_C_SOURCE -UOPENSSL_BN_ASM_PART_WORDS"
        ;;
      *iPhoneOS*)
		    case "$BUILDARCH" in
		      arm64)
  	        CONFIGUREFLAGS="no-shared no-asm no-krb5 no-gost no-hw iphoneos-cross"
  	        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -miphoneos-version-min=7.0 -no-cpp-precomp -pipe -fvisibility=hidden  -D_DARWIN_C_SOURCE -UOPENSSL_BN_ASM_PART_WORDS"
          ;;
		      armv*)
	          CONFIGUREFLAGS="no-shared no-asm no-krb5 no-gost no-hw iphoneos-cross"
		  		  sed -ie "s!static volatile sig_atomic_t intr_signal;!static volatile intr_signal;!" "crypto/ui/ui_openssl.c"
	          COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -miphoneos-version-min=6.0 -no-cpp-precomp -pipe -fvisibility=hidden  -D_DARWIN_C_SOURCE -UOPENSSL_BN_ASM_PART_WORDS"
          ;;
          *)
	          CONFIGUREFLAGS="no-shared no-asm no-krb5 no-gost no-hw"
	          COMMONFLAGS="-arch $BUILDARCH -fembed-bitcode -isysroot ${SDKROOT}"
          ;;
        esac
        export CROSS_TOP=`echo ${SDKROOT} | sed 's/\(.*\)SDKs\(.*\)/\1/'`
        export CROSS_SDK=`echo ${SDKROOT} | sed 's/\(.*\)SDKs\(.*\)/\2/'`
        ;;
      *MacOSX*)
	      case "$BUILDARCH" in
  	      i386)
  	        CONFIGUREFLAGS="shared no-asm no-krb5 no-gost no-hw darwin-i386-cc"
	        ;;
	        x86_64)
	          CONFIGUREFLAGS="shared no-asm no-krb5 no-gost no-hw darwin64-x86_64-cc"
  	      ;;
  	    esac
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -no-cpp-precomp -D_DARWIN_C_SOURCE -UOPENSSL_BN_ASM_PART_WORDS"
        ;;
			*)
  			echo "***** unknown SDK $SDKROOT *****"
   			exit 1
				;;
    esac

    export CC=`xcrun --find cc -sdk $SDKROOT`
    export CXX=`xcrun --find c++ -sdk $SDKROOT`
    export LD=`xcrun --find ld -sdk $SDKROOT`
    export LIBTOOL=`xcrun --find libtool -sdk $SDKROOT`
    export AR=`xcrun --find ar -sdk $SDKROOT`
    export AS=`xcrun --find as -sdk $SDKROOT`
    export NM=`xcrun --find nm -sdk $SDKROOT`
    export RANLIB=`xcrun --find ranlib -sdk $SDKROOT`
    export LDFLAGS="${COMMONFLAGS} -O2"
    export CFLAGS="${COMMONFLAGS}"
    export CPPFLAGS="${COMMONFLAGS}"
    export CXXFLAGS="${COMMONFLAGS}"

    #############################
    # run configure + make
    ./configure --prefix=/usr/local2 --openssldir=/usr/local/openssl2 ${CONFIGUREFLAGS} && make build_libs
    if [ "$?" == "0" ]; then
      echo "copying header files to $TARGET_BUILD_DIR/include/${LIBRARY_NAME}"
      [ -d "$TARGET_BUILD_DIR/include/${LIBRARY_NAME}/" ] || mkdir -p "$TARGET_BUILD_DIR/include/${LIBRARY_NAME}/"
      cp "$TARGET_SRC/include/${LIBRARY_NAME}"/*.h "$TARGET_BUILD_DIR/include/${LIBRARY_NAME}/"

		  [ -d "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" ] || mkdir -p "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
		  rm -rf "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}"*
		  if [ -f ./lib${LIBRARY_CRYPTO}.a ]; then
		    cp -RPLf ./lib${LIBRARY_CRYPTO}.a "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" 
	      TARGET_CRYPTO_STATIC_LIBS="$TARGET_CRYPTO_STATIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_CRYPTO}.a"
		  fi
		  if [ -f ./lib${LIBRARY_CRYPTO}.dylib ]; then
		    cp -RPf ./lib${LIBRARY_CRYPTO}*.dylib "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
	      TARGET_CRYPTO_DYNAMIC_LIBS="$TARGET_CRYPTO_DYNAMIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_CRYPTO}.dylib"
		  fi
		  if [ -f ./lib${LIBRARY_SSL}.a ]; then
		    cp -RPLf ./lib${LIBRARY_SSL}.a "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" 
	      TARGET_SSL_STATIC_LIBS="$TARGET_SSL_STATIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_SSL}.a"
		  fi
		  if [ -f ./lib${LIBRARY_SSL}.dylib ]; then
		    cp -RPf ./lib${LIBRARY_SSL}*.dylib "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
	      TARGET_SSL_DYNAMIC_LIBS="$TARGET_SSL_DYNAMIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_SSL}.dylib"
      fi

	  	echo "Installed ${LIBRARY_DESCRIPTION} in ${TARGET_BUILD_DIR}/${BUILDARCH}/lib."
		else 
		  echo "***** Failed to build ${LIBRARY_DESCRIPTION}  *****" 
		  exit 1 
		fi 
	done

	# build universal library
	if [ -n "$TARGET_SSL_STATIC_LIBS" ]; then
    lipo -create $TARGET_SSL_STATIC_LIBS -output "$TARGET_BUILD_DIR/lib${LIBRARY_SSL}.a"
  	#ranlib "$TARGET_SSL_BUILD_DIR/lib${LIBRARY_SSL}.a"
	  echo "Installed static universal ${BUILDARCHS} in ${TARGET_BUILD_DIR}."
  fi
# HINT: see doc/openssl-shared.txt for using openssl as shared lib
#       I still have problems with the xcode unit test projects and DYLD_LIBRARY_PATH
#if [[ "$SDKROOT" =~ "MacOSX" ]] && [ -n "$TARGET_SSL_DYNAMIC_LIBS" ]; then
#  lipo -create $TARGET_SSL_DYNAMIC_LIBS -output "$TARGET_BUILD_DIR/lib${LIBRARY_SSL}.dylib"
#  echo "Installed dynamic universal ${BUILDARCHS} ${LIBRARY_SSL} in ${TARGET_BUILD_DIR}."
# fi

	if [ -n "$TARGET_CRYPTO_STATIC_LIBS" ]; then
    lipo -create $TARGET_CRYPTO_STATIC_LIBS -output "$TARGET_BUILD_DIR/lib${LIBRARY_CRYPTO}.a"
  	#ranlib "$TARGET_CRYPTO_BUILD_DIR/lib${LIBRARY_CRYPTO}.a"
	  echo "Installed static universal ${BUILDARCHS} in ${TARGET_BUILD_DIR}."
  fi
#	if [[ "$SDKROOT" =~ "MacOSX" ]] && [ -n "$TARGET_CRYPTO_DYNAMIC_LIBS" ]; then
#    lipo -create $TARGET_CRYPTO_DYNAMIC_LIBS -output "$TARGET_BUILD_DIR/lib${LIBRARY_CRYPTO}.dylib"
#	  echo "Installed dynamic universal ${BUILDARCHS} ${LIBRARY_SSL} in ${TARGET_BUILD_DIR}."
#  fi

	exit 0
}

# - - - - - - - - - - - - - - -
cleanAction () {	
	# clear header file
	rm -rf "$TARGET_BUILD_DIR/include/${LIBRARY_NAME}/"
	for BUILDARCH in $BUILDARCHS
	do
    LIBRARY_DESCRIPTION="${LIBRARY_NAME} ${BUILDARCH} Library for ${SDKROOT}"
  	echo "Cleaning ${LIBRARY_DESCRIPTION} ..."
    rm -f "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_SSL}"*
    rm -f "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_CRYPTO}"*
    rm -f "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/pkgconfig/lib${LIBRARY_SSL}"*
    rm -f "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/pkgconfig/lib${LIBRARY_CRYPTO}"*
  done
  rm -f "${TARGET_BUILD_DIR}/lib${LIBRARY_SSL}"*
  rm -f "${TARGET_BUILD_DIR}/lib${LIBRARY_CRYPTO}"*
	echo "Cleaning ${LIBRARY_NAME} sources ..."
  rm -rf "${PROJECT_DERIVED_FILE_DIR}" 
  exit 0;
}

###############################
# MAIN

case $ACTION in
  # NOTE: for some reason, it gets set to "" rather than "build" when
  # doing a build.
  "")
    buildAction
    ;;

  "clean")
    cleanAction
    ;;
esac

exit 0
