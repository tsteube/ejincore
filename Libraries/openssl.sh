#!/bin/sh
#
# These are implemented as functions, and just called by the
# short MAIN section below
## Determine the appropriate expat source path to use
## Introduced by michaeltyson, 
## Adapted by Thorsten Steube

#set
#set -x

##
## environment variables
##
LIBRARY_NAME=openssl

# read from environment
#PLATFORM_DEVELOPER_USR_DIR=/Developer/Platforms/iPhoneOS.platform/Developer/usr
#SRCROOT=/Users/tsteube/Development/ejin-alashan/libs
#PROJECT_TEMP_DIR=/Users/tsteube/Development/tmp/XcodeBuild/expat.build
#TARGET_BUILD_DIR=/Users/tsteube/Development/tmp/XcodeBuild/Debug
#SDKROOT=/Developer/SDKs/MacOSX10.6.sdk
#SDKROOT=/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS4.3.sdk/
#ARCHS="armv7 i386 x86_64"

# derive DEVELOPER environment from SDKROOT
PLATFORM_DEVELOPER_SDK=${SDKROOT}/..
PLATFORM_DEVELOPER_DIR=${PLATFORM_DEVELOPER_SDK}/..
PLATFORM_DEVELOPER_USR_DIR=${PLATFORM_DEVELOPER_DIR}/usr/
PLATFORM_DEVELOPER_BIN_DIR=${PLATFORM_DEVELOPER_USR_DIR}/bin

# figure out the right set of build architectures for this run
BUILDARCHS="${ARCHS}"
case "$SDKROOT" in
  *iPhoneSimulator*)
	  BUILDARCHS="x86_64"
    ;;
  *iPhoneOS*)
	  BUILDARCHS="armv7 arm64"
    ;;
  *MacOSX*)
    BUILDARCHS="x86_64"
    ;;
  *)
    echo "***** unknown SDK $SDKROOT *****"
    exit 1
  ;;
esac

# - - - - - - - - - - - - - - -
buildAction () {
  FRAMEWORK_OPENSSL_NAME=openssl
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${FRAMEWORK_OPENSSL_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  if [ -d "${FRAMEWORK_LOCN}" ]; then
    echo "${LIBRARY_NAME} Framework in ${TARGET_BUILD_DIR} has already been build"
    return;
  fi
  echo "Building ${LIBRARY_NAME} ..."

	# locate src archive file if present
	SRC_ARCHIVE=( ${SRCROOT}/*openssl*.tar.gz )
	if [ "${SRC_ARCHIVE}" == "" ] 
	then
 		echo "***** \"${LIBRARY_NAME}\" source package not found" >&2
 		exit 1;
	fi
  
	TARGET_SRC="${PROJECT_DERIVED_FILE_DIR}"
  if [ ! -d "${TARGET_SRC}" ] ||  [ -z "$(ls ${TARGET_SRC}/)" ]; then
  	echo "***** extracting $SRC_ARCHIVE..."
	  mkdir -p "$TARGET_SRC"
	  tar -C "$TARGET_SRC" --strip-components=1 -zxf "$SRC_ARCHIVE" || exit 1
  fi

	for BUILDARCH in $BUILDARCHS
 	do
    LIBRARY_DESCRIPTION="${LIBRARY_NAME} ${BUILDARCH} Library for ${SDKROOT}"
  	echo "Building ${LIBRARY_DESCRIPTION} ..."

 		cd "$TARGET_SRC"
#make dclean clean
    make distclean

    case "$SDKROOT" in
      *iPhoneSimulator*)
        case "$BUILDARCH" in
	        i386)
	          CONFIGUREFLAGS="no-shared no-asm no-gost no-hw darwin-i386-cc threads"
          ;;
          x86_64)
            CONFIGUREFLAGS="no-shared no-asm no-gost no-hw darwin64-x86_64-cc threads"
	        ;;
	      esac
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension -mios-simulator-version-min=10.0 -no-cpp-precomp -D_DARWIN_C_SOURCE -D_REENTRANT -UOPENSSL_BN_ASM_PART_WORDS -O3"
        ;;
      *iPhoneOS*)
		    case "$BUILDARCH" in
		      arm64)
  	        CONFIGUREFLAGS="no-shared no-asm no-gost no-hw iphoneos-cross threads"
  	        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -fapplication-extension -miphoneos-version-min=10.0 -no-cpp-precomp -pipe -fvisibility=hidden  -D_DARWIN_C_SOURCE -D_REENTRANT -UOPENSSL_BN_ASM_PART_WORDS -Os"
          ;;
		      armv*)
	          CONFIGUREFLAGS="no-shared no-asm no-gost no-hw iphoneos-cross threads"
		  		  sed -ie "s!static volatile sig_atomic_t intr_signal;!static volatile intr_signal;!" "crypto/ui/ui_openssl.c"
	          COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -fapplication-extension -miphoneos-version-min=10.0 -no-cpp-precomp -pipe -fvisibility=hidden  -D_DARWIN_C_SOURCE -D_REENTRANT -UOPENSSL_BN_ASM_PART_WORDS -Os"
          ;;
          *)
	          CONFIGUREFLAGS="no-shared no-asm no-krb5 no-gost no-hw"
	          COMMONFLAGS="-arch $BUILDARCH -fembed-bitcode -isysroot ${SDKROOT} -Os"
          ;;
        esac
        export CROSS_TOP=`echo ${SDKROOT} | sed 's/\(.*\)SDKs\(.*\)/\1/'`
        export CROSS_SDK=`echo ${SDKROOT} | sed 's/\(.*\)SDKs\(.*\)/\2/'`
        ;;
      *MacOSX*)
	      case "$BUILDARCH" in
  	      i386)
  	        CONFIGUREFLAGS="no-shared no-asm no-gost no-hw darwin-i386-cc threads"
	        ;;
	        x86_64)
	          CONFIGUREFLAGS="no-shared no-asm no-gost no-hw darwin64-x86_64-cc threads"
  	      ;;
  	    esac
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension -mmacosx-version-min=10.12 -no-cpp-precomp -D_DARWIN_C_SOURCE -D_REENTRANT -UOPENSSL_BN_ASM_PART_WORDS -O3"
        ;;
			*)
  			echo "***** unknown SDK $SDKROOT *****"
   			exit 1
				;;
    esac

    export CC=`xcrun --find cc -sdk $SDKROOT`
    export CXX=`xcrun --find c++ -sdk $SDKROOT`
    export LD=`xcrun --find ld -sdk $SDKROOT`
    export LIBTOOL=`xcrun --find libtool -sdk $SDKROOT`
    export AR=`xcrun --find ar -sdk $SDKROOT`
    export AS=`xcrun --find as -sdk $SDKROOT`
    export NM=`xcrun --find nm -sdk $SDKROOT`
    export RANLIB=`xcrun --find ranlib -sdk $SDKROOT`
    export LDFLAGS="${COMMONFLAGS}"
    export CFLAGS="${COMMONFLAGS}"
    export CPPFLAGS="${COMMONFLAGS}"
    export CXXFLAGS="${COMMONFLAGS}"

    #############################
    # run configure + make
    ./configure --prefix=/usr/local --openssldir=/usr/local/openssl ${CONFIGUREFLAGS} && make V=1 build_libs
    if [ "$?" == "0" ]; then

		  [ -d "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" ] || mkdir -p "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
		  rm -rf "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}"*

      if [ -f ./libssl.a ] && [ -f ./libcrypto.a ]; then
        libtool -static -o lib${LIBRARY_NAME}.a ./libssl.a ./libcrypto.a
        cp -RPLf ./lib${LIBRARY_NAME}.a "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
        TARGET_STATIC_LIBS="$TARGET_STATIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.a"
      fi
		  if [ -f ./libssl.dylib ] && [ -f ./libcrypto.dylib ]; then
        libtool -static -o lib${LIBRARY_NAME}.a ./libssl.dylib ./libcrypto.dylib
		    cp -RPf ./lib${LIBRARY_NAME}*.dylib "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
	      TARGET_DYNAMIC_LIBS="$TARGET_DYNAMIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.dylib"
		  fi

	  	echo "Build ${LIBRARY_DESCRIPTION} in ${TARGET_BUILD_DIR}/${BUILDARCH}/lib."
		else 
		  echo "***** Failed to build ${LIBRARY_DESCRIPTION}  *****" 
		  exit 1 
		fi 
	done

  # Create the path to the real Headers dir
  /bin/rm -rf "${FRAMEWORK_LOCN}"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Resources/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Documentation/"
  # Create the required symlinks
  /bin/ln -sfh A "${FRAMEWORK_LOCN}/Versions/Current"
  /bin/ln -sfh Versions/Current/Headers "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/${FRAMEWORK_OPENSSL_NAME}" "${FRAMEWORK_LOCN}/${FRAMEWORK_OPENSSL_NAME}"
  # Copy the public headers into the framework
  cp "$TARGET_SRC/include/openssl/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/"

	# build universal library
  if [ -n "$TARGET_DYNAMIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_DYNAMIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${FRAMEWORK_OPENSSL_NAME}"
    /usr/bin/install_name_tool -id "@rpath/${FRAMEWORK_OPENSSL_NAME}.framework/Versions/A/${FRAMEWORK_OPENSSL_NAME}" \
       "${FRAMEWORK_LOCN}/Versions/A/${FRAMEWORK_OPENSSL_NAME}"
	elif [ -n "$TARGET_STATIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_STATIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${FRAMEWORK_OPENSSL_NAME}"
  fi

  /bin/ln -sfh "Versions/Current/Headers" "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/Resources" "${FRAMEWORK_LOCN}/Resources"
  /bin/ln -sfh "Versions/Current/Documentation" "${FRAMEWORK_LOCN}/Documentation"
  [ -d `dirname ${DEBUG_FRAMEWORK_LOCN}` ] || /bin/mkdir `dirname ${DEBUG_FRAMEWORK_LOCN}`
  /bin/rm -rf ${DEBUG_FRAMEWORK_LOCN}
  /bin/ln -sfh ${FRAMEWORK_LOCN} ${DEBUG_FRAMEWORK_LOCN}

	case "$SDKROOT" in
	  *iPhoneSimulator*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
  	<array>
	    <string>iPhoneOS</string>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *iPhoneOS*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>iPhoneOS</string>
	  </array>"
	    ;;
	  *MacOSX*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *)
	    ;;
	esac

	cat > ${FRAMEWORK_LOCN}/Resources/Info.plist <<EOF
		<?xml version="1.0" encoding="UTF-8"?>
		<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
		<plist version="1.0">
		<dict>
		  <key>CFBundleDevelopmentRegion</key>
		  <string>English</string>
		  <key>CFBundleIdentifier</key>
		  <string>openssl</string>
			<key>CFBundleExecutable</key>
			<string>openssl</string>	
		  <key>CFBundleInfoDictionaryVersion</key>
		  <string>6.0</string>
		  <key>CFBundlePackageType</key>
		  <string>FMWK</string>
		  <key>CFBundleSignature</key>
		  <string>????</string>
		  <key>CFBundleVersion</key>
		  <string>${VERSION}</string>
				$PLATFORM_KEY
			</dict>
			</plist>
EOF
  
  echo "Create ${LIBRARY_NAME}.framework for ${BUILDARCHS} in ${FRAMEWORK_LOCN}."
  if [ "x$SIGNATURE" != "x" ] ; then
		/usr/bin/codesign -i "${LIBRARY_NAME}" --force --deep --sign "${SIGNATURE}" --preserve-metadata=identifier,entitlements --timestamp=none "${FRAMEWORK_LOCN}/Versions/A" && echo "signed with ${SIGNATURE}"
	else 
  	# sign binary with empty signatur
    /usr/bin/codesign --force --sign - "${FRAMEWORK_LOCN}/Versions/A"
  fi 

	exit 0
}

# - - - - - - - - - - - - - - -
cleanAction () {	
  FRAMEWORK_OPENSSL_NAME=openssl
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${FRAMEWORK_OPENSSL_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  echo "Cleaning ${FRAMEWORK_LOCN} ..."
  rm -rf "${FRAMEWORK_LOCN}"
  rm -f  "${DEBUG_FRAMEWORK_LOCN}"
	echo "Cleaning ${LIBRARY_NAME} sources ..."
  rm -rf "${PROJECT_DERIVED_FILE_DIR}" 
  exit 0;
}

###############################
# MAIN

case $ACTION in
  # NOTE: for some reason, it gets set to "" rather than "build" when
  # doing a build.
  "")
    buildAction
    ;;

  "clean")
    cleanAction
    ;;
esac

exit 0
