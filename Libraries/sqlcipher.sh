#!/bin/sh
#
# These are implemented as functions, and just called by the
# short MAIN section below
## Determine the appropriate expat source path to use
## Introduced by michaeltyson, 
## Adapted by Thorsten Steube

#set

##
## environment variables
##
LIBRARY_NAME=sqlcipher

# extend environment
#
#SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS6.1.sdk
#SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.1.sdk
#SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk
#
# derive DEVELOPER environment from SDKROOT
PLATFORM_DEVELOPER_SDK=${SDKROOT}/..
PLATFORM_DEVELOPER_DIR=${PLATFORM_DEVELOPER_SDK}/..
PLATFORM_DEVELOPER_USR_DIR=${PLATFORM_DEVELOPER_DIR}/usr/
PLATFORM_DEVELOPER_BIN_DIR=${PLATFORM_DEVELOPER_USR_DIR}/bin

#TARGET_BUILD_DIR=/Users/tsteube/Development/tmp/XcodeBuild/Release-iphoneos
#SRCROOT=/Users/tsteube/Development/ejin-alashan/libs

# figure out the right set of build architectures for this run
#BUILDARCHS=${ARCHS:=$ARCHS_UNIVERSAL_IPHONE_OS}
# figure out the right set of build architectures for this run
BUILDARCHS="${ARCHS}"
case "$SDKROOT" in
  *iPhoneSimulator*)
    BUILDARCHS="x86_64"
  ;;
  *iPhoneOS*)
    BUILDARCHS="arm64"
  ;;
  *MacOSX*)
    BUILDARCHS="x86_64"
  ;;
  *)
    echo "***** unknown SDK $SDKROOT *****"
    exit 1
  ;;
esac

# - - - - - - - - - - - - - - -
buildAction () {
  
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${LIBRARY_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  if [ -d "${FRAMEWORK_LOCN}" ]; then
    echo "${LIBRARY_NAME} Framework in ${TARGET_BUILD_DIR} has already been build"
    return;
  fi
  echo "Building ${LIBRARY_NAME} ..."

	# locate src archive file if present
	SRC_ARCHIVE=( ${SRCROOT}/*${LIBRARY_NAME}*.tar.gz )
	if [ "${SRC_ARCHIVE}" == "" ] 
	then
 		echo "***** \"${LIBRARY_NAME}\" source package not found" >&2
 		exit 1;
	fi
  
	TARGET_SRC="${PROJECT_DERIVED_FILE_DIR}"
  if [ ! -d "${TARGET_SRC}" ] ||  [ -z "$(ls ${TARGET_SRC}/)" ]; then
    echo "***** extracting $SRC_ARCHIVE to $TARGET_SRC ..."
	  mkdir -p "$TARGET_SRC"
	  tar -C "$TARGET_SRC" --strip-components=1 -zxf "$SRC_ARCHIVE" || exit 1
  fi

	for BUILDARCH in $BUILDARCHS
 	do
    LIBRARY_DESCRIPTION="${CONFIGURATION} ${LIBRARY_NAME} ${BUILDARCH} Library for ${SDKROOT}"
  	echo "Building ${LIBRARY_DESCRIPTION} ..."

 		cd "$TARGET_SRC"
    make distclean clean

    # system is not supported on ios sdk  !!!
    cd "$TARGET_SRC"
    sed -i.bak 's/x = system(zCmd);/x = -1;/' shell.c

    case "$SDKROOT" in
      *iPhoneSimulator*)
        CONFIGUREFLAGS="--enable-fts5 --enable-tempstore=yes --with-crypto-lib=commoncrypto --enable-shared --enable-static --host=i686-apple-darwin10"
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension -mios-simulator-version-min=10.0 -no-cpp-precomp -D_DARWIN_C_SOURCE -DSQLITE_HAS_CODEC -DSQLITE_ENABLE_FTS5 -DSQLITE_TEMP_STORE=2"
        ;;
      *iPhoneOS*)
		    case "$BUILDARCH" in
		      arm64)
  	        CONFIGUREFLAGS="--enable-fts5 --enable-tempstore=yes --with-crypto-lib=commoncrypto --disable-shared --enable-static --host=arm-apple-darwin10"
  	        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -fapplication-extension -miphoneos-version-min=10.0 -no-cpp-precomp -D_DARWIN_C_SOURCE -DSQLITE_HAS_CODEC -DSQLITE_ENABLE_FTS5 -DSQLITE_TEMP_STORE=2"
          ;;
		      armv*)
	          CONFIGUREFLAGS="--enable-fts5 --enable-tempstore=yes --with-crypto-lib=commoncrypto --disable-shared --enable-static  --host=arm-apple-darwin10"
	          COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -fapplication-extension -miphoneos-version-min=10.0 -no-cpp-precomp -D_DARWIN_C_SOURCE -DSQLITE_HAS_CODEC -DSQLITE_ENABLE_FTS5 -DSQLITE_TEMP_STORE=2"
          ;;
          *)
	          CONFIGUREFLAGS="--enable-fts5 --enable-tempstore=yes --with-crypto-lib=commoncrypto --disable-shared --enable-static"
	          COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode -DSQLITE_HAS_CODEC -DSQLITE_ENABLE_FTS5 -DSQLITE_TEMP_STORE=2"
          ;;
        esac
        ;;
      *MacOSX*)
        CONFIGUREFLAGS="--enable-fts5 --enable-tempstore=yes --with-crypto-lib=commoncrypto --enable-shared --enable-static --prefix=${TARGET_BUILD_DIR}/${BUILDARCH}"
        COMMONFLAGS="-arch $BUILDARCH -fapplication-extension -mmacosx-version-min=10.12 -isysroot ${SDKROOT} -no-cpp-precomp -D_DARWIN_C_SOURCE -DSQLITE_HAS_CODEC -DSQLITE_ENABLE_FTS5 -DSQLITE_TEMP_STORE=2"
        ;;
			*)
  			echo "***** unknown SDK $SDKROOT *****"
   			exit 1
				;;
    esac

    export CC=`xcrun --find cc -sdk $SDKROOT`
    export GCC=`xcrun --find gcc -sdk $SDKROOT`
    export CXX=`xcrun --find c++ -sdk $SDKROOT`
    export LD=`xcrun --find ld -sdk $SDKROOT`
    export LIBTOOL=`xcrun --find libtool -sdk $SDKROOT`
    export AR=`xcrun --find ar -sdk $SDKROOT`
    export AS=`xcrun --find as -sdk $SDKROOT`
    export NM=`xcrun --find nm -sdk $SDKROOT`
    export RANLIB=`xcrun --find ranlib -sdk $SDKROOT`
    export LDFLAGS="${COMMONFLAGS} -O2 -framework Security -framework Foundation"
    export CFLAGS="${COMMONFLAGS}"
    export CPPFLAGS="${COMMONFLAGS}"
    export CXXFLAGS="${COMMONFLAGS}"

echo CC=$CC
echo CXX=$CXX
echo LD=$LD
echo LIBTOOL=$LIBTOOL
echo AR=$AR
echo AS=$AS
echo NM=$CNM
echo RANLIB=$RANLIB
echo LDFLAGS=$LDFLAGS
echo CFLAGS=$CFLAGS
echo CPPFLAGS=$CPPFLAGS
echo CXXFLAGS=$CXXFLAGS
echo ./configure ${CONFIGUREFLAGS}

    #############################
    # run configure + make
    ./configure ${CONFIGUREFLAGS} && make
    if [ "$?" == "0" ]; then

		  [ -d "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" ] || mkdir -p "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
		  rm -rf "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}"*
		  if [ -f ./.libs/lib${LIBRARY_NAME}.a ]; then
		    cp -RPLf ./.libs/lib${LIBRARY_NAME}.a "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" 
	      TARGET_STATIC_LIBS="$TARGET_STATIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.a"
		  fi
		  if [ -f ./.libs/lib${LIBRARY_NAME}.dylib ]; then
		    cp -RPf ./.libs/lib${LIBRARY_NAME}*.dylib "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
	      TARGET_DYNAMIC_LIBS="$TARGET_DYNAMIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.dylib"
      fi

	  	echo "Build ${LIBRARY_DESCRIPTION} in ${TARGET_BUILD_DIR}/${BUILDARCH}/lib."
		else 
		  echo "***** Failed to build ${LIBRARY_DESCRIPTION}  *****" 
		  exit 1 
		fi 
	done

  # Create the path to the real Headers dir
  /bin/rm -rf "${FRAMEWORK_LOCN}"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Resources/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Documentation/"
  # Create the required symlinks
  /bin/ln -sfh A "${FRAMEWORK_LOCN}/Versions/Current"
  /bin/ln -sfh Versions/Current/Headers "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/${LIBRARY_NAME}" "${FRAMEWORK_LOCN}/${LIBRARY_NAME}"
  # Copy the public headers into the framework
  cp "$TARGET_SRC/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/"

	# build universal library
  if [ -n "$TARGET_DYNAMIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_DYNAMIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
    /usr/bin/install_name_tool -id "@rpath/${LIBRARY_NAME}.framework/Versions/A/${LIBRARY_NAME}" \
       "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
	elif [ -n "$TARGET_STATIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_STATIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
  fi

  /bin/ln -sfh "Versions/Current/Headers" "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/Resources" "${FRAMEWORK_LOCN}/Resources"
  /bin/ln -sfh "Versions/Current/Documentation" "${FRAMEWORK_LOCN}/Documentation"
  [ -d `dirname ${DEBUG_FRAMEWORK_LOCN}` ] || /bin/mkdir `dirname ${DEBUG_FRAMEWORK_LOCN}`
  /bin/rm -rf ${DEBUG_FRAMEWORK_LOCN}
  /bin/ln -sfh ${FRAMEWORK_LOCN} ${DEBUG_FRAMEWORK_LOCN}

	case "$SDKROOT" in
	  *iPhoneSimulator*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
  	<array>
	    <string>iPhoneOS</string>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *iPhoneOS*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>iPhoneOS</string>
	  </array>"
	    ;;
	  *MacOSX*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *)
	    ;;
	esac

	cat > ${FRAMEWORK_LOCN}/Resources/Info.plist <<EOF
		<?xml version="1.0" encoding="UTF-8"?>
		<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
		<plist version="1.0">
		<dict>
		  <key>CFBundleDevelopmentRegion</key>
		  <string>English</string>
		  <key>CFBundleIdentifier</key>
		  <string>sqlite3</string>
			<key>CFBundleExecutable</key>
			<string>sqlite3</string>	
		  <key>CFBundleInfoDictionaryVersion</key>
		  <string>6.0</string>
		  <key>CFBundlePackageType</key>
		  <string>FMWK</string>
		  <key>CFBundleSignature</key>
		  <string>????</string>
		  <key>CFBundleVersion</key>
		  <string>${VERSION}</string>
				$PLATFORM_KEY
			</dict>
			</plist>
EOF
  
  echo "Create ${LIBRARY_NAME}.framework for ${BUILDARCHS} in ${FRAMEWORK_LOCN}."
  if [ "x$SIGNATURE" != "x" ] ; then
		/usr/bin/codesign -i "${LIBRARY_NAME}" --force --deep --sign "${SIGNATURE}" --preserve-metadata=identifier,entitlements --timestamp=none "${FRAMEWORK_LOCN}/Versions/A" && echo "signed with ${SIGNATURE}"
	else 
  	# sign binary with empty signatur
    /usr/bin/codesign --force --sign - "${FRAMEWORK_LOCN}/Versions/A"
  fi 

	exit 0
}

# - - - - - - - - - - - - - - -
cleanAction () {	
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${LIBRARY_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  echo "Cleaning ${FRAMEWORK_LOCN} ..."
  rm -rf "${FRAMEWORK_LOCN}"
  rm -f  "${DEBUG_FRAMEWORK_LOCN}"
	echo "Cleaning ${LIBRARY_NAME} sources ..."
  rm -rf "${PROJECT_DERIVED_FILE_DIR}" 
  exit 0;
}

###############################
# MAIN

case $ACTION in
  # NOTE: for some reason, it gets set to "" rather than "build" when
  # doing a build.
  "")
    buildAction
    ;;

  "clean")
    cleanAction
    ;;
esac

exit 0
