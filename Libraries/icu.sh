#!/bin/sh
#
# These are implemented as functions, and just called by the
# short MAIN section below
## Determine the appropriate icu source path to use
## Introduced by michaeltyson, 
## Adapted by Thorsten Steube

#set
#set -x

##
## environment variables
##
LIBRARY_NAME=icu
#LIBRARY_NAMES="i18n io le lx uc data"
LIBRARY_NAMES="${LIBRARY_NAME}i18n ${LIBRARY_NAME}io ${LIBRARY_NAME}tu ${LIBRARY_NAME}uc ${LIBRARY_NAME}data"

# extend environment
#
#SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS6.1.sdk
#SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.1.sdk
#SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk
#
# derive DEVELOPER environment from SDKROOT
PLATFORM_DEVELOPER_SDK=${SDKROOT}/..
PLATFORM_DEVELOPER_DIR=${PLATFORM_DEVELOPER_SDK}/..
PLATFORM_DEVELOPER_USR_DIR=${PLATFORM_DEVELOPER_DIR}/usr/
PLATFORM_DEVELOPER_LIB_DIR=${PLATFORM_DEVELOPER_USR_DIR}/lib

#TARGET_BUILD_DIR=/Users/tsteube/Development/tmp/XcodeBuild/Release-iphoneos
#SRCROOT=/Users/tsteube/Development/ejin-alashan/libs
#ARCHS="armv7 i386 x86_64"

# figure out the right set of build architectures for this run
BUILDARCHS="${ARCHS}"
case "$SDKROOT" in
  *iPhoneSimulator*)
    BUILDARCHS="i386 x86_64"
    ;;
  *iPhoneOS*)
    BUILDARCHS="armv7 arm64"
    ;;
  *MacOSX*)
    BUILDARCHS="x86_64"
    ;;
	*)
		echo "***** unknown SDK $SDKROOT *****"
		exit 1
		;;
esac


# remove Xcode.app internal unix programs
#export PATH=`echo ${PATH} | awk -v RS=: -v ORS=: '/Xcode.app/ {next} {print}'`

# - - - - - - - - - - - - - - -
buildAction () {

  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${LIBRARY_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  if [ -d "${FRAMEWORK_LOCN}" ]; then
    echo "${LIBRARY_NAME} Framework in ${TARGET_BUILD_DIR} has already been build"
    return;
  fi
  echo "Building ${LIBRARY_NAME} ..."

	# locate src archive file if present
  SRC_ARCHIVE=( ${SRCROOT}/*${LIBRARY_NAME}*.tgz )
	if [ "${SRC_ARCHIVE}" == "" ]
	then
 		echo "***** \"${LIBRARY_NAME}\" source package not found" >&2
 		exit 1;
	fi
  
	TARGET_SRC="${PROJECT_DERIVED_FILE_DIR}"
	if [ ! -d "${TARGET_SRC}" ] ||  [ -z "$(ls ${TARGET_SRC}/)" ]; then
  	echo "***** extracting $SRC_ARCHIVE to $TARGET_SRC ..."
	  mkdir -p "$TARGET_SRC"
	  tar -C "$TARGET_SRC" --strip-components=1 -zxf "$SRC_ARCHIVE" || exit 1
  fi

  CROSS_COMPILE_SUBDIR="$TARGET_SRC/source/build_host"
  for BUILDARCH in $BUILDARCHS
 	do
    LIBRARY_DESCRIPTION="${CONFIGURATION} ${LIBRARY_NAME} ${BUILDARCH} Library for ${SDKROOT}"
  	echo "Building ${LIBRARY_DESCRIPTION} ..."

    cd "$TARGET_SRC/source/" && gnumake clean

    case "$SDKROOT" in
      *iPhoneSimulator*)
        COMPILE_SUBDIR="$TARGET_SRC/source/build_sim"
        CONFIGUREFLAGS="--host=i686-apple-darwin11 --disable-shared --enable-static --with-data-packaging=archive --disable-renaming --with-cross-build=$CROSS_COMPILE_SUBDIR"
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension -mios-simulator-version-min=10.0 -no-cpp-precomp -D_DARWIN_C_SOURCE"
        ;;
      *iPhoneOS*)
        # system is not supported on ios sdk  !!!
        cd "$TARGET_SRC"
        sed -i.bak 's/int result = system(cmd);/int result = -1;/' ./source/tools/pkgdata/pkgdata.cpp

        COMPILE_SUBDIR="$TARGET_SRC/source/build_ios"
		    case "$BUILDARCH" in
		      arm64)
  	        CONFIGUREFLAGS="--disable-shared --enable-static --disable-samples --disable-tests --host=arm-apple-darwin --with-data-packaging=archive  --disable-renaming --with-cross-build=$CROSS_COMPILE_SUBDIR"
  	        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension  -miphoneos-version-min=10.0  -fembed-bitcode"
          ;;
		      armv*)
	          CONFIGUREFLAGS="--disable-shared --enable-static --disable-samples --disable-tests --with-data-packaging=archive --host=arm-apple-darwin  --disable-renaming --with-cross-build=$CROSS_COMPILE_SUBDIR"
	          COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension -miphoneos-version-min=10.0 -fembed-bitcode"
          ;;
          *)
	          CONFIGUREFLAGS="--disable-shared --enable-static"
	          COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fembed-bitcode"
          ;;
        esac
        ;;
      *MacOSX*)
        COMPILE_SUBDIR="$TARGET_SRC/source/build_host"
        CONFIGUREFLAGS="--host=i686-apple-darwin11 --disable-shared --enable-static --disable-renaming --with-data-packaging=archive"
        COMMONFLAGS="-arch $BUILDARCH -isysroot ${SDKROOT} -fapplication-extension -mmacosx-version-min=10.12 -no-cpp-precomp -D_DARWIN_C_SOURCE"
        ;;
			*)
  			echo "***** unknown SDK $SDKROOT *****"
   			exit 1
				;;
    esac

    [ -d "$COMPILE_SUBDIR" ] || mkdir "$COMPILE_SUBDIR"
    cd "$COMPILE_SUBDIR"

    case "$SDKROOT" in
      *iPhoneSimulator*)
        LIB_TYPES="--disable-shared --enable-static"
        ;;
      *iPhoneOS*)
        LIB_TYPES="--disable-shared --enable-static"
        ;;
      *MacOSX*)
        LIB_TYPES="--disable-shared --enable-static"
        ;;
			*)
  			echo "***** unknown SDK $SDKROOT *****"
   			exit 1
				;;
    esac

    # see common/unicode/uconfig.h
    export ICU_FLAGS="-O3 -DU_USING_ICU_NAMESPACE=0 -DU_NO_DEFAULT_INCLUDE_UTF_HEADERS=1 -DUCONFIG_NO_LEGACY_CONVERSION=1 -DUCONFIG_ONLY_HTML_CONVERSION=1 -DUCONFIG_NO_BREAK_ITERATION=1 -DUCONFIG_NO_FILTERED_BREAK_ITERATION=1 -DUCONFIG_NO_COLLATION=1 -DUCONFIG_NO_FORMATTING=0 -DUCONFIG_NO_TRANSLITERATION=1 -DUCONFIG_NO_REGULAR_EXPRESSIONS=1 -DU_DISABLE_RENAMING=1 -DUCONFIG_NO_NORMALIZATION=1 -DUCONFIG_NO_SERVICE=1 -DUCONFIG_NO_IDNA=1 -DU_CHARSET_IS_UTF8=1"
    export CC=`xcrun --find cc -sdk $SDKROOT`
    export CXX=`xcrun --find c++ -sdk $SDKROOT`
    export LD=`xcrun --find ld -sdk $SDKROOT`
    export LIBTOOL=`xcrun --find libtool -sdk $SDKROOT`
    export AR=`xcrun --find ar -sdk $SDKROOT`
    export AS=`xcrun --find as -sdk $SDKROOT`
    export NM=`xcrun --find nm -sdk $SDKROOT`
    export RANLIB=`xcrun --find ranlib -sdk $SDKROOT`
    export LDFLAGS="${COMMONFLAGS} -O2"
    export CFLAGS="${COMMONFLAGS}"
    export CPPFLAGS="${COMMONFLAGS} ${ICU_FLAGS}"
    export CXXFLAGS="${COMMONFLAGS} ${ICU_FLAGS}"
    #############################
    # run configure + make
    ../configure ${CONFIGUREFLAGS} && gnumake clean && gnumake
    if [ "$?" == "0" ]
    then

		  [ -d "${TARGET_BUILD_DIR}/${BUILDARCH}/lib" ] || mkdir -p "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
		  rm -rf "${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}"*

      unset SOURCE_STATIC_LIBS
      unset SOURCE_DYNAMIC_LIBS
      for libName in ${LIBRARY_NAMES}
      do
	      if [ -f "./lib/lib${libName}.dylib" ]; then
	        SOURCE_DYNAMIC_LIBS="$SOURCE_DYNAMIC_LIBS ./lib/lib${libName}.dylib"
        elif [ -f "./stubdata/lib${libName}.dylib" ]; then
          SOURCE_DYNAMIC_LIBS="$SOURCE_DYNAMIC_LIBS ./stubdata/lib${libName}.dylib"
	      elif [ -f "./lib/lib${libName}.a" ]; then
	        SOURCE_STATIC_LIBS="$SOURCE_STATIC_LIBS ./lib/lib${libName}.a"
        elif [ -f "./stubdata/lib${libName}.a" ]; then
          SOURCE_STATIC_LIBS="$SOURCE_STATIC_LIBS ./stubdata/lib${libName}.a"
	      fi
      done
		  if [ -n "${SOURCE_DYNAMIC_LIBS}" ]; then
        rm -f ./lib${LIBRARY_NAME}.dylib
		    libtool -o ./lib${LIBRARY_NAME}.dylib ${SOURCE_DYNAMIC_LIBS}
        cp -RPLf ./lib${LIBRARY_NAME}.dylib "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
        TARGET_DYNAMIC_LIBS="$TARGET_DYNAMIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.dylib"
      elif [ -n "${SOURCE_STATIC_LIBS}" ]; then
        rm -f ./lib${LIBRARY_NAME}.a
		    libtool -static -o ./lib${LIBRARY_NAME}.a ${SOURCE_STATIC_LIBS}
        cp -RPLf ./lib${LIBRARY_NAME}.a "${TARGET_BUILD_DIR}/${BUILDARCH}/lib"
        TARGET_STATIC_LIBS="$TARGET_STATIC_LIBS ${TARGET_BUILD_DIR}/${BUILDARCH}/lib/lib${LIBRARY_NAME}.a"
      fi

		else 
		  echo "***** Failed to build ${LIBRARY_DESCRIPTION}  *****" 
		  exit 1 
		fi 
	done

  # Create the path to the real Headers dir
  /bin/rm -rf "${FRAMEWORK_LOCN}"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Resources/"
  /bin/mkdir -p "${FRAMEWORK_LOCN}/Versions/A/Documentation/"
  # Create the required symlinks
  /bin/ln -sfh A "${FRAMEWORK_LOCN}/Versions/Current"
  /bin/ln -sfh Versions/Current/Headers "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/${LIBRARY_NAME}" "${FRAMEWORK_LOCN}/${LIBRARY_NAME}"
  # Copy the public headers into the framework
  mkdir "${FRAMEWORK_LOCN}/Versions/A/Headers/unicode"
	/bin/cp -a "$TARGET_SRC/source/common/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/cp -a "$TARGET_SRC/source/common/unicode/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/unicode/"
  /bin/cp -a "$TARGET_SRC/source/io/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/cp -a "$TARGET_SRC/source/io/unicode/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/unicode/"
  /bin/cp -a "$TARGET_SRC/source/i18n/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/"
  /bin/cp -a "$TARGET_SRC/source/i18n/unicode/"*.h "${FRAMEWORK_LOCN}/Versions/A/Headers/unicode/"

  # patch header files include inside the framework
  sed -i.~ 's/#[[:space:]]*include "\(.*\)"/#include <icu\/\1>/' "${FRAMEWORK_LOCN}/Versions/A/Headers/"*.h
  rm -f "${FRAMEWORK_LOCN}/Versions/A/Headers/"*.h.~
  sed -i.~ 's/#[[:space:]]*include "\(.*\)"/#include <icu\/\1>/' "${FRAMEWORK_LOCN}/Versions/A/Headers/unicode/"*.h
  rm -f "${FRAMEWORK_LOCN}/Versions/A/Headers/unicode/"*.h.~

  # rebuild adapted data package
  ICU_DAT_FILELIST=( ${SRCROOT}/icudata.lst )
  if [ "${ICU_DAT_FILELIST}" != "" ]
  then
    echo "***** \"${LIBRARY_NAME}\" rebuild data package" >&2
    cd ../build_host/data
    echo timestamp > build-local
    cp "$ICU_DAT_FILELIST" out/tmp/
    make
    /bin/cp out/icudt*.dat "${FRAMEWORK_LOCN}/Versions/A/Resources/"
    cd ../
  fi

	# build universal library
  if [ -n "$TARGET_DYNAMIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_DYNAMIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
    /usr/bin/install_name_tool -id "@rpath/${LIBRARY_NAME}.framework/${LIBRARY_NAME}" \
       "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
  elif [ -n "$TARGET_STATIC_LIBS" ]; then
    /usr/bin/lipo -create ${TARGET_STATIC_LIBS} -output "${FRAMEWORK_LOCN}/Versions/A/${LIBRARY_NAME}"
  fi

  /bin/ln -sfh "Versions/Current/Headers" "${FRAMEWORK_LOCN}/Headers"
  /bin/ln -sfh "Versions/Current/Resources" "${FRAMEWORK_LOCN}/Resources"
  /bin/ln -sfh "Versions/Current/Documentation" "${FRAMEWORK_LOCN}/Documentation"
  [ -d `dirname ${DEBUG_FRAMEWORK_LOCN}` ] || /bin/mkdir `dirname ${DEBUG_FRAMEWORK_LOCN}`
  /bin/rm -rf ${DEBUG_FRAMEWORK_LOCN}
  /bin/ln -sfh ${FRAMEWORK_LOCN} ${DEBUG_FRAMEWORK_LOCN}

	case "$SDKROOT" in
	  *iPhoneSimulator*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
  	<array>
	    <string>iPhoneOS</string>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *iPhoneOS*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>iPhoneOS</string>
	  </array>"
	    ;;
	  *MacOSX*)
	      PLATFORM_KEY="<key>CFBundleSupportedPlatforms</key>
	  <array>
	    <string>MacOSX</string>
	  </array>"
	    ;;
	  *)
	    ;;
	esac
	
	cat > ${FRAMEWORK_LOCN}/Resources/Info.plist <<EOF
	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
	<dict>
	  <key>CFBundleDevelopmentRegion</key>
	  <string>English</string>
	  <key>CFBundleIdentifier</key>
	  <string>${LIBRARY_NAME}</string>
    <key>CFBundleExecutable</key>
		<string>${LIBRARY_NAME}</string>	
	  <key>CFBundleInfoDictionaryVersion</key>
	  <string>6.0</string>
	  <key>CFBundlePackageType</key>
	  <string>FMWK</string>
	  <key>CFBundleSignature</key>
	  <string>????</string>
	  <key>CFBundleVersion</key>
	  <string>${VERSION}</string>
	  $PLATFORM_KEY
	 </dict>
	 </plist>
EOF

  echo "Create ${LIBRARY_NAME}.framework for ${BUILDARCHS} in ${FRAMEWORK_LOCN}."
  if [ "x$SIGNATURE" != "x" ] ; then
		/usr/bin/codesign -i "${LIBRARY_NAME}" --force --deep --sign "${SIGNATURE}" --preserve-metadata=identifier,entitlements --timestamp=none "${FRAMEWORK_LOCN}/Versions/A" && echo "signed with ${SIGNATURE}"
	else 
  	# sign binary with empty signatur
    /usr/bin/codesign --force --sign - "${FRAMEWORK_LOCN}/Versions/A"
  fi 

	exit 0
}

# - - - - - - - - - - - - - - -
cleanAction () {
  FRAMEWORK_LOCN="${BUILT_PRODUCTS_DIR}/${LIBRARY_NAME}.framework"
  DEBUG_FRAMEWORK_LOCN="${FRAMEWORK_LOCN/Release/Debug}"

  echo "Cleaning ${FRAMEWORK_LOCN} ..."
  rm -rf "${FRAMEWORK_LOCN}"
  rm -f  "${DEBUG_FRAMEWORK_LOCN}"

  case "$SDKROOT" in
  *MacOSX*)
    echo "Cleaning ${LIBRARY_NAME} sources ..."
    rm -rf "${PROJECT_DERIVED_FILE_DIR}"
  ;;
  *)
  ;;
  esac

  exit 0;
}

###############################
# MAIN

case $ACTION in
  # NOTE: for some reason, it gets set to "" rather than "build" when
  # doing a build.
  "")
    buildAction
    ;;

  "clean")
    cleanAction
    ;;
esac

exit 0
