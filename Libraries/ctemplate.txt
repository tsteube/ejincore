URL: http://code.google.com/p/google-ctemplate/

% ./configure --prefix=/Users/tsteube/local/ 
% make
% make check
% make install

-------------
Reconfigure for i386 architecture

% make distclean

% ./configure NO_THREADS=1 --enable-shared=no 

% make

% cp .libs/libctemplate_nothreads.a lnsout/libctemplate_nothreads.x86_64

-------------
Configure for ARM architecture

export DEVROOT=/Developer/Platforms/iPhoneOS.platform/Developer
export SDKROOT=$DEVROOT/SDKs/iPhoneOS4.3.sdk

% make distclean

% ./configure CC=$DEVROOT/usr/bin/gcc CCFLAGS="-arch armv7 -isysroot $SDKROOT -no-cpp-precomp -miphoneos-version-min=3.0" CXX=$DEVROOT/usr/bin/g++ CXXFLAGS="-arch armv7 -isysroot $SDKROOT -no-cpp-precomp -miphoneos-version-min=3.0" LD=$DEVROOT/usr/bin/ld LDFLAGS="-O2" AR=$DEVROOT/usr/bin/ar RANLIB=$DEVROOT/usr/bin/ranlib AS=$DEVROOT/usr/bin/as NM=$DEVROOT/usr/bin/nm RANLIB=$DEVROOT/usr/bin/ranlib --host=arm-apple-darwin --enable-shared=no

% make

% cp .libs/libctemplate_nothreads.a lnsout/libctemplate_nothreads.arm

=================
Create fat lib

% $DEVROOT/usr/bin/lipo -arch arm lnsout/libctemplate_nothreads.arm -arch x86_64 lnsout/libctemplate_nothreads.x86_64 -create -output lnsout/libctemplate_static

