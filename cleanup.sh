#!/bin/sh
#

#set

case $ACTION in
  # NOTE: for some reason, it gets set to "" rather than "build" when
  # doing a build.
  "")
    ;;

  "clean")
    rm -rf "${BUILT_PRODUCTS_DIR}/${PUBLIC_HEADERS_FOLDER_PATH}"
    ;;
esac

exit 0
